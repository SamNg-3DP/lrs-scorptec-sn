<?php


class St_client_visit_m extends MY_Model {
    /**
     * set active status for specail job incident
     * @param $job_id
     * @return boolean
     */
    function set_active($job_id)
    {
    	return $this->update($job_id, 
    			array('status' => ACTIVE)
    	);
    }
    
    /**
     * set in-active status for specail job incident
     * @param $job_id
     * @return boolean
     */
	function set_inactive($job_id)
	{
    	return $this->update($job_id,
    			array('status' => IN_ACTIVE)
    	);
    }
    
    function get_by_id($id, $id_type='client_visit'){
    	$sql = "SELECT job.*, user.first_name, user.last_name , request.state as req_state
				FROM st_client_visit job
				LEFT JOIN st_request request ON request.id = job.`request_id`
				LEFT JOIN st_users `user` ON job.user_id = user.id
				LEFT JOIN st_department department ON department.id = user.department_id 
				LEFT JOIN st_site AS site ON department.site_id = site.id";
    	
    	if ($id_type == 'client_visit')
    		$sql .= " WHERE job.id=".$id;
    	elseif ($id_type == 'request')
    		$sql .= " WHERE job.request_id=".$id;
    	
    	log_message("debug", "get_info == ".$sql);
    	$query = $this->db->query($sql);
        return $query->row();
    }
    
    /**
     * get client visit by status.
     */
    function get_by_status($status, $filters=array(), $site_id=0, $is_count=false, $page_no=1, $limit=PAGE_SIZE){
    	
    	$sql = "SELECT job.*, user.first_name, user.last_name, user.user_name
				FROM st_client_visit job
				LEFT JOIN st_request request ON request.id = job.`request_id`
				LEFT JOIN st_users `user` ON job.user_id = user.id
				LEFT JOIN st_department department ON department.id = user.department_id 
				LEFT JOIN st_site AS site ON department.site_id = site.id";
    	
    	switch ($status){
    		case 'new':
    			$sql .= " WHERE request.state=20 AND job.visit_date >= ".now();
    			break;
    		case 'upcoming':
    			$sql .= " WHERE request.state=40 AND job.visit_date >= ".now();
    			break;
    		case 'completed':
    			$sql .= " WHERE job.visit_date<".now();
    			break;
    		case 'reimburse':
    			$sql .= " WHERE request.state=40 AND job.reimbursed=0 AND job.visit_date<".now();
    			break;	
    	}
    	
    	if (!empty($filters)){
    		foreach ($filters as $key => $val){
    			switch ($key){
	    			case 'user_name':
	    				if ($val)
							$sql .= " AND user.".$key."='".$val."'";
    					break;
	    			case 'time_period':
	    				if ($val){
		    				$time_period = get_time_period($val);
		    				$sql .= " AND job.visit_date > '".$time_period[0]."' AND job.visit_date < '".$time_period[1]."'";
	    				}
	    				break;
	    				
    			}
    			
    		}
    	}
    	
    	
    	$sql .= " ORDER BY job.visit_date desc ";
    	if(empty($page_no)){
			$page_no = 1;
		}
		
		if(!empty($limit) && !$is_count){
			$start = ($page_no - 1) * $limit;
			$sql .= " LIMIT " . $start . ", " . $limit;
		}
		
		log_message("debug", "get_by_state_user <<< " . $sql);
    	$query = $this->db->query($sql);
    	
    	if ($is_count)
    		return $query->num_rows();
    	else
    		return $query->result();
    }
    
    function update_by_id($id, $arr){
    	$this->db->where('id', $id);
		return $this->db->update($this->_table, $arr);
    }    
    
    
    function get_scorpy_order_master($ordid){
//    	$scorp_db = $this->db->load("scorpy", TRUE);
    	
    	$query = $this->db->query("SELECT om.*, m.qb_name, sm.service_datetime, sm.service_tech 
    								FROM scorplocal.order_master om
    								LEFT JOIN scorplocal.`member` m ON m.memid = om.memid
    								LEFT JOIN scorplocal.`service_master` sm ON sm.ordid=om.ordid 
    								WHERE om.ordid=".$ordid);
    	return $query->row();
    }
}