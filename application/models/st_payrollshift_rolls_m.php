<?php

class St_payrollshift_rolls_m extends MY_Model {

    function get_by_payrollshift($payrollshift_id){
    	$sql = "SELECT roll.*, daily.description 
    			FROM st_payrollshift_rolls AS roll, st_daily_shift AS daily
    			WHERE roll.payrollshift_id = " . $payrollshift_id . "    			 
    				AND roll.daily_shift_id = daily.id " .
    			" ORDER BY roll.start ASC";
    	$query = $this->db->query($sql);
		return $query->result();
    }
    
    function delete_by_payrollshift($payrollshift_id){
		$this->db->delete($this->_table, array('payrollshift_id' => $payrollshift_id));
    }
}