<?php

class St_publicholiday_m extends MY_Model {
	
	function get_publicholidays($year=false, $start=false, $end=false) {
		
		$sql = "SELECT * FROM st_publicholiday";
		
		if ($year)
			$sql .= " WHERE year(from_unixtime(date))=".$year;
			
		if ($start && $end && !$year){
			$sql .= " WHERE `date`>=".$start." AND `date`<=".$end;
		}
		
		$sql .= " ORDER BY date";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function delete($publicholiday_id) {
        $this->db->query("DELETE FROM st_publicholiday WHERE id = '$publicholiday_id';"); 
	}
	
	/**
	 * check whether public holiday date is exists 
	 * @param $date
	 * @return int
	 */
	function check_except($date){
		$sql = "SELECT * FROM st_publicholiday
				WHERE `date` = " . strtotime($date);
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
}