<?php

/**
 * user level
 * @author Bai Yinbing
 *
 */
class St_user_level_m extends MY_Model {

	function get_user_levels() {
		$query = $this->db->query("SELECT * FROM st_user_level ORDER BY id;");
		return $query->result();
	}

    function get_userlevel_active() {
		$query = $this->db->query("SELECT * FROM st_user_level WHERE status = ". ACTIVE. " ORDER BY id;");
		return $query->result();
	}
	
	/**
	 * get user level list which less then current own user 's user_level
	 * @param $my_user_level
	 * @return array
	 */
	function get_my_user_level($my_user_level) {
		$query = $this->db->query("SELECT * FROM st_user_level WHERE status = ". ACTIVE. " and id <= " . $my_user_level ." ORDER BY id;");
		return $query->result();
	}

    function get_userlevel_single($userlevelid) {
		$query = $this->db->query("SELECT * FROM st_user_level WHERE id = $userlevelid;");
		return $query->result();
	}
	
	/**
	 * get user level by user level id
	 * @param $dapartmentid
	 * @return unknown_type
	 */
	function get_by_user_level_id($user_level_id) {
		return parent::get("id",$user_level_id);
	}
    
	/**
	 * get dropdown list
	 * @return array
	 */
	function get_dropdown(){
		$this->db
    		->where('status', ACTIVE)
    		->order_by('id', 'ASC');
		$user_levels = $this->get_all();
		$a_ret = array();
		$a_ret[''] = 'All';
		foreach ($user_levels as $row){
			$a_ret[$row->id] = $row->user_level;
		}
		return $a_ret;
	}
	
	/**
	 * get dropdown list
	 * @return array
	 */
	function get_select_dropdown(){
		$this->db
    		->where('status', ACTIVE)
    		->order_by('id', 'ASC');
		$user_levels = $this->get_all();
		$a_ret = array();
		$a_ret[''] = 'Please select ...';
		foreach ($user_levels as $row){
			$a_ret[$row->id] = $row->user_level;
		}
		return $a_ret;
	}
}