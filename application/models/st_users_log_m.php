<?php

class St_users_log_m extends MY_Model {
    
    /**
     * get user by user name
     * @param $user_name
     * @return user object
     */
    function history($user_name){
    	$sql = 'SELECT log.*, department.department, userlevel.user_level, employment_type.employment_type 
            FROM st_users_log AS log, st_department AS department, st_user_level AS userlevel, st_employment_type AS employment_type  
            WHERE log.department_id=department.id 
            	AND log.user_level_id=userlevel.id
            	AND log.employment_type_id=employment_type.id 
            	AND log.user_name = "'. $user_name .'"
            ORDER BY log.operate_time ASC'; 
        $query = $this->db->query($sql);
        return $query->result();
    }
}