<?php

/**
 * job incident model class
 * @author Bai Yinbing
 */
class St_job_incident_m extends MY_Model {
    /**
     * set active status for specail job incident
     * @param $job_id
     * @return boolean
     */
    function set_active($job_id)
    {
    	return $this->update($job_id, 
    			array('status' => ACTIVE)
    	);
    }
    
    /**
     * set in-active status for specail job incident
     * @param $job_id
     * @return boolean
     */
	function set_inactive($job_id)
	{
    	return $this->update($job_id,
    			array('status' => IN_ACTIVE)
    	);
    }
    
    function get_info($id){
/*     	$sql = "SELECT job.*, user.user_name, user.first_name, user.last_name" .
    			" , user.user_level_id, user.email, user.personal_email, dept.department " .
    			" FROM st_job_incident AS job, st_users AS user, st_department AS dept " .
    			" WHERE job.id = " . $id .
    			" AND job.user_id = user.id " .
    			" AND user.department_id = dept.id"; */
    	
    	$sql = "SELECT job.*, user.first_name AS user_firstname, user.last_name AS user_lastname, staff.`first_name` AS staff_firstname, staff.`last_name` AS staff_lastname,
    			user.user_level_id, user.email, user.personal_email, dept.department, `user`.user_name AS user_username, staff.user_name AS staff_username, staff.email AS staff_email 
				    FROM st_job_incident AS job
					LEFT JOIN st_users `user` ON job.`user_id` = user.id 
					LEFT JOIN st_users staff ON job.`staff_id` = staff.id 
					LEFT JOIN st_department dept ON dept.id = user.department_id
					LEFT JOIN st_site site ON dept.site_id = site.id
    			WHERE job.id = " . $id;
    			
    	
    	log_message("debug", "get_info == ".$sql);
    	$query = $this->db->query($sql);
        return $query->row();
    }
    
    
    /**
     * get new job incidents
     */
    function get_new_by_user($user_id, $page_no=1, $limit=PAGE_SIZE){
    	return $this->get_by_state_user(1, $user_id, $page_no, $limit);
    }
    
    /**
     * get in progress job incidents
     */
    function get_progress_by_user($user_id, $page_no=1, $limit=PAGE_SIZE){
    	return $this->get_by_state_user(2, $user_id, $page_no, $limit);
    }
    
    /**
     * get completed job incidents
     */
    function get_completed_by_user($user_id, $page_no=1, $limit=PAGE_SIZE){
    	return $this->get_by_state_user(3, $user_id, $page_no, $limit);
    }
    
    /**
     * get count of new job incidents
     */
    function count_new_by_user($user_id){
    	return $this->count_by_state_user(1, $user_id);
    }
    
    /**
     * get count of in progress job incidents
     */
    function count_progress_by_user($user_id){
    	return $this->count_by_state_user(2, $user_id);
    }
    
    /**
     * get count of completed job incidents
     */
    function count_completed_by_user($user_id){
    	return $this->count_by_state_user(3, $user_id);
    }
        
    /**
	 * get count by state and user
	 */
    function count_by_state_user($state, $user_id){
    	$where = array(
    		'user_id' => $user_id,
    		'state' => $state,
    		'status' => ACTIVE
    	);
    	return $this->count_by($where);
    }
    
    /**
     * get job incidents by state and user
     */
    function get_by_state_user($state, $user_id, $page_no=1, $limit=PAGE_SIZE){
    	$sql = " SELECT job.*, user.first_name, user.last_name " .
    			" FROM st_job_incident AS job, st_users AS user " .
    			" WHERE job.user_id = user.id " .
    			"   AND job.state = " . $state .
    			"   AND job.status = " . ACTIVE .
    			" 	AND job.user_id = " . $user_id .
    			" ORDER BY job.id desc " ;
    	if(empty($page_no)){
			$page_no = 1;
		}
		
		if(!empty($limit)){
			$start = ($page_no - 1) * $limit;
			$sql .= " LIMIT " . $start . ", " . $limit;
		}
		
		log_message("debug", "get_by_state_user <<< " . $sql);
    	$query = $this->db->query($sql);
    	return $query->result();
    }
    
    /**
     * get new job incidents approvals
     */
    function get_approvals_by_new($user_level, $site_id=0, $department_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name="", $page_no=1, $limit=PAGE_SIZE){
    	return $this->get_approvals_by_state(1, $user_level, $site_id, $department_id, $period_start_time, $period_end_time, $first_name, $last_name, $page_no, $limit);
    }
    
    /**
     * get in progress job incidents approvals
     */
    function get_approvals_by_progress($user_level, $site_id=0, $department_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name="", $page_no=1, $limit=PAGE_SIZE){
    	return $this->get_approvals_by_state(2, $user_level, $site_id, $department_id, $period_start_time, $period_end_time, $first_name, $last_name, $page_no, $limit);
    }
    
    /**
     * get completed job incidents approvals
     */
    function get_approvals_by_completed($user_level, $site_id=0, $department_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name="", $page_no=1, $limit=PAGE_SIZE){
    	return $this->get_approvals_by_state(3, $user_level, $site_id, $department_id, $period_start_time, $period_end_time, $first_name, $last_name, $page_no, $limit);
    }
    
    /**
     * get approvals job incidents by state
     */
    function get_approvals_by_state($state, $user_level, $site_id=0, $department_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name="", $page_no=1, $limit=PAGE_SIZE){
/*     	$sql = " SELECT job.*, user.first_name, user.last_name " .
    			" FROM st_job_incident AS job, st_users AS user " .
    			" , st_department AS department, st_site AS site" .
    			" WHERE job.user_id = user.id " .
    			"   AND department.id = user.department_id " .
    			"  AND department.site_id = site.id " .
    			"   AND job.state = " . $state .
    			"   AND job.status = " . ACTIVE; */
    	
    	$sql = "SELECT job.*, user.first_name AS user_firstname, user.last_name AS user_lastname, staff.`first_name` AS staff_firstname, staff.`last_name` AS staff_lastname 
				FROM st_job_incident AS job
					LEFT JOIN st_users `user` ON job.`user_id` = user.id 
					LEFT JOIN st_users staff ON job.`staff_id` = staff.id 
					LEFT JOIN st_department dept ON dept.id = user.department_id
					LEFT JOIN st_site site ON dept.site_id = site.id
				WHERE job.user_id = user.id
					AND dept.id = user.department_id
					AND dept.site_id = site.id
					AND job.state = " . $state . "
					AND job.status = " . ACTIVE;
    	
    	
    	if($user_level == 2){
    		$sql .= " AND user.user_level_id != 3 AND user.user_level_id != 4";
    	}

		if(!empty($site_id) && $site_id > 0){
			$sql .= " AND dept.site_id = " . $site_id;
		}
		
    	if(!empty($department_id)){
			$sql .= " AND dept.department = '" . $department_id . "'";
		}
		
		if(!empty($period_start_time) && $period_start_time > 0){
			$sql .= " AND job.incident_date >= " . $period_start_time;
		}
		
		if(!empty($period_end_time) && $period_end_time > 0){
			$sql .= " AND job.incident_date < " . $period_end_time;
		}
		
		if(!empty($first_name)){
			$sql .= " AND user.first_name like '%" . $first_name . "%'";
		}
		
		if(!empty($last_name)){
			$sql .= " AND user.last_name like '%" . $last_name . "%'";
		}		
		

    	$sql .= " ORDER BY job.id desc ";		
    	if(empty($page_no)){
			$page_no = 1;
		}
		
		if(!empty($limit)){
			$start = ($page_no - 1) * $limit;
			$sql .= " LIMIT " . $start . ", " . $limit;
		}
		
		log_message("debug", "get_by_state_user <<< " . $sql);
    	$query = $this->db->query($sql);
    	return $query->result();
    }
    
    /**
     * get count of new job incidents
     */
    function count_approvals_by_new($user_level, $site_id=0, $department_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name=""){
    	return $this->count_approvals_by_state(1, $user_level, $site_id, $department_id, $period_start_time, $period_end_time, $first_name, $last_name);
    }
    
    /**
     * get count of in progress job incidents
     */
    function count_approvals_by_progress($user_level, $site_id=0, $department_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name=""){
    	return $this->count_approvals_by_state(2, $user_level, $site_id, $department_id, $period_start_time, $period_end_time, $first_name, $last_name);
    }
    
    /**
     * get count of completed job incidents
     */
    function count_approvals_by_completed($user_level, $site_id=0, $department_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name=""){
    	return $this->count_approvals_by_state(3, $user_level, $site_id, $department_id, $period_start_time, $period_end_time, $first_name, $last_name);
    }
    
    /**
     * get count approvals job incidents by state
     */
    function count_approvals_by_state($state, $user_level, $site_id=0, $department_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name=""){
    	$sql = " SELECT count(job.id) AS num " .
    			" FROM st_job_incident AS job, st_users AS user " .
    			" , st_department AS dept, st_site AS site" .
    			" WHERE job.user_id = user.id " .
    			"   AND dept.id = user.department_id " .
    			"  AND dept.site_id = site.id " .
    			"   AND job.state = " . $state .
    			"   AND job.status = " . ACTIVE;
    	
    	if($user_level == 2){
    		$sql .= " AND user.user_level_id != 3 AND user.user_level_id != 4";
    	}

		if(!empty($site_id) && $site_id > 0){
			$sql .= " AND dept.site_id = " . $site_id;
		}
		
    	if(!empty($department_id)){
			$sql .= " AND dept.department = '" . $department_id . "'";
		}
		
		if(!empty($period_start_time) && $period_start_time > 0){
			$sql .= " AND job.incident_date >= " . $period_start_time;
		}
		
		if(!empty($period_end_time) && $period_end_time > 0){
			$sql .= " AND job.incident_date < " . $period_end_time;
		}
		
		if(!empty($first_name)){
			$sql .= " AND user.first_name like '%" . $first_name . "%'";
		}
		
		if(!empty($last_name)){
			$sql .= " AND user.last_name like '%" . $last_name . "%'";
		}
		
		$total_count = 0;
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0){
			$row = $query->row();
			$total_count = $row->num;
		}
		
        return $total_count;
    }
    
    function get_scorpy_order_master($ref_no, $customer){
  	//    	$scorp_db = $this->db->load("scorpy", TRUE);

    	$query = $this->db->query("SELECT om.*
    								FROM scorplocal.order_master om
    								WHERE (om.ordno='".$ref_no."' or om.invoice_no='".$ref_no."') and om.bill_name='".$customer."'");
    	
    	return $query->row();
    }    
}