<?php

/**
 * daily shift time slots
 */
class St_daily_shift_slot_m extends MY_Model {
	
	/**
	 * get time slot by daily shift id
	 * @param int $daily_shift_id
	 */
	function get_by_daily_shift($daily_shift_id){
		$sql =  " SELECT slot.*, cat.description as cat_name " .
				" FROM st_daily_shift_slot AS slot, st_shift_category AS cat " .
				" WHERE slot.daily_shift_id = " . $daily_shift_id .
				"     AND slot.status = " . ACTIVE .
				"     AND slot.shift_category_code = cat.code " .
				" ORDER BY slot.id ASC"
				;
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	
	
	function get_calendar_by_user($user_id, $date){
		$payroll = false;
		
		$query = $this->db->query("SELECT ul.* FROM st_user_labor ul
									WHERE ul.user_id=".$user_id." AND ul.status=1 AND ul.`entry_date` <= '".$date."' AND 
									ul.`entry_date` IN (SELECT MAX(entry_date) FROM st_user_labor u WHERE ul.`user_id`=u.`user_id` AND u.status=1 AND u.`entry_date` <= '".$date."')");

		$labor = $query->row();
		if ($labor){
			$query = $this->db->query("SELECT * FROM st_payrollshift AS pay
										WHERE pay.id = " . $labor->payrollshift_id);
					
			$payroll = $query->row();
		}
		if($payroll){
			
			$payroll_date = $payroll->start_date;
			$rolldays = $payroll->rolldays;
			
			if($payroll_date >= $date){
				// log_message("debug", "payroll shift start date is invalid");
				return 0;
			}else if($payroll_date >= $date && $payroll_date < $date ){
				$start_time = $payroll_date;
			}
				
			$start_day_no = intval(floor(($date / 86400))) - intval(floor(($payroll_date / 86400)));
			$remainder = $start_day_no % $rolldays;
			$start = $remainder * 24;
				
			
			$query = $this->db->query("SELECT MIN(slot.start_time) AS start_time, MAX(slot.end_time) AS end_time , roll.name AS wday, shift.lunch_hour
										FROM st_payrollshift_rolls AS roll, st_shift_category AS cat, st_daily_shift_slot AS slot, st_daily_shift AS shift
										WHERE roll.payrollshift_id = " . $payroll->id ."
										AND roll.start = " . $start ."
										AND roll.daily_shift_id = slot.daily_shift_id
										AND roll.daily_shift_id = shift.id
										AND cat.total IN (2,3) AND slot.shift_category_code = cat.code 
										GROUP BY roll.start");
			
		
/* 			$query = $this->db->query("SELECT ps.*,dss.* 
										FROM st_user_labor ul
										LEFT JOIN st_payrollshift_rolls ps ON ps.payrollshift_id=ul.`payrollshift_id`
										LEFT JOIN st_daily_shift_slot dss ON dss.`daily_shift_id`=ps.`daily_shift_id`
										WHERE ul.user_id=".$user_id." AND ul.status=1  AND ul.id=".$labor->id." AND dss.shift_category_code IN (2,3,4,6,7)"); */
		
		}
		
		return $query->row();		
	}
	
	/**
	 * delete all slot which daily shift id is $daily_shift_id
	 * @param int $daily_shift_id
	 */
	function delete_by_daily_shift($daily_shift_id){
		$where = array(
			'daily_shift_id' => $daily_shift_id
		);
		return $this->delete_by($where);
	}
    
}