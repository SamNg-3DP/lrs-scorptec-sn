<?php
class St_user_log_m extends MY_Model {

	/**
	 * create a user log
	 * @param $input
	 * @return integer
	 */
	function add($input = array())
	{
		return parent::insert(array(
        	'log_time'	=> now(),
        	'user_name'	=> $input['user_name'],
        	'log_info'	=> $input['log_info'],
        	'ip' 		=> $this->input->ip_address()
       ));
	}
	
	/**
	 * get user login logs
	 */
	function get_logs($from_date=0, $to_date=0, $page_no=1, $limit=PAGE_SIZE){
		$sql = " SELECT log.*, user.first_name, user.last_name " .
    			" FROM st_user_log AS log, st_users AS user " .
    			" WHERE log.user_name = user.user_name";
    			
		if($from_date > 0){
			$sql .= " AND log.log_time >= " . $from_date;
		}
		
		if($to_date > 0){
			$sql .= " AND log.log_time < " . $to_date;
		}
		
		$sql .= " ORDER BY log.log_time desc";
    	if(empty($page_no)){
			$page_no = 1;
		}
		
		if(!empty($limit)){
			$start = ($page_no - 1) * $limit;
			$sql .= " LIMIT " . $start . ", " . $limit;
		}
		
		log_message("debug", "get_logs <<< " . $sql);
    	$query = $this->db->query($sql);
    	return $query->result();
	}
	
	/**
	 * get count of logs
	 */
	function count_logs($from_date=0, $to_date=0){
		$sql = " SELECT count(log.id) AS num " .
    			" FROM st_user_log AS log, st_users AS user " .
    			" WHERE log.user_name = user.user_name";
    			
		if($from_date > 0){
			$sql .= " AND log.log_time >= " . $from_date;
		}
		
		if($to_date > 0){
			$sql .= " AND log.log_time < " . $to_date;
		}
		
		$total_count = 0;
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0){
			$row = $query->row();
			$total_count = $row->num;
		}
		log_message("debug", "count_requests_by_state <<< " . $sql);
        return $total_count;
	}
	
}

/* End of file muserlog.php */
/* Location: ./system/application/models/muserlog.php */