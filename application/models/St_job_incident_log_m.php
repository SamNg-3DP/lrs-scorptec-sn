<?php

/**
 * job incident log model class
 * @author Bai Yinbing
 */
class St_job_incident_log_m extends MY_Model {
	
	function get_by_search($request_id, $operator_first_name, $operator_last_name, $operate_start_date, $operate_end_date, $operation, $page_no=1, $limit = PAGE_SIZE ){
		$sql = "SELECT log.*, user.user_name
    				, user.first_name AS operator_first_name, user.last_name AS operator_last_name
    			FROM st_job_incident_log AS log , st_users AS user
    			WHERE log.operator_id = user.id";
		 
		if(!empty($request_id)){
			$sql .= " AND log.id = " . $request_id;
		}
	
		if(!empty($operator_first_name)){
			$sql .= " AND user.first_name like '%" . $operator_first_name. "%'";
		}
	
		if(!empty($operator_last_name)){
			$sql .= " AND user.last_name like '%" . $operator_last_name. "%'";
		}
	
		if(!empty($operate_start_date) && $operate_start_date > 0){
			$sql .= " AND log.operate_time >= " . $operate_start_date;
		}
	
		if(!empty($operate_end_date) && $operate_end_date > 0){
			$sql .= " AND log.operate_time < " . $operate_end_date;
		}
	
		if(!empty($operation)){
			$sql .= " AND log.operation = '" . $operation . "'";
		}
	
		$sql .= " ORDER BY log.log_id Desc";
	
		if(empty($page_no)){
			$page_no = 1;
		}
	
		if(!empty($limit)){
			$start = ($page_no - 1) * $limit;
			$sql .= " LIMIT " . $start . ", " . $limit;
		}
	
		log_message("debug", "get_by_search sql <<< ".$sql);
	
		$query = $this->db->query($sql);
		return $query->result();
	}	
    
}