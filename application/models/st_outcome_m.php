<?php

/**
 * outcome class
 * @author Bai Yinbing
 */
class St_outcome_m extends MY_Model {
	
	/**
	 * get working hours dropdown list
	 * @return array
	 */
	function get_working_hours_select_dropdown(){
		$this->db
    		->where('status', ACTIVE)
    		->where('type', 1);
		$outcomes = $this->get_all();
		$a_ret = array();
		$a_ret[''] = 'Please select';
		foreach ($outcomes as $row){
			$a_ret[$row->id] = $row->outcome;
		}
		return $a_ret;
	}
    
	/**
	 * get leave balance dropdown list
	 * @return array
	 */
	function get_leave_balance_select_dropdown(){
		$this->db
    		->where('status', ACTIVE)
    		->where('type', 2);
		$outcomes = $this->get_all();
		$a_ret = array();
		$a_ret[''] = 'Please select';
		foreach ($outcomes as $row){
			$a_ret[$row->id] = $row->outcome;
		}
		return $a_ret;
	}
	
	/**
	 * get leave type dropdown list
	 * @return array
	 */
	function get_dropdown(){
		$this->db
    		->where('status', ACTIVE)
    		->order_by('leave_type', 'ASC');
		$leave_types = $this->get_all();
		$a_ret = array();
		$a_ret[''] = 'All';
		foreach ($leave_types as $row){
			$a_ret[$row->id] = $row->leave_type;
		}
		return $a_ret;
	}
	
    /**
     * set active status for specail leave type
     * @param $leave_type_id
     * @return boolean
     */
    function set_active($leave_type_id)
    {
    	return $this->update($leave_type_id, 
    			array('status' => ACTIVE)
    	);
    }
    
    /**
     * set in-active status for specail leave type
     * @param $leave_type_id
     * @return boolean
     */
	function set_inactive($leave_type_id)
	{
    	return $this->update($leave_type_id,
    			array('status' => IN_ACTIVE)
    	);
    }   
}