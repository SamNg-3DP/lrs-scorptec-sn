<?php

class St_shift_category_m extends MY_Model {
	
	/**
	 * get shift category dropdown list
	 * @return array
	 */
	function get_select_dropdown(){
		$this->db
    		->order_by('code', 'ASC');
		$leave_types = $this->get_all();
		$a_ret = array();
		$a_ret[''] = 'Please select..';
		foreach ($leave_types as $row){
			$a_ret[$row->code] = $row->description;
		}
		return $a_ret;
	}
	
	function add($code, $description, $color, $total) {
		$insert = array(
			'code' => $code,
			'description' => $description,
			'color' => $color,
			'total' => $total
		);
        //$this->db->query("INSERT INTO st_shift_category VALUES('$code', '$description', '$color', '$total');");
        return $this->insert($insert); 
	}
	
	function get_shiftcategory() {
		$query = $this->db->query('SELECT * FROM st_shift_category ORDER BY code;');
		return $query->result();
	}
	
	function delete1($code) {
        $this->db->query("DELETE FROM st_shift_category WHERE code = '$code';"); 
	}
	
	/**
	 * check whether code is unique except the special id
	 * @param $code
	 * @param $id
	 * @return int
	 */
	function check_except($code, $id){
		$sql = "SELECT * FROM st_shift_category 
			WHERE id != " . $id . "
			AND code = '" . $code ."'";
		$query = $this->db->query($sql);
		log_message("debug", " check_except == ".$sql);
		return $query->num_rows();
	}
}