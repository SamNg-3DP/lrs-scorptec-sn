<?php

/**
 * leave type class
 * @author Bai Yinbing
 */
class St_leave_type_m extends MY_Model {
	
	/**
	 * get leave type dropdown list
	 * @return array
	 */
	function get_select_dropdown(){
		$this->db
    		->where('status', ACTIVE)
    		->order_by('leave_type', 'ASC');
		$leave_types = $this->get_all();
		$a_ret = array();
		$a_ret[''] = 'Please select';
		foreach ($leave_types as $row){
			$a_ret[$row->id] = $row->leave_type;
		}
		return $a_ret;
	}
    
	/**
	 * get leave type dropdown list
	 * @return array
	 */
	function get_dropdown(){
		$this->db
    		->where('status', ACTIVE)
    		->order_by('leave_type', 'ASC');
		$leave_types = $this->get_all();
		$a_ret = array();
		$a_ret['0'] = 'All';
		foreach ($leave_types as $row){
			$a_ret[$row->id] = $row->leave_type;
		}
		return $a_ret;
	}
	
	/**
	 * get leave type dropdown list which can be changed
	 * @return array
	 */
	function get_change_select_dropdown(){
		$this->db
			->where('id', LEAVE_TYPE_ANNUAL)
			->or_where('id', LEAVE_TYPE_PERSONAL)
    		->where('status', ACTIVE)
    		->order_by('leave_type', 'ASC');
		$leave_types = $this->get_all();
		$a_ret = array();
		$a_ret[''] = 'Please select';
		foreach ($leave_types as $row){
			$a_ret[$row->id] = $row->leave_type;
		}
		return $a_ret;
	}
	
    /**
     * set active status for specail leave type
     * @param $leave_type_id
     * @return boolean
     */
    function set_active($leave_type_id)
    {
    	return $this->update($leave_type_id, 
    			array('status' => ACTIVE)
    	);
    }
    
    /**
     * set in-active status for specail leave type
     * @param $leave_type_id
     * @return boolean
     */
	function set_inactive($leave_type_id)
	{
    	return $this->update($leave_type_id,
    			array('status' => IN_ACTIVE)
    	);
    }
    
	/**
     * get all the leave type data.
     * @return <type> 
     */
	function get_leave_types() {
		$sql = "SELECT leave_type.*, wk.outcome AS working_outcome, lv.outcome AS leave_outcome  
				FROM st_leave_type AS leave_type LEFT JOIN st_outcome AS wk ON 
					leave_type.working_hours_outcome_id = wk.id LEFT JOIN st_outcome AS lv ON leave_type.leave_balance_outcome_id = lv.id
				ORDER BY leave_type.leave_type ASC
				";
		log_message("debug", "get_leave_types<<< ".$sql);
		$query = $this->db->query($sql);
    	return $query->result();
	}
	
	/**
	 * get active leave type
	 * @return array
	 */
    function get_leave_types_active() {
		$this->db
    		->where('status', ACTIVE)
    		->order_by('leave_type', 'ASC');
		return $this->get_all();
	}
	
	/**
	 * check whether the name is unique by id
	 * @param $name
	 * @param $id
	 * @return bool
	 */
	function check_unique_name($name, $id){
		$sql = "SELECT count(*) AS num
				FROM " . $this->_table.
			"   WHERE leave_type = '" . $name ."' and id != " . $id;
		$count = 0;
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0){
			$row = $query->row();
			$count = $row->num;
		}
		return $count > 0?TRUE:FALSE;
	}
}