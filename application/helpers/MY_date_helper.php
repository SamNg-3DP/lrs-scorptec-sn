<?php

/**
 * "Employment Period" (the number of years and days from the original 
 * hire	date until today next to it, for example 2 Year, 5 days.
 * @access	public
 * @param	string	
 * @param	
 */
if ( ! function_exists('employment_period'))
{
	function employment_period($hire_date) {
		$ret = "";
		if($hire_date == 0){
			return "";
		}
		$end_date = now();
		$time = $end_date - $hire_date;
		if($time>=86400){
			$pday = $time / 86400;
			$preday = explode('.',$pday);
			$pday = $preday[0];
			if($pday >= 365){
				$pyear = floor($pday / 365);
				if($pyear > 1){
					$ret = $pyear . " Years ";
				}else{
					$ret = $pyear . " Year ";
				}
			}
			$pday = $pday % 365;
			if($pday > 1){
				$ret .= $pday . " days";
			}elseif($pday == 1){
				$ret .= $pday . " day";
			}
		}
		return $ret;
	}
}

if ( ! function_exists('hour_time'))
{
	function hour_time() {
		$start = array(
			"" => "Please select...",
			"07:00" => "7:00",
			"07:15" => "7:15",
			"07:30" => "7:30",
			"07:45" => "7:45",
			"08:00" => "8:00",
			"08:15" => "8:15",				
			"08:30" => "8:30",
			"08:45" => "8:45",
			"09:00" => "9:00",
			"09:15" => "9:15", 	
			"09:30" => "9:30",
			"09:45" => "9:45",
			"10:00" => "10:00",
			"10:15" => "10:15",
			"10:30" => "10:30",
			"10:45" => "10:45",
			"11:00" => "11:00",
			"11:15" => "11:15",
			"11:30" => "11:30",
			"11:45" => "11:45",
			"12:00" => "12:00",
			"12:15" => "12:15",
			"12:30" => "12:30",
			"12:45" => "12:45",
			"13:00" => "13:00",
			"13:15" => "13:15",
			"13:30" => "13:30",
			"13:45" => "13:45",
			"14:00" => "14:00",
			"14:15" => "14:15",
			"14:30" => "14:30",
			"14:45" => "14:45",
			"15:00" => "15:00",
			"15:15" => "15:15",
			"15:30" => "15:30",
			"15:45" => "15:45",
			"16:00" => "16:00",
			"16:15" => "16:15",
			"16:30" => "16:30",
			"16:45" => "16:45",
			"17:00" => "17:00",
			"17:15" => "17:15",
			"17:30" => "17:30",
			"17:45" => "17:45",
			"18:00" => "18:00",
			"18:15" => "18:15",
			"18:30" => "18:30",
			"18:45" => "18:45",
			"19:00" => "19:00",
			"19:15" => "19:15",
			"19:30" => "19:30",
			"19:45" => "19:45",
			"20:00" => "20:00",
			"20:15" => "20:15",
			"20:30" => "20:30",
			"20:45" => "20:45",
			"21:00" => "21:00",
			"21:15" => "21:15",
			"21:30" => "21:30",
			"21:45" => "21:45",
			"22:00" => "22:00",
			"22:15" => "22:15",
			"22:30" => "22:30",
			"22:45" => "22:45",
			"23:00" => "23:00",
			"23:15" => "23:15",
			"23:30" => "23:30",
			"23:45" => "23:45",
				
		);
		return $start;
	}
}

if ( ! function_exists('time_period'))
{
	function time_period() {
		$period = array(
			"" 				=> "All",
			"today"		 	=> "Today",
			"yesterday"	 	=> "Yesterday",
			"this_week" 	=> "This Week",
			"last_week" 	=> "Last Week",
			"this_month" 	=> "This Month",
			"last_month" 	=> "Last Month",
			"this_quarter" 	=> "This Quarter",
			"last_quarter" 	=> "Last Quarter",
			"this_year" 	=> "This Year",
			"last_year" 	=> "Last Year"
		);
		return $period;
	}
}

/**
 * Calculate Date Difference
 * usage:
 * 	$date1="07/11/2003";$date2="09/04/2004"; 
 * 	print "If we subtract " . $date1 . " from " . $date2 . " we get " . dateDiff("/", $date2, $date1) . ".";
 */
if ( ! function_exists('date_diff'))
{
	function date_diff($dformat, $endDate, $beginDate)
	{    
	   $date_parts1=explode($dformat, $beginDate);    
	   $date_parts2=explode($dformat, $endDate);    
	   $start_date=gregoriantojd($date_parts1[0], $date_parts1[1], $date_parts1[2]);          
	   $end_date=gregoriantojd($date_parts2[0], $date_parts2[1], $date_parts2[2]);    
	   return $end_date - $start_date;
	}
}

/**
 * Calculate Date Difference
 * usage: echo days_difference('2008-03-12','2008-03-09');
 */
if ( ! function_exists('days_difference'))
{
	function daysDifference($endDate, $beginDate)
	{
	   //explode the date by "-" and storing to array
	   $date_parts1=explode("-", $beginDate);
	   $date_parts2=explode("-", $endDate);
	   //gregoriantojd() Converts a Gregorian date to Julian Day Count
	   $start_date=gregoriantojd($date_parts1[1], $date_parts1[2], $date_parts1[0]);
	   $end_date=gregoriantojd($date_parts2[1], $date_parts2[2], $date_parts2[0]);
	   return $end_date - $start_date;
	}
}


/**
 * get this week start time and end time
 * return array
 */
if ( ! function_exists('today'))
{
	function today()
	{
		$start_time = strtotime(date("Y-m-d")." 00:00:00");
		$end_time = strtotime(date("Y-m-d")." 23:59:59");
		
		return array($start_time, $end_time );
	}
}

if ( ! function_exists('yesterday'))
{
	function yesterday()
	{
		$start_time = strtotime(date("Y-m-d")." 00:00:00") - 86400;
		$end_time = strtotime(date("Y-m-d")." 23:59:59") - 86400;

		return array($start_time, $end_time );
	}
}

/**
 * get this week start time and end time
 * return array
 */
if ( ! function_exists('this_week'))
{
	function this_week()
	{
		$t=getdate();
		$today=date('Y-m-d',$t[0]);
		//This Week//
	    $start=$t[0]-(86400 * $t['wday']);
	    $twstart=date('Y-m-d',$start);
	    $start_time = strtotime($twstart);
	    $end_time =$t[0];
		return array($start_time, $end_time );
	}
}

/**
 * get last week start time and end time
 * return array
 */
if ( ! function_exists('last_week'))
{
	function last_week()
	{
		$t=getdate();
		//This Week//
	    $start=$t[0]-(86400*$t['wday']);
	    //Last Week//
	    $lwstart=$start-604800;
	    $lwend=$lwstart+518400;
	    $lwstart=date('Y-m-d',$lwstart);
	    $lwend=date('Y-m-d',$lwend);
	    $start_time = strtotime($lwstart);
	    $end_time = strtotime($lwend . " 23:59:59");
		return array($start_time, $end_time );
	}
}

/**
 * get last week start time and end time
 * return array
 */
if ( ! function_exists('this_month'))
{
	function this_month()
	{
		$t=getdate();
		//This Month//
    	$tmstart=$t['year']. "-" .$t['mon'] . "-01";
    	
	    $start_time = strtotime($tmstart);
	    $end_time = $t[0];
		return array($start_time, $end_time );
	}
}

/**
 * get last month start time and end time
 * return array
 */
if ( ! function_exists('last_month'))
{
	function last_month()
	{
		$t=getdate();
		//Last Month//
	    if($t['mon']=="1"){
	    	$lmstart=($t['year'] - 1). "-12-01";
	    }
	    else {
	    	$lmstart=$t['year']."-".($t['mon']-1)."-01";
	    }
	    
	    $lmmonth=($t['mon']-1);
	    if($lmmonth=="4" OR $lmmonth=="5" OR $lmmonth=="9" OR $lmmonth=="11"){
	    	$lmend=$t['year']."-". $lmmonth . "-30";
	    }
	    elseif($lmmonth=="2"){
	    	if($t['year'] % 4 == 0){
	    		$lmend=$t['year']. "-" . $lmmonth. "-29";
	    	}else{
	    		$lmend=$t['year']. "-" . $lmmonth. "-28";
	    	}
	    }
	    else {
	    	$lmend=$t['year'] . "-" . $lmmonth . "-31";
	    }
    	
	    $start_time = strtotime($lmstart);
	    $end_time = strtotime($lmend . " 23:59:59");
		return array($start_time, $end_time );
	}
}

/**
 * get this quarter start time and end time
 * return array
 */
if ( ! function_exists('this_quarter'))
{
	function this_quarter()
	{
		$t=getdate();
		//This Quarter//
	    if($t['mon']=="1" OR $t['mon']=="2" OR $t['mon']=="3"){
	    	$tqstart="$t[year]-01-01";
	    	$tqend=$t['year']."-03-31";
	    }
	    elseif($t['mon']=="4" OR $t['mon']=="5" OR $t['mon']=="6"){
	    	$tqstart=$t['year']."-04-01";
	    	$tqend=$t['year']."-06-30";
	    }
	    elseif($t['mon']=="7" OR $t['mon']=="8" OR $t['mon']=="9"){
	    	$tqstart=$t['year']."-07-01";
	    	$tqend=$t['year']."-09-30";
	    }
	    else {
	    	$tqstart=$t['year']."-10-01";
	    	$tqend=$t['year']."-12-31";
	    }
    	
	    $start_time = strtotime($tqstart);
	    //$end_time = strtotime($tqend);
	    $end_time = $t[0];
		return array($start_time, $end_time );
	}
}

/**
 * get last quarter start time and end time
 * return array
 */
if ( ! function_exists('last_quarter'))
{
	function last_quarter()
	{
		$t=getdate();
		//Last Quarter//
    
	    if($t['mon']=="1" OR $t['mon']=="2" OR $t['mon']=="3"){
	    	$lqstart=($t['year']-1)."-10-01";
	    	$lqend=($t['year']-1)."-12-31";
	    }
	    elseif($t['mon']=="4" OR $t['mon']=="5" OR $t['mon']=="6") {
	    	$lqstart=$t['year']."-01-01";
	    	$lqend=$t['year']."-03-31";
	    }
	    elseif($t['mon']=="7" OR $t['mon']=="8" OR $t['mon']=="9"){
	    	$lqstart=$t['year']."-04-01";
	    	$lqend=$t['year']."-06-30";
	    }
	    else {
	    	$lqstart=$t['year']."-07-01";
	    	$lqend=$t['year']."-09-30";
	    }
    	
	    $start_time = strtotime($lqstart);
	    $end_time = strtotime($lqend . " 23:59:59");
		return array($start_time, $end_time );
	}
}

/**
 * get this year start time and end time
 * return array
 */
if ( ! function_exists('this_year'))
{
	function this_year()
	{
		$t=getdate();
		//Year To Date//
    	$ystart=$t['year']."-01-01";
    	
	    $start_time = strtotime($ystart);
	    $end_time = $t[0];
		return array($start_time, $end_time );
	}
}

/**
 * get last year start time and end time
 * return array
 */
if ( ! function_exists('last_year'))
{
	function last_year()
	{
		$t=getdate();
		//Year To Date//
	    $ystart=($t['year']-1)."-01-01";
	    
	    //Last Year//
	    $lyend=($t['year']-1)."-12-31 23:59:59";
    
	    $start_time = strtotime($ystart);
	    $end_time = strtotime($lyend);
		return array($start_time, $end_time );
	}
}

/**
 * get time period start time and end time
 * @param $type
 * return array() index 0: start time, index 1: end time
 */
if ( ! function_exists('get_time_period'))
{
	function get_time_period($type)
	{
		if($type == "this_week"){
			return this_week();
		}else if($type == "last_week"){
			return last_week();
		}else if($type == "this_month"){
			return this_month();
		}else if($type == "last_month"){
			return last_month();
		}else if($type == "this_quarter"){
			return this_quarter();
		}else if($type == "last_quarter"){
			return last_quarter();
		}else if($type == "this_year"){
			return this_year();
		}else if($type == "last_year"){
			return last_year();
		}else if ($type == "today"){
			return today();
		}else if ($type == "yesterday"){
			return yesterday();
		}else{
			return array( 0,0 );
		}
	}
}

/**
 * subtract from or add  seconds or minutes or hours or days or months or weeks or years to a specified date and return the updated date
 * e.g date_add('d',7,$dateTime) add 7 days
 */
if ( ! function_exists('date_add'))
{
	function date_add($interval,$number,$dateTime) {
	    $dateTime = (strtotime($dateTime) != -1) ? strtotime($dateTime) : $dateTime;       
	    $dateTimeArr=getdate($dateTime);
	                
	    $yr=$dateTimeArr['year'];
	    $mon=$dateTimeArr['mon'];
	    $day=$dateTimeArr['mday'];
	    $hr=$dateTimeArr['hours'];
	    $min=$dateTimeArr['minutes'];
	    $sec=$dateTimeArr['seconds'];
	
	    switch($interval) {
	        case "s"://seconds
	            $sec += $number; 
	            break;
	
	        case "n"://minutes
	            $min += $number; 
	            break;
	
	        case "h"://hours
	            $hr += $number; 
	            break;
	
	        case "d"://days
	            $day += $number; 
	            break;
	
	        case "ww"://Week
	            $day += ($number * 7); 
	            break;
	
	        case "m": //similar result "m" dateDiff Microsoft
	            $mon += $number; 
	            break;
	
	        case "yyyy": //similar result "yyyy" dateDiff Microsoft
	            $yr += $number; 
	            break;
	
	        default:
	            $day += $number; 
		}       
	                
		$dateTime = mktime($hr,$min,$sec,$mon,$day,$yr);
//	        $dateTimeArr=getdate($dateTime);
//	        
//	        $nosecmin = 0;
//	        $min=$dateTimeArr['minutes'];
//	        $sec=$dateTimeArr['seconds'];
//	
//	        if ($hr==0){$nosecmin += 1;}
//	        if ($min==0){$nosecmin += 1;}
//	        if ($sec==0){$nosecmin += 1;}
//	        
//	        if ($nosecmin>2){     
//	        	return(date("Y-m-d",$dateTime));
//	        } else {     
//	        	return(date("Y-m-d G:i:s",$dateTime));
//	        }
		return $dateTime;
	} 
}

/* End of file MY_date_helper.php */
/* Location: ./system/applicatioin/helpers/MY_date_helper.php */