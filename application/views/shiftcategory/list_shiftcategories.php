<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Shift category list</title>
    <base href="<?=base_url()?>"/>
    <script type="text/javascript" src="js/piwik-lrs.js"></script>
</head>

<body>
<form id="form1" name="form1" method="post" action="">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" background="images/Footer.png">
        <tr>
            <td>
                <table width="100%" border="0" cellspacing="1" cellpadding="0">
                    <tr>
                        <td height="20" colspan="5" align="right" bgcolor="#EBEBEB"><a href="<?=site_url('shiftcategory/add')?>">ADD NEW</a></td>
                    </tr>
                    <tr style="background-image:url('images/TableHeader.png');">
                        <td width="16%" height="20">Code</td>
                        <td width="33%" height="20">Description</td>
                        <td width="12%" height="20">Color</td>
                        <td width="27%" height="20">Total</td>
                        <td width="12%" height="20">Action</td>
                    </tr>
                    <?php foreach($shiftcategory as $row): ?>
                    <tr>
                        <td height="20" bgcolor="#FFFFFF"><?=$row->code?></td>
                        <td height="20" bgcolor="#FFFFFF"><?=$row->description?></td>
                        <td height="20" bgcolor="<?=$row->color?>"></td>
                        <td height="20" align="center" bgcolor="#FFFFFF">
						<?
						if ($row->total == 0) {
							echo '-';
						} elseif ($row->total == 1) {
							echo '0';
						} else {
							echo '+';
						}
						?>
                        </td>
                        <td height="20" align="center" bgcolor="#FFFFFF">
                        <a href="<?=site_url('shiftcategory/edit/'.$row->id)?>"><img src="images/action/b_edit.png" width="16" height="16" border="0" /></a>&nbsp;
                        <a href="<?=site_url('shiftcategory/remove/'.$row->id)?>"><img src="images/action/b_drop.png" width="16" height="16" border="0" title="Remove"/></a></td>
                    </tr>
                    <?php endforeach;?>
                </table>
            </td>
        </tr>
    </table>
</form>
</body>
</html>
