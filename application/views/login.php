<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Leave Request System</title>
	<base href="<?=base_url()?>" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="text/javascript" src="js/script.js"></script>
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
</head>
<body background="images/Page-BgSimpleGradient.jpg">
<div class="PageBackgroundSimpleGradient">
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<table align="center" width="400" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><div class="sidebar2">
    <div class="Block">
        <div class="Block-tl"></div>
        <div class="Block-tr"></div>
        <div class="Block-bl"></div>
        <div class="Block-br"></div>
        <div class="Block-tc"></div>
        <div class="Block-bc"></div>
        <div class="Block-cl"></div>
        <div class="Block-cr"></div>
        <div class="Block-cc"></div>
        <div class="Block-body">
        	
<!--  			<?php 
                if(!empty($last_svn_update)){
                	echo "<div style='text-align: center;color:red;'>";
                	echo "    Last update time:" . $last_svn_update;
                	echo "</div>";
                }?>
-->			
            <div class="BlockHeader">
                <div class="l"></div>
                <div class="r"></div>
                <div class="t">Welcome to Leave Request System!</div>
            </div>
            </div><div class="BlockContent">
                <div class="BlockContent-body">
                	<?php
                	echo $this->session->flashdata('auth');
                	echo validation_errors();
					echo form_open('login/submit', array('id'=>'login'));
					echo form_hidden("back_url", $this->session->flashdata('back_url'));
					?>
                        <table width="400" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="150" align="right">Username:&nbsp;&nbsp;</td>
                            <td width="220"><input type="text" name="user_name" style="width: 60%;" value="<?php echo set_value('username'); ?>"/></td>
                          </tr>
                          <tr><td colspan="2" >&nbsp;</td></tr>
                          <tr>
                            <td align="right">Password:&nbsp;&nbsp;</td>
                            <td><input type="password" name="password" style="width: 60%;" value="<?php echo set_value('password'); ?>"/></td>
                          </tr>
                          <tr>
                            <td colspan="2" align="center">
                            	<button class="Button" type="submit" name="submit">
                                <span class="btn">
                                    <span class="l"></span>
                                    <span class="r"></span>
                                    <span class="t">Login</span>
                                </span>
                                </button>
                            </td>
                          </tr>
                        </table>
                    <?php
					echo form_close();
					?>
                </div>
            </div>
        </div>
    </div></td>
  </tr>
</table>
</div>
</body>
</html>
