<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>QB Payroll</title>
<base href="<?=base_url ()?>" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />
<script type="text/javascript" src="js/jquery-1.3.2.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type='text/javascript' src='js/jquery.qtip.js'></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
//<![CDATA[

function undo(user_id, payroll_data_id){
	$.ajax({
		async: false,
		type: 'POST',
		dataType: 'json',
		url: "<?=site_url("payroll/undo_payroll_ajax")?>",
		data: {
		  	payroll_data_id: payroll_data_id
		},
		success: function(data){
			if(data.status){
				$("#undo"+payroll_data_id).html('Process..');
			}
	  	}
	});
}

function qb_export(user_id, payroll_data_id){
	$.ajax({
		async: false,
		type: 'POST',
		dataType: 'json',
		url: "<?=site_url("payroll/qb_export_payroll_ajax")?>",
		data: {
		  	payroll_data_id: payroll_data_id
		},
		success: function(data){
			if(data.status){
				$("#qb"+payroll_data_id).html('QB Exported..');
			}
	  	}
	});
}

//]]>
</script>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="90%">
        	<table width="100%" border="0" cellspacing="1" cellpadding="1">
            	<tr bgcolor="#FFFFFF">
            		<td align="left" >
            		Payroll ID:&nbsp;<?=$payroll_id?>&nbsp;&nbsp;&nbsp;
					</td>
                </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="20" style="text-decoration:underline"><strong></strong></td>
                </tr>
            </table>
            <form id="import_csv" name="import_csv" method="post" action="<?php echo site_url('payroll/do_import_csv/')?>">
            <input type="hidden" name="payroll_id" value="<?=$payroll_id?>" />
            <table width="100%" border="0" cellpadding="0" cellspacing="0" background="images/Footer.png">
                <tr>
                    <td>                    	
                        <table width="100%" border="0" cellspacing="1" cellpadding="1" id="list_data">
                            <tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                                <td width="200">Staff Name</td>
                                <td width="190">Current Annual Leave Balance</td>
                                <td width="190">Current Personal Leave Balance</td>
                                
                                <td width="190">New Annual Leave Balance</td>
                                <td width="190">New Personal Leave Balance</td>
                            </tr>
                            <? 
							$num = 0;
							if(count($payrolldatas) > 0){
							
							foreach ($payrolldatas as $row): 
							?>
							<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
							    <td><? echo $row->user_name;?></td>
							    <td><?=$row->payroll_annual_leave_end?></td>
							    <td><?=$row->payroll_personal_leave_end?></td>
							    
							    <td id="annual_leave_end"><input type="text" name="annual[<?=$row->id?>]" value="<?=$row->annual_leave_end?>" /></td>
							    <td id="personal_leave_end"><input type="text" name="personal[<?=$row->id?>]"  value="<?=$row->personal_leave_end?>" /></td>
							</tr>
							<?
							$num++;
							endforeach;
							?>
							<tr><td><input type="submit" value="Import Data" /></td></tr>
							
							<?
							}else{
							?>
							<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
							    <td height="20" colspan="11">All data had been imported</td>
							</tr>
							<?php }
							?>
							
                        </table>
                    </td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
</table>
</body>
</html>