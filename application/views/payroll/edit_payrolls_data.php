                            <tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                            	<td width="30" height="20">Staff ID</td>
                                <td width="70">Staff Name</td>
                                <td width="90">Total Working hours</td>
                                <td width="90">Accrued Annual Leave</td>
                                <td width="90">Accrued Personal Leave</td>
                                <td width="90">Annual Leave Used</td>
                                <td width="90">Personal Leave Used</td>
                                <td width="190">Total Annual Leave Balance at the End of Payroll</td>
                                <td width="190">Total Personal Leave Balance at the End of Payroll</td>
                                <td width="20">Salary</td>
                                <td colspan="2" align="center">Action</td>
                            </tr>
                            <? 
							$num = 0;
							if(!empty($payrolldatas)){
							foreach ($payrolldatas as $row): 
							?>
							<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
							    <td height="20"><?=$row->user_id?></td>
							    <td><? echo $row->first_name . " " . $row->last_name;?></td>
							    <td id="total_working_hours<?=$row->id?>"><?=$row->total_working_hours?></td>
							    <td id="accrued_annual_leave<?=$row->id?>"><?=$row->accrued_annual_leave?></td>
							    <td id="accrued_personal_leave<?=$row->id?>"><?=$row->accrued_personal_leave?></td>
							    <td id="annual_leave_used<?=$row->id?>"><?=$row->annual_leave_used?></td>
							    <td id="personal_leave_used<?=$row->id?>"><?=$row->personal_leave_used?></td>
							    <td id="annual_leave_end<?=$row->id?>"><?=$row->annual_leave_end?></td>
							    <td id="personal_leave_end<?=$row->id?>"><?=$row->personal_leave_end?></td>
							    <td id="salary<?=$row->id?>"><?=$row->salary?></td>
							    <td class="request" pdid="<?=$row->id?>" uid="<?=$row->user_id?>" name="<? echo $row->first_name . ' ' . $row->last_name;?>">Reqeuests</td>
							    <td>
									<p id="process<?=$row->id?>"><a onClick="process(<?=$row->user_id?>,<?=$row->id?>)" href="javascript:void(0)" >Process..</a></p>
							    </td>
							</tr>							
							<? 
							$num++;
							endforeach;
							?>
							<tr bgcolor="#FFFFFF" font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
					            <td align="right" colspan="12">
					            <?php 
					            	$space = "&nbsp;&nbsp;&nbsp;";
					
					        		if($page_count > 1){
					//                            		if($page_count > 1){
					//                                		echo $page_no ." / " . $page_count.$space;
					//                                	}
					//                                	if($page_no == 1){
					//                                		echo "First";
					//                                	}else{
					//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(1);'>First</a>";
					//                                	}
					                	echo $space;
					                	if($page_no > 1){
					                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . ($page_no - 1) . ");'>Pre.</a>";
					                	}else{
					                		echo "Pre.";
					                	}
					                	echo $space;
					                	if($page_no + 1 <= $page_count){
					                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . ($page_no + 1) . ");'>Next</a>";
					                	}else{
					                		echo "Next";
					                	}
					//                                	echo $space;
					//                                	if($page_no == $page_count || $page_count == 0){
					//                                		echo "Last";
					//                                	}else{
					//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . $page_count . ")'>Last</a>";
					//                                	}                            			
					        			echo $space. "GoTo:";
					        			$page_dropdown_id = $form_id.'_page_dropdown';
					                    $js = 'id="'.$page_dropdown_id.'" style="font-size: 11px;" onChange="gotopage(\''.$form_id.'\', this.value)"';
					                    $page_dropdown = array();
					                    for($i = 1; $i <= $page_count; $i++){
					                    	$page_dropdown[$i] = $i;
					                    }
										echo form_dropdown($page_dropdown_id, $page_dropdown, $page_no, $js);
					            	}
					            	echo $space. "Items Per Page:";
					            	$page_limit_id = $form_id.'_limit';
					            	$js = 'id="'.$page_limit_id.'" style="font-size: 11px;" onChange="pagelimit(\''.$form_id.'\')"';
					            	echo form_dropdown($page_limit_id, pagination_option(), $page_limit, $js);
					            	
					            ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					            </td>
					        </tr>
<script type="text/javascript">
//<![CDATA[

$(function() {
	$('table td.request:not(:empty)').each(function()
	{
	   var self = this;   
	   // Setup title   
	   var title = $(self).attr('name');
	   var uid = $(self).attr('uid');
	   var pdid = $(self).attr('pdid');
	   
	   $(self).qtip({
	      content: {
	         text: "<img src='images/loading.gif'/>",
	         url: requesturl,
	         data: { 
	         	id: uid,
	         	payroll_data: pdid,
	         	start_date: start_date,
	         	end_date: end_date 
	         },
	         method: 'post',
	         title: {
	            text: title,
	            button: 'Close'
	         }
	      },
	      position: {
	         corner: {
	            target: 'leftMiddle',
	            tooltip: 'rightMiddle'
	         }
	      },
	      show: 'click',
	      hide: 'unfocus',//'inactive',
	      style: {
	         border: {
	            width: 2
	         },
	         tip: {
	            corner: 'rightMiddle'
	         },
	         name: 'green',
	         width: {
	            max: 650,
	            min: 500
	         },
	         padding: 14
	      }
	   })
	   .html('<a href="javascript:void(0)">Requests</a>')
	   //.html('Requests')
	   .show();
	});
});
//]]>
</script>					        
					        
							<?
							}else{
							?>
							<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
							    <td height="20" colspan="12">All records had been processed.</td>
							</tr>
							<?php }
							?>