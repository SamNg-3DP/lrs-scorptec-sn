<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Adjustment Payroll</title>
<base href="<?=base_url ()?>" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />
<script type="text/javascript" src="js/jquery-1.3.2.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type='text/javascript' src='js/jquery.qtip.js'></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
//<![CDATA[
var payroll_id = "<?=$payroll->id?>";
var start_date = "<?=$payroll->start_date?>";
var end_date = "<?=$payroll->end_date?>";
var requesturl = "<?=site_url('payroll/get_requests_by_update')?>";

function qb_export(user_id, payroll_data_id){
	$.ajax({
		async: false,
		type: 'POST',
		dataType: 'json',
		url: "<?=site_url("payroll/qb_export_payroll_ajax")?>",
		data: {
		  	payroll_data_id: payroll_data_id
		},
		success: function(data){
			if(data.status){
				$("#qb"+payroll_data_id).html('QB Exported..');
			}
	  	}
	});
}

function check(){
	var user_count = $("#user_count").val();
	if(!user_count || user_count == 0){
		return false;
	}
	return true;
}

function process(user_id,payroll_data_id){
	$.ajax({
		async: false,
		type: 'POST',
		url: "<?=site_url("payroll/process_payroll_ajax")?>",
		data: {
		  	user_id: user_id,
		  	payroll_id: payroll_id,
		  	start_date: start_date,
		  	end_date: end_date,
		  	payroll_data_id: payroll_data_id
		},
		success: function(data){
			if(data.status){
				$("#total_working_hours"+payroll_data_id).html(data.total_working_hours);
				$("#accrued_annual_leave"+payroll_data_id).html(data.accrued_annual_leave);
				$("#accrued_personal_leave"+payroll_data_id).html(data.accrued_personal_leave);
				$("#annual_leave_used"+payroll_data_id).html(data.annual_leave_used);
				$("#personal_leave_used"+payroll_data_id).html(data.personal_leave_used);
				$("#annual_leave_end"+payroll_data_id).html(data.annual_leave_end);
				$("#personal_leave_end"+payroll_data_id).html(data.personal_leave_end);
				$("#salary"+payroll_data_id).html(data.salary);
				//$("#process"+payroll_data_id).html('<a onClick="undo('+ user_id +','+payroll_data_id+')" href="javascript:void(0)" >Undo</a>');
				$("#process"+payroll_data_id).html('Processed');
			}
	  	},
		dataType: 'json'
	});
}

function undo(user_id, payroll_data_id){
	$.ajax({
		async: false,
		type: 'POST',
		dataType: 'json',
		url: "<?=site_url("payroll/undo_payroll_ajax")?>",
		data: {
		  	payroll_data_id: payroll_data_id
		},
		success: function(data){
			if(data.status){
				//alert($("#process"+payroll_data_id).html());
				//alert('<a onClick="process('+ user_id +','+payroll_data_id+')" href="javascript:void(0)" >Process..</a>');
				$("#process"+payroll_data_id).html('<a onClick="process('+ user_id +','+payroll_data_id+')" href="javascript:void(0)" >Process..</a>');
			}
	  	}
	});
}

function changeRequest(id, user_id, payroll_data_id){
	var leave_type = $("#leave_type_id" + id); //leave_type_id16
	if(leave_type.length > 0){
		var leave_type_value = leave_type.val();
		$.ajax({
			async: false,
			type: 'POST',
			dataType: 'json',
			url: "<?=site_url("payroll/update_payroll_request_ajax")?>",
			data: {
				user_id: user_id,
			  	id: id, //request id
			  	leave_type: leave_type_value,
			  	payroll_id: payroll_id,
			  	start_date: start_date,
			  	end_date: end_date,
			  	payroll_data_id: payroll_data_id
			},
			success: function(data){
				$("#total_working_hours"+payroll_data_id).html(data.total_working_hours);
				$("#accrued_annual_leave"+payroll_data_id).html(data.accrued_annual_leave);
				$("#accrued_personal_leave"+payroll_data_id).html(data.accrued_personal_leave);
				$("#annual_leave_used"+payroll_data_id).html(data.annual_leave_used);
				$("#personal_leave_used"+payroll_data_id).html(data.personal_leave_used);
				$("#annual_leave_end"+payroll_data_id).html(data.annual_leave_end);
				$("#personal_leave_end"+payroll_data_id).html(data.personal_leave_end);
				$("#salary"+payroll_data_id).html(data.salary);
		  	}
		});
	}
}

function gotopage(form, pageNo){
	$("#" + form + "_page_no").val(pageNo);
	submitForm(form);
}

function pagelimit(form){
	$("#" + form + "_page_no").val(1);
	submitForm(form);
}

function submitForm(form_id){
	if(form_id == "payroll"){
		var payroll_options = {
			dataType:  'json',
			success: payroll_response,
			url: "<?=site_url('payroll/get_edit_payrolls_ajax')?>"
		};
		$('#'+form_id).ajaxSubmit(payroll_options);
	}

	return false;
}

function payroll_response(responseText, statusText, xhr, $form){
	$("#list_payroll").find("tr:gt(0)").remove();
	$('#list_payroll tr:last').after(responseText.content);
	$("#list_payroll").find("tr:eq(0)").remove();
}

function payroll_page(pageNo){
	$("#payroll_page_no").val(pageNo);
	submitForm("payroll");
}

$(function() {
	$('table td.request:not(:empty)').each(function()
	{
	   var self = this;
	   // Setup title
	   var title = $(self).attr('name');
	   var uid = $(self).attr('uid');
	   var pdid = $(self).attr('pdid');

	   $(self).qtip({
	      content: {
	         text: "<img src='images/loading.gif'/>",
	         url: requesturl,
	         data: {
	         	id: uid,
	         	payroll_data: pdid,
	         	start_date: start_date,
	         	end_date: end_date
	         },
	         method: 'post',
	         title: {
	            text: title,
	            button: 'Close'
	         }
	      },
	      position: {
	         corner: {
	            target: 'leftMiddle',
	            tooltip: 'rightMiddle'
	         }
	      },
	      show: 'click',
	      hide: 'unfocus',//'inactive',
	      style: {
	         border: {
	            width: 2
	         },
	         tip: {
	            corner: 'rightMiddle'
	         },
	         name: 'green',
	         width: {
	            max: 650,
	            min: 500
	         },
	         padding: 14
	      }
	   })
	   .html('<a href="javascript:void(0)">Requests</a>')
	   //.html('Requests')
	   .show();
	});
});
//]]>
</script>
<script type="text/javascript" src="js/piwik-lrs.js"></script>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="90%">
        	<table width="100%" border="0" cellspacing="1" cellpadding="1">
            	<tr bgcolor="#FFFFFF">
            		<td align="left">
            		Payroll ID:&nbsp;<?=$payroll->id?>&nbsp;&nbsp;&nbsp;
            		Start Date:&nbsp;<?=date(DATE_FORMAT, $payroll->start_date)?>&nbsp;&nbsp;&nbsp;
            		&nbsp;&nbsp;&nbsp;End Date:&nbsp;
            		<?=date(DATE_FORMAT, $payroll->end_date)?>&nbsp;&nbsp;&nbsp;
            		Total:&nbsp;<?=$total_count?>
					</td>
                </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="20" style="text-decoration:underline"><strong></strong></td>
                </tr>
            </table>
            <form id="<?=$form_id?>" name="<?=$form_id?>" method="post" action="">
            <input type="hidden" id="<?=$form_id?>_page_no" name="<?=$form_id?>_page_no" value=""/>
            <input type="hidden" id="payroll_id" name="payroll_id" value="<?=$payroll_id?>"/>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" background="images/Footer.png">
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="1" cellpadding="1" id="list_<?=$form_id?>">
                            <tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                            	<td width="30" height="20">Staff ID</td>
                                <td width="70">Staff Name</td>
                                <td width="90">Total Working hours</td>
                                <td width="90">Accrued Annual Leave</td>
                                <td width="90">Accrued Personal Leave</td>
                                <td width="90">Annual Leave Used</td>
                                <td width="90">Personal Leave Used</td>
                                <td width="190">Total Annual Leave Balance at the End of Payroll</td>
                                <td width="190">Total Personal Leave Balance at the End of Payroll</td>
                                <td width="20">Salary</td>
                                <td colspan="2" align="center">Action</td>
                            </tr>
                            <?
							$num = 0;
							if(!empty($payrolldatas)){
							foreach ($payrolldatas as $row):
							?>
							<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
							    <td height="20"><?=$row->user_id?></td>
							    <td><? echo $row->first_name . " " . $row->last_name;?></td>
							    <td id="total_working_hours<?=$row->id?>"><?=$row->total_working_hours?></td>
							    <td id="accrued_annual_leave<?=$row->id?>"><?=$row->accrued_annual_leave?></td>
							    <td id="accrued_personal_leave<?=$row->id?>"><?=$row->accrued_personal_leave?></td>
							    <td id="annual_leave_used<?=$row->id?>"><?=$row->annual_leave_used?></td>
							    <td id="personal_leave_used<?=$row->id?>"><?=$row->personal_leave_used?></td>
							    <td id="annual_leave_end<?=$row->id?>"><?=$row->annual_leave_end?></td>
							    <td id="personal_leave_end<?=$row->id?>"><?=$row->personal_leave_end?></td>
							    <td id="salary<?=$row->id?>"><?=$row->salary?></td>
							    <td class="request" pdid="<?=$row->id?>" uid="<?=$row->user_id?>" name="<? echo $row->first_name . ' ' . $row->last_name;?>">Reqeuests</td>
							    <td>
									<p id="qb<?=$row->id?>"><a onClick="qb_export(<?=$row->user_id?>,<?=$row->id?>)" href="javascript:void(0)" >QB Export</a></p>
							    </td>
							</tr>
							<?
							$num++;
							endforeach;
							?>
							<!--
							<tr bgcolor="#FFFFFF" font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
					            <td align="right" colspan="12">
					            <?php
					            	$space = "&nbsp;&nbsp;&nbsp;";

					        		if($page_count > 1){
					//                            		if($page_count > 1){
					//                                		echo $page_no ." / " . $page_count.$space;
					//                                	}
					//                                	if($page_no == 1){
					//                                		echo "First";
					//                                	}else{
					//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(1);'>First</a>";
					//                                	}
					                	echo $space;
					                	if($page_no > 1){
					                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . ($page_no - 1) . ");'>Pre.</a>";
					                	}else{
					                		echo "Pre.";
					                	}
					                	echo $space;
					                	if($page_no + 1 <= $page_count){
					                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . ($page_no + 1) . ");'>Next</a>";
					                	}else{
					                		echo "Next";
					                	}
					//                                	echo $space;
					//                                	if($page_no == $page_count || $page_count == 0){
					//                                		echo "Last";
					//                                	}else{
					//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . $page_count . ")'>Last</a>";
					//                                	}
					        			echo $space. "GoTo:";
					        			$page_dropdown_id = $form_id.'_page_dropdown';
					                    $js = 'id="'.$page_dropdown_id.'" style="font-size: 11px;" onChange="gotopage(\''.$form_id.'\', this.value)"';
					                    $page_dropdown = array();
					                    for($i = 1; $i <= $page_count; $i++){
					                    	$page_dropdown[$i] = $i;
					                    }
										echo form_dropdown($page_dropdown_id, $page_dropdown, $page_no, $js);
					            	}
					            	echo $space. "Items Per Page:";
					            	$page_limit_id = $form_id.'_limit';
					            	$js = 'id="'.$page_limit_id.'" style="font-size: 11px;" onChange="pagelimit(\''.$form_id.'\')"';
					            	echo form_dropdown($page_limit_id, pagination_option(), PAGE_SIZE, $js);

					            ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					            </td>
					        </tr>-->
							<?
							}else{
							?>
							<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
							    <td height="20" colspan="12">All records had been processed.</td>
							</tr>
							<?php }
							?>
							<input type="hidden" id="user_count" name="user_count" value="<?=$num?>" />
                        </table>
                    </td>
                </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                </tr>
            </table>

            </form>
        </td>
    </tr>
</table>
</body>
</html>
