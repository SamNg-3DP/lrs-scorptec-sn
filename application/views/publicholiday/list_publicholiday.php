<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<base href="<?=base_url()?>"/>
<title>List Public Holiday</title>
<script type="text/javascript" src="js/piwik-lrs.js"></script>
</head>

<body>
<?php echo form_open ( 'publicholiday' ); ?>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" background="images/Footer.png">
    <tr>
      <td><table width="100%" border="0" cellspacing="1" cellpadding="0">
        <tr>
	   		<td colspan="2" align="center" bgcolor="#EBEBEB">Year:
				<?php
		            $js = 'id="years" style="font-size: 11px; width: 70px;" onchange="submit()"';
					echo form_dropdown("years", $years, $year, $js);
				?>
			</td>
	        <td height="20" colspan="9" align="right" bgcolor="#EBEBEB"><a href="<?=site_url('publicholiday/add')?>">ADD NEW</a></td>
        </tr>
        <tr style="background-image:url('images/TableHeader.png');">
    	    <td width="100">Date</td>
        	<td>Holiday Name</td>
        	<td colspan="3" align="center">Action</td>
        </tr>
        <? foreach ($publicholidays as $row): ?>
        <tr bgcolor="#FFFFFF">
        	<td ><?=date(DATE_FORMAT,$row->date)?></td>
        	<td ><?=$row->holiday_name?></td>
        	<td align="center">
          		<a href="<?php echo site_url("publicholiday/remove/".$row->id)?>"><img src="images/action/b_drop.png" alt="" width="16" height="16" border="0" title="Remove"/></a>
        	</td>
        </tr>
        <? endforeach; ?>
      </table></td>
    </tr>
  </table>
</form>
</body>
</html>
