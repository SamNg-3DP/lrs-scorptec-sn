<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add Public Holiday</title>
<base href="<?=base_url ()?>" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
//<![CDATA[



function check_unique(){
	var exist = false;
	$.ajax({
		async: false,
		type: 'POST',
		url: "<?=site_url("publicholiday/check_code_ajax")?>",
		data: {
			holiday_date: $("#holiday_date").val()
		},
		success: function(data){
			if(data.status == "exist"){
				exist = true;
			}
	  	},
		dataType: 'json'
	});

	if(exist){
		alert("Public Holiday already exist.");
		return false;
	}
}

function show_desc(value, index){
	if(value!=""){
		$.ajax({
			type: 'POST',
			url: "<?=site_url("dailyshift/get_daily_shift_ajax")?>",
			data: {
			  	id: value
			},
			success: function(data){
				$("#desc_"+index).html(data.description);
		  	},
			dataType: 'json'
		});
	}else{
		$("#desc_"+index).html("");
	}
}


$(function() {
    $("#holiday_date").datepicker({
    	disabled: true,
    	dateFormat: '<?=JS_DATE_FORMAT?>',
    	changeMonth: true,
		changeYear: true
	});
});

//]]>
</script>
<script type="text/javascript" src="js/piwik-lrs.js"></script>
</head>
<body>
<?php echo form_open ( 'publicholiday/do_add', 'onsubmit="return check_unique()"' ); ?>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" background="images/Footer.png">
    <tr>
      <td><table width="100%" border="0" cellspacing="1" cellpadding="0">
        <tr>
          <td height="20" colspan="3" bgcolor="#EBEBEB">&nbsp;</td>
        </tr>
        <tr>
          <td height="20" width="100" bgcolor="#FFFFFF">Holiday Name:</td>
		  <td bgcolor="#FFFFFF">
		  <?php
			$js =  'id="holiday_name" name="holiday_name" style="font-size: 11px;" ';
			echo form_dropdown("holiday_name", $holidays, false, $js);
           ?>
		  </td>
        </tr>
        <tr>
          <td height="20" bgcolor="#FFFFFF">Date:</td>
          <td height="20" bgcolor="#FFFFFF">
          	<label>
            <input type="text" name="holiday_date" id="holiday_date"
				style="font-size: 11px"
                onchange="javascript: change();"/>
          	</label>
          </td>
        </tr>
        <tr>
          <td height="20" colspan="3" align="center" bgcolor="#EBEBEB"><label>
            <input type="submit" name="button" id="button" value="     Save     " style="font-size:11px"/>
          </label></td>
        </tr>
      </table></td>
    </tr>
  </table>
</form>
</body>
</html>
