<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<base href="<?=base_url()?>"/>

<link rel="stylesheet" type="text/css" href="css/jquery.tools.tabs.css" /> 
<style type="text/css">
<!--
#form1 table tr td table {
	text-align: left;
}
#form1 p {
	text-align: left;
}
#form1 table tr td table tr td {
    font-family: Tahoma, Geneva, sans-serif;
	font-size: 11px;
}

/* tab pane styling */
div.panes div {
	display:none;		
	padding:15px 10px;
	border:1px solid #999;
	border-top:0;
	font-size:14px;
	background-color:#fff;
}
-->
</style>

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript">
$(function() { 
    // setup ul.tabs to work as tabs for each div directly under div.panes 
    $("ul.tabs").tabs("div.panes > div"); 
});
</script>

</head>

<body>
<!-- the tabs --> 
<ul class="tabs"> 
	<li><a href="#">Pending Approval (4)</a></li> 
    <li><a href="#">New (5)</a></li> 
    <li><a href="#">Not Approved (3)</a></li>
    <li><a href="#">Approved (7)</a></li> 
</ul> 
 
<!-- tab "panes" --> 
<div class="panes">
	<div>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
	      <tr>
	        <td width="90%"><table width="100%" border="0" cellspacing="0" cellpadding="0">
	          <tr>
	            <td height="20" style="text-decoration:underline"><strong>Pending Approval</strong></td>
	          </tr>
	        </table>
	          <table width="100%" border="0" cellpadding="0" cellspacing="0" background="images/Footer.png">
	          <tr>
	            <td><table width="100%" border="0" cellspacing="1" cellpadding="1">
	              <tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
	                <td width="29" height="20">#</td>
	                <td width="113">Request ID</td>
	                <td width="70">Applicant</td>
	                <td width="117">Start Date</td>
	                <td width="117">End Date</td>
	                <td width="100">Leave Type</td>
	                <td width="250">Reason</td>
	                <td width="117">Request Date</td>
	                <td width="30" height="20" align="center" style="color:#FFF">Action</td>
	              </tr>
	              <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
	                <td height="20">1</td>
	                <td>R032901</td>
	                <td>Bai</td>
	                <td>2010/03/10 9:00</td>
	                <td>2010/03/10 11:00</td>
	                <td>Personal</td>
	                <td>Shopping</td>
	                <td>2010/03/10 11:00</td>
	                <td align="center"><a href="index.php/request1/approve">approve</a></td>
	              </tr>
	              <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
	                <td height="20">2</td>
	                <td>R032902</td>
	                <td>Tom Liu</td>
	                <td>2010/03/10 9:00</td>
	                <td>2010/03/10 11:00</td>
	                <td>Personal</td>
	                <td>Tour</td>
	                <td>2010/03/10 11:00</td>
	                <td align="center"><a href="index.php/request1/approve">approve</a></td>
	              </tr>
	              <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
	                <td height="20">3</td>
	                <td>R032903</td>
	                <td>Jerry</td>
	                <td>2010/03/10 9:00</td>
	                <td>2010/03/10 11:00</td>
	                <td>Unpaid</td>
	                <td>Tour</td>
	                <td>2010/03/10 11:00</td>
	                <td align="center"><a href="index.php/request1/approve">approve</a></td>
	              </tr>
	              <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
	                <td height="20">4</td>
	                <td>R032904</td>
	                <td>James</td>
	                <td>2010/03/10 9:00</td>
	                <td>2010/03/10 11:00</td>
	                <td>Annual</td>
	                <td>Tour</td>
	                <td>2010/03/10 11:00</td>
	                <td align="center"><a href="index.php/request1/approve">approve</a></td>
	              </tr>
	            </table></td>
	          </tr>
	          <tr bgcolor="#FFFFFF" >
	          	<td align="right"><br />
	          		Total Records: 208&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">First</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">Previous</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">Next</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">Last</a>&nbsp;&nbsp;
	          	</td>
	          </tr>
	        </table>
	        </td>
	      </tr>
	    </table>
	  </div>
    <div>  
	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	      <tr>
	        <td width="90%"><table width="100%" border="0" cellspacing="0" cellpadding="0">
	          <tr>
	            <td height="20" style="text-decoration:underline"><strong>New</strong></td>
	          </tr>
	        </table>
	          <table width="100%" border="0" cellpadding="0" cellspacing="0" background="images/Footer.png">
	          <tr>
	            <td><table width="100%" border="0" cellspacing="1" cellpadding="1">
	              <tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
	                <td width="29" height="20">#</td>
	                <td width="113">Request ID</td>
	                <td width="70">Applicant</td>
	                <td width="117">Start Date</td>
	                <td width="117">End Date</td>
	                <td width="100">Leave Type</td>
	                <td width="250">Reason</td>
	                <td width="117">Request Date</td>
	                <td width="30" height="20" align="center" style="color:#FFF">Action</td>
	              </tr>
	              <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
	                <td height="20">1</td>
	                <td>R032901</td>
	                <td>Bai</td>
	                <td>2010/03/10 9:00</td>
	                <td>2010/03/10 11:00</td>
	                <td>Personal</td>
	                <td>Shopping</td>
	                <td>2010/03/10 11:00</td>
	                <td align="center"><a href="index.php/request1/index_add">view</a></td>
	              </tr>
	              <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
	                <td height="20">2</td>
	                <td>R032902</td>
	                <td>Tom Liu</td>
	                <td>2010/03/10 9:00</td>
	                <td>2010/03/10 11:00</td>
	                <td>Personal</td>
	                <td>Tour</td>
	                <td>2010/03/10 11:00</td>
	                <td align="center"><a href="index.php/request1/index_add">view</a></td>
	              </tr>
	              <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
	                <td height="20">3</td>
	                <td>R032903</td>
	                <td>Jerry</td>
	                <td>2010/03/10 9:00</td>
	                <td>2010/03/10 11:00</td>
	                <td>Unpaid</td>
	                <td>Tour</td>
	                <td>2010/03/10 11:00</td>
	                <td align="center"><a href="index.php/request1/index_add">view</a></td>
	              </tr>
	              <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
	                <td height="20">4</td>
	                <td>R032904</td>
	                <td>James</td>
	                <td>2010/03/10 9:00</td>
	                <td>2010/03/10 11:00</td>
	                <td>Annual</td>
	                <td>Tour</td>
	                <td>2010/03/10 11:00</td>
	                <td align="center"><a href="index.php/request1/index_add">view</a></td>
	              </tr>
	              <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
	                <td height="20">5</td>
	                <td>R032905</td>
	                <td>James</td>
	                <td>2010/03/10 9:00</td>
	                <td>2010/03/10 11:00</td>
	                <td>Annual</td>
	                <td>Tour</td>
	                <td>2010/03/10 11:00</td>
	                <td align="center"><a href="index.php/request1/index_add">view</a></td>
	              </tr>
	            </table></td>
	          </tr>
	          <tr bgcolor="#FFFFFF" >
	          	<td align="right"><br />
	          		Total Records: 208&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">First</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">Previous</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">Next</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">Last</a>&nbsp;&nbsp;
	          	</td>
	          </tr>
	        </table>
	        </td>
	      </tr>
	  </table>
	</div> 
    <div>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
	      <tr>
	        <td width="90%"><table width="100%" border="0" cellspacing="0" cellpadding="0">
	          <tr>
	            <td height="20" style="text-decoration:underline"><strong>Not Approved</strong></td>
	          </tr>
	        </table>
	          <table width="100%" border="0" cellpadding="0" cellspacing="0" background="images/Footer.png">
	          <tr>
	            <td><table width="100%" border="0" cellspacing="1" cellpadding="1">
	              <tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
	                <td width="29" height="20">#</td>
	                <td width="113">Request ID</td>
	                <td width="70">Applicant</td>
	                <td width="117">Start Date</td>
	                <td width="117">End Date</td>
	                <td width="100">Leave Type</td>
	                <td width="250">Reason</td>
	                <td width="117">Request Date</td>
	                <td width="30" height="20" align="center" style="color:#FFF">Action</td>
	              </tr>
	              <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
	                <td height="20">1</td>
	                <td>R032901</td>
	                <td>Bai</td>
	                <td>2010/03/10 9:00</td>
	                <td>2010/03/10 11:00</td>
	                <td>Personal</td>
	                <td>Shopping</td>
	                <td>2010/03/10 11:00</td>
	                <td align="center"><a href="index.php/request1/index_add">view</a></td>
	              </tr>
	              <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
	                <td height="20">2</td>
	                <td>R032902</td>
	                <td>Tom Liu</td>
	                <td>2010/03/10 9:00</td>
	                <td>2010/03/10 11:00</td>
	                <td>Personal</td>
	                <td>Tour</td>
	                <td>2010/03/10 11:00</td>
	                <td align="center"><a href="index.php/request1/index_add">view</a></td>
	              </tr>
	              <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
	                <td height="20">3</td>
	                <td>R032903</td>
	                <td>Jerry</td>
	                <td>2010/03/10 9:00</td>
	                <td>2010/03/10 11:00</td>
	                <td>Unpaid</td>
	                <td>Tour</td>
	                <td>2010/03/10 11:00</td>
	                <td align="center"><a href="index.php/request1/index_add">view</a></td>
	              </tr>
	            </table></td>
	          </tr>
	          <tr bgcolor="#FFFFFF" >
	          	<td align="right"><br />
	          		Total Records: 208&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">First</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">Previous</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">Next</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">Last</a>&nbsp;&nbsp;
	          	</td>
	          </tr>
	        </table>
	        </td>
	      </tr>
	  </table>
	</div> 
    <div>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
	      <tr>
	        <td width="90%"><table width="100%" border="0" cellspacing="0" cellpadding="0">
	          <tr>
	            <td height="20" style="text-decoration:underline"><strong>Approved</strong></td>
	          </tr>
	        </table>
	          <table width="100%" border="0" cellpadding="0" cellspacing="0" background="images/Footer.png">
	          <tr>
	            <td><table width="100%" border="0" cellspacing="1" cellpadding="1">
	              <tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
	                <td width="29" height="20">#</td>
	                <td width="113">Request ID</td>
	                <td width="70">Applicant</td>
	                <td width="117">Start Date</td>
	                <td width="117">End Date</td>
	                <td width="100">Leave Type</td>
	                <td width="250">Reason</td>
	                <td width="117">Request Date</td>
	                <td width="30" height="20" align="center" style="color:#FFF">Action</td>
	              </tr>
	              <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
	                <td height="20">1</td>
	                <td>R032901</td>
	                <td>Bai</td>
	                <td>2010/03/10 9:00</td>
	                <td>2010/03/10 11:00</td>
	                <td>Personal</td>
	                <td>Shopping</td>
	                <td>2010/03/10 11:00</td>
	                <td align="center"><a href="index.php/request1/index_add">view</a></td>
	              </tr>
	              <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
	                <td height="20">2</td>
	                <td>R032902</td>
	                <td>Tom Liu</td>
	                <td>2010/03/10 9:00</td>
	                <td>2010/03/10 11:00</td>
	                <td>Personal</td>
	                <td>Tour</td>
	                <td>2010/03/10 11:00</td>
	                <td align="center"><a href="index.php/request1/index_add">view</a></td>
	              </tr>
	              <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
	                <td height="20">3</td>
	                <td>R032903</td>
	                <td>Jerry</td>
	                <td>2010/03/10 9:00</td>
	                <td>2010/03/10 11:00</td>
	                <td>Unpaid</td>
	                <td>Tour</td>
	                <td>2010/03/10 11:00</td>
	                <td align="center"><a href="index.php/request1/index_add">view</a></td>
	              </tr>
	              <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
	                <td height="20">4</td>
	                <td>R032904</td>
	                <td>James</td>
	                <td>2010/03/10 9:00</td>
	                <td>2010/03/10 11:00</td>
	                <td>Annual</td>
	                <td>Tour</td>
	                <td>2010/03/10 11:00</td>
	                <td align="center"><a href="index.php/request1/index_add">view</a></td>
	              </tr>
	              <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
	                <td height="20">5</td>
	                <td>R032905</td>
	                <td>Tom Liu</td>
	                <td>2010/03/10 9:00</td>
	                <td>2010/03/10 11:00</td>
	                <td>Personal</td>
	                <td>Tour</td>
	                <td>2010/03/10 11:00</td>
	                <td align="center"><a href="index.php/request1/index_add">view</a></td>
	              </tr>
	              <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
	                <td height="20">6</td>
	                <td>R032906</td>
	                <td>Jerry</td>
	                <td>2010/03/10 9:00</td>
	                <td>2010/03/10 11:00</td>
	                <td>Unpaid</td>
	                <td>Tour</td>
	                <td>2010/03/10 11:00</td>
	                <td align="center"><a href="index.php/request1/index_add">view</a></td>
	              </tr>
	              <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
	                <td height="20">7</td>
	                <td>R032907</td>
	                <td>James</td>
	                <td>2010/03/10 9:00</td>
	                <td>2010/03/10 11:00</td>
	                <td>Annual</td>
	                <td>Tour</td>
	                <td>2010/03/10 11:00</td>
	                <td align="center"><a href="index.php/request1/index_add">view</a></td>
	              </tr>
	            </table></td>
	          </tr>
	          <tr bgcolor="#FFFFFF" >
	          	<td align="right"><br />
	          		Total Records: 208&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">First</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">Previous</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">Next</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">Last</a>&nbsp;&nbsp;
	          	</td>
	          </tr>
	        </table>
	        </td>
	      </tr>
	  </table>
	</div>
</div>
</body>
</html>