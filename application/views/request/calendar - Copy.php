<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<base href="<?=base_url()?>"/>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/qtip.css" />
<link rel='stylesheet' type='text/css' href='css/fullcalendar.css' />

<script type="text/javascript" src="js/jquery-1.3.2.js"></script>
<script type='text/javascript' src='js/jquery.qtip.js'></script>
<script type='text/javascript' src='js/fullcalendar.min.js'></script>




<script type='text/javascript'>
	function changeDept(value){
		var site = $("#site_id").val();
		if(site == ""){
			site = 0;
		}

		var leave = $("#leave_type_id").val();
		if(leave == ""){
			leave = 0;
		}

		var show_shift = $("#show_shift_id").val();
		
		window.location = "<?=site_url('calendar/department')?>/" + site + "/"+ value + "/" + leave + "/" + show_shift + "/"
	}

	function changeLeave(value){
		var site = $("#site_id").val();
		if(site == ""){
			site = 0;
		}

		var dept = $("#department_id").val();
		if(dept == ""){
			dept = 0;
		}

		var show_shift = $("#show_shift_id").val();
		
		window.location = "<?=site_url('calendar/department')?>/" + site + "/"+ dept + "/" + value + "/" + show_shift + "/"
	}

	function changeShow(value){
		var site = $("#site_id").val();
		if(site == ""){
			site = 1;
		}

		var dept = $("#department_id").val();
		if(dept == "0" && value == 1){
			dept = "Sales";
		}

		window.location = "<?=site_url('calendar/department')?>/" + site + "/"+ dept + "/" + 0 + "/" + value + "/"
	}	
	

	function change_site(value){
		var leave = $("#leave_type_id").val();
		if(leave == ""){
			leave = 0;
		}

		var show_shift = $("#show_shift_id").val();

		if (show_shift == 1 && value == 1){
			var dept = 'Sales';
		}else{
			var dept = 0;
		}
						
		
		window.location = "<?=site_url('calendar/department')?>/" + value + "/" + dept + "/" + leave + "/" + show_shift + "/"
	}
	

	$(document).ready(function() {
		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,basicWeek,basicDay'
			},
			//allDayDefault:false,
			editable: false,
			//viewDisplay: function(view) {
				//alert('The new title of the view is ' + view.title);
				//alert('The start of the view is ' + view.start);
				//alert('The end of the view is ' + view.end);
			//},			
			events: "<?=site_url('calendar/events/' . $site_id . '/'. $department_id . '/'. $leave_id . '/' . $show_shift  ).'/'?>"
			,eventClick: function(event) {
//		        if (event.url) {
		            //window.open(event.url);
//		        	view_request(event.id);
//		            return false;
//		        }
		    }
			,eventRender: function(event, element, view)
		    {
		        //alert(element.html());
		        element.qtip({ 
		        	content: event.description,
					style: { name: 'cream' }, //refer to $.fn.qtip.styles
		        	position: { adjust: { screen: true } }
		        });
		    }
			//,agenda: 'h:mm{ - h:mm}', // 5:00 - 6:30
		});
	});


	function view_request(id){
		
		var url = "<?=site_url('request/view')?>/" + id;
		show_view();
		$("#dialog-view").load(url, function() {
			//show_view();
	    });	
	}
	 function show_view(){
	 	$("#dialog-view").dialog("destroy");
	 	$("#dialog-view").dialog({
			modal: true,
			height: 500,
			width: 600,
			resizable: true
		});
	 }	

</script>
<style type='text/css'>

	body {
		margin-top: 40px;
		text-align: center;
		font-size: 14px;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		}

	#calendar {
		width: 900px;
		margin: 0 auto;
	}
	
	.cal_public_holiday { background-color: #0AFF0A; font-size: 14px; font-weight: bold; }
	.cal_public_holiday a { color: #2B2B2B; border: none; }
	
	.cal_shift { background-color: #B8FFB8; font-size: 12px; font-weight: bold;}
	.cal_shift a { color: #2B2B2B; border: none; }	
<?
foreach($leave_types as $leave){
	?>
	.cal_leave_<?=$leave->id?>{
	background-color: <?=$leave->color?>;
	font-size: 10px;
	}
<?}?>
	
</style>
</head>
<body>
<div align="right">
	<?php 
	echo '<label><a href="index.php/request/add" style="font-size:11px">ADD REQUEST</a></label><br/><br/>';
	
	
	echo "Show: ";
    $js = 'id="show_shift_id" style="font-size: 11px; width: 60px;" onChange="changeShow(this.value)"';
	echo form_dropdown('show_shift_id', array("Leave","Shift"), $show_shift, $js);
		
	echo " Site: "; 
    $js = 'id="site_id" style="font-size: 11px; width: 120px;" onChange="change_site(this.value)"';
	echo form_dropdown('site_id', $sites, $site_id, $js);
	
	echo " Dept: ";
	$js = 'id="department_id" style="font-size: 11px; width: 120px;" onchange="changeDept(this.value)"';
	echo form_dropdown('department_id', $departments, $department_id, $js);
	
	echo " Type: ";
	$js = 'id="leave_type_id" style="font-size: 11px; width: 120px;" onchange="changeLeave(this.value)"';
	echo form_dropdown('leave_type_id', $leave_types_id, $leave_id, $js);
	
	?>
</div>
	<br/>
<div id='calendar'></div>
<div id="dialog-view" title="View Request" style="display:none"> 
	<div style="margin: 100px auto; text-align: center;"><img border="0" src="images/loading.gif"/></div> 
</div>
</body>
</html>
