<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add a new leave request</title>
<base href="<?=base_url()?>" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />
<script type="text/javascript" src="js/mootools.v1.11.js"></script>
<!--  <script type="text/javascript" src="js/nogray_time_picker_min.js"></script>  -->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
//<![CDATA[
jQuery.noConflict();
/*
window.addEvent("domready", function (){
var tp = new TimePicker('start_time_picker', 'start_time', 'start_time_toggler', {format24:true});
new TimePicker('end_time_picker', 'end_time', 'end_time_toggler', {format24:true});
new TimePicker('arrival_time_picker', 'arrival_time', 'arrival_time_toggler', {format24:true});
});
*/
function count() {
	var request_full_day = document.getElementById('request_full_day');
	var request_partial_day = document.getElementById('request_partial_day');
	if(request_full_day.checked){
		var start_day = document.getElementById("start_day").value;
	    var end_day = document.getElementById("end_day").value;
	    if(start_day && end_day){
			var start = start_day.split('-');
			var dstart = new Date(start[2], start[1], start[0]);
			var end = end_day.split('-');
			var dend = new Date(end[2], end[1], end[0]);
			if(dstart.getTime() > dend.getTime()){
	        	alert("Start day shouldn't be later than end day.");
	        	return false;
			}
			calculate();
	    }
	}
	if(request_partial_day.checked){
		var start_day = document.getElementById("start_day").value;
	    var start_time = document.getElementById("start_time").value;
	    var end_time = document.getElementById("end_time").value;

	    if(start_day){
	        if(start_time && end_time && start_time > end_time){
	            alert("Start date shouldn't be later than end date.");
	            return false;
	        }
	        if(start_time && end_time && start_time == end_time){
	            alert("Leave time must be greater than zero!");
	            return false;
	        }
	        if(start_day && start_time && end_time){
		    	calculate();
		    }
	    }
	}

    return true;
}

function changeLeaveType(value){
    if(value == <?=LEAVE_TYPE_LATE?>
    	|| value == <?=LEAVE_TYPE_LUNCH?>
    	|| value == <?=LEAVE_TYPE_CLOCK?>){
        jQuery("#common_type").hide();
        jQuery("#medicalCertificate").hide();
        jQuery("#carer").hide();
        jQuery("#late_type").show();

        if (value == <?=LEAVE_TYPE_CLOCK?>){
        	jQuery("#minute_used").hide();
        }else{
        	jQuery("#minute_used").show();
        }
    }else{
    	jQuery("#common_type").show();
        jQuery("#late_type").hide();
        jQuery("#medicalCertificate").hide();
        jQuery("#carer").hide();
        jQuery("#minute_used").show();
    	if(value == "<?=LEAVE_TYPE_PERSONAL?>" || value == "<?=LEAVE_TYPE_PERSONAL_UNPAID?>"){
            jQuery("#medicalCertificate").show();
            jQuery("#carer").show();
        }
    }

    if (value == <?=LEAVE_TYPE_CASUAL?> || value == <?=LEAVE_TYPE_INTERSTORE?>){
		jQuery("#store").show();
    }else{
		jQuery("#store").hide();
    }

    if (value == <?=LEAVE_TYPE_CASUAL?> ){
    	jQuery("#department").show();
    }else{
    	jQuery("#department").hide();
    }
}

function fullDayClick(value){
	if(value == 1){
		jQuery("#start_time_type").hide();
		jQuery("#end_time_type").hide();
		jQuery("#include_break_type").hide();
	    jQuery("#end_day_type").show();
	}else{
		jQuery("#end_day_type").hide();
		jQuery("#start_time_type").show();
		jQuery("#end_time_type").show();
		jQuery("#include_break_type").show();
	}
}

function breakClick(){
	count();
}

function submit_click(action){
	var leave_type = jQuery("#leave_type").val();
	if(!leave_type){
		alert("Please select leave type!");
		return false;
	}

	if(leave_type == "<?=LEAVE_TYPE_LATE?>"
		|| leave_type == "<?=LEAVE_TYPE_LUNCH?>"
		|| leave_type == "<?=LEAVE_TYPE_CLOCK?>"){
		var arrival_day = jQuery("#arrival_day").val();
		if(!arrival_day){
			alert("Please select arrival day!");
			return false;
		}
		var arrival_time = jQuery("#arrival_time").val();
		if(!arrival_time){
			alert("Please select arrival time!");
			return false;
		}
		if(!isTime(arrival_time)){
			return false;
		}
		var minutes_used = jQuery("#minutes_used").val();
		if (leave_type != "<?=LEAVE_TYPE_CLOCK?>"){
			if(minutes_used){
				if(!isNumber(minutes_used)){
					alert("Please enter a valid number!");
					return false;
				}
			}else{
				alert("Please enter minutes used!");
				return false;
			}
		}

		var reason = jQuery("#reason").val();
		if(jQuery.trim(reason) == ""){
			alert("Please enter the reason");
			return false;
		}
		var same = calculate_same();
		if(same){
			return false;
		}
		if(action == "apply"){
			if (leave_type != "<?=LEAVE_TYPE_CLOCK?>"){
				if(confirm("Do you confirm request " + jQuery("#minutes_used").val() + " minutes leave?") ){
					submitform(action);
				}
			}else{
				if(confirm("Do you confirm manual clock in/out ?") ){
					submitform(action);
				}
			}
		}else{
			submitform(action);
		}
	}else{
		var request_full_day = document.getElementById('request_full_day');
		var request_partial_day = document.getElementById('request_partial_day');
		var start_day = jQuery("#start_day").val();
		if(!start_day){
			alert("Please select start day!");
			return false;
		}

		if(request_full_day.checked){
			var end_day = jQuery("#end_day").val();
			if(!end_day){
				alert("Please select end day!");
				return false;
			}
		}else if(request_partial_day.checked){
			var start_time = jQuery("#start_time").val();
			if(!start_time){
				alert("Please select start time!");
				return false;
			}

		    var end_time = jQuery("#end_time").val();
		    if(!end_time){
				alert("Please select end time!");
				return false;
			}
			if(end_time == "00:00"){
				alert("Please select end time again");
				return false;
			}
		}

	    if(!count()){
	        return false;
		}
	    calculate();
		hours_used = jQuery("#hours_used").val();
		mc_provided = jQuery("#mc_provided:checked").val();
//		if(hours_used =="" || hours_used == 0){
//			alert("The hours used is 0 and can't be saved or applied, please try again! ");
//			return false;
//		}
		if(leave_type == "<?=LEAVE_TYPE_ANNUAL?>"){
			var available = calculate_available(leave_type,0);
			if(hours_used > available){
				alert("Annual leave request is more than available balance. You have " + available + " hours available.");
				return false;
			}
		}else if(leave_type == "<?=LEAVE_TYPE_PERSONAL?>"){
			if (mc_provided == 1)
				var available = calculate_available(leave_type,0);
			else
				var available = calculate_available(leave_type,1);

			if(hours_used > available){
				alert("Personal leave request is more than your available balance. You have " + available + " hours available. Request annual or unpaid leave instead");
				return false;
			}else if(mc_provided != 1 && hours_used > available){
				alert("Personal leave with no MC request is more than your annual allowance. You have " + available + " hours available. Request annual or unpaid leave instead");
				return false;
			}
		}else if(leave_type == "<?=LEAVE_TYPE_COMPASIONATE?>"){
			var available = calculate_available(leave_type,0);
			if(hours_used > available){
				alert("Compasionate leave request is more than your annual allowance. You have " + available + " hours available. Request annual or unpaid leave instead");
				return false;
			}
		}else if(leave_type == "<?=LEAVE_TYPE_INLIEU?>"){
			var available = calculate_available(leave_type,0);
			if(hours_used > available){
				alert("In Lieu leave request is more than your available balance. You have " + available + " hours available.");
				return false;
			}
		}

		var reason = jQuery("#reason").val();
		if(jQuery.trim(reason) == ""){
			alert("Please enter the reason");
			return false;
		}
		var same = calculate_same();
		if(same){
			return false;
		}
		if(action == "apply"){
			if(leave_type == "<?=LEAVE_TYPE_OVERTIME?>"){
				if(confirm("Do you confirm " + jQuery("#hours_used").val() + " hours overtime?") ){
					submitform(action);
				}
			}
			else{
				if(confirm("Do you confirm " + jQuery("#hours_used").val() + " hours leave?") ){
					submitform(action);
				}
			}
		}else{
			submitform(action);
		}
	}
	return false;
}

function submitform(action){
	jQuery("#operate").val(action);
	var theForm = document.getElementById("add_request");
	theForm.submit();
	jQuery('#apply').attr('disabled','true');
	jQuery('#save').attr('disabled','true');
}

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function isTime(strTime) {
	var strSeparator = ":";
	var intHour;
	var intMinute;
	var strTimeArray = strTime.split(strSeparator);
	if(strTimeArray.length!=2){
		alert('Prompt\r\nPlease enter correct format time,E.g. HH:mm');
		return false;
	}
	intHour = parseInt(strTimeArray[0],10);
	intMinute = parseInt(strTimeArray[1],10);
	if(!isNumber(intHour) || !isNumber(intMinute)
			|| intHour < 1 || intHour > 23
			|| intMinute < 0 || intMinute > 59){
		alert('Prompt\r\nPlease enter correct format time,E.g. HH:mm');
		return false;
	}
	return true;
}

function blurTime(){
	if(!jQuery("#arrival_day").val()){
		alert("Please select arrival day!");
		return false;
	}
	if(!jQuery("#arrival_time").val()){
		alert("Please enter arrival time!");
		return false;
	}
	if(isTime(jQuery("#arrival_time").val())){
		var leave_type = jQuery("#leave_type").val();
		if(leave_type && leave_type == "<?=LEAVE_TYPE_LATE?>"){
			return calculate_late();
		}
	}
	return true;
}

function count_late(){
	if(!jQuery("#arrival_day").val()){
		alert("Please select arrival day!");
		return false;
	}
	if(jQuery("#arrival_time").val()){
		return blurTime();
	}
	return true;
}

function calculate(){
	var request_full_day = document.getElementById('request_full_day');
	var request_partial_day = document.getElementById('request_partial_day');
	var user_id = document.getElementById('user_id').value;
	var leave_type = document.getElementById('leave_type').value;
	var start_day = "";
	var end_day = "";
	var start_time = "";
	var end_time = "";
	var include_break = false;
	var request_type = 0;

	if(request_full_day.checked){
		start_day = jQuery("#start_day").val();
		end_day = jQuery("#end_day").val();
		start_time = "00:00";
		end_time = "23:59";
		request_type = 1;
	}else if(request_partial_day.checked){
		start_day = jQuery("#start_day").val();
		end_day = start_day;
		start_time = jQuery("#start_time").val();
		end_time = jQuery("#end_time").val();
		var include_break_ojb = document.getElementById('include_break');
		include_break = include_break_ojb.checked;
		request_type = 2;
	}
	jQuery.ajax({
		async: false,
		type: 'POST',
		url: "<?=site_url("request/calculate_working_hours_ajax")?>",
		data: {
		  	start_day: start_day,
		  	start_time: start_time,
		  	end_day: end_day,
		  	end_time: end_time,
		  	request_type: request_type,
		  	leave_type: leave_type,
		  	include_break: include_break,
		  	user_id: user_id
		},
		success: function(data){
			jQuery("#hours_used").val(data.used_hours_count);
	  	},
		dataType: 'json'
	});
}

function calculate_late(){
	jQuery.ajax({
		async: false,
		type: 'POST',
		url: "<?=site_url("request/calculate_minutes_late_ajax")?>",
		data: {
			user_id: jQuery("#user_id").val(),
			arrival_day: jQuery("#arrival_day").val(),
			arrival_time: jQuery("#arrival_time").val()
		},
		success: function(data){
			if(data.status == "ok"){
				jQuery("#minutes_used").val(data.used_minutes_count);
			}
	  	},
		dataType: 'json'
	});

//	check_minute();
}

function check_minute(){
	if (jQuery("#minutes_used").val() > 15)
		alert ("Late/Long Lunch can't be more than 15 minutes");

}

function calculate_available(leave_type_value, no_mc_value){
	var available = 0;

    jQuery.ajax({
        async: false,
        type: 'POST',
        url: "<?=site_url("request/calculate_available_ajax")?>",
        data: {
        	user_id: jQuery("#user_id").val(),
            leave_type: leave_type_value,
            no_mc: no_mc_value
        },
        success: function(data){
            if(data.status == "ok"){
            	available = data.available;
            }
        },
        dataType: 'json'
    });
    return available;
}

function calculate_same(){
	var start_date = "";
	var end_date = "";
	var leave_type = jQuery("#leave_type").val();
	if(!leave_type){
		alert("Please select leave type!");
		return false;
	}
	if(leave_type == "<?=LEAVE_TYPE_LATE?>"
		|| leave_type == "<?=LEAVE_TYPE_LUNCH?>"){
		var arrival_day = jQuery("#arrival_day").val();
		var arrival_time = jQuery("#arrival_time").val();
		start_date = arrival_day + " " + arrival_time;
		end_date = arrival_day + " 23:59:59";
	}else{
		var request_full_day = document.getElementById('request_full_day');
		var request_partial_day = document.getElementById('request_partial_day');
		var start_day = jQuery("#start_day").val();
		if(request_full_day.checked){
			start_date = start_day;
			end_date = jQuery("#end_day").val();
		}else if(request_partial_day.checked){
			start_date = start_day + "" + jQuery("#start_time").val();
			end_date = start_day + "" + jQuery("#end_time").val();
		}
	}

	var same = false;
    jQuery.ajax({
        async: false,
        type: 'POST',
        url: "<?=site_url("request/calculate_same_request_ajax")?>",
        data: {
        	user_id: jQuery("#user_id").val(),
            leave_type: leave_type,
            start_date: start_date,
            end_date: end_date,
            request_id: 0,
        },
        success: function(data){
            if(data.count > 0){
            	//
            	alert("You have request a same leave type in current time period.");
            	same = true;
            }
        },
        dataType: 'json'
    });
    return same;
}

function update_dept_dropdown(site_id){
    jQuery.ajax({
        async: false,
        type: 'POST',
        url: "<?=site_url("request/update_dept_ajax")?>",
        data: {
        	site_id: site_id,
        },
        success: function(html){

            jQuery("#dept_td").html(html);
        },
        dataType: 'html'
    });

}

jQuery(function() {
    jQuery("#start_day").datepicker({
    	disabled: true,
    	dateFormat: '<?=JS_DATE_FORMAT?>',
    	changeMonth: true,
		changeYear: true
	});
    jQuery("#end_day").datepicker({
    	disabled: true,
    	dateFormat: '<?=JS_DATE_FORMAT?>',
    	changeMonth: true,
		changeYear: true
	});
    jQuery("#arrival_day").datepicker({
    	disabled: true,
    	dateFormat: '<?=JS_DATE_FORMAT?>',
    	changeMonth: true,
		changeYear: true
	});
    changeLeaveType(jQuery("#leave_type").val());
});
//]]>
</script>
<script type="text/javascript" src="js/piwik-lrs.js"></script>
    </head>
    <body>
    	<div>
        <?php
        echo form_open_multipart('request/do_add', array('id'=>'add_request'));
        ?>
        <input type="hidden" id="operate" name="operate" value=""/>
        <table width="100%" height="22" border="0" align="center" cellpadding="0" cellspacing="0" background="images/Footer.png">
            <tr>
                <td>
                    <table width="100%" height="50" border="0" cellspacing="1" cellpadding="1">
                        <tr bgcolor="#EBEBEB">
                            <td width="50%" height="20" colspan="2" align="right" bgcolor="#EBEBEB" style="color: red">&nbsp;</td>
                        </tr>
						<?php if ($user_level >= 2){?>
	                        <tr bgcolor="#FFFFFF">
	                            <td width="20%" height="20" align="right">User:</td>
	                            <td width="80%" height="20">
	                            	<?php
	                            	$js = 'id="user_id" style="font-size:11px" onchange="count();"';
	                            	echo form_dropdown('user_id', $users, set_value('user_name'), $js);?>
	                            	<label style="color: #F00">*</label>
	                            </td>
	                        </tr>
                        <?php }else{
                        	echo "<input type='hidden' id='user_id' value=".$user_id.">";

                        }?>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="right">Leave Type:</td>
                            <td width="80%" height="20">
                            	<?php
                            	$js = 'id="leave_type" style="font-size:11px" onchange="changeLeaveType(this.value)"';
                            	echo form_dropdown('leave_type', $leave_types, set_value('leave_type'), $js);?>
                                <label style="color: #F00">*</label><?php echo form_error('leave_type'); ?>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF" id="store">
                            <td width="20%" height="20" align="right">Store:</td>
                            <td width="80%" height="20">
                            	<?php
                            	$js = 'id="site_id" style="font-size:11px" onchange="update_dept_dropdown(this.value);"';
                            	echo form_dropdown('site_id', $stores, set_value('site_id'), $js);
                                echo form_error('stores'); ?>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF" id="department">
                            <td width="20%" height="20" align="right">Department:</td>
                            <td width="80%" height="20" id='dept_td'>
                            	<select><option>Please select..</option></select>
                            </td>
                        </tr>


                     </table>
                     <table width="100%" height="150" border="0" cellspacing="1" cellpadding="1" id="common_type" >
                     	<tr bgcolor="#FFFFFF" id="request_type">
                            <td height="20" align="right"></td>
                            <td height="20" align="left">
                            	<input type="radio" name="request_type" id="request_full_day" value="1" onclick="fullDayClick(1)" checked="checked"/>FULL day
                            	<input type="radio" name="request_type" id="request_partial_day" value="2" onclick="fullDayClick(0)"/>PARTIAL day
                            </td>
                        </tr>
                     	<tr bgcolor="#FFFFFF" id="medicalCertificate" style="display:none">
                            <td height="20" align="right">Medical Certificate (MC):</td>
                            <td height="20"><label>
                            		<input type="checkbox" name="mc_provided" id="mc_provided" value="1" />
<!--                                <input type="file" name="medical_certificate" id="medical_certificate" style="font-size:11px;"/>  -->
                                </label></td>
                        </tr>
                     	<tr bgcolor="#FFFFFF" id="carer" style="display:none">
                            <td height="20" align="right">Carer's Leave:</td>
                            <td height="20"><label>
                            		<input type="checkbox" name="carer_leave" value="1" />
                                </label></td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="right">Start Date:</td>
                            <td width="80%" height="20">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="100%">
                                        	<input type="text" name="start_day" id="start_day"
                                            value="<?php echo set_value('start_day');?>" style="font-size: 11px"
                                            onchange="javascript: count();"/>
                                    		<label style="color: #F00">*</label><?php echo form_error('start_day'); ?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF" id="end_day_type">
                            <td height="20" align="right">End Date:</td>
                            <td height="20">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="100%">
                                        	<input type="text" name="end_day" id="end_day"
                                                               value="<?php echo set_value('end_day');?>" style="font-size: 11px"
                                                               onchange="javascript: count();"/>
                                            <label style="color: #F00">*</label><?php echo form_error('end_day'); ?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF" id="start_time_type" style="display:none">
                            <td height="20" align="right">Start Time:</td>
                            <td height="20">
                            	<?php
                            	$js = 'id="start_time" style="font-size:11px" onchange="count();"';
                            	echo form_dropdown('start_time', hour_time(), '', $js); ?>
                            	<label style="color: #F00">*</label>
<!--
                            	<input type="text" name="start_time" id="start_time" onchange="count();"/> <a href="#" id="start_time_toggler">Open time picker</a>
								<div id="start_time_picker" class="time_picker_div"/>
-->
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF" id="end_time_type" style="display:none">
                            <td height="20" align="right">End Time:</td>
                            <td height="20">
	                           	<?php
                            	$js = 'id="end_time" style="font-size:11px" onchange="count();"';
                            	echo form_dropdown('end_time', hour_time(), '', $js); ?>
                            	<label style="color: #F00">*</label>

<!--
                            	<input type="text" name="end_time" id="end_time" onchange="count();"/> <a href="#" id="end_time_toggler">Open time picker</a>
								<div id="end_time_picker" class="time_picker_div"/>
-->
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF" id="include_break_type" style="display:none">
                            <td height="20"></td>
							<td height="20" align="left"><input type="checkbox" name="include_break" id="include_break" onclick="breakClick()" value="1"/> deduct lunch </td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td height="20" align="right">Hours Used:</td>
                            <td height="20">
								<input name="hours_used" type="text" id="hours_used" style="background-color:#F5F5F5; font-size:11px; color:#000" size="7" readonly/>
							</td>
                        </tr>
                 	</table>
                	<table width="100%" height="100" border="0" cellspacing="1" cellpadding="1" id="late_type" style="display:none;">
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="right">Date:</td>
                            <td width="80%" height="20">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="100%">
                                        	<input type="text" name="arrival_day" id="arrival_day"
                                            value="<?php echo set_value('arrival_day');?>" style="font-size: 11px"
                                            onchange="javascript: count_late();"/>
                                    		<label style="color: #F00">*</label><?php echo form_error('arrival_day'); ?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF" id="arrival_type">
                            <td height="20" align="right">Actual In/Out Time:</td>
                            <td height="20">
                                <input type="text" name="arrival_time" id="arrival_time" onchange="blurTime();"/> (e.g. 17:35)

<!--                           	<input type="text" name="arrival_time" id="arrival_time" onblur="return blurTime();"/> <a href="#" id="arrival_time_toggler">Open time picker</a>
								<div id="arrival_time_picker" class="time_picker_div"/>
-->
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF" id="minute_used">
                            <td height="20" align="right">Minutes Used:</td>
                            <td height="20">
								<input name="minutes_used" type="text" id="minutes_used" style="font-size:11px; color:#000" size="7"/>
							</td>
                        </tr>
                    </table>
              		<table width="100%" height="50" border="0" cellspacing="1" cellpadding="1">
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="right">Reason:</td>
                            <td width="80%" height="20">
	                        <?php
							echo form_textarea(array(
								'name' => 'reason',
								'id' => 'reason',
								'value' => set_value('reason'),
								'cols' => "45",
								'rows' => "5",
								'style'=> "font-size:11px"
							));
							?>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td height="20" colspan="2" align="center" bgcolor="#EBEBEB">
                            <!--  <input style="font-size: 11px" type="button" name="save" onclick="submit_click('save');" id="save" value="     Save     "/>  -->
                                <input style="font-size: 11px" type="button" name="apply" onclick="submit_click('apply');"  id="apply" value="  Request Leave  "/>
                            </td>
                        </tr>
                    </table>
                    </td>
            </tr>
        </table>
        <?php echo form_close();?>
        </div>
    </body>
</html>
