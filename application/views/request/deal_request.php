<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Approve leave request</title>
<base href="<?=base_url()?>"/>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />
<style>
#input, select
{
	font-family: Tahoma, Helvetica, Sans-Serif;
	font-size: 11px;
	font-style: normal;
	font-weight: normal;
}

/* tab pane styling */
div.panes div {
	display:none;		
	padding:15px 10px;
	border:1px solid #999;
	border-top:0;
	font-size:14px;
	background-color:#fff;
}

</style>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
</head>

<body>
<?
//= @flash_message() 
echo $this->session->flashdata('message');
?>

<h4><?php echo $user->first_name. " " . $user->last_name;?></h4>

<ul class="tabs"> 
	<li><a href="#request">Request Info</a></li> 
    <li><a href="#history">Leave History</a></li> 
    <li><a href="#colleague">Other People requests</a></li>
    <li><a href="#waiting">Waiting Approval (<?=$waiting_count?>)</a></li>
    <li><a href="#upcoming">Upcoming (<?=$upcoming_count?>)</a></li> 
    <li><a href="#logs">Logs</a></li>
</ul>
<div class="panes">

<div class="pane">

<table width="70%" height="100" border="0" cellspacing="0" cellpadding="0" background="images/Footer.png">
    <tr>
      <td valign="top">
      <table width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr>
            <td width="30%" height="20">
	            <table bgcolor="#FFFFFF" width="100%" border="0" cellspacing="0" cellpadding="3">
	              <tr style="background-image:url(images/TableHeader.png);" height="20">
	              	<td colspan="2" style="font-weight: bold;">Annual Leave</td>
	              </tr>
	              <tr >
	                <td width="55%" height="20">Currently Available:</td>
	                <td><?=$annual_available?> Hours</td>
	              </tr>
	              <tr>
	                <td width="55%" height="20">Upcoming Leave Request:</td>
	                <td><?=$annual_upcoming?> Hours</td>
	              </tr>
	              <tr>
	                <td width="55%" height="20">Total Balance:</td>
	                <td><?=$total_annual?> Hours</td>
	              </tr>
	              <tr>
	                <td width="55%" height="20">Total In Lieu:</td>
	                <td><?=$total_inlieu?> Hours</td>
	              </tr>
	              <tr>
	                <td width="55%" height="20">Upcoming Unpaid:</td>
	                <td><?=$unpaid_upcoming?> Hours</td>
	              </tr>
	              
	            </table>
            </td>
            <td width="30%" height="20">
			<table bgcolor="#FFFFFF" width="100%" border="0" cellspacing="0" cellpadding="3">
				<tr style="background-image:url(images/TableHeader.png);" height="20">
					<td colspan="2" style="font-weight: bold;">Personal Leave</td>
				</tr>
				<tr >
				  <td width="55%" height="20">Currently Available:</td>
				  <td><?=$personal_available?> Hours</td>
				</tr>
				<tr>
				  <td width="55%" height="20">Upcoming Leave Request:</td>
				  <td><?=$personal_upcoming?> Hours</td>
				</tr>
				<tr>
				  <td width="55%" height="20">Total Balance:</td>
				  <td><?=$total_personal?> Hours</td>
				</tr>
	              
	              
				<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
				  <td width="55%" height="20">Total No MC Used:</td>
				  <td><?php echo $no_mc_used;?> Hours (Max: <?php echo $max_no_mc?> Hours/year)</td>
				</tr>
				
				<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
				  <td width="55%" height="20">Total Compasionate Used:</td>
				  <td><?php echo $comp_used;?> Hours (Max: <?php echo $max_comp_leave?> Hours/year)</td>
				</tr>
	              
	            </table>
            </td>            
          </tr>
      </table>
	  </td>
    </tr>
</table>    

<br/>


<table width="70%" height="100" border="0" cellspacing="0" cellpadding="0" background="images/Footer.png">
    <tr>
      <td valign="top">

	<table width="100%" border="0" align="left" cellpadding="3" cellspacing="1" background="images/Footer.png">
	<tr style="background-image:url(images/TableHeader.png);" height="20">
		<td colspan="12" style="font-weight: bold;">Past Request</td>
	</tr>
	<tr bgcolor="#FFFFFF" style="font-weight: bold;">
		<td rowspan='2'>&nbsp;</td>
		<td align='center' colspan='2'>Annual Leave</td>
		<td align='center' colspan='2'>Personal Leave</td>
		<td align='center' colspan='2'>Unpaid Leave</td>
		<td align='center' colspan='2'>Late</td>
		<td align='center' colspan='2'>Long Lunch</td>
		<td align='center' colspan='2'>Clock In/Out</td>
		     </tr>
		     <tr bgcolor="#FFFFFF" style="font-weight: bold;">
		      <td align="center">Requests</td>
		      <td align="center">Hours</td>
		      <td align="center">Requests</td>
		      <td align="center">Hours</td>
		      <td align="center">Requests</td>
		      <td align="center">Hours</td>
		      <td align="center">Requests</td>
		      <td align="center">Minutes</td>
		      <td align="center">Requests</td>
		      <td align="center">Minutes</td>
		      <td align="center">Requests</td>
		     </tr>
		     <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
		  <td>Last 7 days</td>
		  <td align="center"><?=$annual_7_days?></td>
		  <td align="center">
		    	<?=$annual_7_times?> 
		  </td>
		  <td align="center"><?=$personal_7_days?></td>			          
		  <td align="center">
		    	<?=$personal_7_times?> 
		  </td>
		  <td align="center"><?=$unpaid_7_days?></td>			          
		  <td align="center">
		    	<?=$unpaid_7_times?> 
		  </td>
		  
		  <td align="center"><?=$late_7_days?></td>
		  <td align="center">
		    	<?=$late_7_times?> 
		  </td>
		  <td align="center"><?=$lunch_7_days?></td>
		  <td align="center">
		    	<?=$lunch_7_times?> 
		  </td>
		  <td align="center"><?=$clock_7_days?></td>
		</tr>
		<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
		  <td>Last 30 days</td>
		  <td align="center"><?=$annual_30_days?></td>
		  <td align="center">
		    	<?=$annual_30_times?> 
		  </td>
		  <td align="center"><?=$personal_30_days?></td>			          
		  <td align="center">
		    	<?=$personal_30_times?> 
		  </td>
		  <td align="center"><?=$unpaid_30_days?></td>			          
		  <td align="center">
		    	<?=$unpaid_30_times?> 
		  </td>
		  
		  <td align="center"><?=$late_30_days?></td>			          
		  <td align="center">
		    	<?=$late_30_times?> 
		  </td>
		
		  <td align="center"><?=$lunch_30_days?></td>			          
		  <td align="center">
		    	<?=$lunch_30_times?> 
		  </td>
		  <td align="center"><?=$clock_30_days?></td>
		</tr>
		<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
		  <td>Last 3 months</td>
		  <td align="center"><?=$annual_90_days?></td>
		  <td align="center">
		    	<?=$annual_90_times?> 
		  </td>
		
		  <td align="center"><?=$personal_90_days?></td>			          
		  <td align="center">
		    	<?=$personal_90_times?> 
		  </td>
		  <td align="center"><?=$unpaid_90_days?></td>			          
		  <td align="center">
		    	<?=$unpaid_90_times?> 
		  </td>
		  
		
		  <td align="center"><?=$late_90_days?></td>			          
		  <td align="center">
		    	<?=$late_90_times?> 
		  </td>
		
		  <td align="center"><?=$lunch_90_days?></td>			          
		  <td align="center">
		    	<?=$lunch_90_times?> 
		  </td>
		  <td align="center"><?=$clock_90_days?></td>
		</tr>
		<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
		      <td>Last 6 months</td>
		<td align="center"><?=$annual_180_days?></td>
		  <td align="center">
		    	<?=$annual_180_times?> 
		  </td>
		  <td align="center"><?=$personal_180_days?></td>
		  <td align="center">
		    	<?=$personal_180_times?> 
		  </td>
		  <td align="center"><?=$unpaid_180_days?></td>
		  <td align="center">
		    	<?=$unpaid_180_times?> 
		  </td>		  
		  <td align="center"><?=$late_180_days?></td>			          
		  <td align="center">
		    	<?=$late_180_times?> 
		  </td>
		  <td align="center"><?=$lunch_180_days?></td>			          
		  <td align="center">
		    	<?=$lunch_180_times?> 
		  </td>
		  <td align="center"><?=$clock_180_days?></td>
		</tr>
		<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
		  <td>Last 12 months</td>
		  <td align="center"><?=$annual_360_days?></td>
		  <td align="center">
		    	<?=$annual_360_times?> 
		  </td>
		  
		  <td align="center"><?=$personal_360_days?></td>
		  <td align="center">
		    	<?=$personal_360_times?> 
		  </td>
		  
		  <td align="center"><?=$unpaid_360_days?></td>
		  <td align="center">
		    	<?=$unpaid_360_times?> 
		  </td>
		  
		  
		  <td align="center"><?=$late_360_days?></td>
		  <td align="center">
		    	<?=$late_360_times?> 
		  </td>
		  <td align="center"><?=$lunch_360_days?></td>
		  <td align="center">
		    	<?=$lunch_360_times?> 
		  </td>
		  <td align="center"><?=$clock_360_days?></td>
		</tr>
		<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
		  <td>This Year</td>
		  <td align="center"><?=$annual_thisyear_days?></td>
		  <td align="center">
		    	<?=$annual_thisyear_times?> 
		  </td>
		  
		  <td align="center"><?=$personal_thisyear_days?></td>
		  <td align="center">
		    	<?=$personal_thisyear_times?> 
		  </td>
		  
		  <td align="center"><?=$unpaid_thisyear_days?></td>
		  <td align="center">
		    	<?=$unpaid_thisyear_times?> 
		  </td>
		  
		  
		  <td align="center"><?=$late_thisyear_days?></td>
		  <td align="center">
		    	<?=$late_thisyear_times?> 
		  </td>
		  <td align="center"><?=$lunch_thisyear_days?></td>
		  <td align="center">
		    	<?=$lunch_thisyear_times?> 
		  </td>
		  <td align="center"><?=$clock_thisyear_days?></td>
		</tr>		
		<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
		  <td>All Time Total</td>
		  <td align="center"><?=$annual_all_days?></td>
		  <td align="center">
		    	<?=$annual_all_times?> 
		  </td>
		  
		  <td align="center"><?=$personal_all_days?></td>
		  <td align="center">
		    	<?=$personal_all_times?> 
		  </td>
		  
		  <td align="center"><?=$unpaid_all_days?></td>
		  <td align="center">
		    	<?=$unpaid_all_times?> 
		  </td>
		  
		  
		  <td align="center"><?=$late_all_days?></td>
		  <td align="center">
		    	<?=$late_all_times?> 
		  </td>
		  <td align="center"><?=$lunch_all_days?></td>
		  <td align="center">
		    	<?=$lunch_all_times?> 
		  </td>
		  <td align="center"><?=$clock_all_days?></td>		  
		</tr>
	</table>
	</td>
	</tr>
</table>
<br/>

<table width="50%" height="100" border="0" cellspacing="0" cellpadding="0" background="images/Footer.png">
    <tr>
      <td valign="top">


<?php 
	echo form_open('approve/do_deal', array('id'=>'deal', 'name'=>'deal', 'onsubmit' => 'return check_submit(this);' )); 
 	echo form_hidden("request_id", $request->id);
 ?>


<table width="100%" border="0" align="left" cellpadding="3" cellspacing="1" background="images/Footer.png">
        <tr style="background-image:url(images/TableHeader.png);" height="20">
            <td height="20" colspan="2" align="left" style="font-weight: bold;">Leave Request Information</td>
        </tr>

        <tr bgcolor="#FFFFFF">
          <td height="20">Applicant:</td>
          <td height="20">
          	<label>
            	<?php echo $user->first_name. " " . $user->last_name;?>
          	</label>
          </td>
        </tr>

        <tr bgcolor="#FFFFFF">
          <td width="150">Leave Type:</td>
          <td>
          <label>
            <?php
				$js = 'id="leave_type_id" style="font-size: 11px; width: 120px;" disabled="disabled"'; 
				echo form_dropdown('leave_type_id', $leave_types, $request->leave_type_id, $js);
			?>
          </label>
          </td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td>Store:</td>
          <td><label>
          		<?php echo $store;?>
          </label>
          </td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td>Department:</td>
          <td><label>
            <?php	echo $dept;		?>
          </label>
          </td>
        </tr>
        
        <?php 
        if($request->leave_type_id == LEAVE_TYPE_LATE || $request->leave_type_id == LEAVE_TYPE_LUNCH){
        	?>
        	<tr bgcolor="#FFFFFF">
			    <td>Arrival Day:</td>
			    <td>
			        <table width="100%" border="0" cellspacing="0" cellpadding="0">
			            <tr>
			                <td width="100%">
			                    <?=date(DATE_FORMAT, $request->start_day)?>
			                </td>
			            </tr>
			        </table>
			    </td>
			</tr>
			<tr bgcolor="#FFFFFF">
			    <td>Actual Arrival Time:</td>
			    <td>
			    	<?=$request->start_time?>
			    </td>
			</tr>
			<tr bgcolor="#FFFFFF">
			    <td>Minutes Used:</td>
			    <td>
			    	<?=$request->hours_used?>
			    </td>
			</tr>
        <? 
        }else{
        ?>
        <tr bgcolor="#FFFFFF">
          <td>Start Day:</td>
          <td><label>
            <input type="text" name="fromdate" id="fromdate" style="font-size:11px;background-color:#F5F5F5;" value="<?=date(DATE_FORMAT, $request->start_day)?>" disabled="disabled"/>
          </label></td>
        </tr>
        <? if($request->request_type == 1){?>
        <tr bgcolor="#FFFFFF">
          <td>End Day:</td>
          <td><label>
            <input type="text" name="todate" id="todate" style="font-size:11px;background-color:#F5F5F5;" value="<?=date(DATE_FORMAT, $request->end_day)?>" disabled="disabled"/>
          </label></td>
        </tr>
        <? }else if($request->request_type == 2){
        ?>
        <tr bgcolor="#FFFFFF">
          <td>Start Time:</td>
          <td>
          <label>
            <select name="select2" id="select2" style="font-size:11px;background-color:#F5F5F5;" disabled="disabled">
            <option value="" selected><?=$request->start_time?></option>
            </select>
          </label>
          </td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td>End Time:</td>
          <td>
          	<label>
            <select name="select3" id="select3" style="font-size:11px;background-color:#F5F5F5;" disabled="disabled">
            	<option value="" selected><?=$request->end_time?></option>
            </select>
          	</label>
          </td>
        </tr>
        <tr bgcolor="#FFFFFF">
			<td height="20" align="right"></td>
            <td height="20">
				<input type="checkbox" name="include_break" id="include_break" onclick="this.checked=!this.checked"
       			<?php 
            	if($request->include_break == 1){
                	echo "checked='checked'";
                }
				?>
				/> deduct lunch 
			</td>
		</tr>
        <?php }else{?>
        <tr bgcolor="#FFFFFF">
          <td>Start Time:</td>
          <td>
          <label>
            <select name="select2" id="select2" style="font-size:11px;background-color:#F5F5F5;" disabled="disabled">
            <option value="" selected><?=$request->start_time?></option>
            </select>
          </label>
          </td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td>End Day:</td>
          <td><label>
            <input type="text" name="todate" id="todate" style="font-size:11px;background-color:#F5F5F5;" value="<?=date(DATE_FORMAT, $request->end_day)?>" disabled="disabled"/>
          </label></td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td>End Time:</td>
          <td>
          	<label>
            <select name="select3" id="select3" style="font-size:11px;background-color:#F5F5F5;" disabled="disabled">
            	<option value="" selected><?=$request->end_time?></option>
            </select>
          	</label>
          </td>
        </tr>
        
        <?php }?>
        <tr bgcolor="#FFFFFF">
          <td>Hours Used:</td>
          <td><label>
            <input name="textfield3" type="text" id="textfield3" value="<?echo $request->hours_used?>" style="background-color:#F5F5F5; font-size:11px" size="10" disabled="disabled"/>
            <input type="hidden" name="request_hours" value="<?echo $request->hours_used?>">
          </label></td>
        </tr>

        <?php 
        }
        ?>
        
        <tr bgcolor="#FFFFFF">
          <td>Reason:</td>
          <td><label>
            <textarea name="textarea" id="textarea" cols="45" rows="5" style="font-size:11px;background-color:#F5F5F5;" disabled="disabled"><?=$request->reason?></textarea>
          </label></td>
        </tr>        
	  <?php if(isset($order_no)){ ?>
	  <tr bgcolor="#FFFFFF">
	    <td>Order:</span></th>
	    <td>&nbsp;
	    <?php if ($order_url){?>
	    	<a href="<?=$order_url?>"><?=$order_no?></a>
	    <?php }else{
	    	echo $order_no;
        }?>
	    </td>
	  </tr>
	  <tr bgcolor="#FFFFFF">
	    <td>Client Visit:</span></th>
	    <td>&nbsp;
	    <a href="<?=$client_visit_url?>"><?=$client_visit_id?></a>
	    </td>
	  </tr>
	  
	  <?php }?>
        
        
        <?php 
        $mc_provided = FALSE;
        //
        if($request->leave_type_id == LEAVE_TYPE_PERSONAL || $request->leave_type_id == LEAVE_TYPE_PERSONAL_UNPAID){
        	if(!empty($request->medical_certificate_file)){
        		$mc_file = $request->medical_certificate_file;
        		$mc_link = DIR_MEDICAL_CERTIFICATE . "$request->medical_certificate_file";
        		
        		if(strrchr(strtolower($mc_file),".jpg") == ".jpg" 
        			|| strrchr(strtolower($mc_file),".gif") == ".gif" 
        			|| strrchr(strtolower($mc_file),".png") == ".png" 
        			|| strrchr(strtolower($mc_file),".bmp") == ".bmp" 
        		){
        			$mc_view = '<a href="'.$mc_link.'" target="_blank"><img src="'.$mc_link.'" alt="Medical Certificate Photo" width="60" height="100" border="0"/> download</a>';
        		}else{
        			$mc_view = '<a href="'.$mc_link.'" target="_blank">download</a>';
        		}
        		?>
        		<tr bgcolor="#FFFFFF">
		          <td height="20" align="right">Medical Certificate (MC):</td>
		          <td height="20"><label>
		            <?php echo $mc_view;?>
		          </label></td>
		        </tr>
        		<?php         		
        	}
        }
        if($request->leave_type_id == LEAVE_TYPE_PERSONAL || $request->leave_type_id == LEAVE_TYPE_PERSONAL_UNPAID){
        	if(!is_null(medical_certificate_file)){
        		$mc_provided = true;
			}
       		if(!is_null($request->mc_provided)){
       			if($request->mc_provided == 1){
        			$mc_provided = true;
       			}else{
       				$mc_provided = false;
       			}
			}
        ?>
        <tr style="background-image:url(images/TableHeader.png);" height="20">
          <td height="20" colspan="2" valign="middle"><label>HOD Information (<span style="color: #F00; font-weight: bold;">*</span>):</label></td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td height="20" colspan="2" bgcolor="#FFFFFF">
          	<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="2%" height="20"><input type="radio" name="mc_provided" <?php if($mc_provided){echo " checked='checked' ";}?> id="radio_mc_provided" value="1" /></td>
              <td width="21%" height="20" align="left">M.C. Provided</td>
              <td width="2%" height="20"><input type="radio" name="mc_provided" id="radio_mc_not_provided" value="0" <?php if ($no_mc_leave_left < 0) echo "disabled='disabled'";?>/></td>
              <td width="28%" height="20" align="left">M.C. Not Provided (<?=$no_mc_leave_left ?> hours)</td>
              <td width="2%" height="20">&nbsp;</td>
              <td width="45%" height="20">&nbsp;</td>
            </tr>
            </table>
          </td>
          </tr>
          <tr bgcolor="#FFFFFF">
          	<td height="20" colspan="2" bgcolor="#FFFFFF">
          		<input type="checkbox" name="carer_leave" value="1" <?php if ($request->carer_leave == 1) echo " checked='checked'";?> />
          		<label>Carer's Leave</label>
          	</td>
          </tr>
        <?php 
        }//end if leave_type_id == LEAVE_TYPE_PERSONAL HOD Information
        ?>
</table>
</td></tr>





</table>

<br/>

<table width="50%" height="100" border="0" cellspacing="0" cellpadding="0" background="images/Footer.png">
    <tr>
      <td valign="top">


	<table width="100%" border="0" align="left" cellpadding="3" cellspacing="1" background="images/Footer.png">
        <tr style="background-image:url(images/TableHeader.png);" height="20">
          <td height="20" colspan="2" align="left" style="font-weight: bold;">Approval by <? echo $first_approval; if ($manager_user->first_name) echo " (".$manager_user->first_name." ".$manager_user->last_name.")"?></td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td height="20" colspan="2" align="left" bgcolor="#FFFFFF">
          	<table width="100%" border="0" cellspacing="0" cellpadding="0">
          		<input type="hidden" name="first_approval_lv" value="<? echo $first_approval_lv; ?>" />
          		<input type="hidden" name="second_approval_lv" value="<? echo $second_approval_lv; ?>" />
          		<?php 
          		if( ($this->session->userdata('user_level') >= $first_approval_lv && $this->session->userdata('user_name') != $request->user_name) || $can_approve_all){
          			$show_button = true;
          		?>
	          	<tr>
	              <td width="2%" height="20">
	              		<input type="radio" name="reason_acceptable" id="reason_acceptable" value="1" 
	              			<?php if(!is_null($request->reason_acceptable) && $request->reason_acceptable == 1){ ?> checked="checked"<? }?>
	              			<?php if($this->session->userdata('user_level') != $first_approval_lv && $request->state == 30 ){ ?> disabled <? }?> 
	              		/>
	              </td>
	              <td width="21%" height="20" align="left">Reason Acceptable</td>
	              <td width="2%" height="20">
	              		<input type="radio" name="reason_acceptable" id="reason_not_acceptable" value="2" 
	              			<?php if(!is_null($request->reason_acceptable) && $request->reason_acceptable == 2){ echo 'checked="checked"'; }?>
	              			<?php if($this->session->userdata('user_level') != $first_approval_lv && $request->state == 30 ){ ?> disabled <? }?>	
	              		/>
	              </td>
	              <td width="28%" height="20" align="left">Reason Not Acceptable</td>
	              <td width="2%" height="20"></td>
	              <td width="45%" height="20"></td>
	            </tr>
	            <tr>
	              <td height="20">
	              		<input type="radio" name="supervisor_approved" id="supervisor_approved" value="1" 
	              			<?php if(!is_null($request->supervisor_approved) && $request->supervisor_approved == 1){ ?>checked="checked"<? }?>
	              			<?php if($this->session->userdata('user_level') != $first_approval_lv && $request->state == 30 ){ ?> disabled <? }?>
	              		/>
	              </td>
	              <td height="20" align="left">Approve</td>
	              <td height="20">
	              		<input type="radio" name="supervisor_approved" id="supervisor_not_approved" value="2" 
	              			<?php if(!is_null($request->supervisor_approved) && $request->supervisor_approved == 2){ ?>checked="checked"<? }?>
	              			<?php if($this->session->userdata('user_level') != $first_approval_lv && $request->state == 30 ){ ?> disabled <? }?>
	              		/>
	              </td>
	              <td height="20" align="left">Reject</td>
	              <td height="20"></td>
	              <td height="20"></td>
	            </tr>
	            <?php if( ($this->session->userdata('user_level') >= $first_approval_lv && $request->state != 30) || $can_approve_all){ ?>
	            <tr>
	              <td height="20"><input type="radio" name="paid" id="radio_paid" value="1" 
	              	<?php 
       				if($request->leave_type_id == LEAVE_TYPE_LATE 
              			|| $request->leave_type_id == LEAVE_TYPE_LUNCH ){
              				if($request->hours_used <= 15){
              					echo ' checked="checked" ';
              				}
              		}else if($request->leave_type_id != LEAVE_TYPE_OVERTIME){
						if(!is_null($request->reason_acceptable) && $request->reason_acceptable == 2){
              				
              			}else{
							echo ' checked="checked" ';
						}
					}if($request->leave_type_id == LEAVE_TYPE_UNPAID){
							echo ' disabled="disabled" ';
					}
        			?>
	              /></td>
	              <td height="20">Paid<?php if($request->leave_type_id == LEAVE_TYPE_LATE 
	              			|| $request->leave_type_id == LEAVE_TYPE_LUNCH )
	              		echo "/Make Up";
	              	?></td>
	              <td height="20"><input type="radio" name="paid" id="radio_uppaid" value="2" 
	              	<?php 
       				if($request->leave_type_id == LEAVE_TYPE_LATE 
              			|| $request->leave_type_id == LEAVE_TYPE_LUNCH ){
              				if($request->hours_used > 15){
              					echo ' checked="checked" ';
              				}
              		}
              		if($request->leave_type_id == LEAVE_TYPE_UNPAID){
              			echo ' checked="checked" disabled="disabled"';
              		}
        			?>
	              /></td>
	              <td height="20" align="left">Unpaid</td>
	              <td height="20"></td>
	              <td height="20"></td>
	            </tr>
	            <?php }?>
	            <tr>
	              <td height="20" colspan="6">Comments:</td>
	            </tr>
	            <tr>
	              <td height="20" colspan="6">
	              		<textarea name="supervisor_comment" id="supervisor_comment" cols="45" rows="5" <?php if($this->session->userdata('user_level') != $first_approval_lv && $request->state == 30) echo 'disabled="disabled"' ?> ><?=$request->supervisor_comment?></textarea>
	              </td>
	            </tr>
	            
	            <?php 
          		}?>
          	</table>
          </td>
        </tr>
        <?php if( $second_approval && $this->session->userdata('user_level') >= $second_approval_lv && $request->state == 30 ) {
        	$show_button = true;
        ?>
        <tr style="background-image:url(images/TableHeader.png);" height="20">
          <td height="20" colspan="2" align="left" style="font-weight: bold;">Approval by <? echo $second_approval; ?></td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td colspan="2" align="center" bgcolor="#FFFFFF">
          	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	            <tr>
	              <td width="2%" height="20">
	              	<input type="radio" name="paid" id="radio_paid" value="1"
        			<?php 
        			if($request->state == 30){
        				if($request->leave_type_id == LEAVE_TYPE_LATE 
	              			|| $request->leave_type_id == LEAVE_TYPE_LUNCH ){
	              				if($request->hours_used <= 15){
	              					echo ' checked="checked" ';
	              				}
	              		}else if($request->leave_type_id != LEAVE_TYPE_OVERTIME){
							if(!is_null($request->reason_acceptable) && $request->reason_acceptable == 2){
	              				
	              			}else{
								echo ' checked="checked" ';
							}
						}
        			}
        			if($request->leave_type_id == LEAVE_TYPE_UNPAID){
        				echo ' disabled="disabled" ';
        			}        			
        			?>
        			/></td>
	              <td width="21%" height="20" align="left">Paid<?php if($request->leave_type_id == LEAVE_TYPE_LATE 
	              			|| $request->leave_type_id == LEAVE_TYPE_LUNCH )
	              		echo "/Make Up";
	              	?></td>
	              <td width="2%" height="20">
				  <input type="radio" name="paid" id="radio_uppaid" value="2"
				  <?php 
        			if($request->state == 30){
        				if($request->leave_type_id == LEAVE_TYPE_LATE 
	              			|| $request->leave_type_id == LEAVE_TYPE_LUNCH ){
	              				if($request->hours_used > 15){
	              					echo ' checked="checked" ';
	              				}
								//if reason is not acceptable is ticked also become Unpaid
								if(!is_null($request->reason_acceptable) && $request->reason_acceptable == 2){
	              					echo ' checked="checked" ';
	              				}
	              		}
        			}
        			if($request->leave_type_id == LEAVE_TYPE_UNPAID){
        				echo ' checked="checked" disabled="disabled" ';
        			}        			
        			?>
				  /></td>
	              <td width="28%" height="20" align="left">Unpaid</td>
	              <td width="2%" height="20"><?php echo form_error('paid'); ?></td>
	              <td width="45%" height="20"></td>
	            </tr>
	            <tr>
	              <td height="20"><input type="radio" name="approve" id="approve_approve" value="1"/></td>
	              <td height="20" align="left">Approve</td>
	              <td height="20"><input type="radio" name="approve" id="approve_reject" value="2"/></td>
	              <td height="20" align="left">Reject</td>
	              <td height="20"></td>
	              <td height="20"></td>
	            </tr>
	            <tr>
	              <td height="20" colspan="6" align="left">Comments:</td>
	            </tr>
	            <tr>
	              <td height="20" colspan="6" align="left"><textarea name="manager_comment" id="manager_comment" cols="45" rows="5"></textarea></td>
	            </tr>
          	</table>
          </td>
        </tr>
        <?php }
        if ($show_button){
        ?>
          <tr bgcolor="#FFFFFF">
	          <td colspan="2" height="20" align="center">
	            <input type="button" name="button" id="button" onclick="goto_index();return false;" value="     Close     " style="font-size:11px"/>
	          	<input type="submit" name="button2" id="button2" value=" Approve/Reject " style="font-size:11px" 
	          		<?php if ($request->state >=40 && $this->session->userdata('user_level') == $first_approval_lv) echo "disabled='disabled'";?> />
	          </td>
          </tr>
          <?php }?>
      </table>
</td>
</tr>
</table>


<?php echo form_close();?>
</div>
	<div class="pane" id="history"> 
	
<strong>Leave History</strong>
<br/><br/>
	
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="90%">
            <form id="<?=$form_id?>" name="<?=$form_id?>" method="post" action="">
            
            <input type="hidden" id="user_name" name="user_name" value="<?=$request->user_name?>"/>
        	<input type="hidden" id="<?=$form_id?>_order_by" name="<?=$form_id?>_order_by" value=""/>
        	<input type="hidden" id="<?=$form_id?>_order" name="<?=$form_id?>_order" value=""/>
        	<input type="hidden" id="<?=$form_id?>_filter_leave_type_id" name="<?=$form_id?>_filter_leave_type_id" value="<?=$request->leave_type_id?>"/>
        	<input type="hidden" id="<?=$form_id?>_filter_time_period_id" name="<?=$form_id?>_filter_time_period_id" value=""/>
        	<input type="hidden" id="<?=$form_id?>_page_no" name="<?=$form_id?>_page_no" value=""/>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" background="images/Footer.png">
                <tr>
                    <td>
                    	<table width="100%" border="0" cellspacing="1" cellpadding="3">
                        	<tr bgcolor="#FFFFFF">
                        		<td align="center">
                        		Leave Type: 
                                <?php
                                $leave_type_id = $form_id.'_leave_type_id';
                                $js = 'id="'.$leave_type_id.'" style="font-size: 11px; width: 120px;"'; 
								echo form_dropdown($leave_type_id, $leave_types, $request->leave_type_id, $js);
								?>&nbsp;&nbsp;&nbsp;&nbsp;
                        		Time period: 
								<?php
                                $time_period_id = $form_id.'_time_period_id';
                                $js = 'id="'.$time_period_id.'" style="font-size: 11px; width: 120px;"'; 
								echo form_dropdown($time_period_id, time_period(), '', $js);
								?>
								<input type="button" value="search" onclick="search('<?=$form_id?>');" />
								</td>
                            </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="1" cellpadding="3" id="list_<?=$form_id?>">
                            <tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                            	<td width="20" height="20">#</td>
                                <td width="113"><a href="javascript:void(0)" onclick="sort('<?=$form_id?>','id');">Request ID</a></td>
                                <td width="90"><a href="javascript:void(0)" onclick="sort('<?=$form_id?>','start_date');">Start Date</a></td>
                                <td width="90">End Date</td>
                                <td width="190">Leave Type</td>
                                <td width="117">Requested</td>
                                <td width="190">Reason</td>
                                <td width="90">Request Date</td>
                                <td height="20" align="center" style="color:#FFF">Action</td>
                            </tr>
                            <? 
                            $total_hours = 0;
                            if(!empty($requests)){
                            	$num = 0;
                            foreach ($requests as $row): 
                            	$num++;
                            ?>
                            <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
                                <td height="20"><?=$num?></td>
                                <td>R<?=$row->id?></td>
                                <td><?=(date(DATE_FORMAT, $row->start_day) . " " . $row->start_time)?></td>
                                <td><?=(date(DATE_FORMAT, $row->end_day) . " " . $row->end_time)?></td>
                                <td><?=$row->leave_type?></td>
                                <td>
                                <?
                                echo $row->hours_used;
                                $total_hours += $row->hours_used; 
                                if($row->leave_type_id == LEAVE_TYPE_LUNCH
                                	|| $row->leave_type_id == LEAVE_TYPE_LATE){
                                	echo " Minutes";
                                }else{
                                	echo " Hours";
                                }
                                ?>
                                </td>
                                <td><?=$row->reason?></td>
                                <td><?=date(DATETIME_FORMAT,$row->add_time)?></td>
                                <td align="center">
                                	<?
                                	if($row->state == 10){
                                		?>
                                		<a href="<?=site_url('request/edit/'.$row->id)?>">edit</a>&nbsp;
                                		<a href="javascript:void(0)" onclick="cancel_request(<?=$row->id?>);">cancel</a>
                                		<?
                                	}else if($row->state == 20 || $row->state == 30){
                                		?>
                                		<a href="<?=site_url('request/view/'.$row->id)?>">view</a>&nbsp;
                                		<a href="javascript:void(0)" onclick="cancel_request('<?=$form_id?>', <?=$row->id?>);">cancel</a>
                                		<?
                                	}else{
                                	?>
                                		<a href="<?=site_url('request/view/'.$row->id)?>">view</a>&nbsp;
                                	<?
                                	}
                                	?>
                                </td>
                            </tr>
                            <? 
                            endforeach;
                            ?>
                            <tr bgcolor="#FFFFFF" font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                            <td align="right" colspan="5">
                            	Total:
                            </td>
                            <td>
								<?php echo $total_hours;?>
                            </td>
                                <td align="right" colspan="3">
                                <?php 
                                	$space = "&nbsp;&nbsp;&nbsp;";

                            		if($page_count > 1){
	//                            		if($page_count > 1){
	//                                		echo $page_no ." / " . $page_count.$space;
	//                                	}
	//                                	if($page_no == 1){
	//                                		echo "First";
	//                                	}else{
	//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(1);'>First</a>";
	//                                	}
	                                	echo $space;
	                                	if($page_no > 1){
	                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . ($page_no - 1) . ");'>Pre.</a>";
	                                	}else{
	                                		echo "Pre.";
	                                	}
	                                	echo $space;
	                                	if($page_no + 1 <= $page_count){
	                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . ($page_no + 1) . ");'>Next</a>";
	                                	}else{
	                                		echo "Next";
	                                	}
	//                                	echo $space;
	//                                	if($page_no == $page_count || $page_count == 0){
	//                                		echo "Last";
	//                                	}else{
	//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . $page_count . ")'>Last</a>";
	//                                	}                            			
                            			echo $space. "GoTo:";
                            			$page_dropdown_id = $form_id.'_page_dropdown';
		                                $js = 'id="'.$page_dropdown_id.'" style="font-size: 11px;" onChange="gotopage(\''.$form_id.'\', this.value)"';
		                                $page_dropdown = array();
		                                for($i = 1; $i <= $page_count; $i++){
		                                	$page_dropdown[$i] = $i;
		                                }
										echo form_dropdown($page_dropdown_id, $page_dropdown, $page_no, $js);
                                	}
                                	echo $space. "Items Per Page:";
                                	$page_limit_id = $form_id.'_limit';
                                	$js = 'id="'.$page_limit_id.'" style="font-size: 11px;" onChange="pagelimit(\''.$form_id.'\')"';
                                	echo form_dropdown($page_limit_id, pagination_option(), PAGE_SIZE, $js);
                                	
                                ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                            </tr>
                            
                            <?
                            }else{
                            ?>
							<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
                                <td height="20" colspan="9">No record</td>
                            </tr>
                            <?php }?>
                        </table>
                    </td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
</table>
	</div><!-- end Leave History -->
	<div class="pane" id="colleague"> 


	</div>
	<div class="pane" style="display:block" id="tab_waiting"></div>


	
	<div class="pane" style="display:block" id="tab_upcoming"></div>
	
	<div class="pane" style="display:block" id="tab_logs">
        	<?
            if(count($request_logs) > 0){
            ?>
            <table width="100%" border="0" cellspacing="1" cellpadding="1">
                <tr>
                    <td width="50%" height="20" colspan="5" align="left"><strong>Request Log</strong></td>
                </tr>
                <tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                    <td width="20" height="20">#</td>
                    <td width="100">Action</td>
                    <td width="100">Actioned On</td>
                    <td width="117">Actioned By</td>
                    <td width="20">IP</td>
                </tr>
                <? 
                    $num = 0;
                    foreach ($request_logs as $row): 
                    $num++;
                    ?>
                    <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
	                    <td height="20"><?=$num?></td>
	                    <td><?=$row['action']?></td>
	                    <td height="20"><?=$row['actioned_on']?></td>
	                    <td><?=$row['actioned_by']?></td>
	                    <td><?=$row['action_ip']?></td>
	                </tr>
                    <? 
                    endforeach;
                    ?>
            </table>
            <?}else{
            	echo "There are no request logs.";
            }?>
        </div>
</div>
<!-- This JavaScript snippet activates those tabs -->
<script type="text/javascript">
$(function() { 
	$("ul.tabs").tabs("div.panes > .pane");
	$("#colleague").load("<?php echo site_url("approve/colleague_approved/".$request->id)?>");
	$("#tab_waiting").load("<?php echo site_url("request/waiting/".$user->user_name)?>");
	$("#tab_upcoming").load("<?php echo site_url("request/approved_upcoming/".$user->user_name)?>");
});


	function check_submit(form){

		if ($('input[name=reason_acceptable]').length ){
			if(typeof($('input[name=reason_acceptable]:checked').val()) == 'undefined' && !$('#reason_acceptable').attr('disabled') ){
				alert("Please select Reason Acceptable/Not Acceptable option.");
				return false;
			}
		}

		if ($('input[name=supervisor_approved]').length ){
			if(typeof($('input[name=supervisor_approved]:checked').val()) == 'undefined' && !$('#supervisor_approved').attr('disabled')){
				alert("Please select Approve/Reject option.");
				return false;
			}
		}
		 
		if ($('input[name=paid]').length ){
			if(typeof($('input[name=paid]:checked').val()) == 'undefined' && !$('#radio_paid').attr('disabled')){
				alert("Please select Paid/Unpaid option.");
				return false;
			}
		}

 		if ($('input[name=approve]').length ){
			if(typeof($('input[name=approve]:checked').val()) == 'undefined' && !$('#approve_approve').attr('disabled')){
				alert("Please select Approve/Reject option. 2");
				return false;
			}
		}
		
		return true;
	}



function sort(form_id, orderby){
	if($("#" + form_id + "_order_by").val() == orderby){
		if($("#" + form_id + "_order").val() == "ASC"){
			$("#" + form_id + "_order").val("DESC");
		}else{
			$("#" + form_id + "_order").val("ASC");
		}
	}else{
		$("#" + form_id + "_order_by").val(orderby);
		$("#" + form_id + "_order").val("ASC");
	}
	//alert($("#colleague_filter_start_date").val());
	//alert($("#colleague_filter_end_date").val());
	$("#" + form_id + "_page_no").val(1);
	return submitForm(form_id);
}

function submitForm(form_id){
	if(form_id == "past_leave"){
		var past_leave_options = {
			dataType:  'json',
			success: past_leave_response,
			url: "<?=site_url('approve/past_leave_ajax')?>"
		};
		$('#'+form_id).ajaxSubmit(past_leave_options);
	}else if(form_id == "colleague"){
		var colleague_options = {
				dataType:  'json',
				success: colleague_response,
				url: "<?=site_url('approve/colleague_approved_ajax')?>"
			};
		$('#'+form_id).ajaxSubmit(colleague_options);
	}else if(form_id == "past_colleague"){
		var past_colleague_options = {
				dataType:  'json',
				success: past_colleague_response,
				url: "<?=site_url('approve/colleague_approved_ajax')?>"
			};
		$('#'+form_id).ajaxSubmit(past_colleague_options);
	}
	else if(form_id == "waiting"){
		var waiting_options = {
			dataType:  'json',
			success: waiting_response,
			url: "<?=site_url('request/waiting_ajax')?>"
		};
		$('#'+form_id).ajaxSubmit(waiting_options);
	}
	
	return false;
}



function past_leave_page(pageNo){
	$("#past_leave_page_no").val(pageNo);
	return submitForm("past_leave");
}

function colleague_page(pageNo){
	$("#colleague_page_no").val(pageNo);
	return submitForm("colleague");
}

function past_leave_response(responseText, statusText, xhr, $form){
	//$("#list_waiting").html(responseText.content);
	$("#list_past_leave").find("tr:gt(0)").remove();
	$('#list_past_leave tr:last').after(responseText.content);
	$("#list_past_leave").find("tr:eq(0)").remove();
}

function colleague_response(responseText, statusText, xhr, $form){
	//$("#list_waiting").html(responseText.content);
	$("#list_colleague").find("tr:gt(0)").remove();
	$('#list_colleague tr:last').after(responseText.content);
	$("#list_colleague").find("tr:eq(0)").remove();
}

function past_colleague_response(responseText, statusText, xhr, $form){
	$("#list_past_colleague").find("tr:gt(0)").remove();
	$('#list_past_colleague tr:last').after(responseText.content);
	$("#list_past_colleague").find("tr:eq(0)").remove();
}

function past_colleague_page(pageNo){
	$("#past_colleague_page_no").val(pageNo);
	submitForm("past_colleague");
}

function waiting_response(responseText, statusText, xhr, $form){
	$("#list_waiting").find("tr:gt(0)").remove();
	$('#list_waiting tr:last').after(responseText.content);
	$("#list_waiting").find("tr:eq(0)").remove();
}

function search(form){
	$("#" + form + "_order_by").val("");
	$("#" + form + "_order").val("");
	$("#" + form + "_filter_leave_type_id").val($("#" + form + "_leave_type_id").val());
	$("#" + form + "_filter_time_period_id").val($("#" + form + "_time_period_id").val());
	$("#" + form + "_page_no").val(1);
	return submitForm(form);
}

function search_colleague(form){
	if(!$("#" + form + "_start_date").val()){
		alert("Please select start date");
		return false;
	}
	if(!$("#" + form + "_end_date").val()){
		alert("Please select end date");
		return false;
	}
	$("#" + form + "_order_by").val("");
	$("#" + form + "_order").val("");
	$("#" + form + "_filter_leave_type_id").val($("#" + form + "_leave_type_id").val());
	$("#" + form + "_filter_department_id").val($("#" + form + "_department_id").val());
	$("#" + form + "_filter_start_date").val($("#" + form + "_start_date").val());
	$("#" + form + "_filter_end_date").val($("#" + form + "_end_date").val());
	$("#" + form + "_page_no").val(1);
	return submitForm(form);
}

function goto_index(){
	window.location="<?=site_url("approve")?>";	
}

function gotopage(form, pageNo){
	$("#" + form + "_page_no").val(pageNo);
	submitForm(form);
}

function pagelimit(form){
	$("#" + form + "_page_no").val(1);
	submitForm(form);
}
</script>
</body>
</html>