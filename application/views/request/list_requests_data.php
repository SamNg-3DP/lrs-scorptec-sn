                            <tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                                <td width="20" height="20">#</td>
                                <td width="113"><a href="javascript:void(0)" onclick="sort('<?=$form_id?>','id');">Request ID</a><?=$sort_id?></td>
                                <td width="90">Request Date</td>
                                <td width="90"><a href="javascript:void(0)" onclick="sort('<?=$form_id?>','start_date');">Start Date</a><?=$sort_start_date?></td>
                                <td width="90">End Date</td>
                                <td width="190">Leave Type</td>
                                <td width="117">Requested</td>
                                <td width="190">Reason</td>
                                <td height="20" align="center" style="color:#FFF">Action</td>
                            </tr>
                            <? 
                            $total_hours = 0;
                            if(!empty($requests)){
                            	$num = 0;
                            foreach ($requests as $row): 
                            	$num++;
                            ?>
                            <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';" onclick="view_request(<?=$row->id?>)">
                                <td height="20"><?=$num?></td>
                                <td>R<?=$row->id?></td>
                                <td><?=date(DATETIME_FORMAT,$row->add_time)?></td>
                                <td><?=(date(DATE_FORMAT, $row->start_day) . " " . $row->start_time)?></td>
                                <td>
                                <?
                                	if($row->end_date > 0){
                                		echo date(DATETIME_FORMAT, $row->end_date);
                                	}
                                ?>
                                </td>
                                <td><?=$row->leave_type?></td>
                                <td>
                                <?

                                if($row->leave_type_id == LEAVE_TYPE_LUNCH
                                	|| $row->leave_type_id == LEAVE_TYPE_LATE){
									echo round(($row->hours_used*60),2). " Minutes";
									$total_hours += ($row->hours_used*60);
                                }else{
									echo $row->hours_used. " Hours";
									$total_hours += $row->hours_used;
                                }
                                ?>
                                </td>
                                <td><?=$row->reason?></td>
                                <td align="center">
                                	<?
                                	if($row->state == 10){
                                		?>
                                		<a href="<?=site_url('request/edit/'.$row->id)?>">edit</a>&nbsp;
                                		<a href="javascript:void(0)" onclick="cancel_request(<?=$row->id?>);">cancel</a>
                                		<?
                                	}else if($row->state == 20 || $row->state == 30){
                                		?>
                                		<a href="<?=site_url('request/view/'.$row->id)?>">view</a>&nbsp;
                                		<a href="javascript:void(0)" onclick="cancel_request('<?=$form_id?>', <?=$row->id?>);">cancel</a>
                                		<?
                                	}else{
                                	?>
                                		<a href="<?=site_url('request/view/'.$row->id)?>">view</a>&nbsp;
                                	<?
                                	}
                                	?>
                                </td>
                            </tr>
                            <? 
                            endforeach;
                            ?>
                            <tr bgcolor="#FFFFFF" font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                            <td align="right" colspan="6">
                            	Total:
                            </td>
                            <td>
								<?php echo round($total_hours,2);?>
                            </td>
                                <td align="right" colspan="2">
                                <?php 
                                	$space = "&nbsp;&nbsp;&nbsp;";

                                	if($page_count > 1){
                                		//                                	if($page_count > 1){
//                                		echo $page_no ." / " . $page_count.$space;
	//                                	}
	//                                	if($page_no == 1){
	//                                		echo "First";
	//                                	}else{
	//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(1);'>First</a>";
	//                                	}
	//                                	echo $space;
	                                	if($page_no > 1){
	                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . ($page_no - 1) . ");'>Pre.</a>";
	                                	}else{
	                                		echo "Pre.";
	                                	}
	                                	echo $space;
	                                	if($page_no+1 <= $page_count){
	                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . ($page_no + 1) . ");'>Next</a>";
	                                	}else{
	                                		echo "Next";
	                                	}
	//                                	echo $space;
	//                                	if($page_no == $page_count || $page_count == 0){
	//                                		echo "Last";
	//                                	}else{
	//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . $page_count . ")'>Last</a>";
	//                                	}
                            			echo $space;
                            			$page_dropdown_id = $form_id.'_page_dropdown';
		                                $js = 'id="'.$page_dropdown_id.'" style="font-size: 11px;" onChange="gotopage(\''.$form_id.'\', this.value)"';
		                                $page_dropdown = array();
		                                for($i = 1; $i <= $page_count; $i++){
		                                	$page_dropdown[$i] = $i;
		                                }
		                                echo "GoTo:&nbsp;";
										echo form_dropdown($page_dropdown_id, $page_dropdown, $page_no, $js);
                                	}
                                	echo $space. "Items Per Page:&nbsp;";
                                	$page_limit_id = $form_id.'_limit';
	                                $js = 'id="'.$page_limit_id.'" style="font-size: 11px;" onChange="pagelimit(\''.$form_id.'\')"';
	                                echo form_dropdown($page_limit_id, pagination_option(), $page_limit, $js);
                                ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                            </tr>
                            
                            <?
                            }else{
                            ?>
							<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
                                <td height="20" colspan="9">No record</td>
                            </tr>
                            <?php }?>