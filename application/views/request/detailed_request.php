<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<base href="<?=base_url()?>"/>
<style>
#input, select
{
	font-family: Tahoma, Helvetica, Sans-Serif;
	font-size: 11px;
	font-style: normal;
	font-weight: normal;
}
</style>
<script type="text/javascript" src="js/piwik-lrs.js"></script>
</head>

<body onload="document.forms[0].departmentname.focus();">
<form action="index.php/request/close" method="post" enctype="multipart/form-data" name="form1" id="form1">
  <table width="100%" height="22" border="0" align="center" cellpadding="0" cellspacing="0" background="images/Footer.png">
    <tr>
      <td>
      <table width="100%" height="295" border="0" cellspacing="1" cellpadding="1">
        <tr bgcolor="#EBEBEB">
            <td width="50%" height="20" colspan="2" align="left" bgcolor="#EBEBEB">Leave Request Information:</td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td width="20%" height="20" align="right">Leave Type:</td>
          <td width="80%" height="20"><label>
            <select name="select" id="select" style="font-size:11px;background-color:#F5F5F5;" disabled="disabled">
              <option value="Annual Leave" selected="selected">Annual Leave</option>
              <option value="Unpaid">Unpaid</option>
              <option value="Personal (Sick, Carer’s, Compassionate)">Personal (Sick, Carer’s, Compassionate)</option>
              <option value="Approved Training">Approved Training</option>
              <option value="Late">Late</option>
            </select>
          </label></td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td height="20" align="right">Start Day:</td>
          <td height="20"><label>
            <input type="text" name="fromdate" id="fromdate" style="font-size:11px;background-color:#F5F5F5;" disabled="disabled"/>
          </label></td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td height="20" align="right">End Day:</td>
          <td height="20"><label>
            <input type="text" name="todate" id="todate" style="font-size:11px;background-color:#F5F5F5;" disabled="disabled"/>
          </label></td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td height="20" align="right">Start Time:</td>
          <td height="20"><label>
            <select name="select2" id="select2" style="font-size:11px;background-color:#F5F5F5;" disabled="disabled">
            <option value="6:00">6:00</option>
                <option value="6:30">6:30</option>
                <option value="7:00">7:00</option>
                <option value="7:30">7:30</option>
                <option value="8:00">8:00</option>
                <option value="8:30">8:30</option>
                <option value="9:00">9:00</option>
                <option value="9:30" selected="selected">9:30</option>
                <option value="10:00">10:00</option>
                <option value="10:30">10:30</option>
                <option value="11:00">11:00</option>
                <option value="11:30">11:30</option>
                <option value="12:00">12:00</option>
                <option value="12:30">12:30</option>
                <option value="13:00">13:00</option>
                <option value="13:30">13:30</option>
                <option value="14:00">14:00</option>
                <option value="14:30">14:30</option>
                <option value="15:00">15:00</option>
                <option value="15:30">15:30</option>
                <option value="16:00">16:00</option>
                <option value="16:30">16:30</option>
                <option value="17:00">17:00</option>
                <option value="17:30">17:30</option>
                <option value="18:00">18:00</option>
                <option value="18:30">18:30</option>
                <option value="19:00">19:00</option>
                <option value="19:30">19:30</option>
                <option value="20:00">20:00</option>
                <option value="20:30">20:30</option>
                <option value="21:00">21:00</option>
                <option value="21:30">21:30</option>
                <option value="22:00">22:00</option>
                <option value="22:30">22:30</option>
                <option value="23:00">23:00</option>
                <option value="23:30">23:30</option>
            </select>
          </label></td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td height="20" align="right">End Time:</td>
          <td height="20"><label>
            <select name="select3" id="select3" style="font-size:11px;background-color:#F5F5F5;" disabled="disabled">
            <option value="6:00">6:00</option>
                <option value="6:30">6:30</option>
                <option value="7:00">7:00</option>
                <option value="7:30">7:30</option>
                <option value="8:00">8:00</option>
                <option value="8:30">8:30</option>
                <option value="9:00">9:00</option>
                <option value="9:30" selected="selected">9:30</option>
                <option value="10:00">10:00</option>
                <option value="10:30">10:30</option>
                <option value="11:00">11:00</option>
                <option value="11:30">11:30</option>
                <option value="12:00">12:00</option>
                <option value="12:30">12:30</option>
                <option value="13:00">13:00</option>
                <option value="13:30">13:30</option>
                <option value="14:00">14:00</option>
                <option value="14:30">14:30</option>
                <option value="15:00">15:00</option>
                <option value="15:30">15:30</option>
                <option value="16:00">16:00</option>
                <option value="16:30">16:30</option>
                <option value="17:00">17:00</option>
                <option value="17:30">17:30</option>
                <option value="18:00">18:00</option>
                <option value="18:30">18:30</option>
                <option value="19:00">19:00</option>
                <option value="19:30">19:30</option>
                <option value="20:00">20:00</option>
                <option value="20:30">20:30</option>
                <option value="21:00">21:00</option>
                <option value="21:30">21:30</option>
                <option value="22:00">22:00</option>
                <option value="22:30">22:30</option>
                <option value="23:00">23:00</option>
                <option value="23:30">23:30</option>
            </select>
          </label></td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td height="20" align="right">Hours Used:</td>
          <td height="20"><label>
            <input name="textfield3" type="text" id="textfield3" style="background-color:#F5F5F5; font-size:11px" size="10" disabled="disabled"/>
          </label></td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td height="20" align="right">Reason:</td>
          <td height="20"><label>
            <textarea name="textarea" id="textarea" cols="45" rows="5" style="font-size:11px;background-color:#F5F5F5;" disabled="disabled"></textarea>
          </label></td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td height="20" align="right">Medical Certificate (MC):</td>
          <td height="20"><label>
            <input type="file" name="fileField" id="fileField" style="font-size:11px; background-color:#F5F5F5" disabled="disabled"/>
          </label></td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td height="20" colspan="2" align="left" bgcolor="#EBEBEB">HOD:</td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td height="20" colspan="2" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="2%" height="20"><input type="checkbox" name="checkbox3" style="font-size:11px; background-color:#F5F5F5" id="checkbox3" disabled="disabled"/></td>
              <td width="21%" height="20">M.C. Provided</td>
              <td width="2%" height="20"><input type="checkbox" name="checkbox4" style="font-size:11px; background-color:#F5F5F5" id="checkbox4" disabled="disabled"/></td>
              <td width="28%" height="20">M.C. Not Provided</td>
              <td width="2%" height="20">&nbsp;</td>
              <td width="45%" height="20">&nbsp;</td>
            </tr>
            <tr>
              <td height="20"><input type="checkbox" name="checkbox" id="checkbox" style="font-size:11px; background-color:#F5F5F5" disabled="disabled"/></td>
              <td height="20">Approved/Reason Acceptable</td>
              <td height="20"><input type="checkbox" name="checkbox2" id="checkbox2" style="font-size:11px; background-color:#F5F5F5" disabled="disabled"/></td>
              <td height="20">Not Approved/Reason Not Acceptable</td>
              <td height="20"><input type="checkbox" name="checkbox5" id="checkbox5" style="font-size:11px; background-color:#F5F5F5" disabled="disabled"/></td>
              <td height="20">Make up</td>
            </tr>
          </table></td>
          </tr>
        <tr bgcolor="#FFFFFF">
          <td height="20" colspan="2" align="left" bgcolor="#EBEBEB">Approval:</td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td height="20" colspan="2" align="left" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="2%" height="20"><input type="checkbox" name="checkbox6" id="checkbox6" style="font-size:11px; background-color:#F5F5F5" disabled="disabled"/></td>
              <td width="21%" height="20">Approved</td>
              <td width="2%" height="20"><input type="checkbox" name="checkbox6" id="checkbox7" style="font-size:11px; background-color:#F5F5F5" disabled="disabled"/></td>
              <td width="75%" height="20">Not Approved</td>
              </tr>
            <tr>
              <td height="20"><input type="checkbox" name="checkbox6" id="checkbox8" style="font-size:11px; background-color:#F5F5F5" disabled="disabled"/></td>
              <td height="20">Paid</td>
              <td height="20"><input type="checkbox" name="checkbox6" id="checkbox9" style="font-size:11px; background-color:#F5F5F5" disabled="disabled"/></td>
              <td height="20">Unpaid</td>
              </tr>
          </table></td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td height="20" colspan="2" align="center" bgcolor="#EBEBEB"><label>
            <input type="submit" name="button" id="button" value="     Close     " style="font-size:11px"/>
          </label></td>
          </tr>
      </table></td>
    </tr>
  </table>
</form>
</body>
</html>
