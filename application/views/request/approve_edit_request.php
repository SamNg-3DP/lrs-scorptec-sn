<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Change Leave Request</title>
<base href="<?=base_url()?>" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />
<script type="text/javascript" src="js/mootools.v1.11.js"></script>
<script type="text/javascript" src="js/nogray_time_picker_min.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
//<![CDATA[

jQuery.noConflict(); 

window.addEvent("domready", function (){
var tp = new TimePicker('start_time_picker', 'start_time', 'start_time_toggler', {format24:true});
new TimePicker('end_time_picker', 'end_time', 'end_time_toggler', {format24:true});
new TimePicker('arrival_time_picker', 'arrival_time', 'arrival_time_toggler', {format24:true});
});

function goto_index(){
	window.location="<?=site_url("approve")?>";	
}

function count(cal) {
	var request_full_day = document.getElementById('request_full_day');
	var request_partial_day = document.getElementById('request_partial_day');
	if(request_full_day.checked){
		var start_day = document.getElementById("start_day").value;
	    var end_day = document.getElementById("end_day").value;
	    if(start_day && end_day){
			var start = start_day.split('-');
			var dstart = new Date(start[2], start[1], start[0]);
			var end = end_day.split('-');
			var dend = new Date(end[2], end[1], end[0]);
			if(dstart.getTime() > dend.getTime()){
	        	alert("Start day shouldn't be later than end day.");        
	        	return false;
			}
			if(cal){
				calculate();
			}
			
	    }
	}
	if(request_partial_day.checked){
		var start_day = document.getElementById("start_day").value;
	    var start_time = document.getElementById("start_time").value;
	    var end_time = document.getElementById("end_time").value;
	    
	    if(start_day){
	        if(start_time && end_time && start_time > end_time){
	            alert("Start date shouldn't be later than end date.");
	            return false;
	        }
	        if(start_time && end_time && start_time == end_time){
	            alert("Leave time must be greater than zero!");
	            return false;
	        }
	        if(start_day && start_time && end_time){
		    	if(cal){
					calculate();
				}
		    }
	    }
	}
    return true;
}

function changeLeaveType(value){
	if(value == <?=LEAVE_TYPE_LATE?>
		|| value == "<?=LEAVE_TYPE_LUNCH?>"){
        jQuery("#common_type").hide();
        jQuery("#medicalCertificate").hide();
        jQuery("#late_type").show();
    }else{
    	jQuery("#common_type").show();
        jQuery("#late_type").hide();
        jQuery("#medicalCertificate").hide();
    	if(value == "<?=LEAVE_TYPE_PERSONAL?>"){
            jQuery("#medicalCertificate").show();
        }
    }
}

function fullDayClick(value){
	if(value == 1){
		jQuery("#start_time_type").hide();
		jQuery("#end_time_type").hide();
		jQuery("#include_break_type").hide();
	    jQuery("#end_day_type").show();
	}else if(value == 2){
		jQuery("#end_day_type").hide();
		jQuery("#start_time_type").show();
		jQuery("#end_time_type").show();
		jQuery("#include_break_type").show();
	}
}

function breakClick(){
	count();
}

function calculate(){
	alert(1);
	var request_full_day = document.getElementById('request_full_day');
	var request_partial_day = document.getElementById('request_partial_day');
	var start_day = "";
	var end_day = "";
	var start_time = "";
	var end_time = "";
	var include_break = false;
	var request_type = 0;
	
	if(request_full_day.checked){
		start_day = jQuery("#start_day").val();
		end_day = jQuery("#end_day").val();
		start_time = "00:00";
		end_time = "23:59";
		request_type = 1;
	}else if(request_partial_day.checked){
		start_day = jQuery("#start_day").val();
		end_day = start_day;
		start_time = jQuery("#start_time").val();
		end_time = jQuery("#end_time").val();
		var include_break_ojb = document.getElementById('include_break');
		include_break = include_break_ojb.checked;
		request_type = 2;
	}

	jQuery.ajax({
		async: false,
		type: 'POST',
		url: "<?=site_url("request/calculate_working_hours_ajax")?>",
		data: {
		  	start_day: start_day, 
		  	start_time: start_time,
		  	end_day: end_day,
		  	end_time: end_time,
		  	request_type: request_type,
		  	include_break: include_break
		},
		success: function(data){
			jQuery("#hours_used").val(data.used_hours_count);
			/*
			if(data.status == "ok"){
				alert(data.used_hours_count);
				jQuery("#hours_used").val(data.used_hours_count);
			}
			*/
	  	},
		dataType: 'json'
	});
}

function calculate_late(){
	jQuery.ajax({
		async: false,
		type: 'POST',
		url: "<?=site_url("request/calculate_minutes_late_ajax")?>",
		data: {
			arrival_day: jQuery("#arrival_day").val(), 
			arrival_time: jQuery("#arrival_time").val()
		},
		success: function(data){
			if(data.status == "ok"){
				jQuery("#minutes_used").val(data.used_minutes_count);
			}
	  	},
		dataType: 'json'
	});
}

function submit_click(value){
	var is_submit = false;
    var leave_type = jQuery("#leave_type").val();
	if(!leave_type){
		alert("Please select leave type!");
		return false;
	}

	if(leave_type == "<?=LEAVE_TYPE_LATE?>"
		|| leave_type == "<?=LEAVE_TYPE_LUNCH?>"){
		var arrival_day = jQuery("#arrival_day").val();
		if(!arrival_day){
			alert("Please select arrival day!");
			return false;
		}
		var arrival_time = jQuery("#arrival_time").val();
		if(!arrival_time){
			alert("Please select arrival time!");
			return false;
		}
		if(!isTime(arrival_time)){
			return false;
		}
		var minutes_used = jQuery("#minutes_used").val();
		if(minutes_used){
			if(!isNumber(minutes_used)){
				alert("Please enter a valid number!");
				return false;
			}
		}else{
			alert("Please enter minutes used!");
			return false;
		}

		var reason = jQuery("#reason").val();
		if(jQuery.trim(reason) == ""){
			alert("Please enter the reason");
			return false;
		}
		var same = calculate_same();
		if(same){
			return false;
		}
		
		if(value == 'apply'){
			if(confirm("Do you confirm request " + jQuery("#minutes_used").val() + " minutes leave?") ){
				is_submit = true;
			}
		}else{
			is_submit = true;
		}
	}else{
		var request_full_day = document.getElementById('request_full_day');
		var request_partial_day = document.getElementById('request_partial_day');
		var start_day = jQuery("#start_day").val();
		if(!start_day){
			alert("Please select start day!");
			return false;
		}

		if(request_full_day.checked){
			var end_day = jQuery("#end_day").val();
			if(!end_day){
				alert("Please select end day!");
				return false;
			}
		}else if(request_partial_day.checked){
			var start_time = jQuery("#start_time").val();
			if(!start_time){
				alert("Please select start time!");
				return false;
			}
			
		    var end_time = jQuery("#end_time").val();
		    if(!end_time){
				alert("Please select end time!");
				return false;
			}
		}
		
	    if(!count(false)){
	        return false;
		}
	    //calculate();
		hours_used = jQuery("#hours_used").val();
		
		if(hours_used =="" || hours_used == 0){
			alert("The hours used is 0 and can't be saved or applied, please try again! ");
			return false;
		}
		if(leave_type == "<?=LEAVE_TYPE_ANNUAL?>"){
			var available = calculate_available(leave_type);
			if(hours_used > available){
				alert("You are requesting for annual leave is more than your annual balance available. You have " + available+ " hours available.");
				return false;
			}
		}else if(leave_type == "<?=LEAVE_TYPE_PERSONAL?>"){
			var available = calculate_available(leave_type);
			if(hours_used > available){
				alert("You are requesting for personal leave is more than your annual balance available. You have " + available+ " hours available.");
				return false;
			}
		}

		var reason = jQuery("#reason").val();
		if(jQuery.trim(reason) == ""){
			alert("Please enter the reason");
			return false;
		}

		var same = calculate_same();
		if(same){
			return false;
		}
		if(value == 'apply'){
			if(confirm("Do you confirm request " + jQuery("#hours_used").val() + " hours leave?") ){
				is_submit = true;
			}
		}else{
			is_submit = true;
		}
	}
	if(is_submit){
		jQuery("#operate").val(value);
		
		var theForm = document.getElementById("edit_request");
		theForm.submit();
		//jQuery('#apply').attr('disabled','true');
		//jQuery('#save').attr('disabled','true');
	}
	return false;
}

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function isTime(strTime) {
	var strSeparator = ":";
	var intHour;
	var intMinute;
	var strTimeArray = strTime.split(strSeparator);
	if(strTime.length > 5 || strTimeArray.length!=2){   
		alert('Prompt\r\nPlease enter correct format time,E.g. HH:mm');
		return false;   
	}

	intHour = parseInt(strTimeArray[0],10);
	intMinute = parseInt(strTimeArray[1],10);

	if(!isNumber(strTimeArray[0]) || !isNumber(strTimeArray[1])
		|| !isNumber(intHour) || !isNumber(intMinute) 
		|| intHour < 1 || intHour > 23 
		|| intMinute < 0 || intMinute > 59){
		alert('Prompt\r\nPlease enter correct format time,E.g. HH:mm');
		return false;
	}
	return true;
}

function blurTime(){
	if(!jQuery("#arrival_day").val()){
		alert("Please slect arrival day!");
		return false;
	}
	if(!jQuery("#arrival_time").val()){
		alert("Please enter arrival time!");
		return false;
	}
	if(isTime(jQuery("#arrival_time").val())){
		return calculate_late();
	}
	return true;
}

function edit_response(responseText, statusText, xhr, $form){
	if(responseText.status=='ok'){
		alert(responseText.message);
		window.location="<?=site_url('request')?>";
    }else{
    	alert(responseText.message);
    	return false;
    }
}

function calculate_available(leave_type_value){
	var available = 0;
    jQuery.ajax({
        async: false,
        type: 'POST',
        url: "<?=site_url("request/calculate_available_ajax")?>",
        data: {
            leave_type: leave_type_value
        },
        success: function(data){
            if(data.status == "ok"){
            	available = data.available;
            }
        },
        dataType: 'json'
    });
    return available;
}

function calculate_same(){
	var start_date = "";
	var end_date = "";
	var leave_type = jQuery("#leave_type").val();
	if(!leave_type){
		alert("Please select leave type!");
		return false;
	}
	if(leave_type == "<?=LEAVE_TYPE_LATE?>"
		|| leave_type == "<?=LEAVE_TYPE_LUNCH?>"){
		var arrival_day = jQuery("#arrival_day").val();
		var arrival_time = jQuery("#arrival_time").val();
		start_date = arrival_day + " " + arrival_time;
		end_date = arrival_day + " 23:59:59";		
	}else{
		var request_full_day = document.getElementById('request_full_day');
		var request_partial_day = document.getElementById('request_partial_day');
		var start_day = jQuery("#start_day").val();		
		if(request_full_day.checked){
			start_date = start_day;	
			end_date = jQuery("#end_day").val();
		}else if(request_partial_day.checked){
			start_date = start_day + "" + jQuery("#start_time").val();			
			end_date = start_day + "" + jQuery("#end_time").val();
		}
	}
	
	var same = false;
    jQuery.ajax({
        async: false,
        type: 'POST',
        url: "<?=site_url("request/calculate_same_request_ajax")?>",
        data: {
            leave_type: leave_type,
            start_date: start_date,
            end_date: end_date,
            request_id: jQuery("#request_id").val(),
        },
        success: function(data){
            if(data.count > 0){
            	//
            	alert("You have request a same leave type in current time period.");
            	same = true;
            }
        },
        dataType: 'json'
    });
    return same;
}

jQuery(function() {
    jQuery("#start_day").datepicker({ 
    	disabled: true, 
    	dateFormat: '<?=JS_DATE_FORMAT?>',
    	changeMonth: true,
		changeYear: true 
	});
    jQuery("#end_day").datepicker({ 
    	disabled: true, 
    	dateFormat: '<?=JS_DATE_FORMAT?>',
    	changeMonth: true,
		changeYear: true 
	});
    changeLeaveType(<?=$request->leave_type_id?>);
    <?php 
    if($request->leave_type_id == LEAVE_TYPE_LATE
    	|| $request->leave_type_id == LEAVE_TYPE_LUNCH){
    }else{
    	?>
    	fullDayClick(<?=$request->request_type?>);
    	<?
    }
    ?>
    
    jQuery("ul.tabs").tabs("div.panes > .pane");
});
//]]>
</script>
    </head>

    <body>
    <div class="wrap">
	  <!-- the tabs --> 
	  <ul class="tabs">
	    <li><a href="#Update">Update</a></li>
	    <li><a href="#Logs">Logs</a></li> 
	  </ul>
	  <!-- tab "panes" --> 
	  <div class="panes">
	    <div class="pane" style="display:block" id="tab_update">
        <?php 
        echo form_open_multipart('approve/do_approve_edit', array('id'=>'edit_request', 'name'=>'edit_request'));
        
        echo form_hidden("state", $request->state);
        ?>
        <input type="hidden" id="operate" name="operate" value=""/>
        <input type="hidden" id="request_id" name="request_id" value="<?=$request->id?>"/>
        <table width="100%" height="22" border="0" align="center" cellpadding="0" cellspacing="0" background="images/Footer.png">
            <tr>
                <td>
                    <table width="100%" height="50" border="0" cellspacing="1" cellpadding="1">
                        <tr bgcolor="#EBEBEB">
                            <td width="50%" height="20" colspan="2" align="right" bgcolor="#EBEBEB" style="color: red">&nbsp;</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="right">Leave Type:</td>
                            <td width="80%" height="20">
                            	<?php 
                            		$js = 'id="leave_type" style="font-size:11px" onchange="changeLeaveType(this.value)"';
                            		echo form_dropdown('leave_type', $leave_types, $request->leave_type_id, $js);?>
                                    <label><span style="color: #F00; font-weight: bold;">*</span></label><?php echo form_error('leave_type'); ?>
                            </td>
                        </tr>
                     </table>
                     <table width="100%" height="150" border="0" cellspacing="1" cellpadding="1" id="common_type" >
                     	<tr bgcolor="#FFFFFF" id="request_type">
                            <td height="20" align="right"></td>
                            <td height="20" align="left">
                            	<input type="radio" name="request_type" id="request_full_day" value="1" onclick="fullDayClick(1)" 
                                	<?php 
                                	if($request->request_type == 1){
                                		echo ' checked="checked" ';
                                	}
                                	?>
								/>FULL day
                            	<input type="radio" name="request_type" id="request_partial_day" value="2" onclick="fullDayClick(2)"
                            		<?php 
                                    if($request->request_type == 2){
                                        echo ' checked="checked" ';
                                    }
                                    ?>
                            	/>PARTIAL day
                            </td>
                        </tr>
                     	<tr bgcolor="#FFFFFF" id="medicalCertificate" style="display:none">
                            <td height="20" align="right">Medical Certificate (MC):</td>
                            <td height="20">
								<label>
                                    <input type="file" name="medical_certificate" id="medical_certificate" style="font-size:11px;"/>
                                </label>
								<?
								if(!empty($request->medical_certificate_file)){
									$mc_file = $request->medical_certificate_file;
									$mc_link = DIR_MEDICAL_CERTIFICATE . $request->medical_certificate_file;
									
									$mc_view = '<a target="_blank" href="'.$mc_link.'">download</a>';
									echo $mc_view;
								}
								?>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="right">Start Day:</td>
                            <td width="80%" height="20">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="100%">
                                        	<input type="text" name="start_day" id="start_day"
                                            value="<?
                                            if(!($request->leave_type_id == LEAVE_TYPE_LATE || $request->leave_type_id == LEAVE_TYPE_LUNCH)){
                                            	echo date(DATE_FORMAT, $request->start_day);
                                            }
                                            ?>" style="font-size: 11px"
                                            onchange="javascript: count(true);"/>
                                    		<label style="color: #F00">*</label><?php echo form_error('start_day'); ?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF" id="end_day_type">
                            <td height="20" align="right">End Day:</td>
                            <td height="20">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="100%">
                                        	<input type="text" name="end_day" id="end_day"
                                                               value="<?php echo date(DATE_FORMAT, $request->end_day);?>" style="font-size: 11px"
                                                               onchange="javascript: count(true);"/>
                                            <label style="color: #F00">*</label><?php echo form_error('end_day'); ?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF" id="start_time_type">
                            <td height="20" align="right">Start Time:</td>
                            <td height="20">
                            	<input type="text" name="start_time" id="start_time" value="<?=($request->leave_type_id == LEAVE_TYPE_LATE || $request->leave_type_id == LEAVE_TYPE_LUNCH)?"":$request->start_time?>" onchange="count(true);"/> <a href="#" id="start_time_toggler">Open time picker</a>
								<div id="start_time_picker" class="time_picker_div"/>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF" id="end_time_type">
                            <td height="20" align="right">End Time:</td>
                            <td height="20">
                            	<input type="text" name="end_time" id="end_time" value="<?=$request->end_time?>" onchange="count(true);"/> <a href="#" id="end_time_toggler">Open time picker</a>
								<div id="end_time_picker" class="time_picker_div"/>
							</td>
                        </tr>
                        
                        <tr bgcolor="#FFFFFF" id="include_break_type">
                            <td height="20" align="right"></td>
                            <td height="20">
                            	<input type="checkbox" name="include_break" id="include_break" onclick="breakClick()" value="1" 
                            	<?php 
                            	if($request->include_break == 1){
                            		echo "checked='checked'";
                            	}
                            	?>
                            	/> deduct lunch 
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td height="20" align="right">Hours Used:</td>
                            <td height="20">
								<input name="hours_used" type="text" id="hours_used" value="<?=(($request->leave_type_id == LEAVE_TYPE_LATE || $request->leave_type_id == LEAVE_TYPE_LUNCH)?"":$request->hours_used)?>" size="7" />
							</td>
                        </tr>
                 	</table>
                	<table width="100%" height="100" border="0" cellspacing="1" cellpadding="1" id="late_type" style="display:none;">                       
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="right">Arrival Day:</td>
                            <td width="80%" height="20">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="100%">
                                        	<input type="text" name="arrival_day" id="arrival_day"
                                            value="<?=(($request->leave_type_id == LEAVE_TYPE_LATE || $request->leave_type_id == LEAVE_TYPE_LUNCH)?date(DATE_FORMAT, $request->start_day):"")?>" style="font-size: 11px"
                                            onchange="javascript: count(true);"/>
                                    		<label style="color: #F00">*</label><?php echo form_error('arrival_day'); ?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td height="20" align="right">Actual Arrival Time:</td>
                            <td height="20">
                            	<input type="text" name="arrival_time" id="arrival_time" value="<?=(($request->leave_type_id == LEAVE_TYPE_LATE || $request->leave_type_id == LEAVE_TYPE_LUNCH)?$request->start_time:"")?>" onblur="return blurTime();"/> <a href="#" id="arrival_time_toggler">Open time picker</a>
								<div id="arrival_time_picker" class="time_picker_div"/>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td height="20" align="right">Minutes Used:</td>
                            <td height="20">
								<input name="minutes_used" type="text" id="minutes_used" value="<?=(($request->leave_type_id == LEAVE_TYPE_LATE || $request->leave_type_id == LEAVE_TYPE_LUNCH)?$request->hours_used:"")?>" style="font-size:11px; color:#000" size="7"/>
							</td>
                        </tr>
                    </table>
              		<table width="100%" height="50" border="0" cellspacing="1" cellpadding="1">
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="right">Reason:</td>
                            <td width="80%" height="20">
	                        <?php 
							echo form_textarea(array(
								'name' => 'reason',
								'id' => 'reason',
								'value' => $request->reason,
								'cols' => "45",
								'rows' => "5",
								'style'=> "font-size:11px"
							));
							?>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td height="20" colspan="2" align="center" bgcolor="#EBEBEB">
                                <input type="button" name="save" id="save" onclick="goto_index();return false;" value="     Close     " style="font-size:11px"/>
                                <input id="apply" style="font-size: 11px" type="button" name="apply" onclick="submit_click('apply');"  id="apply" value="     Change     "/>
                            </td>
                        </tr>
                    </table>
                    </td>
            </tr>
        </table>
        <?php echo form_close();?>
        </div>
        <div class="pane" style="display:block" id="tab_upcoming">
        	<?
            if(count($request_logs) > 0){
            ?>
            <table width="100%" border="0" cellspacing="1" cellpadding="1">
                <tr bgcolor="#EBEBEB">
                    <td width="50%" height="20" colspan="5" align="left" bgcolor="#EBEBEB" style="color: red">&nbsp;Request Log</td>
                </tr>
                <tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                    <td width="20" height="20">#</td>
                    <td width="100">Action</td>
                    <td width="100">Actioned On</td>
                    <td width="117">Actioned By</td>
                    <td width="20">IP</td>
                </tr>
                <? 
                    $num = 0;
                    foreach ($request_logs as $row): 
                    $num++;
                    ?>
                    <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
	                    <td height="20"><?=$num?></td>
	                    <td><?=$row['action']?></td>
	                    <td height="20"><?=$row['actioned_on']?></td>
	                    <td><?=$row['actioned_by']?></td>
	                    <td><?=$row['action_ip']?></td>              
	                </tr>
                    <? 
                    endforeach;
                    ?>
            </table>
            <?}else{
            	echo "There are no request logs.";
            }?>
        </div>
	</div>
    </body>
</html>