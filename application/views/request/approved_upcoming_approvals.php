<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="90%">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="20" style="text-decoration:underline"><strong>Upcoming</strong></td>
                </tr>
            </table>
            <form id="upcoming" name="upcoming" method="post" action="">
        	<input type="hidden" id="upcoming_order_by" name="upcoming_order_by" value=""/>
        	<input type="hidden" id="upcoming_order" name="upcoming_order" value=""/>
        	<input type="hidden" id="upcoming_filter_department_id" name="upcoming_filter_department_id" value="<?=$department_id?>"/>
        	<input type="hidden" id="upcoming_filter_leave_type_id" name="upcoming_filter_leave_type_id" value=""/>
        	<input type="hidden" id="upcoming_page_no" name="upcoming_page_no" value=""/>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" background="images/Footer.png">
                <tr>
                    <td>
                    	<table width="100%" border="0" cellspacing="1" cellpadding="1">
                        	<tr bgcolor="#FFFFFF">
                        		<td align="center">
                        		Department: 
                                <?php 
                                $js = 'id="upcoming_department_id" style="font-size: 11px; width: 120px;"';
								echo form_dropdown('upcoming_department_id', $departments, $department_id, $js);
								?>  
								&nbsp;&nbsp;Leave Type: 
                                <?php
                                $js = 'id="upcoming_leave_type_id" style="font-size: 11px; width: 120px;"'; 
								echo form_dropdown('upcoming_leave_type_id', $leave_types, '', $js);
								?>
								<input type="button" value="search" onclick="search('upcoming');" />
								</td>
                            </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="1" cellpadding="1" id="list_upcoming">
                            <tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                                <td width="70"><a href="javascript:void(0)" onclick="sort('upcoming','id');">Request ID</a></td>
                                <td width="100"><a href="javascript:void(0)" onclick="sort('upcoming','add_time');">Request Date</a></td>
                                <td width="100"><a href="javascript:void(0)" onclick="sort('upcoming','applicant');">Applicant</a></td>
                                <td width="117"><a href="javascript:void(0)" onclick="sort('upcoming','start_date');">Start Date</a></td>
                                <td width="117">End Date</td>
                                <td width="117"><a href="javascript:void(0)" onclick="sort('upcoming','department_id');">Department</a></td>
                                <td width="117">Hours Requested</td>
                                <td width="190"><a href="javascript:void(0)" onclick="sort('upcoming','leave_type_id');">Leave Type</a></td>
                                <td width="190">Reason</td>
                                <td width="30" height="20" align="center" style="color:#FFF">Action</td>
                            </tr>
                            <? 
                            if(!empty($approvals)){
                            foreach ($approvals as $row): ?>
                            <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
                                <td height="20"><?=$row->id?></td>
                                <td><?=date(DATETIME_FORMAT,$row->add_time)?></td>
                                <td height="20"><?=$row->first_name?> <?=$row->last_name?></td>
                                <td><?=(date(DATETIME_FORMAT, $row->start_date) )?></td>
                                <td><?=(date(DATETIME_FORMAT, $row->end_date) )?></td>
                                <td><?=$row->department?></td>
                                <td>TODO..</td>
                                <td><?=$row->leave_type?></td>
                                <td><?=$row->reason?></td>
                                <td align="center"><a href="<?=site_url("approve/deal/" . $row->id)?>">approve</a></td>
                            </tr>
                            <? 
                            endforeach;
                            ?>
                            <tr bgcolor="#FFFFFF" font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                                <td align="right" colspan="10">
                                <?php 
                                	$space = "&nbsp;&nbsp;&nbsp;";
                                	if($page_no == 1){
                                		echo "First";
                                	}else{
                                		echo "<a href='javascript:void()' onclick='upcoming_page(1);'>First</a>";
                                	}
                                	echo $space;
                                	if($page_no > 1){
                                		echo "<a href='javascript:void()' onclick='upcoming_page(" . ($page_no - 1) . ");'>previous</a>";
                                	}else{
                                		echo "previous";
                                	}
                                	echo $space;
                                	if($page_no < $page_count){
                                		echo "<a href='javascript:void()' onclick='upcoming_page(" . ($page_no + 1) . ");'>next</a>";
                                	}else{
                                		echo "next";
                                	}
                                	echo $space;
                                	if($page_no == $page_count || $page_count == 0){
                                		echo "Last";
                                	}else{
                                		echo "<a href='javascript:void()' onclick='upcoming_page(" . $page_count . ")'>Last</a>";
                                	}
                                ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                            </tr>
                            <?php 
                            }else{
                            ?>
							<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
                                <td height="20" colspan="10">No record</td>
                            </tr>
                            <?php }?>
                        </table>
                    </td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
</table>