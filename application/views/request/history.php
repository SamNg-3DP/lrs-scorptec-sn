<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Request History</title>
<base href="<?=base_url()?>"/>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.tools.tabs.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />
<style type="text/css">
<!--
#form1 table tr td table {
	text-align: left;
}
#form1 p {
	text-align: left;
}
#form1 table tr td table tr td {
    font-family: Tahoma, Geneva, sans-serif;
	font-size: 11px;
}
/* tab pane styling */
div.panes div {
	display:none;		
	padding:15px 10px;
	border:1px solid #999;
	border-top:0;
	font-size:14px;
	background-color:#fff;
}

div.pane {
	display:none;		
	padding:15px 10px;
	border:1px solid #999;
	border-top:0;
	font-size:14px;
	background-color:#fff;
}
-->
</style>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
<!-- This JavaScript snippet activates those tabs -->
function sort(form_id, orderby){
	if($("#" + form_id + "_order_by").val() == orderby){
		if($("#" + form_id + "_order").val() == "ASC"){
			$("#" + form_id + "_order").val("DESC");
		}else{
			$("#" + form_id + "_order").val("ASC");
		}
	}else{
		$("#" + form_id + "_order_by").val(orderby);
		$("#" + form_id + "_order").val("ASC");
	}
	$("#" + form_id + "_page_no").val(1);
	return submitForm(form_id);
}

function submitForm(form_id){
	if(form_id == "saved"){
		var saved_options = {
				dataType:  'json',
				success: saved_response,
				url: "<?=site_url('history/request_ajax')?>"
			};
		$('#'+form_id).ajaxSubmit(saved_options);
	}
	
	return false;
}

function saved_response(responseText, statusText, xhr, $form){
	$("#list_saved").find("tr:gt(0)").remove();
	$('#list_saved tr:last').after(responseText.content);
	$("#list_saved").find("tr:eq(0)").remove();
}

function search(form){
	$("#" + form + "_order_by").val("");
	$("#" + form + "_order").val("");
	$("#" + form + "_filter_request_id").val($("#" + form + "_request_id").val());
	$("#" + form + "_filter_first_name").val($("#" + form + "_first_name").val());
	$("#" + form + "_filter_last_name").val($("#" + form + "_last_name").val());
	$("#" + form + "_filter_from_date").val($("#" + form + "_from_date").val());
	$("#" + form + "_filter_operation").val($("#" + form + "_operation").val());
	$("#" + form + "_page_no").val(1);

	return submitForm(form);
}

function gotopage(form, pageNo){
	$("#" + form + "_page_no").val(pageNo);
	submitForm(form);
}

function pagelimit(form){
	$("#" + form + "_page_no").val(1);
	submitForm(form);
}

function saved_page(pageNo){
	$("#saved_page_no").val(pageNo);
	submitForm("saved");
}

$(function() {
	$("#<?=$form_id?>_from_date").datepicker({ 
		disabled: true, 
		dateFormat: '<?=JS_DATE_FORMAT?>',
		changeMonth: true,
		changeYear: true 
	});	
});

</script>

</head>
<body>

<div id="flashmessage"> <?php echo $this->session->flashdata('message');?> </div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="90%">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="20" style="text-decoration:underline"><strong></strong></td>
                </tr>
            </table>
            <form id="<?=$form_id?>" name="<?=$form_id?>" method="post" action="">
        	<input type="hidden" id="<?=$form_id?>_order_by" name="<?=$form_id?>_order_by" value=""/>
        	<input type="hidden" id="<?=$form_id?>_order" name="<?=$form_id?>_order" value=""/>
        	<input type="hidden" id="<?=$form_id?>_page_no" name="<?=$form_id?>_page_no" value=""/>
        	<input type="hidden" id="<?=$form_id?>_filter_request_id" name="<?=$form_id?>_filter_request_id" value=""/>
        	<input type="hidden" id="<?=$form_id?>_filter_first_name" name="<?=$form_id?>_filter_first_name" value=""/>
        	<input type="hidden" id="<?=$form_id?>_filter_last_name" name="<?=$form_id?>_filter_last_name" value=""/>
        	<input type="hidden" id="<?=$form_id?>_filter_from_date" name="<?=$form_id?>_filter_from_date" value=""/>
        	<input type="hidden" id="<?=$form_id?>_filter_operation" name="<?=$form_id?>_filter_operation" value=""/>
        	<table width="100%" border="0" cellspacing="1" cellpadding="1">
         		<tr>
                	<td width="8%" height="20" bgcolor="#FFFFFF" align="right">Request ID:</td>
                	
                	<td width="20%" height="20" bgcolor="#FFFFFF"><input type="text" name="<?=$form_id?>_request_id" id="<?=$form_id?>_request_id" style="font-size:11px"/></td>
                	<td height="20" bgcolor="#FFFFFF">&nbsp;<input type="button" value="search" onclick="search('<?=$form_id?>');" /></td>
                	<!--<td width="8%" height="20" bgcolor="#FFFFFF" align="right">Handler:</td>
                	<td height="20" bgcolor="#FFFFFF">First Name<input type="text" name="<?=$form_id?>_first_name" id="<?=$form_id?>_first_name" style="font-size:11px"/></td>
                	<td height="20" bgcolor="#FFFFFF" align="left">Last Name<input type="text" name="<?=$form_id?>_last_name" id="<?=$form_id?>_last_name" style="font-size:11px"/></td>
                	-->
            	</tr>
                <!--<tr>
                	<td width="8%" height="20" bgcolor="#FFFFFF" align="right">Handle Time:</td>
                	<td width="20%" height="20" bgcolor="#FFFFFF"><input type="text" name="<?=$form_id?>_from_date" id="<?=$form_id?>_from_date" style="font-size:11px"/></td>
                	<td height="20" bgcolor="#FFFFFF" align="right">Action:</td>
                	<td height="20" bgcolor="#FFFFFF">
                	<?php 
                    $operation = $form_id.'_operation';
                    $js = 'id="'.$operation.'" style="font-size: 11px; width: 120px;"';
                    
                    echo form_dropdown($operation, $operations, '', $js);
                    ?>
                	
                	</td>
                	
            	</tr>-->
            </table>
            <table width="100%" border="0" cellspacing="1" cellpadding="1" id="list_<?=$form_id?>">
                <tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                    <td width="20" height="20">#</td>
                    <td width="70">Request ID</td>
                    <td width="100">Action</td>
                    <td width="100">Actioned On</td>
                    <td width="117">Actioned By</td>
                </tr>
                <? 
                if(!empty($approvals)){
                    $num = 0;
                foreach ($approvals as $row): 
                    $num++;
                ?>
                <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
                    <td height="20"><?=$num?></td>
                    <td height="20">R<?=$row->id?></td>
                    <td><?=$row->first_name?> <?=$row->last_name?></td>
                    <td height="20"><?=date(DATETIME_FORMAT,$row->operate_time)?></td>
                    <td><?=$row->approve?></td>                   
                </tr>
                <? 
                endforeach;
                ?>
                <tr bgcolor="#FFFFFF" font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                    <td align="right" colspan="5">
                    <?php 
                    	$space = "&nbsp;&nbsp;&nbsp;";
                		
                    	echo $space. "Items Per Page:";
                    	$page_limit_id = $form_id.'_limit';
                    	$js = 'id="'.$page_limit_id.'" style="font-size: 11px;" onChange="pagelimit(\''.$form_id.'\')"';
                    	echo form_dropdown($page_limit_id, pagination_option(), PAGE_SIZE, $js);
                    	
                    ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <?php 
                }else{
                ?>
                <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
                    <td height="20" colspan="5">&nbsp;</td>
                    <?
               	 	$page_limit_id = $form_id.'_limit';
                	?>
                	<input type="hidden" name="<?=$page_limit_id?>" id="<?=$page_limit_id?>" value="<?=PAGE_SIZE?>" />
                </tr>
                <?php }?>
            </table>
            </form>
        </td>
    </tr>
</table>



</body>
</html>