<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Approve leave request</title>
<base href="<?=base_url()?>"/>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />
<style>
#input, select
{
	font-family: Tahoma, Helvetica, Sans-Serif;
	font-size: 11px;
	font-style: normal;
	font-weight: normal;
}

/* tab pane styling */
div.panes div {
	display:none;
	padding:15px 10px;
	border:1px solid #999;
	border-top:0;
	font-size:14px;
	background-color:#fff;
}

</style>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
</head>

<body>
<?
//= @flash_message()
echo $this->session->flashdata('message');
?>

<ul class="tabs">
	<li><a href="#request">Request Info</a></li>
    <li><a href="#history">Leave History</a></li>
</ul>
<div class="panes">

<div class="pane" style="display:block" >
<strong><?php echo $user->first_name. " " . $user->last_name;?></strong>
<br/><br/>
<table width="70%" height="100" border="0" cellspacing="0" cellpadding="0" background="images/Footer.png">
    <tr>
      <td valign="top">
      <table width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr>
            <td width="30%" height="20">
	            <table bgcolor="#FFFFFF" width="100%" border="0" cellspacing="0" cellpadding="3">
	              <tr style="background-image:url(images/TableHeader.png);" height="20">
	              	<td colspan="2" style="font-weight: bold;">Annual Leave</td>
	              </tr>
	              <tr >
	                <td width="55%" height="20">Currently Available:</td>
	                <td><?=$annual_available?> Hours</td>
	              </tr>
	              <tr>
	                <td width="55%" height="20">Upcoming Leave Request:</td>
	                <td><?=$annual_upcoming?> Hours</td>
	              </tr>
	              <tr>
	                <td width="55%" height="20">Total Balance:</td>
	                <td><?=$total_annual?> Hours</td>
	              </tr>
	              <tr>
	              	<td width="55%" height="20">Total In Lieu:</td>
	                <td><?=$total_inlieu?> Hours</td>
	              </tr>
	              <tr>
	                <td width="55%" height="20">Upcoming Unpaid:</td>
	                <td><?=$unpaid_upcoming?> Hours</td>
	              </tr>

	            </table>
            </td>
            <td width="40%" height="20">
			<table bgcolor="#FFFFFF" width="100%" border="0" cellspacing="0" cellpadding="3">
				<tr style="background-image:url(images/TableHeader.png);" height="20">
					<td colspan="2" style="font-weight: bold;">Personal Leave</td>
				</tr>
				<tr >
				  <td width="55%" height="20">Currently Available:</td>
				  <td><?=$personal_available?> Hours</td>
				</tr>
				<tr>
				  <td width="55%" height="20">Upcoming Leave Request:</td>
				  <td><?=$personal_upcoming?> Hours</td>
				</tr>
				<tr>
				  <td width="55%" height="20">Total Balance:</td>
				  <td><?=$total_personal?> Hours</td>
				</tr>


				<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
				  <td width="55%" height="20">No MC Balance:</td>
				  <td><?php echo $no_mc_leave_left;?> Hours (Max: <?php echo $max_no_mc?> Hours/year)</td>
				</tr>

				<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
				  <td width="55%" height="20">Total Compasionate Used:</td>
				  <td><?php echo $comp_used;?> Hours (Max: <?php echo $max_comp_leave?> Hours/year)</td>
				</tr>

	            </table>
            </td>
          </tr>
      </table>
	  </td>
    </tr>
</table>



<?php

$past_requests = past_request_types();
//$past_request_days = array(7, 30, 90, 180, 360, 'thisyear', 0);
$past_request_days = array(7, 30, 90, 180, 360, 'thisyear');
$past_request_groups = array("p" => $user->first_name. " " . $user->last_name . " Past Request", "d" => $department." Department Average", "s" => $store." Store Average");
//$past_periods = array("7 Days", "30 Days", "3 Months", "6 Months", "12 Months", "This Year", "All Time");
$past_periods = array("7 Days", "30 Days", "3 Months", "6 Months", "12 Months", "This Year");



foreach ($past_request_groups as $key => $group){
	if ($key == 'p'){
		$days = $p_days;
		$times = $p_times;
	}elseif ($key == 'd'){
		$days = $d_days;
		$times = $d_times;
	}elseif ($key == 's'){
		$days = $s_days;
		$times = $s_times;
	}

	?>
	<br/><br/>
	<table width="70%" height="100" border="0" cellspacing="0" cellpadding="0" background="images/Footer.png">
	    <tr>
	      <td valign="top">

		<table width="100%" border="0" align="left" cellpadding="3" cellspacing="1" background="images/Footer.png">
		<tr style="background-image:url(images/TableHeader.png);" height="20">
			<td colspan="12" style="font-weight: bold;">
				<?=$group;
					$p=0;
				?>
			</td>
		</tr>
		<tr bgcolor="#FFFFFF" style="font-weight: bold;">
			<td rowspan='2'>&nbsp;</td>
			<td align='center' colspan='2'>Annual Leave</td>
			<td align='center' colspan='2'>Personal Leave</td>
			<td align='center' colspan='2'>Unpaid Leave</td>
			<td align='center' colspan='2'>Late</td>
			<td align='center' colspan='2'>Long Lunch</td>
			<td align='center' colspan='2'>Clock In/Out</td>
			     </tr>
			     <tr bgcolor="#FFFFFF" style="font-weight: bold;">
			      <td align="center">Requests</td>
			      <td align="center">Hours</td>
			      <td align="center">Requests</td>
			      <td align="center">Hours</td>
			      <td align="center">Requests</td>
			      <td align="center">Hours</td>
			      <td align="center">Requests</td>
			      <td align="center">Minutes</td>
			      <td align="center">Requests</td>
			      <td align="center">Minutes</td>
			      <td align="center">Requests</td>
			     </tr>

		    	<?php foreach ($past_request_days as $day){ ?>
					<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
		    	   	<td><? echo $past_periods[$p];?>
		    	   	</td>
		    	   		<?php foreach ($past_requests as $req_id => $req_name){
			    			$days_key =  $req_name."_".$day."_days";
			    			$times_key = $req_name."_".$day."_times";
			    		?>
							<td align="center"><?=$days[$days_key]?></td>
						<?php if ($req_name != 'clock'){?>
							<td align="center"><?=$times[$times_key]?></td>
						<?php }?>
						<?php
		    	   		} ?>
					</tr>
				<?php $p++; } ?>
		</table>
		</td>
		</tr>
	</table>

<?php }?>

</div>
<div class="pane" id="history">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="90%">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="20" style="text-decoration:underline"><strong>Leave History</strong></td>
                </tr>
            </table>
            <form id="<?=$form_id?>" name="<?=$form_id?>" method="post" action="">

			<input type="hidden" id="user_name" name="user_name" value="<?=$user->user_name?>"/>
        	<input type="hidden" id="<?=$form_id?>_order_by" name="<?=$form_id?>_order_by" value=""/>
        	<input type="hidden" id="<?=$form_id?>_order" name="<?=$form_id?>_order" value=""/>
			<input type="hidden" id="<?=$form_id?>_filter_leave_type_id" name="<?=$form_id?>_filter_leave_type_id" value="1"/>
        	<input type="hidden" id="<?=$form_id?>_filter_time_period_id" name="<?=$form_id?>_filter_time_period_id" value=""/>
        	<input type="hidden" id="<?=$form_id?>_page_no" name="<?=$form_id?>_page_no" value=""/>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" background="images/Footer.png">
                <tr>
                    <td>
                    	<table width="100%" border="0" cellspacing="1" cellpadding="1">
                        	<tr bgcolor="#FFFFFF">
                        		<td align="center">
                        		Leave Type:
                                <?php
                                $leave_type_id = $form_id.'_leave_type_id';
                                $js = 'id="'.$leave_type_id.'" style="font-size: 11px; width: 120px;"';
								echo form_dropdown($leave_type_id, $leave_types, 1, $js);
								?>&nbsp;&nbsp;&nbsp;&nbsp;
                        		Time period:
								<?php
                                $time_period_id = $form_id.'_time_period_id';
                                $js = 'id="'.$time_period_id.'" style="font-size: 11px; width: 120px;"';
								echo form_dropdown($time_period_id, time_period(), '', $js);
								?>
								<input type="button" value="search" onclick="search('<?=$form_id?>');" />
								</td>
                            </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="1" cellpadding="1" id="list_<?=$form_id?>">
                            <tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                            	<td width="20" height="20">#</td>
                                <td width="113"><a href="javascript:void(0)" onclick="sort('<?=$form_id?>','id');">Request ID</a></td>
                                <td width="90"><a href="javascript:void(0)" onclick="sort('<?=$form_id?>','start_date');">Start Date</a></td>
                                <td width="90">End Date</td>
                                <td width="190">Leave Type</td>
                                <td width="117">Requested</td>
                                <td>Reason</td>
                                <td width="90">Request Date</td>
                                <td height="20" align="center" style="color:#FFF">Action</td>
                            </tr>
                            <?
                            if(!empty($requests)){
                            	$num = 0;
                            foreach ($requests as $row):
                            	$num++;
                            ?>
                            <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
                                <td height="20"><?=$num?></td>
                                <td>R<?=$row->id?></td>
                                <td><?=(date(DATE_FORMAT, $row->start_day) . " " . $row->start_time)?></td>
                                <td><?=(date(DATE_FORMAT, $row->end_day) . " " . $row->end_time)?></td>
                                <td><?=$row->leave_type?></td>
                                <td>
                                <?
                                echo $row->hours_used;
                                if($row->leave_type_id == LEAVE_TYPE_LUNCH
                                	|| $row->leave_type_id == LEAVE_TYPE_LATE){
                                	echo " Minutes";
                                }else{
                                	echo " Hours";
                                }
                                ?>
                                </td>
                                <td><?=$row->reason?></td>
                                <td><?=date(DATETIME_FORMAT,$row->add_time)?></td>
                                <td align="center">
                                	<?
                                	if($row->state == 10){
                                		?>
                                		<a href="<?=site_url('request/edit/'.$row->id)?>">edit</a>&nbsp;
                                		<a href="javascript:void(0)" onclick="cancel_request(<?=$row->id?>);">cancel</a>
                                		<?
                                	}else if($row->state == 20 || $row->state == 30){
                                		?>
                                		<a href="<?=site_url('request/view/'.$row->id)?>">view</a>&nbsp;
                                		<a href="javascript:void(0)" onclick="cancel_request('<?=$form_id?>', <?=$row->id?>);">cancel</a>
                                		<?
                                	}else{
                                	?>
                                		<a href="<?=site_url('request/view/'.$row->id)?>">view</a>&nbsp;
                                	<?
                                	}
                                	?>
                                </td>
                            </tr>
                            <?
                            endforeach;
                            ?>
                            <tr bgcolor="#FFFFFF" font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                                <td align="right" colspan="9">
                                <?php
                                	$space = "&nbsp;&nbsp;&nbsp;";

                            		if($page_count > 1){
	//                            		if($page_count > 1){
	//                                		echo $page_no ." / " . $page_count.$space;
	//                                	}
	//                                	if($page_no == 1){
	//                                		echo "First";
	//                                	}else{
	//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(1);'>First</a>";
	//                                	}
	                                	echo $space;
	                                	if($page_no > 1){
	                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . ($page_no - 1) . ");'>Pre.</a>";
	                                	}else{
	                                		echo "Pre.";
	                                	}
	                                	echo $space;
	                                	if($page_no + 1 <= $page_count){
	                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . ($page_no + 1) . ");'>Next</a>";
	                                	}else{
	                                		echo "Next";
	                                	}
	//                                	echo $space;
	//                                	if($page_no == $page_count || $page_count == 0){
	//                                		echo "Last";
	//                                	}else{
	//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . $page_count . ")'>Last</a>";
	//                                	}
                            			echo $space. "GoTo:";
                            			$page_dropdown_id = $form_id.'_page_dropdown';
		                                $js = 'id="'.$page_dropdown_id.'" style="font-size: 11px;" onChange="gotopage(\''.$form_id.'\', this.value)"';
		                                $page_dropdown = array();
		                                for($i = 1; $i <= $page_count; $i++){
		                                	$page_dropdown[$i] = $i;
		                                }
										echo form_dropdown($page_dropdown_id, $page_dropdown, $page_no, $js);
                                	}
                                	echo $space. "Items Per Page:";
                                	$page_limit_id = $form_id.'_limit';
                                	$js = 'id="'.$page_limit_id.'" style="font-size: 11px;" onChange="pagelimit(\''.$form_id.'\')"';
                                	echo form_dropdown($page_limit_id, pagination_option(), PAGE_SIZE, $js);

                                ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                            </tr>

                            <?
                            }else{
                            ?>
							<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
                                <td height="20" colspan="9">No record</td>
                            </tr>
                            <?php }?>
                        </table>
                    </td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
</table>
</div><!-- end Leave History -->

<div class="pane" id="colleague">


	</div>
	<div class="pane" id="tab_upcoming">
        	<?
            if(count($request_logs) > 0){
            ?>
            <table width="100%" border="0" cellspacing="1" cellpadding="1">
                <tr bgcolor="#EBEBEB">
                    <td width="50%" height="20" colspan="5" align="left" bgcolor="#EBEBEB" style="color: red">&nbsp;Request Log</td>
                </tr>
                <tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                    <td width="20" height="20">#</td>
                    <td width="100">Action</td>
                    <td width="100">Actioned On</td>
                    <td width="117">Actioned By</td>
                    <td width="20">IP</td>
                </tr>
                <?
                    $num = 0;
                    foreach ($request_logs as $row):
                    $num++;
                    ?>
                    <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
	                    <td height="20"><?=$num?></td>
	                    <td><?=$row['action']?></td>
	                    <td height="20"><?=$row['actioned_on']?></td>
	                    <td><?=$row['actioned_by']?></td>
	                    <td><?=$row['action_ip']?></td>
	                </tr>
                    <?
                    endforeach;
                    ?>
            </table>
            <?}else{
            	echo "There are no request logs.";
            }?>
        </div>
</div>
<!-- This JavaScript snippet activates those tabs -->
<script type="text/javascript">
$(function() {
	$("ul.tabs").tabs("div.panes > .pane");
});






function sort(form_id, orderby){
	if($("#" + form_id + "_order_by").val() == orderby){
		if($("#" + form_id + "_order").val() == "ASC"){
			$("#" + form_id + "_order").val("DESC");
		}else{
			$("#" + form_id + "_order").val("ASC");
		}
	}else{
		$("#" + form_id + "_order_by").val(orderby);
		$("#" + form_id + "_order").val("ASC");
	}
	//alert($("#colleague_filter_start_date").val());
	//alert($("#colleague_filter_end_date").val());
	$("#" + form_id + "_page_no").val(1);
	return submitForm(form_id);
}



function search_past(form){
	if(!$("#" + form + "_start_date").val()){
		alert("Please select start date");
		return false;
	}
	if(!$("#" + form + "_end_date").val()){
		alert("Please select end date");
		return false;
	}
	$("#" + form + "_order_by").val("");
	$("#" + form + "_order").val("");
	$("#" + form + "_filter_department_id").val($("#" + form + "_department_id").val());
	$("#" + form + "_filter_leave_type_id").val($("#" + form + "_leave_type_id").val());
	$("#" + form + "_filter_start_date").val($("#" + form + "_start_date").val());
	$("#" + form + "_filter_end_date").val($("#" + form + "_end_date").val());
	$("#" + form + "_page_no").val(1);
	return submitForm(form);
}

function past_leave_page(pageNo){
	$("#past_leave_page_no").val(pageNo);
	return submitForm("past_leave");
}

function colleague_page(pageNo){
	$("#colleague_page_no").val(pageNo);
	return submitForm("colleague");
}

function past_leave_response(responseText, statusText, xhr, $form){
	//$("#list_waiting").html(responseText.content);
	$("#list_past_leave").find("tr:gt(0)").remove();
	$('#list_past_leave tr:last').after(responseText.content);
	$("#list_past_leave").find("tr:eq(0)").remove();
}

function colleague_response(responseText, statusText, xhr, $form){
	//$("#list_waiting").html(responseText.content);
	$("#list_colleague").find("tr:gt(0)").remove();
	$('#list_colleague tr:last').after(responseText.content);
	$("#list_colleague").find("tr:eq(0)").remove();
}

function past_colleague_response(responseText, statusText, xhr, $form){
	$("#list_past_colleague").find("tr:gt(0)").remove();
	$('#list_past_colleague tr:last').after(responseText.content);
	$("#list_past_colleague").find("tr:eq(0)").remove();
}

function past_colleague_page(pageNo){
	$("#past_colleague_page_no").val(pageNo);
	submitForm("past_colleague");
}

function search(form){
	$("#" + form + "_order_by").val("");
	$("#" + form + "_order").val("");
	$("#" + form + "_filter_leave_type_id").val($("#" + form + "_leave_type_id").val());
	$("#" + form + "_filter_time_period_id").val($("#" + form + "_time_period_id").val());
	$("#" + form + "_page_no").val(1);
	return submitForm(form);
}

function search_colleague(form){
	if(!$("#" + form + "_start_date").val()){
		alert("Please select start date");
		return false;
	}
	if(!$("#" + form + "_end_date").val()){
		alert("Please select end date");
		return false;
	}
	$("#" + form + "_order_by").val("");
	$("#" + form + "_order").val("");
	$("#" + form + "_filter_leave_type_id").val($("#" + form + "_leave_type_id").val());
	$("#" + form + "_filter_department_id").val($("#" + form + "_department_id").val());
	$("#" + form + "_filter_start_date").val($("#" + form + "_start_date").val());
	$("#" + form + "_filter_end_date").val($("#" + form + "_end_date").val());
	$("#" + form + "_page_no").val(1);
	return submitForm(form);
}

function goto_index(){
	window.location="<?=site_url("approve")?>";
}

function gotopage(form, pageNo){
	$("#" + form + "_page_no").val(pageNo);
	submitForm(form);
}

function pagelimit(form){
	$("#" + form + "_page_no").val(1);
	submitForm(form);
}

function submitForm(form_id){
	if(form_id == "past_leave"){
		var past_leave_options = {
			dataType:  'json',
			success: past_leave_response,
			url: "<?=site_url('user/past_leave_ajax')?>"
		};
		$('#'+form_id).ajaxSubmit(past_leave_options);
	}

	return false;
}
</script>
</body>
</html>
