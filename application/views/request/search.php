<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Advanced Search</title>
<base href="<?=base_url()?>"/>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.tools.tabs.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />
<style type="text/css">
<!--
#form1 table tr td table {
	text-align: left;
}
#form1 p {
	text-align: left;
}
#form1 table tr td table tr td {
    font-family: Tahoma, Geneva, sans-serif;
	font-size: 11px;
}
/* tab pane styling */
div.panes div {
	display:none;		
	padding:15px 10px;
	border:1px solid #999;
	border-top:0;
	font-size:14px;
	background-color:#fff;
}

div.pane {
	display:none;		
	padding:15px 10px;
	border:1px solid #999;
	border-top:0;
	font-size:14px;
	background-color:#fff;
}
-->
</style>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
<!-- This JavaScript snippet activates those tabs -->
function sort(form_id, orderby){
	if($("#" + form_id + "_order_by").val() == orderby){
		if($("#" + form_id + "_order").val() == "ASC"){
			$("#" + form_id + "_order").val("DESC");
		}else{
			$("#" + form_id + "_order").val("ASC");
		}
	}else{
		$("#" + form_id + "_order_by").val(orderby);
		$("#" + form_id + "_order").val("ASC");
	}
	$("#" + form_id + "_page_no").val(1);
	return submitForm(form_id);
}

function submitForm(form_id){
	if(form_id == "saved"){
		var saved_options = {
				dataType:  'json',
				success: saved_response,
				url: "<?=site_url('search/search_ajax')?>"
			};
		$('#'+form_id).ajaxSubmit(saved_options);
	}
	
	return false;
}

function change_site(value, dept_id){
	var dept_select = document.getElementById(dept_id);
	dept_select.options.length=0;
	$.ajax({
		async: true,
		type: 'POST',
		url: "<?=site_url("approve/dept_by_site_ajax")?>",
		data: {
		  	site_id: value
		},
		success: function(data){
			$.each(data, function(i, dep){
				dept_select.options.add(new Option(dep.name,dep.id));
			});
	  	},
		dataType: 'json'
	});
}

function saved_response(responseText, statusText, xhr, $form){
	$("#list_saved").find("tr:gt(0)").remove();
	$('#list_saved tr:last').after(responseText.content);
	$("#list_saved").find("tr:eq(0)").remove();
}

function search(form){
	$("#" + form + "_order_by").val("");
	$("#" + form + "_order").val("");
	$("#" + form + "_filter_department_id").val($("#" + form + "_department_id").val());
	$("#" + form + "_filter_site_id").val($("#" + form + "_site_id").val());
	$("#" + form + "_filter_leave_type_id").val($("#" + form + "_leave_type_id").val());
	$("#" + form + "_filter_first_name").val($("#" + form + "_first_name").val());
	$("#" + form + "_filter_last_name").val($("#" + form + "_last_name").val());
	$("#" + form + "_filter_from_date").val($("#" + form + "_from_date").val());
	$("#" + form + "_filter_to_date").val($("#" + form + "_to_date").val());
	$("#" + form + "_filter_state").val($("#state").val());
	$("#" + form + "_filter_time_period").val($("#" + form + "_time_period").val());
	$("#" + form + "_page_no").val(1);

	return submitForm(form);
}

function gotopage(form, pageNo){
	$("#" + form + "_page_no").val(pageNo);
	submitForm(form);
}

function saved_page(pageNo){
	$("#saved_page_no").val(pageNo);
	submitForm("saved");
}

$(function() {
	$("#<?=$form_id?>_from_date").datepicker({ 
		disabled: true, 
		dateFormat: '<?=JS_DATE_FORMAT?>',
		changeMonth: true,
		changeYear: true 
	});
	$("#<?=$form_id?>_to_date").datepicker({ 
		disabled: true, 
		dateFormat: '<?=JS_DATE_FORMAT?>',
		changeMonth: true,
		changeYear: true 
	});
});


function view_request(id){
	
	var url = "<?=site_url('request/view')?>/" + id;
	show_view();
	$("#dialog-view").load(url, function() {
		//show_view();
    });	
}

function show_view(){
 	$("#dialog-view").dialog("destroy");
 	$("#dialog-view").dialog({
		modal: true,
		height: 500,
		width: 600,
		resizable: true
	});
 }

</script>

</head>
<body>

<div id="flashmessage"> <?php echo $this->session->flashdata('message');?> </div>

<div id="dialog-view" title="View Request" style="display:none"> 
	<div style="margin: 100px auto; text-align: center;"><img border="0" src="images/loading.gif"/></div> 
</div>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="90%">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="20" style="text-decoration:underline"><strong>Detailed Leave Request Records</strong></td>
                </tr>
            </table>
            <form id="<?=$form_id?>" name="<?=$form_id?>" method="post" action="">
        	<input type="hidden" id="<?=$form_id?>_order_by" name="<?=$form_id?>_order_by" value=""/>
        	<input type="hidden" id="<?=$form_id?>_order" name="<?=$form_id?>_order" value=""/>
        	<input type="hidden" id="<?=$form_id?>_filter_site_id" name="<?=$form_id?>_filter_site_id" value="<?=$site_id?>"/>
        	<input type="hidden" id="<?=$form_id?>_filter_department_id" name="<?=$form_id?>_filter_department_id" value="<?=$department_id?>"/>
        	<input type="hidden" id="<?=$form_id?>_filter_leave_type_id" name="<?=$form_id?>_filter_leave_type_id" value=""/>
        	<input type="hidden" id="<?=$form_id?>_page_no" name="<?=$form_id?>_page_no" value=""/>
        	
        	<input type="hidden" id="<?=$form_id?>_filter_first_name" name="<?=$form_id?>_filter_first_name" value=""/>
        	<input type="hidden" id="<?=$form_id?>_filter_last_name" name="<?=$form_id?>_filter_last_name" value=""/>
        	<input type="hidden" id="<?=$form_id?>_filter_from_date" name="<?=$form_id?>_filter_from_date" value=""/>
        	<input type="hidden" id="<?=$form_id?>_filter_to_date" name="<?=$form_id?>_filter_to_date" value=""/>
        	<input type="hidden" id="<?=$form_id?>_filter_state" name="<?=$form_id?>_filter_state" value=""/>
        	<input type="hidden" id="<?=$form_id?>_filter_time_period" name="<?=$form_id?>_filter_time_period" value=""/>
        	<table width="700" border="0" cellspacing="1" cellpadding="1">
         		<tr>
                	<td width="8%" height="20" bgcolor="#FFFFFF" align="right">First Name:</td>
                	<td width="20%" height="20" bgcolor="#FFFFFF"><input type="text" name="<?=$form_id?>_first_name" id="<?=$form_id?>_first_name" style="font-size:11px"/></td>
                	<td width="8%" height="20" bgcolor="#FFFFFF" align="right">Last Name:</td>
                	<td width="20%" height="20" bgcolor="#FFFFFF"><input type="text" name="<?=$form_id?>_last_name" id="<?=$form_id?>_last_name" style="font-size:11px"/></td>
					<td width="8%" height="20" bgcolor="#FFFFFF" align="right">Date:</td>
					<td height="20" bgcolor="#FFFFFF">
						<input type="text" name="<?=$form_id?>_from_date" id="<?=$form_id?>_from_date" style="font-size:11px" size='8'/> -
						<input type="text" name="<?=$form_id?>_to_date" id="<?=$form_id?>_to_date" style="font-size:11px" size='8'/> 
					</td>
                	
            	</tr>
            	<tr>
                	<td width="8%" height="20" bgcolor="#FFFFFF" align="right">Site:</td>
                	<td width="20%" height="20" bgcolor="#FFFFFF">
	                <?php 
	                $site = $form_id.'_site_id';
	                $department = $form_id.'_department_id';
	                $js = 'id="'.$site.'" style="font-size: 11px; width: 120px;" onChange="change_site(this.value, \'' . $department . '\')"';
					echo form_dropdown($site, $sites, $site_id, $js);
                    ?>
                	</td>
                	<td height="20" bgcolor="#FFFFFF" align="right">Dept.:</td>
                	<td height="20" bgcolor="#FFFFFF">
                	<?php $js = 'id="'.$department.'" style="font-size: 11px; width: 120px;"';
                    
                    echo form_dropdown($department, $departments, $department_id, $js);
                    ?>
					</td>

                	<td width="8%" height="20" bgcolor="#FFFFFF" align="right">Time period:</td>
                	<td width="20%" height="20" bgcolor="#FFFFFF"> 
					<?php
	                    $time_period_id = $form_id.'_time_period';
                        $js = 'id="'.$time_period_id.'" style="font-size: 11px; width: 120px;"'; 
						echo form_dropdown($time_period_id, time_period(), '', $js);
					?>
                	</td>
					
            	</tr>
            	<tr>
                	<td width="8%" height="20" bgcolor="#FFFFFF" align="right">Leave Type:</td>
                	<td width="20%" height="20" bgcolor="#FFFFFF">
                	<?php
                    $leave_type_id = $form_id.'_leave_type_id';
                    $js = 'id="'.$leave_type_id.'" style="font-size: 11px; width: 120px;"'; 
                    echo form_dropdown($leave_type_id, $leave_types, '', $js);
                    ?>
                	</td>
                	<td height="20" bgcolor="#FFFFFF" align="right">State:</td>
                	<td height="20" bgcolor="#FFFFFF">
                	<?php
                	$state_id = $form_id.'state';
	                $js = 'id="state" style="font-size: 11px; width: 120px;"'; 
	                echo form_dropdown($state_id, $states, '', $js);
	                ?>
					</td>
					<td>&nbsp;</td>
                	<td height="20" bgcolor="#FFFFFF" align="right">&nbsp;<input type="button" value="search" onclick="search('<?=$form_id?>');" /></td>
            	</tr>
            </table>
            <table width="100%" border="0" cellspacing="1" cellpadding="1" id="list_<?=$form_id?>">
                <tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                    <td width="20" height="20">#</td>
                    <td width="70"><a href="javascript:void(0)" onclick="sort('<?=$form_id?>','id');">Request ID</a></td>
                    <td width="100"><a href="javascript:void(0)" onclick="sort('<?=$form_id?>','add_time');">Request Date</a></td>
                    <td width="100"><a href="javascript:void(0)" onclick="sort('<?=$form_id?>','applicant');">Applicant</a></td>
                    <td width="117"><a href="javascript:void(0)" onclick="sort('<?=$form_id?>','start_date');">Start Date</a></td>
                    <td width="117">End Date</td>
                    <td width="117"><a href="javascript:void(0)" onclick="sort('<?=$form_id?>','department_id');">Department</a></td>
                    <td width="117">Requested</td>
                    <td width="190"><a href="javascript:void(0)" onclick="sort('<?=$form_id?>','leave_type_id');">Leave Type</a></td>
                    <td width="190">State</td>
                </tr>
                <? 
                if(!empty($approvals)){
                    $num = 0;
                foreach ($approvals as $row): 
                    $num++;
                ?>
                <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';" onclick="view_request(<?=$row->id?>)">
                    <td height="20"><?=$num?></td>
                    <td height="20">R<?=$row->id?></td>
                    <td><?=date(DATETIME_FORMAT,$row->add_time)?></td>
                    <td height="20"><?=$row->first_name?> <?=$row->last_name?></td>
                    <td><?=(date(DATE_FORMAT, $row->start_day) . " " . $row->start_time)?></td>
                    <td>
                    <?
                        if($row->end_date > 0){
                            echo date(DATETIME_FORMAT, $row->end_date);
                        }
                    ?>
                    </td>
                    <td><?=$row->department?></td>
                    <td>
                    <?
                    echo $row->hours_used;
                    if($row->leave_type_id == LEAVE_TYPE_LUNCH
                        || $row->leave_type_id == LEAVE_TYPE_LATE){
                        echo " Minutes";
                    }else{
                        echo " Hours";
                    }
                    ?>
                    </td>
                    <td><?=$row->leave_type?></td>
                    <td>
                    <?
                    	$str_state = "";
                    	if($row->state == 10){
                    		$str_state = "New";
                    	}else if($row->state == 20 || $row->state == 30){
                    		$str_state = "Waiting";
                    	}else if($row->state == 40){
                    		$str_state = "Approved";
                    	}else if($row->state == 50){
                    		$str_state = "Rejected";
                    	}
                    	echo $str_state;
                    ?>
                    </td>
                </tr>
                <? 
                endforeach;
                ?>
                <tr bgcolor="#FFFFFF" font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                    <td align="right" colspan="11">
                    <?php 
                        $space = "&nbsp;&nbsp;&nbsp;";
                        if($page_count > 1){
                            echo $page_no ." / " . $page_count.$space;
                        }
                        if($page_no == 1){
                            echo "First";
                        }else{
                            echo "<a href='javascript:void(0)' onclick='".$form_id."_page(1);'>First</a>";
                        }
                        echo $space;
                        if($page_no > 1){
                            echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . ($page_no - 1) . ");'>previous</a>";
                        }else{
                            echo "previous";
                        }
                        echo $space;
                        if($page_no + 1 <= $page_count){
                            echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . ($page_no + 1) . ");'>next</a>";
                        }else{
                            echo "next";
                        }
                        echo $space;
                        if($page_no == $page_count || $page_count == 0){
                            echo "Last";
                        }else{
                            echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . $page_count . ")'>Last</a>";
                        }
                        if($page_count > 1){
                            echo $space;
                            $page_dropdown_id = $form_id.'_page_dropdown';
                            $js = 'id="'.$page_dropdown_id.'" style="font-size: 11px;" onChange="gotopage(\''.$form_id.'\', this.value)"';
                            $page_dropdown = array();
                            for($i = 1; $i <= $page_count; $i++){
                                $page_dropdown[$i] = $i;
                            }
                            echo form_dropdown($page_dropdown_id, $page_dropdown, $page_no, $js);
                        }
                    ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <?php 
                }else{
                ?>
                <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
                    <td height="20" colspan="11">No record</td>
                </tr>
                <?php }?>
            </table>
            </form>
        </td>
    </tr>
</table>



</body>
</html>