<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Leave Statement</title>
<base href="<?=base_url()?>"/>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.tools.tabs.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />

<style type="text/css">
<!--
#form1 table tr td table {
	text-align: left;
}
#form1 p {
	text-align: left;
}
#form1 table tr td table tr td {
    font-family: Tahoma, Geneva, sans-serif;
	font-size: 11px;
}
/* tab pane styling */
div.panes div {
	display:none;
	padding:15px 10px;
	border:1px solid #999;
	border-top:0;
	font-size:14px;
	background-color:#fff;
}
-->
</style>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/piwik-lrs.js"></script>
</head>
<body>
  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" background="images/Footer.png">
    <tr>
      <td><table width="100%" border="0" cellspacing="1" cellpadding="1">
        <tr bgcolor="#EBEBEB">
            <td width="100%" height="20" align="right" bgcolor="#EBEBEB"><label><a href="index.php/request/add" style="font-size:11px">ADD NEW</a></label></td>
        </tr>
      </table>
      </td>
    </tr>
  </table>
  <h4>General Information</h4>
  <table width="90%" height="100" border="0" cellspacing="0" cellpadding="0" background="images/Footer.png">
    <tr>
      <td valign="top">
      <table width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr>
            <td width="30%" height="20">
	            <table bgcolor="#FFFFFF" width="100%" border="0" cellspacing="0" cellpadding="0">
	              <tr style="background-image:url(images/TableHeader.png);" height="20">
	              	<td colspan="2" >Annual Leave</td>
	              </tr>
	              <tr >
	                <td width="55%" height="20">Currently Available:</td>
	                <td align="center"><?=$annual_available?> Hours</td>
	              </tr>
	              <tr>
	                <td width="55%" height="20">Upcoming Leave Request:</td>
	                <td align="center"><?=$annual_upcoming?> Hours</td>
	              </tr>
	              <tr>
	                <td width="55%" height="20">Total Balance:</td>
	                <td align="center"><?=$total_annual?> Hours</td>
	              </tr>
	              <tr>
	                <td width="55%" height="20">Total In Lieu:</td>
	                <td align="center"><?=$total_inlieu?> Hours</td>

	              </tr>
	            </table>
            </td>
            <td width="30%" height="20">
	            <table bgcolor="#FFFFFF" width="100%" border="0" cellspacing="0" cellpadding="0">
	              <tr style="background-image:url(images/TableHeader.png);" height="20">
	              	<td colspan="2" >Personal Leave</td>
	              </tr>
	              <tr >
	                <td width="55%" height="20">Currently Available:</td>
	                <td align="center"><?=$personal_available?> Hours</td>
	              </tr>
	              <tr>
	                <td width="55%" height="20">Upcoming Leave Request:</td>
	                <td align="center"><?=$personal_upcoming?> Hours</td>
	              </tr>
	              <tr>
	                <td width="55%" height="20">Total Balance:</td>
	                <td align="center"><?=$total_personal?> Hours</td>
	              </tr>
	              <tr>
	                <td width="55%" height="20">No MC Balance:</td>
	                <td align="center"><?=$no_mc_leave_left?> Hours (Max: <?=$max_no_mc?> Hours/year)</td>
	              </tr>

	            </table>
            </td>
          </tr>
          <tr>
            <td width="30%" height="20">
	            <table bgcolor="#FFFFFF" width="100%" border="0" cellspacing="0" cellpadding="0">
	              <tr style="background-image:url(images/TableHeader.png);" height="20">
	              	<td colspan="3">Exceptions(Late)</td>
	              </tr>
	              <tr>
	                <td width="50%" height="20">Late Arrival Last 7 days:</td>
	                <td align="center"><?=$late_7_times?> Minutes</td><td>(<?=$late_7_days?>&nbsp;Times)</td>
	              </tr>
	              <tr>
	                <td>Late Arrival Last 30 days:</td>
	                <td align="center"><?=$late_30_times?> Minutes</td><td>(<?=$late_30_days?>&nbsp;Times)</td>
	              </tr>
	              <tr>
	                <td>Total Late Arrival:</td>
	                <td align="center"><?=$late_all_times?> Minutes</td><td>(<?=$late_all_days?>&nbsp;Times)</td>
	              </tr>
	            </table>
            </td>
            <td width="30%" height="20">
	            <table bgcolor="#FFFFFF" width="100%" border="0" cellspacing="0" cellpadding="0">
	              <tr style="background-image:url(images/TableHeader.png);" height="20">
	              	<td colspan="3">Exceptions(Long lunch)</td>
	              </tr>
	              <tr>
	                <td width="50%" height="20">Long Lunch break Last 7 days:</td>
	                <td align="center"><?=$lunch_7_times?> Minutes</td><td>(<?=$lunch_7_days?>&nbsp;Times)</td>
	              </tr>
	              <tr>
	                <td>Long Lunch break Last 30 days:</td>
	                <td align="center"><?=$lunch_30_times?> Minutes</td><td>(<?=$lunch_30_days?>&nbsp;Times)</td>
	              </tr>
	              <tr>
	                <td>Total Long Lunch Break:</td>
	                <td align="center"><?=$lunch_all_times?> Minutes</td><td>(<?=$lunch_all_days?>&nbsp;Times)</td>
	              </tr>
	            </table>
            </td>
          </tr>
      </table>
	  </td>
    </tr>
  </table>

<div id="flashmessage"> <?php echo $this->session->flashdata('message');?> </div>

<div class="wrap">
	<div style="padding-left:10px;padding-bottom:5px;"><H4>Current Leave Records</H4></div>
	<!-- the tabs -->
	<ul class="tabs">
	    <li><a href="#waiting">Waiting Approval (<?=$waiting_count?>)</a></li>
	    <li><a href="#New">New (<?=$new_count?>)</a></li>
	    <li><a href="#rejected">Rejected (<?=$rejected_count?>)</a></li>
	    <li><a href="#upcoming">Upcoming (<?=$upcoming_count?>)</a></li>
	</ul>

	<!-- tab "panes" -->
	<div class="panes">
	    <div class="pane" style="display:block" id="tab_waiting"></div>
	    <div class="pane" style="display:block" id="tab_new"></div>
	    <div class="pane" style="display:block" id="tab_rejected"></div>
	    <div class="pane" style="display:block" id="tab_upcoming"></div>
	</div>
</div>

<div class="wrap">
	<div style="padding-top:10px;padding-left:10px;padding-bottom:5px;"><H4>Past Leave Records</H4></div>
	<!-- the tabs -->
	<ul class="tabs">
		<li><a href="#annual">Annual Leave (<?=$past_annual_count?>)</a></li>
		<li><a href="#personal">Personal Leave (<?=$past_personal_count?>)</a></li>
		<li><a href="#lateness">Lateness (<?=$past_late_count?>)</a></li>
		<li><a href="#lateness">Long Lunch (<?=$past_lunch_count?>)</a></li>
		<li><a href="#training">Training/Seminar (<?=$past_training_count?>)</a></li>
		<li><a href="#client">Client Visit (<?=$past_client_count?>)</a></li>
		<li><a href="#overtime">Overtime (<?=$past_overtime_count?>)</a></li>
		<li><a href="#unpaid">Unpaid (<?=$past_unpaid_count?>)</a></li>
		<li><a href="#clock">Clock In/Out (<?=$past_clock_count?>)</a></li>
		<li><a href="#clock">In Lieu (<?=$past_inlieu_count?>)</a></li>
	</ul>

	<!-- tab "panes" -->
	<div class="panes">
		<div class="pane" id="tab_annual"></div>
	    <div class="pane" id="tab_personal"></div>
	    <div class="pane" id="tab_lateness"></div>
	    <div class="pane" id="tab_lunch"></div>
		<div class="pane" id="tab_training"></div>
		<div class="pane" id="tab_client"></div>
		<div class="pane" id="tab_overtime"></div>
		<div class="pane" id="tab_unpaid"></div>
		<div class="pane" id="tab_clock"></div>
		<div class="pane" id="tab_inlieu"></div>
	</div>
</div>
<div id="dialog-view" title="View Request" style="display:none">
	<div style="margin: 100px auto; text-align: center;"><img border="0" src="images/loading.gif"/></div>
</div>
<!-- This JavaScript snippet activates those tabs -->
<script type="text/javascript">
$(function() {
    //$("ul.tabs").tabs("div.panes > div");
	$("ul.tabs").tabs("div.panes > .pane");
    $("#tab_new").load("<?php echo site_url("request/saved")?>");
    $("#tab_annual").load("<?php echo site_url("request/approved_annual")?>");

    $("#tab_rejected").load("<?php echo site_url("request/rejected")?>");
    $("#tab_waiting").load("<?php echo site_url("request/waiting")?>");
    $("#tab_upcoming").load("<?php echo site_url("request/approved_upcoming")?>");

    $("#tab_personal").load("<?php echo site_url("request/approved_personal")?>");
    $("#tab_lateness").load("<?php echo site_url("request/approved_late")?>");
    $("#tab_lunch").load("<?php echo site_url("request/approved_lunch")?>");
    $("#tab_training").load("<?php echo site_url("request/approved_training")?>");
    $("#tab_client").load("<?php echo site_url("request/approved_client")?>");
    $("#tab_overtime").load("<?php echo site_url("request/approved_overtime")?>");
    $("#tab_unpaid").load("<?php echo site_url("request/approved_unpaid")?>");
    $("#tab_clock").load("<?php echo site_url("request/approved_clock")?>");
    $("#tab_inlieu").load("<?php echo site_url("request/approved_inlieu")?>");
    // first slide down and blink the message box
    //$("#flashmessage").animate({top: "0px"}, 1000 ).show('fast').fadeIn(200).fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100);
});

function sort(form_id, orderby){
	if($("#" + form_id + "_order_by").val() == orderby){
		if($("#" + form_id + "_order").val() == "ASC"){
			$("#" + form_id + "_order").val("DESC");
		}else{
			$("#" + form_id + "_order").val("ASC");
		}
	}else{
		$("#" + form_id + "_order_by").val(orderby);
		$("#" + form_id + "_order").val("ASC");
	}
	$("#" + form_id + "_page_no").val(1);
	return submitForm(form_id);
}

function submitForm(form_id){
	if(form_id == "waiting"){
		var waiting_options = {
			dataType:  'json',
			success: waiting_response,
			url: "<?=site_url('request/waiting_ajax')?>"
		};
		$('#'+form_id).ajaxSubmit(waiting_options);
	}else if(form_id == "saved"){
		var saved_options = {
				dataType:  'json',
				success: saved_response,
				url: "<?=site_url('request/saved_ajax')?>"
			};
		$('#'+form_id).ajaxSubmit(saved_options);
	}else if(form_id == "rejected"){
		var notApproved_options = {
				dataType:  'json',
				success: rejected_response,
				url: "<?=site_url('request/rejected_ajax')?>"
			};
		$('#'+form_id).ajaxSubmit(notApproved_options);
	}else if(form_id == "past_annual"){
		var past_annual_options = {
				dataType:  'json',
				success: past_annual_response,
				url: "<?=site_url('request/approved_annual_ajax')?>"
			};
		$('#'+form_id).ajaxSubmit(past_annual_options);
	}else if(form_id == "past_personal"){
		var past_personal_options = {
				dataType:  'json',
				success: past_personal_response,
				url: "<?=site_url('request/approved_personal_ajax')?>"
			};
		$('#'+form_id).ajaxSubmit(past_personal_options);
	}else if(form_id == "past_late"){
		var past_late_options = {
				dataType:  'json',
				success: past_late_response,
				url: "<?=site_url('request/approved_late_ajax')?>"
			};
		$('#'+form_id).ajaxSubmit(past_late_options);
	}else if(form_id == "past_training"){
		var past_annual_options = {
				dataType:  'json',
				success: past_training_response,
				url: "<?=site_url('request/approved_training_ajax')?>"
			};
		$('#'+form_id).ajaxSubmit(past_annual_options);
	}else if(form_id == "upcoming"){
		var upcoming_options = {
				dataType:  'json',
				success: upcoming_response,
				url: "<?=site_url('request/approved_upcoming_ajax')?>"
			};
		$('#'+form_id).ajaxSubmit(upcoming_options);
	}else if(form_id == "past_client"){
		var client_options = {
				dataType:  'json',
				success: client_response,
				url: "<?=site_url('request/approved_client_ajax')?>"
			};
		$('#'+form_id).ajaxSubmit(client_options);
	}else if(form_id == "past_overtime"){
		var overtime_options = {
				dataType:  'json',
				success: overtime_response,
				url: "<?=site_url('request/approved_overtime_ajax')?>"
			};
		$('#'+form_id).ajaxSubmit(overtime_options);
	}else if(form_id == "past_unpaid"){
		var unpaid_options = {
				dataType:  'json',
				success: unpaid_response,
				url: "<?=site_url('request/approved_unpaid_ajax')?>"
			};
		$('#'+form_id).ajaxSubmit(unpaid_options);
	}

	return false;
}

function waiting_response(responseText, statusText, xhr, $form){
	$("#list_waiting").find("tr:gt(0)").remove();
	$('#list_waiting tr:last').after(responseText.content);
	$("#list_waiting").find("tr:eq(0)").remove();
}

function saved_response(responseText, statusText, xhr, $form){
	$("#list_saved").find("tr:gt(0)").remove();
	$('#list_saved tr:last').after(responseText.content);
	$("#list_saved").find("tr:eq(0)").remove();
}

function rejected_response(responseText, statusText, xhr, $form){
	$("#list_rejected").find("tr:gt(0)").remove();
	$('#list_rejected tr:last').after(responseText.content);
	$("#list_rejected").find("tr:eq(0)").remove();
}

function upcoming_response(responseText, statusText, xhr, $form){
	$("#list_upcoming").find("tr:gt(0)").remove();
	$('#list_upcoming tr:last').after(responseText.content);
	$("#list_upcoming").find("tr:eq(0)").remove();
}

function past_annual_response(responseText, statusText, xhr, $form){
	$("#list_past_annual").find("tr:gt(0)").remove();
	$('#list_past_annual tr:last').after(responseText.content);
	$("#list_past_annual").find("tr:eq(0)").remove();
}

function past_personal_response(responseText, statusText, xhr, $form){
	$("#list_past_personal").find("tr:gt(0)").remove();
	$('#list_past_personal tr:last').after(responseText.content);
	$("#list_past_personal").find("tr:eq(0)").remove();
}

function past_late_response(responseText, statusText, xhr, $form){
	$("#list_past_late").find("tr:gt(0)").remove();
	$('#list_past_late tr:last').after(responseText.content);
	$("#list_past_late").find("tr:eq(0)").remove();
}

function past_training_response(responseText, statusText, xhr, $form){
	$("#list_past_training").find("tr:gt(0)").remove();
	$('#list_past_training tr:last').after(responseText.content);
	$("#list_past_training").find("tr:eq(0)").remove();
}

function client_response(responseText, statusText, xhr, $form){
	$("#list_past_client").find("tr:gt(0)").remove();
	$('#list_past_client tr:last').after(responseText.content);
	$("#list_past_client").find("tr:eq(0)").remove();
}

function overtime_response(responseText, statusText, xhr, $form){
	$("#list_past_overtime").find("tr:gt(0)").remove();
	$('#list_past_overtime tr:last').after(responseText.content);
	$("#list_past_overtime").find("tr:eq(0)").remove();
}

function unpaid_response(responseText, statusText, xhr, $form){
	$("#list_past_unpaid").find("tr:gt(0)").remove();
	$('#list_past_unpaid tr:last').after(responseText.content);
	$("#list_past_unpaid").find("tr:eq(0)").remove();
}
function search(form){
	$("#" + form + "_order_by").val("");
	$("#" + form + "_order").val("");
	$("#" + form + "_filter_department_id").val($("#" + form + "_department_id").val());
	$("#" + form + "_filter_leave_type_id").val($("#" + form + "_leave_type_id").val());
	$("#" + form + "_filter_time_period_id").val($("#" + form + "_time_period_id").val());
	$("#" + form + "_page_no").val(1);
	return submitForm(form);
}

function search_past(form){
	$("#" + form + "_order_by").val("");
	$("#" + form + "_order").val("");
	$("#" + form + "_filter_department_id").val($("#" + form + "_department_id").val());
	$("#" + form + "_filter_time_period_id").val($("#" + form + "_time_period_id").val());
	$("#" + form + "_page_no").val(1);
	return submitForm(form);
}

function gotopage(form, pageNo){
	$("#" + form + "_page_no").val(pageNo);
	submitForm(form);
}

function pagelimit(form){
	$("#" + form + "_page_no").val(1);
	submitForm(form);
}

function waiting_page(pageNo){
	$("#waiting_page_no").val(pageNo);
	submitForm("waiting");
}

function saved_page(pageNo){
	$("#saved_page_no").val(pageNo);
	submitForm("saved");
}

function rejected_page(pageNo){
	$("#rejected_page_no").val(pageNo);
	submitForm("rejected");
}

function past_annual_page(pageNo){
	$("#past_annual_page_no").val(pageNo);
	submitForm("past_annual");
}

function past_personal_page(pageNo){
	$("#past_personal_page_no").val(pageNo);
	submitForm("past_personal");
}

function past_late_page(pageNo){
	$("#past_late_page_no").val(pageNo);
	submitForm("past_late");
}

function past_training_page(pageNo){
	$("#past_training_page_no").val(pageNo);
	submitForm("past_training");
}

function past_overtime_page(pageNo){
	$("#past_overtime_page_no").val(pageNo);
	submitForm("past_overtime");
}

function past_unpaid_page(pageNo){
	$("#past_unpaid_page_no").val(pageNo);
	submitForm("past_unpaid");
}

function past_client_page(pageNo){
	$("#past_client_page_no").val(pageNo);
	submitForm("past_client");
}

function upcoming_page(pageNo){
	$("#upcoming_page_no").val(pageNo);
	submitForm("upcoming");
}

function cancel_request(form_id, id){
	if(confirm("Confirm cancel the request?")){
		$.ajax({
		  type: 'POST',
		  url: "<?=site_url("request/cancel_ajax")?>",
		  data: {id: id, form_id: form_id},
		  success: cancel_response,
		  dataType: 'json'
		});
		//submitForm(form_id);
	}

}

function cancel_response(data){
	if(data.status == "ok"){
		alert(data.message);
		submitForm(data.form_id);
	}else{
		alert(data.message);
	}
}

function view_request(id){

	var url = "<?=site_url('request/view')?>/" + id;
	show_view();
	$("#dialog-view").load(url, function() {
		//show_view();
    });
}
 function show_view(){
 	$("#dialog-view").dialog("destroy");
 	$("#dialog-view").dialog({
		modal: true,
		height: 500,
		width: 600,
		resizable: true
	});
 }

</script>

</body>
</html>
