<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Leave approval</title>
<base href="<?=base_url()?>"/>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.tools.tabs.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />
<style type="text/css">
<!--
#form1 table tr td table {
	text-align: left;
}
#form1 p {
	text-align: left;
}
#form1 table tr td table tr td {
    font-family: Tahoma, Geneva, sans-serif;
	font-size: 11px;
}
/* tab pane styling */
div.panes div {
	display:none;
	padding:15px 10px;
	border:1px solid #999;
	border-top:0;
	font-size:14px;
	background-color:#fff;
}

div.pane {
	display:none;
	padding:15px 10px;
	border:1px solid #999;
	border-top:0;
	font-size:14px;
	background-color:#fff;
}
-->
</style>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/piwik-lrs.js"></script>
</head>
<body>

<div id="flashmessage"> <?php echo $this->session->flashdata('message');?> </div>

<div class="wrap">
	<div style="padding-left:10px;padding-bottom:5px;"> Current Leave Records </div>
	<!-- the tabs -->
	<ul class="tabs">
	    <li><a href="#waiting">Waiting Approval (<?=$waiting_count?>)</a></li>
	    <?php if ($current_user_level == 2){?>
	    	<li><a href="#waiting_manager">Waiting Manager App (<?=$manager_waiting_count?>)</a></li>
	    <?php }?>

	    <?php if ($current_user_dept == 'Website' || $current_user_dept == 'Accounts'){?>
	    	<li><a href="#waiting_mc">Waiting MC (<?=$mc_waiting_count?>)</a></li>
	    <?php }?>

	    <li><a href="#New">New (<?=$new_count?>)</a></li>
	    <li><a href="#not_approved">Rejected (<?=$not_approved_count?>)</a></li>
	    <li><a href="#upcoming">Upcoming (<?=$upcoming_count?>)</a></li>
	</ul>

	<!-- tab "panes" -->
	<div class="panes">
	    <div class="pane" style="display:block" id="tab_waiting"></div>
	    <?php if ($current_user_level == 2){?>
	    	<div class="pane" style="display:block" id="tab_waiting_manager"></div>
	    <?php }?>
	    <?php if ($current_user_dept == 'Website' || $current_user_dept == 'Accounts'){?>
	    	<div class="pane" style="display:block" id="tab_waiting_mc"></div>
	    <?php }?>
	    <div class="pane" style="display:block" id="tab_new"></div>
	    <div class="pane" style="display:block" id="tab_not_approved"></div>
	    <div class="pane" style="display:block" id="tab_upcoming"></div>
	</div>
</div>
<div id="dialog-view" title="View Request" style="display:none">
	<div style="margin: 100px auto; text-align: center;"><img border="0" src="images/loading.gif"/></div>
</div>
<!-- This JavaScript snippet activates those tabs -->
<script type="text/javascript">
$(function() {
    //$("ul.tabs").tabs("div.panes > div");
	$("ul.tabs").tabs("div.panes > .pane");

	$("#tab_waiting").load("<?php echo site_url("approve/waiting")?>");
	$("#tab_waiting_manager").load("<?php echo site_url("approve/waiting_manager")?>");
	$("#tab_waiting_mc").load("<?php echo site_url("approve/waiting_mc")?>");
    $("#tab_new").load("<?php echo site_url("approve/saved")?>");
    $("#tab_not_approved").load("<?php echo site_url("approve/not_approved")?>");
    $("#tab_upcoming").load("<?php echo site_url("approve/approved_upcoming")?>");
});

function change_site(value, dept_id){
	var dept_select = document.getElementById(dept_id);
//	dept_select.options.length=0;
	$.ajax({
		async: true,
		type: 'POST',
		url: "<?=site_url("approve/dept_by_site_ajax")?>",
		data: {
		  	site_id: value
		},
		success: function(data){
			$.each(data, function(i, dep){
				dept_select.options.add(new Option(dep.name,dep.id));
			});
	  	},
		dataType: 'json'
	});
}

function sort(form_id, orderby){
	if($("#" + form_id + "_order_by").val() == orderby){
		if($("#" + form_id + "_order").val() == "ASC"){
			$("#" + form_id + "_order").val("DESC");
		}else{
			$("#" + form_id + "_order").val("ASC");
		}
	}else{
		$("#" + form_id + "_order_by").val(orderby);
		$("#" + form_id + "_order").val("ASC");
	}
	$("#" + form_id + "_page_no").val(1);
	return submitForm(form_id);
}

function submitForm(form_id){
	if(form_id == "waiting"){
		var waiting_options = {
			dataType:  'json',
			success: waiting_response,
			url: "<?=site_url('approve/waiting_ajax')?>"
		};
		$('#'+form_id).ajaxSubmit(waiting_options);
	}else if(form_id == "saved"){
		var saved_options = {
				dataType:  'json',
				success: saved_response,
				url: "<?=site_url('approve/saved_ajax')?>"
			};
		$('#'+form_id).ajaxSubmit(saved_options);
	}else if(form_id == "not_approved"){
		var notApproved_options = {
				dataType:  'json',
				success: not_approved_response,
				url: "<?=site_url('approve/not_approved_ajax')?>"
			};
		$('#'+form_id).ajaxSubmit(notApproved_options);
	}else if(form_id == "past_annual"){
		var past_annual_options = {
				dataType:  'json',
				success: past_annual_response,
				url: "<?=site_url('approve/approved_annual_ajax')?>"
			};
		$('#'+form_id).ajaxSubmit(past_annual_options);
	}else if(form_id == "past_personal"){
		var past_personal_options = {
				dataType:  'json',
				success: past_personal_response,
				url: "<?=site_url('approve/approved_personal_ajax')?>"
			};
		$('#'+form_id).ajaxSubmit(past_personal_options);
	}else if(form_id == "past_late"){
		var past_late_options = {
				dataType:  'json',
				success: past_late_response,
				url: "<?=site_url('approve/approved_late_ajax')?>"
			};
		$('#'+form_id).ajaxSubmit(past_late_options);
	}else if(form_id == "past_training"){
		var past_annual_options = {
				dataType:  'json',
				success: past_training_response,
				url: "<?=site_url('approve/approved_training_ajax')?>"
			};
		$('#'+form_id).ajaxSubmit(past_annual_options);
	}else if(form_id == "upcoming"){
		var upcoming_options = {
				dataType:  'json',
				success: upcoming_response,
				url: "<?=site_url('approve/approved_upcoming_ajax')?>"
			};
		$('#'+form_id).ajaxSubmit(upcoming_options);
	}else if(form_id == "past_client"){
		var past_client_options = {
				dataType:  'json',
				success: past_client_response,
				url: "<?=site_url('approve/approved_client_ajax')?>"
			};
		$('#'+form_id).ajaxSubmit(past_client_options);
	}else if(form_id == "past_overtime"){
		var past_overtime_options = {
				dataType:  'json',
				success: past_overtime_response,
				url: "<?=site_url('approve/approved_overtime_ajax')?>"
			};
		$('#'+form_id).ajaxSubmit(past_overtime_options);
	}else if(form_id == "past_unpaid"){
		var past_unpaid_options = {
				dataType:  'json',
				success: past_unpaid_response,
				url: "<?=site_url('approve/approved_unpaid_ajax')?>"
			};
		$('#'+form_id).ajaxSubmit(past_unpaid_options);
	}

	return false;
}

function waiting_response(responseText, statusText, xhr, $form){
	$("#list_waiting").find("tr:gt(0)").remove();
	$('#list_waiting tr:last').after(responseText.content);
	$("#list_waiting").find("tr:eq(0)").remove();
}

function saved_response(responseText, statusText, xhr, $form){
	$("#list_saved").find("tr:gt(0)").remove();
	$('#list_saved tr:last').after(responseText.content);
	$("#list_saved").find("tr:eq(0)").remove();
}

function not_approved_response(responseText, statusText, xhr, $form){
	$("#list_not_approved").find("tr:gt(0)").remove();
	$('#list_not_approved tr:last').after(responseText.content);
	$("#list_not_approved").find("tr:eq(0)").remove();
}

function past_annual_response(responseText, statusText, xhr, $form){
	$("#list_past_annual").find("tr:gt(0)").remove();
	$('#list_past_annual tr:last').after(responseText.content);
	$("#list_past_annual").find("tr:eq(0)").remove();
}

function past_personal_response(responseText, statusText, xhr, $form){
	$("#list_past_personal").find("tr:gt(0)").remove();
	$('#list_past_personal tr:last').after(responseText.content);
	$("#list_past_personal").find("tr:eq(0)").remove();
}

function past_late_response(responseText, statusText, xhr, $form){
	$("#list_past_late").find("tr:gt(0)").remove();
	$('#list_past_late tr:last').after(responseText.content);
	$("#list_past_late").find("tr:eq(0)").remove();
}

function past_training_response(responseText, statusText, xhr, $form){
	$("#list_past_training").find("tr:gt(0)").remove();
	$('#list_past_training tr:last').after(responseText.content);
	$("#list_past_training").find("tr:eq(0)").remove();
}

function past_client_response(responseText, statusText, xhr, $form){
	$("#list_past_client").find("tr:gt(0)").remove();
	$('#list_past_client tr:last').after(responseText.content);
	$("#list_past_client").find("tr:eq(0)").remove();
}

function past_overtime_response(responseText, statusText, xhr, $form){
	$("#list_past_overtime").find("tr:gt(0)").remove();
	$('#list_past_overtime tr:last').after(responseText.content);
	$("#list_past_overtime").find("tr:eq(0)").remove();
}

function past_unpaid_response(responseText, statusText, xhr, $form){
	$("#list_past_unpaid").find("tr:gt(0)").remove();
	$('#list_past_unpaid tr:last').after(responseText.content);
	$("#list_past_unpaid").find("tr:eq(0)").remove();
}

function upcoming_response(responseText, statusText, xhr, $form){
	$("#list_upcoming").find("tr:gt(0)").remove();
	$('#list_upcoming tr:last').after(responseText.content);
	$("#list_upcoming").find("tr:eq(0)").remove();
}

function search(form){
	$("#" + form + "_order_by").val("");
	$("#" + form + "_order").val("");
	$("#" + form + "_filter_site_id").val($("#" + form + "_site_id").val());
	$("#" + form + "_filter_department_id").val($("#" + form + "_department_id").val());
	$("#" + form + "_filter_leave_type_id").val($("#" + form + "_leave_type_id").val());
	$("#" + form + "_filter_time_period_id").val($("#" + form + "_time_period_id").val());
	$("#" + form + "_filter_first_name").val($("#" + form + "_first_name").val());
	$("#" + form + "_filter_last_name").val($("#" + form + "_last_name").val());
	$("#" + form + "_page_no").val(1);
	return submitForm(form);
}

function search_past(form){

	$("#" + form + "_order_by").val("");
	$("#" + form + "_order").val("");
	$("#" + form + "_filter_site_id").val($("#" + form + "_site_id").val());
	$("#" + form + "_filter_department_id").val($("#" + form + "_department_id").val());
	$("#" + form + "_filter_time_period_id").val($("#" + form + "_time_period_id").val());
	$("#" + form + "_filter_first_name").val($("#" + form + "_first_name").val());
	$("#" + form + "_filter_last_name").val($("#" + form + "_last_name").val());
	$("#" + form + "_page_no").val(1);
	return submitForm(form);
}

function waitingRefreshPageinate(pageCount, pageNo){
	if(pageCount == 0 || pageCount == 1){
		$("#waitingCurrentPaginate").hide();
	}else{
		$("#waitingCurrentPaginate").show();
		$("#waitingjPaginate").show();

		$("#waitingjPaginate").paginate({
			count 		: pageCount,
			start 		: pageNo,
			display     : 12,
			border					: false,
			text_color  			: '#79B5E3',
			background_color    	: 'none',
			text_hover_color  		: '#2573AF',
			background_hover_color	: 'none',
			rotate      : false,
			images		: false,
			mouse		: 'press',
			onChange 	: waitingGotoPage
		});
	}
}

function gotopage(form, pageNo){
	$("#" + form + "_page_no").val(pageNo);
	submitForm(form);
}
function pagelimit(form){
	$("#" + form + "_page_no").val(1);
	submitForm(form);
}
function waiting_page(pageNo){
	$("#waiting_page_no").val(pageNo);
	submitForm("waiting");
}

function saved_page(pageNo){
	$("#saved_page_no").val(pageNo);
	submitForm("saved");
}

function not_approved_page(pageNo){
	$("#not_approved_page_no").val(pageNo);
	submitForm("not_approved");
}

function past_annual_page(pageNo){
	$("#past_annual_page_no").val(pageNo);
	submitForm("past_annual");
}

function past_personal_page(pageNo){
	$("#past_personal_page_no").val(pageNo);
	submitForm("past_personal");
}

function past_late_page(pageNo){
	$("#past_late_page_no").val(pageNo);
	submitForm("past_late");
}

function past_training_page(pageNo){
	$("#past_training_page_no").val(pageNo);
	submitForm("past_training");
}

function past_client_page(pageNo){
	$("#past_client_page_no").val(pageNo);
	submitForm("past_client");
}

function past_overtime_page(pageNo){
	$("#past_overtime_page_no").val(pageNo);
	submitForm("past_overtime");
}

function past_unpaid_page(pageNo){
	$("#past_unpaid_page_no").val(pageNo);
	submitForm("past_unpaid");
}

function upcoming_page(pageNo){

	$("#upcoming_page_no").val(pageNo);
	submitForm("upcoming");
}

function view_request(id){

	var url = "<?=site_url('request/view')?>/" + id;
	$("#dialog-view").dialog("destroy");
	show_view();
	$("#dialog-view").load(url, function() {
		//show_view();
    });
}
 function show_view(){
 	$("#dialog-view").dialog({
		modal: true,
		height: 500,
		width: 600,
		resizable: true
	});
 }
</script>

</body>
</html>
