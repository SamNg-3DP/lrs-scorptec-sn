<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<base href="<?=base_url()?>"/>
<style type="text/css">
<!--
#form1 table tr td table {
	text-align: left;
}
#form1 p {
	text-align: left;
}
#form1 table tr td table tr td {
    font-family: Tahoma, Geneva, sans-serif;
	font-size: 11px;
}
-->
</style>
<script type="text/javascript" src="js/piwik-lrs.js"></script>
</head>

<body>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" background="images/Footer.png">
    <tr>
      <td><table width="100%" border="0" cellspacing="1" cellpadding="1">
        <tr bgcolor="#EBEBEB">
            <td height="20" colspan="6" align="right">
                <label>
                    <a href="index.php/department/index_add" style="font-size:11px">ADD NEW</a>
                </label></td>
        </tr>
        <tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
          <td height="20">No.</td>
          <td height="20">Department Name</td>
          <td height="20">Status</td>
          <td height="20" colspan="3" style="color:#EEF8FC">Action</td>
        </tr>
        <? if (empty($department)) { ?>
        <tr bgcolor="#FFFFFF">
          <td height="20" colspan="6" style="color: #F00">No departments!</td>
        </tr>
        <? } else { $num = 0; ?>
		<? foreach ($department as $row): $num = $num + 1; ?>
        <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
          <td width="60" height="20"><?=$num?></td>
          <td height="20"><?=$row->department?></td>
          <td width="120" height="20"><?=($row->status==1)?'Active':'Inactive'?></td>
          <td width="50" height="20" align="center"><a href="index.php/department/index_edit/<?=$row->id?>"><img border="0" src="images/action/b_edit.png" width="16" height="16" title="Edit"/></a></td>
          <td width="50" height="20" align="center"><a href="index.php/department/remove/<?=$row->id?>"><img border="0" src="images/action/b_drop.png" width="16" height="16" title="Remove" /></a></td>
          <td width="100" height="20" align="center"><?=anchor("department/change_status/$row->id/$row->status", 'CHANGE STATUS')?></td>
        </tr>
		<? endforeach; } ?>
      </table>
      </td>
    </tr>
  </table>
</form>
</body>
</html>
