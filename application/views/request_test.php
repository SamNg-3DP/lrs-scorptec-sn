<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<title>jQuery UI Datepicker - Default functionality</title>
        <base href="<?=base_url()?>"/>
        <link type="text/css" href="css/style.css" rel="stylesheet" />
        <link type="text/css" href="css/jqueri-ui.css" rel="stylesheet" />
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery-ui.min.js"></script>
	<script type="text/javascript">
	$(function() {
		$("#datepicker").datepicker();
	});
	</script>
</head>
<body>

<div>
<?php echo $testjson;?>
</div>

<div class="demo">

<p>Date: <input type="text" id="datepicker"></p>

</div><!-- End demo -->

<div class="demo-description">

<p>The datepicker is tied to a standard form input field.  Focus on the input (click, or use the tab key) to open an interactive calendar in a small overlay.  Choose a date, click elsewhere on the page (blur the input), or hit the Esc key to close. If a date is chosen, feedback is shown as the input's value.</p>

</div><!-- End demo-description -->

</body>
</html>

        