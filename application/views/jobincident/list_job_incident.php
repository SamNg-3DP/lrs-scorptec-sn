
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.tools.tabs.css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript">

$(function() {
	$("#<?=$status?>_from_date").datepicker({
		disabled: true,
		dateFormat: '<?=JS_DATE_FORMAT?>',
		changeMonth: true,
		changeYear: true
	});
	$("#<?=$status?>_to_date").datepicker({
		disabled: true,
		dateFormat: '<?=JS_DATE_FORMAT?>',
		changeMonth: true,
		changeYear: true
	});
});


function gotopage(form, pageNo){
	$("#" + form + "_page_no").val(pageNo);
	submitForm(form);
}

function pagelimit(form){
	$("#" + form + "_page_no").val(1);
	submitForm(form);
}

function submitForm(form_id){
	if(form_id == "new"){
		var new_options = {
			dataType:  'json',
			success: new_response,
			url: "<?=site_url('jobincident/new_ajax')?>"
		};
		$('#'+form_id).ajaxSubmit(new_options);
	}else if(form_id == "progress"){
		var progress_options = {
			dataType:  'json',
			success: progress_response,
			url: "<?=site_url('jobincident/progress_ajax')?>"
		};
		$('#'+form_id).ajaxSubmit(progress_options);
	}else if(form_id == "completed"){
		var completed_options = {
			dataType:  'json',
			success: completed_response,
			url: "<?=site_url('jobincident/completed_ajax')?>"
		};
		$('#'+form_id).ajaxSubmit(completed_options);
	}

	return false;
}

function new_response(responseText, statusText, xhr, $form){
	$("#list_new").find("tr:gt(0)").remove();
	$('#list_new tr:last').after(responseText.content);
	$("#list_new").find("tr:eq(0)").remove();
}

function new_page(pageNo){
	$("#new_page_no").val(pageNo);
	submitForm("new");
}

function progress_response(responseText, statusText, xhr, $form){
	$("#list_progress").find("tr:gt(0)").remove();
	$('#list_progress tr:last').after(responseText.content);
	$("#list_progress").find("tr:eq(0)").remove();
}

function progress_page(pageNo){
	$("#progress_page_no").val(pageNo);
	submitForm("progress");
}

function completed_response(responseText, statusText, xhr, $form){
	$("#list_completed").find("tr:gt(0)").remove();
	$('#list_completed tr:last').after(responseText.content);
	$("#list_completed").find("tr:eq(0)").remove();
}

function completed_page(pageNo){
	$("#completed_page_no").val(pageNo);
	submitForm("completed");
}

function search_past(form){
	$("#" + form + "_order_by").val("");
	$("#" + form + "_order").val("");
	$("#" + form + "_filter_site_id").val($("#" + form + "_site_id").val());
	$("#" + form + "_filter_department_id").val($("#" + form + "_department_id").val());
	$("#" + form + "_filter_time_period_id").val($("#" + form + "_time_period_id").val());
	$("#" + form + "_filter_from_date").val($("#" + form + "_from_date").val());
	$("#" + form + "_filter_to_date").val($("#" + form + "_to_date").val());
	$("#" + form + "_filter_first_name").val($("#" + form + "_first_name").val());
	$("#" + form + "_filter_last_name").val($("#" + form + "_last_name").val());
	$("#" + form + "_page_no").val(1);

	return submitForm(form);
}

$(function() {
	$("ul.tabs").tabs("div.panes > .pane");
});

</script>
<script type="text/javascript" src="js/piwik-lrs.js"></script>

  <table width="100%" border="0" cellspacing="1" cellpadding="1">
    <tr bgcolor="#EBEBEB">
        <td height="20" colspan="14" align="right">
            <label>
                <a href="<?=site_url('jobincident/add')?>" style="font-size:11px">ADD NEW</a>
            </label>
        </td>
    </tr>
</table>

  <div class="wrap">

	<div class="panes">

		<form id="<?=$status?>" name="<?=$status?>" method="post" action="">
        	<input type="hidden" id="<?=$status?>_order_by" name="<?=$status?>_order_by" value=""/>
        	<input type="hidden" id="<?=$status?>_order" name="<?=$status?>_order" value=""/>
        	<input type="hidden" id="<?=$status?>_filter_site_id" name="<?=$status?>_filter_site_id" value="<?=$this->session->userdata('site_id')?>"/>
        	<input type="hidden" id="<?=$status?>_filter_department_id" name="<?=$status?>_filter_department_id" value="<?=$department_id?>"/>
        	<input type="hidden" id="<?=$status?>_filter_leave_type_id" name="<?=$status?>_filter_leave_type_id" value=""/>
        	<input type="hidden" id="<?=$status?>_filter_time_period_id" name="<?=$status?>_filter_time_period_id" value=""/>
        	<input type="hidden" id="<?=$status?>_filter_from_date" name="<?=$status?>_filter_from_date" value=""/>
        	<input type="hidden" id="<?=$status?>_filter_to_date" name="<?=$status?>_filter_to_date" value=""/>
        	<input type="hidden" id="<?=$status?>_filter_first_name" name="<?=$status?>_filter_first_name" value=""/>
        	<input type="hidden" id="<?=$status?>_filter_last_name" name="<?=$status?>_filter_last_name" value=""/>

  		  <input type="hidden" id="<?=$status?>_page_no" name="<?=$status?>_page_no" value=""/>
		  <table width="100%" border="0" cellspacing="0" cellpadding="0" background="images/Footer.png" >
		    <tr>
		      <td>

                    	<table width="100%" border="0" cellspacing="1" cellpadding="1">
                        	<tr bgcolor="#FFFFFF">
                        		<td align="center">
                        		<?
                        		$site = $status.'_site_id';
                        		$department = $status.'_department_id';
                        		if($this->session->userdata('site_id') == 0){
                        		?>
	                        		Site:
	                                <?php
	                                $js = 'id="'.$site.'" style="font-size: 11px; width: 120px;" ';
									echo form_dropdown($site, $sites, "", $js);
                        		}else{
                        			echo '<input type="hidden" id="'. $site .'" name="'. $site .'" value="'. $this->session->userdata('site_id') .'"/>';
                        		}

                        		if ($this->session->userdata('user_level') >= 3){
                        			echo "&nbsp;&nbsp;Dept: ";
                        			$js = 'id="'.$department.'" style="font-size: 11px; width: 120px;"';
                        			echo form_dropdown($department, $departments, "", $js);
                        		}

								?>
								&nbsp;&nbsp;Time period:
								<?php
                                $time_period_id = $status.'_time_period_id';
                                $js = 'id="'.$time_period_id.'" style="font-size: 11px; width: 120px;"';
								echo form_dropdown($time_period_id, time_period(), '', $js);
								?>
								&nbsp;&nbsp;Date:<input type="text" name="<?=$status?>_from_date" id="<?=$status?>_from_date" style="font-size:11px" size='8'/> -
								<input type="text" name="<?=$status?>_to_date" id="<?=$status?>_to_date" style="font-size:11px" size='8'/>

								&nbsp;&nbsp;First Name:<input type="text" name="<?=$status?>_first_name" id="<?=$status?>_first_name" style="font-size:11px; width: 50px;"/>
								Last Name:<input type="text" name="<?=$status?>_last_name" id="<?=$status?>_last_name" style="font-size:11px; width: 50px;"/>
								<input type="button" value="search" onclick="search_past('<?=$status?>');" />
								</td>
                            </tr>
                        </table>



		      <table width="100%" border="0" cellspacing="1" cellpadding="0" id="list_<?=$status?>">
		        <tr style="background-image:url('images/TableHeader.png');">
		          <td height="20">No</td>
		          <td>Incident ID</td>
		          <td>User</td>
		          <td>Staff Involved</td>
		          <td>Date</td>
		          <td>Customer(s)</td>
		          <td>Reference/Order No</td>
		          <td>$ Value</td>
		          <td colspan="2" align="center">Action</td>
		        </tr>
		        <?
				if($count > 0){
					$num = 0;
					$total_value = 0;
		        foreach ($jobs as $row):
		        	$total_value += $row->cost_value;
		        ?>
		        <tr bgcolor="#FFFFFF">
		          <td ><?=++$num?></td>
		          <td ><?=$row->id?></td>
		          <td >
		          <?=$row->user_firstname.' '.$row->user_lastname?>
		          </td>
		          <td >
		          <?=$row->staff_firstname.' '.$row->staff_lastname?>
		          </td>
		          <td ><?=date(DATE_FORMAT, $row->incident_date)?></td>
		          <td >
		          <?=$row->customer?>
		          </td>
		          <td >
		          <?=$row->reference_no?>
		          </td>
		          <td align="right">
		          $<?=number_format($row->cost_value,2)?>
		          </td>

		          <td align="center">
		          <?php if ($status=='new' || ($user_level >= 3)){?>
						<a href="<?php echo site_url("jobincident/edit/".$row->id);?>">Edit</a>
						&nbsp;&nbsp;&nbsp;
				  <?php }?>
			      <?php if ($user_level >=2 && ($status=='progress')){?>
						<a href="<?php echo site_url("jobincident/deal/".$row->id);?>">Review</a>
						&nbsp;&nbsp;&nbsp;
				  <?php }?>
		          <a href="<?php echo site_url("jobincident/view/".$row->id);?>">View</a>
		          </td>
		        </tr>
		        <? endforeach; ?>
		        <tr bgcolor="#FFFFFF" font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
		        	<td align="right" colspan="7">Total Value:</td><td align="right" >$<?=number_format($total_value,2) ?></td>
		        	<td>&nbsp;</td>
		        </tr>
		        <tr bgcolor="#FFFFFF" font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
		            <td align="right" colspan="9">
		            <?php
		            	$space = "&nbsp;&nbsp;&nbsp;";

                            		if($page_count > 1){
	                                	echo $space;
	                                	if($page_no > 1){
	                                		echo "<a href='javascript:void(0)' onclick='".$status."_page(" . ($page_no - 1) . ");'>Pre.</a>";
	                                	}else{
	                                		echo "Pre.";
	                                	}
	                                	echo $space;
	                                	if($page_no + 1 <= $page_count){
	                                		echo "<a href='javascript:void(0)' onclick='".$status."_page(" . ($page_no + 1) . ");'>Next</a>";
	                                	}else{
	                                		echo "Next";
	                                	}
                            			echo $space. "GoTo:";
                            			$page_dropdown_id = $status.'_page_dropdown';
		                                $js = 'id="'.$page_dropdown_id.'" style="font-size: 11px;" onChange="gotopage(\''.$status.'\', this.value)"';
		                                $page_dropdown = array();
		                                for($i = 1; $i <= $page_count; $i++){
		                                	$page_dropdown[$i] = $i;
		                                }
										echo form_dropdown($page_dropdown_id, $page_dropdown, $page_no, $js);
                                	}
                                	echo $space. "Items Per Page:";
                                	$page_limit_id = $status.'_limit';
                                	$js = 'id="'.$page_limit_id.'" style="font-size: 11px;" onChange="pagelimit(\''.$status.'\')"';
                                	echo form_dropdown($page_limit_id, pagination_option(), PAGE_SIZE, $js);


		            ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		            </td>
		        </tr>
		        <?}?>
		      </table></td>
		    </tr>
		  </table>
		  </form>

	</div>
</div>


</body>
</html>
