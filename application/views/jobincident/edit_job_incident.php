<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Approve a job incident</title>
<base href="<?=base_url()?>" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>

<script type="text/javascript">
//<![CDATA[

function submit_click(action){
	var incident_date = $("#incident_date").val();
	if($.trim(incident_date) == ""){
		alert("Please select date of incident!");
		return false;
	}

	var customer = $("#customer").val();
	if($.trim(customer) == ""){
		alert("Please enter Customer(s) involved!");
		return false;
	}

	var reference_no = $("#reference_no").val();
	if($.trim(reference_no) == ""){
		alert("Please enter Reference no - Invoice no!");
		return false;
	}

	var detail = $("#detail").val();
	if($.trim(detail) == ""){
		alert("Please enter Detail of the inicident!");
		return false;
	}

	var how_occur = $("#how_occur").val();
	if($.trim(how_occur) == ""){
		alert("Please enter How did the incident occur!");
		return false;
	}

	var how_prevent = $("#how_prevent").val();
	if($.trim(how_prevent) == ""){
		alert("Please enter What should have been done to prevent it from happening next time!");
		return false;
	}

	var how_repair = $("#how_repair").val();
	if($.trim(how_repair) == ""){
		alert("Please enter What action should be taken now to repair or recover from the incident!");
		return false;
	}

	var what_cost = $("#what_cost").val();
	if($.trim(what_cost) == ""){
		alert("Please enter What is the approximate time and cost involved as a result of this incident!");
		return false;
	}

	$("#operate").val(action);

	var theForm = document.getElementById("edit_jobincident");
	theForm.submit();
}

jQuery(function() {
    $("#incident_date").datepicker({
    	disabled: true,
    	dateFormat: '<?=JS_DATE_FORMAT?>',
    	changeMonth: true,
		changeYear: true
	});
});
//]]>
</script>
<script type="text/javascript" src="js/piwik-lrs.js"></script>
    </head>
    <body>
    	<div>
        <?php
        if ($job->state == 1)
        	echo form_open_multipart('jobincident/do_add', array('id'=>'edit_jobincident'));
        else
        	echo form_open_multipart('jobincident/do_edit', array('id'=>'edit_jobincident'));
        ?>
        <input type="hidden" id="operate" name="operate" value=""/>
        <input type="hidden" id="job_id" name="job_id" value="<?=$job->id?>"/>
        <table width="100%" height="22" border="0" align="center" cellpadding="0" cellspacing="0" background="images/Footer.png">
            <tr>
                <td>
                     <table width="100%" height="150" border="0" cellspacing="1" cellpadding="1" id="common_type" >
                        <tr bgcolor="#FFFFFF">
                            <td height="20" colspan="2"><p>The purpose of this form is to make note and address any potential issues that can cause job incidents and prevent it from happening in the future. A repeat of similar incident by the same person will not be tolerated and will be deemed as a failure to observe instructions and may result in sanctions being applied</p></td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Date of incident:</td>
                            <td width="80%" height="20">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="100%">
                                        	<input type="text" name="incident_date" id="incident_date"
                                            value="<?=date(DATE_FORMAT, $job->incident_date)?>" style="font-size: 11px"
                                            />
                                    		<label style="color: #F00">*</label><?php echo form_error('incident_date'); ?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Submitted by:</td>
                            <td width="80%" height="20">
                                <?=$job->user_firstname . " " . $job->user_lastname?>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Staff name involved:</td>
                            <td width="80%" height="20">
                            	<?php
                            	$js = 'id="staff_id" style="font-size:11px"';
                            	echo form_dropdown('staff_id', $users, $job->staff_id, $js);?>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Department:</td>
                            <td width="80%" height="20">
                                <?=$job->department?>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF" id="end_day_type">
                            <td height="20" align="left">Customer(s) involved:</td>
                            <td height="20">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="100%">
                                        	<input type="text" name="customer" id="customer"
                                                               value="<?=$job->customer?>" class="divinput"
                                                               />
                                            <label style="color: #F00">*</label><?php echo form_error('customer'); ?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF" id="end_day_type">
                            <td height="20" align="left">Reference no - Invoice no:</td>
                            <td height="20">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="100%">
                                        	<input type="text" name="reference_no" id="reference_no"
                                                               value="<?=$job->reference_no?>" class="divinput"
                                                               />
                                            <label style="color: #F00">*</label><?php echo form_error('reference_no'); ?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Detail of the incident:</td>
                            <td width="80%" height="20">
	                        <?php
							echo form_textarea(array(
								'name' => 'detail',
								'id' => 'detail',
								'value' => $job->detail,
								'cols' => "45",
								'rows' => "5",
								'class'=> "divtextarea"
							));
							?>
							<label style="color: #F00">*</label><?php echo form_error('detail'); ?>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">How did the incident occur? </td>
                            <td width="80%" height="20">
	                        <?php
							echo form_textarea(array(
								'name' => 'how_occur',
								'id' => 'how_occur',
								'value' => $job->how_occur,
								'cols' => "45",
								'rows' => "5",
								'class'=> "divtextarea"
							));
							?>
							<label style="color: #F00">*</label><?php echo form_error('how_occur'); ?>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">What should have been done to prevent it from happening next time?</td>
                            <td width="80%" height="20">
	                        <?php
							echo form_textarea(array(
								'name' => 'how_prevent',
								'id' => 'how_prevent',
								'value' => $job->how_prevent,
								'cols' => "45",
								'rows' => "5",
								'class'=> "divtextarea"
							));
							?>
							<label style="color: #F00">*</label><?php echo form_error('how_prevent'); ?>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">What action should be taken now to repair or recover from the incident</td>
                            <td width="80%" height="20">
	                        <?php
							echo form_textarea(array(
								'name' => 'how_repair',
								'id' => 'how_repair',
								'value' => $job->how_repair,
								'cols' => "45",
								'rows' => "5",
								'class'=> "divtextarea"
							));
							?>
							<label style="color: #F00">*</label><?php echo form_error('how_repair'); ?>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">What is the approximate time and cost involved as a result of this incident:</td>
                            <td width="80%" height="20">
	                        <?php
							echo form_textarea(array(
								'name' => 'what_cost',
								'id' => 'what_cost',
								'value' => $job->what_cost,
								'cols' => "45",
								'rows' => "5",
								'class'=> "divtextarea"
							));
							?>
							<label style="color: #F00">*</label><?php echo form_error('what_cost'); ?>
							</td>
                        </tr>

                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Specify the actual dollar value:</td>
                            <td width="80%" height="20">
	                           	<input type="text" name="cost_value" id="cost_value"
    	                             value="<?php echo $job->cost_value;?>" class="divinput" />
    	                        <label style="color: #F00">*</label><?php echo form_error('cost_value'); ?>
							</td>
                        </tr>


						<tr bgcolor="#FFFFFF">
							<td height="20">Attach File:</td>
							<td height="20">
                            	<?php
                            		if ($attached_files){
	                            		foreach($attached_files as $file){
											echo "<a href='".DIR_JOB_INCIDENT_FILE.$file->file."'>$file->file</a><br/>";
										}
									}
								?>
							<label><input type="file" name="upload_file" id="upload_file" style="font-size: 11px; font-weight: normal;" size="50" value="<?php echo set_value('upload_file');?>" /></label>
							</td>
						</tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Additional Emails (For multiple put (;) in between emails):</td>
                            <td width="80%" height="20">
								<input type="text" name="emails" id="emails" size="80"
									value="<?=$job->emails?>" class="divinput"
								/>
							</td>
                        </tr>


                        <tr bgcolor="#FFFFFF">
                            <td height="20" colspan="2" align="center" bgcolor="#EBEBEB">
                                <input style="font-size: 11px" type="button" name="save" id="save" onclick="submit_click('save');" value="     Save     "/>
                                <input style="font-size: 11px" type="button" name="apply" id="apply" onclick="submit_click('apply');" value="     Submit     "/>
                            </td>
                        </tr>
                    </table>
               </td>
            </tr>
        </table>
        <?php echo form_close();?>
        </div>
    </body>
</html>
