<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Approve a job incident</title>
<base href="<?=base_url()?>" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>

<script type="text/javascript">
//<![CDATA[

function submit_click(){
	<?
	if($this->session->userdata('user_level') == 2){
		?>
		var supervisor_comment = $("#supervisor_comment").val();
		if($.trim(supervisor_comment) == ""){
			alert("Please enter supervisor comment!");
			return false;
		}

		var resolved=document.getElementsByName("resolved");
		var resolvedChecked = false;
		for(var i = 0; i < resolved.length; i++){
			if(resolved[i].checked){
				resolvedChecked = true;
				break;
			}
		}
		if(!resolvedChecked){
			alert("Please select a Resolved/Not Resolved option.");
			return false;
		}

		var approved=document.getElementsByName("approved");
		var approvedChecked = false;
		for(var i = 0; i < approved.length; i++){
			if(approved[i].checked){
				approvedChecked = true;
				break;
			}
		}
		if(!approvedChecked){
			alert("Please select a Approved/Not Approved option.");
			return false;
		}
		var theForm = document.getElementById("deal");
		theForm.submit();
		<?
	}else if($this->session->userdata('user_level') >= 3){
		?>
		var manager_comment = $("#manager_comment").val();
		if($.trim(manager_comment) == ""){
			alert("Please enter manager comment!");
			return false;
		}
		var theForm = document.getElementById("deal");
		theForm.submit();
		<?
	}else if($this->session->userdata('user_level') == 4){
		?>
		goto_approve();
		<?
	}
	?>
}

function goto_approve(){
	window.location="<?=site_url("jobincident/incident_list/progress")?>";
}

//]]>
</script>
<script type="text/javascript" src="js/piwik-lrs.js"></script>
    </head>
    <body>
    	<div>
        <?php
        echo form_open_multipart('jobincident/do_deal', array('id'=>'deal'));
        ?>
        <input type="hidden" id="job_id" name="job_id" value="<?=$job->id?>"/>
        <table width="100%" height="22" border="0" align="center" cellpadding="0" cellspacing="0" background="images/Footer.png">
            <tr>
                <td>
                     <table width="100%" height="150" border="0" cellspacing="1" cellpadding="1" id="common_type" >
                        <tr bgcolor="#FFFFFF">
                            <td height="20" colspan="2"><p>The purpose of this form is to make note and address any potential issues that can cause job incidents and prevent it from happening in the future. A repeat of similar incident by the same person will not be tolerated and will be deemed as a failure to observe instructions and may result in sanctions being applied</p></td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Date of incident:</td>
                            <td width="80%" height="20">
                                <?=date(DATE_FORMAT, $job->incident_date)?>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Submitted by:</td>
                            <td width="80%" height="20">
                                <?=$job->user_firstname . " " . $job->user_lastname?>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Staff name involved:</td>
                            <td width="80%" height="20">
                                <?=$job->staff_firstname . " " . $job->staff_lastname;?>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Deptartment:</td>
                            <td width="80%" height="20">
                                <?=$job->department?>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF" id="end_day_type">
                            <td height="20" align="left">Customer(s) involved:</td>
                            <td height="20">
                                 <p><?=$job->customer?></p>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF" id="end_day_type">
                            <td height="20" align="left">Reference no - Invoice no:</td>
                            <td height="20">
                            	<p><? if ($job->order_url)
										echo "<a href='".$job->order_url."' >".$job->reference_no."</a>";
									  else
										echo $job->reference_no;
									?>
								</p>

                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Detail of the incident:</td>
                            <td width="80%" height="20">
		                       <p><?=$job->detail?></p>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">How did the incident occur? </td>
                            <td width="80%" height="20">
	                        <p><?=$job->how_occur?></p>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">What should have been done to prevent it from happening next time?</td>
                            <td width="80%" height="20">
	                        <p><?=$job->how_prevent?></p>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">What action should be taken now to repair or recover from the incident</td>
                            <td width="80%" height="20">
	                        <p><?=$job->how_repair?></p>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">What is the approximate time and cost involved as a result of this incident:</td>
                            <td width="80%" height="20">
	                        <p><?=$job->what_cost?></p>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Specify the actual dollar value:</td>
                            <td width="80%" height="20">
	                        <p><?=$job->cost_value?></p>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Additional Emails (For multiple put (;) in between emails):</td>
                            <td width="80%" height="20">
								<input type="text" name="emails" id="emails" size="80"
									value="<?=$job->emails?>" class="divinput"
								/>
							</td>
                        </tr>

                        <?
						if($this->session->userdata('user_level') >= 2){
						?>
<!--  						<tr bgcolor="#FFFFFF">
	                            <td width="20%" height="20" align="left">Manager comment:</td>
	                            <td width="80%" height="20">
		                        	<p><?=$job->manager_comment?></p>
								</td>
	                        </tr>
-->
	                        <tr bgcolor="#FFFFFF">
	                            <td width="20%" height="20" align="left">Supervisor comment:</td>
	                            <td width="80%" height="20">
		                        <?php
								echo form_textarea(array(
									'name' => 'supervisor_comment',
									'id' => 'supervisor_comment',
									'value' => $job->supervisor_comment,
									'cols' => "45",
									'rows' => "5",
									'style'=> "font-size:11px"
								));
								?>
								</td>
	                        </tr>
	                        <tr bgcolor="#FFFFFF">
	                            <td width="20%" height="20" align="left">Manager comment:</td>
	                            <td width="80%" height="20">
		                        <?php
								echo form_textarea(array(
									'name' => 'manager_comment',
									'id' => 'manager_comment',
									'value' => $job->manager_comment,
									'cols' => "45",
									'rows' => "5",
									'style'=> "font-size:11px"
								));
								?>
								</td>
	                        </tr>

	                        <tr bgcolor="#FFFFFF">
	                            <td width="20%" height="20" align="left">(Recommend and/or approve action to be taken)</td>
	                            <td width="80%" height="20">
		                        <?php
								echo form_textarea(array(
									'name' => 'take_action',
									'id' => 'take_action',
									'value' => set_value('take_action'),
									'cols' => "45",
									'rows' => "5",
									'style'=> "font-size:11px",
									'value'=> ""
								));
								?>
								</td>
	                        </tr>
	                        <tr bgcolor="#FFFFFF">
	                              <td ><input type="radio" name="resolved" id="not_resolved" value="2" /> Not Resolved</td>
					              <td ><input type="radio" name="resolved" id="resolved" value="1" /> Resolved</td>
	                        </tr>
	                        <tr bgcolor="#FFFFFF">
	                              <td ><input type="radio" name="approved" id="not_approved" value="2" /> Not Approved</td>
					              <td ><input type="radio" name="approved" id="approved" value="1" /> Approved</td>
	                        </tr>
                        <?
						}else if($this->session->userdata('user_level') == 3 ){
						?>
						<tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Supervisor comment:</td>
                            <td width="80%" height="20">
	                        <p><?=$job->supervisor_comment?></p>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Manager comment:</td>
                            <td width="80%" height="20">
	                        <?php
							echo form_textarea(array(
								'name' => 'manager_comment',
								'id' => 'manager_comment',
								'value' => set_value('manager_comment'),
								'cols' => "45",
								'rows' => "5",
								'style'=> "font-size:11px",
								'value'=> ""
							));
							?>
							</td>
                        </tr>
                        <?}else if($this->session->userdata('user_level') == 4){
                        	?>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Supervisor comment:</td>
                            <td width="80%" height="20">
	                        <p><?=$job->supervisor_comment?></p>
							</td>
                        </tr>
                    	<tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Manager comment:</td>
                            <td width="80%" height="20">
	                        	<p><?=$job->manager_comment?></p>
							</td>
                      	</tr>
                        	<?
                        }?>
                        <tr bgcolor="#FFFFFF">
                            <td height="20" colspan="2" align="center" bgcolor="#EBEBEB">
                                <input style="font-size: 11px" type="button" name="Close" id="Close" onclick="goto_approve();return false;" value="     Close     "/>
                                <input style="font-size: 11px" type="button" name="Done"  id="Done" onclick="submit_click();" value="     Done     "/>
                            </td>
                        </tr>
                    </table>
            	</td>
            </tr>
        </table>
        <?php echo form_close();?>
        </div>
    </body>
</html>
