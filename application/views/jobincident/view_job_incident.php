<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Approve a job incident</title>
<base href="<?=base_url()?>" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>

<script type="text/javascript">
//<![CDATA[
function goto_index(){

	<? if ($job->state == 1){ ?>
		window.location="<?=site_url("jobincident/incident_list/new")?>";
	<?} elseif ($job->state == 2) {?>
		window.location="<?=site_url("jobincident/incident_list/progress")?>";
	<?} elseif ($job->state == 3) {?>
		window.location="<?=site_url("jobincident/incident_list/completed")?>";
	<?} ?>
}


//]]>
</script>
<script type="text/javascript" src="js/piwik-lrs.js"></script>
</head>
    <body>
    <div class="wrap">

      <!-- the tabs -->
	  <ul class="tabs">
	    <li><a href="#View">View</a></li>
	    <li><a href="#Logs">Logs</a></li>
	  </ul>
	  <!-- tab "panes" -->
	  <div class="panes">
	    <div class="pane" style="display:block" id="tab_update">

        <table width="100%" height="22" border="0" align="center" cellpadding="0" cellspacing="0" background="images/Footer.png">
            <tr>
                <td>
                     <table width="100%" height="150" border="0" cellspacing="1" cellpadding="1" id="common_type" >
                        <tr bgcolor="#FFFFFF">
                            <td height="20" colspan="2">
                            	<p>The purpose of this form is to make note and address any potential issues that can cause job incidents and prevent it from happening in the future.
                            	A repeat of similar incident by the same person will not be tolerated and will be deemed as a failure to observe instructions and may result in sanctions being applied</p>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Date of incident:</td>
                            <td width="80%" height="20">
                                <?=date(DATE_FORMAT, $job->incident_date)?>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Submitted by:</td>
                            <td width="80%" height="20">
                                <?=$job->user_firstname . " " . $job->user_lastname?>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Staff name involved:</td>
                            <td width="80%" height="20">
                                <?=$job->staff_firstname . " " . $job->staff_lastname;?>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Department:</td>
                            <td width="80%" height="20">
                                <?=$job->department?>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF" id="end_day_type">
                            <td height="20" align="left">Customer(s) involved:</td>
                            <td height="20">
                                 <p><?=$job->customer?></p>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF" id="end_day_type">
                            <td height="20" align="left">Reference no - Invoice no:</td>
                            <td height="20">
								<p><? if ($job->order_url)
										echo "<a href='".$job->order_url."' >".$job->reference_no."</a>";
									  else
										echo $job->reference_no;
									?>
								</p>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Detail of the incident:</td>
                            <td width="80%" height="20">
		                       <p><?=$job->detail?></p>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">How did the incident occur? </td>
                            <td width="80%" height="20">
	                        <p><?=$job->how_occur?></p>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">What should have been done to prevent it from happening next time?</td>
                            <td width="80%" height="20">
	                        <p><?=$job->how_prevent?></p>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">What action should be taken now to repair or recover from the incident</td>
                            <td width="80%" height="20">
	                        <p><?=$job->how_repair?></p>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">What is the approximate time and cost involved as a result of this incident:</td>
                            <td width="80%" height="20">
	                        <p><?=$job->what_cost?></p>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Specify the actual dollar value:</td>
                            <td width="80%" height="20">
	                        <p><?=$job->cost_value?></p>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Supervisor:</td>
                            <td width="80%" height="20">
                            <p><?=$job->supervisor_name?></p>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Supervisor comment:</td>
                            <td width="80%" height="20">
                            <p><?=$job->supervisor_comment?></p>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Manager:</td>
                            <td width="80%" height="20">
	                        	<p><?=$job->manager_name?></p>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Manager comment:</td>
                            <td width="80%" height="20">
	                        	<p><?=$job->manager_comment?></p>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">(Recommend and/or approve action to be taken)</td>
                            <td width="80%" height="20">
                            <p><?=$job->take_action?></p>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                        	<td height="20">Attached File:</td>

                            <td width="80%" height="20">
                            <p><?
                            	if ($attached_files){
	                            	foreach($attached_files as $file){
										echo "<a href='".DIR_JOB_INCIDENT_FILE.$file->file."'>$file->file</a><br/>";
									}
								}
								?>
                            </p>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Additional Emails (For multiple put (;) in between emails):</td>
                            <td width="80%" height="20">
	                        <p><?=$job->emails?></p>
							</td>
                        </tr>


                        <tr bgcolor="#FFFFFF">
                              <td ><input type="radio" name="resolved" id="not_resolved" value="2" <?php if(!is_null($job->resolved) && $job->resolved == 2){ ?>checked="checked"<? }?> disabled="disabled" /> Not Resolved</td>
				              <td ><input type="radio" name="resolved" id="resolved" value="1" <?php if(!is_null($job->resolved) && $job->resolved == 1){ ?>checked="checked"<? }?> disabled="disabled" /> Resolved</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                              <td ><input type="radio" name="approved" id="approved" value="2" <?php if(!is_null($job->approved) && $job->resolved == 2){ ?>checked="checked"<? }?> disabled="disabled"/> Not Approved</td>
				              <td ><input type="radio" name="approved" id="not_approved" value="1" <?php if(!is_null($job->approved) && $job->resolved == 1){ ?>checked="checked"<? }?> disabled="disabled"/> Approved</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td height="20" colspan="2" align="center" bgcolor="#EBEBEB">
                                <input style="font-size: 11px" type="button" name="Close" id="Close" onclick="goto_index();return false;" value="     Close     "/>
                            </td>
                        </tr>
                    </table>
            	</td>
            </tr>
        </table>
        </div>

	        <div class="pane" style="display:block" id="tab_upcoming">
	        	<table width="100%" border="0" cellspacing="1" cellpadding="1">
                        <tr bgcolor="#EBEBEB">
                            <td width="50%" height="20" colspan="5" align="left" bgcolor="#EBEBEB" >&nbsp;Job Incident Logs</td>
                        </tr>
                        <tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                            <td width="20" height="20">#</td>
		                    <td width="100">Action</td>
		                    <td width="100">Actioned On</td>
		                    <td width="117">Actioned By</td>
		                    <td width="20">IP</td>
                        </tr>
                        <?
                    if(count($job_incident_logs) > 0){
                            $num = 0;
                            foreach ($job_incident_logs as $row):
                            $num++;
                            ?>
                            <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
			                    <td height="20"><?=$num?></td>
			                    <td><?=$row['action']?></td>
			                    <td height="20"><?=$row['actioned_on']?></td>
			                    <td><?=$row['actioned_by']?></td>
			                    <td><?=$row['action_ip']?></td>
			                </tr>
                            <?
                            endforeach;
                    }
                            ?>
                    </table>
	    	</div>



       </div>


    </div>
    </body>
<script type="text/javascript">
//<![CDATA[
$(function() {
	$("ul.tabs").tabs("div.panes > .pane");
});
//]]>
</script>

</html>
