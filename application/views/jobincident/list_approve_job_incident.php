<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>List Payroll</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<base href="<?=base_url()?>"/>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.tools.tabs.css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript">
function gotopage(form, pageNo){
	$("#" + form + "_page_no").val(pageNo);
	submitForm(form);
}

function pagelimit(form){
	$("#" + form + "_page_no").val(1);
	submitForm(form);
}

function submitForm(form_id){
	if(form_id == "new"){
		var new_options = {
			dataType:  'json',
			success: new_response,
			url: "<?=site_url('jobincident/new_approvals_ajax')?>"
		};
		$('#'+form_id).ajaxSubmit(new_options);
	}else if(form_id == "progress"){
		var progress_options = {
			dataType:  'json',
			success: progress_response,
			url: "<?=site_url('jobincident/progress_approvals_ajax')?>"
		};
		$('#'+form_id).ajaxSubmit(progress_options);
	}else if(form_id == "completed"){
		var completed_options = {
			dataType:  'json',
			success: completed_response,
			url: "<?=site_url('jobincident/completed_approvals_ajax')?>"
		};
		$('#'+form_id).ajaxSubmit(completed_options);
	}

	return false;
}

function new_response(responseText, statusText, xhr, $form){
	$("#list_new").find("tr:gt(0)").remove();
	$('#list_new tr:last').after(responseText.content);
	$("#list_new").find("tr:eq(0)").remove();
}

function new_page(pageNo){
	$("#new_page_no").val(pageNo);
	submitForm("new");
}

function progress_response(responseText, statusText, xhr, $form){
	$("#list_progress").find("tr:gt(0)").remove();
	$('#list_progress tr:last').after(responseText.content);
	$("#list_progress").find("tr:eq(0)").remove();
}

function progress_page(pageNo){
	$("#progress_page_no").val(pageNo);
	submitForm("progress");
}

function completed_response(responseText, statusText, xhr, $form){
	$("#list_completed").find("tr:gt(0)").remove();
	$('#list_completed tr:last').after(responseText.content);
	$("#list_completed").find("tr:eq(0)").remove();
}

function completed_page(pageNo){
	$("#completed_page_no").val(pageNo);
	submitForm("completed");
}

function change_site(value, dept_id){
	var dept_select = document.getElementById(dept_id);
	dept_select.options.length=0;
	$.ajax({
		async: true,
		type: 'POST',
		url: "<?=site_url("approve/dept_by_site_ajax")?>",
		data: {
		  	site_id: value
		},
		success: function(data){
			$.each(data, function(i, dep){
				dept_select.options.add(new Option(dep.name,dep.id));
			});
	  	},
		dataType: 'json'
	});
}

function change_dept(form){
	search(form);
}

function search(form){
	$("#" + form + "_filter_site_id").val($("#" + form + "_site_id").val());
	$("#" + form + "_filter_department_id").val($("#" + form + "_department_id").val());
	$("#" + form + "_page_no").val(1);
	return submitForm(form);
}
$(function() {
	$("ul.tabs").tabs("div.panes > .pane");
});

</script>
<script type="text/javascript" src="js/piwik-lrs.js"></script>
</head>

<body>
  <div class="wrap">
	<!-- the tabs -->
	<ul class="tabs">
		<li><a href="#inProgress">In Progress(<?=$progress_count?>)</a></li>
		<li><a href="#new">New(<?=$new_count?>)</a></li>
		<li><a href="#completed">Completed(<?=$completed_count?>)</a></li>
	</ul>

	<!-- tab "panes" -->
	<div class="panes">
		<div class="pane" id="tab_inprogress">
	    <form id="progress" name="progress" method="post" action="">
  		  <input type="hidden" id="progress_page_no" name="progress_page_no" value=""/>
  		  <input type="hidden" id="progress_filter_site_id" name="progress_filter_site_id" value="<?=$site_id?>"/>
          <input type="hidden" id="progress_filter_department_id" name="progress_filter_department_id" value="<?=$department_id?>"/>
		  <table width="100%" border="0" cellspacing="0" cellpadding="0" background="images/Footer.png">
		    <tr>
		      <td>
		      <table width="100%" border="0" cellspacing="1" cellpadding="1">
              	<tr bgcolor="#FFFFFF">
                	<td align="center">
                		<?
                		$site = 'progress_site_id';
                		$department = 'progress_department_id';
                		if($this->session->userdata('site_id') == 0){
                		?>
                    		Site:
                            <?php
                            $js = 'id="'.$site.'" style="font-size: 11px; width: 120px;" onChange="change_site(this.value, \'' . $department . '\')"';
							echo form_dropdown($site, $sites, "", $js);
							echo "Dept.: ";
							$js = 'id="'.$department.'" style="font-size: 11px; width: 120px;" onChange="change_dept(\'progress\')"';
							echo form_dropdown($department, $departments, "", $js);
                		}else{
                			echo '<input type="hidden" id="'. $site .'" name="'. $site .'" value="'. $this->session->userdata('site_id') .'"/>';
                			echo "Dept.: ";
							$js = 'id="'.$department.'" style="font-size: 11px; width: 120px;" onChange="change_dept(\'progress\')"';
							echo form_dropdown($department, $departments, $department_id, $js);
                		}
						?>
					</td>
              	</tr>
              </table>
		      <table width="100%" border="0" cellspacing="1" cellpadding="0" id="list_progress">
		        <tr style="background-image:url('images/TableHeader.png');">
		          <td height="20">ID</td>
		          <td>Incident ID</td>
		          <td>Date</td>
		          <td>Customer(s)</td>
		          <td>Reference/Invoice No</td>
		          <td>Added by</td>
		          <td align="center">Action</td>
		        </tr>
		        <?
				if($progress_count > 0){
				$num = 0;
		        foreach ($progress_jobs as $row): ?>
		        <tr bgcolor="#FFFFFF">
		          <td ><?=++$num?></td>
		          <td ><?=$row->id?></td>
		          <td ><?=date(DATE_FORMAT, $row->incident_date)?></td>
		          <td >
		          <?=$row->customer?>
		          </td>
		          <td >
		          <?=$row->reference_no?>
		          </td>
		          <td><?=$row->first_name . " " . $row->last_name?></td>
		           <td align="center">
		           <a href="<?php echo site_url("jobincident/deal/".$row->id)?>">review</a><br>
		           <a href="<?php echo site_url("jobincident/view/".$row->id)?>/2">view</a>
		           </td>
		        </tr>
		        <? endforeach; ?>
		        <tr bgcolor="#FFFFFF" font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
		            <td align="right" colspan="8">
		            <?php
		            	$space = "&nbsp;&nbsp;&nbsp;";

		        		if($progress_page_count > 1){
		//                            		if($page_count > 1){
		//                                		echo $page_no ." / " . $page_count.$space;
		//                                	}
		//                                	if($page_no == 1){
		//                                		echo "First";
		//                                	}else{
		//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(1);'>First</a>";
		//                                	}
		                	echo $space;
		                	if($progress_page_no > 1){
		                		echo "<a href='javascript:void(0)' onclick='progress_page(" . ($page_no - 1) . ");'>Pre.</a>";
		                	}else{
		                		echo "Pre.";
		                	}
		                	echo $space;
		                	if($progress_page_no + 1 <= $progress_page_count){
		                		echo "<a href='javascript:void(0)' onclick='progress_page(" . ($progress_page_no + 1) . ");'>Next</a>";
		                	}else{
		                		echo "Next";
		                	}
		//                                	echo $space;
		//                                	if($page_no == $page_count || $page_count == 0){
		//                                		echo "Last";
		//                                	}else{
		//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . $page_count . ")'>Last</a>";
		//                                	}
		        			echo $space. "GoTo:";
		        			$page_dropdown_id = 'progress_page_dropdown';
		                    $js = 'id="'.$page_dropdown_id.'" style="font-size: 11px;" onChange="gotopage(\'progress\', this.value)"';
		                    $page_dropdown = array();
		                    for($i = 1; $i <= $progress_page_count; $i++){
		                    	$page_dropdown[$i] = $i;
		                    }
							echo form_dropdown($page_dropdown_id, $page_dropdown, $progress_page_no, $js);
		            	}
		            	echo $space. "Items Per Page:";
		            	$page_limit_id = 'progress_limit';
		            	$js = 'id="'.$page_limit_id.'" style="font-size: 11px;" onChange="pagelimit(\'progress\')"';
		            	echo form_dropdown($page_limit_id, pagination_option(), PAGE_SIZE, $js);

		            ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		            </td>
		        </tr>
		        <?}?>
		      </table></td>
		    </tr>
		  </table>
		  </form>
	    </div>
		<div class="pane" id="tab_new">
		<form id="new" name="new" method="post" action="">
  		  <input type="hidden" id="new_page_no" name="new_page_no" value=""/>
		  <input type="hidden" id="new_filter_site_id" name="new_filter_site_id" value="<?=$site_id?>"/>
          <input type="hidden" id="new_filter_department_id" name="new_filter_department_id" value="<?=$department_id?>"/>
		  <table width="100%" border="0" cellspacing="0" cellpadding="0" background="images/Footer.png">
		    <tr>
		      <td>
		      <table width="100%" border="0" cellspacing="1" cellpadding="1">
              	<tr bgcolor="#FFFFFF">
                		<td align="center">
                		<?
                		$site = 'new_site_id';
                		$department = 'new_department_id';
                		if($this->session->userdata('site_id') == 0){
                		?>
                    		Site:
                            <?php
                            $js = 'id="'.$site.'" style="font-size: 11px; width: 120px;" onChange="change_site(this.value, \'' . $department . '\')"';
							echo form_dropdown($site, $sites, "", $js);
							echo "Dept.: ";
							$js = 'id="'.$department.'" style="font-size: 11px; width: 120px;" onChange="change_dept(\'new\')"';
							echo form_dropdown($department, $departments, "", $js);
                		}else{
                			echo '<input type="hidden" id="'. $site .'" name="'. $site .'" value="'. $this->session->userdata('site_id') .'"/>';
                			echo "Dept.: ";
							$js = 'id="'.$department.'" style="font-size: 11px; width: 120px;" onChange="change_dept(\'new\')"';
							echo form_dropdown($department, $departments, $department_id, $js);
                		}
						?>
					</td>
              	</tr>
              </table>
		      <table width="100%" border="0" cellspacing="1" cellpadding="0" id="list_new">
		        <tr style="background-image:url('images/TableHeader.png');">
		          <td height="20">ID</td>
		          <td>Incident ID</td>
		          <td>Date</td>
		          <td>Customer(s)</td>
		          <td>Reference/Invoice No</td>
		          <td>Added by</td>
		          <td colspan="1" align="center">Action</td>
		        </tr>
		        <?
				if($new_count > 0){
					$num = 0;
		        foreach ($new_jobs as $row): ?>
		        <tr bgcolor="#FFFFFF">
		          <td ><?=++$num?></td>
		          <td ><?=$row->id?></td>
		          <td ><?=date(DATE_FORMAT, $row->incident_date)?></td>
		          <td >
		          <?=$row->customer?>
		          </td>
		          <td >
		          <?=$row->reference_no?>
		          </td>
		          <td><?=$row->first_name . " " . $row->last_name?></td>
		           <td align="center">
		           <a href="<?php echo site_url("jobincident/deal/".$row->id)?>">review</a><br>
		           <a href="<?php echo site_url("jobincident/view/".$row->id)?>/2">view</a>
		           </td>
		        </tr>
		        <? endforeach; ?>
		        <tr bgcolor="#FFFFFF" font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
		            <td align="right" colspan="7">
		            <?php
		            	$space = "&nbsp;&nbsp;&nbsp;";

		        		if($new_page_count > 1){
		//                            		if($page_count > 1){
		//                                		echo $page_no ." / " . $page_count.$space;
		//                                	}
		//                                	if($page_no == 1){
		//                                		echo "First";
		//                                	}else{
		//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(1);'>First</a>";
		//                                	}
		                	echo $space;
		                	if($new_page_no > 1){
		                		echo "<a href='javascript:void(0)' onclick='new_page(" . ($page_no - 1) . ");'>Pre.</a>";
		                	}else{
		                		echo "Pre.";
		                	}
		                	echo $space;
		                	if($new_page_no + 1 <= $new_page_count){
		                		echo "<a href='javascript:void(0)' onclick='new_page(" . ($new_page_no + 1) . ");'>Next</a>";
		                	}else{
		                		echo "Next";
		                	}
		//                                	echo $space;
		//                                	if($page_no == $page_count || $page_count == 0){
		//                                		echo "Last";
		//                                	}else{
		//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . $page_count . ")'>Last</a>";
		//                                	}
		        			echo $space. "GoTo:";
		        			$page_dropdown_id = 'new_page_dropdown';
		                    $js = 'id="'.$page_dropdown_id.'" style="font-size: 11px;" onChange="gotopage(\'new\', this.value)"';
		                    $page_dropdown = array();
		                    for($i = 1; $i <= $new_page_count; $i++){
		                    	$page_dropdown[$i] = $i;
		                    }
							echo form_dropdown($page_dropdown_id, $page_dropdown, $new_page_no, $js);
		            	}
		            	echo $space. "Items Per Page:";
		            	$page_limit_id = 'new_limit';
		            	$js = 'id="'.$page_limit_id.'" style="font-size: 11px;" onChange="pagelimit(\'new\')"';
		            	echo form_dropdown($page_limit_id, pagination_option(), PAGE_SIZE, $js);

		            ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		            </td>
		        </tr>
		        <?}?>
		      </table></td>
		    </tr>
		  </table>
		  </form>
		</div>

		<div class="pane" id="tab_completed">
		<form id="completed" name="completed" method="post" action="">
  		  <input type="hidden" id="completed_page_no" name="completed_page_no" value=""/>
  		  <input type="hidden" id="completed_filter_site_id" name="completed_filter_site_id" value="<?=$site_id?>"/>
          <input type="hidden" id="completed_filter_department_id" name="completed_filter_department_id" value="<?=$department_id?>"/>
		  <table width="100%" border="0" cellspacing="0" cellpadding="0" background="images/Footer.png">
		    <tr>
		      <td>
		      <table width="100%" border="0" cellspacing="1" cellpadding="1">
              	<tr bgcolor="#FFFFFF">
                		<td align="center">
                		<?
                		$site = 'completed_site_id';
                		$department = 'completed_department_id';
                		if($this->session->userdata('site_id') == 0){
                		?>
                    		Site:
                            <?php
                            $js = 'id="'.$site.'" style="font-size: 11px; width: 120px;" onChange="change_site(this.value, \'' . $department . '\')"';
							echo form_dropdown($site, $sites, "", $js);
							echo "Dept.: ";
							$js = 'id="'.$department.'" style="font-size: 11px; width: 120px;" onChange="change_dept(\'completed\')"';
							echo form_dropdown($department, $departments, "", $js);
                		}else{
                			echo '<input type="hidden" id="'. $site .'" name="'. $site .'" value="'. $this->session->userdata('site_id') .'"/>';
                			echo "Dept.: ";
							$js = 'id="'.$department.'" style="font-size: 11px; width: 120px;" onChange="change_dept(\'completed\')"';
							echo form_dropdown($department, $departments, $department_id, $js);
                		}
						?>
					</td>
              	</tr>
              </table>
		      <table width="100%" border="0" cellspacing="1" cellpadding="0" id="list_completed">
		        <tr style="background-image:url('images/TableHeader.png');">
		          <td height="20">ID</td>
		          <td>Incident ID</td>
		          <td>Date</td>
		          <td>Customer(s)</td>
		          <td>Reference/Invoice No</td>
		          <td>Added by</td>
		          <td colspan="1" align="center">Action</td>
		        </tr>
		        <?
				if($completed_count > 0){
					$num = 0;
		        foreach ($completed_jobs as $row): ?>
		        <tr bgcolor="#FFFFFF">
		          <td ><?=++$num?></td>
		          <td ><?=$row->id?></td>
		          <td ><?=date(DATE_FORMAT, $row->incident_date)?></td>
		          <td >
		          <?=$row->customer?>
		          </td>
		          <td >
		          <?=$row->reference_no?>
		          </td>
		          <td><?=$row->first_name . " " . $row->last_name?></td>
		           <td align="center">
		           <?
		           if($this->session->userdata('user_level') == 3){
		           	?>
		           	<a href="<?php echo site_url("jobincident/deal/".$row->id)?>">review</a><br>
		           	<?
		           }
		           ?>

		           <a href="<?php echo site_url("jobincident/view/".$row->id)?>/2">view</a>
		           </td>
		        </tr>
		        <? endforeach; ?>
		        <tr bgcolor="#FFFFFF" font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
		            <td align="right" colspan="7">
		            <?php
		            	$space = "&nbsp;&nbsp;&nbsp;";

		        		if($completed_page_count > 1){
		//                            		if($page_count > 1){
		//                                		echo $page_no ." / " . $page_count.$space;
		//                                	}
		//                                	if($page_no == 1){
		//                                		echo "First";
		//                                	}else{
		//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(1);'>First</a>";
		//                                	}
		                	echo $space;
		                	if($completed_page_no > 1){
		                		echo "<a href='javascript:void(0)' onclick='completed_page(" . ($page_no - 1) . ");'>Pre.</a>";
		                	}else{
		                		echo "Pre.";
		                	}
		                	echo $space;
		                	if($completed_page_no + 1 <= $completed_page_count){
		                		echo "<a href='javascript:void(0)' onclick='completed_page(" . ($completed_page_no + 1) . ");'>Next</a>";
		                	}else{
		                		echo "Next";
		                	}
		//                                	echo $space;
		//                                	if($page_no == $page_count || $page_count == 0){
		//                                		echo "Last";
		//                                	}else{
		//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . $page_count . ")'>Last</a>";
		//                                	}
		        			echo $space. "GoTo:";
		        			$page_dropdown_id = 'completed_page_dropdown';
		                    $js = 'id="'.$page_dropdown_id.'" style="font-size: 11px;" onChange="gotopage(\'completed\', this.value)"';
		                    $page_dropdown = array();
		                    for($i = 1; $i <= $completed_page_count; $i++){
		                    	$page_dropdown[$i] = $i;
		                    }
							echo form_dropdown($page_dropdown_id, $page_dropdown, $completed_page_no, $js);
		            	}
		            	echo $space. "Items Per Page:";
		            	$page_limit_id = 'completed_limit';
		            	$js = 'id="'.$page_limit_id.'" style="font-size: 11px;" onChange="pagelimit(\'completed\')"';
		            	echo form_dropdown($page_limit_id, pagination_option(), PAGE_SIZE, $js);

		            ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		            </td>
		        </tr>
		        <?}?>
		      </table></td>
		    </tr>
		  </table>
		  </form>
		</div>
	</div>
</div>
</body>
</html>
