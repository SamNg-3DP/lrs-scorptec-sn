
<tr>
		      <td>
		      
		      
		      
		      <table width="100%" border="0" cellspacing="1" cellpadding="0" id="list_new">
		        <tr style="background-image:url('images/TableHeader.png');">
		          <td height="20">No</td>
		          <td>Incident ID</td>
		          <td>User</td>
		          <td>Staff Involved</td>
		          <td>Date</td>
		          <td>Customer(s)</td>
		          <td>Reference/Order No</td>
		          <td>$ Value</td>
		          <td colspan="2" align="center">Action</td>
		        </tr>
		        <? 
				if($count > 0){
					$num = 0;     
					$total_value = 0;
		        foreach ($jobs as $row):
		        	$total_value += $row->cost_value;
		        	$num++;
		        ?>
		        <tr bgcolor="#FFFFFF">
		          <td height="20"><?=$num + ($page_limit * ($page_no-1))?></td>
		          <td ><?=$row->id?></td>
		          <td >
		          <?=$row->user_firstname.' '.$row->user_lastname?>
		          </td>
		          <td >
		          <?=$row->staff_firstname.' '.$row->staff_lastname?>
		          </td>		          
		          <td ><?=date(DATE_FORMAT, $row->incident_date)?></td>
		          <td >
		          <?=$row->customer?>
		          </td>
		          <td >
		          <?=$row->reference_no?>
		          </td>
		          <td align="right">
		          $<?=number_format($row->cost_value,2)?>
		          </td>
		          
		          <td align="center">
		          <?php if ($status=='new' || ($user_level >= 3)){?>
						<a href="<?php echo site_url("jobincident/edit/".$row->id);?>">Edit</a>
						&nbsp;&nbsp;&nbsp;
				  <?php }?>
			      <?php if ($user_level >=2 && ($status=='progress')){?>
						<a href="<?php echo site_url("jobincident/deal/".$row->id);?>">Review</a>
						&nbsp;&nbsp;&nbsp;
				  <?php }?>
		          <a href="<?php echo site_url("jobincident/view/".$row->id);?>">View</a>
		          </td>
		        </tr>
		        <? endforeach; ?>
		        <tr bgcolor="#FFFFFF" font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
		        	<td align="right" colspan="7">Total Value:</td><td align="right" >$<?=number_format($total_value,2) ?></td>
		        	<td>&nbsp;</td>
		        </tr>
		        
		        <tr bgcolor="#FFFFFF" font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
		            <td align="right" colspan="9">
		            <?php 
		            	$space = "&nbsp;&nbsp;&nbsp;";
		
                            		if($page_count > 1){
	                                	echo $space;
	                                	if($page_no > 1){
	                                		echo "<a href='javascript:void(0)' onclick='".$status."_page(" . ($page_no - 1) . ");'>Pre.</a>";
	                                	}else{
	                                		echo "Pre.";
	                                	}
	                                	echo $space;
	                                	if($page_no + 1 <= $page_count){
	                                		echo "<a href='javascript:void(0)' onclick='".$status."_page(" . ($page_no + 1) . ");'>Next</a>";
	                                	}else{
	                                		echo "Next";
	                                	}
                            			echo $space. "GoTo:";
                            			$page_dropdown_id = $status.'_page_dropdown';
		                                $js = 'id="'.$page_dropdown_id.'" style="font-size: 11px;" onChange="gotopage(\''.$status.'\', this.value)"';
		                                $page_dropdown = array();
		                                for($i = 1; $i <= $page_count; $i++){
		                                	$page_dropdown[$i] = $i;
		                                }
										echo form_dropdown($page_dropdown_id, $page_dropdown, $page_no, $js);
                                	}
                                	echo $space. "Items Per Page:";
                                	$page_limit_id = $status.'_limit';
                                	$js = 'id="'.$page_limit_id.'" style="font-size: 11px;" onChange="pagelimit(\''.$status.'\')"';
                                	echo form_dropdown($page_limit_id, pagination_option(), $page_limit, $js);
		            	
		            	
		            ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		            </td>
		        </tr>
		        <?}?>
		      </table></td>
		    </tr>