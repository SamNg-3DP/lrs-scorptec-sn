<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>List Leave Type </title>
<base href="<?=base_url()?>"/>
<style type="text/css">
<!--
#form1 table tr td table {
	text-align: left;
}
#form1 p {
	text-align: left;
}
#form1 table tr td table tr td {
    font-family: Tahoma, Geneva, sans-serif;
	font-size: 11px;
}
-->
</style>
<script type="text/javascript" src="js/piwik-lrs.js"></script>
</head>

<body>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" background="images/Footer.png">
    <tr>
      <td><table width="100%" border="0" cellspacing="1" cellpadding="1">
        <tr bgcolor="#EBEBEB">
            <td height="20" colspan="9" align="right">
                <label>
                    <a href="<?=site_url('leavetype/add')?>" style="font-size:11px">ADD NEW</a>
                </label>
            </td>
        </tr>
        <tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
		    <th rowspan="2" scope="col">No.</th>
		    <th rowspan="2" scope="col">Name</th>
		    <th colspan="2" scope="col" align="center">Outcome</th>
		    <th rowspan="2" scope="col">Pay status</th>
		    <td rowspan="2" scope="col">Color</td>
		    <th rowspan="2" scope="col">Status</th>
		    <th rowspan="2" colspan="2" scope="col">Action</th>
		  </tr>
		  <tr>
		    <th scope="col" align="center">Working hours</th>
		    <th scope="col" align="center">Leave Balance</th>
		  </tr>
        <?php if (empty($leave_types)) { ?>
        <tr bgcolor="#FFFFFF">
          <td height="20" colspan="6" style="color: #F00">No leave types!</td>
        </tr>
        <?php } else { $num = 0; ?>
		<?php foreach ($leave_types as $row): $num = $num + 1; ?>
        <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
          <td width="60" height="20"><?=$num?></td>
          <td height="20"><?=$row->leave_type?></td>
          <td height="20"><?=$row->working_outcome?></td>
          <td height="20"><?=$row->leave_outcome?></td>
          <td height="20">
          <?php
          if($row->pay_status == 1){
          	echo "Paid";
          }else if($row->pay_status == 2){
          	echo "Unpaid";
          }
          ?></td>
          <td height="20" bgcolor="<?=$row->color?>"></td>
          <td width="120" height="20"><?=($row->status==1)?'Active':'Inactive'?></td>
          <td width="50" height="20" align="center"><a href="<?=site_url('leavetype/edit/'.$row->id)?>"><img border="0" src="images/action/b_edit.png" width="16" height="16" title="Edit"/></a></td>
          <td height="20" align="center"><a href="<?=site_url('leavetype/change_status/'.$row->id.'/'.$row->status)?>">Change Status</a></td>
        </tr>
		<?php endforeach; } ?>
      </table>
      </td>
    </tr>
  </table>
</form>
</body>
</html>
