<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Leave approval</title>
<base href="<?=base_url()?>"/>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.tools.tabs.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />
<style type="text/css">
<!--
#form1 table tr td table {
	text-align: left;
}
#form1 p {
	text-align: left;
}
#form1 table tr td table tr td {
    font-family: Tahoma, Geneva, sans-serif;
	font-size: 11px;
}
/* tab pane styling */
div.panes div {
	display:none;
	padding:15px 10px;
	border:1px solid #999;
	border-top:0;
	font-size:14px;
	background-color:#fff;
}

div.pane {
	display:none;
	padding:15px 10px;
	border:1px solid #999;
	border-top:0;
	font-size:14px;
	background-color:#fff;
}
-->
</style>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/piwik-lrs.js"></script>
</head>
<body>

<script type="text/javascript">

$(function() {
	$("#flt_from_date").datepicker({
		disabled: true,
		dateFormat: '<?=JS_DATE_FORMAT?>',
		changeMonth: true,
		changeYear: true
	});
	$("#flt_to_date").datepicker({
		disabled: true,
		dateFormat: '<?=JS_DATE_FORMAT?>',
		changeMonth: true,
		changeYear: true
	});
});


function change_site(value){
	var dept_select = document.getElementById('flt_department');
	dept_select.options.length=0;
	$.ajax({
		async: true,
		type: 'POST',
		url: "<?=site_url("approve/dept_by_site_ajax")?>",
		data: {
		  	site_id: value
		},
		success: function(data){
			$.each(data, function(i, dep){
				dept_select.options.add(new Option(dep.name,dep.id));
			});
	  	},
		dataType: 'json'
	});
}

</script>

<base href="<?=base_url()?>"/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="90%">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="20"><h2>Actatek Clock In/Out Log</h2></td>
                </tr>
            </table>
	        <?php
	        echo form_open_multipart('request/view_actual_clock', array('id'=>'user_clock', 'method'=>'post'));
	        ?>

        	<input type="hidden" id="user_clock_page_no" name="user_clock_page_no" value=""/>
            <table  border="0" cellpadding="0" cellspacing="0" background="images/Footer.png">
                <tr>
                    <td>
                    	<table  width="100%" border="0" cellspacing="1" cellpadding="1">
                        	<tr bgcolor="#FFFFFF">
                        		<td align="center">
	                        		Site:
	                                <?php
	                                $js = 'id="flt_site" style="font-size: 11px; width: 120px;" onChange="change_site(this.value)"';
									echo form_dropdown("flt_site", $sites, $selected_site, $js);

                        			echo "&nbsp;&nbsp;Dept: ";
                        			$js = 'id="flt_department" style="font-size: 11px; width: 120px;"';
                        			echo form_dropdown("flt_department", $departments, $selected_department, $js);

									?>
									Time period:
									<?php
	                                $js = 'id="flt_time_period" style="font-size: 11px; width: 120px;"';
									echo form_dropdown('flt_time_period', time_period(), $selected_time_period, $js);
									?>
									&nbsp;&nbsp;Date:<input type="text" name="flt_from_date" id="flt_from_date" style="font-size:11px" size='8' value="<?=$selected_from_date?>"/> -
									<input type="text" name="flt_to_date" id="flt_to_date" style="font-size:11px" size='8'value="<?=$selected_to_date?>"/>

									&nbsp;&nbsp;User:
									<?php
	                                $js = 'id="flt_user_name" style="font-size: 11px; width: 120px;"';
									echo form_dropdown('flt_user_name', $users, $selected_user, $js);
									?>
									<input type="submit" value="search" />
								</td>
                            </tr>
                        </table>
                        <table  width="100%" border="0" cellspacing="1" cellpadding="5">
                            <tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;" >
                                <td width="30">&nbsp;</td>
                                <td width="120">Name</td>
                                <td width="45">Direction</td>
                                <td colspan="10">Time</td>
                                <td width="100">Total Working Hours</td>
                            </tr>
                            <?
                            if(!empty($clock)){
	                            $num = 0;
	                            foreach ($clock as $date => $user):
	                            	$num++;
	                            ?>
	                            <tr bgcolor="#DDDDDD">
	                            	<td colspan="14"><strong><?php echo date("D d F Y", $date);?></strong></td>
	                            </tr>

	                            <? foreach ($user as $id => $u){?>
		                            <tr bgcolor="#FFFFFF">
	                            		<?	if ($u['last_dir']=='IN')
	                            				echo '<td bgcolor="#44FF05" align="center" rowspan="2"><strong>IN</strong></td>';
											else if ($u['last_dir']=='OUT')
	                            				echo '<td bgcolor="#FF4242" align="center" rowspan="2"><strong>OUT</strong></td>';
											else
												echo '<td align="center" rowspan="2">&nbsp;</td>';
                            			?>

		                            	<td><?=$id?></td>
		                            	<td><strong>IN</strong></td>
		                            	<? 	$i = 1;
		                            		$a_in = count($u['IN']);
		                            		$colspan = 10;
		                            		foreach($u['IN'] as $in){
			                            		if ($i == $a_in || $a_in == 0)
			                            			$colspan = 10 - $i;
			                            		else
			                            			$colspan = 1;
		                            	?>
			                            		<td width='50'><?= $in; ?></td>
		                            	<? 		$i++;
		                            		}?>
		                            	<td  colspan="<?=$colspan ?>">&nbsp;</td>
		                            	<td><?=$u['total']?></td>
		                            </tr>
		                            <tr bgcolor="#FFFFFF">
		                            	<td>&nbsp;</td>

		                            	<td><strong>OUT</strong></td>
		                            	<? 	$o = 1;
		                            		$a_out = count($u['OUT']);
		                            		$colspan = 10;
			                            	foreach($u['OUT'] as $out){
		                            			if ($o == $a_out || $a_out == 0)
		                            				$colspan = 10 - $o;
		                            			else
		                            				$colspan = 1;
		                            	?>
			                            		<td width='50'><?= $out; ?></td>
		                            	<? 		$o++;
			                            	}?>
			                            <td  colspan="<?=$colspan ?>">&nbsp;</td>
										<td>&nbsp;</td>
		                            </tr>
	                            <? }
	                            endforeach;
	                            ?>
	                            <?php
                            }else{
                            ?>
								<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
	                                <td height="20" colspan="14">No record</td>
	                            </tr>
                            <?php }?>
                        </table>
                    </td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
</table>
