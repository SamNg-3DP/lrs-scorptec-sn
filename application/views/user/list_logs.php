<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>User login logs</title>
<base href="<?=base_url()?>"/>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />
<style type="text/css">
<!--
#form1 table tr td table {
	text-align: left;
}
#form1 p {
	text-align: left;
}
#form1 table tr td table tr td {
    font-family: Tahoma, Geneva, sans-serif;
	font-size: 11px;
}
/* tab pane styling */
div.panes div {
	display:none;
	padding:15px 10px;
	border:1px solid #999;
	border-top:0;
	font-size:14px;
	background-color:#fff;
}

div.pane {
	display:none;
	padding:15px 10px;
	border:1px solid #999;
	border-top:0;
	font-size:14px;
	background-color:#fff;
}
-->
</style>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
<!-- This JavaScript snippet activates those tabs -->

function submitForm(form_id){
	if(form_id == "log"){
		var log_options = {
				dataType:  'json',
				success: log_response,
				url: "<?=site_url('user/login_log_ajax')?>"
			};
		$('#'+form_id).ajaxSubmit(log_options);
	}

	return false;
}


function log_response(responseText, statusText, xhr, $form){
	$("#list_log").find("tr:gt(0)").remove();
	$('#list_log tr:last').after(responseText.content);
	$("#list_log").find("tr:eq(0)").remove();
}

function search(form){
	$("#" + form + "_filter_from_date").val($("#" + form + "_from_date").val());
	$("#" + form + "_filter_to_date").val($("#" + form + "_to_date").val());
	$("#" + form + "_page_no").val(1);

	return submitForm(form);
}

function gotopage(form, pageNo){
	$("#" + form + "_page_no").val(pageNo);
	submitForm(form);
}

function pagelimit(form){
	$("#" + form + "_page_no").val(1);
	submitForm(form);
}

function log_page(pageNo){
	$("#log_page_no").val(pageNo);
	submitForm("log");
}

$(function() {
	$("#<?=$form_id?>_from_date").datepicker({
		disabled: true,
		dateFormat: '<?=JS_DATE_FORMAT?>',
		changeMonth: true,
		changeYear: true
	});
	$("#<?=$form_id?>_to_date").datepicker({
		disabled: true,
		dateFormat: '<?=JS_DATE_FORMAT?>',
		changeMonth: true,
		changeYear: true
	});
});

</script>
<script type="text/javascript" src="js/piwik-lrs.js"></script>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="90%">
            <form id="<?=$form_id?>" name="<?=$form_id?>" method="post" action="">
        	<input type="hidden" id="<?=$form_id?>_page_no" name="<?=$form_id?>_page_no" value=""/>
        	<input type="hidden" id="<?=$form_id?>_filter_from_date" name="<?=$form_id?>_filter_from_date" value=""/>
        	<input type="hidden" id="<?=$form_id?>_filter_to_date" name="<?=$form_id?>_filter_to_date" value=""/>
        	<table width="100%" border="0" cellspacing="1" cellpadding="1">
                <tr bgcolor="#FFFFFF">
                	<td width="100" height="20"  align="right">From Date:</td>
                	<td width="30"><input type="text" name="<?=$form_id?>_from_date" id="<?=$form_id?>_from_date" style="font-size:11px"/></td>
                	<td width="50" align="right">To Date:</td>
                	<td width="30"><input type="text" name="<?=$form_id?>_from_date" id="<?=$form_id?>_to_date" style="font-size:11px"/></td>
                	<td >&nbsp;<input type="button" value="search" onclick="search('<?=$form_id?>');" /></td>
            	</tr>
            </table>
            <table width="100%" border="0" cellspacing="1" cellpadding="1" id="list_<?=$form_id?>">
                <tr style="background-image:url('images/TableHeader.png');">
                    <td width="20" height="20">#</td>
                    <td width="70">Log ID</td>
                    <td width="100">Applicant</td>
                    <td width="117">Operator</td>
                    <td width="100">Log Date</td>
                </tr>
                <?
                if(!empty($logs)){
                    $num = 0;
                foreach ($logs as $row):
                    $num++;
                ?>
                <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
                    <td height="20"><?=$num?></td>
                    <td height="20"><?=$row->id?></td>
                    <td height="20"><?=$row->first_name?> <?=$row->last_name?></td>
                    <td height="20"><?=$row->log_info?></td>
                    <td><?=date(DATETIME_FORMAT,$row->log_time)?></td>
                </tr>
                <?
                endforeach;
                ?>
                <tr bgcolor="#FFFFFF" font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                    <td align="right" colspan="5">
                     <?php
            	$space = "&nbsp;&nbsp;&nbsp;";

        		if($page_count > 1){
//                            		if($page_count > 1){
//                                		echo $page_no ." / " . $page_count.$space;
//                                	}
//                                	if($page_no == 1){
//                                		echo "First";
//                                	}else{
//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(1);'>First</a>";
//                                	}
                	echo $space;
                	if($page_no > 1){
                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . ($page_no - 1) . ");'>Pre.</a>";
                	}else{
                		echo "Pre.";
                	}
                	echo $space;
                	if($page_no + 1 <= $page_count){
                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . ($page_no + 1) . ");'>Next</a>";
                	}else{
                		echo "Next";
                	}
//                                	echo $space;
//                                	if($page_no == $page_count || $page_count == 0){
//                                		echo "Last";
//                                	}else{
//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . $page_count . ")'>Last</a>";
//                                	}
        			echo $space. "GoTo:";
        			$page_dropdown_id = $form_id.'_page_dropdown';
                    $js = 'id="'.$page_dropdown_id.'" style="font-size: 11px;" onChange="gotopage(\''.$form_id.'\', this.value)"';
                    $page_dropdown = array();
                    for($i = 1; $i <= $page_count; $i++){
                    	$page_dropdown[$i] = $i;
                    }
					echo form_dropdown($page_dropdown_id, $page_dropdown, $page_no, $js);
            	}
            	echo $space. "Items Per Page:";
            	$page_limit_id = $form_id.'_limit';
            	$js = 'id="'.$page_limit_id.'" style="font-size: 11px;" onChange="pagelimit(\''.$form_id.'\')"';
            	echo form_dropdown($page_limit_id, pagination_option(), PAGE_SIZE, $js);

            ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <?php
                }else{
                ?>
                <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
                    <td height="20" colspan="11">No record</td>
                </tr>
                <?php }?>
            </table>
            </form>
        </td>
    </tr>
</table>

</body>
</html>
