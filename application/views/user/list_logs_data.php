                <tr style="background-image:url('images/TableHeader.png');">
                    <td width="20" height="20">#</td>
                    <td width="70">Log ID</td>
                    <td width="100">Applicant</td>
                    <td width="117">Operator</td>
                    <td width="100">Log Date</td>
                </tr>
                <? 
                if(!empty($logs)){
                    $num = 0;
                foreach ($logs as $row): 
                    $num++;
                ?>
                <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
                    <td height="20"><?=$num?></td>
                    <td height="20"><?=$row->id?></td>
                    <td height="20"><?=$row->first_name?> <?=$row->last_name?></td>
                    <td height="20"><?=$row->log_info?></td>
                    <td><?=date(DATETIME_FORMAT,$row->log_time)?></td>
                </tr>
                <? 
                endforeach;
                ?>
                <tr bgcolor="#FFFFFF" font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                    <td align="right" colspan="5">
                     <?php 
            	$space = "&nbsp;&nbsp;&nbsp;";

        		if($page_count > 1){
//                            		if($page_count > 1){
//                                		echo $page_no ." / " . $page_count.$space;
//                                	}
//                                	if($page_no == 1){
//                                		echo "First";
//                                	}else{
//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(1);'>First</a>";
//                                	}
                	echo $space;
                	if($page_no > 1){
                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . ($page_no - 1) . ");'>Pre.</a>";
                	}else{
                		echo "Pre.";
                	}
                	echo $space;
                	if($page_no + 1 <= $page_count){
                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . ($page_no + 1) . ");'>Next</a>";
                	}else{
                		echo "Next";
                	}
//                                	echo $space;
//                                	if($page_no == $page_count || $page_count == 0){
//                                		echo "Last";
//                                	}else{
//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . $page_count . ")'>Last</a>";
//                                	}                            			
        			echo $space. "GoTo:";
        			$page_dropdown_id = $form_id.'_page_dropdown';
                    $js = 'id="'.$page_dropdown_id.'" style="font-size: 11px;" onChange="gotopage(\''.$form_id.'\', this.value)"';
                    $page_dropdown = array();
                    for($i = 1; $i <= $page_count; $i++){
                    	$page_dropdown[$i] = $i;
                    }
					echo form_dropdown($page_dropdown_id, $page_dropdown, $page_no, $js);
            	}
            	echo $space. "Items Per Page:";
            	$page_limit_id = $form_id.'_limit';
            	$js = 'id="'.$page_limit_id.'" style="font-size: 11px;" onChange="pagelimit(\''.$form_id.'\')"';
            	echo form_dropdown($page_limit_id, pagination_option(), $page_limit, $js);
            	
            ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <?php 
                }else{
                ?>
                <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
                    <td height="20" colspan="11">No record</td>
                </tr>
                <?php }?>
