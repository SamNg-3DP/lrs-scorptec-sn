						<table width="100%" border="0" cellspacing="1" cellpadding="1">
                            <tr bgcolor="#FFFFFF" style="white-space:nowrap;background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 12px;">
                                <td width="20" height="20">#</td>
                                <td width="20" height="20">Photo</td>
                                <td height="20"><a href="javascript:void(0)" onclick="sort('users_form','user_name');">User Name</a><?=$sort_user_name?></td>
                                <td height="20"><a href="javascript:void(0)" onclick="sort('users_form','first_name');">First Name</a><?=$sort_first_name?></td>
                                <td height="20"><a href="javascript:void(0)" onclick="sort('users_form','last_name');">Last Name</a><?=$sort_last_name?></td>
                                <td height="20">Hire Date</td>
                                <td height="20"><a href="javascript:void(0)" onclick="sort('users_form','review_date');">Review Date</a><?=$sort_review_date?></td>
                                <td height="20">Age</td>
                                <td height="20">Employment Period</td>
                                <td height="20"><a href="javascript:void(0)" onclick="sort('users_form','birthday');">Date of Birth</a><?=$sort_birthday?></td>
                                <td height="20"><a href="javascript:void(0)" onclick="sort('users_form','department_id');">Department</a><?=$sort_department_id?></td>
                                <td height="20"><a href="javascript:void(0)" onclick="sort('users_form','user_level_id');">User Level</a><?=$sort_user_level_id?></td>
                                <td height="20">Employment Type</td>
                                <td height="20">email</td>
                                <td height="20">Last Login</td>
                                <td height="20" colspan="6" align="center" style="color:#EEF8FC">Action</td>
                            </tr>

                            <?php if (empty($users)) { ?>
                            <tr bgcolor="#FFFFFF">
                                <td height="20" colspan="18" style="color: #F00">No Users!</td>
                                <?php
                                $page_limit_id = $form_id.'_limit';
                                ?>
                                <input type="hidden" name="<?=$page_limit_id?>" id="<?=$page_limit_id?>" value="<?=PAGE_SIZE?>" />
                            </tr>
                                <?php } else {
                                $num = 0; ?>
    						<?php foreach ($users as $row): $num = $num + 1; ?>
                            <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
                                <td height="20"><?=$num?></td>
                                <td height="20">
                                <?
                                if(empty($row->photo_file)){
                                	$photo_url = DIR_PHOTO . "/nophoto.jpg";
									echo "<img src='". $photo_url ."' hight='50' width='40'/>";
                                }else{
									$photo_url = DIR_PHOTO . $row->photo_file;
									echo "<a href='".$photo_url ."' target='_blank'><img src='". $photo_url ."' hight='50' width='40'/></a>";
								}
								?></td>
                                <td height="20"><?=$row->user_name?></td>
                                <td height="20"><?=$row->first_name?></td>
                                <td height="20"><?=$row->last_name?></td>
                                <td height="20">
                                <?php
                                if($row->hire_date > 0){
                                	echo date(DATE_FORMAT, $row->hire_date);
                                }else{
                                	echo "";
                                }
                                ?>
                                </td>
                                <td height="20">
                                <?php
                                if($row->review_date > 0){
                                	echo date(DATE_FORMAT, $row->review_date);
                                }else{
                                	echo "";
                                }
                                ?>
                                </td>

                                <td>
                                <?php
                                	echo calculate_age($row->birthday);
                                ?>
                                </td>
                                <td height="20">
                                <?php
                                	echo employment_period($row->hire_date, now());
                                ?>
                                </td>
                                <td height="20">
                                <?php
                                if(!empty($row->birthday)){
                                	echo date(DATE_FORMAT, $row->birthday);
                                }
                                ?>
                                </td>
                                <td height="20"><?=$row->department?></td>
                                <td height="20"><?=$row->user_level?></td>
                                <td height="20"><?=$row->employment_type?></td>
                                <td height="20"><?=$row->email?></td>
                                <td height="20">
                                <?php
                                if(!empty($row->last_login_time)){
                                	echo date(DATE_FORMAT, $row->last_login_time);
                                }
                                ?>
                                </td>
                                <td width="20"><a href="<?=site_url("user/history/".$row->user_name)?>" target="_blank">History</a></td>
                                <td width="20"><a href="<?=site_url("user/working_hours/".$row->user_name)?>">Working</a></td>
                                <td width="20"><a href="<?=site_url("user/view_stats/".$row->id)?>">Stats</a></td>
                                <td width="20"><a href="<?=site_url("userinfo/detail/".$row->user_name)?>"><img border="0" src="images/action/b_select.png" alt="" width="16" height="16" /></a></td>
                                <td width="20"><a href="<?php echo site_url("user/edit/".$row->id)?>"><img border="0" src="images/action/b_edit.png" alt="" width="16" height="16" /></a></td>
                                <td width="20" height="22"><a href="<?php echo site_url("user/remove/".$row->user_name)?>"><img border="0" src="images/action/b_drop.png" width="16" height="16" /></a></td>
                            </tr>
    						<?php endforeach;
                                ?>

                                <tr bgcolor="#FFFFFF" font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
	                                <td align="right" colspan="19">
	                                 <?php
                                	$space = "&nbsp;&nbsp;&nbsp;";

                                	if($page_count > 1){
                                		//                                	if($page_count > 1){
//                                		echo $page_no ." / " . $page_count.$space;
	//                                	}
	//                                	if($page_no == 1){
	//                                		echo "First";
	//                                	}else{
	//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(1);'>First</a>";
	//                                	}
	//                                	echo $space;
	                                	if($page_no > 1){
	                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . ($page_no - 1) . ");'>Pre.</a>";
	                                	}else{
	                                		echo "Pre.";
	                                	}
	                                	echo $space;
	                                	if($page_no+1 <= $page_count){
	                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . ($page_no + 1) . ");'>Next</a>";
	                                	}else{
	                                		echo "Next";
	                                	}
	//                                	echo $space;
	//                                	if($page_no == $page_count || $page_count == 0){
	//                                		echo "Last";
	//                                	}else{
	//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . $page_count . ")'>Last</a>";
	//                                	}
                            			echo $space;
                            			$page_dropdown_id = $form_id.'_page_dropdown';
		                                $js = 'id="'.$page_dropdown_id.'" style="font-size: 11px;" onChange="gotopage(\''.$form_id.'\', this.value)"';
		                                $page_dropdown = array();
		                                for($i = 1; $i <= $page_count; $i++){
		                                	$page_dropdown[$i] = $i;
		                                }
		                                echo "GoTo:&nbsp;";
										echo form_dropdown($page_dropdown_id, $page_dropdown, $page_no, $js);


                                	}
                                	echo $space. "Items Per Page:&nbsp;";
                                	$page_limit_id = $form_id.'_limit';
	                                $js = 'id="'.$page_limit_id.'" style="font-size: 11px;" onChange="pagelimit(\''.$form_id.'\')"';
	                                echo form_dropdown($page_limit_id, pagination_option(), $page_limit, $js);
                                ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	                                </td>
	                            </tr>
                                <?php

                                } ?>
                        </table>
