<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>User detail info</title>
<base href="<?=base_url()?>" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />
<style type="text/css">
<!--
#form1 table tr td table {
	font-family: Tahoma, Helvetica, sans-serif;
	font-size: 11px;
}

#form1 p {
	text-align: center;
}

#input,select {
	font-family: Tahoma, Helvetica, Sans-Serif;
	font-size: 11px;
	font-style: normal;
	font-weight: normal;
}
/* tab pane styling */
div.panes div {
	display:none;
	padding:15px 10px;
	border:1px solid #999;
	border-top:0;
	font-size:14px;
	background-color:#fff;
}

-->
</style>

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">

    $(function() {
        // setup ul.tabs to work as tabs for each div directly under div.panes
        $("ul.tabs").tabs("div.panes > div");
    });
</script>
<script type="text/javascript" src="js/piwik-lrs.js"></script>
</head>
<body>

<div id="wizard">
<ul class="tabs">
	<li><a href="#info">User Info</a></li>
</ul>
<div class="panes">
<div>
		<table width="100%" height="568" border="0" cellspacing="1" cellpadding="1">
			<tr bgcolor="#EBEBEB">
				<td width="50%" height="20" colspan="2" bgcolor="#EBEBEB">Basic User Info</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td width="25%" height="20">Login username:</td>
				<td width="75%" height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
						<?php
						echo $user->user_name;
						?>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">Login Password:</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="41%">
						<?php
						echo $user->password;
						?>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">First Name:</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%">
						<?php
						echo $user->first_name;
						?>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#FFFFFF">Last Name:</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%">
						<?php
						echo $user->last_name;
						?>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#FFFFFF">Sex:</td>
				<td height="20">
				<?php
					if($user->sex == 1){
						echo "Male";
					}else if($user->sex == 2){
						echo "Female";
					}
				?>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#FFFFFF">Department:</td>
				<td height="20">
				<?php
				echo $user->department;
				?>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#FFFFFF">User Level:</td>
				<td height="20">
				<?php
				echo $user->user_level;
				?>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#FFFFFF">Employment Type:</td>
				<td height="20">
				<?php
				echo $user->employment_type;
				?>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#FFFFFF">Hire Date:</td>
				<td height="20">
				<?php
				if($user->hire_date > 0){
                   echo date(DATE_FORMAT, $user->hire_date);
           		}
				?>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#FFFFFF">Terminate Date:</td>
				<td height="20">
				<?php
				if($user->terminate_date > 0){
                   echo date(DATE_FORMAT, $user->terminate_date);
           		}
				?>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#EBEBEB">Contact Details</td>
				<td height="20" bgcolor="#EBEBEB">&nbsp;</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Address:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="73%">
						<?php
						echo $user->address;
						?>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Home Phone:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%">
						<?php
						echo $user->home_phone;
						?>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Mobile:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%">
						<?php
						echo $user->mobile;
						?>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Email(work):</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="55%">
						<?php
							echo $user->email;
						?>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Email(personal):</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="55%">
						<?php
							echo $user->personal_email;
						?>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">Date of Birth:</td>
				<td height="20">
				<?php
				if($user->birthday > 0){
                   echo date(DATE_FORMAT, $user->birthday);
           		}
				?>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#EBEBEB">Next of Kin</td>
				<td height="20" bgcolor="#EBEBEB">&nbsp;</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Name:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<?php
				echo $user->kin_name;
				?>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Contact details:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<?php
				echo $user->kin_contact;
				?>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">Medical conditions (if any):</td>
				<td height="20">
				<?php
				echo $user->medical_conditions;
				?>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">Driver License No:</td>
				<td height="20">
				<?php
				echo $user->driver_license_no;
				if(!empty($user->driver_license_file)){
					echo "&nbsp;&nbsp;&nbsp;&nbsp;<a href='".DIR_DRIVER_LICENSE . $user->driver_license_file ."' target='_blank'>Download</a>";
				}
				?>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">Passport No:</td>
				<td height="20">
				<?php
				echo $user->passport_license_no;
				if(!empty($user->passport_license_file)){
					echo "&nbsp;&nbsp;&nbsp;&nbsp;<a href='".DIR_PASSPORT_LICENSE . $user->driver_license_file ."' target='_blank'>Download</a>";
				}
				?>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">TFN:</td>
				<td height="20">
				<?php
				echo $user->tfn;
				?>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#EBEBEB">Bank Details</td>
				<td height="20" bgcolor="#EBEBEB">&nbsp;</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Account Name:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%">
						<?php
						echo $user->bank_account_name;
						?>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Bank:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%">
						<?php
						echo $user->bank_name;
						?>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">BSB:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%">
						<?php
						echo $user->bank_bsb;
						?>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Account No:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%">
						<?php
						echo $user->bank_account_no;
						?>
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
</div>
</div><!-- end panes -->
</div><!-- end wizard -->
</body>
</html>
