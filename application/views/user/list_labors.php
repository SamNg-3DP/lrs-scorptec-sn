		<table width="100%" border="0" cellpadding="0" cellspacing="0" background="images/Footer.png" id="labor_list">
	          <tr>
	            <td><table width="100%" border="0" cellspacing="1" cellpadding="1">
	              <tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
	                <td width="29" height="20">#</td>
	                <td width="60">Entry Date</td>
	                <td width="60">Payroll shift</td>
	                <td width="60">Hourly Rate</td>
					<td width="60">Labor</td>
	                <td width="117">Add Date</td>
	                <td width="30" height="20" align="center" style="color:#FFF">Action</td>
	              </tr>
	              <? if (empty($labor_list)) { ?>
					<tr bgcolor="#FFFFFF">
						<td height="20" colspan="11" style="color: #F00">No labor contract!</td>
					</tr>
						<? } else { 
						$num = 0; ?>
    			<? foreach ($labor_list as $row): $num = $num + 1; ?>
	              <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
	                <td height="20"><?=$num?></td>
	                <td><?php echo date(DATE_FORMAT, $row->entry_date);?></td>
	                <td><?=$row->payrollshiftcode?></td>
	                <td><?=$row->hourly_rate?></td>
					<td>
					<?php 
						if(!is_null($row->labor_contract_file)){
							echo "<a target=_blank href= '".DIR_LABOR_CONTRACTOR . $row->labor_contract_file . "' >download</a>";
						}else{
							echo "-";
						}
					?></td>
	                <td><?php echo date(DATE_FORMAT, $row->add_time);?></td>
	                <td align="center"><a href="javascript:void(0)" onclick="remove_labor(<?=$row->id?>);"><img border="0" src="images/action/b_drop.png" width="16" height="16" /></a></td>
	              </tr>
	              <? endforeach; } ?>
	            </table></td>
	          </tr>	          
	        </table>