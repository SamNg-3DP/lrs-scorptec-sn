<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Untitled Document</title>
    <base href="<?=base_url()?>"></head>
    <script type="text/javascript" src="js/mootools.js"></script>
	<script type="text/javascript" src="js/calendar.rc4.js"></script>
	<script type="text/javascript">
	//<![CDATA[
		window.addEvent('domready', function() {
			myCal1 = new Calendar({ entrydate: 'd/m/Y' }, { direction: 1, tweak: {x: 6, y: 0} });
		});
	//]]>
	</script>
	<link rel="stylesheet" type="text/css" href="css/calendar.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="css/dashboard.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="css/i-heart-ny.css" media="screen" />
  <script type="text/javascript" src="js/piwik-lrs.js"></script>
</head>

<body>
<?php echo form_open('user/save_workingtime'); ?>
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0"
           background="images/Footer.png">
        <tr>
            <td>
                <table width="100%" border="0" cellspacing="1" cellpadding="0">
                    <tr bgcolor="#FFFFFF">
                        <td height="22" colspan="2" bgcolor="#EBEBEB"></td>
                    </tr>
                    <tr bgcolor="#FFFFFF">
                        <td width="25%" height="22" bgcolor="#FFFFFF">Entry Date:</td>
                        <td width="77%" height="22" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                              <td width="34%"><input type="text" name="entrydate" id="entrydate" style="font-size:11px;" value="<?php echo set_value('entrydate');?>"/></td>
                              <td width="66%"><?php echo form_error('entrydate'); ?></td>
                          </tr>
                        </table></td>
                    </tr>
                    <tr bgcolor="#FFFFFF">
                        <td height="22" colspan="2" bgcolor="#EBEBEB">Working Time</td>
                    </tr>
                    <tr>
                        <td height="22" bgcolor="#FFFFFF">Monday To Friday:</td>
                        <td height="22" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                                <td width="9%">StartTime</td>
                                <td width="18%"><select name="mstarttime" id="mstarttime" style="font-size:11px; width:100px;">
                                  <option value="9:30" selected="selected">9:30</option>
                                  <option value="10:00">10:00</option>
                </select></td>
                                <td width="8%">EndTime</td>
                                <td width="65%"><select name="mendtime" id="mendtime" style="font-size:11px; width:100px;">
                                  <option value="18:00">18:00</option>
                                  <option value="18:30">18:30</option>
                      </select></td>
                          </tr>
                            </table></td>
                    </tr>
                    <tr>
                        <td height="22" bgcolor="#FFFFFF">Saturday:</td>
                        <td height="22" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="9%">StartTime</td>
                            <td width="18%"><select name="sstarttime" id="sstarttime" style="font-size:11px; width:100px;">
                              <option value="10:00">10:00</option>
                              <option value="10:30">10:30</option>
                            </select></td>
                            <td width="8%">EndTime</td>
                            <td width="65%"><select name="sendtime" id="sendtime" style="font-size:11px; width:100px;">
                              <option value="15:00">15:00</option>
                              <option value="15:30">15:30</option>
                            </select></td>
                          </tr>
                        </table></td>
                    </tr>
                    <tr bgcolor="#FFFFFF">
                        <td height="22" colspan="2" align="center" bgcolor="#EBEBEB"><label>
                            <input type="submit" name="button" id="button" value="     Save     " style="font-size:11px;"/>
                        </label></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</form>
</body>
</html>
