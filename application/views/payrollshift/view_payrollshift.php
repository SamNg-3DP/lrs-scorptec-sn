<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>View payroll</title>
<base href="<?=base_url ()?>" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/piwik-lrs.js"></script>
</head>
<body>
  <input type="hidden" name="payrollshift_id" id="payrollshift_id" value="<?=$payrollshift->id?>"/>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" background="images/Footer.png">
    <tr>
      <td><table width="100%" border="0" cellspacing="1" cellpadding="0">
        <tr>
          <td height="20" colspan="3" bgcolor="#EBEBEB">&nbsp;</td>
        </tr>
        <tr>
          <td height="20" bgcolor="#FFFFFF">Site:</td>
          <td height="20" bgcolor="#FFFFFF"><label>
		  <?php
	      	$site = 'site_id';
			$js = 'id="'.$site.'" style="font-size: 11px;" disabled="disabled"';
			echo form_dropdown($site, $sites, set_value($site,$payrollshift->site_id), $js);
           ?>
          </label>
          <span style="color: #F00">＊</span>
          </td>
          <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
        <tr>
          <td width="20%" height="20" bgcolor="#FFFFFF">Payroll Shift Code:</td>
          <td height="20" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="30%">
                <?php echo $payrollshift->payrollshiftcode;?>
                <span style="color: #F00">＊</span></td>
                <td><?php echo form_error ( 'payrollshiftcode' );?></td>
              </tr>
          </table></td>
          <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
        <tr>
          <td height="20" bgcolor="#FFFFFF">Description:</td>
          <td height="20" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="65%">
                <?php echo $payrollshift->description;?>
                <span style="color: #F00">＊</span></td>
                <td><?php echo form_error ( 'description' );?></td>
              </tr>
          </table></td>
          <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
        <tr>
          <td height="20" bgcolor="#FFFFFF">Notes:</td>
          <td height="20" bgcolor="#FFFFFF"><label>
            <textarea name="notes" id="notes" cols="45" disabled="disabled" rows="5" style="font-size:11px; font-family:Tahoma, Geneva, sans-serif"><?=$payrollshift->notes?></textarea>
          </label></td>
          <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
        <tr>
          <td height="20" bgcolor="#FFFFFF">Pay (Days):</td>
          <td height="20" bgcolor="#FFFFFF">
          <label>
          <?php
          	$paydays_select = array(
          		"7" => "Every Week",
            	"14" => "2 Week",
          		"28" => "4 Week"
          	);
          	$js = 'id="paydays" disabled="disabled"';
          	echo form_dropdown("paydays" , $paydays_select, $payrollshift->paydays, $js);
          ?>
          </label>
          <span style="color: #F00">＊</span>
          </td>
          <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
        <tr>
          <td height="20" bgcolor="#FFFFFF">Start Date:</td>
          <td height="20" bgcolor="#FFFFFF">
          	<label>
            <?php echo date(DATE_FORMAT, $payrollshift->start_date);?>
          	</label>
          	<span style="color: #F00">＊</span>
          </td>
          <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
        <tr>
          <td height="20" bgcolor="#FFFFFF">Roll (Days):</td>
          <td height="20" bgcolor="#FFFFFF">
          <label>
          <?php
          	$rolldays_select = array(
          		""  => "Please select..",
          		"7" => "Every Week",
            	"14" => "2 Week",
          		"28" => "4 Week"
          	);
          	$js = 'id="rolldays" disabled="disabled" style="font-size:11px"';
          	echo form_dropdown("rolldays" , $paydays_select, $payrollshift->rolldays, $js);
          ?>
          </label>
          <span style="color: #F00">＊</span>
          </td>
          <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
        <tr>
        	<td colspan="3" bgcolor="#FFFFFF">
        		<table width="600" border="0" cellspacing="1" cellpadding="1" id="list_roll" >
                	<tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                    	<td width="20" height="20">Start</td>
                        <td width="80">Name</td>
                     	<td width="40">Shift</td>
                     	<td >Description</td>
                	</tr>
                	<?
                	$weekarray =  array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
                	$i = 0;
                	foreach ($rolls as $row): ?>
                	<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
				        <td height="20"><?=$row->start?></td>
				        <td height="20"><?
				        echo $weekarray[$row->name];
				        ?></td>
				        <td><?php
				        $shift = "shift_" . $i;
				        $js = 'id="'.$shift.'" disabled="disabled" style="font-size: 11px; width: 120px;" onChange="show_desc(this.value, \''.$i.'\')"';
						echo form_dropdown($shift, $daily_shifts, $row->daily_shift_id, $js);
				        ?></td>
				        <td ><div id="desc_<?=$i?>"><?=$row->description?></div></td>
				        <input type="hidden" name="start_<?=$i?>" value="<?=$row->start?>" />
				        <input type="hidden" name="name_<?=$i?>" value="<?=$row->name?>" />
				    </tr>
                	<?
                	$i++;
                	endforeach; ?>
        		</table>
        	</td>
        </tr>
      </table></td>
    </tr>
  </table>
</body>
</html>
