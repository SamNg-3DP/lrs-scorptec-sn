<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US" xml:lang="en">
<head>
	<title>LRS Left Leave Menu</title>
	<base href="<?=base_url()?>"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
</head>
<body>
<div class="Block">
    <div class="Block-tl"></div>
    <div class="Block-tr"></div>
    <div class="Block-bl"></div>
    <div class="Block-br"></div>
    <div class="Block-tc"></div>
    <div class="Block-bc"></div>
    <div class="Block-cl"></div>
    <div class="Block-cr"></div>
    <div class="Block-cc"></div>
    <div class="Block-body">
      <div class="BlockHeader">
        <div class="l"></div>
        <div class="r"></div>
        <div class="t">Admin</div>
      </div>
      <p><a href="<?=site_url('user')?>" target="main-frame">User</a></p>
      <p><a href="<?=site_url('site')?>" target="main-frame">Site</a></p>
      <p><a href="<?=site_url('department')?>" target="main-frame">Department</a></p>
	  <p><a href="<?=site_url('userlevel')?>" target="main-frame">User Level</a></p>
      <p><a href="<?=site_url('leavetype')?>" target="main-frame">Leave type</a></p>
      
    </div>
    <?php if ($this->session->userdata('user_level') == 5){ ?>
    <div class="Block-body">
      <div class="BlockHeader">
        <div class="l"></div>
        <div class="r"></div>
        
        <div class="t">Parameter</div>
      </div>
      <p><a href="<?=site_url('config/email')?>" target="main-frame"">Email</a></p>
      <p><a href="<?=site_url('config/leave_hours')?>" target="main-frame">Leave Hour</a></p>
      <p><a href="<?=site_url('config/payroll')?>" target="main-frame">Payroll</a></p>
      
    </div>
    <?php }?>
    <div class="Block-body">
      <div class="BlockHeader">
        <div class="l"></div>
        <div class="r"></div>
        <div class="t">Shift/Roster</div>
      </div>
      <p><a href="<?=site_url('shiftcategory')?>" target="main-frame"">Shift Category</a></p>
      <p><a href="<?=site_url('dailyshift')?>" target="main-frame">Daily Shift</a></p>
	  <p><a href="<?=site_url('payrollshift')?>" target="main-frame">Payroll Shift</a></p>
	  <p><a href="<?=site_url('publicholiday')?>" target="main-frame">Public Holiday</a></p>
    </div>
    <?php if ($this->session->userdata('user_level') == 5){ ?>
	<div class="Block-body">
    	<div class="BlockHeader">
        	<div class="l"></div>
        	<div class="r"></div>
        	<div class="t">Payroll</div>
    	</div>
    	<p><a href="<?=site_url('payroll/add')?>" target="main-frame"">Add Payroll Data</a></p>
    	<p><a href="<?=site_url('payroll/qb_export')?>" target="main-frame"">QB Export Payroll</a></p>
    	<p><a href="<?=site_url('payroll/history')?>" target="main-frame"">View Payroll History</a></p>
    	<p><a href="<?=site_url('payroll/adjustment')?>" target="main-frame"">Adjustment Payroll</a></p>
    </div>
    <?php }?>
    <div class="Block-body">
    	<div class="BlockHeader">
        	<div class="l"></div>
        	<div class="r"></div>
        	<div class="t">Logs</div>
    	</div>
    	<p><a href="<?=site_url('user/login_log')?>" target="main-frame">Users log</a></p>
    	<p><a href="<?=site_url('user/view_actual_clock')?>" target="main-frame">Clock In/Out</a></p>
    </div>
  </div>
</body>
</html>
