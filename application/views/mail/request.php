<p>Application</p>
<table width="450" border="1">
  <tr>
    <th width="186" align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Employee: </span></th>
    <td width="248">&nbsp;
    <? echo $request->first_name . " " . $request->last_name;?>
    </td>
  </tr>
  <tr>
    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Type:</span></th>
    <td>&nbsp;
    <?=$request->leave_type?>
    </td>
  </tr>
  <?php if ($request->site_id){?>
  <tr>
    	<th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Store</span></th>
    	<td>&nbsp;
    	<?=$store?>
    	</td>
  </tr>
  <?php }?>
  <?php if ($request->department_id){?>
  <tr>
    	<th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Department</span></th>
    	<td>&nbsp;
    	<?=$department?>
    	</td>
  </tr>
  <?php }?>
  
  <tr>
    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Requested  On</span></th>
    <td>&nbsp;
    <?=date(DATETIME_FORMAT, $request->add_time)?>
    </td>
  </tr>
<?php if($request->leave_type_id == LEAVE_TYPE_LATE || $request->leave_type_id == LEAVE_TYPE_LUNCH){ ?>
    <tr>
        <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Arrival Day:</span></th>
        <td>&nbsp;
        <?=date(DATETIME_FORMAT, $request->start_date)?>
        </td>
      </tr>
      <tr>
        <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Minutes used</span></th>
        <td>&nbsp;
        <?=round($request->hours_used*60,2)?>
        </td>
      </tr>
	 <tr>
	   <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Reason:</span></th>
	   <td>&nbsp;
	   <?=$request->reason?>
	   </td>
	 </tr>      
<?php }else{ ?>  
  <tr>
    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">From:</span></th>
    <td>&nbsp;
     <?
     if ($request->request_type == 1)
     	echo date(DATE_FORMAT, $request->start_date);
     else
     	echo date(DATETIME_FORMAT, $request->start_date);
     ?>
    </td>
  </tr>
  <tr>
    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">To:</span></th>
    <td>&nbsp;
    <?
    if ($request->request_type == 1)
    	echo date(DATE_FORMAT, $request->end_date);
    else
    	echo date(DATETIME_FORMAT, $request->end_date);
    ?>
    </td>
  </tr>
  <tr>
    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Reason:</span></th>
    <td>&nbsp;
    <?=$request->reason?>
    </td>
  </tr>
  
  <?php if($order_no){ ?>
  <tr>
    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Order:</span></th>
    <td>&nbsp;
    <a href="<?=$order_url?>"><?=$order_no?></a>
    </td>
  </tr>
  <tr>
  	<th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">QB Name:</span></th>
  	<td>&nbsp;
    <a href="<?=$qb_name?>"><?=$qb_name?></a>
    </td>
  </tr>
  <?php }?>
  
  <?
  if(!empty($amount_leave_left)){
  ?>
  <tr>
    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Available Leave (after this request)</span></th>
    <td>&nbsp;
    <?=$amount_leave_left?>
    </td>
  </tr>
  <?
  }
  ?>
  <tr>
  	<? if ($request->leave_type_id == LEAVE_TYPE_OVERTIME){?>
  			<th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Overtime duration</span></th>
  	<? }else{?>
    		<th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Leave duration</span></th>
    <? }?>	
    <td>&nbsp;
    <?
    if ($request->request_type == 1){
	    $day_count = (floor(($request->end_date - $request->start_date) / 86400) + 1);
		$total_days = "";
		if($day_count > 1){
	        $total_days = $day_count. " days";
	    } else{
	    	$total_days = $day_count. " day";
	    }
	    echo $total_days . "(".$request->hours_used." hours)";
    }elseif ($request->request_type == 2){
    	echo $request->hours_used." hours";
    }
    
    ?>
    </td>
  </tr>
<?php }?>  
</table>
<p>Response </p>
<p>Please <a href="<?=$appove_url?>"><?=$appove_name?></a></p>