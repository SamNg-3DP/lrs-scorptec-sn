<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Untitled Document</title>
    <base href="<?=base_url()?>"/>
    <script type="text/javascript" src="js/piwik-lrs.js"></script>
</head>

<body>
<form id="form1" name="form1" method="post" action="">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" background="images/Footer.png">
        <tr>
            <td>
                <table width="100%" border="0" cellspacing="1" cellpadding="0">
                    <tr>
                        <td height="20" colspan="9" align="right" bgcolor="#EBEBEB"><a href="<?=site_url('dailyshift/add')?>">ADD NEW</a></td>
                    </tr>
                    <tr style="background-image:url('images/TableHeader.png');">
                        <td width="10%" height="20">Shift Code</td>
                        <td height="20">Description</td>
                        <td width="15%" height="20">Site</td>
                        <td width="60" height="20">Lunch</td>
                        <td width="10%" height="20">Slot</td>
                        <td height="20" colspan="2">Action</td>
                    </tr>
                    <? foreach($dailyshift as $row): ?>
                    <tr>
                        <td height="20" bgcolor="#FFFFFF"><?=$row->shiftcode?></td>
                        <td height="20" bgcolor="#FFFFFF"><?=$row->description?></td>
                        <td height="20" bgcolor="#FFFFFF"><?=$row->name?></td>
                        <td height="20" bgcolor="#FFFFFF" align="center"><?=$row->lunch_hour?> hour</td>
                        <td height="20" bgcolor="#FFFFFF">
                        	<table width="100%" border="0" cellspacing="1" cellpadding="0">
								<tr>
									<td>Start</td>
									<td>End</td>
									<td>Category</td>
									<td>Rate</td>
								</tr>
								<?
								$label = "daily".$row->id;
		                        foreach($$label as $slot){ ?>
		                        	<tr onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
										<td><?=$slot->start_time?></td>
										<td><?=$slot->end_time?></td>
										<td><?=$slot->cat_name?></td>
										<td><?=$slot->rate?></td>
									</tr>
		                        <?
		                        }
		                        ?>
							</table>
                        </td>
                        <td width="5%" align="center" bgcolor="#FFFFFF"><a href="<?=site_url('dailyshift/edit/'.$row->id)?>"><img src="images/action/b_edit.png" width="16" height="16" border="0" /></a></td>
                        <td width="5%" height="20" align="center" bgcolor="#FFFFFF"><a href="<?=site_url('dailyshift/remove/'.$row->id)?>"><img src="images/action/b_drop.png" width="16" height="16" border="0" title="Remove"/></a></td>
                    </tr>
                    <? endforeach; ?>
                </table>
            </td>
        </tr>
    </table>
</form>
</body>
</html>
