<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Untitled Document</title>
    <base href="<?=base_url()?>"/>
    <script type="text/javascript">
        function a() {
        }
    </script>
</head>

<body>
<form id="form1" name="form1" method="post" action="">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" background="images/Footer.png">
        <tr>
            <td>
                <table width="100%" border="0" cellspacing="1" cellpadding="0">
                    <tr>
                        <td height="20" colspan="9" align="right" bgcolor="#EBEBEB"><a href="index.php/dailyshift/index_add">ADD NEW</a></td>
                    </tr>
                    <tr style="background-image:url('images/TableHeader.png');">
                        <td width="10%" height="20">Shift Code</td>
                        <td width="30%" height="20">Description</td>
                        <td width="15%" height="20">Site</td>
                        <td width="10%" height="20">Start Time</td>
                        <td width="10%">End Time</td>
                        <td width="10%">Shift Category</td>
                        <td width="5%">Rate</td>
                        <td height="20" colspan="2">Action</td>
                    </tr>
                    <? foreach($dailyshift as $row): ?>
                    <tr>
                        <td height="20" bgcolor="#FFFFFF"><?=$row->shiftcode?></td>
                        <td height="20" bgcolor="#FFFFFF"><?=$row->description?></td>
                        <td height="20" bgcolor="#FFFFFF"><?=$row->name?></td>
                        <td height="20" bgcolor="#FFFFFF"><?=$row->starttime?></td>
                        <td height="20" bgcolor="#FFFFFF"><?=$row->endtime?></td>
                        <td height="20" bgcolor="#FFFFFF"><?=$row->category?></td>
                        <td height="20" bgcolor="#FFFFFF"><?=$row->rate?></td>
                        <td width="5%" align="center" bgcolor="#FFFFFF"><a href="index.php/dailyshift/index_edit/<?=$row->shiftcode?>"><img src="images/action/b_edit.png" width="16" height="16" border="0" /></a></td>
                        <td width="5%" height="20" align="center" bgcolor="#FFFFFF"><a href="index.php/dailyshift/remove/<?=$row->shiftcode?>"><img src="images/action/b_drop.png" width="16" height="16" border="0" title="Remove"/></a></td>
                    </tr>
                    <? endforeach; ?>
                </table>
            </td>
        </tr>
    </table>
</form>
</body>
</html>
