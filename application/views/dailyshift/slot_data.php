<tbody id="slot<?=$num?>">
	<tr bgcolor="#EBEBEB">
		<td colspan="3">Time Slot&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void(0)' title="Add new time slot" style="text-decoration:none; " onclick='remove_slot(<?=$num?>);'>-</a></td>
	</tr>
    <tr>
      <td height="20" bgcolor="#FFFFFF">Start Time:</td>
      <td height="20" bgcolor="#FFFFFF">
      <input type="text" name="starttime<?=$num?>" id="starttime<?=$num?>" /> <a href="#" id="starttime<?=$num?>_toggler">Open time picker</a>
		<div id="starttime<?=$num?>_picker" class="time<?=$num?>_picker_div"/>
      </td>
      <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
    </tr>
    <tr>
      <td height="20" bgcolor="#FFFFFF">End Time:</td>
      <td height="20" bgcolor="#FFFFFF">
      <input type="text" name="endtime<?=$num?>" id="endtime<?=$num?>" /> <a href="#" id="endtime<?=$num?>_toggler">Open time picker</a>
		<div id="endtime<?=$num?>_picker" class="time_picker_div"/>
      </td>
      <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
    </tr>
    <tr>
      <td height="20" bgcolor="#FFFFFF">Shift Category:</td>
      <td height="20" bgcolor="#FFFFFF">
      <label>
        <?
        $id = "shiftcategory". $num;
        $js = 'id="'.$id.'" style="font-size:11px" ';
        echo form_dropdown($id, $shiftcategory, set_value($id), $js);
        ?>
      </label>
      </td>
      <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
    </tr>
    <tr>
      <td height="20" bgcolor="#FFFFFF">
      Rate:
      </td>
      <td height="20" bgcolor="#FFFFFF">
      <label>
        <?
        $rate_id = "rate".$num;
	    $js = 'id="<?=$rate_id?>" style="font-size:11px" ';
	    echo form_dropdown($rate_id, $rates, set_value($rate_id), $js);
	    ?>
      </label>
      </td>
      <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
    </tr>
</tbody>
<script type="text/javascript">
window.addEvent("domready", function (){
var tp = new TimePicker('starttime<?=$num?>_picker', 'starttime<?=$num?>', 'starttime<?=$num?>_toggler', {format24:true});
new TimePicker('endtime<?=$num?>_picker', 'endtime<?=$num?>', 'endtime<?=$num?>_toggler', {format24:true});
});
</script>