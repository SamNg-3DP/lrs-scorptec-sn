<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Edit daily shift</title>
<base href="<?=base_url ()?>" />
<script type="text/javascript" src="js/mootools.v1.11.js"></script>
<script type="text/javascript" src="js/nogray_time_picker_min.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
jQuery.noConflict();

window.addEvent("domready", function (){
var tp = new TimePicker('starttime0_picker', 'starttime0', 'starttime0_toggler', {format24:true});
new TimePicker('endtime0_picker', 'endtime0', 'endtime0_toggler', {format24:true});
<? for($num=1; $num<count($slots); $num++){ ?>
var tp = new TimePicker('starttime<?=$num?>_picker', 'starttime<?=$num?>', 'starttime<?=$num?>_toggler', {format24:true});
new TimePicker('endtime<?=$num?>_picker', 'endtime<?=$num?>', 'endtime<?=$num?>_toggler', {format24:true});
<?}?>
});
var num = <?=count($slots)?>;
if(num > 1){
	num = num - 1;
}

function add_slot(){
	num++;
	jQuery.ajax({
		async: false,
		type: 'POST',
		url: "<?=site_url("dailyshift/get_new_slot_ajax")?>",
		data: {
		  	num: num,
		},
		success: function(data){
			var i = num-1;
			while(i >= 0){
				var obj = jQuery("#slot"+i);
				if(obj.length > 0){
					obj.after(data.content);
					break;
				}
				i--;
			}
	  	},
		dataType: 'json'
	});
}

function remove_slot(num){
	jQuery("#slot"+num).remove();
}

function check(){
	var shiftcode = jQuery.trim(jQuery("#shiftcode").val());
	if(!shiftcode){
		alert("Please enter shift code");
		return false;
	}

	var description = jQuery.trim(jQuery("#description").val());
	if(!description){
		alert("Please enter description");
		return false;
	}
	var i = 0;
	var bslot = false;
	while(i <= num){
		var obj = jQuery("#slot"+i);
		if(obj.length > 0){
			var starttime = jQuery.trim(jQuery("#starttime"+i).val());
			var endtime = jQuery.trim(jQuery("#endtime"+i).val());
			var shiftcategory = jQuery("#shiftcategory"+i).val();
			if(starttime == ""){
				alert("Please select start time");
				bslot = true;
				break;
			}
			if(endtime == ""){
				alert("Please select end time");
				bslot = true;
				break;
			}
			if(endtime == "00:00"){
				alert("Please select end time again");
				bslot = true;
				break;
			}
			if(starttime == endtime){
				alert("start time should not equal to end time");
				bslot = true;
				break;
			}
			if(starttime > endtime){
				alert("start time should not greater than end time");
				bslot = true;
				break;
			}
			if(shiftcategory == ""){
				alert("Please select shift category");
				bslot = true;
				break;
			}
		}
		i++;
	}
	if(bslot){
		return false;
	}
	jQuery("#slot_num").val(num);
	return true;
}
</script>
<script type="text/javascript" src="js/piwik-lrs.js"></script>
</head>

<body>
<?php echo form_open ( 'dailyshift/do_edit', 'onsubmit="return check()"' ); ?>
  <input type="hidden" name="slot_num" id="slot_num" value="<?=count($slots)?>" />
  <input type="hidden" name="id" id="id" value="<?=$dailyshift->id?>" />
  <table width="100%" border="0" cellspacing="0" cellpadding="0" background="images/Footer.png">
    <tr>
      <td><table width="100%" border="0" cellspacing="1" cellpadding="0">
        <tr>
          <td height="20" colspan="3" bgcolor="#EBEBEB">&nbsp;</td>
        </tr>
        <tr>
          <td width="20%" height="20" bgcolor="#FFFFFF">Shift Code:</td>
          <td height="20" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="30%"><input name="shiftcode" type="text" id="shiftcode" style="font-size:11px" size="20" value="<?=$dailyshift->shiftcode?>" readonly="readonly"/>
                <span style="color: #F00">＊</span></td>
                <td><?php echo form_error ( 'shiftcode' );?></td>
              </tr>
          </table></td>
          <td height="20" bgcolor="#FFFFFF"></td>
        </tr>
        <tr>
          <td height="20" bgcolor="#FFFFFF">Description:</td>
          <td height="20" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="65%"><input name="description" type="text" id="description" style="font-size:11px" size="70" value="<?=$dailyshift->description?>"/>
                <span style="color: #F00">＊</span></td>
                <td><?php echo form_error ( 'description' );?></td>
              </tr>
          </table></td>
          <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
        <tr>
          <td height="20" bgcolor="#FFFFFF">Notes:</td>
          <td height="20" bgcolor="#FFFFFF"><label>
            <textarea name="notes" id="notes" cols="45" rows="5" style="font-size:11px; font-family:Tahoma, Geneva, sans-serif"><?=$dailyshift->notes?></textarea>
          </label></td>
          <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
        <tr>
          <td height="20" bgcolor="#FFFFFF">Site:</td>
          <td height="20" bgcolor="#FFFFFF"><label>
            <?php
		        $site = "site";
		        $js = 'id="'.$site.'" style="font-size: 11px; width: 120px;"';
				echo form_dropdown($site, $sites, $dailyshift->site_id, $js);
		        ?>
          </label></td>
          <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>

	    <tr>
	      <td height="20" bgcolor="#FFFFFF">Lunch Hour:</td>
	      <td height="20" bgcolor="#FFFFFF">
		      <input type="text" size="2" name="lunch" id="lunch" value="<?=$dailyshift->lunch_hour?>"/>
	      </td>
	      <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
	    </tr>

        <tr><td colspan="2">
        	&nbsp;
        </td></tr>
        <tr><td colspan="2">
        		<?
        		$num = 0;
        		if(count($slots) > 0){
        		foreach($slots as $slot){
        		?>
		        <tbody id="slot<?=$num?>">
					<tr bgcolor="#EBEBEB">
						<td colspan="3">Time Slot&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<?
						if($num==0){
						?>
						<a href='javascript:void(0)' title="Add new time slot" style="text-decoration:none; " onclick='add_slot(<?=$num?>);'>+</a>
						<?
						}else{
						?>
						<a href='javascript:void(0)' title="Remove time slot" style="text-decoration:none; " onclick='remove_slot(<?=$num?>);'>-</a>
						<?
						}
						?>
					</tr>
				    <tr>
				      <td height="20" bgcolor="#FFFFFF">Start Time:</td>
				      <td height="20" bgcolor="#FFFFFF">
				      <input type="text" name="starttime<?=$num?>" id="starttime<?=$num?>" value="<?=$slot->start_time?>"/> <a href="#" id="starttime<?=$num?>_toggler">Open time picker</a>
						<div id="starttime<?=$num?>_picker" class="time<?=$num?>_picker_div"/>
				      </td>
				      <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
				    </tr>
				    <tr>
				      <td height="20" bgcolor="#FFFFFF">End Time:</td>
				      <td height="20" bgcolor="#FFFFFF">
				      <input type="text" name="endtime<?=$num?>" id="endtime<?=$num?>" value="<?=$slot->end_time?>"/> <a href="#" id="endtime<?=$num?>_toggler">Open time picker</a>
						<div id="endtime<?=$num?>_picker" class="time_picker_div"/>
				      </td>
				      <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
				    </tr>
				    <tr>
				      <td height="20" bgcolor="#FFFFFF">Shift Category:</td>
				      <td height="20" bgcolor="#FFFFFF">
				      <label>
				        <?
				        $id = "shiftcategory". $num;
				        $js = 'id="'.$id.'" style="font-size:11px" ';
				        echo form_dropdown($id, $shiftcategory, set_value($id, $slot->shift_category_code), $js);
				        ?>
				      </label>
				      </td>
				      <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
				    </tr>
				    <tr>
				      <td height="20" bgcolor="#FFFFFF">
				      Rate:
				      </td>
				      <td height="20" bgcolor="#FFFFFF">
				      <label>
				      <?
				        $rate_id = "rate".$num;
					    $js = 'id="<?=$rate_id?>" style="font-size:11px" ';
					    echo form_dropdown($rate_id, $rates, set_value($rate_id, $slot->rate), $js);
					    ?>
				      </label>
				      </td>
				      <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
				    </tr>
				</tbody>
	        	<?
	        	$num++;
	        	}
        		}else{
        			?>
        		<tbody id="slot<?=$num?>">
					<tr bgcolor="#EBEBEB">
						<td colspan="3">Time Slot&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<?
						if($num==0){
						?>
						<a href='javascript:void(0)' title="Add new time slot" style="text-decoration:none; " onclick='add_slot(<?=$num?>);'>+</a>
						<?
						}else{
						?>
						<a href='javascript:void(0)' title="Add new time slot" style="text-decoration:none; " onclick='remove_slot(<?=$num?>);'>-</a>
						<?
						}
						?>
					</tr>
				    <tr>
				      <td height="20" bgcolor="#FFFFFF">Start Time:</td>
				      <td height="20" bgcolor="#FFFFFF">
				      <input type="text" name="starttime<?=$num?>" id="starttime<?=$num?>" value=""/> <a href="#" id="starttime<?=$num?>_toggler">Open time picker</a>
						<div id="starttime<?=$num?>_picker" class="time<?=$num?>_picker_div"/>
				      </td>
				      <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
				    </tr>
				    <tr>
				      <td height="20" bgcolor="#FFFFFF">End Time:</td>
				      <td height="20" bgcolor="#FFFFFF">
				      <input type="text" name="endtime<?=$num?>" id="endtime<?=$num?>" value=""/> <a href="#" id="endtime<?=$num?>_toggler">Open time picker</a>
						<div id="endtime<?=$num?>_picker" class="time_picker_div"/>
				      </td>
				      <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
				    </tr>
				    <tr>
				      <td height="20" bgcolor="#FFFFFF">Shift Category:</td>
				      <td height="20" bgcolor="#FFFFFF">
				      <label>
				        <?
				        $id = "shiftcategory". $num;
				        $js = 'id="'.$id.'" style="font-size:11px" ';
				        echo form_dropdown($id, $shiftcategory, set_value($id), $js);
				        ?>
				      </label>
				      </td>
				      <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
				    </tr>
				    <tr>
				      <td height="20" bgcolor="#FFFFFF">
				      Rate:
				      </td>
				      <td height="20" bgcolor="#FFFFFF">
				      <label>
				      <?
				        $rate_id = "rate".$num;
					    $js = 'id="<?=$rate_id?>" style="font-size:11px" ';
					    echo form_dropdown($rate_id, $rates, set_value($rate_id), $js);
					    ?>
				      </label>
				      </td>
				      <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
				    </tr>
				</tbody>
        			<?
        		}
	        	?>
        	</td>
        </tr>
        <tr>
          <td height="20" colspan="3" align="center" bgcolor="#EBEBEB"><label>
            <input type="submit" name="button" id="button" value="     Save     " style="font-size:11px"/>
          </label></td>
        </tr>
      </table></td>
    </tr>
  </table>
</form>
</body>
</html>
