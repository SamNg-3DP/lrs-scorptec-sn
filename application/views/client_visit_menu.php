<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US" xml:lang="en">
<head>
	<title>LRS Left Menu</title>
	<base href="<?=base_url()?>"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
</head>
<body>
<div class="Block">
	<div class="Block-tl"></div>
    <div class="Block-tr"></div>
    <div class="Block-bl"></div>
    <div class="Block-br"></div>
    <div class="Block-tc"></div>
    <div class="Block-bc"></div>
    <div class="Block-cl"></div>
    <div class="Block-cr"></div>
    <div class="Block-cc"></div>
	<div class="Block-body">
		<div class="BlockHeader">
			<div class="l"></div>
        	<div class="r"></div>
        	<div class="t">On Site Service</div>
      	</div>
	    <p><a href="<?=site_url('client_visit/client_visit_list/new')?>" target="main-frame">New (<?php echo $new_count?>)</a></p>
	    <p><a href="<?=site_url('client_visit/client_visit_list/upcoming')?>" target="main-frame">Upcoming (<?php echo $upcoming_count?>)</a></p>
	    <?php if ($this->session->userdata('department') == 'Website' || $this->session->userdata('department') == 'Accounts'){?>	
	    	<p><a href="<?=site_url('client_visit/client_visit_list/reimburse')?>" target="main-frame">Reimburse (<?php echo $reimburse_count?>)</a></p>
	    <?php }?>	
	    <p><a href="<?=site_url('client_visit/client_visit_list/completed')?>" target="main-frame">Completed (<?php echo $completed_count?>)</a></p>
	    <p><a href="<?=site_url('calendar/department/0/0/7')?>" target="main-frame">Calendar</a></p>

	</div>
  </div>
</body>
</html>
