<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<base href="<?=base_url()?>"/>
<style>
#input, select
{
	font-family: Tahoma, Helvetica, Sans-Serif;
	font-size: 11px;
	font-style: normal;
	font-weight: normal;
}
</style>
<script type="text/javascript" src="js/piwik-lrs.js"></script>
</head>

<body >
<?php echo form_open ( 'department/do_edit', "name='form1'" ); ?>
  <table width="100%" height="22" border="0" align="center" cellpadding="0" cellspacing="0" background="images/Footer.png">
    <tr>
      <td>
      <? foreach ($department as $row): ?>
      <table width="100%" height="40" border="0" cellspacing="1" cellpadding="1">
        <tr bgcolor="#EBEBEB">
            <td height="20" colspan="3" align="left" bgcolor="#EBEBEB" style="color: red">&nbsp;<span style="color: red">
            <?=$status?>
            </span></td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td width="10%" height="20" align="right">Site:</td>
          <td width="18%" height="20" >
          <?php
			$js = 'id="site_id" style="font-size: 11px; width: 120px;"';
			echo form_dropdown('site_id', $sites, set_value ( 'site_id', $row->site_id ), $js);
		  ?><span style="color: #F00; font-weight: bold;">*</span>
          </td>
          <td><?php echo form_error('site_id'); ?></td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td align="right">Department Name:</td>
          <td height="20">
          	<label>
                <input name="department_name" type="text" id="department_name" size="30" style="font-size: 11px" value="<?=set_value ( 'department_name', $row->department)?>"/>
          	</label>
            <span style="color: #F00; font-weight: bold;">*</span></td>
            <td><?php echo form_error('department_name'); ?></td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td align="right">Status:</td>
          <td height="20">
          <?php
			$js = 'id="status" style="font-size: 11px; width: 120px;"';
			echo form_dropdown('status', status_list(), set_value ( 'status', $row->status ), $js);
		  ?><span style="color: #F00; font-weight: bold;">*</span>
          </td>
          <td><?php echo form_error('status'); ?></td>
        </tr>
      </table>
          <input type="hidden" name="department_id" id="department_id" value="<?=$row->id?>" />
      <? endforeach; ?>
      </td>
    </tr>
  </table>
    <p align="center">
      <input style="font-size: 11px" type="submit" name="submit" id="submit" value="     Save     " />
  </p>
</form>
</body>
</html>
