<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Exception Report</title>
<base href="<?=base_url()?>"/>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.tools.tabs.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />
<style type="text/css">
<!--
#form1 table tr td table {
	text-align: left;
}
#form1 p {
	text-align: left;
}
#form1 table tr td table tr td {
    font-family: Tahoma, Geneva, sans-serif;
	font-size: 11px;
}
/* tab pane styling */
div.panes div {
	display:none;		
	padding:15px 10px;
	border:1px solid #999;
	border-top:0;
	font-size:14px;
	background-color:#fff;
}

div.pane {
	display:none;		
	padding:15px 10px;
	border:1px solid #999;
	border-top:0;
	font-size:14px;
	background-color:#fff;
}
-->
</style>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
</head>
<body>

<h3>Exception Report</h3>

<form id=<?php echo $form_id ?> method='post'>
<div style="margin-top: 30px;">
	<div style=" margin: 10px;">
	Time period: 
	<?php
		$time_period_id = $form_id.'_time_period_id';
		$js = 'id="'.$time_period_id.'" style="font-size: 11px; width: 120px;" onchange="submit();"'; 
		echo form_dropdown($time_period_id, time_period(), $selected_time_period, $js);
	?>
	&nbsp;&nbsp;User:
	<?php $js = 'id="user_id" style="font-size:11px" onchange="submit();"';
		echo form_dropdown('user_id', $users, $selected_user, $js); 
	?>
	
	</div>
	<hr/>
	<div id="user_stats"></div>
</div>
</form>


<script type="text/javascript">

$("document").ready(function($){
	var start_date = <?php echo $start_date; ?>;
	var end_date = <?php echo $end_date; ?>;
	var user_id = <?php echo $selected_user ? $selected_user : 0 ; ?>;
	var url = "<?=site_url('daily_task/exception_report')?>/" + start_date + "/" + end_date + "/" + user_id;
	$("#user_stats").load(url, function() {
		//show_view();
    });

});




</script>

</body>
</html>