<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>LRS admin</title>
<base href="<?=base_url()?>"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/general.css" rel="stylesheet" type="text/css" />
<style type="text/css">
#header-div {
  background: #278296;
  border-bottom: 1px solid #FFF;
  height:85px;
  background-image: url("images/Footer.png");
}

#logo-div {
  height: 50px;
  float: left;
}

#submenu-div {
  height: 50px;
}

#submenu-div ul {
  margin: 0;
  padding: 0;
  list-style-type: none;
}

#submenu-div li {
  float: right;
  padding: 0 10px;
  margin: 3px 0;
  border-left: 1px solid #FFF;
}

#submenu-div a:visited, #submenu-div a:link {
  color: #FFF;
  text-decoration: none;
}

#submenu-div a:hover {
  color: #F5C29A;
}

#loading-div {
  clear: right;
  text-align: right;
  display: block;
}

#menu-div {
  background: #80BDCB;
  font-weight: bold;
  height: 24px;
  line-height:24px;
}

#menu-div ul {
  margin: 0;
  padding: 0;
  list-style-type: none;
}

#menu-div li {
  float: right;
  border-right: 1px solid #192E32;
  border-left:1px solid #BBDDE5;
}

#menu-div a:visited, #menu-div a:link {
  display:block;
  padding: 0 20px;
  text-decoration: none;
  color: #335B64;
  background:#9CCBD6;
}

#menu-div a:hover {
  color: #000;
  background:#80BDCB;
}

#submenu-div a.fix-submenu{ clear:both; margin-left:5px; padding:1px 5px; *padding:3px 5px 5px; background:#DDEEF2; color:#278296; }
#submenu-div a.fix-submenu:hover{ padding:1px 5px; *padding:3px 5px 5px; background:#FFF; color:#278296; }
#menu-div li.fix-spacel{ width:30px; border-left:none; }
#menu-div li.fix-spacer{ border-right:none; }
</style>
<script type="text/javascript" src="js/piwik-lrs.js"></script>
</head>
<body>
<div id="header-div">
  <div id="logo-div"><img src="images/Scorpy Logo.png" alt="Scorpy" /></div>
  <div id="submenu-div">
    <div style=" text-align:right"><a href="index.php/logout" target="_top" style="color:#0000FF">Logout</a></div>
  	<div style="font-family: 'Comic Sans MS', cursive; font-size: 24px;padding-left:450px;">Leave Request System</div>
  	<div style="font-family: Arial, Helvetica, sans-serif;padding-left:600px;">WWW.SCORPTEC.COM.AU</div>
  </div>
</div>
<div id="menu-div">
  <ul>
  	<li style="float:left; font-size:10px">&nbsp;&nbsp;&nbsp;&nbsp;Welcome&nbsp;<?php echo $this->session->userdata('first_name')." ".$this->session->userdata('last_name'); ?>, Logged in at <?php echo date(DATETIME_FORMAT,$this->session->userdata('last_login_time')); ?></li>
    <li class="fix-spacel">&nbsp;</li>
    <li><a href="index.php/userinfo" target="main-frame">MyAccount</a></li>
    <?php if($this->session->userdata('user_level') > 3) {?><li><a href="index.php/admin" target="_top">Admin</a></li><?php }?>
    <li><a href="index.php/admin/jobincident" target="_top">Job Incident</a></li>
    <li><a href="index.php/admin/leave" target="_top">Leave Request</a></li>
    <li><a href="index.php/admin/client_visit" target="_top">On Site Service</a></li>

  </ul>
  <br class="clear" />
</div>
</body>
</html>
