<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo ucfirst($action); ?> Client Visit</title>
<base href="<?=base_url()?>" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>

<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>

<style type="text/css">
  html { height: 100% }
  body { height: 100%; margin: 0; padding: 0 }
  #map_canvas { height: 100% }
</style>



<script type="text/javascript">
//<![CDATA[

function submit_click(){

	var client_name = $("#client_name").val();
	if($.trim(client_name) == ""){
		alert("Please enter client name involved");
		return false;
	}
/*
	var order_no = $("#ordno").val();
	if($.trim(order_no) == ""){
		alert("Please enter order_no");
		return false;
	}
*/
	var address = $("#street").val();
	if($.trim(address) == ""){
		alert("Please enter client street address");
		return false;
	}

	var address = $("#suburb").val();
	if($.trim(address) == ""){
		alert("Please enter client suburb");
		return false;
	}

	var visit_date = $("#visit_date").val();
	if($.trim(visit_date) == ""){
		alert("Please enter visiting date");
		return false;
	}

	var reason = $("#reason").val();
	if($.trim(reason) == ""){
		alert("Please enter reason");
		return false;
	}



//	var theForm = document.getElementById("add_client_visit");

	document.forms['add_client_visit'].submit();
}

function save_click(){
	$("#total_km").removeAttr("disabled");

	document.forms['add_client_visit'].submit();
}

function confirm_delete(value){
	if(confirm("Do you confirm deleting this client visit?") ){
		jQuery("#action").val(value);
		document.forms['add_client_visit'].submit();
	}
}

jQuery(function() {
    $("#visit_date").datepicker({
    	disabled: true,
    	dateFormat: '<?=JS_DATE_FORMAT?>',
    	changeMonth: true,
		changeYear: true
	});

    $("#approved_date").datepicker({
    	disabled: true,
    	dateFormat: '<?=JS_DATE_FORMAT?>',
    	changeMonth: true,
		changeYear: true
	});
});

function update_site(){
	var site_id = document.getElementById('site').value;
	var user_select = document.getElementById('user_id');
	var store_address = document.getElementById('store_address');
	user_select.options.length=0;

	jQuery.ajax({
		async: false,
		type: 'POST',
		url: "<?=site_url("client_visit/user_list_ajax")?>",
		data: {
		  	site_id: site_id,
		},
		success: function(data){
			$.each(data, function(name, i){
				user_select.options.add(new Option(i,name));
			});


	  	},
		dataType: 'json'
	});

	jQuery.ajax({
		async: false,
		type: 'POST',
		url: "<?=site_url("client_visit/store_address_ajax")?>",
		data: {
		  	site_id: site_id,
		},
		success: function(data){
				store_address.value=data;

	  	},
		dataType: 'json'
	});

	render_map();

}


// Google Map
var map;
var geocoder;
var bounds = new google.maps.LatLngBounds();
var markersArray = [];
var destinationIcon = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=D|FF0000|000000";
var originIcon = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=O|FFFF00|000000";

function render_map() {
    var latlng = new google.maps.LatLng(-37.9262, 145.1195);
    var origin = document.getElementById('store_address').value;

	var client_address = document.getElementById('client_address').value;

	var destination = false;
	if (client_address){
		destination = client_address;
	}else{
		var street = document.getElementById('street').value;
		var suburb = document.getElementById('suburb').value;
		if (street && suburb){
			destination = street+","+suburb+", VICTORIA, Australia";
		}
	}

    map = new google.maps.Map(document.getElementById("map_canvas"),
    		{
		        zoom: 12,
		        center: latlng,
		        mapTypeId: google.maps.MapTypeId.ROADMAP
		    });

    geocoder = new google.maps.Geocoder();

    if (destination)
		calculateDistances(origin,destination);
    else
    	addMarker(origin, false);
}


function calculateDistances(origin, destination) {
    var service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix(
      {
        origins: [origin],
        destinations: [destination],
        travelMode: google.maps.TravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.METRIC,
        avoidHighways: false,
        avoidTolls: true
      }, function(response, status) {
    	    if (status == google.maps.DistanceMatrixStatus.OK) {
    	        var origins = response.originAddresses;
    	        var destinations = response.destinationAddresses;
    	        var outputDiv = document.getElementById('outputDiv');
    	        var total_km = document.getElementById('total_km');

    	        outputDiv.innerHTML = '';
    	        deleteOverlays();

    	        for (var i = 0; i < origins.length; i++) {
    	          var results = response.rows[i].elements;
    	          addMarker(origins[i], false);
    	          for (var j = 0; j < results.length; j++) {
    	            addMarker(destinations[j], true);
    	            total_km.value = results[j].distance.text.replace(" km","") * 2;
    	          }
    	        }
    	      }
    	});


    var directions = new google.maps.DirectionsService();
    directions.route(
    	{
			origin: origin,
			destination: destination,
	        travelMode: google.maps.TravelMode.DRIVING,
	        unitSystem: google.maps.UnitSystem.METRIC,
	        avoidHighways: false,
	        avoidTolls: true

        }, function(response, status){
      	  	if (status == google.maps.DirectionsStatus.OK) {
      			var renderer = new google.maps.DirectionsRenderer(
      				{
      					directions: response,
      					draggable: false,
      					suppressMarkers: true,
      					map: map
      				}
      			);
      	    }
        });

}

function addMarker(location, isDestination) {
  var icon;
  if (isDestination) {
    icon = destinationIcon;
  } else {
    icon = originIcon;
  }
  geocoder.geocode({'address': location}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      bounds.extend(results[0].geometry.location);
      //map.fitBounds(bounds);
      var marker = new google.maps.Marker({
        map: map,
        position: results[0].geometry.location,
        icon: icon
      });
      markersArray.push(marker);

    }
  });
}



function deleteOverlays() {
  if (markersArray) {
    for (i in markersArray) {
      markersArray[i].setMap(null);
    }
    markersArray.length = 0;
  }
}




//]]>
</script>
<script type="text/javascript" src="js/piwik-lrs.js"></script>
    </head>
    <body onload="render_map()">
    	<div>
        <?php  if ($user_level >= '4' && $action == 'view'){
        	echo form_open('client_visit/do_admin_save_form', array('id'=>'add_client_visit'));
        }else{
        	echo form_open('client_visit/do_save_form', array('id'=>'add_client_visit'));
        }?>
        <input type="hidden" name="status" value="<?php echo $status;?>" />
        <input type="hidden" name="ordid" id="ordid" value="<?php if ($order_no) echo $ordid; ?>" />
        <input type="hidden" name="action" id="action" value="<?php echo $action; ?>" />
        <input type="hidden" id="store_address" value="<?php echo $store_address; ?>" />
        <input type="hidden" id="client_address" value="<?php echo $client_address; ?>" />

        <input type="hidden" name="client_visit_id" id="client_visit_id" value="<?php if ($action != 'add') echo $client_visit_id; ?>" />
        <input type="hidden" name="request_id" id="request_id" value="<?php if ($action != 'add') echo $request_id; ?>" />
        <?php if (isset($user_id)){?>
        	<input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>" />
        <?php }?>

        <h4><?php echo ucfirst($action); ?> Client Visit</h4>
        <table width="100%" height="22" border="0" align="center" cellpadding="5" cellspacing="1" background="images/Footer.png">
        	<tr bgcolor="#FFFFFF">
        		<td width="120" height="20" align="left">Store:</td>
				<td >
					<?php
						$js = 'id="site" style="font-size:11px" onchange="update_site();"';
						if ($read_only) $js .= "disabled='disabled'";
						echo form_dropdown('site', $sites, $current_user->site_id, $js);
					?>
				</td>
        	</tr>
        	<tr bgcolor="#FFFFFF">
        		<td width="120" height="20" align="left">Tech Name:</td>
				<td >
					<?php
	                   	$js = 'id="user_id" style="font-size:11px"';
	                   	if ($read_only) $js .= "disabled='disabled'";
	                   	echo form_dropdown('user_id', $users_list, $assigned_user->id, $js);
					?>
				</td>
        	</tr>
        	<tr bgcolor="#FFFFFF">
        		<td width="120" height="20" align="left">Client QB Name:</td>
				<td >
					<?php if ($read_only){
						echo $client_qb_name;
					}else{?>
						<input type="text" name="client_qb_name" id="client_qb_name" size="80" value="<?php if ($client_qb_name) echo $client_qb_name;?>"  />
					    <?php echo form_error('client_qb_name');
					}?>
				</td>
        	</tr>
        	<tr bgcolor="#FFFFFF">
        		<td width="120" height="20" align="left">Client Name:</td>
				<td >
					<?php if ($read_only){
						echo $client_name;
					}else{?>
						<input type="text" name="client_name" id="client_name" size="80" value="<?php if ($client_name) echo $client_name;?>"  />
					    <?php echo form_error('client_name');
					}?>
				</td>
        	</tr>
        	<tr bgcolor="#FFFFFF">
        		<td width="120" height="20" align="left">Order Number:</td>
				<td >
					<?php if ($read_only && $order_no){?>
						<a href="<?=$order_url?>"><?=$order_no?></a>
					<?}else{?>
						<input type="text" name="ordno" id="ordno" size="10" value="<?php if ($order_no) echo $order_no;?>"  />
					    <?php echo form_error('ordno'); ?>&nbsp;
					    <?php echo $order_link_url;
					}?>
				</td>
        	</tr>
        	<tr bgcolor="#FFFFFF">
        		<td width="120" height="20" align="left">Address:</td>
				<td>
				<div style="float: left">
					<table cellpadding="5" cellspacing="1">
					<tr><td><label>Street:</label></td>
						<td width="180">
							<?php if ($read_only){
								echo $client_street;
							}else{?>
								<input type="text" name="street" id="street" size='40' onchange="render_map();" value="<?php if ($client_street) echo $client_street;?>" />
							<?php }?>
						</td>
					</tr>
					<tr><td><label>Suburb:</label></td>
						<td>
							<?php if ($read_only){
								echo $client_suburb;
							}else{?>
								<input type="text" name="suburb" id="suburb" size='40' onchange="render_map();" value="<?php if ($client_suburb) echo $client_suburb;?>" />
							<?php }?>
						</td>
					</tr>
					<tr><td><label>State:</label></td>
						<td>
							<?php if ($read_only){
								echo $client_state;
							}else{?>
								<input type="text" name="state" size='40' value="<?php if ($client_state) echo $client_state;?>" />
							<?php }?>
						</td>
					</tr>
					<tr><td><label>Postcode:</label></td>
						<td>
							<?php if ($read_only){
								echo $client_postcode;
							}else{?>
								<input type="text" name="postcode" size='8' value="<?php if ($client_postcode) echo $client_postcode;?>" />
							<?php } ?>
						</td></tr>
					</table>
				</div>
					&nbsp;&nbsp;
				<div id="map_canvas" style="width: 600px; height: 400px"></div>
				<!-- <div id="map">
					<?php if ($order_no){ ?>
					    <img border="0" id="g_map" alt="Client Location"
					    src="http://maps.google.com/maps/api/staticmap?size=300x200&maptype=roadmap<?php echo $g_map;?>
					    &sensor=false">
				    <?php } ?>
				</div>	-->

				<div id="outputDiv"></div>
				</td>
        	</tr>
			<tr bgcolor="#FFFFFF">
				<td width="120" height="20" align="left">Total Kilometres:</td>
                <td >
                	<input type="text" name="total_km" id="total_km" value="<?php if ($total_km) echo $total_km; ?>" <?php if ($read_only) echo "disabled='disabled'";?> style="font-size: 11px" />

   	        		<?php echo form_error('total_km'); ?>
                </td>
			</tr>
        	<tr bgcolor="#FFFFFF">
        		<td width="120" height="20" align="left">Telephone:</td>
				<td >
					<?php if ($read_only){
						echo $phone;
					}else{?>
						<input type="text" name="phone" id="phone" value="<?php if ($phone)echo $phone;?>" class="divinput" />
				    	<?php echo form_error('phone');
					}?>
				</td>
        	</tr>
        	<tr bgcolor="#FFFFFF">
        		<td width="120" height="20" align="left">Visiting Date:</td>
                <td >
					<?php if ($read_only){
						echo $visit_date;
					}else{?>
	                	<input type="text" name="visit_date" id="visit_date" size="8" value="<?php if ($visit_date) echo $visit_date;?>" style="font-size: 11px" />
    	        		<?php echo form_error('visit_date');
					}?>
                </td>
        	</tr>
        	<tr bgcolor="#FFFFFF">
        		<td width="120" height="20" align="left">Visiting Time:</td>
				<td >
					From:
					<?php if ($read_only){
						echo $from_time;
					}else{?>
						<input type="text" name="from_time" id="from_time" size="4" value="<?php if ($from_time) echo $from_time;?>"  />&nbsp;
					<?php }?>
					To:
					<?php if ($read_only){
						echo $to_time;
					}else{?>
						<input type="text" name="to_time" id="to_time" size="4" value="<?php if ($to_time) echo $to_time;?>"  />&nbsp;<span>* e.g. 16:35</span>
					<?php }?>
				</td>
        	</tr>
        	<tr bgcolor="#FFFFFF">
        		<td width="120" height="20" align="left">Reason:</td>
                <td >
					<?php if ($read_only){
						echo $reason;
					}else{?>
	                	<input type="text" name="reason" id="reason" size="50" value="<?php if ($reason) echo $reason;?>" style="font-size: 11px" />
	            		<?php echo form_error('reason');
					}?>
                </td>
        	</tr>
		</table>
		<br/>



	<? if(!empty($clock)){ ?>
		<h4>Actatek Clock In/Out Log</h4>
            <table  width="60%" border="0" cellpadding="0" cellspacing="0" background="images/Footer.png">
            <tr><td>
	        	<table  width="100%" border="0" cellspacing="1" cellpadding="5">
	            	<tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;" >
	                	<td width="30">&nbsp;</td>
                        <td width="120">Name</td>
						<td width="45">Direction</td>
						<td colspan="10">Time</td>
						<td width="100">Total Working Hours</td>
					</tr>



				<? 	$num = 0;
				foreach ($clock as $date => $user): $num++; ?>
					<tr bgcolor="#DDDDDD">
						<td colspan="14"><strong><?php echo date("D d F Y", $date);?></strong></td>
						</tr>

						<? foreach ($user as $id => $u){?>
						<tr bgcolor="#FFFFFF">
						<?	if ($u['last_dir']=='IN')
									echo '<td bgcolor="#44FF05" align="center" rowspan="2"><strong>IN</strong></td>';
								else if ($u['last_dir']=='OUT')
									echo '<td bgcolor="#FF4242" align="center" rowspan="2"><strong>OUT</strong></td>';
								else
									echo '<td align="center" rowspan="2">&nbsp;</td>';
                            ?>

							<td><?=$id?></td>
                            <td><strong>IN</strong></td>
                            <? 	$i = 1;
								$a_in = count($u['IN']);
								$colspan = 10;
								foreach($u['IN'] as $in){
									if ($i == $a_in || $a_in == 0)
										$colspan = 10 - $i;
									else
										$colspan = 1;
                            	?>
								<td width='40'><?= $in; ?></td>
                            	<? 		$i++;
                            	}?>
							<td  colspan="<?=$colspan ?>">&nbsp;</td>
							<td><?=$u['total']?></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td>&nbsp;</td>

								<td><strong>OUT</strong></td>
                            	<? 	$o = 1;
                            		$a_out = count($u['OUT']);
                            		$colspan = 10;
	                            	foreach($u['OUT'] as $out){
                            			if ($o == $a_out || $a_out == 0)
                            				$colspan = 10 - $o;
                            			else
                            				$colspan = 1;
                            	?>
                            		<td width='40'><?= $out; ?></td>
                            	<? 		$o++;
                            	}?>
                            	<td  colspan="<?=$colspan ?>">&nbsp;</td>
									<td>&nbsp;</td>
	                            </tr>
                            <? }
						endforeach;
                            ?>
				</table>
			</td></tr>
			</table>
		<?}?>




        <?php if ($user_level >= '4' && $action != 'add'){?>
		<h4>Administrator</h4>
		<table width="100%" height="22" border="0" align="center" cellpadding="5" cellspacing="1" background="images/Footer.png">
			<tr bgcolor="#FFFFFF">
				<td width="120" height="20" align="left">Actual Visit Time:</td>
				<td >
					From: <input type="text" name="actual_from_time" id="actual_from_time" value="<?php echo $actual_from_time;?>" class="divinput" />&nbsp;
					To: <input type="text" name="actual_to_time" id="actual_to_time" value="<?php echo $actual_to_time;?>" class="divinput" />
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td width="120" height="20" align="left">Total Kilometres:</td>
                <td >
                	<input type="text" name="actual_km" id="actual_km" value="<?php echo $actual_km;?>" style="font-size: 11px" />
            		<?php echo form_error('total_km'); ?>
                </td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td width="120" height="20" align="left">Reimbursed:</td>
                <td ><input type="checkbox" name="reimbursed" value="1" <?php if ($reimbursed == 1 ){?>checked="checked"<?php ;}?> /></td>
			</tr>
		</table>
		<br/>
		<?php }else{?>
		<h4>Administrator</h4>
		<table width="100%" height="22" border="0" align="center" cellpadding="5" cellspacing="1" background="images/Footer.png">
			<tr bgcolor="#FFFFFF">
				<td width="120" height="20" align="left">Actual Visit Time:</td>
				<td >
					From: <?php echo $actual_from_time;?>&nbsp;
					To: <?php echo $actual_to_time;?>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td width="120" height="20" align="left">Total Kilometres:</td>
                <td >  	<?php echo $actual_km;?>      </td>
			</tr>
		</table>
		<?php }?>

		<table width="100%" height="22" border="0" align="center" cellpadding="5" cellspacing="1" background="images/Footer.png">
			<tr bgcolor="#EBEBEB">
			<?php if ($allow_edit_delete){?>
				<td colspan="2" align="center">
					<input type="button" name="apply" value="Save" onclick="save_click()" />
					<input type="button" onClick="confirm_delete('delete');"  name="delete" value="Delete"  />
				</td>
			<?php }elseif($action == 'add'){?>
				<td colspan="2" align="center"><input type="button" name="apply" value="Save" onclick="submit_click()" /></td>
			<?php }?>
			</tr>
		</table>

        <?php echo form_close();?>






        </div>
    </body>
</html>
