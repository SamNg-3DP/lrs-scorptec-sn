<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Edit site</title>
<base href="<?=base_url()?>" />
<script type="text/javascript">
	function check(){
		var iprange = $.trim($("#ip_range").val());
		if(iprange){
			var iplen = iprange.split(".");
			if(iplen.length != 4 || iprange.substring(iprange.length -1 ) != "."){
				alert("IP format is invalid.");
				return false;
			}
		}
		return true;
	}
</script>
<script type="text/javascript" src="js/piwik-lrs.js"></script>
</head>

<body >
<?php echo form_open ( 'site/do_edit', 'onsubmit="return check()"' ); ?>
<input type="hidden" name="site_id" id="site_id" value="<?=$site->id?>" />
<table width="100%" border="0" cellspacing="0" cellpadding="0" background="images/Footer.png">
	<tr>
		<td>
		<table width="100%" border="0" cellspacing="1" cellpadding="0">
			<tr>
				<td height="20" colspan="2" bgcolor="#EBEBEB">&nbsp;<font color="red"><?=$status?></font></td>
			</tr>
			<tr>
				<td width="10%" height="20" align="right" bgcolor="#FFFFFF">Site Name:</td>
				<td height="20" bgcolor="#FFFFFF">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="35%"><input name="name" type="text" id="name" style="font-size: 11px" size="30"
							value="<?=$site->name?>"/></td>
						<td width="65%"><?php echo form_error('name'); ?></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td width="10%" height="20" align="right" bgcolor="#FFFFFF">IP range:</td>
				<td height="20" bgcolor="#FFFFFF">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="35%"><input name="ip_range" type="text" id="ip_range" style="font-size: 11px" size="30"
							value="<?=$site->ip_range?>"/>&nbsp;e.g 192.168.1.</td>
						<td width="65%"><?php echo form_error('ip_range'); ?></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td width="10%" height="20" align="right" bgcolor="#FFFFFF">Status:</td>
				<td height="20" bgcolor="#FFFFFF">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="35%">
						<?php
	                    $js = 'id="status" style="font-size: 11px; width: 120px;"';
						echo form_dropdown('status', status_list(), $site->status, $js);
						?>
						</td>
						<td width="65%"><?php echo form_error('status'); ?></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td height="20" colspan="3" align="center" bgcolor="#EBEBEB"><label> <input type="submit"
					name="button" id="button" value="     Save     " style="font-size: 11px" /> </label></td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</form>
</body>
</html>
