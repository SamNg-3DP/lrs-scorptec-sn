<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Leave Hour Parameter</title>
<base href="<?=base_url()?>"/>
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
<style>
#input, select
{
	font-family: Tahoma, Helvetica, Sans-Serif;
	font-size: 11px;
	font-style: normal;
	font-weight: normal;
}
</style>
<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript">
//<![CDATA[

$(function() {
	$("#Initial_Payroll_Date_Parameter").datepicker({
		disabled: true,
		dateFormat: '<?=JS_DATE_FORMAT?>',
		changeMonth: true,
		changeYear: true
	});
});
//]]>
</script>
<script type="text/javascript" src="js/piwik-lrs.js"></script>
</head>

<body>
<form id="form1" name="form1" method="post" action="<?=site_url('config/update_payroll')?>">
  <table width="100%" height="22" border="0" align="center" cellpadding="0" cellspacing="0" background="images/Footer.png">
    <tr>
      <td>
      <?php
      echo validation_errors();
      echo $this->session->flashdata('message');
      $value = "";
      ?>
        <table width="100%" height="85" border="0" cellspacing="1" cellpadding="1">
        <tr bgcolor="#EBEBEB">
            <td width="50%" height="20" colspan="2" align="right" bgcolor="#EBEBEB" style="color: red">&nbsp;</td>
        </tr>
        <? foreach ($configs as $row): ?>
        <tr bgcolor="#FFFFFF">
          <td width="80" height="20" align="left"><?=$row->name?>:</td>
          <td width="300" height="20" align="left">
          	<label>
          		<?
          		$value = $row->value;
          		if($row->code == "Initial_Payroll_Date_Parameter" && $value > 0){
          			$value = date(DATE_FORMAT, $value);
          		}
          		if(!empty($value)){
          			echo $value;
          		}else{
          		?>
                <input name="<?=$row->code?>" type="text" id="<?=$row->code?>" size="30" style="font-size: 11px" value="<?php echo set_value($row->code, $value);?>" />
                <?
          		}
                ?>
          	</label>
          </td>
        </tr>
        <? endforeach;
        if(!empty($value)){
        }else{
        ?>
        <tr bgcolor="#EBEBEB">
	        <td>&nbsp;</td>
			<td>
				<input type="submit" name="button" id="button" value="     Save     " style="font-size:11px;"/>
			</td>
		</tr>
		<?}?>
      </table>
      </td>
    </tr>
  </table>
</form>
</body>
</html>
