<?php
 
 class Payroll extends CI_Controller {
 	function Payroll() {
		parent::__construct();
		if(!$this->userauth->logined()){
			$infoMsg = array(
				'info' => 'You have no privilege into this page !'
			);
			$this->session->set_flashdata('auth', $this->load->view('msgbox/info', $infoMsg , True));
			redirect("login");
		}else{
			if(!$this->userauth->judge_priv("payroll")){
				$errorMsg = array(
					'error' => 'You have no privilege into this page !'
				);
				$this->session->set_flashdata('auth', $this->load->view('msgbox/error', $errorMsg , True));
				redirect("msgbox", 'location');
			}
		}
		
		$this->form_validation->set_error_delimiters ( "<div style='font:Tahoma, Geneva, sans-serif; font-size:11px; color:#F00'>", "</div>" );
		//load St_payroll_m model
		$this->load->model('St_payroll_m', 'payroll_m');
		$this->load->model('St_payroll_data_m', 'payroll_data_m');
		$this->load->model('St_payroll_data_log_m', 'payroll_data_log_m');
		$this->load->model('St_request_m', 'request_m');
		$this->load->model('St_working_hours_m', 'working_hours_m');
		$this->load->model('St_user_labor_m', 'user_labor_m');
		$this->load->library('workinghour');
	}
	
	function index() {
		$this->add();
	}
	
	/**
	 * generate the payroll list based on the config setting
	 */
	function add() {
		$adjustment = $this->payroll_m->get_adjustment();
		if(!empty($adjustment)){
			$this->adjustment();
			return;
		}
		
		$now_time = now();  
		$data ['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('payroll')."'>Payroll</a> > Payroll List";
		$this->load->view ( 'navigation', $data );
		$this->load->model('St_config_m', 'config_m');
		$days_payroll = $this->config_m->get_by_code("Days_Payroll_Parameter")->value;
		$initial_payroll_date = $this->config_m->get_by_code("Initial_Payroll_Date_Parameter")->value;
		
		if(empty($initial_payroll_date) || empty($days_payroll)){
			redirect('config/payroll');
			return;
		}
		
		//generate the payroll list based on the config setting
		
		$last = $this->payroll_m->get_last();
		if(!empty($last)){
			$initial_payroll_date = $last->end_date;
		}
		
		$end_time = date_add('d', $days_payroll, date(DATE_FORMAT, $initial_payroll_date));
		$now_time += 86400;  // Move payroll 1 day ahead to Monday. 
		while($end_time < $now_time){
			//add payroll
			$payroll_info = array(
				'start_date' => $initial_payroll_date,
				'end_date' => $end_time,
				'add_time' => $now_time,
				'operator_id' => $this->session->userdata('user_id'),
				'status' => ACTIVE
			);
			$this->payroll_m->insert($payroll_info);
			$initial_payroll_date = $end_time;
			$end_time = date_add('d', $days_payroll, date(DATE_FORMAT, $initial_payroll_date));
		}
		
		$data['payrolls'] = $this->payroll_m->get_unprocessed();
		
		$data['form_id'] = "payroll";
		//$total_count = $this->payroll_m->count_payrolls();
    	$processed_count = $this->payroll_m->count_processed();
    	$unprocessed_count = $this->payroll_m->count_unprocessed();
    	
    	$total_count = $unprocessed_count;
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	$data['total_count'] = $total_count;
    	$data['processed_count'] = $processed_count;
    	$data['unprocessed_count'] = $unprocessed_count;
		$this->load->view ( 'payroll/list_payrolls', $data);
	}
	
	/**
	 * get payrolls by ajax
	 */
	function get_payrolls_ajax(){
		log_message("debug", "get_payrolls_ajax....");
		$form_id = "payroll";
		$data['form_id'] = $form_id;
        
    	$page_no = $this->input->post('payroll_page_no');
        
		$limit = $this->input->post('payroll_limit');
		
		log_message("debug", "payroll_page_no...." . $page_no);
		log_message("debug", "payroll_limit...." . $limit);
    	$data['page_limit'] = $limit;
    	
		$data['payrolls'] = $this->payroll_m->get_unprocessed($page_no, $limit);
		$total_count = $this->payroll_m->count_unprocessed();
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
		
		log_message("debug", "payroll_encode....");
		$content = $this->load->view('payroll/list_payrolls_data', $data, true);
		log_message("debug", "payroll_encode2....");
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
	}
	
	/**
	 * add payroll data
	 */
	function do_add() {
		$this->form_validation->set_rules ( 'start_date', 'Start date', 'trim|required' );
		$this->form_validation->set_rules ( 'end_date', 'End date', 'trim|required' );
		log_message("debug", "Payroll do_add ... ");
		if ($this->form_validation->run () == FALSE) {
			$this->add();
		} else {
			$user_count = $this->input->post ( 'user_count' );
			$user_count = get_number($user_count);
			$this->load->model('St_payroll_m', 'payroll_m');
			$dnow = now();
			$start_date = $this->input->post ( 'start_date' );
			$start_date = strtotime($start_date);
			$end_date = $this->input->post ( 'end_date' );
			$end_date = strtotime($end_date);
			
			$payroll = array(
				'start_date' => $start_date,
				'end_date' => $end_date,
				'operator_id' => $this->session->userdata("user_id"),
				'add_time' => $dnow,
				'status' => ACTIVE
			);
			
			$payroll_id = $this->payroll_m->insert($payroll);
			
			$this->load->model('St_payroll_data_m', 'payroll_data_m');
			for($i = 0; $i < $user_count; $i++){
				$user_id = 'huser_id'.$i;
				$total_working_hours = 'htotal_working_hours'.$i;
				$accrued_annual_leave = 'haccrued_annual_leave'.$i;
				$accrued_personal_leave = 'haccrued_personal_leave'.$i;
				$annual_leave_used = 'hannual_leave_used'.$i;
				$personal_leave_used = 'hpersonal_leave_used'.$i;
				$annual_leave_end = 'hannual_leave_end'.$i;
				$personal_leave_end = 'hpersonal_leave_end'.$i;
				
				$payroll_data = array(
					'payroll_id' => $payroll_id,
					'user_id' => $this->input->post( $user_id ),
					'total_working_hours' => $this->input->post( $total_working_hours ),
					'accrued_annual_leave' => $this->input->post( $accrued_annual_leave ),
					'accrued_personal_leave' => $this->input->post( $accrued_personal_leave ),
					'annual_leave_used' => $this->input->post( $annual_leave_used ),
					'personal_leave_used' => $this->input->post( $personal_leave_used ),
					'annual_leave_end' => $this->input->post( $annual_leave_end ),
					'personal_leave_end' => $this->input->post( $personal_leave_end )
				);
				
				$this->payroll_data_m->insert($payroll_data);
			}
			$this->index();
		}
	}
	
	function edit($payroll_id) {
		$data['form_id'] = "payroll";
		$data['payroll_id'] = $payroll_id;
		$data ['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('payroll')."'>Payroll</a> > Edit Payroll Data";
		
		$payroll = $this->payroll_m->get($payroll_id);
		$data['payroll'] = $payroll;
		
//		$payrolldatas = $this->payroll_data_m->get_by_payroll($payroll_id);
		
		
		$now_time = now();
		//get config leave hour parameter
    	$this->load->model('St_config_m', 'config_m');
    	$annual_row = $this->config_m->get_by_code('Annual_Leave_Parameter');
    	$annual_Leave_Parameter = $annual_row->value;
    	$personal_row = $this->config_m->get_by_code('Personal_Leave_Parameter');
    	$personal_Leave_Parameter = $personal_row->value;
    	if(empty($annual_Leave_Parameter) || empty($personal_Leave_Parameter)){
    		redirect('config/leave_hours');
    		return;
    	}
		$start_date = $payroll->start_date;
		$end_date = $payroll->end_date;

		//read all users
		$this->load->model('St_users_m', 'users_m');
		$users = $this->users_m->get_by_payroll($end_date);
		foreach($users as $user){
			$payrollinfo = $this->_get_payroll_data($user, $start_date, $end_date, $annual_Leave_Parameter, $personal_Leave_Parameter);
			$payroll_data = array(
				'total_working_hours' 			=> $payrollinfo['total_working_hours'],
				'total_working_hours_accrued' 	=> $payrollinfo['total_working_hours_accrued'],
				'total_OT0_hours' 				=> $payrollinfo['total_OT0_hours'],
				'total_OT1_hours' 				=> $payrollinfo['total_OT1_hours'],
				'total_OT2_hours' 				=> $payrollinfo['total_OT2_hours'],
				'total_OT3_hours' 				=> $payrollinfo['total_OT3_hours'],
				'total_casual'					=> $payrollinfo['casual_working_hour'],
				'total_rest_break' 				=> $payrollinfo['total_rest_break'],
				'accrued_annual_leave' 			=> $payrollinfo['accrued_annual_leave'],
				'accrued_personal_leave' 		=> $payrollinfo['accrued_personal_leave'],
				'annual_leave_used' 			=> $payrollinfo['annual_leave_used'],
				'personal_leave_used' 			=> $payrollinfo['personal_leave_used'],
				'unpaid_leave_used' 			=> $payrollinfo['unpaid_leave_used'],
				'annual_leave_end' 				=> $payrollinfo['annual_leave_end'],
				'personal_leave_end' 			=> $payrollinfo['personal_leave_end'],
				'inlieu_used'					=> $payrollinfo['inlieu_used'],					
				'inlieu_end'		 			=> $payrollinfo['inlieu_end'],
				'salary' 						=> $payrollinfo['salary'],
				'processed' 					=> 0,
				'add_time' 						=> $now_time
			);
			

			$existing_payroll = $this->payroll_data_m->get_by_payroll_user($payroll_id, $user->id);
			if(empty($existing_payroll)){
				$payroll_data['payroll_id']= $payroll_id;
				$payroll_data['user_id']= $user->id;
				$payroll_data['user_name']= $user->user_name;
				
				$insert_id = $this->payroll_data_m->insert($payroll_data);
				$payroll_data_log = array(
					'id' => $insert_id,
					'operator_id' => $this->session->userdata('user_id'),
					'operation' => 'add',
					'operate_time' => $now_time,
					'operate_ip' => $this->input->ip_address()
				);
				$payroll_data_log = array_merge($payroll_data_log, $payroll_data);
				$this->payroll_data_log_m->insert($payroll_data_log);
			}
			elseif ($existing_payroll->processed == 0){
				$this->payroll_data_m->update($existing_payroll->id, $payroll_data);
			}
		}
		
		
		
		$payrolldatas = $this->payroll_data_m->get_process_by_payroll($payroll_id);
		
		$page_count = 0;
		$total_count = $this->payroll_data_m->count_process_by_payroll($payroll_id);
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
		
		$data['payrolldatas'] = $payrolldatas;
		
//		$this->output->enable_profiler(TRUE);
		$this->load->view ( 'navigation', $data );
		$this->load->view ( 'payroll/edit_payroll' );
	}
	
	/**
	 * get edit payrolls by ajax
	 */
	function get_edit_payrolls_ajax(){
		log_message("debug", "get_edit_payrolls_ajax....");
		$form_id = "payroll";
		$data['form_id'] = $form_id;
        $payroll_id = $this->input->post('payroll_id');
    	$page_no = $this->input->post('payroll_page_no');
		$limit = $this->input->post('payroll_limit');
		
		log_message("debug", "get_edit_payrolls_ajax payroll_page_no...." . $page_no);
		log_message("debug", "get_edit_payrolls_ajax payroll_limit...." . $limit);
    	$data['page_limit'] = $limit;
    	
		$data['payrolldatas'] = $this->payroll_data_m->get_process_by_payroll($payroll_id, $page_no, $limit);
		$total_count = $this->payroll_data_m->count_process_by_payroll($payroll_id);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
		
		log_message("debug", "payroll_encode....");
		$content = $this->load->view('payroll/edit_payrolls_data', $data, true);
		log_message("debug", "edit_payrolls_data payroll_encode2....");
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
	}
	function do_edit(){
		//TODO ...do_edit
	}
	
	function view($payroll_id){
		$data ['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('payroll')."'>Payroll</a> > View Payroll Data";
		
		$payroll = $this->payroll_m->get($payroll_id);
		$data['payroll'] = $payroll;
		
//		$payrolldatas = $this->payroll_data_m->get_by_payroll($payroll_id);
		$payrolldatas = $this->payroll_data_m->get_process_by_payroll($payroll_id);
		
		$data['payrolldatas'] = $payrolldatas;
		
		$this->load->view ( 'navigation', $data );
		$this->load->view ( 'payroll/view_payroll' );
	}
	
	/**
	 * get staff payroll data by ajax
	 */
	function get_staff_ajax(){
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');
		
		log_message("debug", "start_date === ".$start_date);
		log_message("debug", "end_date === ".$end_date);
		$start_date = strtotime($start_date);
		log_message("debug", "1start_date === ".$start_date);
		log_message("debug", "1end_date === ".strtotime($end_date));
		//include end date, so add 1 day for end date
		$end_date = date_add('d', 1 , $end_date);
		log_message("debug", "2end_date === ".$end_date);
		
		//get config leave hour parameter
    	$this->load->model('St_config_m', 'config_m');
    	$annual_row = $this->config_m->get_by_code('Annual_Leave_Parameter');
    	$annual_Leave_Parameter = $annual_row->value;
    	$personal_row = $this->config_m->get_by_code('Personal_Leave_Parameter');
    	$personal_Leave_Parameter = $personal_row->value;

		//read all users
		$this->load->model('St_users_m', 'users_m');
		$users = $this->users_m->get_by_payroll($start_date);
		$user_list = array();
		foreach($users as $user){
			$user_list[] = $this->_get_payroll_data($user, $start_date, $end_date, $annual_Leave_Parameter, $personal_Leave_Parameter);
		}
		$data['users'] = $user_list;
		$content = $this->load->view('payroll/add_payroll_data', $data, true);
		$result = array(
        	"content" 	=> $content
        );
        echo json_encode($result);
	}
	
	function _get_payroll_data($user, $start_date, $end_date, $annual_Leave_Parameter, $personal_Leave_Parameter, $do_update=false){
			$userinfo = array();
			$user_id = $user->id;
			$user_name = $user->user_name;
			
			$labor = $this->user_labor_m->get_by_end_time($user_id, $end_date);
			$hourly_rate = 0;
			if(!empty($labor)){
				$hourly_rate = $labor->hourly_rate;
			}
			
			//Total Working hours
//			$total_working_hours = $this->_get_total_working_hours($user_id, $start_date, $end_date-86400);
			$total_working_hours = $this->workinghour->calculate_hours_used( $user_id, $start_date, $end_date-86400, 1, "true", "false", "true" );
			//$total_working_hours = $this->_get_total_working_hours($user_id, $start_date, $end_date);

			
			//Annual Leave Used
			$annual_leave_used = $this->_get_annual_leave_used($user_id, $start_date, $end_date);
			$annual_leave_used = get_number($annual_leave_used);
			
			//Personal Leave Used
			$personal_leave_used = $this->_get_personal_leave_used($user_id, $start_date, $end_date);
			$personal_leave_used = get_number($personal_leave_used);
			
			$compasionate_leave_used = $this->_get_compasionate_leave_used($user_id, $start_date, $end_date);
			$compasionate_leave_used = get_number($compasionate_leave_used);

			$casual_working_hour = $this->_get_casual_working_hour($user_id, $start_date, $end_date);
			$casual_working_hour = get_number($compasionate_leave_used);
			
			$inlieu_used = $this->_get_inlieu_used($user_id, $start_date, $end_date);
			$inlieu_used = get_number($inlieu_used);
			
			//$total_working_hours = $total_working_hours - $annual_leave_used - $personal_leave_used;
			//$total_working_hours = $total_working_hours ;
			$increase_work_hours = $this->workinghour->calculate_increase_work_hours_by_leave($user_id, $start_date, $end_date);
			$total_working_hours += $increase_work_hours;
			
			$total_working_hours_accrued = $total_working_hours; 
			
			$deduct_work_hours = $this->workinghour->calculate_deduct_work_hours_by_leave($user_id, $start_date, $end_date);
			$total_working_hours -= $deduct_work_hours;
			
			$unpaid_working_hours = $this->_get_unpaid_used($user_id, $start_date, $end_date);
			
			$total_working_hours_accrued -= $unpaid_working_hours;
//			$total_working_hours_accrued = $total_working_hours + $annual_leave_used + $personal_leave_used;

			
			
			$overtime_hours = array();
			$overtime_hours = $this->_get_total_overtime_hours($user_id, $start_date, $end_date);
			$total_overtime_leave_hours = $overtime_hours['leave_hours'];
			
			$total_working_hours += $total_overtime_leave_hours;
			
			//Accrued Annual Leave
			// Add total working hours with leave used when calculating accrued hours
			$accrued_annual_leave = $this->_get_accrued_annual_leave($total_working_hours_accrued, $annual_Leave_Parameter);
			$accrued_annual_leave = get_number($accrued_annual_leave);
			
			//Accrued Personal Leave
			// Add total working hours with leave used when calculating accrued hours
			$accrued_personal_leave = $this->_get_accrued_personal_leave($total_working_hours_accrued, $personal_Leave_Parameter);
			$accrued_personal_leave = get_number($accrued_personal_leave);
			
			
			
//			$all_working_hours = $this->workinghour->calculate_hours_used($user_id, $user->hire_date, $end_date, 1, "true");
//			$all_working_hours = get_number($all_working_hours);
//			$all_increase_work_hours = $this->workinghour->calculate_increase_work_hours_by_leave($user_id, $user->hire_date, $end_date);
//			$all_working_hours += $all_increase_work_hours;
//			$all_deduct_work_hours = $this->workinghour->calculate_deduct_work_hours_by_leave($user_id, $user->hire_date, $end_date);
//			$all_working_hours -= $all_deduct_work_hours;
//			
//			$all_annual_leave_used = $this->_get_annual_leave_used($user_id, $user->hire_date, $end_date); 
//			$all_annual_leave_used = get_number($all_annual_leave_used);
//			
//			$all_personal_leave_used = $this->_get_personal_leave_used($user_id, $user->hire_date, $end_date);
//			$all_personal_leave_used = get_number($all_personal_leave_used);
//						
////			log_message("debug", "hire_date == " . date(DATE_FORMAT,$user->hire_date));
////			log_message("debug", "all_working_hours == " . $all_working_hours);
////			log_message("debug", "all_annual_leave_used == " . $all_annual_leave_used);
////			log_message("debug", "all_personal_leave_used == " . $all_personal_leave_used);
			
//			$all_working_hours = $all_working_hours - $all_annual_leave_used - $all_personal_leave_used;
			//Total Annual Leave Balance at the End of Payroll
			$payroll_data = $this->payroll_data_m->get_last_by_user($user_id);
			$last_annual_leave_end = 0;
			$last_personal_leave_end = 0;
			$last_inlieu_end = 0;
			if($payroll_data){
				$last_annual_leave_end = $payroll_data->annual_leave_end;
				$last_personal_leave_end = $payroll_data->personal_leave_end;	
				$last_inlieu_end = $payroll_data->inlieu_end;
			}else{
    			$total_working_stat = $this->working_hours_m->get_staff_working_hours($user_name);
				$last_annual_leave_end = get_number($total_working_stat->annual_hours_count);
				$last_personal_leave_end = get_number($total_working_stat->personal_hours_count);
			}
			
			log_message("debug", "last_annual_leave_end == " . $last_annual_leave_end);
			log_message("debug", "last_personal_leave_end == " . $last_personal_leave_end);
			

			//Total Annual and Personal Leave Balance at the End of Payroll
			if ($do_update){
				$annual_leave_end = $last_annual_leave_end + $accrued_annual_leave - $annual_leave_used;
				$personal_leave_end = $last_personal_leave_end + $accrued_personal_leave - $personal_leave_used;
				$inlieu_end = $last_inlieu_end - $inlieu_used;
			}
			else{
				$annual_leave_end = $last_annual_leave_end;
				$personal_leave_end = $last_personal_leave_end;
				$inlieu_end = $last_inlieu_end;
			}
			
			// Rest Break
			$userinfo['total_rest_break'] = $this->workinghour->calculate_total_rest_break($user_id, $start_date, $end_date);
			
			
			
			//staff id
			$userinfo['user_id'] = $user_id;
			
			//staff name
			$userinfo['name'] = $user->first_name . " " . $user->last_name;
			
			//Total Working hours
			$userinfo['total_working_hours'] = $total_working_hours;
			$userinfo['total_working_hours_accrued'] = $total_working_hours_accrued;
			
			//Accrued Annual Leave
			$userinfo['accrued_annual_leave'] = $accrued_annual_leave;
			
			//Accrued Personal Leave
			$userinfo['accrued_personal_leave'] = $accrued_personal_leave;
			
			//Annual Leave Used
			$userinfo['annual_leave_used'] = $annual_leave_used;
			
			//Personal Leave Used
			$userinfo['personal_leave_used'] = $personal_leave_used;
			
			//In Lieu Used
			$userinfo['inlieu_used'] = $inlieu_used;
			
			//Casual working hour
			$userinfo['casual_working_hour'] = $casual_working_hour;

			
			//Personal Leave Used
			$userinfo['unpaid_leave_used'] = $unpaid_working_hours;
			
			//Total Annual Leave Balance at the End of Payroll
			$userinfo['annual_leave_end'] = $annual_leave_end;
			
			//Total Personal Leave Balance at the End of Payroll
			$userinfo['personal_leave_end'] = $personal_leave_end;
			
			//Total In Lieu Balance at the End of Payroll
			$userinfo['inlieu_end'] = $inlieu_end;
				
			
			//Total Personal Leave Balance at the End of Payroll
			$userinfo['salary'] = round($total_working_hours_accrued * $hourly_rate, 2);
			
			//Overtime hours
			$userinfo['total_OT0_hours'] = $overtime_hours['OT 100%'] ? $overtime_hours['OT 100%'] : 0 ;
			$userinfo['total_OT1_hours'] = $overtime_hours['OT 150%'] ? $overtime_hours['OT 150%'] : 0 ;
			$userinfo['total_OT2_hours'] = $overtime_hours['OT 200%'] ? $overtime_hours['OT 200%'] : 0 ;
			$userinfo['total_OT3_hours'] = $overtime_hours['OT 125%'] ? $overtime_hours['OT 125%'] : 0 ;
			
			
			return $userinfo;
	}
	
	/*
	 * get total working hours
	 * @param user_id
	 * @param start date
	 * @param end date
	 */
	function _get_total_working_hours($user_id, $start_date, $end_date){
		log_message("debug", "_get_total_working_hours start time== " . $start_date);
//		$total_working_hours = $this->workinghour->calculate_hours_used( $user_id, $start_date, $end_date, 1, "true");
		$total_working_hours = $this->workinghour->calculate_hours_used( $user_id, $start_time, $end_time, 1, "true", "false", "true" );
		return get_number($total_working_hours);
	}
	
	/*
	 * get accrued annual leave
	 * @param int $total_working_hours
	 * @param float $param
	 */
	function _get_accrued_annual_leave($total_working_hours, $param){
		$count = round($total_working_hours / $param, 2);
		return $count;
	}
	
	/*
	 * get accrued personal leave
	 * @param int $total_working_hours
	 * @param float $param
	 */
	function _get_accrued_personal_leave($total_working_hours, $param){
		$count = round($total_working_hours / $param, 2);
		return $count;
	}
	
	/*
	 * get accrued annual leave used
	 * @param int $user_name
	 * @param int $start_date
	 * @param int $end_date
	 */
	function _get_annual_leave_used($user_id, $start_date, $end_date){
		//return $this->request_m->get_used_count_by_leave_type_time($user_name, LEAVE_TYPE_ANNUAL, $start_date, $end_date);
		return $this->workinghour->calculate_leave_used($user_id, LEAVE_TYPE_ANNUAL, $start_date, $end_date);
		
	}
	
	/*
	 * get accrued personal leave used
	 * @param int $user_name
	 * @param int $start_date
	 * @param int $end_date
	 */
	function _get_personal_leave_used($user_id, $start_date, $end_date){
		//return $this->request_m->get_used_count_by_leave_type_time($user_name, LEAVE_TYPE_PERSONAL, $start_date, $end_date);
		return $this->workinghour->calculate_leave_used($user_id, LEAVE_TYPE_PERSONAL, $start_date, $end_date);
	}
	
 	function _get_compasionate_leave_used($user_id, $start_date, $end_date){
		return $this->workinghour->calculate_leave_used($user_id, LEAVE_TYPE_COMPASIONATE, $start_date, $end_date);
	}	
	
	/*
	 * get unpaid leave used
	 * @param int $user_name
	 * @param int $start_date
	 * @param int $end_date
	 */
	function _get_unpaid_used($user_id, $start_date, $end_date){
		//return $this->request_m->get_used_count_by_leave_type_time($user_name, LEAVE_TYPE_PERSONAL, $start_date, $end_date);
		return $this->workinghour->calculate_leave_used($user_id, LEAVE_TYPE_UNPAID, $start_date, $end_date);
	}
	
	/*
	 * get in lieu used
	 * @param int $user_name
	 * @param int $start_date
	 * @param int $end_date
	 */
	function _get_inlieu_used($user_id, $start_date, $end_date){
		//return $this->request_m->get_used_count_by_leave_type_time($user_name, LEAVE_TYPE_PERSONAL, $start_date, $end_date);
		return $this->workinghour->calculate_leave_used($user_id, LEAVE_TYPE_INLIEU, $start_date, $end_date);
	}	
	
	/*
	 * get casual working hour
 	 * @param int $user_name
	 * @param int $start_date
	 * @param int $end_date
	 */
	function _get_casual_working_hour($user_id, $start_date, $end_date){
		//return $this->request_m->get_used_count_by_leave_type_time($user_name, LEAVE_TYPE_PERSONAL, $start_date, $end_date);
		return $this->workinghour->calculate_leave_used($user_id, LEAVE_TYPE_CASUAL, $start_date, $end_date);
	}	
	
	/*
	 * get Total Annual Leave Balance at the End of Payroll
	 * @param int $user_name
	 * @param int $all_working_hours
	 * @param float $param
	 * @param int $hire_date
	 * @param int $end_date
	 */
	function _get_annual_leave_end($user_name, $all_working_hours, $all_annual_leave_used, $param){
		//return round(($all_working_hours / $param - $all_annual_leave_used), 2);
		
		
	}
	
	/*
	 * get Total Personal Leave Balance at the End of Payroll
	 * @param int $user_name
	 * @param int $all_working_hours
	 * @param float $param
	 * @param int $hire_date
	 * @param int $end_date
	 */
	function _get_personal_leave_end($user_name, $all_working_hours, $all_personal_leave_used , $param){
		return round(($all_working_hours / $param - $all_personal_leave_used), 2);
	}
	
	function _get_total_overtime_hours($user_id, $start_date, $end_date){
		return $this->workinghour->calculate_overtime_hours($user_id, $start_date, $end_date);
	}

	/**
	 * get requests in edit payroll page
	 */
	function get_requests_by_update(){
		$user_id = $this->input->post('id');
		$data['user_id'] = $user_id;
		$data['payroll_data_id'] = $this->input->post('payroll_data');
		$start_time = $this->input->post('start_date');
		$end_time = $this->input->post('end_date');
		$this->load->model('St_leave_type_m', 'leave_type_m');
		$leave_types = $this->leave_type_m->get_change_select_dropdown();
        $data['leave_types'] = $leave_types;
		$data['requests'] = $this->request_m->get_approved_by_period($user_id, $start_time, $end_time);
		$this->load->view ( 'payroll/get_requests_data', $data );
	}
	
	/**
	 * get requests in add payroll page
	 */
	function get_requests_by_add(){
		$user_id = $this->input->post('id');
		$data['user_id'] = $user_id;
		$data['payroll_data_id'] = $this->input->post('payroll_data');
		$start_time = $this->input->post('start_date');
		$end_time = $this->input->post('end_date');
		$start_time = strtotime($start_time);
		$end_time = strtotime($end_time);
		$this->load->model('St_leave_type_m', 'leave_type_m');
		$leave_types = $this->leave_type_m->get_select_dropdown();
        $data['leave_types'] = $leave_types;
		$data['requests'] = $this->request_m->get_approved_by_period($user_id, $start_time, $end_time);
		$this->load->view ( 'payroll/get_requests_data', $data );
	}
	
	/**
	 * update request of payroll data
	 */
	function update_payroll_request_ajax(){
		log_message("debug", "update_payroll_request_ajax ...");
		$this->load->model('St_users_m', 'users_m');
		$now_time = now();
		$request_id = $this->input->post('id');
		$user_id = $this->input->post('user_id');
		$leave_type = $this->input->post('leave_type');
		$payroll_id = $this->input->post('payroll_id');
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');
		$payroll_data_id = $this->input->post('payroll_data_id');
		
		if(!empty($payroll_id)){
			$payroll = $this->payroll_m->get($payroll_id);
			$start_date = $payroll->start_date;
			$end_date = $payroll->end_date;
		}else{
			$start_date = strtotime($start_date);
			$end_date = strtotime($end_date);
		}
    	$requestinfo = $this->request_m->get_info($request_id);
    	$old_leave_type = $requestinfo->leave_type;
    	$old_leave_type_id = $requestinfo->leave_type_id;
		//TODO update request
		$update_request = array(
			'leave_type_id' => $leave_type
		);
		$this->request_m->update($request_id, $update_request);
		
		//add history log
    	$this->load->model('St_request_log_m', 'request_log_m');
    	$log = array(
    		'id' => $request_id,
    		'leave_type_id' => $leave_type,
    		'operator_id' => $this->session->userdata('user_id'),
    		'operation' => "changed", //changed by admin
    		'operate_time' => $now_time
    	);
    	$this->request_log_m->insert($log);
	    
	    //send mail to admin/staff
	    $requestinfo = $this->request_m->get_info($request_id);
    	$user_name = $requestinfo->user_name;
		
		if($old_leave_type_id != $requestinfo->leave_type_id){
			$this->load->library('phpmail');
			$this->phpmail->readConfig();
			
			//send mail to all manager and admin
			$managers = $this->users_m->get_managers_by_user_name($user_name);
			if(!empty($managers)){
				foreach($managers as $manager){
					if(!empty($manager)){
						if(!empty($manager->email)){
							$this->phpmail->AddAddress($manager->email);
						}
						if(!empty($manager->personal_email)){
							$this->phpmail->AddCC($manager->personal_email);
						}
					}
				}
			}
			
			$amount_leave = "";
			if($requestinfo->leave_type_id == LEAVE_TYPE_ANNUAL 
				|| $requestinfo->leave_type_id == LEAVE_TYPE_PERSONAL){
				//calculate total annual/personal leave balance
		    	$this->load->model('St_working_hours_m', 'working_hours_m');    	
		    	$total_working_stat = $this->working_hours_m->get_staff_working_hours($user_name);
		    	
		    	$work_count = $total_working_stat->work_count;
		    	
		    	if($requestinfo->leave_type_id == LEAVE_TYPE_ANNUAL){
		    		$total_annual = $total_working_stat->annual_hours_count;
		    		$annual_used_count = $total_working_stat->annual_used_count;
		    		//calculate the annual leave balance
		    		$annual_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_ANNUAL);
		    		$annual_upcoming_used = $this->request_m->get_upcoming_count_by_leave_type($user_name, LEAVE_TYPE_ANNUAL);
		    		$annual_upcoming_used = IS_NULL($annual_upcoming_used)?0:$annual_upcoming_used;
		    		$amount_leave_count = $total_annual - $annual_used - $annual_upcoming_used;
		    		$amount_leave_count = round($amount_leave_count, 2);
		    		$amount_leave = ($amount_leave_count > 0)?$amount_leave_count:0;
		    	}
		    	else if($requestinfo->leave_type_id == LEAVE_TYPE_PERSONAL){
		    		$total_personal = $total_working_stat->personal_hours_count;
		    		$personal_used_count = $total_working_stat->personal_used_count;
		    		//calculate the annual leave balance
		    		$personal_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL);
		    		$personal_upcoming_used = $this->request_m->get_upcoming_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL);
		    		$personal_upcoming_used = IS_NULL($personal_upcoming_used)?0:$personal_upcoming_used;
		    		$amount_leave_count = $total_personal - $personal_used - $personal_upcoming_used;
		    		$amount_leave_count = round($amount_leave_count, 2);
		    		$amount_leave = ($amount_leave_count > 0)?$amount_leave_count:0;
		    	}
			}
			
			if($amount_leave > 1){
				$amount_leave = $amount_leave . " Hours";
			}else{
				$amount_leave = $amount_leave . " Hour";
			}
			$mail_body['amount_leave_left'] = $amount_leave;
			
			$subject = "Changed Requested - " 
			. $requestinfo->first_name . " " . $requestinfo->last_name  
			. " From " . $old_leave_type . " To " . $requestinfo->leave_type;
			$this->phpmail->setSubject($subject);
			
	    	$mail_body['appove_url'] = site_url('request/view/'.$request_id);
	    	$mail_body['appove_name'] = "view this leave application here.";
	    	$mail_body['request'] = $requestinfo;
	    	
			$this->phpmail->setBody($this->load->view('mail/request', $mail_body , True));
			if(!$this->phpmail->send()){
	        	log_message("debug", "send mail .... ".$subject . $this->phpmail->ErrorInfo);
	        }
	        
	        //
	        $this->phpmail->ClearAllAddresses();
			$email = $this->session->userdata("email");
			if(!empty($email)){
				$this->phpmail->AddAddress($email);
			}
			$personal_email = $this->session->userdata("personal_email");
			if(!empty($personal_email)){
				$this->phpmail->AddCC($personal_email);	
			}
			$mail_body['appove_url'] = site_url('request/view/'.$request_id);
			$mail_body['appove_name'] = "view this leave application here.";
			$this->phpmail->setBody($this->load->view('mail/request', $mail_body , True));
			if(!$this->phpmail->send()){
	        	log_message("debug", "send mail .... ".$subject . $this->phpmail->ErrorInfo);
	        }
			$this->phpmail->ClearAllAddresses();
		}
			        
		//get config leave hour parameter
    	$this->load->model('St_config_m', 'config_m');
    	$annual_row = $this->config_m->get_by_code('Annual_Leave_Parameter');
    	$annual_Leave_Parameter = $annual_row->value;
    	$personal_row = $this->config_m->get_by_code('Personal_Leave_Parameter');
    	$personal_Leave_Parameter = $personal_row->value;
    	//$this->load->model('St_users_m', 'users_m');
		$user = $this->users_m->get($user_id);
    	
		$payrollinfo = $this->_get_payroll_data($user, $start_date, $end_date, $annual_Leave_Parameter, $personal_Leave_Parameter);
		
		$update_payroll_data = array(
			//Total Working hours
			'total_working_hours' => $payrollinfo['total_working_hours'],
			'total_working_hours_accrued' => $payrollinfo['total_working_hours_accrued'],
			//Accrued Annual Leave
			'accrued_annual_leave' => $payrollinfo['accrued_annual_leave'],
			//Accrued Personal Leave
			'accrued_personal_leave' => $payrollinfo['accrued_personal_leave'],
			//Annual Leave Used
			'annual_leave_used' => $payrollinfo['annual_leave_used'],
			//Personal Leave Used
			'personal_leave_used' => $payrollinfo['personal_leave_used'],
			//Total Annual Leave Balance at the End of Payroll
			'annual_leave_end' => $payrollinfo['annual_leave_end'],
			//Total Personal Leave Balance at the End of Payroll
			'personal_leave_end' => $payrollinfo['personal_leave_end'],
			'salary' => $payrollinfo['salary']
		);
		
		$this->payroll_data_m->update($payroll_data_id, $update_payroll_data);
		$paryoll_data = $this->payroll_data_m->get($payroll_data_id);
		$payroll_data_log = array(
					'id' => $payroll_data_id,
					'processed' => $paryoll_data->processed,
					'operator_id' => $this->session->userdata('user_id'),
					'operation' => 'update',
					'operate_time' => $now_time,
					'operate_ip' => $this->input->ip_address()
				);
		$payroll_data_log = array_merge($payroll_data_log, $update_payroll_data);
		$this->payroll_data_log_m->insert($payroll_data_log);
		//TODO get payroll data
		//$payroll_data = $this->payroll_data_m->get($payroll_data_id);
        echo json_encode($update_payroll_data);
	}
	
	/**
	 * process payroll data by ajax
	 */
	function process_payroll_ajax(){
		$now_time = now();
		$user_id = $this->input->post('user_id');
		$payroll_id = $this->input->post('payroll_id');
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');
		$payroll_data_id = $this->input->post('payroll_data_id');
		
		if(!empty($payroll_id)){
			$payroll = $this->payroll_m->get($payroll_id);
			$start_date = $payroll->start_date;
			$end_date = $payroll->end_date;
		}else{
			$start_date = strtotime($start_date);
			$end_date = strtotime($end_date);
		}
		
		//get config leave hour parameter
    	$this->load->model('St_config_m', 'config_m');
    	$annual_row = $this->config_m->get_by_code('Annual_Leave_Parameter');
    	$annual_Leave_Parameter = $annual_row->value;
    	$personal_row = $this->config_m->get_by_code('Personal_Leave_Parameter');
    	$personal_Leave_Parameter = $personal_row->value;
    	$this->load->model('St_users_m', 'users_m');
		$user = $this->users_m->get($user_id);
    	
		$payrollinfo = $this->_get_payroll_data($user, $start_date, $end_date, $annual_Leave_Parameter, $personal_Leave_Parameter, $do_update=true);
		
		$update_payroll_data = array(
			//Total Working hours
			'total_working_hours' => $payrollinfo['total_working_hours'],
			//Accrued Annual Leave
			'accrued_annual_leave' => $payrollinfo['accrued_annual_leave'],
			//Accrued Personal Leave
			'accrued_personal_leave' => $payrollinfo['accrued_personal_leave'],
			//Annual Leave Used
			'annual_leave_used' => $payrollinfo['annual_leave_used'],
			//Personal Leave Used
			'personal_leave_used' => $payrollinfo['personal_leave_used'],
			//Total Annual Leave Balance at the End of Payroll
			'annual_leave_end' => $payrollinfo['annual_leave_end'],
			//Total Personal Leave Balance at the End of Payroll
			'personal_leave_end' => $payrollinfo['personal_leave_end'],
			'inlieu_end' => $payrollinfo['inlieu_end'],
			'salary' => $payrollinfo['salary']
		);
		
		$this->payroll_data_m->update($payroll_data_id, $update_payroll_data);

		$status = $this->payroll_data_m->process($payroll_data_id);
		
		$result = array(
        	"status" 	=> $status
        );
        
        $result = array_merge($result, $update_payroll_data);
        
		$payroll_data_log = array(
					'id' => $payroll_data_id,
					'processed' => 1,
					'operator_id' => $this->session->userdata('user_id'),
					'operation' => 'update',
					'operate_time' => $now_time,
					'operate_ip' => $this->input->ip_address()
				);
		
		$this->payroll_data_log_m->insert($payroll_data_log);
		log_message("debug", "process_payroll_ajax=====1");
		//check whether all data is processed
		$process_count = $this->payroll_data_m->get_process_count_by_payroll($payroll_id);
		if($process_count == 0){
			//update the process status of payroll
			$this->payroll_m->setProcessed($payroll_id);
		}
		log_message("debug", "process_payroll_ajax=====2");
		
        echo json_encode($result);
	}
	
	/**
	 * undo payroll data
	 */
	function undo_payroll_ajax(){
		$now_time = now();
		$payroll_data_id = $this->input->post('payroll_data_id');
		$status = $this->payroll_data_m->undo_process($payroll_data_id);
		$payroll_data_log = array(
					'id' => $payroll_data_id,
					'processed' => 0,
					'operator_id' => $this->session->userdata('user_id'),
					'operation' => 'undo',
					'operate_time' => $now_time,
					'operate_ip' => $this->input->ip_address()
				);
		$this->payroll_data_log_m->insert($payroll_data_log);
		
		//update payroll process status
		$payroll_data = $this->payroll_data_m->get($payroll_data_id);
		$this->payroll_m->setAdjustment($payroll_data->payroll_id);
		$result = array(
        	"status" 	=> $status
        );
        echo json_encode($result);
	}
	
	/**
	 * list QB export
	 */
	function qb_export() {
		$data ['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('payroll')."'>Payroll</a> > QB Export List";
		$data['payrolls'] = $this->payroll_m->get_qb_export();
		$data['form_id'] = "payroll";
		//$total_count = $this->payroll_m->count_payrolls();
    	$total_count = $this->payroll_m->count_processed();
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	
		$this->load->view ( 'navigation', $data );
		$this->load->view ( 'payroll/list_qb_export', $data );
	}
	
	
	/**
	 * get QB payrolls by ajax
	 */
	function get_qb_payrolls_ajax(){
		log_message("debug", "get_qb_payrolls_ajax....");
		$form_id = "payroll";
		$data['form_id'] = $form_id;
        
    	$page_no = $this->input->post('payroll_page_no');
        
		$limit = $this->input->post('payroll_limit');
		
		log_message("debug", "payroll_page_no...." . $page_no);
		log_message("debug", "payroll_limit...." . $limit);
    	$data['page_limit'] = $limit;
    	
		$data['payrolls'] = $this->payroll_m->get_processed($page_no, $limit);
		$total_count = $this->payroll_m->count_processed();
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
		
		log_message("debug", "payroll_encode....");
		$content = $this->load->view('payroll/list_qb_export_data', $data, true);
		log_message("debug", "payroll_encode2....");
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
	}
	
	function qb($payroll_id){
		$data ['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('payroll/qb_export')."'>QB Export List</a> > QB Payroll Data";
		$form_id = "payroll";
		$data['form_id'] = $form_id;
		
		$payroll = $this->payroll_m->get($payroll_id);
		$data['payroll'] = $payroll;
		
		$payrolldatas = $this->payroll_data_m->get_qb_by_payroll($payroll_id);
		
		$total_count = $this->payroll_data_m->count_proccessed_by_payroll($payroll_id);
		
		$limit = PAGE_SIZE;
    	$page_count = 0;
    	if(($total_count % $limit) == 0){
    		$page_count = $total_count / $limit;
    	}else{
    		$page_count = floor($total_count / $limit) + 1;
    	}
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
		
		$data['payrolldatas'] = $payrolldatas;
		
		$this->load->view ( 'navigation', $data );
		$this->load->view ( 'payroll/qb_payroll' );
	}
	
	function qb_export_payroll_ajax(){
		$now_time = now();
		$payroll_data_id = $this->input->post('payroll_data_id');
		
		$status = $this->payroll_data_m->qb_exported($payroll_data_id);
        
		$payroll_data_log = array(
					'id' => $payroll_data_id,
					'processed' => 2,
					'operator_id' => $this->session->userdata('user_id'),
					'operation' => 'update',
					'operate_time' => $now_time,
					'operate_ip' => $this->input->ip_address()
				);
		
		$this->payroll_data_log_m->insert($payroll_data_log);
		
		$payroll_data = $this->payroll_data_m->get($payroll_data_id);
		
		//check whether all data is processed
		$payroll_id = $payroll_data->payroll_id;
		$all_count = $this->payroll_data_m->count_by_payroll($payroll_id);
		$qb_count = $this->payroll_data_m->count_qb_by_payroll($payroll_id);
		if($all_count == $qb_count){
			//update the QB exported status of payroll
			$this->payroll_m->setQBexported($payroll_id);
		}
		
		$result = array(
        	"status" 	=> $status
        );
		
        echo json_encode($result);
	}
	
	/**
	 * history
	 */
	function history() {
		$now_time = now();
		$data ['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > Payroll > View Payroll History";
		$this->load->view ( 'navigation', $data );
		
		$data['payrolls'] = $this->payroll_m->get_qb_exported();
		
		$data['form_id'] = "payroll";
		//$total_count = $this->payroll_m->count_payrolls();
    	$qb_exported_count = $this->payroll_m->count_qb_exported();
    	
    	//$adjustment_id = $this->_get_adjustment_id();
    	
    	$total_count = $qb_exported_count;
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
        //$data['adjustment_id'] = $adjustment_id;
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	$data['total_count'] = $total_count;
		$this->load->view ( 'payroll/list_history_payrolls', $data);
	}
	
	function get_history_payrolls_ajax(){
		log_message("debug", "get_history_payrolls_ajax....");
		$form_id = "payroll";
		$data['form_id'] = $form_id;
        
    	$page_no = $this->input->post('payroll_page_no');
        
		$limit = $this->input->post('payroll_limit');
		
		log_message("debug", "payroll_page_no...." . $page_no);
		log_message("debug", "payroll_limit...." . $limit);
    	$data['page_limit'] = $limit;
    	
		$data['payrolls'] = $this->payroll_m->get_qb_exported($page_no, $limit);
		$total_count = $this->payroll_m->count_qb_exported();
		//$adjustment_id = $this->_get_adjustment_id();
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	//$data['adjustment_id'] = $adjustment_id;
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
		
		$content = $this->load->view('payroll/list_history_payrolls_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
	}
	
	function view_history($payroll_id){
		$data ['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('payroll')."'>Payroll</a> > View Payroll Data";
		
		$payroll = $this->payroll_m->get($payroll_id);
		$data['payroll'] = $payroll;
		if($payroll->processed != 2 && $payroll->processed != 3){
			show_404();
		}
		
		$payrolldatas = $this->payroll_data_m->get_by_payroll($payroll_id);
		
		$adjustment_id = $this->_get_adjustment_id();
		$data['adjustment_id'] = $adjustment_id;
		
		$data['payrolldatas'] = $payrolldatas;
		
		$this->load->view ( 'navigation', $data );
		$this->load->view ( 'payroll/view_history_payroll' );
	}
	
	/**
	 * adjustment
	 */
	function adjustment() {
		$now_time = now();
		$data ['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > Payroll > Adjustment Payroll";
		$this->load->view ( 'navigation', $data );
		
		$data['payrolls'] = $this->payroll_m->get_adjustments();
		
		$data['form_id'] = "payroll";
    	$adjustment_count = $this->payroll_m->count_adjustment();
    	
    	$total_count = $adjustment_count;
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	$data['total_count'] = $total_count;
		$this->load->view ( 'payroll/list_adjustment_payrolls', $data);
	}
	
	function adjust($payroll_id) {
		$data['form_id'] = "payroll";
		$data['payroll_id'] = $payroll_id;
		$data ['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('payroll')."'>Payroll</a> > Edit Payroll Data";
		
		$payroll = $this->payroll_m->get($payroll_id);
		if($payroll->processed != 3){
			show_404();
		}
		$data['payroll'] = $payroll;
		
		
		$payrolldatas = $this->payroll_data_m->get_adjustment_by_payroll($payroll_id);
		
		$page_count = 0;
		$total_count = $this->payroll_data_m->count_adjustment_by_payroll($payroll_id);
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        $data['total_count'] = $total_count;
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
		
		$data['payrolldatas'] = $payrolldatas;
		
		$this->load->view ( 'navigation', $data );
		$this->load->view ( 'payroll/edit_adjustment_payroll' );
	}
	
	/**
	 * get the adjustment_id
	 */
	function _get_adjustment_id(){
		//judge whether there is adjustment payroll
		$payroll_id = 0;
		$adjustment = $this->payroll_m->get_adjustment();
		if(empty($adjustment)){
			//if no, get the first payroll which is not processed. return less than the first payroll
			$payroll = $this->payroll_m->get_first_unprocessed();
			if(!empty($payroll)){
				$tmp_id = $payroll->id;
				$tmp_id -= 1;
				if($tmp_id > 0){
					$payroll = $this->payroll_m->get_history_by_id($tmp_id);
					if(!empty($payroll)){
						$payroll_id = $payroll->id;
					}
				}
			}
		}else{
			//if yes, return the payroll id
			$payroll_id = $adjustment->id;
		}
		
		log_message("debug", "_get_adjustment_id == " . $payroll_id);
		
		return $payroll_id;
	}
	
	
	function import_csv(){
		
		$payroll_id = $this->input->post('payroll_id');
		$data['payroll_id'] = $payroll_id;
		
		$payroll = $this->payroll_data_m->get_by_payroll($payroll_id);
		
		
		$handle = fopen($_FILES['file_name']['tmp_name'], "r");
		$i=0;
		
		$payrolldatas = array();
		
 		while (($csv = fgetcsv($handle, 10000, ",")) !== FALSE){
			if ($i > 0 && !empty($csv[0])){
				$payrolldatas[$i]->user_name = $csv[0];
				$payrolldatas[$i]->personal_leave_end = $csv[1];
				$payrolldatas[$i]->annual_leave_end = $csv[2];
				
				reset($payroll);
				$payrolldatas[$i]->id = 0;
				$payrolldatas[$i]->payroll_personal_leave_end = 0;
				$payrolldatas[$i]->payroll_annual_leave_end = 0;
				$payrolldatas[$i]->payroll_inlieu_end = 0;
				$payrolldatas[$i]->inlieu_end = 0;
				foreach ($payroll as $p){
					if (trim($p->qb_employee_name) == trim($csv[0])){
						$payrolldatas[$i]->id = $p->id;
						$payrolldatas[$i]->payroll_personal_leave_end = $p->personal_leave_end;
						$payrolldatas[$i]->payroll_annual_leave_end = $p->annual_leave_end;
						$payrolldatas[$i]->payroll_inlieu_end = $p->inlieu_end;
						$payrolldatas[$i]->inlieu_end = $p->inlieu_end;
					}		
				}
				
				// Find user id/username
				
			}	
			$i++;
		}

		
		$data['payrolldatas'] = $payrolldatas;
		
		$this->load->view ( 'payroll/import_payroll_data', $data );
	}
	
	function do_import_csv(){
		$annual = $this->input->post('annual');
		$personal = $this->input->post('personal');
		$inlieu = $this->input->post('inlieu');
		$payroll_id = $this->input->post('payroll_id');
		
		foreach ($annual as $id=>$val){
			if ($id){
				$payroll_data = array('annual_leave_end'=> $val);
				$this->payroll_data_m->update($id, $payroll_data);
			}			
		}
		
 		foreach ($personal as $id=>$val){
 			if ($id){
				$payroll_data = array('personal_leave_end'=> $val);
				$this->payroll_data_m->update($id, $payroll_data);
 			}
 		}
 		
 		foreach ($inlieu as $id=>$val){
 			if ($id){
 				$payroll_data = array('inlieu_end'=> $val);
 				$this->payroll_data_m->update($id, $payroll_data);
 			}
 		} 		
	}
}

?>
