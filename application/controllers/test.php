<?php

class Test extends CI_Controller {

    function Test() {
        parent::__construct();
    }

    function index() {
        $data['navigation'] = "<a href='". site_url('admin/leave') ."' target='_top'>Leave Request</a> > <a href='". site_url('request') ."'>Leave Statement</a> > View";
        $this->load->view('navigation', $data);
        
        $this->load->view('test');
    }

    function set_inactive($id){
    	$this->load->model('St_request_m', 'request_m');
    	$b_update = $this->request_m->set_inactive($id);
    	echo $b_update;
    }
    
	function set_active($id){
    	$this->load->model('St_request_m', 'request_m');
    	$b_update = $this->request_m->set_active($id);
    	echo $b_update;
    }
    
    function test_strcmp(){
    	$a = "staff2";
    	$b = "staff2";
    	$c = "staff1";
    	echo strcmp($a, $b);
    	echo strcmp($a, $c);
    }
    /**
     * request a new leave
     * @return 
     */
    function mail() {
        $this->load->library('phpmailer');
    
    //$this->mail->IsSMTP();                           // tell the class to use SMTP
	$this->phpmailer->SMTPAuth   = true;                  // enable SMTP authentication
	$this->phpmailer->Port       = 25;                    // set the SMTP server port
	$this->phpmailer->Host       = "smtp.163.com"; // SMTP server
	$this->phpmailer->Username   = "scorptec";     // SMTP server username
	$this->phpmailer->Password   = "scorptec.com";            // SMTP server password

	$this->phpmailer->IsSMTP();  // tell the class to use Sendmail

	$this->phpmailer->AddReplyTo("baiyb@shinetechchina.com","First Last");

	$this->phpmailer->From       = "scorptec@163.com";
	$this->phpmailer->FromName   = "First Last";

	$to = "baiyb@shinetechchina.com";
	$to = "scorptec@163.com";
	$to = "bybsky@gmail.com";
	$this->phpmailer->AddAddress($to);

	$this->phpmailer->Subject  = "First PHPMailer Message";

	$htmlbody = _htmlbody();
	$this->phpmailer->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
	$this->phpmailer->WordWrap   = 80; // set word wrap

	$this->phpmailer->MsgHTML("Test 1 by bai");

	$this->phpmailer->IsHTML(true); // send as HTML

	$this->phpmailer->Send();
        /*
        $to = "bybsky@gmail.com";
        $this->phpmailer->AddAddress($to);
        $this->phpmailer->setSubject("test1 PHPMailer Message");
        $this->phpmailer->setHTMLBody("test1 Here is a test HTML email");
        $this->phpmailer->send();
        */
    }    
    
    function _htmlbody(){
    	$body = '<p>Leave  Application</p>
<table width="450" border="1">
  <tr>
    <th width="186" align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Employee: </span></th>
    <td width="248">&nbsp;</td>
  </tr>
  <tr>
    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Leave  Type:</span></th>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Requested  On</span></th>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">From:</span></th>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">To:</span></th>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Reason:</span></th>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Amount  of Leave Left (before this request)</span></th>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">This  leave is</span></th>
    <td>&nbsp;</td>
  </tr>
</table>
<p>Supervisors Response </p>
<p>Please <a href="http://aaaaaa.com">approve  or delete this leave application here.</a></p>';
    	return $body;
    }
    
    function mail2() {
        $this->load->library('phpmail');
    $body = '<p>Leave  Application</p>
<table width="450" border="1">
  <tr>
    <th width="186" align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Employee: </span></th>
    <td width="248">&nbsp;</td>
  </tr>
  <tr>
    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Leave  Type:</span></th>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Requested  On</span></th>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">From:</span></th>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">To:</span></th>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Reason:</span></th>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Amount  of Leave Left (before this request)</span></th>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">This  leave is</span></th>
    <td>&nbsp;</td>
  </tr>
</table>
<p>Supervisors Response </p>
<p>Please <a href="http://aaaaaa.com">approve  or delete this leave application here.</a></p>';
        
        $to = "bybsky@gmail.com";
        $this->phpmail->AddAddress($to);
        $this->phpmail->setSubject("Html body test");
        $this->phpmail->setHTMLBody($body);
        if(!$this->phpmail->send()){
        	log_message("debug", $this->phpmail->ErrorInfo);
        	echo $this->phpmail->ErrorInfo;
        }
    }

    function calendar(){
    	//$this->load->library('calendar');
    	//echo $this->calendar->generate();
    	//echo $this->calendar->generate(2006, 6);
    	/**
    	$data = array(
               3  => 'http://example.com/news/article/2006/03/',
               7  => 'http://example.com/news/article/2006/07/',
               13 => 'http://example.com/news/article/2006/13/',
               26 => 'http://example.com/news/article/2006/26/'
             );

		echo $this->calendar->generate(2006, 6, $data);
		*/
    	/*
    	$prefs = array (
               'start_day'    => 'saturday',
               'month_type'   => 'long',
               'day_type'     => 'short'
             );

		$this->load->library('calendar', $prefs);
		
		echo $this->calendar->generate();
		*/
    	/*
    	$prefs = array (
               'show_next_prev'  => TRUE,
               'next_prev_url'   => 'http://example.com/index.php/calendar/show/'
             );

		$this->load->library('calendar', $prefs);
		
		echo $this->calendar->generate($this->uri->segment(3), $this->uri->segment(4));
		*/
    	$prefs['template'] = '

		   {table_open}<table border="0" cellpadding="0" cellspacing="0">{/table_open}
		
		   {heading_row_start}<tr>{/heading_row_start}
		
		   {heading_previous_cell}<th><a href="{previous_url}">&lt;&lt;</a></th>{/heading_previous_cell}
		   {heading_title_cell}<th colspan="{colspan}">{heading}</th>{/heading_title_cell}
		   {heading_next_cell}<th><a href="{next_url}">&gt;&gt;</a></th>{/heading_next_cell}
		
		   {heading_row_end}</tr>{/heading_row_end}
		
		   {week_row_start}<tr>{/week_row_start}
		   {week_day_cell}<td>{week_day}</td>{/week_day_cell}
		   {week_row_end}</tr>{/week_row_end}
		
		   {cal_row_start}<tr>{/cal_row_start}
		   {cal_cell_start}<td>{/cal_cell_start}
		
		   {cal_cell_content}<a href="{content}">{day}</a>{/cal_cell_content}
		   {cal_cell_content_today}<div class="highlight"><a href="{content}">{day}</a></div>{/cal_cell_content_today}
		
		   {cal_cell_no_content}{day}{/cal_cell_no_content}
		   {cal_cell_no_content_today}<div class="highlight">{day}</div>{/cal_cell_no_content_today}
		
		   {cal_cell_blank}&nbsp;{/cal_cell_blank}
		
		   {cal_cell_end}</td>{/cal_cell_end}
		   {cal_row_end}</tr>{/cal_row_end}
		
		   {table_close}</table>{/table_close}
		';
		
		$this->load->library('calendar', $prefs);
		
		echo $this->calendar->generate();
    }

    function date_diff(){
    	$t1 = 'Feb 4, 2008 12:16:00'; 
		$t2 = 'Jul 3, 2006 16:15:30'; 
    	echo date_diff1($t1, $t2), "\n" . 
    		date_diff1($t1, $t2, 3), "\n" .
    		date_diff1($t1, $t2, 2, true), "\n"; 
    }
    /** 
	 * this code assumes php >= 5.1.0. if using < 5.1, read 
	 * php.net/strtotime and change the condition for checking 
	 * for failure from strtotime() 
	 */ 
	
	// $t1, $t2: unix times, or strtotime parseable 
	// $precision: max number of units to output 
	// $abbr: if true, use "hr" instead of "hour", etc. 
	function date_diff1 ($t1, $t2, $precision = 6, $abbr = false) { 
	    if (preg_match('/\D/', $t1) && ($t1 = strtotime($t1)) === false) 
	        return false; 
	
	    if (preg_match('/\D/', $t2) && ($t2 = strtotime($t2)) === false) 
	        return false; 
	
	    if ($t1 > $t2) 
	        list($t1, $t2) = array($t2, $t1); 
	
	    $diffs = array( 
	        'year' => 0, 'month' => 0, 'day' => 0, 
	        'hour' => 0, 'minute' => 0, 'second' => 0, 
	    ); 
	
	    $abbrs = array( 
	        'year' => 'yr', 'month' => 'mth', 'day' => 'day', 
	        'hour' => 'hr', 'minute' => 'min', 'second' => 'sec' 
	    ); 
	
	    foreach (array_keys($diffs) as $interval) { 
	        while ($t2 >= ($t3 = strtotime("+1 ${interval}", $t1))) { 
	            $t1 = $t3; 
	            ++$diffs[$interval]; 
	        } 
	    } 
	
	    $stack = array(); 
	    foreach ($diffs as $interval => $num) 
	        $stack[] = array($num, ($abbr ? $abbrs[$interval] : $interval) . ($num != 1 ? 's' : '')); 
	
	    $ret = array(); 
	    while (count($ret) < $precision && ($item = array_shift($stack)) !== null) { 
	        if ($item[0] > 0) 
	            $ret[] = "{$item[0]} {$item[1]}"; 
	    } 
	
	    return implode(', ', $ret); 
	}
    
	function time_period(){
		$thisweek = this_week();
		echo "This week: <br/>";
		echo $thisweek['start']. "<br/>";
		echo date(DATETIME_FORMAT, $thisweek['start']). "<br/>";
		echo $thisweek['end']. "<br/>";
		echo date(DATETIME_FORMAT, $thisweek['end']). "<br/>";
		
		
		$lastweek = last_week();
		echo "Last week: <br/>";
		echo $lastweek['start']. "<br/>";
		echo date(DATETIME_FORMAT, $lastweek['start']). "<br/>";
		echo $lastweek['end']. "<br/>";
		echo date(DATETIME_FORMAT, $lastweek['end']). "<br/>";
		
		$lastweek = this_month();
		echo "this_month: <br/>";
		echo $lastweek['start']. "<br/>";
		echo date(DATETIME_FORMAT, $lastweek['start']). "<br/>";
		echo $lastweek['end']. "<br/>";
		echo date(DATETIME_FORMAT, $lastweek['end']). "<br/>";
		
		
		$lastweek = last_month();
		echo "last_month: <br/>";
		echo $lastweek['start']. "<br/>";
		echo date(DATETIME_FORMAT, $lastweek['start']). "<br/>";
		echo $lastweek['end']. "<br/>";
		echo date(DATETIME_FORMAT, $lastweek['end']). "<br/>";
		
		$lastweek = this_quarter();
		echo "this_quarter: <br/>";
		echo $lastweek['start']. "<br/>";
		echo date(DATETIME_FORMAT, $lastweek['start']). "<br/>";
		echo $lastweek['end']. "<br/>";
		echo date(DATETIME_FORMAT, $lastweek['end']). "<br/>";
		
		$lastweek = last_quarter();
		echo "last_quarter: <br/>";
		echo $lastweek['start']. "<br/>";
		echo date(DATETIME_FORMAT, $lastweek['start']). "<br/>";
		echo $lastweek['end']. "<br/>";
		echo date(DATETIME_FORMAT, $lastweek['end']). "<br/>";
		
		$lastweek = this_year();
		echo "this_year: <br/>";
		echo $lastweek['start']. "<br/>";
		echo date(DATETIME_FORMAT, $lastweek['start']). "<br/>";
		echo $lastweek['end']. "<br/>";
		echo date(DATETIME_FORMAT, $lastweek['end']). "<br/>";
		
		$lastweek = last_year();
		echo "last_year: <br/>";
		echo $lastweek['start']. "<br/>";
		echo date(DATETIME_FORMAT, $lastweek['start']). "<br/>";
		echo $lastweek['end']. "<br/>";
		echo date(DATETIME_FORMAT, $lastweek['end']). "<br/>";
		
	}
	
	function test_date_add(){
		$t = date_add('d', 1 , "2010-01-02 11:23");
		echo date(DATETIME_FORMAT, $t);
	}
	
	function mailtwo(){
		$this->load->library('phpmail');
		$this->phpmail->AddAddress("baiyb@shinetechchina.com");
		$this->phpmail->AddAddress("bybsky@163.com");
		$this->phpmail->AddAddress("bybsky@sina.com");
		$this->phpmail->AddCC("bybsky@126.com");
		$this->phpmail->AddCC("bybsky@gmail.com");
  		$this->phpmail->setSubject("test subject 2");
		$this->phpmail->setBody("test the mail 2...");
        if(!$this->phpmail->send()){
        	log_message("debug", "test subject" . $this->phpmail->ErrorInfo);
        }		
	}
	
	function age(){
		echo calculate_age(strtotime("1976-08-29"));
	}
}