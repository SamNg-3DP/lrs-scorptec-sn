<?php

class Shiftcategory extends CI_Controller {
	
	function Shiftcategory() {
		parent::__construct();
		if(!$this->userauth->logined()){
			$infoMsg = array(
				'info' => 'You have no privilege into this page !'
			);
			$this->session->set_flashdata('auth', $this->load->view('msgbox/info', $infoMsg , True));
			redirect("login");
		}else{
			if(!$this->userauth->judge_priv("shiftcategory")){
				$errorMsg = array(
					'error' => 'You have no privilege into this page !'
				);
				$this->session->set_flashdata('auth', $this->load->view('msgbox/error', $errorMsg , True));
				redirect("msgbox", 'location');
			}
		}
		$this->form_validation->set_error_delimiters ( "<div style='font:Tahoma, Geneva, sans-serif; font-size:11px; color:#F00'>", "</div>" );
		$this->load->model('St_shift_category_m', 'shift_category_m');
	}
	
	function index() {
		$data ['shiftcategory'] = $this->shift_category_m->get_shiftcategory ();
		$data ['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > Shift/Roster > <a href='".site_url('shiftcategory')."'>Shift Category</a> > View";
		$this->load->view ( 'navigation', $data );
		$this->load->view ( 'shiftcategory/list_shiftcategories', $data );
	}
	
	/**
	 * prepare add shift category
	 */
	function add() {
		$data ['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > Shift/Roster > <a href='".site_url('shiftcategory')."'>Shift Category</a> > Add New";
		$this->load->view ( 'navigation', $data );
		$this->load->view ( 'shiftcategory/add_shiftcategory' );
	}
	
	/**
	 * process add shift category
	 */
	function do_add() {
		$this->form_validation->set_rules ( 'code', 'Code', 'trim|required' );
		$this->form_validation->set_rules ( 'description', 'Description', 'trim|required' );
		$this->form_validation->set_rules ( 'color', 'Color', 'trim|required' );
		
		if ($this->form_validation->run () == FALSE) {
			$this->index_add ();
		} else {
			$code = $this->input->post ( 'code' );
			$description = $this->input->post ( 'description' );
			$color = $this->input->post ( 'color' );
			$total = $this->input->post ( 'radio' );
			
			$shift_category = array(
				'code' 			=> $code,
				'description' 	=> $description,
				'color'			=> $color,
				'total'			=> $total
			);
			$this->shift_category_m->insert ($shift_category);
			$this->index ();
		}
	}
	
	/**
	 * prepare edit shift category
	 */
	function edit($id) {
		$data ['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > Shift/Roster > <a href='".site_url('shiftcategory')."'>Shift Category</a> > Edit";
		$shift_category = $this->shift_category_m->get($id);
		$data['shift_category'] = $shift_category;
		$this->load->view ( 'navigation', $data );
		$this->load->view ( 'shiftcategory/edit_shiftcategory' );
	}
	
	/**
	 * process add shift category
	 */
	function do_edit() {
		$this->form_validation->set_rules ( 'code', 'Code', 'trim|required' );
		$this->form_validation->set_rules ( 'description', 'Description', 'trim|required' );
		$this->form_validation->set_rules ( 'color', 'Color', 'trim|required' );
		$id = $this->input->post ( 'id' );
		if ($this->form_validation->run () == FALSE) {
			$this->edit($id);
		} else {
			
			
			$code = $this->input->post ( 'code' );
			$description = $this->input->post ( 'description' );
			$color = $this->input->post ( 'color' );
			$total = $this->input->post ( 'radio' );
			
			//check unique
			
			$shift_category = array(
				'code' 			=> $code,
				'description' 	=> $description,
				'color'			=> $color,
				'total'			=> $total
			);
			$this->shift_category_m->update ($id, $shift_category);
			$this->index ();
		}
	}
	
	/**
	 * check whether shift category code is unique 
	 * @return json
	 */
	function check_code_ajax(){
		$code = $this->input->post('code');
        $id = $this->input->post('id');
        $id = empty($id)?0:$id;
        $num = $this->shift_category_m->check_except($code, $id);
        $result['status'] = "ok";
        if($num > 0){
        	$result['status'] = "exist";
        	$result['message'] = "The code has existed.";	
        }
        
        echo json_encode($result);
	}
	
	function remove($id) {
		$this->shift_category_m->delete ( $id );
		$this->index ();
	}
	
	function _get_total_dropdown(){
		$option = array(
			''
		);
	}
}
