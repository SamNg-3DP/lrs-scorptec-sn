<?php

/**
 * User's own info 
 * @author Bai Yinbing
 */
class Userinfo extends CI_Controller {
	function Userinfo() {
        parent::__construct();
    	if(!$this->userauth->logined()){
			$infoMsg = array(
				'info' => 'Please login!'
			);
			$this->session->set_flashdata('auth', $this->load->view('msgbox/info', $infoMsg , True));
			redirect("login");
		}else{
			if(!$this->userauth->judge_priv("userinfo")){
				$errorMsg = array(
					'error' => 'You have no privilege into this page !'
				);
				$this->session->set_flashdata('auth', $this->load->view('msgbox/error', $errorMsg , True));
				redirect("msgbox", 'location');
			}
		}
        $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
        $this->load->model('St_users_m', 'users_m');
    }
    
	function index() {
        $data['navigation'] = "My Account";
        $data['user'] = $this->users_m->get_by_user_name($this->session->userdata('user_name'));
        
        $this->load->view('navigation', $data);
        $this->load->view('user/view_userinfo', $data);
        //$this->output->cache(1);
    }
    
	function detail($user_name) {
        $data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='". site_url('user') ."'>User</a> > User detail info ".$user_name;
        $data['user'] = $this->users_m->get_by_user_name($user_name);
        
        $this->load->view('navigation', $data);
        $this->load->view('user/view_userinfo', $data);
        //$this->output->cache(1);
    }
    
	function _get_user_types(){
    	$options = array(
                  '1'  => 'Full Time',
                  '2'  => 'Part Time',
                  '3'  => 'Casual/Trial'
                );
        return $options;
    }
}