<?php

class Userlevel extends CI_Controller {

	function Userlevel() {
		parent::__construct();
		if(!$this->userauth->logined()){
			$infoMsg = array(
				'info' => 'You have no privilege into this page !'
			);
			$this->session->set_flashdata('auth', $this->load->view('msgbox/info', $infoMsg , True));
			redirect("login");
		}else{
			if(!$this->userauth->judge_priv("userlevel")){
				$errorMsg = array(
					'error' => 'You have no privilege into this page !'
				);
				$this->session->set_flashdata('auth', $this->load->view('msgbox/error', $errorMsg , True));
				redirect("msgbox", 'location');
			}
		}
		$this->load->model('St_user_level_m', 'user_level_m');
	}
	
	function index() {

		$data['user_level'] = $this->user_level_m->get_user_levels();
        $data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('userlevel')."'>User Level</a> > List";
        $this->load->view('navigation', $data);
        $this->load->view('admin/userlevel', $data);
	}

    function index_add() {
        $data['status'] = '';
        $data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('userlevel')."'>User Level</a> > Add New";
        $this->load->view('navigation', $data);
        $this->load->view('admin/userlevel_addnew', $data);
    }

    function index_edit() {
		$data['user_level'] = $this->user_level_m->get_user_level_single($this->uri->segment(3));
        $data['status'] = '';
        $data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('userlevel')."'>User Level</a> > Edit";
        $this->load->view('navigation', $data);
        $this->load->view('admin/userlevel_edit', $data);
    }

    function add()
	{
		if ($this->input->post('submit'))
		{
			$user_level = $this->input->post('user_level');
			$status = $this->input->post('status');

			if (empty($user_level)) {
				$data['status'] = 'The Userlevel is NULL!';
                $data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('userlevel')."'>User Level</a> > Add New";
                $this->load->view('navigation', $data);
                $this->load->view('admin/userlevel_addnew', $data);
			}
			else {
				$query = $this->db->query("SELECT COUNT(*) as num FROM user_level WHERE user_level = '$user_level';");
				$array = $query->result();
				foreach ($array as $row):
					$num = $row->num;
					break;
				endforeach;

				$data['status'] = 'The Userlevel already exists!';

				if ($num == 0) {
					$query = $this->db->query("INSERT INTO user_level(user_level, status) VALUES('$user_level', '$status');");
                    $data['user_level'] = $this->user_level_m->get_user_levels();
                    $data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('userlevel')."'>User Level</a> > List";
                    $this->load->view('navigation', $data);
                    $this->load->view('admin/userlevel', $data);
				}
                else {
                    $data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('userlevel')."'>User Level</a> > Add New";
                    $this->load->view('navigation', $data);
                    $this->load->view('admin/userlevel_addnew', $data);
                }
			}
            
		}
	}
	
	function change_status()
	{
		$user_level_id = $this->uri->segment(3);
		$status = trim($this->uri->segment(4));
		
		if ($status == ACTIVE) {
			$status = IN_ACTIVE;
		}
		else {
			$status = ACTIVE;
		}
		
		$query = $this->db->query("UPDATE user_level SET status = '$status' WHERE id = $user_level_id;");
		
		$data['user_level'] = $this->user_level_m->get_user_levels();
        $data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('userlevel')."'>User Level</a> > List";
        $this->load->view('navigation', $data);
		$this->load->view('admin/userlevel', $data);
	}
	
	function edit() {
		if ($this->input->post('submit')) {
            $user_levelid = $this->input->post('id');
			$user_level_name = $this->input->post('user_level');
			$status = $this->input->post('status');

			if (empty($user_level_name)) {
				$data['status'] = 'The Userlevel name is NULL!';
                $data['user_level'] = $this->user_level_m->get_user_level_single($user_levelid);
                $data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('userlevel')."'>User Level</a> > Edit";
                $this->load->view('navigation', $data);
                $this->load->view('admin/userlevel_edit', $data);
			}
			else {
				$query = $this->db->query("SELECT COUNT(*) as num FROM st_user_level WHERE user_level = '$user_level_name'
                       AND user_level <> (SELECT user_level FROM st_user_level WHERE id = $user_levelid);");
				$array = $query->result();
				foreach ($array as $row):
					$num = $row->num;
					break;
				endforeach;

				$data['status'] = 'The Userlevel already exists!';

				if ($num == 0) {
					$query = $this->db->query("UPDATE st_user_level SET user_level = '$user_level_name', status = '$status' WHERE id = $user_levelid;");
                    $data['user_level'] = $this->user_level_m->get_user_levels();
                    $data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('userlevel')."'>User Level</a> > List";
                    $this->load->view('navigation', $data);
                    $this->load->view('admin/userlevel', $data);
				}
                else {
                    $data['user_level'] = $this->user_level_m->get_user_level_single($user_levelid);
                    $data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('userlevel')."'>User Level</a> > Edit";
                    $this->load->view('navigation', $data);
                    $this->load->view('admin/userlevel_edit', $data);
                }
			}
		}
	}
	
	function remove()
	{
		$user_levelid = $this->uri->segment(3);
		$query = $this->db->query("DELETE FROM user_level WHERE id = '$user_levelid';");
		
		$data['user_level'] = $this->user_level_m->get_user_levels();
        $data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('userlevel')."'>User Level</a> > List";
        $this->load->view('navigation', $data);
		$this->load->view('admin/userlevel', $data);
	}
}