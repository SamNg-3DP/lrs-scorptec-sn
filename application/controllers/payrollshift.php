<?php
/**
 * payroll shift
 */
class Payrollshift extends CI_Controller {
	
	function Payrollshift() {
		parent::__construct();
		if(!$this->userauth->logined()){
			$infoMsg = array(
				'info' => 'You have no privilege into this page !'
			);
			$this->session->set_flashdata('auth', $this->load->view('msgbox/info', $infoMsg , True));
			redirect("login");
		}else{
			if(!$this->userauth->judge_priv("payrollshift")){
				$errorMsg = array(
					'error' => 'You have no privilege into this page !'
				);
				$this->session->set_flashdata('auth', $this->load->view('msgbox/error', $errorMsg , True));
				redirect("msgbox", 'location');
			}
		}
		$this->form_validation->set_error_delimiters ( "<div style='font:Tahoma, Geneva, sans-serif; font-size:11px; color:#F00'>", "</div>" );
		//load St_payroll_m model
		$this->load->model('St_payrollshift_m', 'payrollshift_m');
	}
	
	function index() {
		$data['payrollshifts'] = $this->payrollshift_m->get_payrollshifts();
			
		$data ['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > Shift/Roster > <a href='".site_url('payroll')."'>Payroll Shift</a> > View";
		$this->load->view ( 'navigation', $data );
		$this->load->view ( 'payrollshift/list_payrollshifts', $data );
	}
	
	function add() {
		$data ['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > Shift/Roster > <a href='".site_url('payrollshift')."'>Payroll Shift</a> > Add New";
		$this->load->model('St_site_m', 'site_m');
		$data['sites'] = $this->site_m->get_select_dropdown();
		$data['paydayses'] = $this->_get_paydays_dropdown();
		$data['rolldayses']= $this->_get_rolldays_dropdown();
		$this->load->view ( 'navigation', $data );
		$this->load->view ( 'payrollshift/add_payrollshift' );
	}
	
	function do_add() {
		$this->form_validation->set_rules ( 'site_id', 'Site', 'required' );
		$this->form_validation->set_rules ( 'payrollshiftcode', 'Payroll Shift Code', 'required' );
		$this->form_validation->set_rules ( 'description', 'Description', 'required' );
		
		if ($this->form_validation->run () == FALSE) {
			$this->add();
		} else {
			$site_id = $this->input->post ( 'site_id' );
			$payrollshiftcode = $this->input->post ( 'payrollshiftcode' );
			$description = $this->input->post ( 'description' );
			$notes = $this->input->post ( 'notes' );
			$paydays = $this->input->post ( 'paydays' );
			$start_date = $this->input->post ( 'start_day' );
			$rolldays = $this->input->post ( 'rolldays' );
			
			$dnow = now();
			$payrollshift = array(
				'site_id' => $site_id,
				'payrollshiftcode' => $payrollshiftcode,
				'description' => $description,
				'notes' => $notes,
				'paydays' => $paydays,
				'start_date' => strtotime($start_date),
				'rolldays' => $rolldays
			);
			
			$insert_id = $this->payrollshift_m->insert($payrollshift);
			
			
			if($rolldays > 0 && $insert_id > 0){
				$this->load->model('St_payrollshift_rolls_m', 'payrollshift_rolls_m');
				for($i=0; $i < $rolldays; $i++){
					$start = 'start_'.$i;
					$name = 'name_'.$i;
					$shift = 'shift_'.$i;
					$roll = array(
						'payrollshift_id' => $insert_id,
						'start' => $this->input->post( $start ),
						'name' => $this->input->post( $name ),
						'daily_shift_id' => $this->input->post( $shift ),
						'add_time' => $dnow
					);
					$this->payrollshift_rolls_m->insert($roll);
				}
			}
			
			$this->index ();
		}
	}
	
	function edit($id){
		$data ['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > Shift/Roster > <a href='".site_url('payrollshift')."'>Payroll Shift</a> > Edit";
		$data['paydayses'] = $this->_get_paydays_dropdown();
		$data['rolldayses']= $this->_get_rolldays_dropdown();
		$this->load->model('St_site_m', 'site_m');
		$data['sites'] = $this->site_m->get_select_dropdown();
		$payrollshift = $this->payrollshift_m->get($id);
		$data['payrollshift'] = $payrollshift;
		$this->load->model('St_payrollshift_rolls_m', 'payrollshift_rolls_m');
		$data['rolls'] = $this->payrollshift_rolls_m->get_by_payrollshift($id);
		$this->load->model('St_daily_shift_m', 'daily_shift_m');
        $data['daily_shifts'] = $this->daily_shift_m->get_select_dropdown($payrollshift->site_id);
		$this->load->view ( 'navigation', $data );		
		$this->load->view ( 'payrollshift/edit_payrollshift', $data);
	}
	
	function do_edit() {
		$this->form_validation->set_rules ( 'payrollshiftcode', 'Payroll Code', 'required' );
		$this->form_validation->set_rules ( 'description', 'Description', 'required' );
		
		if ($this->form_validation->run () == FALSE) {
			$this->add();
		} else {
			$site_id = $this->input->post ( 'site_id' );
			$payrollshiftcode = $this->input->post ( 'payrollshiftcode' );
			$description = $this->input->post ( 'description' );
			$notes = $this->input->post ( 'notes' );
			$paydays = $this->input->post ( 'paydays' );
			$start_date = $this->input->post ( 'start_day' );
			$rolldays = $this->input->post ( 'rolldays' );
			$payrollshift_id = $this->input->post ( 'payrollshift_id' );
			$dnow = now();
			$payrollshift = array(
				'site_id' => $site_id,
				'payrollshiftcode' => $payrollshiftcode,
				'description' => $description,
				'notes' => $notes,
				'paydays' => $paydays,
				'start_date' => strtotime($start_date),
				'rolldays' => $rolldays
			);
			
			$insert_id = $this->payrollshift_m->update($payrollshift_id, $payrollshift);
			
			if($rolldays > 0 && $insert_id){
				$this->load->model('St_payrollshift_rolls_m', 'payrollshift_rolls_m');
				$this->payrollshift_rolls_m->delete_by(array('payrollshift_id'=>$payrollshift_id));
				for($i=0; $i < $rolldays; $i++){
					$start = 'start_'.$i;
					$name = 'name_'.$i;
					$shift = 'shift_'.$i;
					$roll = array(
						'payrollshift_id' => $payrollshift_id,
						'start' => $this->input->post( $start),
						'name' => $this->input->post( $name),
						'daily_shift_id' => $this->input->post( $shift ),
						'add_time' => $dnow
					);
					$this->payrollshift_rolls_m->insert($roll);
				}
			}
			
			$this->index ();
		}
	}
	
	function view($id){
		$data ['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > Shift/Roster > <a href='".site_url('payrollshift')."'>Payroll Shift</a> > Edit";
		$payrollshift = $this->payrollshift_m->get($id);
		$data['payrollshift'] = $payrollshift;
		$this->load->model('St_site_m', 'site_m');
		$data['sites'] = $this->site_m->get_select_dropdown();
		$this->load->model('St_payrollshift_rolls_m', 'payrollshift_rolls_m');
		$data['rolls'] = $this->payrollshift_rolls_m->get_by_payrollshift($id);
		$this->load->model('St_daily_shift_m', 'daily_shift_m');
        $data['daily_shifts'] = $this->daily_shift_m->get_select_dropdown($payrollshift->site_id);
		$this->load->view ( 'navigation', $data );
		$this->load->view ( 'payrollshift/view_payrollshift', $data);
	}
	
	function remove($payrollshift_id) {
		$this->payrollshift_m->delete ( $payrollshift_id );
		$this->index ();
	}
	
	function get_rolldays_ajax(){
		$start_day = $this->input->post('start_day');
        $rolldays = $this->input->post('rolldays');
        $site = $this->input->post('site');
        $data['start_day'] = $start_day;
        $data['rolldays'] = $rolldays;
        $data['weekday'] = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
        
        $this->load->model('St_daily_shift_m', 'daily_shift_m');
        $data['daily_shifts'] = $this->daily_shift_m->get_select_dropdown($site);
        $str = $this->load->view('payrollshift/list_roll_data', $data, true);
        $result = array(
        	"content" 	=> $str
        );
        
        log_message("debug", "get_rolldays_ajax....end");
        echo json_encode($result);
	}

	/**
     * get departments by site id
     * @return json
     */
    function get_payrollshifts_ajax(){
    	$site_id = $this->input->post('site_id');
    	$select = $this->input->post('select');
    	$result = array();
    	$index = 0;
	    $result[$index]['id'] = "";
	    if($select){
	    	$result[$index]['code'] = "Please select..";
	    }else{
	    	$result[$index]['code'] = "All dept.";
	    }
    	if(!empty($site_id)){
	    	$payrollshifts = $this->payrollshift_m->get_by_site($site_id);
	    	foreach ($payrollshifts as $row){
	    		$index++;
	    		$result[$index]['id'] = $row->id;
	    		$result[$index]['code'] = $row->payrollshiftcode;
	    	}
    	}
    	echo json_encode($result);
    }

	/**
	 * check whether payrollshift code is unique 
	 * @return json
	 */
	function check_code_ajax(){
		$payrollshiftcode = $this->input->post('payrollshiftcode');
        $id = $this->input->post('id');
        $id = empty($id)?0:$id;
        $num = $this->payrollshift_m->check_except($payrollshiftcode, $id);
        $result['status'] = "ok";
        if($num > 0){
        	$result['status'] = "exist";
        	$result['message'] = "The code has existed.";	
        }
        
        echo json_encode($result);
	}
	
	/*
	 * pay days select dropdown
	 */
	function _get_paydays_dropdown(){
    	$options = array(
    		''  => "Please select",
    		'7' => "Every Week",
    		'14' => "2 Weeks",
    		'28' => "4 Weeks"
    	);
    	return $options;
    }
    
    /*
     * roll days select dropdown
     */
    function _get_rolldays_dropdown(){
    	$options = array(
    		''  => "Please select",
    		'7' => "Every Week",
    		'14' => "2 Weeks",
    		'28' => "4 Weeks"
    	);
    	return $options;
    }
    
    
}
