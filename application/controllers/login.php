<?php

/**
 * login controller
 * @author Bai Yinbing
 */
class Login extends CI_Controller {

	function Login()
	{
		parent::__construct();
	}
	
	function index()
	{
		$svn_update_time = APPPATH . "svn_update_time.txt";		
		$file_pointer = fopen($svn_update_time, "r"); 
		$file_read = fread($file_pointer, filesize($svn_update_time));
		fclose($file_pointer); 
		$data['last_svn_update'] = $file_read;
		$this->load->view('login', $data);
	}
	
	function submit(){
		// Load validation rules & fields (used multiple times)
		// Validation rules
		$this->form_validation->set_rules('user_name', 'Username', 'required|max_length[20]|min_length[2]');
		$this->form_validation->set_rules('password', 'Password', 'required|max_length[20]|min_length[3]');

		// Set the error delims to a nice styled red box
		$this->form_validation->set_error_delimiters('<p class="error">', '</p>');
		
		// Run validation
	    if ($this->form_validation->run() == FALSE){
	      	// Validation failed, load login page again
			$this->load->view('login');
	    } else {
			// Form validation for length etc. passed, now see if the credentials are OK in the DB
			// Post values 
			$user_name = $this->input->post('user_name');
			$password = $this->input->post('password');
			$this->session->set_flashdata('back_url', $this->input->post('back_url'));
			
			// Now see if we can login
			if($this->userauth->trylogin($user_name, $password)){
				$this->load->model('St_user_log_m', 'user_log_m');
				
				$a_user_log = array(
					'user_name' => $user_name,
					'log_info'  => 'login'
				);
				
				$this->user_log_m->add($a_user_log);
				
				// Success! Redirect to admin panel
				$infoMsg = array(
					'info' => 'Login Suceesfull.'
				);
				$this->session->set_flashdata('auth', $this->load->view('msgbox/info', $infoMsg , True));
				
   				if ( $this->input->post('back_url') ){
					redirect($this->input->post('back_url'), 'location');
  				}else{
					redirect('admin/leave', 'location');
  				}
			} else {
		  		$errorMsg = array(
					'error' => 'Incorrect username and/or password.'
				);
				
				$this->session->set_flashdata('auth', $this->load->view('msgbox/error', $errorMsg , True));
		  		redirect('login');
			}
	  	}
	}
	
	/**
	 * casual function
	 * @return 
	 */
	function svn(){
		$svn_update_time = APPPATH . "svn_update_time.txt";
		$fh = fopen($svn_update_time, 'w') or die("can't open file");
		date_default_timezone_set("Australia/ACT"); //set timezone
		$stringData = "" . date(DATETIME_FORMAT, now());
		fwrite($fh, $stringData);		
		fclose($fh);
		$file_pointer = fopen($svn_update_time, "r"); 
		$file_read = fread($file_pointer, filesize($svn_update_time));
		fclose($file_pointer); 
		$data['last_svn_update'] = $file_read;
		$this->load->view('svn', $data);
		
	}
}
