<?php

class Request extends CI_Controller {

    function Request() {
        parent::__construct();
    	if(!$this->userauth->logined()){
			$infoMsg = array(
				'info' => 'Please login !'
			);
			$this->session->set_flashdata('auth', $this->load->view('msgbox/info', $infoMsg , True));
			$this->session->set_flashdata('back_url', current_url());
			redirect("login");
		}else{
			if(!$this->userauth->judge_priv("request")){
				$errorMsg = array(
					'error' => 'You have no privilege into this page !'
				);
				$this->session->set_flashdata('auth', $this->load->view('msgbox/error', $errorMsg , True));
				redirect("msgbox", 'location');
			}
		}
		
		$this->form_validation->set_error_delimiters('<p class="error">', '</p>');
		
		//load request model
		$this->load->model('St_request_m', 'request_m');
    }

    function index() {
    	$user_name = $this->session->userdata('user_name');
    	$user_id = $this->session->userdata('user_id');
    	$now_time = now();
    	$now_day = date(DATE_FORMAT, $now_time);
    	/*
    	//calculate total annual/personal leave balance
    	$this->load->model('St_working_hours_m', 'working_hours_m');    	
    	$total_working_stat = $this->working_hours_m->get_staff_working_hours($user_name);
    	
    	$work_count = $total_working_stat->work_count;
//    	$total_annual = round($work_count / 13, 1);
//    	$total_personal = round($work_count / 26, 1);
		$total_annual = $total_working_stat->annual_hours_count;
		$total_personal = $total_working_stat->personal_hours_count;
		
    	
    	$annual_used_count = $total_working_stat->annual_used_count;
    	$personal_used_count = $total_working_stat->personal_used_count;
    	*/
    	$this->load->model('St_payroll_data_m', 'payroll_data_m');
    	$this->load->library('workinghour');
    	$this->load->model('st_config_m', 'config_m');
    	 
		$payroll_data = $this->payroll_data_m->get_last_by_user($user_id);
		$last_annual_leave_end = 0;
		$last_personal_leave_end = 0;
		$last_inlieu_end = 0;
		
       	if($payroll_data){
    		$this->load->model('St_payroll_m', 'payroll_m');    		
    		
    		$last_payroll = $this->payroll_m->get_last();
	
	    	$annual_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_ANNUAL, true, false, $last_payroll->end_date);
//	    	$annual_upcoming = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_ANNUAL, true, false);
	    	$annual_upcoming_used = IS_NULL($annual_upcoming_used)?0:$annual_upcoming_used;
    	
    		$personal_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, true, false, $last_payroll->end_date);
//    		$personal_upcoming = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, true, false);
    		$personal_upcoming_used = IS_NULL($personal_upcoming_used)?0:$personal_upcoming_used;
    		
//    		$inlieu_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_INLIEU, true, false, $last_payroll->end_date);

    		
    		$days_payroll = $this->config_m->get_by_code("Days_Payroll_Parameter")->value;
    		$next_payroll_start_date = date_add('d', $days_payroll, date(DATE_FORMAT, $last_payroll->end_date));
    		$inlieu_upcoming_used = $this->workinghour->calculate_leave_used($user_id, LEAVE_TYPE_INLIEU, $last_payroll->end_date, $next_payroll_start_date);
    		$inlieu_upcoming_used += $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_INLIEU, true, false, $next_payroll_start_date);    		
    		$inlieu_upcoming_used = IS_NULL($inlieu_upcoming_used)?0:$inlieu_upcoming_used;    		
    		
			$last_annual_leave_end = $payroll_data->annual_leave_end;
			$last_personal_leave_end = $payroll_data->personal_leave_end;
			$last_inlieu_end = $payroll_data->inlieu_end;
		}else{
			$this->load->model('St_working_hours_m', 'working_hours_m');
			$total_working_stat = $this->working_hours_m->get_staff_working_hours($user_name);
			
	    	$annual_upcoming_used = $annual_upcoming = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_ANNUAL, true);
	    	$annual_upcoming_used = IS_NULL($annual_upcoming_used)?0:$annual_upcoming_used;
    	
    		$personal_upcoming_used = $personal_upcoming = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, true);
    		$personal_upcoming_used = IS_NULL($personal_upcoming_used)?0:$personal_upcoming_used;
    		
    		$inlieu_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_INLIEU, true);
    		$inlieu_upcoming_used = IS_NULL($inlieu_upcoming_used)?0:$inlieu_upcoming_used;
			
			$last_annual_leave_end = get_number($total_working_stat->annual_hours_count);
			$last_personal_leave_end = get_number($total_working_stat->personal_hours_count);
		}
		
		$annual_available = round($last_annual_leave_end, 2);
		$personal_available = round($last_personal_leave_end, 2);

    	//calculate the annual leave balance
    	$data['annual_upcoming'] = round($annual_upcoming_used, 2);
    	$data['annual_available'] = $annual_available ;
    	$data['total_annual'] = round($annual_available - $annual_upcoming_used, 2);
    	
    	$data['total_inlieu'] = round($last_inlieu_end - $inlieu_upcoming_used, 2);
    	
    	$data['personal_upcoming'] = round($personal_upcoming_used, 2);
    	$data['personal_available'] = $personal_available ;
    	$data['total_personal'] = round($personal_available - $personal_upcoming_used, 2);
    	
    	$no_mc_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, false, true);
    	
    	$max_no_mc = $this->workinghour->calculate_max_no_mc($user_id);
    	$data['max_no_mc'] = $max_no_mc;
    	 
    	$data['no_mc_leave_left'] = $max_no_mc - $no_mc_used;
    	
    	
    	//get last 7 days
    	$late_7_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LATE, date_add('d', -7, $now_day), $now_time);
    	$data['late_7_days'] = $late_7_stat->daycount;
    	$data['late_7_times'] = IS_NULL($late_7_stat->timecount)?0:round($late_7_stat->timecount * 60, 2);
    	
    	$late_30_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LATE, date_add('d', -30, $now_day), $now_time);
    	$data['late_30_days'] = $late_30_stat->daycount;
    	$data['late_30_times'] = IS_NULL($late_30_stat->timecount)?0:round($late_30_stat->timecount * 60, 2);
    	
    	$late_all_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LATE);
    	$data['late_all_days'] = $late_all_stat->daycount;
    	$data['late_all_times'] = IS_NULL($late_all_stat->timecount)?0:round($late_all_stat->timecount * 60, 2);
    	
    	$lunch_7_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LUNCH, date_add('d', -7, $now_day), $now_time);
    	$data['lunch_7_days'] = $lunch_7_stat->daycount;
    	$data['lunch_7_times'] = IS_NULL($lunch_7_stat->timecount)?0:round($lunch_7_stat->timecount * 60, 2);
    	
    	$lunch_30_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LUNCH, date_add('d', -30, $now_day), $now_time);
    	$data['lunch_30_days'] = $lunch_30_stat->daycount;
    	$data['lunch_30_times'] = IS_NULL($lunch_30_stat->timecount)?0:round($lunch_30_stat->timecount * 60, 2);

    	$lunch_all_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LUNCH);
    	$data['lunch_all_days'] = $lunch_all_stat->daycount;
    	$data['lunch_all_times'] = IS_NULL($lunch_all_stat->timecount)?0:round($lunch_all_stat->timecount * 60, 2);
    	
    	//read each leave request state count
    	$request_counts = $this->request_m->get_requests_counts($user_name);
    	
    	$new_count = 0;
    	$not_approved_count = 0;
    	$approved_count = 0;
    	$pending_count = 0;
    	foreach ($request_counts as $row){
    		if($row->state == 10){
    			$new_count = $row->icount;
    		}else if($row->state == 20 || $row->state == 30){
    			$pending_count += $row->icount;
    		}else if($row->state == 40){
    			$approved_count = $row->icount;
    		}else if($row->state == 50){
    			$not_approved_count = $row->icount;
    		}
    	}
    	    	
		$data['new_count'] = $new_count;
		$data['rejected_count'] = $not_approved_count;
		$data['approved_count'] = $approved_count;
		$data['waiting_count'] = $pending_count;
		
		$upcoming_count = $this->request_m->count_upcoming_approved_requests($user_name); 
		
		$data['upcoming_count'] = $upcoming_count;
		//$approved_counts = $this->request_m->get_past_approved_counts($this->session->userdata('department'));
		$approved_counts = $this->request_m->get_past_approved_requests_counts($user_name);
    	$past_annual_count = 0;
    	$past_personal_count = 0;
    	$past_late_count = 0;
    	$past_lunch_count = 0;
    	$past_training_count = 0;
    	$past_client_count = 0;
    	$past_overtime_count = 0;
    	$past_unpaid_count = 0;
    	$past_clock_count = 0;
    	$past_inlieu_count = 0;
    	foreach ($approved_counts as $row){
    		if($row->leave_type_id == LEAVE_TYPE_ANNUAL){
    			$past_annual_count = $row->icount;
    		}else if($row->leave_type_id == LEAVE_TYPE_PERSONAL){
    			$past_personal_count = $row->icount;
    		}else if($row->leave_type_id == LEAVE_TYPE_LATE){
    			$past_late_count = $row->icount;
    		}else if($row->leave_type_id == LEAVE_TYPE_LUNCH){
    			$past_lunch_count = $row->icount;
    		}else if($row->leave_type_id == LEAVE_TYPE_TRAINING){
    			$past_training_count = $row->icount;
    		}else if($row->leave_type_id == LEAVE_TYPE_CLIENTVISIT){
    			$past_client_count = $row->icount;
    		}else if($row->leave_type_id == LEAVE_TYPE_OVERTIME){
    			$past_overtime_count = $row->icount;
    		}else if($row->leave_type_id == LEAVE_TYPE_UNPAID){
    			$past_unpaid_count = $row->icount;
    		}else if($row->leave_type_id == LEAVE_TYPE_CLOCK){
    			$past_clock_count = $row->icount;
    		}else if($row->leave_type_id == LEAVE_TYPE_INLIEU){
    			$past_inlieu_count = $row->icount;
    		}
    	}
    	
		$data['past_annual_count'] = $past_annual_count;
		$data['past_personal_count'] = $past_personal_count;
		$data['past_late_count'] = $past_late_count;
		$data['past_lunch_count'] = $past_lunch_count;
		$data['past_training_count'] = $past_training_count;
		$data['past_client_count'] = $past_client_count;
		$data['past_overtime_count'] = $past_overtime_count;
		$data['past_unpaid_count'] = $past_unpaid_count;
		$data['past_clock_count'] = $past_clock_count;
		$data['past_inlieu_count'] = $past_inlieu_count;
		
        $data['navigation'] = "<a href='". site_url('admin/leave') ."' target='_top'>Leave Request</a> > <a href='". site_url('request') ."'>Leave Statement</a> > View";
        
        $this->load->view('navigation', $data);
        
        $this->load->view('request/request');
    }

    /**
     * list request which are saved, but not apply
     */
    function saved(){
    	log_message("debug", "request saved.....");
    	$data['state_name'] = "New"; 
    	$data['form_id'] = 'saved';
    	
		$data['requests'] = $this->request_m->get_saved_requests($this->session->userdata('user_name'));
		$total_count = $this->request_m->count_saved_requests($this->session->userdata('user_name'));
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	
    	$this->load->view('request/list_requests', $data);
    }
    
	function saved_ajax(){
    	log_message("debug", "request saved ajax .....");
		
		$data['state_name'] = "New"; 
    	$data['form_id'] = 'saved';
    	
    	$order_by = $this->input->post('saved_order_by');
        
        $order = $this->input->post('saved_order');
    	$department_id = $this->input->post('saved_filter_department_id');
    	$leave_type_id = $this->input->post('saved_filter_leave_type_id');
    	$time_period_id = $this->input->post('saved_filter_time_period_id');
    	$page_no = $this->input->post('saved_page_no');
        
		$limit = $this->input->post('saved_limit');
    	$data['page_limit'] = $limit;
    	
		$data['requests'] = $this->request_m->get_saved_requests($this->session->userdata('user_name'), $leave_type_id, 0,0, $order_by, $order, $page_no, $limit);
		
		$total_count = $this->request_m->count_saved_requests($this->session->userdata('user_name'), $leave_type_id);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
		//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
		
		log_message("debug", "saved ajax request/list_requests_data....");
		
		$content = $this->load->view('request/list_requests_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }
    
    /**
     * list requests which are waiting approval
     */
    function waiting($user_name=false){
    	log_message("debug", "request saved.....");
    	$data['state_name'] = "Waiting Approval"; 
    	$data['form_id'] = 'waiting';
    	
    	if (!$user_name)
    		$user_name = $this->session->userdata('user_name');
    	
		$data['requests'] = $this->request_m->get_waiting_requests($user_name);
		$total_count = $this->request_m->count_waiting_requests($user_name);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	
    	$this->load->view('request/list_requests', $data);
    }
    
	function waiting_ajax(){
    	log_message("debug", "request waiting ajax .....");
		
		$data['state_name'] = "Waiting";
    	$data['form_id'] = 'waiting';
    	
    	$order_by = $this->input->post('waiting_order_by');
        
        $order = $this->input->post('waiting_order');
    	$department_id = $this->input->post('waiting_filter_department_id');
    	$leave_type_id = $this->input->post('waiting_filter_leave_type_id');
    	$time_period_id = $this->input->post('waiting_filter_time_period_id');
    	$page_no = $this->input->post('waiting_page_no');
        
		$limit = $this->input->post('waiting_limit');
		$data['page_limit'] = $limit;
    	
		$data['requests'] = $this->request_m->get_waiting_requests($this->session->userdata('user_name'), $leave_type_id, 0,0, $order_by, $order, $page_no, $limit);
		
		$total_count = $this->request_m->count_waiting_requests($this->session->userdata('user_name'), $leave_type_id);
		
		log_message("debug", "waiting_limit === " . $limit);
		log_message("debug", "waiting total_count === " . $total_count);
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	log_message("debug", "waiting page_count === " . $page_count);
    	
		//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
		
		log_message("debug", "saved ajax request/list_requests_data....");
		
		$content = $this->load->view('request/list_requests_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }
    
	/**
     * list requests which are waiting approval
     */
    function rejected(){
    	log_message("debug", "request rejected_ajax.....");
    	$data['state_name'] = "Rejected Approval"; 
    	$data['form_id'] = 'rejected';
    	
		$data['requests'] = $this->request_m->get_rejected_requests($this->session->userdata('user_name'));
		$total_count = $this->request_m->count_rejected_requests($this->session->userdata('user_name'));
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	
    	$this->load->view('request/list_requests', $data);
    }
    
	function rejected_ajax(){
    	log_message("debug", "request rejected_ajax  .....");
		
		$data['state_name'] = "Rejected Approval"; 
    	$data['form_id'] = 'rejected';
    	
    	$order_by = $this->input->post('rejected_order_by');
        
        $order = $this->input->post('rejected_order');
    	$department_id = $this->input->post('rejected_filter_department_id');
    	$leave_type_id = $this->input->post('rejected_filter_leave_type_id');
    	$time_period_id = $this->input->post('rejected_filter_time_period_id');
    	$page_no = $this->input->post('rejected_page_no');
        
		$limit = $this->input->post('rejected_limit');
    	$data['page_limit'] = $limit;
    	
		$data['requests'] = $this->request_m->get_rejected_requests($this->session->userdata('user_name'), $leave_type_id, 0,0, $order_by, $order, $page_no, $limit);
		
		$total_count = $this->request_m->count_rejected_requests($this->session->userdata('user_name'), $leave_type_id);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
		//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
		
		log_message("debug", "saved ajax request/list_requests_data....");
		
		$content = $this->load->view('request/list_requests_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }
    
	/**
     * list approved upcoming
     */
    function approved_upcoming($user_name=false){
    	log_message("debug", "request approved_upcoming.....");
    	$data['state_name'] = "Upcoming"; 
    	$data['form_id'] = 'upcoming';
    	
    	if (!$user_name)
    		$user_name = $this->session->userdata('user_name');
    	
		$data['requests'] = $this->request_m->upcoming_approved_requests($user_name);
		$total_count = $this->request_m->count_upcoming_approved_requests($user_name);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	
    	$this->load->view('request/list_requests', $data);
    }
    
	function approved_upcoming_ajax(){
    	log_message("debug", "request approved_upcoming_ajax .....");
		
		$data['state_name'] = "Upcoming"; 
    	$data['form_id'] = 'upcoming';
    	
//    	$order_by = $this->input->post('upcoming_order_by');
        
        $order = $this->input->post('upcoming_order');
    	$department_id = $this->input->post('upcoming_filter_department_id');
    	$leave_type_id = $this->input->post('upcoming_filter_leave_type_id');
    	$time_period_id = $this->input->post('upcoming_filter_time_period_id');
    	$page_no = $this->input->post('upcoming_page_no');
        
		$limit = $this->input->post('upcoming_limit');
    	$data['page_limit'] = $limit;
    	
		$data['requests'] = $this->request_m->upcoming_approved_requests($this->session->userdata('user_name'), 0,0, $order_by, $order, $page_no, $limit);
		
		$total_count = $this->request_m->count_upcoming_approved_requests($this->session->userdata('user_name'), $leave_type_id);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
		//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
		
		log_message("debug", "upcoming ajax request/list_requests_data....");
		
		$content = $this->load->view('request/list_requests_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }
    
	/**
     * list requests which are waiting approval
     */
    function approved_annual(){
    	log_message("debug", "request approved_annual.....");
    	$data['state_name'] = "Annual Leave"; 
    	$data['form_id'] = 'past_annual';
    	
		$data['requests'] = $this->request_m->past_approved_requests_annual($this->session->userdata('user_name'));
		$total_count = $this->request_m->count_past_approved_requests_annual($this->session->userdata('user_name'));
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	
    	$this->load->view('request/list_approved_requests', $data);
    }
    
	function approved_annual_ajax(){
    	log_message("debug", "request approved_annual_ajax  .....");
		
		$data['state_name'] = "Annual Leave"; 
    	$data['form_id'] = 'past_annual';
    	
    	$order_by = $this->input->post('past_annual_order_by');
        
        $order = $this->input->post('past_annual_order');
    	$department_id = $this->input->post('past_annual_filter_department_id');
    	$leave_type_id = $this->input->post('past_annual_filter_leave_type_id');
    	$time_period_id = $this->input->post('past_annual_filter_time_period_id');
    	$page_no = $this->input->post('past_annual_page_no');
        
		$limit = $this->input->post('past_annual_limit');
    	$data['page_limit'] = $limit;
    	
    	$time_period = get_time_period($time_period_id);
		$data['requests'] = $this->request_m->past_approved_requests_annual($this->session->userdata('user_name'), $time_period[0],$time_period[1], $order_by, $order, $page_no, $limit);
		
		$total_count = $this->request_m->count_past_approved_requests_annual($this->session->userdata('user_name'), $time_period[0],$time_period[1]);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
		//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
		
		log_message("debug", "saved ajax request/list_requests_data....");
		
		$content = $this->load->view('request/list_requests_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }
    
	/**
     * list requests which are waiting approval
     */
    function approved_personal(){
    	log_message("debug", "request approved_personal.....");
    	$data['state_name'] = "Personal Leave"; 
    	$data['form_id'] = 'past_personal';
    	
		$data['requests'] = $this->request_m->past_approved_requests_personal($this->session->userdata('user_name'));
		$total_count = $this->request_m->count_past_approved_requests_personal($this->session->userdata('user_name'));
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	
    	$this->load->view('request/list_approved_requests', $data);
    }
    
	function approved_personal_ajax(){
    	log_message("debug", "request approved_personal_ajax  .....");
		
		$data['state_name'] = "Personal Leave"; 
    	$data['form_id'] = 'past_personal';
    	
    	$order_by = $this->input->post('past_personal_order_by');
        
        $order = $this->input->post('past_personal_order');
    	$department_id = $this->input->post('past_personal_filter_department_id');
    	$leave_type_id = $this->input->post('past_personal_filter_leave_type_id');
    	$time_period_id = $this->input->post('past_personal_filter_time_period_id');
    	$page_no = $this->input->post('past_personal_page_no');
        
		$limit = $this->input->post('past_personal_limit');
    	$data['page_limit'] = $limit;
    	
    	$time_period = get_time_period($time_period_id);
		$data['requests'] = $this->request_m->past_approved_requests_personal($this->session->userdata('user_name'), $time_period[0], $time_period[1], $order_by, $order, $page_no, $limit);
		
		$total_count = $this->request_m->count_past_approved_requests_personal($this->session->userdata('user_name'),$time_period[0],$time_period[1]);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
		//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
		
		log_message("debug", "saved ajax request/list_requests_data....");
		
		$content = $this->load->view('request/list_requests_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }
    
	/**
     * list approved requests which leave type is late leave
     */
    function approved_late(){
    	log_message("debug", "request approved_late.....");
    	$data['state_name'] = "Late Leave"; 
    	$data['form_id'] = 'past_late';
    	
		$data['requests'] = $this->request_m->past_approved_requests_late($this->session->userdata('user_name'));
		$total_count = $this->request_m->count_past_approved_requests_late($this->session->userdata('user_name'));
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	
    	$this->load->view('request/list_approved_requests', $data);
    }
    
    
    function approved_lunch(){
    	$data['state_name'] = "Long Lunch"; 
    	$data['form_id'] = 'past_lunch';
    	
		$data['requests'] = $this->request_m->past_approved_requests_lunch($this->session->userdata('user_name'));
		$total_count = $this->request_m->count_past_approved_requests_lunch($this->session->userdata('user_name'));

    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	$this->load->view('request/list_approved_requests', $data);
    }
    
	function approved_lunch_ajax(){
    	log_message("debug", "request approved_late_ajax  .....");
		
		$data['state_name'] = "Long Lunch"; 
    	$data['form_id'] = 'long_lunch';
    	
    	$order_by = $this->input->post('long_lunch_order_by');
        
        $order = $this->input->post('long_lunch_order');
    	$department_id = $this->input->post('long_lunch_filter_department_id');
    	$leave_type_id = $this->input->post('long_lunch_filter_leave_type_id');
    	$time_period_id = $this->input->post('long_lunch_filter_time_period_id');
    	$page_no = $this->input->post('long_lunch_page_no');
        
		$limit = $this->input->post('long_lunch_limit');
    	$data['page_limit'] = $limit;
    	
    	$time_period = get_time_period($time_period_id);
		$data['requests'] = $this->request_m->past_approved_requests_lunch($this->session->userdata('user_name'), $time_period[0], $time_period[1], $order_by, $order, $page_no, $limit);
		
		$total_count = $this->request_m->count_past_approved_requests_lunch($this->session->userdata('user_name'), $time_period[0], $time_period[1]);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
		//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
		
		log_message("debug", "saved ajax request/list_requests_data....");
		
		$content = $this->load->view('request/list_requests_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }    
        
    
    function approved_clock(){
    	$data['state_name'] = "Clock In/Out"; 
    	$data['form_id'] = 'past_clock';
    	
		$data['requests'] = $this->request_m->past_approved_requests_clock($this->session->userdata('user_name'));
		$total_count = $this->request_m->count_past_approved_requests_clock($this->session->userdata('user_name'));
    	    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	$this->load->view('request/list_approved_requests', $data);
    }    
    
    
    
	function approved_late_ajax(){
    	log_message("debug", "request approved_late_ajax  .....");
		
		$data['state_name'] = "Late Leave"; 
    	$data['form_id'] = 'past_late';
    	
    	$order_by = $this->input->post('past_late_order_by');
        
        $order = $this->input->post('past_late_order');
    	$department_id = $this->input->post('past_late_filter_department_id');
    	$leave_type_id = $this->input->post('past_late_filter_leave_type_id');
    	$time_period_id = $this->input->post('past_late_filter_time_period_id');
    	$page_no = $this->input->post('past_late_page_no');
        
		$limit = $this->input->post('past_late_limit');
    	$data['page_limit'] = $limit;
    	
    	$time_period = get_time_period($time_period_id);
		$data['requests'] = $this->request_m->past_approved_requests_late($this->session->userdata('user_name'), $time_period[0], $time_period[1], $order_by, $order, $page_no, $limit);
		
		$total_count = $this->request_m->count_past_approved_requests_late($this->session->userdata('user_name'), $time_period[0], $time_period[1]);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
		//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
		
		log_message("debug", "saved ajax request/list_requests_data....");
		
		$content = $this->load->view('request/list_requests_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }
    
	/**
     * list approved requests which leave type is training leave
     */
    function approved_training(){
    	log_message("debug", "request approved_late.....");
    	$data['state_name'] = "Rraining/Seminar"; 
    	$data['form_id'] = 'past_training';
    	
		$data['requests'] = $this->request_m->past_approved_requests_training($this->session->userdata('user_name'));
		$total_count = $this->request_m->count_past_approved_requests_training($this->session->userdata('user_name'));
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	
    	$this->load->view('request/list_approved_requests', $data);
    }
    
	function approved_training_ajax(){
    	log_message("debug", "request approved_late_ajax  .....");
		
		$data['state_name'] = "Rraining/Seminar"; 
    	$data['form_id'] = 'past_training';
    	
    	$order_by = $this->input->post('past_training_order_by');
        
        $order = $this->input->post('past_training_order');
    	$department_id = $this->input->post('past_training_filter_department_id');
    	$leave_type_id = $this->input->post('past_training_filter_leave_type_id');
    	$time_period_id = $this->input->post('past_training_filter_time_period_id');
    	$page_no = $this->input->post('past_training_page_no');
        
		$limit = $this->input->post('past_training_limit');
    	$data['page_limit'] = $limit;
    	
    	$time_period = get_time_period($time_period_id);
		$data['requests'] = $this->request_m->past_approved_requests_training($this->session->userdata('user_name'), $time_period[0], $time_period[1], $order_by, $order, $page_no, $limit);
		
		$total_count = $this->request_m->count_past_approved_requests_training($this->session->userdata('user_name'), $time_period[0], $time_period[1]);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
		//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
		
		log_message("debug", "saved ajax request/list_requests_data....");
		
		$content = $this->load->view('request/list_requests_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }

	/**
     * list requests which are client approval
     */
    function approved_client(){
    	log_message("debug", "request approved_client.....");
    	$data['state_name'] = "Client Visit"; 
    	$data['form_id'] = 'past_client';
    	
		$data['requests'] = $this->request_m->past_approved_requests_client($this->session->userdata('user_name'));
		$total_count = $this->request_m->count_past_approved_requests_client($this->session->userdata('user_name'));
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	
    	$this->load->view('request/list_approved_requests', $data);
    }
    
	function approved_client_ajax(){
    	log_message("debug", "request approved_client_ajax  .....");
		
		$data['state_name'] = "Client Visit";
    	$data['form_id'] = 'past_client';
    	
    	$order_by = $this->input->post('past_client_order_by');
        
        $order = $this->input->post('past_client_order');
    	$department_id = $this->input->post('past_client_filter_department_id');
    	$leave_type_id = $this->input->post('past_client_filter_leave_type_id');
    	$time_period_id = $this->input->post('past_client_filter_time_period_id');
    	$page_no = $this->input->post('past_client_page_no');
        
		$limit = $this->input->post('past_client_limit');
    	$data['page_limit'] = $limit;
    	
    	$time_period = get_time_period($time_period_id);
		$data['requests'] = $this->request_m->past_approved_requests_client($this->session->userdata('user_name'), $time_period[0], $time_period[1], $order_by, $order, $page_no, $limit);
		
		$total_count = $this->request_m->count_past_approved_requests_client($this->session->userdata('user_name'),$time_period[0],$time_period[1]);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
		//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
		
		log_message("debug", "approved_client_ajax request/list_requests_data....");
		
		$content = $this->load->view('request/list_requests_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }
    
	/**
     * list requests which are client approval
     */
    function approved_overtime(){
    	log_message("debug", "request approved_overtime.....");
    	$data['state_name'] = "Overtime"; 
    	$data['form_id'] = 'past_overtime';
    	
		$data['requests'] = $this->request_m->past_approved_requests_overtime($this->session->userdata('user_name'));
		$total_count = $this->request_m->count_past_approved_requests_overtime($this->session->userdata('user_name'));
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	
    	$this->load->view('request/list_approved_requests', $data);
    }
    
	function approved_overtime_ajax(){
    	log_message("debug", "request approved_overtime_ajax  .....");
		
		$data['state_name'] = "Overtime"; 
    	$data['form_id'] = 'past_overtime';
    	
    	$order_by = $this->input->post('past_overtime_order_by');
        
        $order = $this->input->post('past_overtime_order');
    	$department_id = $this->input->post('past_overtime_filter_department_id');
    	$leave_type_id = $this->input->post('past_overtime_filter_leave_type_id');
    	$time_period_id = $this->input->post('past_overtime_filter_time_period_id');
    	$page_no = $this->input->post('past_overtime_page_no');
        
		$limit = $this->input->post('past_overtime_limit');
    	$data['page_limit'] = $limit;
    	
    	$time_period = get_time_period($time_period_id);
		$data['requests'] = $this->request_m->past_approved_requests_overtime($this->session->userdata('user_name'), $time_period[0], $time_period[1], $order_by, $order, $page_no, $limit);
		
		$total_count = $this->request_m->count_past_approved_requests_overtime($this->session->userdata('user_name'),$time_period[0],$time_period[1]);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
		//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
		
		log_message("debug", "saved ajax request/list_requests_data....");
		
		$content = $this->load->view('request/list_requests_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }
    
	/**
     * list requests which are client approval
     */
    function approved_unpaid(){
    	log_message("debug", "request approved_unpaid.....");
    	$data['state_name'] = "Unpaid"; 
    	$data['form_id'] = 'past_unpaid';
    	
		$data['requests'] = $this->request_m->past_approved_requests_unpaid($this->session->userdata('user_name'));
		$total_count = $this->request_m->count_past_approved_requests_unpaid($this->session->userdata('user_name'));
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	
    	$this->load->view('request/list_approved_requests', $data);
    }
    
	function approved_unpaid_ajax(){
    	log_message("debug", "request approved_unpaid_ajax  .....");
		
		$data['state_name'] = "Unpaid"; 
    	$data['form_id'] = 'past_unpaid';
    	
    	$order_by = $this->input->post('past_unpaid_order_by');
        
        $order = $this->input->post('past_unpaid_order');
    	$department_id = $this->input->post('past_unpaid_filter_department_id');
    	$leave_type_id = $this->input->post('past_unpaid_filter_leave_type_id');
    	$time_period_id = $this->input->post('past_unpaid_filter_time_period_id');
    	$page_no = $this->input->post('past_unpaid_page_no');
        
		$limit = $this->input->post('past_unpaid_limit');
    	$data['page_limit'] = $limit;
    	
    	$time_period = get_time_period($time_period_id);
		$data['requests'] = $this->request_m->past_approved_requests_unpaid($this->session->userdata('user_name'), $time_period[0], $time_period[1], $order_by, $order, $page_no, $limit);
		
		$total_count = $this->request_m->count_past_approved_requests_unpaid($this->session->userdata('user_name'),$time_period[0],$time_period[1]);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
		//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
		
		log_message("debug", "saved ajax request/list_requests_data....");
		
		$content = $this->load->view('request/list_requests_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }
    
    function approved_inlieu(){
    	$data['state_name'] = "In Lieu";
    	$data['form_id'] = 'past_inlieu';
    	 
    	$data['requests'] = $this->request_m->past_approved_requests_inlieu($this->session->userdata('user_name'));
    	$total_count = $this->request_m->count_past_approved_requests_inlieu($this->session->userdata('user_name'));
    	 
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
    
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	 
    	$this->load->view('request/list_approved_requests', $data);
    }    
    
    /**
     * list request which are approved
     */
    function approved(){
    	$data['requests'] = $this->request_m->get_approved_requests($this->session->userdata('user_name'));
    	$this->load->view('request/approved_requests', $data);
    }
    
    /**
     * list request which are not approved
     */
	function not_approved(){
		$data['requests'] = $this->request_m->get_not_approved_requests($this->session->userdata('user_name'));
    	$this->load->view('request/not_approved_requests', $data);
    }
    
    /**
     * list request which are pending approval
     */
	function pending(){
		$data['requests'] = $this->request_m->get_pending_requests($this->session->userdata('user_name'));
    	$this->load->view('request/pending_requests', $data);
    }
        
    /**
     * prepare to add a new leave request
     * @return 
     */
    function add() {
        $data['navigation'] = "<a href='". site_url('admin/leave') ."' target='_top'>Leave Request</a> > <a href='". site_url('request') ."'>Leave Statement</a> > Add a new leave request";
        $this->load->model('St_leave_type_m', 'leave_type_m');
        $this->load->model('St_users_m', 'users_m');
        $this->load->model('St_site_m', 'site_m');
        
        $data['site_id'] = $site_id = $this->session->userdata('site_id');
        
        $leave_types = $this->leave_type_m->get_select_dropdown();
        $data['leave_types'] = $leave_types;
        $stores = $this->site_m->get_select_dropdown();
        $data['stores'] = $stores;
        
        $data['user_level'] = $this->session->userdata('user_level');
        $data['user_id'] = $this->session->userdata('user_id');
        
        if ($data['user_level'] >= 2){
        	$user_dept = $this->session->userdata('department_id');
        	// Show all users for accounts user admin level
        	if ($data['user_level'] >= 4)
        		$users = $this->users_m->get_select_dropdown();
        	else
        		$users = $this->users_m->get_select_dropdown(false, $user_dept);
        	
	        $data['users'] = $users;
        }
        
        
        $this->load->view('navigation', $data);
        $this->load->view('request/add_request', $data);
    }
    
    function update_dept_ajax() {
    	$this->load->model('St_department_m', 'department_m');
    	
    	$site_id = $this->input->post('site_id');
    	
    	$depts = $this->department_m->get_select_dropdown($site_id);
    	
    	$js = 'id="dept_id" style="font-size:11px"';
    	echo form_dropdown('department_id', $depts, set_value('department_id'), $js);

    }

    /**
     * deal with add new a leave request
     * @return void
     */
	function do_add() {
        $this->form_validation->set_rules('leave_type', 'Leave Type', 'required');
    	$this->form_validation->set_rules('start_day', 'Start Day', '');
        $this->form_validation->set_rules('end_day', 'End Day', '');
//        $this->form_validation->set_rules('hours_used', 'Hours Used', '');
        $this->form_validation->set_rules('arrival_day', 'Arrival day', '');
        $this->form_validation->set_rules('arrival_time', 'Arrival time', '');
        $this->form_validation->set_rules('reason', 'reason', '');
        
        $this->load->model('St_users_m', 'users_m');
        
        $select_user = false;
        $login_user = false;
        if ($this->input->post('user_id')){
        	$select_user = $this->users_m->get_by_id($this->input->post('user_id'));
	        $user_name = $select_user->user_name;
	        $user_id = $select_user->id;
        }else{
	        $user_name = $this->session->userdata('user_name');
	        $user_id = $this->session->userdata('user_id');
	        $login_user = $this->users_m->get_by_id($user_id);
        }
        
        if ($this->form_validation->run() == FALSE) {
            $this->add();
        } else {
        	$error = "";
        	$leave_type = $this->input->post('leave_type');
        	
        	$operate = $this->input->post('operate');
            $start_day = $this->input->post('start_day');
            $end_day = $this->input->post('end_day');
            $start_time = $this->input->post('start_time');
            $end_time = $this->input->post('end_time');
            $hours_used = $this->input->post('hours_used');
            $reason = $this->input->post('reason');
            $mc_provided = $this->input->post('mc_provided');
            $carer_leave = $this->input->post('carer_leave');
            
            if ($leave_type == LEAVE_TYPE_CASUAL){
	            $site_id = $this->input->post('site_id');
	            $dept_id = $this->input->post('department_id');
            }elseif ($leave_type == LEAVE_TYPE_INTERSTORE){
            	$site_id = $this->input->post('site_id');
            }else{
            	$site_id = '';
            	$dept_id = '';
            }
            
            $request_type = $this->input->post('request_type');
            
            $mc_filename = "";
            
            if($leave_type == LEAVE_TYPE_LATE
            	|| $leave_type == LEAVE_TYPE_LUNCH
            	|| $leave_type == LEAVE_TYPE_CLOCK){
            	$start_day = $this->input->post('arrival_day');
            	$start_time = $this->input->post('arrival_time');
            	$hours_used = $this->input->post('minutes_used')/60;//The field is repaced with minutes here
            	$end_day = $this->input->post('arrival_day');
            	$end_time = "";
            	$request_type = 2;
            }else if($leave_type == LEAVE_TYPE_PERSONAL){
		        $fileElementName = 'medical_certificate';

		        if(empty($_FILES[$fileElementName]['tmp_name']) || $_FILES[$fileElementName]['tmp_name'] == 'none')
				{}else{
					if(!empty($_FILES[$fileElementName]['error']))
					{
						switch($_FILES[$fileElementName]['error'])
						{
							case '1':
								$error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
								break;
							case '2':
								$error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
								break;
							case '3':
								$error = 'The uploaded file was only partially uploaded';
								break;
							case '4':
								$error = 'No file was uploaded.';
								break;
				
							case '6':
								$error = 'Missing a temporary folder';
								break;
							case '7':
								$error = 'Failed to write file to disk';
								break;
							case '8':
								$error = 'File upload stopped by extension';
								break;
							case '999':
							default:
								$error = 'No error code avaiable';
						}
					}else 
					{
						$tempFile = $_FILES[$fileElementName]['tmp_name'];
						
						$now_dir = date('Y/m/d');
						$targetPath = str_replace('//','/',DIR_MEDICAL_CERTIFICATE . $now_dir . '/');
						log_message("debug","targetpath === " .$targetPath);
						if(mkdirs($targetPath)){
							$filename = $_FILES[$fileElementName]['name'];
							
							// Get the extension from the filename.
							$extension = substr($filename, strpos($filename,'.'), strlen($filename)-1);
							
							$target_filename = random_filename($targetPath, $extension, 6);
							$mc_filename = $now_dir . '/' . $target_filename;
							
							$targetFile = $targetPath . $target_filename;
							
							move_uploaded_file($tempFile,$targetFile);
						}else{
							$error = "Can't create directory";
						}
					}
				}
            }

        	if($error != ""){
				$this->session->set_flashdata('auth', $this->load->view('msgbox/error', $error , True));
				$this->add();
			}else{
				$state = 0;
	            if($operate == "save"){
	            	$state = 10;
	            }else if($operate == "apply"){
	            	$state = 20;
	            }
	            
	            //TODO calculate the leave balance
	            $include_break = 0;
	            
				if($request_type == "1"){
	            	$start_time = "";
	            	$end_time = "";
	            }else if($request_type == "2"){
	            	$end_day = $start_day;
	            	$include_break = $this->input->post('include_break');
	            }
	            
	            /*
	             * read user type
	             * read work time
	             * 
	             */
	            $dnow = now();
				$request_data = array(
	                'user_name' 	=> $user_name,
	                'leave_type_id' => $leave_type,
					'request_type'  => $request_type,
					'include_break' => $include_break,
	                'start_day' 	=> strtotime($start_day),
	                'end_day' 		=> strtotime($end_day),
	                'start_time' 	=> $start_time,
	                'end_time' 		=> $end_time,
					'start_date' 	=> strtotime($start_day ." " . $start_time),
					'end_date' 		=> strtotime($end_day . " " . $end_time),
	                'hours_used' 	=> $hours_used,
					'mc_provided'	=> empty($mc_provided) ? 0 : $mc_provided ,
					'carer_leave'	=> $carer_leave,
	                'reason' 		=> $reason,
					'state'			=> $state,
					'add_time' 		=> $dnow,
					'status' 		=> ACTIVE,
					'site_id'		=> $site_id,
					'department_id'	=> $dept_id
	            );
	            
	            if(!empty($mc_filename)){
	            	$a_mc = array(
	            		'medical_certificate_file' => $mc_filename
	            	);
	            	$request_data = array_merge($request_data, $a_mc);
	            }
	            
	            //add new request
				$insert_id = $this->request_m->insert($request_data);
	            if($insert_id <= 0){
	            	$error = "Leave request save failed.";
	            }else{
	            	//add hisotry log
	            	$this->load->model('St_request_log_m', 'request_log_m');
	            	$log = array(
	            		'id' => $insert_id,
	            		'operator_id' => $this->session->userdata('user_id'),
	            		'operation' => $operate,
	            		'operate_time' => $dnow,
	            		'operate_ip' => $this->input->ip_address()
	            	);
	            	
	            	$log = array_merge($request_data, $log);
	            	$this->request_log_m->insert($log);
	            	
	            	//send mail notification
	            	if($state == 20){
	            		$this->load->library('phpmail');
	            		$this->phpmail->readConfig();
	            		
	            		$this->phpmail->ClearAllAddresses();
	            		
	            		$requestinfo = $this->request_m->get_info($insert_id);
	            		

//	            		$user_level = $this->session->userdata("user_level");
	            		$user_level = $requestinfo->user_level_id;
	            		log_message("debug", "mail ...... 1");
	            		
	            		$supervisors = false;
	            		$managers = false;
	            		$operations = false;
	            			            		
	            		if($user_level == 1){ //send mail to supervisor
	            			//need to send an email notification to supervisor
	            			//read supervisor of current department
	            			$supervisors = $this->users_m->get_supervisors_by_user_name($user_name);
	            			if(!empty($supervisors)){
		            			foreach($supervisors as $supervisor){
		            				if(!empty($supervisor)){
		            					if(!empty($supervisor->email)){
		            						$this->phpmail->AddAddress($supervisor->email);
		            						log_message("debug", "supervisor:".$supervisor->email);
		            					}
		            				}
		            			}
	            			}
	            			
	            			// Email manager if longer than
//	            			if ($hours_used > 22.8){
	            				$managers = $this->users_m->get_managers_by_user_name($user_name);
	            				if(!empty($managers)){
	            					foreach($managers as $manager){
	            						if(!empty($manager)){
	            							if(!empty($manager->email)){
	            								$this->phpmail->AddAddress($manager->email);
	            								log_message("debug", "manager:".$manager->email);
	            							}
	            						}
	            					}
	            				}
	            				
/* 	            				$operations = $this->users_m->get_operation_by_user_name($user_name);
	            				if(!empty($operations)){
	            					foreach($operations as $operation){
	            						if(!empty($operation)){
	            							if(!empty($operation->email)){
	            								$this->phpmail->AddAddress($operation->email);
	            								log_message("debug", "operation:".$operation->email);
	            							}
	            						}
	            					}
	            				}	  */           				
//	            			}
	            		}
	            		
	            		if($user_level == 2){ //send mail to all manager
		            		$managers = $this->users_m->get_managers_by_user_name($user_name);
	            			if(!empty($managers)){
		            			foreach($managers as $manager){
		            				if(!empty($manager) && $manager->user_name != $requestinfo->user_name){
		            					if(!empty($manager->email)){
		            						$this->phpmail->AddAddress($manager->email);
		            						log_message("debug", "manager:".$manager->email);
		            					}
		            				}
		            			}
	            			}
	            			
	            			// Email operation if longer than
/* 	            			if ($hours_used > 22.5){
	            				$operations = $this->users_m->get_operation_by_user_name($user_name);
	            				if(!empty($operations)){
	            					foreach($operations as $operation){
	            						if(!empty($operation)){
	            							if(!empty($operation->email)){
	            								$this->phpmail->AddAddress($operation->email);
	            								log_message("debug", "operation:".$operation->email);
	            							}
	            						}
	            					}
	            				}
	            			}	  */           			
	            		}
	            		
//	            		if($user_level >= 3){ //send mail to operation
	            			$operations = $this->users_m->get_operation_by_user_name($user_name);
	            			if(!empty($operations)){
	            				foreach($operations as $operation){
	            					if(!empty($operation)){
	            						if(!empty($operation->email) && $operation->user_name != $requestinfo->user_name ){
	            							$this->phpmail->AddAddress($operation->email);
	            							log_message("debug", "operation:".$operation->email);
	            						}
	            					}
	            				}
	            			}
	            			
	            			// Email Global Sales
 	            			if ($requestinfo->department == 'Sales' || $requestinfo->department == 'Corporate'){
	            				$start_time = date('U',strtotime(date("Y-m-d")." 00:00:00"));
	            				$end_time = date('U',strtotime(date("Y-m-d")." 23:59:59"));
	            				$global_sales = $this->users_m->get_active_users($start_time, $end_time, $site_id=false, $department='Global Sales');
	            				if(!empty($global_sales)){
	            					foreach($global_sales as $sales){
	            						if(!empty($sales) && $sales->user_name != $requestinfo->user_name){
	            							if(!empty($sales->email)){
	            								$this->phpmail->AddAddress($sales->email);
	            								log_message("debug", "manager:".$sales->email);
	            							}
	            						}
	            					}
	            				}
	            			}           			
//	            		}
            			$amount_leave = "";
            			if($requestinfo->leave_type_id == LEAVE_TYPE_ANNUAL 
            				|| $requestinfo->leave_type_id == LEAVE_TYPE_PERSONAL){
	            			//calculate total annual/personal leave balance
//					    	$this->load->model('St_working_hours_m', 'working_hours_m');    	
//					    	$total_working_stat = $this->working_hours_m->get_staff_working_hours($user_name);
					    	
//					    	$work_count = $total_working_stat->work_count;
							$this->load->model('St_payroll_data_m', 'payroll_data_m');
							$payroll_data = $this->payroll_data_m->get_last_by_user($user_id);
							$last_annual_leave_end = 0;
							$last_personal_leave_end = 0;
							
            				if($payroll_data){
					    		$this->load->model('St_payroll_m', 'payroll_m');    		
					    		
					    		$last_payroll = $this->payroll_m->get_last();
					    		
						    	$annual_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_ANNUAL, true, false, $last_payroll->end_date);
						    	$annual_upcoming_used = IS_NULL($annual_upcoming_used)?0:$annual_upcoming_used;
					    	
					    		$personal_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, true, false, $last_payroll->end_date);
					    		$personal_upcoming_used = IS_NULL($personal_upcoming_used)?0:$personal_upcoming_used;
					    		
								$last_annual_leave_end = $payroll_data->annual_leave_end;
								$last_personal_leave_end = $payroll_data->personal_leave_end;
							}else{
								$this->load->model('St_working_hours_m', 'working_hours_m');
								$total_working_stat = $this->working_hours_m->get_staff_working_hours($user_name);
								
						    	$annual_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_ANNUAL, true);
						    	$annual_upcoming_used = IS_NULL($annual_upcoming_used)?0:$annual_upcoming_used;
					    	
					    		$personal_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, true);
					    		$personal_upcoming_used = IS_NULL($personal_upcoming_used)?0:$personal_upcoming_used;
								
								$last_annual_leave_end = get_number($total_working_stat->annual_hours_count);
								$last_personal_leave_end = get_number($total_working_stat->personal_hours_count);
							}
							
							$annual_available = round($last_annual_leave_end, 2);
							$personal_available = round($last_personal_leave_end, 2);
					    	
					    	if($requestinfo->leave_type_id == LEAVE_TYPE_ANNUAL){
					    		$amount_leave_count = $annual_available - $annual_upcoming_used;
					    		$amount_leave_count = round($amount_leave_count, 2);
					    		$amount_leave = ($amount_leave_count > 0)?$amount_leave_count:0;
					    	}
					    	else if($requestinfo->leave_type_id == LEAVE_TYPE_PERSONAL){
					    		$amount_leave_count = $personal_available - $personal_upcoming_used;
					    		$amount_leave_count = round($amount_leave_count, 2);
					    		$amount_leave = ($amount_leave_count > 0)?$amount_leave_count:0;
					    	}
            			}
            			if($amount_leave > 1){
			    			$mail_body['amount_leave_left'] = $amount_leave . " Hours";
			    		}elseif ($amount_leave){
			    			$mail_body['amount_leave_left'] = $amount_leave . " Hour";
			    		}else{
			    			$mail_body['amount_leave_left'] = false;
			    		}
            			
            			$mail_body['order_no'] = false;
            			
            			$subject = "New ". $requestinfo->leave_type ." Requested - " 
            				. $requestinfo->first_name . " " . $requestinfo->last_name
            				." From ".$start_day." to ".$end_day;
			    		$this->phpmail->setSubject($subject);
			    		
		            	$mail_body['appove_url'] = site_url('approve/deal/'.$insert_id);
		            	$mail_body['appove_name'] = "approve this leave application here.";
		            	$mail_body['request'] = $requestinfo;
		            	
		            	if ($requestinfo->site_id){
		            		$this->load->model('St_site_m', 'site_m');
		            		
		            		$store = $this->site_m->get($requestinfo->site_id);
		            		$mail_body['store'] = $store->name;
		            	}
		            	
		            	if ($requestinfo->department_id){
		            		$this->load->model('St_department_m', 'department_m');
		            	
		            		$department = $this->department_m->get($requestinfo->department_id);
		            		$mail_body['department'] = $department->department;
		            	}
		            	 
		            	
            			$this->phpmail->setBody($this->load->view('mail/request', $mail_body , True));
            			if(!$this->phpmail->send()){
				        	log_message("debug", "send mail .... ".$subject . $this->phpmail->ErrorInfo);
				        }
				        
				        //
				        $this->phpmail->ClearAllAddresses();
				        
   	        	        if(!empty($select_user->email)){
       						$this->phpmail->AddAddress($select_user->email);
       						log_message("debug", "user1:".$select_user->email);
       					}elseif ($login_user){
       						$this->phpmail->AddAddress($login_user->email);
       						log_message("debug", "user1:".$login_user->email);
       					}					        
   	        	        if(!empty($select_user->personal_email)){
       						$this->phpmail->AddAddress($select_user->personal_email);
       						log_message("debug", "user2:".$select_user->personal_email);
       					}elseif ($login_user){
       						$this->phpmail->AddAddress($login_user->personal_email);
       						log_message("debug", "user2:".$login_user->personal_email);
       					}
				        
            			$mail_body['appove_url'] = site_url('request/view/'.$insert_id);
            			$mail_body['appove_name'] = "view this leave application here.";
	            		$this->phpmail->setBody($this->load->view('mail/request', $mail_body , True));
             			if(!$this->phpmail->send()){
				        	log_message("debug", "send mail .... ".$subject . $this->phpmail->ErrorInfo);
				        }
	            	}
	            }
	            
	            if(!empty($error)){
					$this->session->set_flashdata('auth', $this->load->view('msgbox/error', $error , True));
					$this->add();
				}else{
	            	$this->index();	
	            }
			}
        }
    }

    /**
     * view a request
     */
	function view($request_id){
    	$data['navigation'] = "<a href='". site_url('admin/leave') ."' target='_top'>Leave Request</a> > <a href='". site_url('request') ."'>Leave Statement</a> > View";
    	//$request = $this->request_m->get_by_user_name_and_id($this->session->userdata('user_name'),$request_id);
    	$request = $this->request_m->get_info($request_id);
    	
    	    	
    	if(empty($request)){
    		show_404();
    	}
    	
    	$data['supervisor_name'] = "";
    	$data['manager_name'] = "";
    	
    	$this->load->model('St_users_m', 'users_m');
    	if(!is_null($request->supervisor_user_name)){
    		$supervisor = $this->users_m->get_by_user_name($request->supervisor_user_name);
    		if(!empty($supervisor)){
    			$data['supervisor_name'] = $supervisor->first_name . " " . $supervisor->last_name;
    		}
    	}
    	
		if(!is_null($request->manager_user_name)){
    		$manager = $this->users_m->get_by_user_name($request->manager_user_name);
    		if(!empty($manager)){
    			$data['manager_name'] = $manager->first_name . " " . $manager->last_name;
    		}
    	}
    	
    	//load leave type model
		$this->load->model('St_leave_type_m', 'leave_type_m');
		
    	$leave_types = $this->leave_type_m->get_dropdown();
    	$data['leave_types'] = $leave_types;
    	
    	$this->load->model('St_site_m', 'site_m');
    	
     	$stores = $this->site_m->get_select_dropdown();
     	$data['stores'] = $stores;
     	
     	$data['depts'] = false;
     	if ($request->site_id){
     		$this->load->model('St_department_m', 'department_m');
	     	$depts = $this->department_m->get_select_dropdown($request->site_id);
	     	$data['depts'] = $depts;
     	}
     	
    	
    	if($request->leave_type_id == LEAVE_TYPE_LATE || $request->leave_type_id == LEAVE_TYPE_LUNCH)
    		$request->hours_used = $request->hours_used * 60;

    	
    	$data['request'] = $request;
    	
    	//load request log model
		$this->load->model('St_request_log_m', 'request_log_m');
    	$request_logs = $this->request_log_m->get_by_search($request_id, "", "", "", "", "", "", "");
		$log_list = array();
		foreach($request_logs AS $request_log){
			$log['request_id'] = $request_log->id;
			//Action
			$operation = $request_log->operation;
			$action = "";
			if("save" == $operation){
				$action = "saved new request";
			}else if("apply" == $operation){
				$action = "applied request";
			}else if("update" == $operation){
				$action = "updated request";
			}else if("approve" == $operation){
				$action = "approved request";
			}else if("rejected" == $operation){
				$action = "rejected request";
			}else if("cancel" == $operation){
				$action = "canceled request";
			}
			$log['action'] = $action;
			//Actioned On
			$log['actioned_on'] = date(DATETIME_FORMAT, $request_log->operate_time);
			//Actioned By
			$log['actioned_by'] = $request_log->operator_first_name . " " . $request_log->operator_last_name;
			$log['action_ip'] = $request_log->operate_ip;
			$log_list[] = $log;
		}
		$data['request_logs'] = $log_list;
		$data['user_level'] = $this->session->userdata('user_level');
		$data['current_user_id'] = $this->session->userdata('user_id');
		
		
		// Load client visit
		$this->load->model('St_client_visit_m', 'client_visit_m');
		$client_visit = $this->client_visit_m->get_by_id($request_id, 'request');
			
		if ($client_visit){
			$data['order_url'] 	= SCORPY_HOME."/orders_forms.php?type=form&ordid=".$client_visit->ordid;
			$data['order_no']	= $client_visit->ordno;
			 
			$data['client_qb_name'] = $client_visit->client_qb_name;
			$data['street'] 		= $client_visit->street;
			$data['suburb'] 		= $client_visit->suburb;
			$data['total_km'] 		= $client_visit->total_km;
		}
		
		// Show actual clock in/out from actatek.
		if ($request->leave_type_id == LEAVE_TYPE_OVERTIME || $request->leave_type_id == LEAVE_TYPE_INTERSTORE || $request->leave_type_id == LEAVE_TYPE_UNPAID
				|| $request->leave_type_id == LEAVE_TYPE_MAKEUP || $request->leave_type_id == LEAVE_TYPE_LATE || $request->leave_type_id == LEAVE_TYPE_LUNCH || $client_visit){
			$job = false;
			$rows = false;
			
			$this->load->model('st_ac_clocking_log_m', 'ac_clocking_m');
			
/* 			if ($client_visit){
        		$rows = $this->ac_clocking_m->get_actual_clock_by_user($request->user_name, $client_visit->visit_date, $client_visit->visit_date);
        	}else{ */
        		$rows = $this->ac_clocking_m->get_actual_clock_by_user($request->user_name, $request->start_day, $request->end_day);
//        	}
        	
        	if ($rows){
        		$prev_date = false;
        		$user = $this->users_m->get_by_id($request->user_id);
        		foreach ($rows as $row){
        			if ($prev_date != $row->date){
        				$prev_date = $row->date;
        				 
        				$clocks[$row->date][$user->first_name." ".$user->last_name]['IN'] = array();
        				$clocks[$row->date][$user->first_name." ".$user->last_name]['OUT'] = array();
        				$clocks[$row->date][$user->first_name." ".$user->last_name]['total'] = 0.00;
        				//    					$clocks[$row->date][$user->first_name." ".$user->last_name]['total'] = $this->ac_clocking_m->get_total_working_hours($user->user_name, $row->date);
        				$total_hours = 0;
        			}
        	
        			if ($row->direction == 'IN'){
        				array_push($clocks[$row->date][$user->first_name." ".$user->last_name]['IN'], $row->time);
        				$in_time = strtotime($row->time);
        			}else{
        				array_push($clocks[$row->date][$user->first_name." ".$user->last_name]['OUT'], $row->time);
        				$out_time = strtotime($row->time);
        	
        				$total_hours += $out_time - $in_time;
        			}
        	
        			$clocks[$row->date][$user->first_name." ".$user->last_name]['last_dir'] = $row->direction;
        	
        			if ($total_hours)
        				$clocks[$row->date][$user->first_name." ".$user->last_name]['total'] = number_format($total_hours/3600, 2);
        	
        		}
        		ksort($clocks);
        		$data['clock'] = $clocks;
        	}        	
		}

		$this->load->view('navigation', $data);
    	$this->load->view('request/view_request_4', $data);
    }
    
    
    function view_actual_clock(){
    	$start_date = false;
    	$end_date = false;
    	 
    	$this->load->library('actatek');
    	$this->load->model('st_users_m', 'users_m');
    	$this->load->model('st_ac_clocking_log_m', 'ac_clocking_m');
    	$this->load->model('st_site_m', 'site_m');
    	$this->load->model('st_department_m', 'department_m');
    	 
    	$data['selected_department'] = $dept_id = $this->input->post('flt_department');
    	$data['selected_user'] = $user_id = $this->input->post('flt_user_name');
    	$data['selected_from_date'] = $start_date = $this->input->post('flt_from_date');
    	$data['selected_to_date'] = $end_date = $this->input->post('flt_to_date');
    	 
    	$time_period_id = $this->input->post('flt_time_period');
    	$data['selected_time_period'] = $time_period_id;
    	 
    	$site_id = $this->input->post('flt_site');
    	if (!$site_id)
    		$site_id = $this->session->userdata('site_id');
    	 
    	$data['selected_site'] = $site_id;
    
    	$data['sites'] = $this->site_m->get_dropdown();
    	$data['departments'] = $this->department_m->get_dropdown($site_id);
    	$data['users'] = $this->users_m->get_select_dropdown();
    
    	 
    	if (!empty($time_period_id)){
    		$time_period = get_time_period($time_period_id);
    		$start_date = $time_period[0];
    		$end_date = $time_period[1];
    	}else{
    		$data['selected_time_period'] = 'today';
    		$start_date = strtotime($start_date);
    		$end_date = strtotime($end_date);
    	}
    	 
    	 
    	$data['clock'] = false;
    	 
    	$users = array();
    	$clocks = array();
    	 
    	if ($start_date && $end_date){
    		if (!$user_id){
    			$users = $this->users_m->get_active_users($start_date, $end_date, $site_id, $dept_id);
    		}else{
    			$users[$user_id] = $this->users_m->get_by_id($user_id);
    		}
    	}

    	$total_hours = 0;
    	if ($users){
    		foreach ($users as $user){
    			$rows = array();
    			$todays = array();
    			
   				$rows = $this->ac_clocking_m->get_actual_clock_by_user($user->user_name, $start_date, $end_date);
   				
   				if (($time_period_id == 'today' || $time_period_id == 'this_week')){
   					if ($user->actatek_id)
   						$todays = $this->actatek->get_current_clock_by_actatek_id($user->actatek_id);
   						$rows = array_merge ($rows, $todays);
   				}
   					
    			
    			
    			$prev_date = false;
    			if ($rows){
	    			foreach ($rows as $row){
	    				if ($prev_date != $row->date){
	    					$prev_date = $row->date;
	    					 
	    					$clocks[$row->date][$user->first_name." ".$user->last_name]['IN'] = array();
	    					$clocks[$row->date][$user->first_name." ".$user->last_name]['OUT'] = array();
	    					$clocks[$row->date][$user->first_name." ".$user->last_name]['total'] = 0.00;
	//    					$clocks[$row->date][$user->first_name." ".$user->last_name]['total'] = $this->ac_clocking_m->get_total_working_hours($user->user_name, $row->date);
	    					$total_hours = 0;
	    				}
	    				
	    				if ($row->direction == 'IN'){
	    					array_push($clocks[$row->date][$user->first_name." ".$user->last_name]['IN'], $row->time." ".$row->location);
	    					$in_time = strtotime($row->time);
	    				}else{
	    					array_push($clocks[$row->date][$user->first_name." ".$user->last_name]['OUT'], $row->time." ".$row->location);
	    					$out_time = strtotime($row->time);
	    					
	    					$total_hours += $out_time - $in_time;
	    				}
	    				
    					$clocks[$row->date][$user->first_name." ".$user->last_name]['last_dir'] = $row->direction;
	    				
	    				if ($total_hours)
	    					$clocks[$row->date][$user->first_name." ".$user->last_name]['total'] = number_format($total_hours/3600, 2);
	    				
	    			}
    			}
    		}
    		ksort($clocks);
    		$data['clock'] = $clocks;
    	}
    	$this->load->view('user/view_clocking', $data);
    }    
    
    /**
     * edit a leave request
     * @param $request_id
     * @return page
     */
	function edit($request_id){
    	$data['navigation'] = "<a href='". site_url('admin/leave') ."' target='_top'>Leave Request</a> > <a href='". site_url('request') ."'>Leave Statement</a> > Update leave";
    	$request = $this->request_m->get_by_user_name_and_id($this->session->userdata('user_name'),$request_id);
    	
    	if(empty($request)){
    		show_404();
    	}
    	
    	if($request->state == 40 || $request->state == 50){
    		//TODO the requesst had been approved
    		show_404();
    	}
    	
    	//load leave type model
		$this->load->model('St_leave_type_m', 'leave_type_m');
		
    	$leave_types = $this->leave_type_m->get_dropdown();
    	$data['leave_types'] = $leave_types;
    	
    	$data['request'] = $request;
    	
    	//load request log model
		$this->load->model('St_request_log_m', 'request_log_m');
    	$request_logs = $this->request_log_m->get_by_search($request_id, "", "", "", "", "", "", "");
		$log_list = array();
		foreach($request_logs AS $request_log){
			$log['request_id'] = $request_log->id;
			//Action
			$operation = $request_log->operation;
			$action = "";
			if("save" == $operation){
				$action = "saved new request";
			}else if("apply" == $operation){
				$action = "applied request";
			}else if("update" == $operation){
				$action = "updated request";
			}else if("approve" == $operation){
				$action = "approved request";
			}else if("rejected" == $operation){
				$action = "rejected request";
			}else if("cancel" == $operation){
				$action = "canceled request";
			}
			$log['action'] = $action;
			//Actioned On
			$log['actioned_on'] = date(DATETIME_FORMAT, $request_log->operate_time);
			//Actioned By
			$log['actioned_by'] = $request_log->operator_first_name . " " . $request_log->operator_last_name;
			$log['action_ip'] = $request_log->operate_ip;
			$log_list[] = $log;
		}
		$data['request_logs'] = $log_list;
    	$this->load->view('navigation', $data);
    	$this->load->view('request/edit_request', $data);
    }
    
    /**
     * update request
     * @return json
     */
    function do_edit(){
        $this->form_validation->set_rules('leave_type', 'Leave Type', 'required');

        $request_id = $this->input->post('request_id');
        $this->load->model('St_users_m', 'users_m');
        $user_name = $this->session->userdata('user_name');
        if ($this->form_validation->run() == FALSE) {
            $this->edit($request_id);
        } else {
        	$error = "";
        	$operate = $this->input->post('operate');
            $leave_type = $this->input->post('leave_type');
            $start_day = $this->input->post('start_day');
            $end_day = $this->input->post('end_day');
            $start_time = $this->input->post('start_time');
            $end_time = $this->input->post('end_time');
            $hours_used = $this->input->post('hours_used');
            $reason = $this->input->post('reason');
            $mc_provided = $this->input->post('mc_provided');
            $dept_id = $this->input->post('department_id');
            
            $mc_filename = "";
            if($leave_type == LEAVE_TYPE_LATE
            	|| $leave_type == LEAVE_TYPE_LUNCH){
            	$start_day = $this->input->post('arrival_day');
            	$start_time = $this->input->post('arrival_time');
            	$hours_used = $this->input->post('minutes_used')/60;//The field is repaced with minutes here
            	$end_day = "";
            	$end_time = "";
            }else if($leave_type == LEAVE_TYPE_PERSONAL){
		        $fileElementName = 'medical_certificate';

		        if(empty($_FILES[$fileElementName]['tmp_name']) || $_FILES[$fileElementName]['tmp_name'] == 'none')
				{}else{
					if(!empty($_FILES[$fileElementName]['error']))
					{
						switch($_FILES[$fileElementName]['error'])
						{
							case '1':
								$error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
								break;
							case '2':
								$error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
								break;
							case '3':
								$error = 'The uploaded file was only partially uploaded';
								break;
							case '4':
								$error = 'No file was uploaded.';
								break;
				
							case '6':
								$error = 'Missing a temporary folder';
								break;
							case '7':
								$error = 'Failed to write file to disk';
								break;
							case '8':
								$error = 'File upload stopped by extension';
								break;
							case '999':
							default:
								$error = 'No error code avaiable';
						}
					}else 
					{
						$tempFile = $_FILES[$fileElementName]['tmp_name'];
						
						$now_dir = date('Y/m/d');
						$targetPath = str_replace('//','/',DIR_MEDICAL_CERTIFICATE . $now_dir . '/');
						log_message("debug","targetpath === " .$targetPath);
						if(mkdirs($targetPath)){
							$filename = $_FILES[$fileElementName]['name'];
							
							// Get the extension from the filename.
							$extension = substr($filename, strpos($filename,'.'), strlen($filename)-1);
							
							$target_filename = random_filename($targetPath, $extension, 6);
							$mc_filename = $now_dir . '/' . $target_filename;
							
							$targetFile = $targetPath . $target_filename;
							
							move_uploaded_file($tempFile,$targetFile);
						}else{
							$error = "Can't create directory";
						}
					}
				}
            }

        	if($error != ""){
				$this->session->set_flashdata('auth', $this->load->view('msgbox/error', $error , True));
				$this->edit($request_id);
			}else{
				$state = $this->input->post('state');
				
				$operation = "";
	            if($operate == "save"){
	            	$state = 10;
	            	$operation = "update";
	            }else if($operate == "apply"){
	            	$state = 20;
	            	$operation = "apply";
	            }
	            
	            if($state == 0){
	            	$this->session->set_flashdata('auth', $this->load->view('msgbox/error', "Operate is invalid" , True));
	            	$this->edit($request_id);
	            }
	            
				$include_break = 0;
	            $request_type = $this->input->post('request_type');
				if($request_type == "1"){
	            	$start_time = "";
	            	$end_time = "";
	            }else if($request_type == "2"){
	            	$end_day = $start_day;
	            	$include_break = $this->input->post('include_break');
	            }
	            
	            /*
	             * read user type
	             * read work time
	             * 
	             */
	            $dnow = now();
				$request_data = array(
	                'leave_type_id' => $leave_type,
					'request_type'  => $request_type,
					'include_break' => $include_break,
	                'start_day' 	=> strtotime($start_day),
	                'end_day' 		=> strtotime($end_day),
	                'start_time' 	=> $start_time,
	                'end_time' 		=> $end_time,
					'start_date' 	=> strtotime($start_day ." " . $start_time),
					'end_date' 		=> strtotime($end_day . " " . $end_time),
	                'hours_used' 	=> $hours_used,
					'mc_provided'	=> empty($mc_provided) ? 0 : $mc_provided ,
	                'reason' 		=> $reason,
					'state'			=> $state
	            );
	            
	            if(!empty($mc_filename)){
	            	$a_mc = array(
	            		'medical_certificate_file' => $mc_filename
	            	);
	            	$request_data = array_merge($request_data, $a_mc);
	            }
	            
	            //update a request
	            log_message("debug", "update a request id ...... " . $request_id);
	            
				$b_update = $this->request_m->update($request_id, $request_data);
	            if(!$b_update){
	            	$error = "Leave request update failed.";
	            }else{
	            	//add hisotry log
	            	$this->load->model('St_request_log_m', 'request_log_m');
	            	$log = array(
	            		'id' => $request_id,
	            		'operator_id' => $this->session->userdata('user_id'),
	            		'operation' => $operation,
	            		'operate_time' => $dnow,
	            		'operate_ip' => $this->input->ip_address()
	            	);
	            	$log = array_merge($request_data, $log);
	            	$this->request_log_m->insert($log);
	            	
	            	if($state == 20){
	            	$requestinfo = $this->request_m->get_info($request_id);
	            		
	            		$this->load->library('phpmail');
	            		$this->phpmail->readConfig();
//	            		$user_level = $this->session->userdata("user_level");
	            		$user_level = $requestinfo->user_level_id;
	            		
	            		if($user_level == 1){ //send mail to supervisor
	            			//need to send an email notification to supervisor
	            			//read supervisor of current department
	            			$supervisors = $this->users_m->get_supervisors_by_user_name($user_name);
	            			if(!empty($supervisors)){
		            			foreach($supervisors as $supervisor){
		            				if(!empty($supervisor)){
		            					if(!empty($supervisor->email)){
		            						$this->phpmail->AddAddress($supervisor->email);
		            					}
//		            					if(!empty($supervisor->personal_email)){
//		            						$this->phpmail->AddCC($supervisor->personal_email);
//		            					}
		            				}
		            			}
	            			}
	            		}
	            		
	            		//send mail to all manager and admin
	            		$managers = $this->users_m->get_managers_by_user_name($user_name);
            			if(!empty($managers)){
	            			foreach($managers as $manager){
	            				if(!empty($manager)){
	            					if(!empty($manager->email)){
	            						$this->phpmail->AddAddress($manager->email);
	            					}
//	            					if(!empty($manager->personal_email)){
//	            						$this->phpmail->AddCC($manager->personal_email);
//	            					}
	            				}
	            			}
            			}
            			
            			$operations = $this->users_m->get_operation_by_user_name($user_name);
            			if(!empty($operations)){
            				foreach($operations as $operation){
            					if(!empty($operation)){
            						if(!empty($operation->email) && $operation->user_name != $requestinfo->user_name ){
            							$this->phpmail->AddAddress($operation->email);
            							log_message("debug", "operation:".$operation->email);
            						}
            					}
            				}
            			}
            			
            			// Email Global Sales
            			if ($requestinfo->department == 'Sales' || $requestinfo->department == 'Corporate'){
            				$start_time = date('U',strtotime(date("Y-m-d")." 00:00:00"));
            				$end_time = date('U',strtotime(date("Y-m-d")." 23:59:59"));
            				$global_sales = $this->users_m->get_active_users($start_time, $end_time, $site_id=false, $department='Global Sales');
            				if(!empty($global_sales)){
            					foreach($global_sales as $sales){
            						if(!empty($sales) && $sales->user_name != $requestinfo->user_name){
            							if(!empty($sales->email)){
            								$this->phpmail->AddAddress($sales->email);
            								log_message("debug", "manager:".$sales->email);
            							}
            						}
            					}
            				}
            			}            			
            			
            			$amount_leave = "";
            			if($requestinfo->leave_type_id == LEAVE_TYPE_ANNUAL 
            				|| $requestinfo->leave_type_id == LEAVE_TYPE_PERSONAL){
	            			//calculate total annual/personal leave balance
					    	$this->load->model('St_working_hours_m', 'working_hours_m');    	
					    	$total_working_stat = $this->working_hours_m->get_staff_working_hours($user_name);
					    	
					    	$work_count = $total_working_stat->work_count;
					    	
					    	if($requestinfo->leave_type_id == LEAVE_TYPE_ANNUAL){
					    		$total_annual = $total_working_stat->annual_hours_count;
					    		$annual_used_count = $total_working_stat->annual_used_count;
					    		//calculate the annual leave balance
					    		$annual_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_ANNUAL);
					    		$annual_upcoming_used = $this->request_m->get_upcoming_count_by_leave_type($user_name, LEAVE_TYPE_ANNUAL);
					    		$annual_upcoming_used = IS_NULL($annual_upcoming_used)?0:$annual_upcoming_used;
					    		$amount_leave_count = $total_annual - $annual_used - $annual_upcoming_used;
					    		$amount_leave_count = round($amount_leave_count, 2);
					    		$amount_leave = ($amount_leave_count > 0)?$amount_leave_count:0;
					    	}
					    	else if($requestinfo->leave_type_id == LEAVE_TYPE_PERSONAL){
					    		$total_personal = $total_working_stat->personal_hours_count;
					    		$personal_used_count = $total_working_stat->personal_used_count;
					    		//calculate the annual leave balance
					    		$personal_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL);
					    		$personal_upcoming_used = $this->request_m->get_upcoming_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL);
					    		$personal_upcoming_used = IS_NULL($personal_upcoming_used)?0:$personal_upcoming_used;
					    		$amount_leave_count = $total_personal - $personal_used - $personal_upcoming_used;
					    		$amount_leave_count = round($amount_leave_count, 2);
					    		$amount_leave = ($amount_leave_count > 0)?$amount_leave_count:0;
					    	}
            			}
            			if($amount_leave > 1){
			    			$mail_body['amount_leave_left'] = $amount_leave . " Hours";
			    		}elseif ($amount_leave){
			    			$mail_body['amount_leave_left'] = $amount_leave . " Hour";
			    		}else{
			    			$mail_body['amount_leave_left'] = false;
			    		}

            			$mail_body['order_no'] = false;
            			
            			$subject = "New ". $requestinfo->leave_type ." Requested - " 
            				. $requestinfo->first_name . " " . $requestinfo->last_name
            				." From ".$start_day." to ".$end_day;
			    		$this->phpmail->setSubject($subject);
			    		
		            	$mail_body['appove_url'] = site_url('approve/deal/'.$request_id);
		            	$mail_body['appove_name'] = "approve this leave application here.";
		            	$mail_body['request'] = $requestinfo;
		            	
		            	if ($requestinfo->site_id){
		            		$this->load->model('St_site_m', 'site_m');
		            	
		            		$store = $this->site_m->get($requestinfo->site_id);
		            		$mail_body['store'] = $store->name;
		            	}
		            	 
		            	
            			$this->phpmail->setBody($this->load->view('mail/request', $mail_body , True));
            			if(!$this->phpmail->send()){
				        	log_message("debug", "send mail .... ".$subject . $this->phpmail->ErrorInfo);
				        }
				        
				        //
				        $this->phpmail->ClearAllAddresses();
	            		$email = $this->session->userdata("email");
	            		if(!empty($email)){
	            			$this->phpmail->AddAddress($email);
            			}
            			$personal_email = $this->session->userdata("personal_email");
            			if(!empty($personal_email)){
            				$this->phpmail->AddCC($personal_email);	
            			}
            			$mail_body['appove_url'] = site_url('request/view/'.$request_id);
            			$mail_body['appove_name'] = "view this leave application here.";
	            		$this->phpmail->setBody($this->load->view('mail/request', $mail_body , True));
            			if(!$this->phpmail->send()){
				        	log_message("debug", "send mail .... ".$subject . $this->phpmail->ErrorInfo);
				        }
	            	}
	            }
	            
	            //return json 
//	            $result = array(
//		        	"message"=> "",
//		        	"status" 	=> ""
//		        );
		        
//				if(!empty($error)){
//					$result['message'] = $error;
//					$result['status'] = "fail";
//				}else{
//					$result['message'] = "Leave update successfully.";
//					$result['status'] = "ok";
//		        }
//		        echo json_encode($result);
				if(!empty($error)){
					$this->session->set_flashdata('auth', $this->load->view('msgbox/error', $error , True));
					$this->edit();
				}else{
	            	redirect("request");
	            }
			}
        }
    }
    
    /**
     * cancel a leave request
     * @return json
     */
    function cancel_ajax(){
		$request_id = $this->input->post('id');
		$form_id = $this->input->post('form_id');
		
		$data['form_id'] = $form_id;
		
		$data['status'] = "err";
		if(is_null($request_id)){
			$data['message'] = "Invalid data";
		}else{
			$request = $this->request_m->get($request_id);
			if(empty($request)){
				$data['message'] = "Invalid data";
			}else{
				if(strcmp($request->user_name, $this->session->userdata("user_name")) != 0 ) {					
					$data['message'] = "You have not permission";
				}else{
					//cancel
					$b_update = $this->request_m->set_inactive($request_id);
					if($b_update == 1){
						//add hisotry log
		            	$this->load->model('St_request_log_m', 'request_log_m');
		            	$log = array(
		            		'id' => $request_id,
		            		'operator_id' => $this->session->userdata('user_id'),
		            		'operation' => 'cancel',
		            		'operate_time' => now(),
		            		'operate_ip' => $this->input->ip_address()
		            	);
		            	$this->request_log_m->insert($log);
	            	
						$data['status'] = "ok";
						$data['message'] = "Cancel successfully!";
						if($request->state > 20){
							//TODO send an email notification
						}
					}else{
						$data['message'] = "Cancel failed!";
					}
				}
			}
		}
		echo json_encode($data);
    }
    
    /**
     * calculate available hours by leave type: annual / personal
     * @return json
     */
    function calculate_available_ajax(){
    	$leave_type = $this->input->post('leave_type');
    	$no_mc = $this->input->post('no_mc');
    	
    	$this->load->model('St_users_m', 'users_m');
    	$this->load->library('workinghour');
    	$this->load->model('st_config_m', 'config_m');
    	 
    	
    	if ($this->input->post('user_id')){
        	$select_user = $this->users_m->get_by_id($this->input->post('user_id'));
	        $user_name = $select_user->user_name;
	        $user_id = $select_user->id;
        }else{
	        $user_name = $this->session->userdata('user_name');
	        $user_id = $this->session->userdata('user_id');
        }
    	

		$data['status'] = "err";
		if(is_null($leave_type)){
			$data['message'] = "Invalid data";
		}else{
	    	$now_time = now();
	    	$now_day = date(DATE_FORMAT, $now_time);
	    	
	    	$this->load->model('St_payroll_data_m', 'payroll_data_m');
			$payroll_data = $this->payroll_data_m->get_last_by_user($user_id);
			$last_annual_leave_end = 0;
			$last_personal_leave_end = 0;
			$last_inlieu_end = 0;
						
	    	if($payroll_data){
	    		$this->load->model('St_payroll_m', 'payroll_m');    		
	    		$last_payroll = $this->payroll_m->get_last();
	    		
				$last_annual_leave_end = $payroll_data->annual_leave_end;
				$last_personal_leave_end = $payroll_data->personal_leave_end;
				
				$last_inlieu_end = $payroll_data->inlieu_end;
			}else{
				$this->load->model('St_working_hours_m', 'working_hours_m');
				$total_working_stat = $this->working_hours_m->get_staff_working_hours($user_name);
				
				$last_annual_leave_end = get_number($total_working_stat->annual_hours_count);
				$last_personal_leave_end = get_number($total_working_stat->personal_hours_count);
			}    	
			
			$annual_available = round($last_annual_leave_end, 2);
			$personal_available = round($last_personal_leave_end, 2);
			$inlieu_available = $last_inlieu_end;
			
	    	$data['available'] = -1;
	    	if($leave_type == LEAVE_TYPE_ANNUAL){
	    		$data['status'] = "ok";
		    	if ($last_payroll){
			    	$annual_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_ANNUAL, true, false, $last_payroll->end_date);
		    	}else{
			    	$annual_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_ANNUAL, true, false);
		    	}
			    $annual_upcoming_used = IS_NULL($annual_upcoming_used)?0:$annual_upcoming_used;
		    	$data['available'] = round($annual_available - $annual_upcoming_used, 2);
	    	}else if($leave_type == LEAVE_TYPE_PERSONAL){
	    		$data['status'] = "ok";
	    		
				if ($last_payroll){
					$personal_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, true, false, $last_payroll->end_date);						
				}else{
					$personal_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, true, false);
				}
				$personal_upcoming_used = IS_NULL($personal_upcoming_used)?0:$personal_upcoming_used;
				$personal_available -= $personal_upcoming_used;
	    		
				if ($no_mc==1){
					$no_mc_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, false, true);
					$no_mc_used = IS_NULL($no_mc_used)?0:$no_mc_used;
					
					$max_no_mc = $this->workinghour->calculate_max_no_mc($user_id);
					$data['max_no_mc'] = $max_no_mc;
					
					$no_mc_available = $max_no_mc - $no_mc_used;
					
					$data['available'] = $personal_available < $no_mc_available ? round($personal_available, 2) : round($no_mc_available, 2); 
				}else{
				    $data['available'] = round($personal_available, 2) ;
				}
				
	    	}else if($leave_type == LEAVE_TYPE_COMPASIONATE){
	    		$data['status'] = "ok";
				$compasionate_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_COMPASIONATE, false, false);
				
				$max_no_mc = $this->workinghour->calculate_max_no_mc($user_id);

		        $compasionate_upcoming_used = IS_NULL($compasionate_upcoming_used)?0:$compasionate_upcoming_used;
        		$data['available'] = round($max_no_mc - $compasionate_upcoming_used, 2);
        		
	    	}else if($leave_type == LEAVE_TYPE_INLIEU){
	    		$data['status'] = "ok";
	    		$inlieu_upcoming_used = 0;
	    		
	    		if ($last_payroll){
//					$inlieu_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_INLIEU, true, false, $last_payroll->end_date);
					
					$days_payroll = $this->config_m->get_by_code("Days_Payroll_Parameter")->value;
					$next_payroll_start_date = date_add('d', $days_payroll, date(DATE_FORMAT, $last_payroll->end_date));
					$inlieu_upcoming_used = $this->workinghour->calculate_leave_used($user_id, LEAVE_TYPE_INLIEU, $last_payroll->end_date, $next_payroll_start_date);
					$inlieu_upcoming_used += $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_INLIEU, true, false, $next_payroll_start_date);
	    		}
				
        		$data['available'] = round($inlieu_available - $inlieu_upcoming_used, 2);
	    	}
		}
		echo json_encode($data);
    }
    
    function detailed_save() {
        $data['navigation'] = "<a href='". site_url('admin/leave') ."' target='_top'>Leave Request</a> > <a href='". site_url('request') ."'>Leave Statement</a> > View";
        $this->load->view('navigation', $data);
        $this->load->view('request/detailed_request_save');
    }

    function detailed() {
        $data['navigation'] = "<a href='". site_url('admin/leave') ."' target='_top'>Leave Request</a> > <a href='". site_url('request') ."'>Leave Statement</a> > View";
        $this->load->view('navigation', $data);
        $this->load->view('request\detailed_request');
    }

    function close() {
        $this->index();
    }

    function approve(){
		$data['navigation'] = "<a href='". site_url('admin/leave') ."' target='_top'>Leave Request</a> > <a href='". site_url('approve') ."'>Leave Approval</a> > View";
		//read each leave request state count
    	$request_counts = $this->request_m->get_requests_counts($this->session->userdata('user_name'));
    	$new_count = 0;
    	$not_approved_count = 0;
    	$approved_count = 0;
    	$pending_count = 0;
    	foreach ($request_counts as $row){
    		if($row->state == 10){
    			$new_count = $row->icount;
    		}else if($row->state == 20 || $row->state == 30){
    			$pending_count += $row->icount;
    		}else if($row->state == 40){
    			$approved_count = $row->icount;
    		}else if($row->state == 50){
    			$not_approved_count = $row->icount;
    		}
    	}
		$data['new_count'] = $new_count;
		$data['not_approved_count'] = $not_approved_count;
		$data['approved_count'] = $approved_count;
		$data['pending_count'] = $pending_count;
		
        $this->load->view('navigation', $data);
		$this->load->view('request/approve');	
    }
    
    /**
     * calculate the working hours
     * @return json
     */
	function calculate_working_hours_ajax(){
		log_message("debug", "calculate_working_hours_ajax..");
		//
		$request_type = $this->input->post('request_type');
		$start_day = $this->input->post('start_day');
		$start_time = $this->input->post('start_time');
		$end_day = $this->input->post('end_day');
		$end_time = $this->input->post('end_time');
		$include_break = $this->input->post('include_break');
		$leave_type = $this->input->post('leave_type');
		
		$start_date = strtotime($start_day . " " . $start_time);
		$end_date = strtotime($end_day . " " . $end_time);
//		if($request_type == 1){
//			$end_date = date_add('d', 1 , $end_day);
//		}
		//$user_name = $this->session->userdata("user_name");

		if ($this->input->post('user_id'))
			$user_id = $this->input->post('user_id');
		else 
			$user_id = $this->session->userdata("user_id");
		
		log_message("debug", "start date == " . date(DATETIME_FORMAT,$start_date));
		log_message("debug", "end date == " . date(DATETIME_FORMAT, $end_date));
		log_message("debug", "request_type == " . $request_type);
		log_message("debug", "include_break == " . $include_break);
		//$this->load->model('St_user_labor_contract_m', 'labor_contract_m');
		//$labor = $this->labor_contract_m->get_current($user_name);
		//$used_hours_count = calculate_working_hours($labor, $start_date, $end_date, $include_break);
		$this->load->library('workinghour');
		$used_hours_count = $this->workinghour->calculate_hours_used( $user_id, $start_date, $end_date, $request_type, $include_break, $leave_type);
		$a_json['status'] = "ok";
		$a_json['used_hours_count'] = $used_hours_count;
		log_message("debug", "calculate_working_hours_ajax..json_encode used_hours_count == " . $used_hours_count);
		echo json_encode($a_json);
	}
	
	/**
	 * calculate late minutes
	 * @return json
	 */
	function calculate_minutes_late_ajax(){
		log_message("debug", "calculate_minutes_late_ajax..");
		//
		$arrival_day = $this->input->post('arrival_day');
		$arrival_time = $this->input->post('arrival_time');
		
//		$arrival_date = strtotime($arrival_day . " " . $arrival_time);
		
		if ($this->input->post('user_id'))
			$user_id = $this->input->post('user_id');
		else
			$user_id = $this->session->userdata("user_id");
		
		
		
		$this->load->library('workinghour');
		$used_minutes_count = $this->workinghour->calculate_late_minutes($user_id, $arrival_day, $arrival_time);
		$a_json['status'] = "ok";
		$a_json['used_minutes_count'] = $used_minutes_count > 0 ? $used_minutes_count : "";
		echo json_encode($a_json);
	}
	
	/**
	 * When requesting for a leave, it should do a check, whether the 
	 * same person has requested a leave of the same type on the same day. A person 
	 * can have a late request, long lunch, and an annual leave and sick leave all 
	 * on the same day, this is fine.
	 * @return json
	 */
	function calculate_same_request_ajax(){
		$this->load->model('St_users_m', 'users_m');
		
	    if ($this->input->post('user_id')){
        	$select_user = $this->users_m->get_by_id($this->input->post('user_id'));
	        $user_name = $select_user->user_name;
	        $user_id = $select_user->id;
        }else{
	        $user_name = $this->session->userdata('user_name');
	        $user_id = $this->session->userdata('user_id');
        }
		
		
//		$user_name = $this->session->userdata('user_name');
		$leave_type = $this->input->post('leave_type');
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');
		$request_id = $this->input->post('request_id');
		$same_count = $this->request_m->get_same_by_leave_type($user_name, $leave_type, strtotime($start_date), strtotime($end_date), $request_id);
		$a_json['count'] = $same_count;
		echo json_encode($a_json);
	}
	
}