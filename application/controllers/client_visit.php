<?php

class Client_Visit extends CI_Controller {

    function Client_Visit() {
        parent::__construct();
    	if(!$this->userauth->logined()){
			$infoMsg = array(
				'info' => 'Please login !'
			);
			$this->session->set_flashdata('auth', $this->load->view('msgbox/info', $infoMsg , True));
			$this->session->set_flashdata('back_url', current_url());
			redirect("login");
		}else{
			if(!$this->userauth->judge_priv("client_visit")){
				$errorMsg = array(
					'error' => 'You have no priviledge into this page !'
				);
				$this->session->set_flashdata('auth', $this->load->view('msgbox/error', $errorMsg , True));
				redirect("msgbox", 'location');
			}
		}
		
		$this->form_validation->set_error_delimiters('<p class="error">', '</p>');
		
		//load request model
		$this->load->model('St_request_m', 'request_m');
		$this->load->model('St_client_visit_m', 'client_visit_m');
    }
	
    function index() {

    }
    
    function new_ajax(){
    	$user_id = $this->session->userdata('user_id');
    	$data['form_id'] = 'new';
        
    	$page_no = $this->input->post('new_page_no');
        
		$limit = $this->input->post('new_limit');
    	$data['page_limit'] = $limit;
    	
    	$data['jobs'] = $this->job_incident_m->get_new_by_user($user_id,$page_no, $limit);
		$total_count = $this->job_incident_m->count_new_by_user($user_id);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
		
		$content = $this->load->view('jobincident/list_job_incident_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }
    
    function progress_ajax(){
    	$user_id = $this->session->userdata('user_id');
    	$data['form_id'] = 'progress';
        
    	$page_no = $this->input->post('progress_page_no');
        
		$limit = $this->input->post('progress_limit');
    	$data['page_limit'] = $limit;
    	
    	$data['jobs'] = $this->job_incident_m->get_progress_by_user($user_id,$page_no, $limit);
		$total_count = $this->job_incident_m->count_progress_by_user($user_id);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
		
		$content = $this->load->view('jobincident/list_job_incident_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }
    
    function completed_ajax(){
    	$user_id = $this->session->userdata('user_id');
    	$data['form_id'] = 'completed';
        
    	$page_no = $this->input->post('completed_page_no');
        
		$limit = $this->input->post('completed_limit');
    	$data['page_limit'] = $limit;
    	
    	$data['jobs'] = $this->job_incident_m->get_completed_by_user($user_id,$page_no, $limit);
		$total_count = $this->job_incident_m->count_completed_by_user($user_id);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
		
		$content = $this->load->view('jobincident/list_job_incident_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }
    
    function user_list_ajax(){
    	$site_id = $this->input->post('site_id');
    	
    	$this->load->model('St_users_m', 'users_m');
    	$users = $this->users_m->get_select_dropdown($site_id);
    	
    	echo json_encode($users);
    	
    }
    
    function store_address_ajax(){
    	$site_id = $this->input->post('site_id');
    	
    	$this->load->model('St_site_m', 'site_m');
    	
    	$site_id = empty($site_id) ? 1 : $site_id;
    	
    	$store = $this->site_m->get_by_storeid($site_id);
    	$store_address = $store->address.",VIC,AUSTRALIA";
    
    	echo json_encode($store_address);
    
    }    
    /**
     * prepare to add a new leave request
     * @return 
     */
    function form($action='view', $id=false) {
    	$this->load->model('St_users_m', 'users_m');
    	
       	$user_id = $this->session->userdata('user_id');
       	$site_id = $this->session->userdata('site_id');
       	$user_level = $this->session->userdata("user_level");
       	
        $data['navigation'] = "<a href='". site_url('admin/client_visit/form'.$action) ."' >Client Visit</a> > ".ucfirst($action)." Client Visit";
        $data['order_link_url'] = "<a href='".SCORPY_HOME."/service_order_list.php?onsite=1'>Service Order List</a>"; 
        
        $data['users_list'] = $this->users_m->get_select_dropdown($site_id);
        
        $this->load->model('St_site_m', 'site_m');
        $data['sites'] = $this->site_m->get_dropdown();
        $data['user_level'] = $user_level;
        $data['now_date'] = date('d-m-Y h:i:s');
        $data['action'] = $action;
        $data['order_no'] = false;
        $data['ordid'] = false;
        
        $data['total_km'] = false;
        $data['phone'] = false;
        $data['visit_date'] = false;
        $data['from_time'] = false;
        $data['to_time'] = false;
        $data['reason'] = false;
        $data['client_address'] = false;
        
        $data['client_qb_name']	= false;
        $data['client_name'] 	= false;
        $data['client_street'] 	= false;
        $data['client_suburb'] 	= false;
        $data['client_state'] 	= false;
        $data['client_postcode'] 	= false;
        
        $data['actual_from_time'] 	= false;
        $data['actual_to_time'] 	= false;
        $data['actual_km'] 	= false;
        $data['allow_edit_delete'] = false;
        $data['status'] = 'new';
        
        $ordid = false;
        $storeid = 1;
        
        if ($action=='add'){
        	$data['read_only'] = false;
	        $current_user = $this->users_m->get_by_id($user_id);
        	
        	if ($id) $ordid = $id;
        }
        else{
        	$data['read_only'] = $action == 'view' ? true : false;
        	
        	$job = $this->client_visit_m->get_by_id($id);
        	$data['ordid'] 			= $ordid = $job->ordid;
        	$data['order_url'] 		= SCORPY_HOME."/orders_forms.php?type=form&ordid=".$job->ordid;
        	$data['client_visit_id'] = $job->id;
        	$data['user_id'] 		= $job->user_id;
        	$data['client_qb_name']	= $job->client_qb_name;
        	$data['client_name'] 	= $job->client_name;
        	$data['order_no']		= $job->ordno;
        	$data['request_id'] 	= $job->request_id;
	        $data['total_km']		= $job->total_km;
	        $data['client_street'] 	= $job->street;
	        $data['client_suburb'] 	= $job->suburb;
	        $data['client_state']	= $job->state;
	        $data['client_postcode']= $job->postcode;
	        $data['phone'] 			= $job->phone;
	        $data['visit_date'] 	= date(DATE_FORMAT, $job->visit_date);
	        $data['from_time'] 		= $job->from_time;
	        $data['to_time'] 		= $job->to_time;
	        $data['reason'] 		= $job->reason;
	        $data['client_address'] = $job->street.",".$job->suburb.",".$job->state.",AUSTRALIA";
	        $data['site_id'] = $storeid = $job->site_id;
	        
	        $data['actual_from_time'] 	= $job->actual_from_time;
	        $data['actual_to_time'] 	= $job->actual_to_time;
	        $data['actual_km'] 			= $job->actual_km;
	        $data['reimbursed'] 		= $job->reimbursed;
	        
	        $current_user = $this->users_m->get_by_id($job->user_id);	        
	        
	        // Allow delete and edit.
	        $today = date('U',strtotime(date("Y-m-d")));
	        
	        if (($job->visit_date >= $today && $job->user_id == $user_id) || $user_level >=3){
	        	$data['allow_edit_delete'] = true;
	        }
        	
        }
        
		$data['current_user'] = $current_user;
        
        
        //Get order master.
       	if ($ordid && $action == 'add'){
	      	$order_master = $this->client_visit_m->get_scorpy_order_master($ordid);
	      	
	      	$storeid = $order_master->pickup_storeid;
	      	
	      	$data['order_no'] = $order_master->ordno;
	      	$data['ordid'] = $order_master->ordid;
	      	$data['client_qb_name'] = $order_master->qb_name;
		    $data['client_name'] = !empty($order_master->bill_company) ? $order_master->bill_company : $order_master->bill_name;
		    $data['client_street'] = $order_master->deliver_street;
		    $data['client_suburb'] = $order_master->deliver_suburb;
		    
		    if ($order_master->deliver_state=='VIC-METROPOLITAN' || $order_master->deliver_state=='VIC-COUNTRY')
		    	$order_master->deliver_state = 'VICTORIA';
		    	
		    $data['client_state'] = $order_master->deliver_state;
		    $data['client_postcode'] = $order_master->deliver_postcode;
		    if (!empty($order_master->deliver_street) && !empty($order_master->deliver_suburb) )
	     		$data['client_address'] = $order_master->deliver_street.",".$order_master->deliver_suburb.",".$order_master->deliver_state.",AUSTRALIA";
	     	
	     	// Set datetime
	     	if ($order_master->service_datetime){
	     		$datetime = strtotime($order_master->service_datetime);
	     		$data['visit_date'] = date("d-m-Y", $datetime);
 	     		$data['from_time'] = date("H:i", $datetime);
 	     		$data['to_time'] = date("H", $datetime) + 2 .":00";
	     	}
	     	
	     	// Set default tech name
	     	if ($order_master->service_tech){
	     		$data['assigned_user'] = $this->users_m->get_by_user_name($order_master->service_tech);
	     	}
       	}
       	
       	if (empty($data['assigned_user']))
       		$data['assigned_user'] = $current_user;
       	
       	
	    $store = $this->site_m->get_by_storeid($storeid);
		$store_address = $store->address.",VIC,AUSTRALIA";
		$data['store_address'] = $store_address; 
		
		
		// Get status.
		if ($action=='add'){
			$data['status'] = 'new';
		}else{
			if ($job){
				if ($job->req_state=20 && $job->visit_date >= now())
					$data['status'] = 'new';
				elseif ($job->req_state=40 && $job->visit_date >= now())
					$data['status'] = 'upcoming';
				elseif ($job->req_state=40 && $job->reimbursed == 0 && $job->visit_date < now())
					$data['status'] = 'reimburse';
				elseif ($job->visit_date < now())
					$data['status'] = 'completed';
				
			}
		}
		
		// Get actual clocking time
		$data['clock'] = false;
		
		if ($action != 'add' && ($data['status'] == 'reimburse' || $data['status'] = 'completed') ){
		
			$this->load->model('st_ac_clocking_log_m', 'ac_clocking_m');
				
			$rows = $this->ac_clocking_m->get_actual_clock_by_user($job->user_name, $job->visit_date, $job->visit_date);
			
			if ($rows){
				$prev_date = false;
				$user = $this->users_m->get_by_id($job->user_id);
				foreach ($rows as $row){
					if ($prev_date != $row->date){
						$prev_date = $row->date;
						 
						$clocks[$row->date][$user->first_name." ".$user->last_name]['IN'] = array();
						$clocks[$row->date][$user->first_name." ".$user->last_name]['OUT'] = array();
						$clocks[$row->date][$user->first_name." ".$user->last_name]['total'] = 0.00;
						//    					$clocks[$row->date][$user->first_name." ".$user->last_name]['total'] = $this->ac_clocking_m->get_total_working_hours($user->user_name, $row->date);
						$total_hours = 0;
					}
					 
					if ($row->direction == 'IN'){
						array_push($clocks[$row->date][$user->first_name." ".$user->last_name]['IN'], $row->time);
						$in_time = strtotime($row->time);
					}else{
						array_push($clocks[$row->date][$user->first_name." ".$user->last_name]['OUT'], $row->time);
						$out_time = strtotime($row->time);
						 
						$total_hours += $out_time - $in_time;
					}
					 
					$clocks[$row->date][$user->first_name." ".$user->last_name]['last_dir'] = $row->direction;
					 
					if ($total_hours)
						$clocks[$row->date][$user->first_name." ".$user->last_name]['total'] = number_format($total_hours/3600, 2);
					 
				}
				ksort($clocks);
				$data['clock'] = $clocks;
			}
		}
       	
        
        $this->load->view('navigation', $data);
        $this->load->view('client_visit/client_visit_form');
    }
    
    
    /**
     * deal with add/update client visit form.
     * @return void
     */
	function do_save_form() {
		$action = $this->input->post('action');
       	$now_time = now();
       	$current_user_id = $this->session->userdata('user_id');
       	$current_user_name = $this->session->userdata('user_name');
       	
       	$this->load->model('St_users_m', 'users_m');
       	$user_id = $this->input->post('user_id');
   	
       	$select_user = $this->users_m->get_by_id($user_id);
        $user_name = $select_user->user_name;

        $list = $this->input->post('status');
       	
       
        // Insert client visit.
       	$client_visit = array(
       		'user_id' 		=> $user_id,
       		'user_name'		=> $user_name,
       		'site_id'		=> $this->input->post('site'),
       		'client_name'	=> $this->input->post('client_name'),
       		'client_qb_name'=> $this->input->post('client_qb_name'),
       		'ordid'			=> $this->input->post('ordid'),
       		'ordno'			=> $this->input->post('ordno'),
       		'street'		=> $this->input->post('street'),
       		'suburb'		=> $this->input->post('suburb'),
       		'state' 		=> $this->input->post('state'),
       		'postcode'		=> $this->input->post('postcode'),
       		'total_km'		=> $this->input->post('total_km'),
       		'phone'			=> $this->input->post('phone'),
       		'visit_date'	=> strtotime($this->input->post('visit_date')),
       		'from_time'		=> $this->input->post('from_time'),
       		'to_time'		=> $this->input->post('to_time'),
       		'reason'		=> $this->input->post('reason'),
       		'actual_from_time' => $this->input->post('actual_from_time'),
       		'actual_to_time' => $this->input->post('actual_to_time'),
       		'reimbursed'	=> $this->input->post('reimbursed'),
       		'createuser'	=> $current_user_name,
       		'createdate'	=> $now_time
       	);
       	
       	
       	
       	$start_date = strtotime($this->input->post('visit_date') ." " . $this->input->post('from_time'));
       	$end_date = strtotime($this->input->post('visit_date') ." " . $this->input->post('to_time'));
       	$hours_used = round(($end_date - $start_date)/3600,2);
       	
       	$request_data = array(
       			'user_name' 	=> $user_name,
       			'leave_type_id' => LEAVE_TYPE_ONSITE,
       			'request_type'  => 2,
       			'include_break' => 0,
       			'start_day' 	=> strtotime($this->input->post('visit_date')),
       			'end_day' 		=> strtotime($this->input->post('visit_date')),
       			'start_time' 	=> $this->input->post('from_time'),
       			'end_time' 		=> $this->input->post('to_time'),
       			'start_date' 	=> $start_date,
       			'end_date' 		=> $end_date,
       			'hours_used' 	=> $hours_used,
       			'mc_provided'	=> 0,
       			'carer_leave'	=> 0,
       			'reason' 		=> $this->input->post('reason'),
       			'add_time' 		=> $now_time,
       			'status' 		=> ACTIVE
       	);
       	
       		
       	if ($action == 'add'){
       		// Insert request.
       		$this->load->model('St_request_m', 'request_m');
       		
       		$request_data['state'] = 20;
       		$client_visit['request_id'] = $request_id = $this->request_m->insert($request_data);
       		$client_visit_id = $this->client_visit_m->insert($client_visit);
       		
       		if($request_id <= 0){
	        	$error = "Leave request save failed.";
	        }else{
	       		//add history log
	       		$this->load->model('St_request_log_m', 'request_log_m');
	       		$log = array(
	       				'id' => $request_id,
	       				'operator_id' => $this->session->userdata('user_id'),
	       				'operation' => 'apply',
	       				'operate_time' => $now_time,
	       				'operate_ip' => $this->input->ip_address()
	       		);
	       		
	       		$log = array_merge($request_data, $log);
	       		$this->request_log_m->insert($log);
	        }
       		 
       		
       	}elseif ($action != 'delete'){
       		$client_visit_id = $this->input->post('client_visit_id');
       		$this->client_visit_m->update_by_id($client_visit_id, $client_visit);
       		
       		$request_id = $this->input->post('request_id');
       		$this->request_m->update_by_user_name_and_id($user_name, $request_id, $request_data);
       	}

   		
		// Send new request email
   		if ($action == 'add'){
	   		$this->load->library('phpmail');
	   		$this->phpmail->readConfig();
	   		$user_level = $this->session->userdata("user_level");
   			
			$requestinfo = $this->request_m->get_info($request_id);
	   		
			$this->phpmail->setSubject("Client Visit id: " . $client_visit_id . " - " . $this->session->userdata('first_name') . " " . $this->session->userdata('last_name'));
	   		$mail_body['appove_url'] = site_url('approve/deal/'.$request_id);
	       	$mail_body['appove_name'] = "Approve this Client Visit here.";
			
	       	$mail_body['request'] = $requestinfo;
	       	$mail_body['order_no'] = $client_visit['ordno'];
	       	$mail_body['order_url'] = SCORPY_HOME."/orders_forms.php?type=form&ordid=".$client_visit['ordid'];
	       	$mail_body['qb_name'] = $client_visit['client_qb_name'];
	         	
	//		$this->phpmail->setBody($this->load->view('mail/client_visit', $mail_body , True));
			$this->phpmail->setBody($this->load->view('mail/request', $mail_body , True));
			
			$supervisors = $this->users_m->get_supervisors_by_user_name($user_name);
			if(!empty($supervisors)){
	   			foreach($supervisors as $supervisor){
	   				if(!empty($supervisor)){
	   					if(!empty($supervisor->email)){
	   						$this->phpmail->AddAddress($supervisor->email);
	   						$this->phpmail->AddAddress('edwards@scorptec.com.au');
				  			if(!$this->phpmail->send())
				        		log_message("debug", "send mail .... " . $this->phpmail->ErrorInfo);
				        		
							$this->phpmail->ClearAllAddresses();			        		
	   					}
	   				}
	   			}
			}
   		}
   		
   		if ($action == 'delete'){
   			$client_visit_id = $this->input->post('client_visit_id');
   			$request_id = $this->input->post('request_id');
   		
   			$this->client_visit_m->delete($client_visit_id);
   			$this->request_m->delete($request_id);
   		}   		
	 			
       	$this->client_visit_list($list);
	}
	
	function do_admin_save_form() {
		$action = $this->input->post('action');
		$list = $this->input->post('status');		
		
		if ($action == 'delete'){
			$client_visit_id = $this->input->post('client_visit_id');
			$request_id = $this->input->post('request_id');
		
			$this->client_visit_m->delete($client_visit_id);
			$this->request_m->delete($request_id);
		}else{
			$client_visit = array(
					'total_km'		=> $this->input->post('total_km'),
					'actual_km'		=> $this->input->post('actual_km'),
					'actual_from_time' => $this->input->post('actual_from_time'),
					'actual_to_time' => $this->input->post('actual_to_time'),
					'reimbursed' => $this->input->post('reimbursed'),
			);
			
			$client_visit_id = $this->input->post('client_visit_id');
			$this->client_visit_m->update_by_id($client_visit_id, $client_visit);
		}
		
		$this->client_visit_list($list);
	}
    
    function client_visit_list($status='new', $filters=array()) {
        $user_level = $this->session->userdata('user_level');
        $site_id = $this->session->userdata('site_id');
        $department_id = $this->session->userdata('department');
        
        // Set default filter value.
        if ($status == 'completed' && !array_key_exists('time_period', $filters)){
        	$filters['time_period'] = 'this_month';
        }
        
		//load site model
		$this->load->model('St_site_m', 'site_m');
		//load department model
		$this->load->model('St_department_m', 'department_m');
		
   		//read site
        $data['sites'] = $this->site_m->get_dropdown();
        
        //read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
        
        // Get user name
        $jobs = $this->client_visit_m->get_by_status($status, false, $site_id);
        // Get user_name for filter.
        $users = array("All");
        if (!empty($jobs)){
        	foreach ($jobs as $job){
        		$users[$job->user_name] = $job->first_name." ".$job->last_name;
        	}
        }        
        
        $jobs = $this->client_visit_m->get_by_status($status, $filters, $site_id);
        
        $total_km = 0;
        if (!empty($jobs)){
           	foreach ($jobs as $job){
           		$total_km += $job->total_km;
           	}
       	}
        
       	$data['selected_user'] = '';
		if (array_key_exists('user_name', $filters))
        	$data['selected_user'] = $filters['user_name'];
		
		$data['selected_time_period'] = '';
		if (array_key_exists('time_period', $filters))
			$data['selected_time_period'] = $filters['time_period'];
		
		
        $data['users'] = $users;
        $data['jobs'] = $jobs;
        
        $data['total_km'] = $total_km;
        
        $data['count'] = $count = $this->client_visit_m->get_by_status($status, false, $site_id, true);
        $data['navigation'] = "<a href='". site_url('admin/client_visit_list/'.$status) ."' target='_top'>On Site Service</a> > ".ucfirst($status);
        
        $data['status'] = $status;
        $data['user_level'] = $user_level;
        
        $page_count = 0;
    	if(($count % PAGE_SIZE) == 0){
    		$page_count = $count / PAGE_SIZE;
    	}else{
    		$page_count = floor($count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	
        $this->load->view('navigation', $data);
        $this->load->view('client_visit/client_visit_list');
    }
    
    function do_update_list(){
    	$filters = array();
    	
    	$action = $this->input->post('re');
    	$status = $this->input->post('status');
    	 
    	if (is_array($action) && !empty($action)){
	    	foreach ($action as $id){
	    		$this->client_visit_m->update_by_id($id, array('reimbursed' => 1));
	    	}
    	}
    	
    	
    	$filters['user_name'] = $this->input->post('user_name');
    	$filters['time_period'] = $this->input->post('time_period');
    	
    	$this->client_visit_list($status, $filters);
    }
    


    function new_approvals_ajax(){
    	log_message("debug", "new_approvals_ajax .....");
		$user_level = $this->session->userdata('user_level');
		
    	$data['form_id'] = 'new';
    	
    	$site_id = $this->input->post('new_filter_site_id');
    	$department_id = $this->input->post('new_filter_department_id');
    	$page_no = $this->input->post('new_page_no');
        
		$limit = $this->input->post('new_limit');
    	$data['page_limit'] = $limit;
    	
		$data['jobs'] = $this->job_incident_m->get_approvals_by_new($user_level, $site_id, $department_id, $page_no, $limit);
		$total_count = $this->job_incident_m->count_approvals_by_new($user_level, $site_id, $department_id);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
		
		$content = $this->load->view('jobincident/list_approve_job_incident_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }
    
    function progress_approvals_ajax(){
    	log_message("debug", "progress_approvals_ajax .....");
		$user_level = $this->session->userdata('user_level');
		
    	$data['form_id'] = 'progress';
    	
    	$site_id = $this->input->post('progress_filter_site_id');
    	$department_id = $this->input->post('progress_filter_department_id');
    	$page_no = $this->input->post('progress_page_no');
        
		$limit = $this->input->post('progress_limit');
    	$data['page_limit'] = $limit;
    	
		$data['jobs'] = $this->job_incident_m->get_approvals_by_progress($user_level, $site_id, $department_id, $page_no, $limit);
		$total_count = $this->job_incident_m->count_approvals_by_progress($user_level, $site_id, $department_id);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
		
		$content = $this->load->view('jobincident/list_approve_job_incident_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }
    
    function completed_approvals_ajax(){
    	log_message("debug", "completed_approvals_ajax .....");
		$user_level = $this->session->userdata('user_level');
		
    	$data['form_id'] = 'completed';
    	
    	$site_id = $this->input->post('completed_filter_site_id');
    	$department_id = $this->input->post('completed_filter_department_id');
    	$page_no = $this->input->post('completed_page_no');
        
		$limit = $this->input->post('completed_limit');
    	$data['page_limit'] = $limit;
    	
		$data['jobs'] = $this->job_incident_m->get_approvals_by_completed($user_level, $site_id, $department_id, $page_no, $limit);
		$total_count = $this->job_incident_m->count_approvals_by_completed($user_level, $site_id, $department_id);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
		
		$content = $this->load->view('jobincident/list_approve_job_incident_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }
    

}