<?php

class Report extends CI_Controller {
	
	function Report() {
		parent::__construct();
		$this->load->helper ( 'url' );
		$this->load->helper ( array ('form', 'url' ) );
		$this->load->library ( 'form_validation' );
		$this->form_validation->set_error_delimiters ( "<div style='font:Tahoma, Geneva, sans-serif; font-size:11px; color:#F00'>", "</div>" );
	}
	
	function index() {
		$data['navigation'] = "<a href='#'>Leave Request</a> > Report";
        $this->load->view('navigation', $data);
		$this->load->view('request/report');
	}
}