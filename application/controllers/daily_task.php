<?php


class daily_task extends CI_Controller {
	
	function daily_task(){
		parent::__construct();
		
		
		$this->load->model('St_daily_log_m', 'daily_log_m');
	}
	
	
	function daily_log_email($date=false, $user_id=false, $output='email'){
		$this->load->model('st_request_m', 'request_m');
		$this->load->model('st_users_m', 'users_m');
		$this->load->model('st_publicholiday_m', 'publicholiday_m');
		$this->load->model('st_ac_clocking_log_m', 'ac_clocking_m');
		
		$this->load->library('actatek');

		if ($output=='email'){
			$this->load->library('phpmail');
			$this->phpmail->readConfig();
		}		
		
		
		
		// Don't run if public holiday.
		if ($date){
			$is_holiday = $this->publicholiday_m->check_except(date("Y-m-d", $date));
				
			if($is_holiday > 0)
				return false;
				
			$u_date = $date;
			
			$start_time = date('U',strtotime(date("Y-m-d", $date)." 00:00:00"));
			$end_time = date('U',strtotime(date("Y-m-d", $date)." 23:59:59"));
				
			$at_start_time = date("Y-m-d", $date)." 00:00:00";
			$at_end_time = date("Y-m-d", $date)." 23:59:59";
				
		}
		else{
		
			$is_holiday = $this->publicholiday_m->check_except(date("Y-m-d"));
			
			if($is_holiday > 0)
				return false;
	
	   		$u_date = date('U',strtotime(date("Y-m-d")));
	   		
//	   		$u_date = date('U',strtotime(date("2015-07-06")));
			
 	   		$start_time = date('U',strtotime(date("Y-m-d")." 00:00:00"));  // Add 2 hours prevent wrong day when daylight saving start.
 	   		$end_time = date('U',strtotime(date("Y-m-d")." 23:59:59"));
	   		
//  	   		$start_time = date('U',strtotime(date("2015-07-06")." 00:00:00"));
//  	   		$end_time = date('U',strtotime(date("2015-07-06")." 23:59:59"));
	   		
			
  	   		$at_start_time = date("Y-m-d")." 00:00:00";
  	   		$at_end_time = date("Y-m-d")." 23:59:59";
	   		
//  	   		$at_start_time = date("2015-07-06")." 00:00:00";
//  	   		$at_end_time = date("2015-07-06")." 23:59:59";	   		
		}
     		 
		
		
		$users = $this->users_m->get_active_users($start_time, $end_time, $site_id=false, false, $user_id);
		
		$body = false;
		$admin_body = false;
		$messages = array();

		foreach ($users as $user){
			if ($user->actatek_id){
//				if ($user->user_name=='shaunm'){

					$in_count = 0;
					$in_time = 0;
					$out_count = 0;
					$out_time = 0;
					$at_lunch = 0;
					$request_hour = 0;
					$slot_hour = 0;
					$lunch_out_time = false;
					$messages = array();
					$messg = false;
					$logs = array();
					$at_logs = array();
					$at_body = false;
					$ins = array();
					$outs = array();
					$request_body = false;
					$slot_start_time = false;
					$slot_end_time = false;
					$lunch_hour = false;
					$missed_clocked = false;
					$no_clocks = false;
					
					echo "<br><br>".$user->user_name."<br>";

					$slots = $this->get_daily_shift($user->id, $user->user_name, $start_time, $end_time);
					
					if ($slots){
						$slot_start_time = $slots->start_time;
						$slot_end_time = $slots->end_time;
						$lunch_hour = $slots->lunch_hour;
						
						$slot_hour = ( date("U", strtotime($slot_end_time)) - date("U", strtotime($slot_start_time)) ) / 3600;
						$slot_hour -= $lunch_hour;
						
 						echo "Slot hour: ". $slot_hour."<br>";
						
 						echo $slot_start_time."<br>";
 						echo $slot_end_time."<br>";
 						echo $slots->wday."<br>";
					}
						
					
					$requests = $this->request_m->get_requests_daily('20,40', $user->user_name, 0, $start_time, $end_time);
					
					
					if (!empty($requests)){
						foreach ($requests as $request){
//							if ($request->leave_type_id != LEAVE_TYPE_INTERSTORE){
							
								if ($request->request_type == 1){
 									echo $request->leave_type ."<br>";
 									echo 'Request: All Day <br>' ;
	
									// Overtime is adding slot time.
									if ($request->leave_type_id == LEAVE_TYPE_OVERTIME){
										$slot_start_time = $request->end_time;
										$slot_end_time = $request->end_time;
									}else{
										$slot_start_time = 0;
										$slot_end_time = 0;
									}
								}elseif ($request->request_type == 2){
 									echo $request->leave_type ."<br>"; 
 									echo 'Request Start: '.$request->start_time.' End: '.$request->end_time.'<br>' ;
									
									if ($request->leave_type_id == LEAVE_TYPE_LATE){
										$slot_start_time = $request->start_time;
									}else{
										$request_start = date("U", strtotime($request->start_time));
										$slot_start = date("U", strtotime($slot_start_time));
										
										$request_end = date("U", strtotime($request->end_time));
										$slot_end = date("U", strtotime($slot_end_time));
										
										if ($request->include_break == 1)
											$request_start -= $lunch_hour ? $lunch_hour * 3600 : 3600 ;
										
										// Overtime is adding slot time.
										if ($request->leave_type_id == LEAVE_TYPE_OVERTIME){
											if ($request_start >= $slot_start && $request_end >= $slot_end){
												$slot_start_time = $request->end_time;
											}elseif ($request_end <= $slot_end && $request_start <= $slot_start){
												$slot_end_time = $request->start_time;
											}
										}else{
											if ($request_start > $slot_start && $request_end < $slot_end){
												$lunch_hour += $request->hours_used;
											}elseif ($request_start <= $slot_start && $request_end <= $slot_end){
												$slot_start_time = $request->end_time;
											}elseif ($request_end >= $slot_end && $request_start >= $slot_start){
												$slot_end_time = $request->start_time;
											}
											
											$request_hour += $request->hours_used;
										}
									}
									
									if ($slot_start_time == $slot_end_time){
										$slot_start_time = 0;
										$slot_end_time = 0;
									}
								}
//							}
							
							$request_body .= "	<tr>
													<td>".$request->leave_type."</td>
													<td>".date(DATETIME_FORMAT, $request->start_date)."</td>
													<td>".date(DATETIME_FORMAT, $request->end_date)."</td>
													<td>".$request->hours_used."</td>
												</tr>";
							
						}
					}
					
 					echo "Total request hour: " . $request_hour."<br>";
 					echo $slot_start_time ? $slot_start_time."<br>" : "N/A<br>";
 					echo $slot_end_time ? $slot_end_time."<br>" : "N/A<br>";
					
 					echo $lunch_hour ? "Lunch: ".$lunch_hour."<br>" : "Lunch: N/A<br>";
					
					
					// Get actatek log.
					if ($output=='email'){
						// Get actual clock in/out from actatek
						$logs = $this->actatek->get_logs($user->actatek_id, $at_start_time, $at_end_time);
  					}else{
 						$logs = array();
						if ($date){
							// Get clock in/out from st_ac_clocking_log table
 							$clocks = $this->ac_clocking_m->get_actual_clock_by_user($user->user_name, $date, $date);
						}
						if ($clocks){
							foreach ($clocks as $clock){
								$log = array(	"trigger" 	=> $clock->direction,
												"timestamp" => $clock->time);
								
								array_push($logs, $log);
							}	
						}else{
							$no_clocks = true;
						}
 					}
					
//  					var_dump($logs);
//  					var_dump("<br/><br/>");
					
					$i = 0;
					foreach ($logs as $log){
//						echo $log['trigger'].": ".$log['timestamp']."<br>";
						
						if ($log['trigger'] == 'OUT' && strtotime($slot_start_time) <= date("U", strtotime($log['timestamp']))){
							$lunch_out_time = date("U", strtotime($log['timestamp']));
						}elseif ($lunch_out_time && $log['trigger'] == 'IN'){
							$lunch_in_time = date("U", strtotime($log['timestamp']));
							
							if ($lunch_in_time >= date("U", strtotime($slot_start_time)) && $lunch_out_time <= date("U", strtotime($slot_end_time)))
								$at_lunch += $lunch_in_time - $lunch_out_time;
						}
						
						if ($log['trigger'] == 'IN'){
							$in_count++;
							if ($i==0)
								$in_time = $log['timestamp'];
								
							array_push($ins, $log['timestamp']);
						}else{
							$out_count++;
							$out_time = $log['timestamp'];
							
							array_push($outs, $log['timestamp']);
						}
						
 						// Insert to st_ac_clocking_log table
 						if ($output=='email'){
  							var_dump($log);
		  					var_dump("<br/><br/>"); 							
							$clock_data = array(
								'user_id'	=> $user->id,
								'user_name' => $user->user_name,
								'date'		=> $u_date,
								'time'		=> $log['timestamp'],
								'direction' => $log['trigger'],
								'location'  => $log['location']
							);
							
							
							$this->ac_clocking_m->insert($clock_data);
 						}
 						
						$i++;
					}

					// Construct table rows
					$at_body = "<tr>";
					$at_body .= "<td>IN</td>";
					foreach ($ins as $in){
						$at_body .= "<td>".$in."</td>";
					}
					$at_body .= "</tr>";
					$at_body .= "<tr>";
					$at_body .= "<td>OUT</td>";
					foreach ($outs as $out){
						$at_body .= "<td>".$out."</td>";
					}
					$at_body .= "</tr>";
					
					$at_lunch = round(($at_lunch)/3600,2);
					
// 					echo "Lunch Hour: ".$at_lunch."<br>";
					
// 					echo $in_time."<br>";
// 					echo $out_time."<br>";
					
// 					echo $in_count."<br>";
// 					echo $out_count."<br>";
					
					
//					echo date("U", strtotime($out_time)) ."<br>";
//					echo date("U", strtotime($slot_end_time));

					// Log daily
					
					
					
					
					// Miss clock in/out
					if ($in_count != $out_count){
						$messg = "Missed Clock In/Out";
						array_push($messages, array("messg"=>$messg,"exp_time"=>'&nbsp;',"act_time"=>'&nbsp;'));
					}
					// Late in or early out
					if (date("U", strtotime($in_time)) - date("U", strtotime($slot_start_time)) > 300 && $slot_start_time && $request_hour < $slot_hour){
						$messg = "Late Clock In";
						$exp_time = $slot_start_time;
						$act_time = $in_time; 
						array_push($messages, array("messg"=>$messg,"exp_time"=>$exp_time,"act_time"=>$act_time));
					}
					if (date("U", strtotime($slot_end_time)) - date("U", strtotime($out_time)) > 60 && $slot_end_time && $request_hour < $slot_hour && $out_time){
						$messg = "Early Clock Out";
						$exp_time = $slot_end_time;
						$act_time = $out_time;
						array_push($messages, array("messg"=>$messg,"exp_time"=>$exp_time,"act_time"=>$act_time)); 
					}
					
					if (!$in_time && !$out_time && $slot_start_time && $slot_end_time && !$no_clocks){
						$messg = "No Clock In/Out";
						$exp_time = $slot_start_time;
						$act_time = "--";
						array_push($messages, array("messg"=>$messg,"exp_time"=>$exp_time,"act_time"=>$act_time));
					}
					
					// Long lunch
					if (($at_lunch - $lunch_hour) > 0.02  && $lunch_hour != 0){
						$messg = "Long Lunch";
						$exp_time = $lunch_hour . " hour";
						$act_time = $at_lunch . " hour";
						array_push($messages, array("messg"=>$messg,"exp_time"=>$exp_time,"act_time"=>$act_time));
					}
					
					if ($messages){
						$msg_user['actatek_id'] = $user->actatek_id;
						$msg_user['name'] = $user->first_name." ".$user->last_name;
						$msg_user['email'] = $user->email;
					}
//				}

					
				// Send email
				if ($messages){
					$body = "";
					if ($date)
						$body .= "<h3 style='color: #182f3a;'>".date("D d-m-Y", $date)."</h3>";
						
					$body .= "<h4>Id: ".$msg_user['actatek_id']." Name: ".$msg_user['name']."</h4>";
					$body .= "	<table border='1' cellpadding='5'>
							<tr>
								<th>Reason</th><th>Expected</th><th>Actual</th>
							</tr>";
					foreach ($messages as $messg){
						$body .= "	<tr>
								<td>".$messg['messg']."</td>
								<td>".$messg['exp_time']."</td>
								<td>".$messg['act_time']."</td>
							</tr>";
							
				
					}
					$body .= "</table><br/>";
				
					if ($request_body){
						$body .= "<h4>LRS Request</h4>";
						$body .= "<table border='1' cellpadding='5'>
								<tr>
									<th>Leave Type</th><th>Start</th><th>End</th><th>Hour</th>
								</tr>";
						;
						$body .= $request_body;
						$body .= "</table><br/><br/>";
					}
				
					if ($at_body){
						$body .= "<h4>Actatek Log</h4>";
						$body .= "<table border='1' cellpadding='5'>";
						$body .= $at_body;
						$body .= "</table><br/>";
					}
				
					$body .= "<hr/>";
				
					$admin_body .= $body;
				
					// Send email for each staff.
					if ($output=='email'){
						$staff_body = "<html><body>";
						$staff_body .= "<h1>".date("d-m-Y")."</h1><br/>";
						$staff_body .= $body;
					
						$staff_body .= "<br/><br/><a href='http://www.lrs.local/index.php/request/add'>Create Request</a>";
						$staff_body .= "</body>";
					
					
					
						 
						$this->phpmail->ClearAllAddresses();
//						$this->phpmail->AddAddress("edwards@scorptec.com.au");
						$this->phpmail->AddAddress($msg_user['email']);
					
						$this->phpmail->setSubject("LRS Staff Report");
						$this->phpmail->setBody($staff_body);
					
				        $this->phpmail->send();
					}
				}
			}
		}
		
		if ($admin_body){
			
			$mail_body = "<html><body>";
			if (!$date)
				$mail_body .= "<h1>".date("d-m-Y")."</h1><br/>";
//			$mail_body .= "<h1>".$at_start_time."</h1><br/>";
			$mail_body .= $admin_body;
			$mail_body .= "</body>";
			
			if ($output=='email'){
		   		$this->phpmail->ClearAllAddresses();
		        $this->phpmail->AddAddress("edwards@scorptec.com.au");
		        $this->phpmail->AddAddress("hrm@scorptec.com.au");
		        
		        $this->phpmail->setSubject("LRS Daily Report");
		        $this->phpmail->setBody($mail_body);
		        
		        $this->phpmail->send();
			}
			elseif ($output=='screen'){
	        
	        	echo $mail_body;
			}
	

		}
	}
	
	
	function get_daily_shift($user_id, $user_name, $start_time, $end_time){
		$payrollshift = false;
		$slots = array();
		
		$this->object =& get_instance();
		$this->object->load->database();
		
		$this->load->model('St_user_labor_m', 'user_labor_m');
		$this->load->model('St_payroll_m', 'payroll_m');
		
 		$labor = $this->user_labor_m->get_by_end_time($user_id, $end_time);
		
		if ($labor){
		
			$sql = "SELECT * FROM st_payrollshift AS pay " .
					" WHERE pay.id = " . $labor->payrollshift_id;
	
			$query = $this->object->db->query($sql);
			$payrollshift = $query->row();
		} 
		
		$payroll = $this->payroll_m->get_last();
		
		if ($payrollshift){
		
			$payroll_date = $payroll->start_date;
//			$payroll_date = $payrollshift->start_date;
			$rolldays = $payrollshift->rolldays;
			
			$start_time += 7200; // Add 2 hours fix daylight saving issue.
			$start_day_no = intval(floor(($start_time - $payroll_date) / 86400));
			$remainder = $start_day_no % $rolldays;
			$start = $remainder * 24;
			
/* 			echo $end_time .'<br/>';
			echo $payroll_date .'<br/>';
			echo $start_day_no .'<br/>';
			echo $remainder .'<br/>';
			echo $start .'<br/>';
 */			
			$sql = "SELECT slot.*, roll.name AS wday, shift.lunch_hour " .
						" FROM st_payrollshift_rolls AS roll, st_shift_category AS cat, st_daily_shift_slot AS slot, st_daily_shift AS shift " .
						" WHERE roll.payrollshift_id = " . $payrollshift->id .
						" AND roll.start = " . $start . 
						" AND roll.daily_shift_id = slot.daily_shift_id " .
						" AND roll.daily_shift_id = shift.id " .
						" AND cat.total IN (2,3) AND slot.shift_category_code = cat.code ";
			$rquery = $this->object->db->query($sql);
			$slots = $rquery->result();
		}
		
		if ($slots)
			return $slots[0];
		else
			return false;
		
	}
	
	function exception_report($start_time, $end_time, $user_id=false){
		if ($user_id == 0) $user_id = false;
		
		while ($start_time <= $end_time){
//			echo date("Y-m-d", $start_time) ."<br/>";
			
			$this->daily_log_email($start_time, $user_id, $output='screen');

			

//			$start_time += 86400;
			
			$start_datetime = new DateTime(date(DATE_FORMAT, $start_time));
			$start_datetime->modify('+1 day');
			$start_time = $start_datetime->format('U');
				
		}
		
		
	}
	
}
?>