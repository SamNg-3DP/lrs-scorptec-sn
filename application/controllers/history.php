<?php

class History extends CI_Controller {
	
	function History() {
		parent::__construct();
		if(!$this->userauth->logined()){
			$infoMsg = array(
				'info' => 'Please login !'
			);
			$this->session->set_flashdata('auth', $this->load->view('msgbox/info', $infoMsg , True));
			$this->session->set_flashdata('back_url', current_url());
			redirect("login");
		}else{
			if(!$this->userauth->judge_priv("history")){
				$errorMsg = array(
					'error' => 'You have no privilege into this page !'
				);
				$this->session->set_flashdata('auth', $this->load->view('msgbox/error', $errorMsg , True));
				redirect("msgbox", 'location');
			}
		}
	}
	
	function index() {
		$this->request();
	}
	
	function request(){
		$data['navigation'] = "<a href='". site_url('admin/leave') ."' target='_top'>Leave Request</a> > <a href='". site_url('history/request') ."'>Leave Request History</a>";
        $data['form_id'] = 'saved';

        $data['operations'] = $this->_get_request_action_dropdown();
        
		$data['approvals'] = "";
		
    	$this->load->view('navigation', $data);
    	$this->load->view('request/history', $data);
	}
	
	function request_ajax(){
		log_message("debug", "<<<request_ajax");
		
		//load request model
		$this->load->model('St_request_log_m', 'request_log_m');
		
    	$data['form_id'] = 'saved';
        $order_by = $this->input->post('saved_order_by');
        
        $order = $this->input->post('saved_order');
    	$request_id = $this->input->post('saved_filter_request_id');
    	$first_name = $this->input->post('saved_filter_first_name');
    	$last_name = $this->input->post('saved_filter_last_name');
    	$from_date = $this->input->post('saved_filter_from_date');
    	$operation = $this->input->post('saved_filter_operation');
    	$page_no = $this->input->post('saved_page_no');
        
        $request_id = strtolower( $request_id );
        $str_start = substr($request_id, 0, 1);
        if($str_start == "r"){
        	$request_id = substr($request_id, 1, strlen($request_id));
        }       
        
		$limit = $this->input->post('saved_limit');
    	$data['page_limit'] = $limit;
        log_message("debug", "page limit ====" . $limit);
        
    	$operate_start_date = 0;
    	$operate_end_date = 0;
    	if(!empty($from_date)){
    		$operate_start_date = strtotime($operate_start_date);
    		$operate_end_date = strtotime($operate_start_date . " 23:59:59");
    	}
    	
    	$total_count = $this->request_log_m->count_requests_by_search($request_id, $first_name, $last_name, $operate_start_date, $operate_end_date, $operation);
		
		$request_logs = $this->request_log_m->get_by_search($request_id, $first_name, $last_name, $operate_start_date, $operate_end_date, $operation, $page_no, $limit);
		$log_list = array();
		if($total_count){
			foreach($request_logs AS $request_log){
				$log['request_id'] = $request_log->id;
				//Action
				$operation = $request_log->operation;
				$action = "";
				if("save" == $operation){
					$action = "saved new request";
				}else if("apply" == $operation){
					$action = "applied request";
				}else if("update" == $operation){
					$action = "updated request";
				}else if("approve" == $operation){
					$action = "approved request";
				}else if("rejected" == $operation){
					$action = "rejected request";
				}else if("cancel" == $operation){
					$action = "canceled request";
				}
				$log['action'] = $action;
				//Actioned On
				$log['actioned_on'] = date(DATETIME_FORMAT, $request_log->operate_time);
				//Actioned By
				$log['actioned_by'] = $request_log->oprator_first_name . " " . $request_log->oprator_last_name;
				$log_list[] = $log;
			}
		}
		
		$data['requests'] = $log_list;
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
        
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	$str = $this->load->view('request/history_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }
    
    /*
     * get operation dropdwon
     */
	function _get_request_action_dropdown(){
    	return array(
			''  => 'Please select',
			'new' 	=> 'New',
    		'update' 	=> 'Update',
			'apply' 	=> 'Apply',
    		'approve' 	=> 'Approve',
    		'rejected' 	=> 'Rejected',
    		'cancel' 	=> 'Cancel'
		);
    }
    
    
    function exception_report(){
    	
    	$data['form_id'] = "exception";
    	
    	
    	$time_period_id = $this->input->post('exception_time_period_id');
    	$user_id = $this->input->post('user_id');
    	
    	if (empty($time_period_id))
    		$time_period_id = "yesterday";
    	
    	$time_period = get_time_period($time_period_id);
    	
    	$data['selected_time_period'] = $time_period_id;
    	$data['selected_user'] = $user_id;
    	
    	$data["start_date"] = $time_period[0];
    	$data["end_date"] = $time_period[1];
    	
    	if ($this->session->userdata('user_level') >= 2){
    		$this->load->model('St_users_m', 'users_m');
    		 
    		$user_dept = $this->session->userdata('department_id');
    		// Show all users for accounts user admin level
    		if ($this->session->userdata('user_level') >= 4)
    			$users = $this->users_m->get_select_dropdown();
    		else
    			$users = $this->users_m->get_select_dropdown(false, $user_dept);
    		 
    		$data['users'] = $users;
    	}
    	 
    	
    	
    	$this->load->view('exception', $data);
    	 
    }
    
}