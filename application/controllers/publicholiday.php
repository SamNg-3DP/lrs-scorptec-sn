<?php
/**
 * payroll shift
 */
class Publicholiday extends CI_Controller {
	
	function Publicholiday() {
		parent::__construct();
		
		$this->holidays = array("New Year" => "New Year", 
								"Australia Day" => "Australia Day",
								"Labour Day" => "Labour Day", 
								"Good Friday" => "Good Friday", 
								"Easter Saturday" => "Easter Saturday",
								"Easter Monday" => "Easter Monday",
								"ANZAC Day" => "ANZAC Day",
								"Queen's Birthday" => "Queen's Birthday",
								"Grand Final Eve" => "Grand Final Eve",
								"Melbourne Cup" => "Melbourne Cup",
								"Christmas Day" => "Christmas Day",
								"Boxing Day" => "Boxing Day");
		
		
		if(!$this->userauth->logined()){
			$infoMsg = array(
				'info' => 'You have no privilege into this page !'
			);
			$this->session->set_flashdata('auth', $this->load->view('msgbox/info', $infoMsg , True));
			redirect("login");
		}else{
			if(!$this->userauth->judge_priv("publicholiday")){
				$errorMsg = array(
					'error' => 'You have no privilege into this page !'
				);
				$this->session->set_flashdata('auth', $this->load->view('msgbox/error', $errorMsg , True));
				redirect("msgbox", 'location');
			}
		}
		$this->form_validation->set_error_delimiters ( "<div style='font:Tahoma, Geneva, sans-serif; font-size:11px; color:#F00'>", "</div>" );
		//load St_payroll_m model
		$this->load->model('St_publicholiday_m', 'publicholiday_m');
	}
	
	function index() {
		$year = $this->input->post('years');
		$year = empty($year) ? date('Y') : $year;
		
		$data['publicholidays'] = $this->publicholiday_m->get_publicholidays($year);
		
		$years = array(date('Y')-1 => date('Y')-1, date('Y') => date('Y'), date('Y')+1 => date('Y')+1, date('Y')+2 => date('Y')+2);
		$data['years'] = $years;
		$data['year'] = $year;
			
		$data ['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > Shift/Roster > <a href='".site_url('publicholiday')."'>Public Holiday</a> > List";
		$this->load->view ( 'navigation', $data );
		$this->load->view ( 'publicholiday/list_publicholiday', $data );
	}
	
	function add() {
		$data ['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > Shift/Roster > <a href='".site_url('publicholiday')."'>Public Holiday</a> > Add New";
		$data['holidays'] = $this->holidays;
		$this->load->view ( 'navigation', $data );
		$this->load->view ( 'publicholiday/add_publicholiday' );
	}
	
	function do_add() {
		$publicholiday = array(
			'holiday_name' => $this->input->post('holiday_name'),
			'date' => strtotime($this->input->post('holiday_date')),
		);
		
		$insert_id = $this->publicholiday_m->insert($publicholiday);
		
		$this->index ();

	}
	
	function remove($publicholiday_id) {
		$this->publicholiday_m->delete ( $publicholiday_id );
		$this->index ();
	}
	

	/**
     * get departments by site id
     * @return json
     */
    function get_payrollshifts_ajax(){
    	$site_id = $this->input->post('site_id');
    	$select = $this->input->post('select');
    	$result = array();
    	$index = 0;
	    $result[$index]['id'] = "";
	    if($select){
	    	$result[$index]['code'] = "Please select..";
	    }else{
	    	$result[$index]['code'] = "All dept.";
	    }
    	if(!empty($site_id)){
	    	$payrollshifts = $this->publicholiday_m->get_by_site($site_id);
	    	foreach ($payrollshifts as $row){
	    		$index++;
	    		$result[$index]['id'] = $row->id;
	    		$result[$index]['code'] = $row->payrollshiftcode;
	    	}
    	}
    	echo json_encode($result);
    }

	/**
	 * check whether payrollshift code is unique 
	 * @return json
	 */
	function check_code_ajax(){
		$date = $this->input->post('holiday_date');
        $num = $this->publicholiday_m->check_except($date);
        $result['status'] = "ok";
        if($num > 0){
        	$result['status'] = "exist";
        }
        
        echo json_encode($result);
	}
    
}
