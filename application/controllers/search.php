<?php

class Search extends CI_Controller {

    function Search() {
        parent::__construct();
        
        if(!$this->userauth->logined()){
        	$infoMsg = array(
        			'info' => 'You have no privilege into this page !'
        	);
        	$this->session->set_flashdata('auth', $this->load->view('msgbox/info', $infoMsg , True));
        	redirect("login");
        }else{
        	if(!$this->userauth->judge_priv("search")){
        		$errorMsg = array(
        				'error' => 'You have no privilege into this page !'
        		);
        		$this->session->set_flashdata('auth', $this->load->view('msgbox/error', $errorMsg , True));
        		redirect("msgbox", 'location');
        	}
        }        
        
        //load request model
		$this->load->model('St_request_m', 'request_m');
		
		//load department model
		$this->load->model('St_department_m', 'department_m');
		
		//load leave type model
		$this->load->model('St_leave_type_m', 'leave_type_m');
    }

    function index1() {
        $data['navigation'] = "<a href='". site_url('admin/leave') ."' target='_top'>Leave Request</a> > <a href='". site_url('search') ."'>Search</a> > Filter";
        
        $data['form_id'] = 'search';
        
        //read leave types
        $data['leave_types'] = $this->leave_type_m->get_dropdown();
        
        //states
        $data['states'] = $this->_get_state_dropdown();
        
        //read department
        $data['departments'] = $this->department_m->get_dropdown();
        
        //default own department
        $department_id = $this->session->userdata('department');
    	if($this->session->userdata('user_level') == 3 
    		|| $this->session->userdata('user_level') == 4){
    		$department_id = "";
    	}
    	$data['department_id'] = $department_id;
    	
        $data['requests'] = $this->request_m->get_requests_by_search($department_id);
		$total_count = $this->request_m->count_requests_by_search($department_id);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	
        $this->load->view('navigation', $data);
        $this->load->view('request/search', $data);
    }
	/**
     * list request which are saved, but not apply
     */
    function index(){
    	$data['navigation'] = "<a href='". site_url('admin/leave') ."' target='_top'>Leave Request</a> > <a href='". site_url('search') ."'>Search</a> > Filter";
    	$site_id = $this->session->userdata('site_id');
    	$data['form_id'] = 'saved';
    	//read site
    	$this->load->model('St_site_m', 'site_m');
        $data['sites'] = $this->site_m->get_dropdown();
		//read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
        
        //read leave types
        $data['leave_types'] = $this->leave_type_m->get_dropdown();
		
        //states
        $data['states'] = $this->_get_state_dropdown();
        
    	$department_id = $this->session->userdata('department');
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	if($this->session->userdata('user_level') == 3 
    		){
    		$department_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		
		$data['approvals'] = $this->request_m->get_requests_by_search($site_id, $department_id);
		$total_count = $this->request_m->count_requests_by_search($site_id, $department_id);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	$this->load->view('navigation', $data);
    	$this->load->view('request/search', $data);
    }
    
	function search_ajax(){
		log_message("debug", "<<<search_ajax");
    	$data['form_id'] = 'saved';
        $order_by = $this->input->post('saved_order_by');
        
        $order = $this->input->post('saved_order');
        $site_id = $this->input->post('saved_filter_site_id');
    	$department_id = $this->input->post('saved_filter_department_id');
    	$leave_type_id = $this->input->post('saved_filter_leave_type_id');
    	$first_name = $this->input->post('saved_filter_first_name');
    	$last_name = $this->input->post('saved_filter_last_name');
    	$from_date = $this->input->post('saved_filter_from_date');
    	$to_date = $this->input->post('saved_filter_to_date');
    	$state = $this->input->post('saved_filter_state');
    	$time_period = $this->input->post('saved_filter_time_period');
    	$page_no = $this->input->post('saved_page_no');
        
		if(!empty($from_date)){
    		$from_date = strtotime($from_date);
    	}
		if(!empty($to_date)){
    		$to_date = strtotime($to_date);
    	}
    	
    	log_message("debug", "<<< Time Period " . $time_period);
    	
    	if (!empty($time_period)){
	    	$time_period = get_time_period($time_period);
	    	$from_date = $time_period[0];
	    	$to_date = $time_period[1];
    	}
    	
    	
    	
		$limit = $this->input->post('saved_limit');
    	if(empty($limit)){
    		$limit = PAGE_SIZE;
    	}
    	
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
		$data['approvals'] = $this->request_m->get_requests_by_search($site_id,$department_id, $first_name, $last_name, $leave_type_id, $state,  $from_date, $to_date, $order_by, $order, $page_no, $limit);
		$total_count = $this->request_m->count_requests_by_search($site_id,$department_id, $first_name, $last_name, $leave_type_id, $state,  $from_date, $to_date);
		
    	$page_count = 0;
    	if(($total_count % $limit) == 0){
    		$page_count = $total_count / $limit;
    	}else{
    		$page_count = floor($total_count / $limit) + 1;
    	}
    	
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
        
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	$str = $this->load->view('request/search_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }

    function close() {
        $this->index();
    }
    
    function _get_state_dropdown(){
    	return array(
			''  => 'All',
			'10' => 'New',
			'20' => 'Waiting Approval',
    		'40' => 'Approved',
    		'50' => 'Rejected'
		);
    }
}