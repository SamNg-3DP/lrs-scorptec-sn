<?php

class Approve extends CI_Controller {

    function Approve() {
        parent::__construct();
    	if(!$this->userauth->logined()){
			$infoMsg = array(
				'info' => 'Please login !'
			);
			$this->session->set_flashdata('auth', $this->load->view('msgbox/info', $infoMsg , True));
			redirect("login");
		}else{
			if(!$this->userauth->judge_priv("approve")){
				$errorMsg = array(
					'error' => 'You have no privilege into this page !'
				);
				$this->session->set_flashdata('auth', $this->load->view('msgbox/error', $errorMsg , True));
				redirect("msgbox", 'location');
			}
		}
		
		$this->form_validation->set_error_delimiters('<p class="error">', '</p>');
		
		//load request model
		$this->load->model('St_request_m', 'request_m');
		//load site model
		$this->load->model('St_site_m', 'site_m');
		//load department model
		$this->load->model('St_department_m', 'department_m');
		//load leave type model
		$this->load->model('St_leave_type_m', 'leave_type_m');
    }

    function index() {
		$data['navigation'] = "<a href='". site_url('admin/leave') ."' target='_top'>Leave Request</a> > <a href='". site_url('approve') ."'>Leave Approval</a> > Current Leave Records";
		//read each leave request state count
    	$approval_counts = $this->request_m->get_approvals_counts($this->session->userdata('user_name'), $this->session->userdata('user_level'),$this->session->userdata('department'));
    	$new_count = 0;
    	$not_approved_count = 0;
    	$approved_count = 0;
    	$waiting_count = 0;
    	$manager_waiting_count = 0;
    	$mc_waiting_count = 0;
    	foreach ($approval_counts as $row){
    		if($row->state == 10){
    			$new_count = $row->icount;
    		}else if($row->state == 20 || $row->state == 30){
    			$waiting_count += $row->icount;
    			if ($row->state == 30)
    				$manager_waiting_count += $row->icount;
    		}else if($row->state == 40){
    			$approved_count = $row->icount;
    		}else if($row->state == 50){
    			$not_approved_count = $row->icount;
    		}
    	}
    	
    	$mc_waiting_count = $this->request_m->get_waiting_mc(true);
    	
		$data['new_count'] = $new_count;
		$data['not_approved_count'] = $not_approved_count;
		$data['approved_count'] = $approved_count;
		$data['waiting_count'] = $waiting_count;
		$data['mc_waiting_count'] = $mc_waiting_count;
		
		$department = $data['current_user_dept'] = $this->session->userdata('department');
    	if(($this->session->userdata('user_level') == ROLE_OPERATION &&  $this->session->userdata('department') != 'Sales')
    		|| $this->session->userdata('user_level') == ROLE_ADMIN){
    		$department = "";
    	}
    	
    	
    	$data['manager_waiting_count'] = $manager_waiting_count;
    	$data['current_user_level'] = $this->session->userdata('user_level');
    	
		$upcoming_count = $this->request_m->count_upcoming_approved($this->session->userdata('user_level'), 0, $department);
		$data['upcoming_count'] = $upcoming_count;
    	
        $this->load->view('navigation', $data);
		$this->load->view('request/approve');
    }
    
    /**
     * past leave records
     */
    function past() {
		$data['navigation'] = "<a href='". site_url('admin/leave') ."' target='_top'>Leave Request</a> > <a href='". site_url('approve/past') ."'>Past Leave Records</a> > List";
		//read each leave request state count
    	
    	if($this->session->userdata('user_level') < ROLE_OPERATION){
			$department_id = $this->session->userdata('department_id');
    	}else{
    		$department_id = 0;
    	}
    	
		$approved_counts = $this->request_m->get_past_approved_counts($department_id);
		$total_count = 0;
    	$past_annual_count = 0;
    	$past_personal_count = 0;
    	$past_late_count = 0;
    	$past_lunch_count = 0;
    	$past_training_count = 0;
    	$past_client_count = 0;
    	$past_overtime_count = 0;
    	$past_unpaid_count = 0;
    	$past_clock_count = 0;
    	$past_casual_count = 0;
    	$past_inlieu_count = 0;
    	foreach ($approved_counts as $row){
    		if($row->id == LEAVE_TYPE_ANNUAL){
    			$past_annual_count = $row->icount;
    			$total_count += $row->icount;
    		}else if($row->id == LEAVE_TYPE_PERSONAL){
    			$past_personal_count = $row->icount;
    			$total_count += $row->icount;
    		}else if($row->id == LEAVE_TYPE_LATE){
    			$past_late_count = $row->icount;
    			$total_count += $row->icount;
    		}else if($row->id == LEAVE_TYPE_LUNCH){
    			$past_lunch_count = $row->icount;
    			$total_count += $row->icount;    			
    		}else if($row->id == LEAVE_TYPE_TRAINING){
    			$past_training_count = $row->icount;
    			$total_count += $row->icount;
    		}else if($row->id == LEAVE_TYPE_CLIENTVISIT){
    			$past_client_count = $row->icount;
    			$total_count += $row->icount;
    		}else if($row->id == LEAVE_TYPE_OVERTIME){
    			$past_overtime_count = $row->icount;
    			$total_count += $row->icount;
    		}else if($row->id == LEAVE_TYPE_UNPAID){
    			$past_unpaid_count = $row->icount;
    			$total_count += $row->icount;
    		}else if($row->id == LEAVE_TYPE_CLOCK){
    			$past_clock_count = $row->icount;
    			$total_count += $row->icount;
    		}else if($row->id == LEAVE_TYPE_CASUAL){
    			$past_casual_count = $row->icount;
    			$total_count += $row->icount;
    		}else if($row->id == LEAVE_TYPE_INLIEU){
    			$past_inlieu_count = $row->icount;
    			$total_count += $row->icount;
    		}
    	}
    	$data['past_all_count'] = $total_count;
		$data['past_annual_count'] = $past_annual_count;
		$data['past_personal_count'] = $past_personal_count;
		$data['past_late_count'] = $past_late_count;
		$data['past_lunch_count'] = $past_lunch_count;
		$data['past_training_count'] = $past_training_count;
		$data['past_client_count'] = $past_client_count;
		$data['past_overtime_count'] = $past_overtime_count;
		$data['past_unpaid_count'] = $past_unpaid_count;
		$data['past_clock_count'] = $past_clock_count;
		$data['past_casual_count'] = $past_casual_count;
		$data['past_inlieu_count'] = $past_inlieu_count;
		
        $this->load->view('navigation', $data);
		$this->load->view('request/approve_past');
    }
    
    function user_stats(){
    	if ($this->session->userdata('user_level') >= 2){
    		$this->load->model('St_users_m', 'users_m');
    	
    		$user_dept = $this->session->userdata('department_id');
    		// Show all users for accounts user admin level
    		
    		// Show all sales for global sales
    		if ($user_dept == 13){
    			$users = $this->users_m->get_select_dropdown(false, false, false, 'Sales');
    		}elseif ($this->session->userdata('user_level') >= 4){
    			$users = $this->users_m->get_select_dropdown();
    		}else{
    			$users = $this->users_m->get_select_dropdown(false, $user_dept);
    		}
    		 
    		$data['users'] = $users;
    	}
    	
    	$this->load->view('request/user_stats', $data);
    	
    }

    /**
     * list request which are saved, but not apply
     */
    function saved(){
    	$department_id = $this->session->userdata('department');
    	$site_id = $this->session->userdata('site_id');
    	$user_level = $this->session->userdata('user_level');
    	
    	$data['state_name'] = 'New';
    	$data['form_id'] = 'saved';
		//read site
        $data['sites'] = $this->site_m->get_dropdown();
        
        //read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
        
        //read leave types
        $data['leave_types'] = $this->leave_type_m->get_dropdown();
		    	
    	
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	if($user_level == ROLE_OPERATION 
    		|| $user_level == ROLE_ADMIN){
    		$department_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		$data['approvals'] = $this->request_m->get_saved_approvals(
			$user_level,
			$site_id,
			$department_id
		);
		
		$total_count = $this->request_m->count_saved_approvals($user_level, $site_id, $department_id);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	$this->load->view('request/list_approvals', $data);
    }
    
	function saved_ajax(){
		log_message("debug", "<<<saved_ajax");
		$user_level = $this->session->userdata('user_level');
    	$data['form_id'] = 'saved';
        $order_by = $this->input->post('saved_order_by');
        
        $order = $this->input->post('saved_order');
        $site_id = $this->input->post('saved_filter_site_id');
    	$department_id = $this->input->post('saved_filter_department_id');
    	$leave_type_id = $this->input->post('saved_filter_leave_type_id');
    	$time_period_id = $this->input->post('saved_filter_time_period_id');
    	$first_name = $this->input->post('saved_filter_first_name');
    	$last_name = $this->input->post('saved_filter_last_name');
    	$page_no = $this->input->post('saved_page_no');
        
		$limit = $this->input->post('saved_limit');
    	$data['page_limit'] = $limit;
    	log_message("debug", "department_id  === ". $department_id);
    	$time_period = get_time_period($time_period_id);
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
		$data['approvals'] = $this->request_m->get_saved_approvals(
			$user_level,$site_id,
			$department_id, $leave_type_id, $time_period[0], $time_period[1], $first_name, $last_name, $order_by, $order, $page_no, $limit
		);
		
		$total_count = $this->request_m->count_saved_approvals(
			$user_level, $site_id, $department_id, $leave_type_id, $time_period[0], $time_period[1], $first_name, $last_name);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
        
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	$str = $this->load->view('request/list_approvals_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }
    
	/**
     * list request which are not approved
     */
    function not_approved(){
    	$department_id = $this->session->userdata('department');
    	$site_id = $this->session->userdata('site_id');
    	$user_level = $this->session->userdata('user_level');
    	
    	$data['state_name'] = 'Not Approved Approval';
    	$data['form_id'] = 'not_approved';
    	//read site
        $data['sites'] = $this->site_m->get_dropdown();
		//read department
        $data['departments'] = $this->department_m->get_dropdown($this->session->userdata('site_id'));
        
        //read leave types
        $data['leave_types'] = $this->leave_type_m->get_dropdown();
		    	
        //Remove default department for Manager and Superuser
		if(($user_level == ROLE_OPERATION && $department_id != 'Sales') 
    		|| $user_level == ROLE_ADMIN){
    		$department_id = "";
    		$site_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		$data['approvals'] = $this->request_m->get_not_approved_approvals(
			$user_level, 
			$site_id,
			$department_id
		);
		
		$total_count = $this->request_m->count_not_approved_approvals($user_level, $site_id, $department_id);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	$this->load->view('request/list_approvals', $data);
    }
    
	function not_approved_ajax(){
		log_message("debug", "<<<not_approved_ajax");
		$user_level = $this->session->userdata('user_level');
		$data['form_id'] = 'not_approved';
        $order_by = $this->input->post('not_approved_order_by');
        
        $order = $this->input->post('not_approved_order');
        $site_id = $this->input->post('not_approved_filter_site_id');
    	$department_id = $this->input->post('not_approved_filter_department_id');
    	$leave_type_id = $this->input->post('not_approved_filter_leave_type_id');
    	$time_period_id = $this->input->post('not_approved_filter_time_period_id');
    	$first_name = $this->input->post('not_approved_filter_first_name');
    	$last_name = $this->input->post('not_approved_filter_last_name');
    	$page_no = $this->input->post('not_approved_page_no');
        
		$limit = $this->input->post('not_approved_limit');
    	$data['page_limit'] = $limit;
    	
    	$time_period = get_time_period($time_period_id);
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
		$data['approvals'] = $this->request_m->get_not_approved_approvals(
			$user_level, $site_id,
			$department_id, $leave_type_id, $time_period[0], $time_period[1], $first_name, $last_name, $order_by, $order, $page_no, $limit
		);
		
		$total_count = $this->request_m->count_not_approved_approvals($user_level, $site_id, $department_id, $leave_type_id, $time_period[0], $time_period[1], $first_name, $last_name);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	log_message("debug", "sort order by === " . $sort_name);
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
        
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	$str = $this->load->view('request/list_approvals_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }
    /**
     * list request which are waiting approval
     */
	function waiting(){
		$department = $this->session->userdata('department');
    	$site_id = $this->session->userdata('site_id');
    	$user_level = $this->session->userdata('user_level');
    	
		$data['state_name'] = 'Waiting Approval';
    	$data['form_id'] = 'waiting';
    	//read site
        $data['sites'] = $this->site_m->get_dropdown();
		//read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
        
        //read leave types
        $data['leave_types'] = $this->leave_type_m->get_dropdown();
        
        // Remove site for Global user
        if ($department == 'Global Sales'){
        	$department = array("Sales", "Corporate");
        	$site_id = "";
        }
		    	
    	//Remove default department for Operation and Superuser
		if(($user_level == ROLE_OPERATION ) 
    		|| $user_level == ROLE_ADMIN){
    		$department = "";
    		$site_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department;
		$data['approvals'] = $this->request_m->get_waiting_approvals(
			$user_level,
			$site_id,
			$department
		);
		$total_count = $this->request_m->count_waiting_approvals($user_level, $site_id, $department);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	//$this->load->view('request/waiting_approvals', $data);
    	$this->load->view('request/list_approvals', $data);
    }
    
	function waiting_ajax(){
		$user_level = $this->session->userdata('user_level');
		$data['form_id'] = 'waiting';
        $order_by = $this->input->post('waiting_order_by');
        
        $order = $this->input->post('waiting_order');
        $site_id = $this->input->post('waiting_filter_site_id');
        
        if ($user_level < ROLE_OPERATION)
        	$department_id = $this->session->userdata('department');
        else
    		$department_id = $this->input->post('waiting_filter_department_id');
        
    	$leave_type_id = $this->input->post('waiting_filter_leave_type_id');
    	$time_period_id = $this->input->post('waiting_filter_time_period_id');
    	$first_name = $this->input->post('waiting_filter_first_name');
    	$last_name = $this->input->post('waiting_filter_last_name');
    	$page_no = $this->input->post('waiting_page_no');
        
		$limit = $this->input->post('waiting_limit');
    	$data['page_limit'] = $limit;
    	
    	//Remove default department for Manager and Superuser
/*     	if(($user_level == ROLE_OPERATION )
    	|| $user_level == ROLE_ADMIN){
    		$department_id = "";
    		$site_id = "";
    	} */
    	
    	$time_period = get_time_period($time_period_id);
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
		$data['approvals'] = $this->request_m->get_waiting_approvals(
			$user_level,$site_id, 
			$department_id, $leave_type_id, $time_period[0], $time_period[1], $first_name, $last_name, $order_by, $order, $page_no, $limit
		);
		
		$total_count = $this->request_m->count_waiting_approvals($user_level, $site_id, $department_id, $leave_type_id, $time_period[0], $time_period[1], $first_name, $last_name);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	log_message("debug", "sort order by === " . $sort_name);
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
    	
    	//$str = $this->load->view('request/waiting_approvals_data', $data, true);
    	$str = $this->load->view('request/list_approvals_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }
    
    
    /**
     * list request which are waiting approval
     */
	function waiting_manager(){
		$department_id = $this->session->userdata('department');
    	$site_id = $this->session->userdata('site_id');
    	$user_level = $this->session->userdata('user_level');
    	
		$data['state_name'] = 'Waiting Manager Approval';
    	$data['form_id'] = 'waiting_manager';
    	//read site
        $data['sites'] = $this->site_m->get_dropdown();
		//read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
        
        //read leave types
        $data['leave_types'] = $this->leave_type_m->get_dropdown();
		    	
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
		if($user_level == ROLE_OPERATION 
    		|| $user_level == ROLE_ADMIN){
    		$department_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		$data['approvals'] = $this->request_m->get_waiting_manager_approvals(
			$user_level,
			$site_id,
			$department_id
		);
		$total_count = $this->request_m->count_waiting_approvals($user_level, $site_id, $department_id);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	//$this->load->view('request/waiting_approvals', $data);
    	$this->load->view('request/list_approvals', $data);
    }    
    
    
    function waiting_mc(){
    	$department_id = $this->session->userdata('department');
    	$site_id = $this->session->userdata('site_id');
    	$user_level = $this->session->userdata('user_level');
    
    	$data['state_name'] = 'Waiting MC';
    	$data['form_id'] = 'waiting_mc';
    	//read site
    	$data['sites'] = $this->site_m->get_dropdown();
    	//read department
    	$data['departments'] = $this->department_m->get_dropdown($site_id);
    
    	//read leave types
    	$data['leave_types'] = $this->leave_type_m->get_dropdown();
    
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	if($user_level == ROLE_OPERATION
    			|| $user_level == ROLE_ADMIN){
    		$department_id = "";
    	}
    
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
    	$data['approvals'] = $this->request_m->get_waiting_mc(false);
    	$total_count = $this->request_m->get_waiting_mc(true);
    
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
    
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	$this->load->view('request/list_approvals', $data);
    }    
    
    /**
     * list request which are approved
     */
    function approved(){
    	$site_id = $this->session->userdata('site_id');
    	$department_id = $this->session->userdata('department');
    	$user_level = $this->session->userdata('user_level');
    	//read site
        $data['sites'] = $this->site_m->get_dropdown();
    	//read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
        
        //read leave types
        $data['leave_types'] = $this->leave_type_m->get_dropdown();
		    	
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	if($user_level == ROLE_OPERATION 
    		|| $user_level == ROLE_ADMIN){
    		$department_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		$data['approvals'] = $this->request_m->get_approved_approvals(
			$user_level,
			$department_id,
			'', '', ''
		);
		
		$total_count = $this->request_m->count_approved_approvals($this->session->userdata('user_level'), $department_id, '');
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	$this->load->view('request/approved_approvals', $data);
    }
    
    
    
    function approved_all(){
    	$site_id = $this->session->userdata('site_id');
    	$department_id = $this->session->userdata('department');
    	$user_level = $this->session->userdata('user_level');
    	$data['state_name'] = 'All';
    	$data['form_id'] = 'past_all';
    	$data['tab_name'] = 'approved_all';
    	//read site
        $data['sites'] = $this->site_m->get_dropdown();
    	//read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
        
        //read leave types
        $data['leave_types'] = $this->leave_type_m->get_dropdown();
		    	
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	if($user_level == ROLE_OPERATION 
    		|| $user_level == ROLE_ADMIN){
    		$department_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		$data['approvals'] = $this->request_m->past_approved_all(
			$user_level,
			$site_id,
			$department_id
		);
		
		$total_count = $this->request_m->count_past_approved_all($user_level, $site_id, $department_id);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	
   		$this->load->view('request/list_approved', $data);

    }
    
	function approved_all_ajax($export=false){
		log_message("debug", "<<< approved_all_ajax");
		$user_level = $this->session->userdata('user_level');
		$data['form_id'] = 'past_all';
        $order_by = $this->input->post('past_all_order_by');
        
        $order = $this->input->post('past_all_order');
        $site_id = $this->input->post('past_all_filter_site_id');
    	$department_id = $this->input->post('past_all_filter_department_id');
    	$time_period_id = $this->input->post('past_all_filter_time_period_id');
    	$from_date = $this->input->post('past_all_filter_from_date');
    	$to_date = $this->input->post('past_all_filter_to_date');
    	$first_name = $this->input->post('past_all_filter_first_name');
    	$last_name = $this->input->post('past_all_filter_last_name');
    	log_message("debug", "from_date == " . $this->input->post('past_all_filter_from_date'));
    	
    	
    	$page_no = $this->input->post('past_all_page_no');
    	
		$limit = $this->input->post('past_all_limit');
    	$data['page_limit'] = $limit;
    	
     	if (!empty($time_period_id)){
    		$time_period = get_time_period($time_period_id);
    	}else{
    		$time_period[0] = strtotime($from_date);
    		$time_period[1] = strtotime($to_date);
    	}
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
		$data['approvals'] = $this->request_m->past_approved_all(
			$user_level,$site_id,
			$department_id, $time_period[0], $time_period[1], $first_name, $last_name, $order_by, $order, $page_no, $limit
		);
		
		$total_count = $this->request_m->count_past_approved_all($user_level, $site_id, $department_id, $time_period[0], $time_period[1], $first_name, $last_name);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
    	
    	//$str = $this->load->view('request/approved_annual_approvals_data', $data, true);
    	$str = $this->load->view('request/list_approved_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        
       	echo json_encode($result);
    }
    
    
    
    
    
            
    /**
     * list request which are approved and leave type is anuual leave
     * @return page
     */
    function approved_annual(){
    	$site_id = $this->session->userdata('site_id');
    	$department_id = $this->session->userdata('department');
    	$user_level = $this->session->userdata('user_level');
    	$data['state_name'] = 'Annual Leave';
    	$data['form_id'] = 'past_annual';
    	$data['leave_type_id'] = LEAVE_TYPE_ANNUAL;
    	//read site
        $data['sites'] = $this->site_m->get_dropdown();
    	//read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
        
        //read leave types
        $data['leave_types'] = $this->leave_type_m->get_dropdown();
		    	
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	if($user_level == ROLE_OPERATION 
    		|| $user_level == ROLE_ADMIN){
    		$department_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		$data['approvals'] = $this->request_m->past_approved_annual(
			$user_level,
			$site_id,
			$department_id
		);
		
		$total_count = $this->request_m->count_past_approved_annual($user_level, $site_id, $department_id);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	
   		$this->load->view('request/list_approved', $data);

    }
    
	function approved_annual_ajax(){
		log_message("debug", "<<< approved_annual_ajax");
		$user_level = $this->session->userdata('user_level');
		$data['form_id'] = 'past_annual';
        $order_by = $this->input->post('past_annual_order_by');
        
        $order = $this->input->post('past_annual_order');
        $site_id = $this->input->post('past_annual_filter_site_id');
    	$department_id = $this->input->post('past_annual_filter_department_id');
    	$time_period_id = $this->input->post('past_annual_filter_time_period_id');
    	$from_date = $this->input->post('past_annual_filter_from_date');
    	$to_date = $this->input->post('past_annual_filter_to_date');
    	$first_name = $this->input->post('past_annual_filter_first_name');
    	$last_name = $this->input->post('past_annual_filter_last_name');
    	log_message("debug", "department_id == " . $department_id);
    	
    	$page_no = $this->input->post('past_annual_page_no');
    	
		$limit = $this->input->post('past_annual_limit');
    	$data['page_limit'] = $limit;
    	
	     if (!empty($time_period_id)){
    		$time_period = get_time_period($time_period_id);
    	}else{
    		$time_period[0] = strtotime($from_date);
    		$time_period[1] = strtotime($to_date);
    	}
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
		$data['approvals'] = $this->request_m->past_approved_annual(
			$user_level,$site_id,
			$department_id, $time_period[0], $time_period[1], $first_name, $last_name, $order_by, $order, $page_no, $limit
		);
		
		$total_count = $this->request_m->count_past_approved_annual($user_level, $site_id, $department_id, $time_period[0], $time_period[1], $first_name, $last_name);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
    	
    	//$str = $this->load->view('request/approved_annual_approvals_data', $data, true);
    	$str = $this->load->view('request/list_approved_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }    
    
    function approved_personal(){
    	$site_id = $this->session->userdata('site_id');
    	$department_id = $this->session->userdata('department');
    	$user_level = $this->session->userdata('user_level');
    	$data['state_name'] = 'Personal Leave';
		$data['form_id'] = 'past_personal';
		$data['leave_type_id'] = LEAVE_TYPE_PERSONAL;
    	//read site
        $data['sites'] = $this->site_m->get_dropdown();
        //read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
		    	
    	$department_id = $this->session->userdata('department');
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	if($user_level == ROLE_OPERATION 
    		|| $user_level == ROLE_ADMIN){
    		$department_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		$data['approvals'] = $this->request_m->past_approved_personal(
			$user_level,
			$site_id, 
			$department_id
		);
		
		$total_count = $this->request_m->count_past_approved_personal($user_level, $site_id, $department_id);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	
   		$this->load->view('request/list_approved', $data);

    }
    
	function approved_personal_ajax(){
		log_message("debug", "<<< approved_personal_ajax");
		$user_level = $this->session->userdata('user_level');
		$data['form_id'] = 'past_personal';
		
        $order_by = $this->input->post('past_personal_order_by');
        
        $order = $this->input->post('past_personal_order');
        $site_id = $this->input->post('past_personal_filter_site_id');
    	$department_id = $this->input->post('past_personal_filter_department_id');
    	$time_period_id = $this->input->post('past_personal_filter_time_period_id');
    	$from_date = $this->input->post('past_personal_filter_from_date');
    	$to_date = $this->input->post('past_personal_filter_to_date');
    	$first_name = $this->input->post('past_personal_filter_first_name');
    	$last_name = $this->input->post('past_personal_filter_last_name');
    	$page_no = $this->input->post('past_personal_page_no');
    	
		$limit = $this->input->post('past_personal_limit');
    	$data['page_limit'] = $limit;
    	
		if (!empty($time_period_id)){
    		$time_period = get_time_period($time_period_id);
    	}else{
    		$time_period[0] = strtotime($from_date);
    		$time_period[1] = strtotime($to_date);
    	}
		$data['approvals'] = $this->request_m->past_approved_personal(
			$user_level,$site_id, 
			$department_id, $time_period[0], $time_period[1], $first_name, $last_name, $order_by, $order, $page_no, $limit
		);
		
		$total_count = $this->request_m->count_past_approved_personal($user_level, $site_id, $department_id, $time_period[0], $time_period[1], $first_name, $last_name);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
    	
		$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	$str = $this->load->view('request/list_approved_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }
    
    function approved_late(){
    	$site_id = $this->session->userdata('site_id');
    	$department_id = $this->session->userdata('department');
    	$user_level = $this->session->userdata('user_level');
    	$data['state_name'] = 'Lateness';
		$data['form_id'] = 'past_late';
		$data['leave_type_id'] = LEAVE_TYPE_LATE;
    	//read site
        $data['sites'] = $this->site_m->get_dropdown();
        //read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
		    	
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	if($user_level == ROLE_OPERATION 
    		|| $user_level == ROLE_ADMIN){
    		$department_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		$data['approvals'] = $this->request_m->past_approved_late(
			$user_level,
			$site_id,
			$department_id
		);
		
		$total_count = $this->request_m->count_past_approved_late($user_level, $site_id, $department_id);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	
   		$this->load->view('request/list_approved', $data);

    }
    
	function approved_late_ajax(){
		log_message("debug", "<<< approved_late_ajax");
		$data['form_id'] = 'past_late';
        $order_by = $this->input->post('past_late_order_by');
        
        $order = $this->input->post('past_late_order');
        $site_id = $this->input->post('past_late_filter_site_id');
    	$department_id = $this->input->post('past_late_filter_department_id');
    	$time_period_id = $this->input->post('past_late_filter_time_period_id');
    	$from_date = $this->input->post('past_late_filter_from_date');
    	$to_date = $this->input->post('past_late_filter_to_date');
    	$first_name = $this->input->post('past_late_filter_first_name');
    	$last_name = $this->input->post('past_late_filter_last_name');
    	
    	$page_no = $this->input->post('past_late_page_no');
    	
		$limit = $this->input->post('past_late_limit');
    	$data['page_limit'] = $limit;
    	
		if (!empty($time_period_id)){
    		$time_period = get_time_period($time_period_id);
    	}else{
    		$time_period[0] = strtotime($from_date);
    		$time_period[1] = strtotime($to_date);
    	}
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
		$data['approvals'] = $this->request_m->past_approved_late(
			$this->session->userdata('user_level'),$site_id, 
			$department_id, $time_period[0], $time_period[1], $first_name, $last_name, $order_by, $order, $page_no, $limit
		);
		
		$total_count = $this->request_m->count_past_approved_late($this->session->userdata('user_level'), $site_id, $department_id, $time_period[0], $time_period[1], $first_name, $last_name);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
    	
		$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	$str = $this->load->view('request/list_approved_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }    
    
    function approved_lunch(){
    	$site_id = $this->session->userdata('site_id');
    	$department_id = $this->session->userdata('department');
    	$user_level = $this->session->userdata('user_level');
    	$data['state_name'] = 'Lunch';
		$data['form_id'] = 'past_lunch';
		$data['leave_type_id'] = LEAVE_TYPE_LUNCH;
    	//read site
        $data['sites'] = $this->site_m->get_dropdown();
        //read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
		    	
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	if($user_level == ROLE_OPERATION 
    		|| $user_level == ROLE_ADMIN){
    		$department_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		$data['approvals'] = $this->request_m->past_approved_lunch(
			$user_level,
			$site_id,
			$department_id
		);
		
		$total_count = $this->request_m->count_past_approved_lunch($user_level, $site_id, $department_id);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	
   		$this->load->view('request/list_approved', $data);

    }
    
    
	function approved_lunch_ajax(){
		log_message("debug", "<<< approved_lunch_ajax");
		$data['form_id'] = 'past_lunch';
        $order_by = $this->input->post('past_lunch_order_by');
        
        $order = $this->input->post('past_lunch_order');
        $site_id = $this->input->post('past_lunch_filter_site_id');
    	$department_id = $this->input->post('past_lunch_filter_department_id');
    	$time_period_id = $this->input->post('past_lunch_filter_time_period_id');
    	$from_date = $this->input->post('past_lunch_filter_from_date');
    	$to_date = $this->input->post('past_lunch_filter_to_date');
    	$first_name = $this->input->post('past_lunch_filter_first_name');
    	$last_name = $this->input->post('past_lunch_filter_last_name');
    	
    	$page_no = $this->input->post('past_lunch_page_no');
    	
		$limit = $this->input->post('past_lunch_limit');
    	$data['page_limit'] = $limit;
    	
		if (!empty($time_period_id)){
    		$time_period = get_time_period($time_period_id);
    	}else{
    		$time_period[0] = strtotime($from_date);
    		$time_period[1] = strtotime($to_date);
    	}
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
		$data['approvals'] = $this->request_m->past_approved_lunch(
			$this->session->userdata('user_level'),$site_id, 
			$department_id, $time_period[0], $time_period[1], $first_name, $last_name, $order_by, $order, $page_no, $limit
		);
		
		$total_count = $this->request_m->count_past_approved_lunch($this->session->userdata('user_level'), $site_id, $department_id, $time_period[0], $time_period[1], $first_name, $last_name);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
    	
		$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	$str = $this->load->view('request/list_approved_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }    
    
    
    
    
    function approved_clock(){
    	$site_id = $this->session->userdata('site_id');
    	$department_id = $this->session->userdata('department');
    	$user_level = $this->session->userdata('user_level');
    	$data['state_name'] = 'Clock';
		$data['form_id'] = 'past_clock';
		$data['leave_type_id'] = LEAVE_TYPE_CLOCK;
    	//read site
        $data['sites'] = $this->site_m->get_dropdown();
        //read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
		    	
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	if($user_level == ROLE_OPERATION 
    		|| $user_level == ROLE_ADMIN){
    		$department_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		$data['approvals'] = $this->request_m->past_approved_clock(
			$user_level,
			$site_id,
			$department_id
		);
		
		$total_count = $this->request_m->count_past_approved_clock($user_level, $site_id, $department_id);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	
   		$this->load->view('request/list_approved', $data);

    }
    
    
	function approved_clock_ajax(){
		log_message("debug", "<<< approved_clock_ajax");
		$data['form_id'] = 'past_clock';
        $order_by = $this->input->post('past_clock_order_by');
        
        $order = $this->input->post('past_clock_order');
        $site_id = $this->input->post('past_clock_filter_site_id');
    	$department_id = $this->input->post('past_clock_filter_department_id');
    	$time_period_id = $this->input->post('past_clock_filter_time_period_id');
    	$from_date = $this->input->post('past_clock_filter_from_date');
    	$to_date = $this->input->post('past_clock_filter_to_date');
    	$first_name = $this->input->post('past_clock_filter_first_name');
    	$last_name = $this->input->post('past_clock_filter_last_name');
    	
    	$page_no = $this->input->post('past_clock_page_no');
    	
		$limit = $this->input->post('past_clock_limit');
    	$data['page_limit'] = $limit;
    	
	     if (!empty($time_period_id)){
    		$time_period = get_time_period($time_period_id);
    	}else{
    		$time_period[0] = strtotime($from_date);
    		$time_period[1] = strtotime($to_date);
    	}
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
		$data['approvals'] = $this->request_m->past_approved_clock(
			$this->session->userdata('user_level'),$site_id, 
			$department_id, $time_period[0], $time_period[1], $first_name, $last_name, $order_by, $order, $page_no, $limit
		);
		
		$total_count = $this->request_m->count_past_approved_clock($this->session->userdata('user_level'), $site_id, $department_id, $time_period[0], $time_period[1], $first_name, $last_name);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
    	
		$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	$str = $this->load->view('request/list_approved_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }    
    

    
	function approved_training(){
		$site_id = $this->session->userdata('site_id');
    	$department_id = $this->session->userdata('department');
    	$user_level = $this->session->userdata('user_level');
		$data['state_name'] = 'Training/Seminar';
		$data['form_id'] = 'past_training';
		$data['leave_type_id'] = LEAVE_TYPE_TRAINING;
		//read site
        $data['sites'] = $this->site_m->get_dropdown();
        //read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
		    	
    	$department_id = $this->session->userdata('department');
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	if($user_level == ROLE_OPERATION 
    		|| $user_level == ROLE_ADMIN){
    		$department_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		$data['approvals'] = $this->request_m->past_approved_training(
			$user_level,
			$site_id,
			$department_id
		);
		
		$total_count = $this->request_m->count_past_approved_training($user_level, $site_id, $department_id);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	
   		$this->load->view('request/list_approved', $data);

    }
    
	function approved_training_ajax(){
		log_message("debug", "<<< approved_training_ajax");
		$data['form_id'] = 'past_training';
        $order_by = $this->input->post('past_training_order_by');
        
        $order = $this->input->post('past_training_order');
        $site_id = $this->input->post('past_training_filter_site_id');
    	$department_id = $this->input->post('past_training_filter_department_id');
    	$time_period_id = $this->input->post('past_training_filter_time_period_id');
    	$from_date = $this->input->post('past_training_filter_from_date');
    	$to_date = $this->input->post('past_training_filter_to_date');
    	$first_name = $this->input->post('past_training_filter_first_name');
    	$last_name = $this->input->post('past_training_filter_last_name');
    	
    	$page_no = $this->input->post('past_training_page_no');
    	
		$limit = $this->input->post('past_training_limit');
    	$data['page_limit'] = $limit;
    	
		if (!empty($time_period_id)){
    		$time_period = get_time_period($time_period_id);
    	}else{
    		$time_period[0] = strtotime($from_date);
    		$time_period[1] = strtotime($to_date);
    	}
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
		$data['approvals'] = $this->request_m->past_approved_training(
			$this->session->userdata('user_level'),$site_id, 
			$department_id, $time_period[0], $time_period[1], $first_name, $last_name, $order_by, $order, $page_no, $limit
		);
		
		$total_count = $this->request_m->count_past_approved_training($this->session->userdata('user_level'), $site_id, $department_id, $time_period[0], $time_period[1], $first_name, $last_name);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
    	
		$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	$str = $this->load->view('request/list_approved_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }
    
	function approved_client(){
    	$site_id = $this->session->userdata('site_id');
    	$department_id = $this->session->userdata('department');
    	$user_level = $this->session->userdata('user_level');
    	$data['state_name'] = 'Client visit';
		$data['form_id'] = 'past_client';
		$data['leave_type_id'] = LEAVE_TYPE_CLIENTVISIT;
    	//read site
        $data['sites'] = $this->site_m->get_dropdown();
        //read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
		    	
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	if($user_level == ROLE_OPERATION 
    		|| $user_level == ROLE_ADMIN){
    		$department_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		$data['approvals'] = $this->request_m->past_approved_client(
			$user_level,
			$site_id,
			$department_id
		);
		
		$total_count = $this->request_m->count_past_approved_client($user_level, $site_id, $department_id);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	
   		$this->load->view('request/list_approved', $data);

    }
    
	function approved_client_ajax(){
		log_message("debug", "<<< approved_client_ajax");
		$data['form_id'] = 'past_client';
        $order_by = $this->input->post('past_client_order_by');
        
        $order = $this->input->post('past_client_order');
        $site_id = $this->input->post('past_client_filter_site_id');
    	$department_id = $this->input->post('past_client_filter_department_id');
    	$time_period_id = $this->input->post('past_client_filter_time_period_id');    	
    	$from_date = $this->input->post('past_client_filter_from_date');
    	$to_date = $this->input->post('past_client_filter_to_date');
    	$first_name = $this->input->post('past_client_filter_first_name');
    	$last_name = $this->input->post('past_client_filter_last_name');
    	
    	$page_no = $this->input->post('past_client_page_no');
    	
		$limit = $this->input->post('past_client_limit');
    	$data['page_limit'] = $limit;
    	
	     if (!empty($time_period_id)){
    		$time_period = get_time_period($time_period_id);
    	}else{
    		$time_period[0] = strtotime($from_date);
    		$time_period[1] = strtotime($to_date);
    	}
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
		$data['approvals'] = $this->request_m->past_approved_client(
			$this->session->userdata('user_level'),$site_id, 
			$department_id, $time_period[0], $time_period[1], $first_name, $last_name, $order_by, $order, $page_no, $limit
		);
		
		$total_count = $this->request_m->count_past_approved_client($this->session->userdata('user_level'), $site_id, $department_id, $time_period[0], $time_period[1], $first_name, $last_name);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
        
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	log_message("debug", "approved_client_ajax page_count" . $page_count);
    	
    	$str = $this->load->view('request/list_approved_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }
    
	function approved_overtime(){
    	$site_id = $this->session->userdata('site_id');
    	$department_id = $this->session->userdata('department');
    	$user_level = $this->session->userdata('user_level');
    	$data['state_name'] = 'Overtime';
		$data['form_id'] = 'past_overtime';
		$data['leave_type_id'] = LEAVE_TYPE_OVERTIME;
    	//read site
        $data['sites'] = $this->site_m->get_dropdown();
        //read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
		    	
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	if($user_level == ROLE_OPERATION 
    		|| $user_level == ROLE_ADMIN){
    		$department_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		$data['approvals'] = $this->request_m->past_approved_overtime(
			$user_level,
			$site_id,
			$department_id
		);
		
		$total_count = $this->request_m->count_past_approved_overtime($user_level, $site_id, $department_id);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	
    		$this->load->view('request/list_approved', $data);

    }
    
	function approved_overtime_ajax(){
		log_message("debug", "<<< approved_overtime_ajax");
		$data['form_id'] = 'past_overtime';
        $order_by = $this->input->post('past_overtime_order_by');
        
        $order = $this->input->post('past_overtime_order');
        $site_id = $this->input->post('past_overtime_filter_site_id');
    	$department_id = $this->input->post('past_overtime_filter_department_id');
    	$time_period_id = $this->input->post('past_overtime_filter_time_period_id');
    	$from_date = $this->input->post('past_overtime_filter_from_date');
    	$to_date = $this->input->post('past_overtime_filter_to_date');
    	$first_name = $this->input->post('past_overtime_filter_first_name');
    	$last_name = $this->input->post('past_overtime_filter_last_name');
    	
    	$page_no = $this->input->post('past_overtime_page_no');
    	
		$limit = $this->input->post('past_overtime_limit');
    	$data['page_limit'] = $limit;
    	
		if (!empty($time_period_id)){
    		$time_period = get_time_period($time_period_id);
    	}else{
    		$time_period[0] = strtotime($from_date);
    		$time_period[1] = strtotime($to_date);
    	}
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
		$data['approvals'] = $this->request_m->past_approved_overtime(
			$this->session->userdata('user_level'),$site_id, 
			$department_id, $time_period[0], $time_period[1], $first_name, $last_name, $order_by, $order, $page_no, $limit
		);
		
		$total_count = $this->request_m->count_past_approved_overtime($this->session->userdata('user_level'), $site_id, $department_id, $time_period[0], $time_period[1], $first_name, $last_name);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
        
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	$str = $this->load->view('request/list_approved_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }
    
	function approved_unpaid(){
    	$site_id = $this->session->userdata('site_id');
    	$department_id = $this->session->userdata('department');
    	$user_level = $this->session->userdata('user_level');
    	$data['state_name'] = 'Unpaid';
		$data['form_id'] = 'past_unpaid';
		$data['leave_type_id'] = LEAVE_TYPE_UNPAID;
    	//read site
        $data['sites'] = $this->site_m->get_dropdown();
        //read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
		    	
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	if($user_level == ROLE_OPERATION 
    		|| $user_level == ROLE_ADMIN){
    		$department_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		$data['approvals'] = $this->request_m->past_approved_unpaid(
			$user_level,
			$site_id,
			$department_id
		);
		
		$total_count = $this->request_m->count_past_approved_unpaid($user_level, $site_id, $department_id);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	
   		$this->load->view('request/list_approved', $data);

    }
    
	function approved_unpaid_ajax(){
		log_message("debug", "<<< approved_unpaid_ajax");
		$data['form_id'] = 'past_unpaid';
        $order_by = $this->input->post('past_unpaid_order_by');
        
        $order = $this->input->post('past_unpaid_order');
        $site_id = $this->input->post('past_unpaid_filter_site_id');
    	$department_id = $this->input->post('past_unpaid_filter_department_id');
    	$time_period_id = $this->input->post('past_unpaid_filter_time_period_id');
    	$from_date = $this->input->post('past_unpaid_filter_from_date');
    	$to_date = $this->input->post('past_unpaid_filter_to_date');
    	$first_name = $this->input->post('past_unpaid_filter_first_name');
    	$last_name = $this->input->post('past_unpaid_filter_last_name');
    	
    	$page_no = $this->input->post('past_unpaid_page_no');
    	
		$limit = $this->input->post('past_unpaid_limit');
    	$data['page_limit'] = $limit;
    	
		if (!empty($time_period_id)){
    		$time_period = get_time_period($time_period_id);
    	}else{
    		$time_period[0] = strtotime($from_date);
    		$time_period[1] = strtotime($to_date);
    	}
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
		$data['approvals'] = $this->request_m->past_approved_unpaid(
			$this->session->userdata('user_level'),$site_id, 
			$department_id, $time_period[0], $time_period[1], $first_name, $last_name, $order_by, $order, $page_no, $limit
		);
		
		$total_count = $this->request_m->count_past_approved_unpaid($this->session->userdata('user_level'), $site_id, $department_id, $time_period[0], $time_period[1], $first_name, $last_name);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
        
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	$str = $this->load->view('request/list_approved_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }
    
    function approved_inlieu(){
    	$site_id = $this->session->userdata('site_id');
    	$department_id = $this->session->userdata('department');
    	$user_level = $this->session->userdata('user_level');
    	$data['state_name'] = 'Time In Lieu';
    	$data['form_id'] = 'past_inlieu';
    	$data['leave_type_id'] = LEAVE_TYPE_INLIEU;
    	//read site
    	$data['sites'] = $this->site_m->get_dropdown();
    	//read department
    	$data['departments'] = $this->department_m->get_dropdown($site_id);
    	 
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	if($user_level >= 3){
    		$department_id = "";
    	}
    	 
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
    	$data['approvals'] = $this->request_m->past_approved_inlieu(
    			$user_level,
    			$site_id,
    			$department_id
    	);
    
    	$total_count = $this->request_m->count_past_approved_inlieu($user_level, $site_id, $department_id);
    	 
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
    
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	
    	$this->load->view('request/list_approved', $data);

    }
    
    function approved_inlieu_ajax(){
    	log_message("debug", "<<< approved_inlieu_ajax");
    	$data['form_id'] = 'past_inlieu';
    	$order_by = $this->input->post('past_inlieu_order_by');
    
    	$order = $this->input->post('past_inlieu_order');
    	$site_id = $this->input->post('past_inlieu_filter_site_id');
    	$department_id = $this->input->post('past_inlieu_filter_department_id');
    	$time_period_id = $this->input->post('past_inlieu_filter_time_period_id');
    	$from_date = $this->input->post('past_inlieu_filter_from_date');
    	$to_date = $this->input->post('past_inlieu_filter_to_date');
    	$first_name = $this->input->post('past_inlieu_filter_first_name');
    	$last_name = $this->input->post('past_inlieu_filter_last_name');
    	 
    	$page_no = $this->input->post('past_inlieu_page_no');
    	 
    	$limit = $this->input->post('past_inlieu_limit');
    	$data['page_limit'] = $limit;
    	 
    	if (!empty($time_period_id)){
    		$time_period = get_time_period($time_period_id);
    	}else{
    		$time_period[0] = strtotime($from_date);
    		$time_period[1] = strtotime($to_date);
    	}
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	$data['approvals'] = $this->request_m->past_approved_inlieu(
    			$this->session->userdata('user_level'),$site_id,
    			$department_id, $time_period[0], $time_period[1], $first_name, $last_name, $order_by, $order, $page_no, $limit
    	);
    
    	$total_count = $this->request_m->count_past_approved_inlieu($this->session->userdata('user_level'), $site_id, $department_id, $time_period[0], $time_period[1], $first_name, $last_name);
    
    	$page_count = 0;
    	if($limit > 0){
    		if(($total_count % $limit) == 0){
    			$page_count = $total_count / $limit;
    		}else{
    			$page_count = floor($total_count / $limit) + 1;
    		}
    	}
    	 
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	 
    	//set value with image tag
    	$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
    
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	 
    	$str = $this->load->view('request/list_approved_data', $data, true);
    	 
    	$result = array(
    			"page_count"=> $page_count,
    			"page_no" 	=> $page_no,
    			"content" 	=> $str
    	);
    	echo json_encode($result);
    }
    
    function approved_casual(){
    	$site_id = $this->session->userdata('site_id');
    	$department_id = $this->session->userdata('department');
    	$user_level = $this->session->userdata('user_level');
    	$data['state_name'] = 'Casual Work';
    	$data['form_id'] = 'past_casual';
    	$data['leave_type_id'] = LEAVE_TYPE_CASUAL;
    	//read site
    	$data['sites'] = $this->site_m->get_dropdown();
    	//read department
    	$data['departments'] = $this->department_m->get_dropdown($site_id);
    
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	if($user_level >= 3){
    		$department_id = "";
    	}
    
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
    	$data['approvals'] = $this->request_m->past_approved_casual(
    			$user_level,
    			$site_id,
    			$department_id
    	);
    
    	$total_count = $this->request_m->count_past_approved_casual($user_level, $site_id, $department_id);
    
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
    
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	
   		$this->load->view('request/list_approved', $data);
    }
    
    function approved_casual_ajax(){
    	log_message("debug", "<<< approved_casual_ajax");
    	$data['form_id'] = 'past_casual';
    	$order_by = $this->input->post('past_casual_order_by');
    
    	$order = $this->input->post('past_casual_order');
    	$site_id = $this->input->post('past_casual_filter_site_id');
    	$department_id = $this->input->post('past_casual_filter_department_id');
    	$time_period_id = $this->input->post('past_casual_filter_time_period_id');
    	$from_date = $this->input->post('past_casual_filter_from_date');
    	$to_date = $this->input->post('past_casual_filter_to_date');
    	$first_name = $this->input->post('past_casual_filter_first_name');
    	$last_name = $this->input->post('past_casual_filter_last_name');
    
    	$page_no = $this->input->post('past_casual_page_no');
    
    	$limit = $this->input->post('past_casual_limit');
    	$data['page_limit'] = $limit;
    
    	if (!empty($time_period_id)){
    		$time_period = get_time_period($time_period_id);
    	}else{
    		$time_period[0] = strtotime($from_date);
    		$time_period[1] = strtotime($to_date);
    	}
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	$data['approvals'] = $this->request_m->past_approved_casual(
    			$this->session->userdata('user_level'),$site_id,
    			$department_id, $time_period[0], $time_period[1], $first_name, $last_name, $order_by, $order, $page_no, $limit
    	);
    
    	$total_count = $this->request_m->count_past_approved_casual($this->session->userdata('user_level'), $site_id, $department_id, $time_period[0], $time_period[1], $first_name, $last_name);
    
    	$page_count = 0;
    	if($limit > 0){
    		if(($total_count % $limit) == 0){
    			$page_count = $total_count / $limit;
    		}else{
    			$page_count = floor($total_count / $limit) + 1;
    		}
    	}
    
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    
    	//set value with image tag
    	$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
    
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    
    	$str = $this->load->view('request/list_approved_data', $data, true);
    
    	$result = array(
    			"page_count"=> $page_count,
    			"page_no" 	=> $page_no,
    			"content" 	=> $str
    	);
    	echo json_encode($result);
    }
    
    
    function approved_upcoming(){
    	$site_id = $this->session->userdata('site_id');
    	$department_id = $this->session->userdata('department');
    	$user_level = $this->session->userdata('user_level');
    	
    	$data['state_name'] = 'Upcoming';
    	$data['form_id'] = 'upcoming';
    	//read site
        $data['sites'] = $this->site_m->get_dropdown();
        //read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
        
        //read leave types
        $data['leave_types'] = $this->leave_type_m->get_dropdown();
        
        // Remove site for Global user
        if ($department == 'Global Sales'){
        	$department = "Sales";
        	$site_id = "";
        }
		    	
        //Remove default department for Manager and Superuser
		if(($user_level == ROLE_OPERATION && $department_id != 'Sales') 
    		|| $user_level == ROLE_ADMIN){
    		$department_id = "";
    		$site_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		$data['approvals'] = $this->request_m->upcoming_approved(
			$user_level,$site_id,
			$department_id, 0
		);
		
		$total_count = $this->request_m->count_upcoming_approved($user_level, $site_id, $department_id);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	//$this->load->view('request/approved_upcoming_approvals', $data);
    	$this->load->view('request/list_approvals', $data);
    	
    }
    
	function approved_upcoming_ajax(){
		log_message("debug", "<<< approved_upcoming_ajax");
		$user_level = $this->session->userdata('user_level');
		$data['form_id'] = 'upcoming';
		
        $order_by = $this->input->post('upcoming_order_by');
        
        $order = $this->input->post('upcoming_order');
        $site_id = $this->input->post('upcoming_filter_site_id');
    	$department_id = $this->input->post('upcoming_filter_department_id');
    	$leave_type_id = $this->input->post('upcoming_filter_leave_type_id');
    	$first_name = $this->input->post('upcoming_filter_first_name');
    	$last_name = $this->input->post('upcoming_filter_last_name');
    	
    	
    	$page_no = $this->input->post('upcoming_page_no');
    	
		$limit = $this->input->post('upcoming_limit');
    	$data['page_limit'] = $limit;
    	
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
		$data['approvals'] = $this->request_m->upcoming_approved(
			$user_level,$site_id, 
			$department_id, $leave_type_id, 0, 0, $first_name, $last_name, $order_by, $order, $page_no, $limit
		);
		
		
		$total_count = $this->request_m->count_upcoming_approved($user_level, $site_id, $department_id, $leave_type_id);

    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
        
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	$str = $this->load->view('request/list_approvals_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }
    
    
    
    /**
     * list the colleague leave statement
     * include all approved records
     * @return view
     */
    function colleague_approved($request_id){
    	$request = $this->request_m->get($request_id);
    	if(empty($request)){
    		show_404();
    	}
    	
    	$data['request'] = $request; 
    	$user_name = $request->user_name;
    	
    	$this->load->model('St_users_m', 'users_m');
    	$user = $this->users_m->get_by_user_name($user_name);
    	
		$department_id = $user->department_id;
    	
		$data['form_id'] = 'past_colleague';
    	
		$site_id = $user->site_id;
		//read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
        
        //read leave types
        $leave_types = $this->leave_type_m->get_dropdown();
		$data['leave_types'] = array("" => "Please select", 
			LEAVE_TYPE_ANNUAL => $leave_types[LEAVE_TYPE_ANNUAL],
			LEAVE_TYPE_PERSONAL => $leave_types[LEAVE_TYPE_PERSONAL],
			);
		    	
    	
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	if($user->user_level_id == 3){
    		$department_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		$data['approvals'] = $this->request_m->get_colleague_approved_by_leave_type(
			array(LEAVE_TYPE_ANNUAL, LEAVE_TYPE_PERSONAL),
			$request->start_date, $request->end_date, $site_id, $department_id
		);
		
		$total_count = $this->request_m->count_colleague_approved_by_leave_type(
			array(LEAVE_TYPE_ANNUAL, LEAVE_TYPE_PERSONAL),
			$request->start_date, $request->end_date, $site_id, $department_id
		);
		
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	$this->load->view('request/list_colleague_approved', $data);
    }
    
	function colleague_approved_ajax(){
		log_message("debug", "<<< colleague_approved_ajax");
		$data['form_id'] = 'past_colleague';
		
        $order_by = $this->input->post('past_colleague_order_by');
        
        $order = $this->input->post('past_colleague_order');
        $site_id = $this->input->post('past_colleague_filter_site_id');
    	$department_id = $this->input->post('past_colleague_department_id');
    	$leave_type_id = $this->input->post('past_colleague_leave_type_id');
    	$start_date = $this->input->post('past_colleague_start_date');
    	$end_date = $this->input->post('past_colleague_end_date');
    	
    	$page_no = $this->input->post('past_colleague_page_no');
    	
		$limit = $this->input->post('past_colleague_limit');
    	$data['page_limit'] = $limit;

		
		//$total_count = $this->request_m->count_past_approved_personal($this->session->userdata('user_level'), $department_id, $time_period[0], $time_period[1]);
    	$a_leave_type = array(LEAVE_TYPE_ANNUAL, LEAVE_TYPE_PERSONAL);
    	if(!empty($leave_type_id)){
    		$a_leave_type = $leave_type_id;
    	}
    	
		$data['approvals'] = $this->request_m->get_colleague_approved_by_leave_type(
			$a_leave_type, strtotime($start_date), strtotime($end_date), $site_id, $department_id, 
			$order_by, $order, $page_no, $limit  
		);
		
		$total_count = $this->request_m->count_colleague_approved_by_leave_type(
			$a_leave_type,strtotime($start_date), strtotime($end_date), $site_id, $department_id
		);
		
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
    	
		$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
        
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	$str = $this->load->view('request/list_approved_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }
    
    
    
    
    
    
    /**
     * prepare to add a new leave request
     * @return 
     */
    function add() {
        $data['navigation'] = "<a href='". site_url('admin/leave') ."' target='_top'>Leave Request</a> > <a href='". site_url('request') ."'>Leave Statement</a> > Add";
        
        $leave_types = $this->leave_type_m->get_dropdown();
        $data['leave_types'] = $leave_types;
        
        $this->load->view('navigation', $data);
        $this->load->view('request/add_request');
    }
    
    function deal($request_id){
    	$leave_types = $this->leave_type_m->get_dropdown();
    	$data['leave_types'] = $leave_types;
    	
    	$request = $this->request_m->get_info($request_id);
    	if(empty($request)){
    		show_404();
    	}
    	
    	
    	if($request->leave_type_id == LEAVE_TYPE_LATE || $request->leave_type_id == LEAVE_TYPE_LUNCH)
    		$request->hours_used = $request->hours_used * 60;
    		    	
		$data['navigation'] = "<a href='". site_url('admin/leave') ."' target='_top'>Leave Request</a> > <a href='". site_url('approve') ."'>Leave Approval</a> > Approve R". $request->id;
    	$data['request'] = $request; 
    	$user_name = $request->user_name;
    	
    	$request_counts = $this->request_m->get_requests_counts($user_name);
    	$new_count = 0;
    	$not_approved_count = 0;
    	$approved_count = 0;
    	$pending_count = 0;
    	foreach ($request_counts as $row){
    		if($row->state == 10){
    			$new_count = $row->icount;
    		}else if($row->state == 20 || $row->state == 30){
    			$pending_count += $row->icount;
    		}else if($row->state == 40){
    			$approved_count = $row->icount;
    		}else if($row->state == 50){
    			$not_approved_count = $row->icount;
    		}
    	}
    	
    	$data['waiting_count'] = $pending_count;
    	
    	$upcoming_count = $this->request_m->count_upcoming_approved_requests($user_name);
    	$data['upcoming_count'] = $upcoming_count;
    	    	
    	
    	$this->load->model('St_users_m', 'users_m');
    	$user = $this->users_m->get_by_user_name($user_name);
    	$data['user'] = $user;

    	$this->load->model('St_site_m', 'site_m');
    	if ($request->site_id){
    		$store = $this->site_m->get($request->site_id);
    	}else{
    		$store = $this->site_m->get($user->site_id);
    	}
    	
    	$data['store'] = $store->name;
    	
    	$data['depts'] = false;
    	if ($request->department_id){
    		$this->load->model('St_department_m', 'department_m');
    		$dept = $this->department_m->get($request->department_id);
    		$data['dept'] = $dept->department;
    	}
    	
    	if (!empty($request->manager_user_name) ){
    		$data['manager_user'] = $this->users_m->get_by_user_name($request->manager_user_name);
    	}
    	 
    	
    	$this->load->library('workinghour');
    	
    	$data['departments'] = $this->department_m->get_dropdown($user->site_id);
    	
    	$now_time = now();
    	$now_day = date(DATE_FORMAT, $now_time);
    	
    	
    	
    	//calculate total annual/personal leave balance
/*    	$this->load->model('St_working_hours_m', 'working_hours_m');    	
	   	$total_working_stat = $this->working_hours_m->get_staff_working_hours($user_name);
    	
	   	$work_count = $total_working_stat->work_count;
    	$total_annual = round($work_count / 13, 1);
    	$total_personal = round($work_count / 26, 1);
		$total_annual = $total_working_stat->annual_hours_count;
		$total_personal = $total_working_stat->personal_hours_count;
    	
    	$annual_used_count = $total_working_stat->annual_used_count;
    	$personal_used_count = $total_working_stat->personal_used_count;*/    	
    	
    	
        $this->load->model('St_payroll_data_m', 'payroll_data_m');
		$payroll_data = $this->payroll_data_m->get_last_by_user($user->id);
		$last_annual_leave_end = 0;
		$last_personal_leave_end = 0;
		$last_inlieu_end = 0;
		if($payroll_data){
    		$this->load->model('St_payroll_m', 'payroll_m');    		
    		
    		$last_payroll = $this->payroll_m->get_last();
			
			$last_annual_leave_end = $payroll_data->annual_leave_end;
			$last_personal_leave_end = $payroll_data->personal_leave_end;
			
	    	$annual_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_ANNUAL, true, false, $last_payroll->end_date);
	    	$annual_upcoming_used = IS_NULL($annual_upcoming_used)?0:$annual_upcoming_used;

    		$personal_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, true, false, $last_payroll->end_date);
    		$personal_upcoming_used = IS_NULL($personal_upcoming_used)?0:$personal_upcoming_used;
    		
    		$inlieu_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_INLIEU, true, false, $last_payroll->end_date);
    		$inlieu_upcoming_used = IS_NULL($inlieu_upcoming_used)?0:$inlieu_upcoming_used;
    		
			$last_annual_leave_end = $payroll_data->annual_leave_end;
			$last_personal_leave_end = $payroll_data->personal_leave_end;
			$last_inlieu_end = $payroll_data->inlieu_end;
			
			$unpaid_upcoming = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_UNPAID, true, false, $last_payroll->end_date);
			
			log_message("debug", "1 annual_hours_count === ". $last_annual_leave_end);
			log_message("debug", "1 personal_hours_count === ". $last_personal_leave_end);
		}else{
			$this->load->model('St_working_hours_m', 'working_hours_m');
			$total_working_stat = $this->working_hours_m->get_staff_working_hours($user_name);
			
	    	$annual_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_ANNUAL, true);
	    	$annual_upcoming_used = IS_NULL($annual_upcoming_used)?0:$annual_upcoming_used;
    	
    		$personal_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, true);
    		$personal_upcoming_used = IS_NULL($personal_upcoming_used)?0:$personal_upcoming_used;
    		
    		$inlieu_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_INLIEU, true);
    		$inlieu_upcoming_used = IS_NULL($inlieu_upcoming_used)?0:$inlieu_upcoming_used;
    		
    		$unpaid_upcoming = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_UNPAID, true);
			
			log_message("debug", "2 annual_hours_count === ". $total_working_stat->annual_hours_count);
			log_message("debug", "2 personal_hours_count === ". $total_working_stat->personal_hours_count);
			$last_annual_leave_end = get_number($total_working_stat->annual_hours_count);
			$last_personal_leave_end = get_number($total_working_stat->personal_hours_count);
		}
    	
		$annual_available = round($last_annual_leave_end, 2);
		$personal_available = round($last_personal_leave_end, 2);
    	
    	//calculate the annual leave balance
    	$annual_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_ANNUAL);
//    	$annual_upcoming_used = $this->request_m->get_upcoming_count_by_leave_type($user_name, LEAVE_TYPE_ANNUAL);
//    	$annual_upcoming_used = IS_NULL($annual_upcoming_used)?0:$annual_upcoming_used;
    	$data['annual_upcoming'] = $annual_upcoming_used;
    	$data['annual_available'] = round($annual_available, 2);
    	$data['total_annual'] = round($annual_available - $annual_upcoming_used, 2);
    	
    	$data['total_inlieu'] = round($last_inlieu_end - $inlieu_upcoming_used, 2);
    	
    	$personal_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL);
//    	$personal_upcoming_used = $this->request_m->get_upcoming_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL);
//    	$personal_upcoming_used = IS_NULL($personal_upcoming_used)?0:$personal_upcoming_used;
    	$data['personal_upcoming'] = $personal_upcoming_used;
    	$data['personal_available'] = round($personal_available - $personal_upcoming_used, 2);
    	$data['total_personal'] = round($personal_available - $personal_upcoming_used, 2);
    	
    	$data['unpaid_upcoming'] = $unpaid_upcoming;
    	
    	
    	$this_year = this_year();
    	 
    	
    	//annual
    	$annual_7_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_ANNUAL, date_add('d', -7, $now_day), $now_time);
    	$data['annual_7_days'] = $annual_7_stat->daycount;
    	$data['annual_7_times'] = IS_NULL($annual_7_stat->timecount)?0:round($annual_7_stat->timecount, 2);
    	
    	$annual_30_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_ANNUAL, date_add('d', -30, $now_day), $now_time);
    	$data['annual_30_days'] = $annual_30_stat->daycount;
    	$data['annual_30_times'] = IS_NULL($annual_30_stat->timecount)?0:round($annual_30_stat->timecount, 2);
    	
    	$annual_90_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_ANNUAL, date_add('d', -90, $now_day), $now_time);
    	$data['annual_90_days'] = $annual_90_stat->daycount;
    	$data['annual_90_times'] = IS_NULL($annual_90_stat->timecount)?0:round($annual_90_stat->timecount, 2);
    	
    	$annual_180_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_ANNUAL, date_add('d', -180, $now_day), $now_time);
    	$data['annual_180_days'] = $annual_180_stat->daycount;
    	$data['annual_180_times'] = IS_NULL($annual_180_stat->timecount)?0:round($annual_180_stat->timecount, 2);
    	
    	$annual_360_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_ANNUAL, date_add('d', -360, $now_day), $now_time);
    	$data['annual_360_days'] = $annual_360_stat->daycount;
    	$data['annual_360_times'] = IS_NULL($annual_360_stat->timecount)?0:round($annual_360_stat->timecount, 2);
    	
    	$annual_thisyear_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_ANNUAL, $this_year[0], $this_year[1]);
    	$data['annual_thisyear_days'] = $annual_thisyear_stat->daycount;
    	$data['annual_thisyear_times'] = IS_NULL($annual_thisyear_stat->timecount)?0:round($annual_thisyear_stat->timecount, 2);
    	
    	$annual_all_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_ANNUAL);
    	$data['annual_all_days'] = $annual_all_stat->daycount;
    	$data['annual_all_times'] = IS_NULL($annual_all_stat->timecount)?0:round($annual_all_stat->timecount, 2);
    	
    	$personal_7_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, date_add('d', -7, $now_day), $now_time);
    	$data['personal_7_days'] = $personal_7_stat->daycount;
    	$data['personal_7_times'] = IS_NULL($personal_7_stat->timecount)?0:round($personal_7_stat->timecount, 2);
    	
    	$personal_30_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, date_add('d', -30, $now_day), $now_time);
    	$data['personal_30_days'] = $personal_30_stat->daycount;
    	$data['personal_30_times'] = IS_NULL($personal_30_stat->timecount)?0:round($personal_30_stat->timecount, 2);
    	
    	$personal_90_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, date_add('d', -90, $now_day), $now_time);
    	$data['personal_90_days'] = $personal_90_stat->daycount;
    	$data['personal_90_times'] = IS_NULL($personal_90_stat->timecount)?0:round($personal_90_stat->timecount, 2);
    	
    	$personal_180_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, date_add('d', -180, $now_day), $now_time);
    	$data['personal_180_days'] = $personal_180_stat->daycount;
    	$data['personal_180_times'] = IS_NULL($personal_180_stat->timecount)?0:round($personal_180_stat->timecount, 2);
    	
    	$personal_360_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, date_add('d', -360, $now_day), $now_time);
    	$data['personal_360_days'] = $personal_360_stat->daycount;
    	$data['personal_360_times'] = IS_NULL($personal_360_stat->timecount)?0:round($personal_360_stat->timecount, 2);
    	
    	$personal_thisyear_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, $this_year[0], $this_year[1]);
    	$data['personal_thisyear_days'] = $personal_thisyear_stat->daycount;
    	$data['personal_thisyear_times'] = IS_NULL($personal_thisyear_stat->timecount)?0:round($personal_thisyear_stat->timecount, 2);
    	 
    	
    	$personal_all_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_PERSONAL);
    	$data['personal_all_days'] = $personal_all_stat->daycount;
    	$data['personal_all_times'] = IS_NULL($personal_all_stat->timecount)?0:round($personal_all_stat->timecount, 2);
    	
    	
    	$unpaid_7_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_UNPAID, date_add('d', -7, $now_day), $now_time);
    	$data['unpaid_7_days'] = $unpaid_7_stat->daycount;
    	$data['unpaid_7_times'] = IS_NULL($unpaid_7_stat->timecount)?0:round($unpaid_7_stat->timecount, 2);
    	 
    	$unpaid_30_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_UNPAID, date_add('d', -30, $now_day), $now_time);
    	$data['unpaid_30_days'] = $unpaid_30_stat->daycount;
    	$data['unpaid_30_times'] = IS_NULL($unpaid_30_stat->timecount)?0:round($unpaid_30_stat->timecount, 2);
    	 
    	$unpaid_90_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_UNPAID, date_add('d', -90, $now_day), $now_time);
    	$data['unpaid_90_days'] = $unpaid_90_stat->daycount;
    	$data['unpaid_90_times'] = IS_NULL($unpaid_90_stat->timecount)?0:round($unpaid_90_stat->timecount, 2);
    	 
    	$unpaid_180_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_UNPAID, date_add('d', -180, $now_day), $now_time);
    	$data['unpaid_180_days'] = $unpaid_180_stat->daycount;
    	$data['unpaid_180_times'] = IS_NULL($unpaid_180_stat->timecount)?0:round($unpaid_180_stat->timecount, 2);
    	 
    	$unpaid_360_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_UNPAID, date_add('d', -360, $now_day), $now_time);
    	$data['unpaid_360_days'] = $unpaid_360_stat->daycount;
    	$data['unpaid_360_times'] = IS_NULL($unpaid_360_stat->timecount)?0:round($unpaid_360_stat->timecount, 2);
    	 
    	$unpaid_thisyear_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_UNPAID, $this_year[0], $this_year[1]);
    	$data['unpaid_thisyear_days'] = $unpaid_thisyear_stat->daycount;
    	$data['unpaid_thisyear_times'] = IS_NULL($unpaid_thisyear_stat->timecount)?0:round($unpaid_thisyear_stat->timecount, 2);
    	
    	$unpaid_all_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_UNPAID);
    	$data['unpaid_all_days'] = $unpaid_all_stat->daycount;
    	$data['unpaid_all_times'] = IS_NULL($unpaid_all_stat->timecount)?0:round($unpaid_all_stat->timecount, 2);
    	
    	
    	$late_7_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LATE, date_add('d', -7, $now_day), $now_time);
    	$data['late_7_days'] = $late_7_stat->daycount;
    	$data['late_7_times'] = IS_NULL($late_7_stat->timecount)?0:round($late_7_stat->timecount * 60, 2);
    	
    	$late_30_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LATE, date_add('d', -30, $now_day), $now_time);
    	$data['late_30_days'] = $late_30_stat->daycount;
    	$data['late_30_times'] = IS_NULL($late_30_stat->timecount)?0:round($late_30_stat->timecount * 60, 2);
    	
    	$late_90_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LATE, date_add('d', -90, $now_day), $now_time);
    	$data['late_90_days'] = $late_90_stat->daycount;
    	$data['late_90_times'] = IS_NULL($late_90_stat->timecount)?0:round($late_90_stat->timecount * 60, 2);
    	
    	$late_180_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LATE, date_add('d', -180, $now_day), $now_time);
    	$data['late_180_days'] = $late_180_stat->daycount;
    	$data['late_180_times'] = IS_NULL($late_180_stat->timecount)?0:round($late_180_stat->timecount * 60, 2);
    	
    	$late_360_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LATE, date_add('d', -360, $now_day), $now_time);
    	$data['late_360_days'] = $late_360_stat->daycount;
    	$data['late_360_times'] = IS_NULL($late_360_stat->timecount)?0:round($late_360_stat->timecount * 60, 2);
    	
    	$late_thisyear_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LATE, $this_year[0], $this_year[1]);
    	$data['late_thisyear_days'] = $late_thisyear_stat->daycount;
    	$data['late_thisyear_times'] = IS_NULL($late_thisyear_stat->timecount)?0:round($late_thisyear_stat->timecount, 2);
    	 
    	
    	$late_all_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LATE);
    	$data['late_all_days'] = $late_all_stat->daycount;
    	$data['late_all_times'] = IS_NULL($late_all_stat->timecount)?0:round($late_all_stat->timecount * 60, 2);
    	
    	$lunch_7_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LUNCH, date_add('d', -7, $now_day), $now_time);
    	$data['lunch_7_days'] = $lunch_7_stat->daycount;
    	$data['lunch_7_times'] = IS_NULL($lunch_7_stat->timecount)?0:round($lunch_7_stat->timecount * 60, 2);
    	
    	$lunch_30_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LUNCH, date_add('d', -30, $now_day), $now_time);
    	$data['lunch_30_days'] = $lunch_30_stat->daycount;
    	$data['lunch_30_times'] = IS_NULL($lunch_30_stat->timecount)?0:round($lunch_30_stat->timecount * 60, 2);
    	
    	$lunch_90_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LUNCH, date_add('d', -90, $now_day), $now_time);
    	$data['lunch_90_days'] = $lunch_90_stat->daycount;
    	$data['lunch_90_times'] = IS_NULL($lunch_90_stat->timecount)?0:round($lunch_90_stat->timecount * 60, 2);
    	
    	$lunch_180_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LUNCH, date_add('d', -180, $now_day), $now_time);
    	$data['lunch_180_days'] = $lunch_180_stat->daycount;
    	$data['lunch_180_times'] = IS_NULL($lunch_180_stat->timecount)?0:round($lunch_180_stat->timecount * 60, 2);
    	
    	$lunch_360_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LUNCH, date_add('d', -360, $now_day), $now_time);
    	$data['lunch_360_days'] = $lunch_360_stat->daycount;
    	$data['lunch_360_times'] = IS_NULL($lunch_360_stat->timecount)?0:round($lunch_360_stat->timecount * 60, 2);
    	
    	$lunch_thisyear_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LUNCH, $this_year[0], $this_year[1]);
    	$data['lunch_thisyear_days'] = $lunch_thisyear_stat->daycount;
    	$data['lunch_thisyear_times'] = IS_NULL($lunch_thisyear_stat->timecount)?0:round($lunch_thisyear_stat->timecount, 2);    	

    	$lunch_all_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LUNCH);
    	$data['lunch_all_days'] = $lunch_all_stat->daycount;
    	$data['lunch_all_times'] = IS_NULL($lunch_all_stat->timecount)?0:round($lunch_all_stat->timecount * 60, 2);
    	
    	
    	$clock_7_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_CLOCK, date_add('d', -7, $now_day), $now_time);
    	$data['clock_7_days'] = $clock_7_stat->daycount;

    	$clock_30_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_CLOCK, date_add('d', -30, $now_day), $now_time);
    	$data['clock_30_days'] = $clock_30_stat->daycount;

    	$clock_90_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_CLOCK, date_add('d', -90, $now_day), $now_time);
    	$data['clock_90_days'] = $clock_90_stat->daycount;

    	$clock_180_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_CLOCK, date_add('d', -180, $now_day), $now_time);
    	$data['clock_180_days'] = $clock_180_stat->daycount;

    	$clock_360_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_CLOCK, date_add('d', -360, $now_day), $now_time);
    	$data['clock_360_days'] = $clock_360_stat->daycount;
    	
    	$clock_thisyear_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_CLOCK, $this_year[0], $this_year[1]);
    	$data['clock_thisyear_days'] = $clock_thisyear_stat->daycount;

    	$clock_all_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_CLOCK);
    	$data['clock_all_days'] = $clock_all_stat->daycount;

    	
    	
    	$data['form_id'] = 'past_leave';
    	$data['requests'] = $this->request_m->get_approved_requests_by_leave_type(FALSE, $user_name, $request->leave_type_id);
    	
    	
    	$compasionate_used = $this->request_m->get_upcoming_count_by_leave_type($user_name, LEAVE_TYPE_COMPASIONATE);
    	$no_mc_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, false, true);

    	$data['comp_used'] = round($compasionate_used, 2);
    	$data['no_mc_used'] = round($no_mc_used, 2);
    	
    	$max_no_mc = $this->workinghour->calculate_max_no_mc($user->id);
    	$data['max_no_mc'] = $max_no_mc;
    	$data['max_comp_leave'] = $max_no_mc;
    	
    	$data['no_mc_leave_left'] = $max_no_mc - $no_mc_used;
    	$data['comp_leave_left'] = $max_no_mc - $compasionate_used;
    	
    	if ($request->leave_type_id == LEAVE_TYPE_PERSONAL)
    		$data['no_mc_leave_left'] -= $request->hours_used;
    	elseif ($request->leave_type_id == LEAVE_TYPE_COMPASIONATE)
    		$data['comp_leave_left'] -= $request->hours_used;
    	
    	$data['no_mc_leave_left'] = round($data['no_mc_leave_left'],2);
    	$data['comp_leave_left'] = round($data['comp_leave_left'],2);
    	
    	
    	$total_count = $this->request_m->count_approved_requests_by_leave_type(FALSE, $user_name,$request->leave_type_id);
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	
    	log_message("debug", "deal page count ==== " . $page_count);
    	
    	//load request log model
		$this->load->model('St_request_log_m', 'request_log_m');
    	$request_logs = $this->request_log_m->get_by_search($request_id, "", "", "", "", "", "", "");
		$log_list = array();
		foreach($request_logs AS $request_log){
			$log['request_id'] = $request_log->id;
			//Action
			$operation = $request_log->operation;
			$action = "";
			if("save" == $operation){
				$action = "saved new request";
			}else if("apply" == $operation){
				$action = "applied request";
			}else if("update" == $operation){
				$action = "updated request";
			}else if("approve" == $operation){
				$action = "approved request";
			}else if("rejected" == $operation){
				$action = "rejected request";
			}else if("cancel" == $operation){
				$action = "canceled request";
			}
			$log['action'] = $action;
			//Actioned On
			$log['actioned_on'] = date(DATETIME_FORMAT, $request_log->operate_time);
			//Actioned By
			$log['actioned_by'] = $request_log->operator_first_name . " " . $request_log->operator_last_name;
			$log['action_ip'] = $request_log->operate_ip;
			$log_list[] = $log;
		}
		$data['request_logs'] = $log_list;
		
		
		// Client Visit.

		$this->load->model('St_client_visit_m', 'client_visit_m');
		$client_visit = $this->client_visit_m->get_by_id($request_id, 'request');
		
		if ($client_visit){
			$data['order_no'] = $client_visit->ordno;
			if ($client_visit->ordid > 0)
				$data['order_url'] = SCORPY_HOME."/orders_forms.php?type=form&ordid=".$client_visit->ordid;
			else
				$data['order_url'] = false;
			
			
			$data['client_visit_id'] = $client_visit->id;
			$data['client_visit_url'] = site_url('client_visit/form/view/'.$client_visit->id);
		}

			
		// Show approving section based on user level
		$this->load->model('St_user_level_m', 'user_level_m');

		$user_lv_1 = array_pop($this->user_level_m->get_userlevel_single($request->user_level_id + 1));
		if (!$user_lv_1) 
			$user_lv_1 = array_pop($this->user_level_m->get_userlevel_single($request->user_level_id));
		
		$user_lv_2 = array_pop($this->user_level_m->get_userlevel_single($request->user_level_id + 2));
		

		$data['first_approval'] = false;
		$data['first_approval_lv'] = false;
		$data['second_approval'] = false;
		$data['second_approval_lv'] = false;
		
		if ($user_lv_1){
			$data['first_approval'] = $user_lv_1->user_level ;
			$data['first_approval_lv'] = $user_lv_1->id;
		}
		
		if ($user_lv_2 && $request->hours_used >= 22.8){
			$data['second_approval'] = $user_lv_2->user_level;
			$data['second_approval_lv'] = $user_lv_2->id;
		}

		// Global Sales users can approve all Sales
		$department = $this->session->userdata('department');
		$data['can_approve_all'] = false;
		if ($department == 'Global Sales' && ($user->department == 'Sales' || $user->department == 'Corporate') ){
			$data['can_approve_all'] = true;
		}
		
		// Operation can approve all
		$user_level = $this->session->userdata('user_level');
		if ($user_level  == '4'){
			$data['can_approve_all'] = true;
		}
		
		
    	$this->load->view('navigation', $data);
    	$this->load->view('request/deal_request', $data);
    }

    /**
     * list the reqeust history
     * @return json
     */
	function past_leave_ajax(){
    	log_message("debug", "request past_leave_ajax  .....");
		
    	$data['form_id'] = 'past_leave';
    	$user_name = $this->input->post('user_name');
    	
    	$order_by = $this->input->post('past_leave_order_by');        
        $order = $this->input->post('past_leave_order');
    	$leave_type_id = $this->input->post('past_leave_filter_leave_type_id');
    	$time_period_id = $this->input->post('past_leave_filter_time_period_id');
    	$page_no = $this->input->post('past_leave_page_no');

		$limit = $this->input->post('past_leave_limit');
    	$data['page_limit'] = $limit;
    	log_message("debug", "past_leave_ajax user_name ==== " .$user_name );
    	log_message("debug", "past_leave_ajax leave_type_id ==== " .$leave_type_id );
    	log_message("debug", "past_leave_ajax order_by ==== " .$order_by );
    	
    	$time_period = get_time_period($time_period_id);
		$data['requests'] = $this->request_m->get_approved_requests_by_leave_type(FALSE, $user_name,$leave_type_id, $time_period[0], $time_period[1], $order_by, $order, $page_no, $limit);
		
		$total_count = $this->request_m->count_approved_requests_by_leave_type(FALSE, $user_name,$leave_type_id, $time_period[0],$time_period[1]);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
		//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
		
		log_message("debug", "saved ajax request/list_requests_data....");
		
		$content = $this->load->view('request/list_requests_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }
	function do_deal(){
		$dnow = now();
		$this->load->model('St_users_m', 'users_m');
    	$request_id = $this->input->post('request_id');
    	$request_hours = $this->input->post('request_hours');
        
    	$operator = $this->session->userdata('user_name');
		$this->load->library('phpmail');
		$this->phpmail->readConfig();
		
		
		
		
		$requestinfo = $this->request_m->get_info($request_id);
		
		$first_approval_lv 		= $this->input->post('first_approval_lv');
		$second_approval_lv 	= $this->input->post('second_approval_lv');
		
		$mc_provided 			= $this->input->post('mc_provided');
		$reason_acceptable 		= $this->input->post('reason_acceptable');
		$supervisor_approved 	= $this->input->post('supervisor_approved');
		$paid 					= $this->input->post('paid');
		$supervisor_comment 	= $this->input->post('supervisor_comment');
		
		$approve 				= $this->input->post('approve');
		$manager_comment 		= $this->input->post('manager_comment');
		
		
		// Override paid option as unpaid for Unpaid leave type
		if ($requestinfo->leave_type_id == LEAVE_TYPE_UNPAID){
			$paid = 2;
		}elseif (!$paid){
			$paid = 1;
		}
		
		$state = 20;
		
		if ($reason_acceptable){
			if($supervisor_approved == 2){
				$state = 50;
			}else if($supervisor_approved == 1){
				// If leave hours less than 22.8 supervisor approval only.
				if ($request_hours > 22.8 && $this->session->userdata('user_level') < 4)
					$state = 30;
				else
					$state = 40;
					
			}			
			
			$request_data = array(
					'reason_acceptable'     => $reason_acceptable,
					'supervisor_approved'   => $supervisor_approved,
					'paid'   				=> $paid,
					'supervisor_comment'    => $supervisor_comment,
					'mc_provided'			=> empty($mc_provided) ? 0 : $mc_provided ,
					'supervisor_user_name'  => $operator,
					'state'  				=> $state,
					'status'				=> 1,
					'supervisor_time' 		=> $dnow
			);
			if ($state == 40){
				$request_data['approve'] = $supervisor_approved;
			}
		}elseif ($approve){
			if($approve == 2){
				$state = 50;
			}else if($approve == 1){
				$state = 40;
			}
						
			$request_data = array(
					'paid'     			=> $paid,
					'approve'   		=> $approve,
					'manager_comment'  	=> $manager_comment,
					'manager_user_name' => $operator,
					'mc_provided'   	=> $mc_provided,
					'state'  			=> $state,
					'status'			=> 1,
					'manager_time' 		=> $dnow
			);
		}
		
		$b_update = $this->request_m->update($request_id, $request_data);		
		
		
		if($b_update){
			// Log
			$approve_log = "";
			if($state == 50){
				$approve_log = "rejected";
			}else if($state == 40){
				$approve_log = "approve";
			}else if ($state == 30){
				$approve_log = "intermediate approve";
			}
			
			$this->load->model('St_request_log_m', 'request_log_m');
			$log = array(
					'id' => $request_id,
					'operator_id' => $this->session->userdata('user_id'),
					'operation' => $approve_log,
					'operate_time' => $dnow,
					'operate_ip' => $this->input->ip_address()
			);
			$log = array_merge($request_data, $log);
			$this->request_log_m->insert($log);
			
			
			// Email notification
			$requestinfo = $this->request_m->get_info($request_id);
			
			// Calculate total annual/personal leave balance
			$amount_leave = "";
			if($requestinfo->leave_type_id == LEAVE_TYPE_ANNUAL || $requestinfo->leave_type_id == LEAVE_TYPE_PERSONAL){

				$this->load->model('St_payroll_data_m', 'payroll_data_m');
				$payroll_data = $this->payroll_data_m->get_last_by_user($requestinfo->user_id);
				$last_annual_leave_end = 0;
				$last_personal_leave_end = 0;
				if($payroll_data){
					$last_annual_leave_end = $payroll_data->annual_leave_end;
					$last_personal_leave_end = $payroll_data->personal_leave_end;
				}else{
					$this->load->model('St_working_hours_m', 'working_hours_m');
					$total_working_stat = $this->working_hours_m->get_staff_working_hours($requestinfo->user_name);
			
					$last_annual_leave_end = get_number($total_working_stat->annual_hours_count);
					$last_personal_leave_end = get_number($total_working_stat->personal_hours_count);
				}
					
				$annual_available = round($last_annual_leave_end, 2);
				$personal_available = round($last_personal_leave_end, 2);
			
				if($requestinfo->leave_type_id == LEAVE_TYPE_ANNUAL){
					$annual_upcoming_used = $this->request_m->get_upcoming_count_by_leave_type($requestinfo->user_name, LEAVE_TYPE_ANNUAL);
					$annual_upcoming_used = IS_NULL($annual_upcoming_used)?0:$annual_upcoming_used;
					$amount_leave_count = $annual_available - $annual_upcoming_used;
					$amount_leave_count = round($amount_leave_count, 2);
					$amount_leave = ($amount_leave_count > 0)?$amount_leave_count:0;
				}else if($requestinfo->leave_type_id == LEAVE_TYPE_PERSONAL){
					$personal_upcoming_used = $this->request_m->get_upcoming_count_by_leave_type($requestinfo->user_name, LEAVE_TYPE_PERSONAL);
					$personal_upcoming_used = IS_NULL($personal_upcoming_used)?0:$personal_upcoming_used;
					$amount_leave_count = $personal_available - $personal_upcoming_used;
					$amount_leave_count = round($amount_leave_count, 2);
					$amount_leave = ($amount_leave_count > 0)?$amount_leave_count:0;
				}
			}
			 
			if($amount_leave > 1){
				$mail_body['amount_leave_left'] = $amount_leave . " Hours";
			}elseif ($amount_leave){
				$mail_body['amount_leave_left'] = $amount_leave . " Hour";
			}else{
				$mail_body['amount_leave_left'] = false;
			}
			
			 
			if ($state < 40){
				$mail_body['appove_url'] = site_url('approve/deal/'.$request_id);
				$mail_body['appove_name'] = "approve this leave application here.";
			}else{
				$mail_body['appove_url'] = site_url('approve/deal/'.$request_id);
				$mail_body['appove_name'] = "view this leave application here.";
			}
			 
			 
			// Get approving supervisor/manager name.
			$requestinfo->supervisor_name = '';
			$requestinfo->manager_name = '';
			if ($requestinfo->supervisor_user_name){
				$s_info = $this->users_m->get_by_user_name($requestinfo->supervisor_user_name);
				$requestinfo->supervisor_name = $s_info->first_name ." ".$s_info->last_name;
			}
			if ($requestinfo->manager_user_name){
				$m_info = $this->users_m->get_by_user_name($requestinfo->manager_user_name);
				$requestinfo->manager_name = $m_info->first_name ." ".$m_info->last_name;
			}
			
			 
			// Get client visit order.
			$this->load->model('St_client_visit_m', 'client_visit_m');
			$client_visit = $this->client_visit_m->get_by_id($request_id, 'request');
			$mail_body['order_no'] = false;
			$mail_body['order_url'] = false;
			$mail_body['qb_name'] = false;
			
			if ($client_visit){
				$mail_body['order_no'] = $client_visit->ordno;
				if ($client_visit->ordid > 0){
					$mail_body['order_url'] = SCORPY_HOME."/orders_forms.php?type=form&ordid=".$client_visit->ordid;
					$mail_body['qb_name'] = $client_visit->client_qb_name;
				}
			}
			 
			$mail_body['request'] = $requestinfo;
			
			if ($requestinfo->site_id){
				$this->load->model('St_site_m', 'site_m');
			
				$store = $this->site_m->get($requestinfo->site_id);
				$mail_body['store'] = $store->name;
			}
				
			
			$this->phpmail->setBody($this->load->view('mail/approve', $mail_body , True));
			
			
			// Get email addresses
			// Notify second level approval.
			if ($state == 30){
				if ($first_approval_lv == 2)
					$second_level_email = $this->users_m->get_managers_by_user_name($requestinfo->user_name);
/* 				elseif ($first_approval_lv == 3)
					$second_level_email = $this->users_m->get_operation_by_user_name($requestinfo->user_name);
 */			
				if(!empty($second_level_email)){
					foreach($second_level_email as $sec){
						if(!empty($sec)){
							if(!empty($sec->email)){
								$this->phpmail->AddAddress($sec->email);
							}
						}
					}
				}
				
				// Always send email to operation for second approval
				$operation_email = $this->users_m->get_operation_by_user_name($requestinfo->user_name);
				if(!empty($operation_email)){
					foreach($operation_email as $sec){
						if(!empty($sec)){
							if(!empty($sec->email)){
								$this->phpmail->AddAddress($sec->email);
							}
						}
					}
				}
				
				
				
				$subject = "Requires Second Approval ". $requestinfo->leave_type . " Request R". $request_id ." - "
						.$requestinfo->first_name . " " . $requestinfo->last_name
						." From ". date(DATE_FORMAT, $requestinfo->start_day)." to ".date(DATE_FORMAT, $requestinfo->end_day);
					
					
				$this->phpmail->setSubject($subject);
				
				if(!$this->phpmail->send()){
					 log_message("debug", "send mail .... ".$subject . $this->phpmail->ErrorInfo);
				}
				
			}
			
			$this->phpmail->ClearAllAddresses();
		
			// Send email to requesting person
			if(!empty($requestinfo->email)){
				$this->phpmail->AddAddress($requestinfo->email);
			}
/* 			if(!empty($requestinfo->personal_email)){
				$this->phpmail->AddAddress($requestinfo->personal_email);
			} */
			$mail_body['appove_url'] = site_url('request/view/'.$request_id);
			$mail_body['appove_name'] = "view this leave application here.";
			
			
			if($state == 50){
				$approve_operate = "Rejected";
			}elseif ($state == 30){
				$approve_operate = "First Approval";
			}else{
				$approve_operate = "Approved";
			}
			
			
			$subject = $approve_operate . " ". $requestinfo->leave_type . " Request R". $request_id ." - "
					.$requestinfo->first_name . " " . $requestinfo->last_name
					." From ". date(DATE_FORMAT, $requestinfo->start_day)." to ".date(DATE_FORMAT, $requestinfo->end_day);
				
				
			$this->phpmail->setSubject($subject);
			 
			$this->phpmail->setBody($this->load->view('mail/approve', $mail_body , True));
			
    		if(!$this->phpmail->send()){
				log_message("debug", "send mail .... ".$subject . $this->phpmail->ErrorInfo);
			}
			
			$this->phpmail->ClearAllAddresses();
		
		}else{
			$this->session->set_flashdata('message', $this->load->view('msgbox/error', array('error' => 'Operation failed !') , True));
			redirect("approve/deal/" . $request_id);
	    }

		redirect("approve#waiting", "refresh");
    }
    
    function do_update_deal(){
    	$dnow = now();
		$this->load->model('St_users_m', 'users_m');
    	$request_id = $this->input->post('request_id');
        
    	$operator = $this->session->userdata('user_name');
		$this->load->library('phpmail');
		$this->phpmail->readConfig();
    	
		if ($this->input->post('operate')=='update'){
            $start_day 		= $this->input->post('start_day');
            $end_day 		= $this->input->post('end_day');
            $start_time 	= $this->input->post('start_time');
            $end_time 		= $this->input->post('end_time');
            $hours_used 	= $this->input->post('hours_used');
            $mc_provided 	= $this->input->post('mc_provided');
            $mc_approved 	= $this->input->post('mc_approved');
            $carer_leave 	= $this->input->post('carer_leave');
            $leave_type 	= $this->input->post('leave_type');
            $reason 		= $this->input->post('reason');
            $paid	 		= $this->input->post('paid');
            $sup_comment 	= $this->input->post('sup_comment');
            $man_comment 	= $this->input->post('man_comment');

            
            if ($leave_type == LEAVE_TYPE_CASUAL){
            	$site_id = $this->input->post('site_id');
            	$dept_id = $this->input->post('department_id');
            }elseif ($leave_type == LEAVE_TYPE_INTERSTORE){
            	$site_id = $this->input->post('site_id');
            }else{
            	$site_id = '';
            	$dept_id = '';
            }
            
            $request_type = $this->input->post('request_type');
            
            $mc_filename = "";
            if($this->input->post('leave_type') == LEAVE_TYPE_LATE || $this->input->post('leave_type') == LEAVE_TYPE_LUNCH || $this->input->post('leave_type') == LEAVE_TYPE_CLOCK){
            	$start_day = $this->input->post('arrival_day');
            	$start_time = $this->input->post('arrival_time');
            	$hours_used = $this->input->post('minutes_used')/60;//The field is repaced with minutes here
            	$end_day = $this->input->post('arrival_day');
            	$end_time = "";
            	$request_type = 2;
            }
            
			$include_break = 0;
            
			if($request_type == "1"){
            	$start_time = "";
            	$end_time = "";
            }else if($request_type == "2"){
            	$end_day = $start_day;
            	$include_break = $this->input->post('include_break');
            }
			
			$request_data = array(
				'leave_type_id' => $leave_type,
				'request_type'  => $request_type,
				'include_break' => $include_break,
                'start_day' 	=> strtotime($start_day),
                'end_day' 		=> strtotime($end_day),
                'start_time' 	=> $start_time,
                'end_time' 		=> $end_time,
				'start_date' 	=> strtotime($start_day ." " . $start_time),
				'end_date' 		=> strtotime($end_day . " " . $end_time),
                'hours_used' 	=> $hours_used,
				'mc_provided'	=> $mc_provided,
				'mc_approved'	=> $mc_approved,
				'carer_leave' 	=> $carer_leave,
				'reason'	 	=> $reason,
				'supervisor_comment' => $sup_comment,
				'manager_comment' => $man_comment,
				'site_id'		=> $site_id,
				'department_id'	=> $dept_id
            );
			
			if ($paid)
				$request_data['paid'] = $paid;
			
	    	$b_update = $this->request_m->update($request_id, $request_data);
			
	    	if ($b_update){
	           	$this->load->model('St_request_log_m', 'request_log_m');
	           	$log = array(
	           		'id' => $request_id,
	           		'operator_id' => $this->session->userdata('user_id'),
	           		'operation' => 'update',
	           		'operate_time' => $dnow,
	           		'operate_ip' => $this->input->ip_address()
	           	);
	           	$log = array_merge($request_data, $log);
	           	$this->request_log_m->insert($log);
	    	}
//	    	redirect("request/view/".$request_id);
	    	redirect("calendar");
		}
		else if($this->input->post('operate')=='delete'){
			//send a mail
			$requestinfo = $this->request_m->get_info($request_id);
				
			
			// Get approving supervisor/manager name.
			$requestinfo->supervisor_name = '';
			$requestinfo->manager_name = '';
			if ($requestinfo->supervisor_user_name){
				$s_info = $this->users_m->get_by_user_name($requestinfo->supervisor_user_name);
				$requestinfo->supervisor_name = $s_info->first_name ." ".$s_info->last_name;
			}
			if ($requestinfo->manager_user_name){
				$m_info = $this->users_m->get_by_user_name($requestinfo->manager_user_name);
				$requestinfo->manager_name = $m_info->first_name ." ".$m_info->last_name;
			}
				
			
//			if ($requestinfo->leave_type_id == LEAVE_TYPE_CLIENTVISIT){

				$this->load->model('St_client_visit_m', 'client_visit_m');
				$client_visit = $this->client_visit_m->get_by_id($request_id, 'request');
				
				$mail_body['order_no'] = false;
				$mail_body['order_url'] = false;
				$mail_body['qb_name'] = false;
				
				if ($client_visit){
					$mail_body['order_no'] = $client_visit->ordno;
					if ($client_visit->ordid > 0){
						$mail_body['order_url'] = SCORPY_HOME."/orders_forms.php?type=form&ordid=".$client_visit->ordid;
						$mail_body['qb_name'] = $client_visit->client_qb_name;
					}
				}
				
//			}

				
			if($requestinfo->user_level_id == ROLE_STAFF){
				$supervisors = $this->users_m->get_supervisors_by_user_name($requestinfo->user_name);
				if(!empty($supervisors)){
					foreach($supervisors as $supervisor){
						if(!empty($supervisor)){
							if(!empty($supervisor->email)){
								$this->phpmail->AddAddress($supervisor->email);
							}
						}
					}
				}
			}
			 
			if($requestinfo->user_level_id == ROLE_SUPERVISOR){
				//send mail to all manager and admin
				$managers = $this->users_m->get_managers_by_user_name($requestinfo->user_name);
				if(!empty($managers)){
					foreach($managers as $manager){
						if(!empty($manager)){
							if(!empty($manager->email)){
								$this->phpmail->AddAddress($manager->email);
							}
						}
					}
				}
			}
			if($requestinfo->user_level_id >= ROLE_MANAGER){
				$operations = $this->users_m->get_operation_by_user_name($requestinfo->user_name);
				if(!empty($operations)){
					foreach($operations as $operation){
						if(!empty($operation)){
							if(!empty($operation->email)){
								$this->phpmail->AddAddress($operation->email);
								log_message("debug", "operation:".$operation->email);
							}
						}
					}
				}
			}			
	       	
	       	// Send email to requested person.
		    if(!empty($requestinfo->email)){
            	$this->phpmail->AddAddress($requestinfo->email);
            }
	   		
    		$subject = "DELETED ". $requestinfo->leave_type . " Request R". $request_id ." - "
           						.$requestinfo->first_name . " " . $requestinfo->last_name
           						." From ". date(DATE_FORMAT, $requestinfo->start_day)." to ".date(DATE_FORMAT, $requestinfo->end_day);
	   		
    		$this->phpmail->setSubject($subject);
           	$mail_body['appove_url'] = "";
           	$mail_body['appove_name'] = "";
           	$mail_body['request'] = $requestinfo;
           	
           	if ($requestinfo->site_id){
           		$this->load->model('St_site_m', 'site_m');
           	
           		$store = $this->site_m->get($requestinfo->site_id);
           		$mail_body['store'] = $store->name;
           	}
           	
           	$this->phpmail->setBody($this->load->view('mail/approve', $mail_body , True));
		   	if(!$this->phpmail->send()){
	        	log_message("debug", "send mail .... ".$subject . $this->phpmail->ErrorInfo);
   			}           	
	        $this->phpmail->ClearAllAddresses();
			
			
			$this->request_m->delete($request_id);
			redirect("calendar");
		}
    }
    
    
    function approve_edit($request_id){
    	$data['navigation'] = "<a href='". site_url('admin/leave') ."' target='_top'>Leave Request</a> > <a href='". site_url('approve') ."'>Leave Approval</a> > Approve Edit";
    	$request = $this->request_m->get_info($request_id);
    	
    	if(empty($request)){
    		show_404();
    	}
    	
    	//load leave type model
		$this->load->model('St_leave_type_m', 'leave_type_m');
		
    	$leave_types = $this->leave_type_m->get_select_dropdown();
    	$data['leave_types'] = $leave_types;
    	
    	$data['request'] = $request;
    	
    	//load request log model
		$this->load->model('St_request_log_m', 'request_log_m');
    	$request_logs = $this->request_log_m->get_by_search($request_id, "", "", "", "", "", "", "");
		$log_list = array();
		foreach($request_logs AS $request_log){
			$log['request_id'] = $request_log->id;
			//Action
			$operation = $request_log->operation;
			$action = "";
			if("save" == $operation){
				$action = "saved new request";
			}else if("apply" == $operation){
				$action = "applied request";
			}else if("update" == $operation){
				$action = "updated request";
			}else if("approve" == $operation){
				$action = "approved request";
			}else if("rejected" == $operation){
				$action = "rejected request";
			}else if("cancel" == $operation){
				$action = "canceled request";
			}
			$log['action'] = $action;
			//Actioned On
			$log['actioned_on'] = date(DATETIME_FORMAT, $request_log->operate_time);
			//Actioned By
			$log['actioned_by'] = $request_log->oprator_first_name . " " . $request_log->oprator_last_name;
			$log['action_ip'] = $request_log->operate_ip;
			$log_list[] = $log;
		}
		$data['request_logs'] = $log_list;
    	$this->load->view('navigation', $data);
    	$this->load->view('request/approve_edit_request', $data);
    }
    
    
    /**
     * change approve request
     * @return page
     */
    function do_approve_edit(){
        $this->form_validation->set_rules('leave_type', 'Leave Type', 'required');

        $request_id = $this->input->post('request_id');
        $this->load->model('St_users_m', 'users_m');
        if ($this->form_validation->run() == FALSE) {
            $this->edit($request_id);
        } else {
        	$error = "";
        	$operate = $this->input->post('operate');
            $leave_type = $this->input->post('leave_type');
            $start_day = $this->input->post('start_day');
            $end_day = $this->input->post('end_day');
            $start_time = $this->input->post('start_time');
            $end_time = $this->input->post('end_time');
            $hours_used = $this->input->post('hours_used');
            $reason = $this->input->post('reason');
            
            $mc_filename = "";
            if($leave_type == LEAVE_TYPE_LATE
            	|| $leave_type == LEAVE_TYPE_LUNCH){
            	$start_day = $this->input->post('arrival_day');
            	$start_time = $this->input->post('arrival_time');
            	$hours_used = $this->input->post('minutes_used');//The field is repaced with minutes here
            	$end_day = "";
            	$end_time = "";
            }else if($leave_type == LEAVE_TYPE_PERSONAL){
		        $fileElementName = 'medical_certificate';

		        if(empty($_FILES[$fileElementName]['tmp_name']) || $_FILES[$fileElementName]['tmp_name'] == 'none')
				{}else{
					if(!empty($_FILES[$fileElementName]['error']))
					{
						switch($_FILES[$fileElementName]['error'])
						{
							case '1':
								$error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
								break;
							case '2':
								$error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
								break;
							case '3':
								$error = 'The uploaded file was only partially uploaded';
								break;
							case '4':
								$error = 'No file was uploaded.';
								break;
				
							case '6':
								$error = 'Missing a temporary folder';
								break;
							case '7':
								$error = 'Failed to write file to disk';
								break;
							case '8':
								$error = 'File upload stopped by extension';
								break;
							case '999':
							default:
								$error = 'No error code avaiable';
						}
					}else 
					{
						$tempFile = $_FILES[$fileElementName]['tmp_name'];
						
						$now_dir = date('Y/m/d');
						$targetPath = str_replace('//','/',DIR_MEDICAL_CERTIFICATE . $now_dir . '/');
						log_message("debug","targetpath === " .$targetPath);
						if(mkdirs($targetPath)){
							$filename = $_FILES[$fileElementName]['name'];
							
							// Get the extension from the filename.
							$extension = substr($filename, strpos($filename,'.'), strlen($filename)-1);
							
							$target_filename = random_filename($targetPath, $extension, 6);
							$mc_filename = $now_dir . '/' . $target_filename;
							
							$targetFile = $targetPath . $target_filename;
							
							move_uploaded_file($tempFile,$targetFile);
						}else{
							$error = "Can't create directory";
						}
					}
				}
            }

        	if($error != ""){
				$this->session->set_flashdata('auth', $this->load->view('msgbox/error', $error , True));
				$this->edit($request_id);
			}else{
				$include_break = 0;
	            $request_type = $this->input->post('request_type');
				if($request_type == "1"){
	            	$start_time = "";
	            	$end_time = "";
	            }else if($request_type == "2"){
	            	$end_day = $start_day;
	            	$include_break = $this->input->post('include_break');
	            }
	            
	            /*
	             * read user type
	             * read work time
	             * 
	             */
	            $dnow = now();
				$request_data = array(
	                'leave_type_id' => $leave_type,
					'request_type'  => $request_type,
					'include_break' => $include_break,
	                'start_day' 	=> strtotime($start_day),
	                'end_day' 		=> strtotime($end_day),
	                'start_time' 	=> $start_time,
	                'end_time' 		=> $end_time,
					'start_date' 	=> strtotime($start_day ." " . $start_time),
					'end_date' 		=> strtotime($end_day . " " . $end_time),
	                'hours_used' 	=> $hours_used,
	                'reason' 		=> $reason
	            );
	            
	            if(!empty($mc_filename)){
	            	$a_mc = array(
	            		'medical_certificate_file' => $mc_filename
	            	);
	            	$request_data = array_merge($request_data, $a_mc);
	            }
	            
	            //update a request
	            log_message("debug", "update a request id ...... " . $request_id);
	            
				$b_update = $this->request_m->update($request_id, $request_data);
	            if(!$b_update){
	            	$error = "Leave request update failed.";
	            }else{
	            	//add hisotry log
	            	$this->load->model('St_request_log_m', 'request_log_m');
	            	$log = array(
	            		'id' => $request_id,
	            		'operator_id' => $this->session->userdata('user_id'),
	            		'operation' => "changed",
	            		'operate_time' => $dnow,
	            		'operate_ip' => $this->input->ip_address()
	            	);
	            	$log = array_merge($request_data, $log);
	            	$this->request_log_m->insert($log);
	            	
	            	$requestinfo = $this->request_m->get_info($request_id);
	            	$user_name = $requestinfo->user_name;
            		$this->load->library('phpmail');
            		$this->phpmail->readConfig();
            		
            		$supervisors = $this->users_m->get_supervisors_by_user_name($user_name);
        			if(!empty($supervisors)){
            			foreach($supervisors as $supervisor){
            				if(!empty($supervisor)){
            					if(!empty($supervisor->email)){
            						$this->phpmail->AddAddress($supervisor->email);
            					}
//            					if(!empty($supervisor->personal_email)){
//            						$this->phpmail->AddCC($supervisor->personal_email);
//            					}
            				}
            			}
        			}
            		
            		//send mail to all manager and admin
            		$managers = $this->users_m->get_managers_by_user_name($user_name);
        			if(!empty($managers)){
            			foreach($managers as $manager){
            				if(!empty($manager)){
            					if(!empty($manager->email)){
            						$this->phpmail->AddAddress($manager->email);
            					}
//            					if(!empty($manager->personal_email)){
//            						$this->phpmail->AddCC($manager->personal_email);
//            					}
            				}
            			}
        			}
        			$amount_leave = "";
        			if($requestinfo->leave_type_id == LEAVE_TYPE_ANNUAL 
        				|| $requestinfo->leave_type_id == LEAVE_TYPE_PERSONAL){
            			//calculate total annual/personal leave balance
				    	$this->load->model('St_working_hours_m', 'working_hours_m');    	
				    	$total_working_stat = $this->working_hours_m->get_staff_working_hours($user_name);
				    	
				    	$work_count = $total_working_stat->work_count;
				    	
				    	if($requestinfo->leave_type_id == LEAVE_TYPE_ANNUAL){
				    		$total_annual = $total_working_stat->annual_hours_count;
				    		$annual_used_count = $total_working_stat->annual_used_count;
				    		//calculate the annual leave balance
				    		$annual_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_ANNUAL);
				    		$annual_upcoming_used = $this->request_m->get_upcoming_count_by_leave_type($user_name, LEAVE_TYPE_ANNUAL);
				    		$annual_upcoming_used = IS_NULL($annual_upcoming_used)?0:$annual_upcoming_used;
				    		$amount_leave_count = $total_annual - $annual_used - $annual_upcoming_used;
				    		$amount_leave_count = round($amount_leave_count, 2);
				    		$amount_leave = ($amount_leave_count > 0)?$amount_leave_count:0;
				    	}
				    	else if($requestinfo->leave_type_id == LEAVE_TYPE_PERSONAL){
				    		$total_personal = $total_working_stat->personal_hours_count;
				    		$personal_used_count = $total_working_stat->personal_used_count;
				    		//calculate the annual leave balance
				    		$personal_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL);
				    		$personal_upcoming_used = $this->request_m->get_upcoming_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL);
				    		$personal_upcoming_used = IS_NULL($personal_upcoming_used)?0:$personal_upcoming_used;
				    		$amount_leave_count = $total_personal - $personal_used - $personal_upcoming_used;
				    		$amount_leave_count = round($amount_leave_count, 2);
				    		$amount_leave = ($amount_leave_count > 0)?$amount_leave_count:0;
				    	}
        			}
        			
           			if($amount_leave > 1){
		    			$mail_body['amount_leave_left'] = $amount_leave . " Hours";
		    		}elseif ($amount_leave){
		    			$mail_body['amount_leave_left'] = $amount_leave . " Hour";
		    		}else{
		    			$mail_body['amount_leave_left'] = false;
		    		}

        			$subject = "Changed ". $requestinfo->leave_type ." Requested - " 
        				. $requestinfo->first_name . " " . $requestinfo->last_name
        				." From ".$start_day." to ".$end_day;
		    		$this->phpmail->setSubject($subject);
		    		
	            	$mail_body['appove_url'] = site_url('approve/deal/'.$request_id);
	            	$mail_body['appove_name'] = "approve this leave application here.";
	            	$mail_body['request'] = $requestinfo;
	            	
	            	if ($requestinfo->site_id){
	            		$this->load->model('St_site_m', 'site_m');
	            	
	            		$store = $this->site_m->get($requestinfo->site_id);
	            		$mail_body['store'] = $store->name;
	            	}
	            	
	            	
        			$this->phpmail->setBody($this->load->view('mail/request', $mail_body , True));
        			if(!$this->phpmail->send()){
			        	log_message("debug", "send mail .... ".$subject . $this->phpmail->ErrorInfo);
			        }
			        
			        //
			        $this->phpmail->ClearAllAddresses();
            		$email = $this->session->userdata("email");
            		if(!empty($email)){
            			$this->phpmail->AddAddress($email);
        			}
        			$personal_email = $this->session->userdata("personal_email");
        			if(!empty($personal_email)){
        				$this->phpmail->AddAddress($personal_email);	
        			}
        			$mail_body['appove_url'] = site_url('request/view/'.$request_id);
        			$mail_body['appove_name'] = "view this leave application here.";
            		$this->phpmail->setBody($this->load->view('mail/request', $mail_body , True));
        			if(!$this->phpmail->send()){
			        	log_message("debug", "send mail .... ".$subject . $this->phpmail->ErrorInfo);
			        }
	            }
	            
	            //return json 
//	            $result = array(
//		        	"message"=> "",
//		        	"status" 	=> ""
//		        );
		        
//				if(!empty($error)){
//					$result['message'] = $error;
//					$result['status'] = "fail";
//				}else{
//					$result['message'] = "Leave update successfully.";
//					$result['status'] = "ok";
//		        }
//		        echo json_encode($result);
				if(!empty($error)){
					$this->session->set_flashdata('auth', $this->load->view('msgbox/error', $error , True));
					$this->approve_edit($request_id);
				}else{
	            	redirect("approve");
	            }
			}
        }
    }
    
    
	/**
     * view a request
     */
	function view($request_id){
    	$data['navigation'] = "<a href='". site_url('admin/leave') ."' target='_top'>Leave Request</a> > <a href='". site_url('approve') ."'>Leave Approval</a> > View";
    	$request = $this->request_m->get_info($request_id);
    	    	
    	if(empty($request)){
    		show_404();
    	}
    	
    	$data['supervisor_name'] = "";
    	$data['manager_name'] = "";
    	
    	$this->load->model('St_users_m', 'users_m');
    	if(!is_null($request->supervisor_user_name)){
    		$supervisor = $this->users_m->get_by_user_name($request->supervisor_user_name);
    		if(!empty($supervisor)){
    			$data['supervisor_name'] = $supervisor->first_name . " " . $supervisor->last_name;
    		}
    	}
    	
		if(!is_null($request->manager_user_name)){
    		$manager = $this->users_m->get_by_user_name($request->manager_user_name);
    		if(!empty($manager)){
    			$data['manager_name'] = $manager->first_name . " " . $manager->last_name;
    		}
    	}
    	
    	//load leave type model
		$this->load->model('St_leave_type_m', 'leave_type_m');
		
    	$leave_types = $this->leave_type_m->get_select_dropdown();
    	$data['leave_types'] = $leave_types;
    	
    	$this->load->model('St_site_m', 'site_m');
    	 
    	$stores = $this->site_m->get_select_dropdown();
    	$data['stores'] = $stores;
    	 
    	$data['depts'] = false;
    	if ($request->site_id){
    		$this->load->model('St_department_m', 'department_m');
    		$depts = $this->department_m->get_select_dropdown($request->site_id);
    		$data['depts'] = $depts;
    	}    	
    	
    	if($request->leave_type_id == LEAVE_TYPE_LATE || $request->leave_type_id == LEAVE_TYPE_LUNCH)
    		$request->hours_used = $request->hours_used * 60;
    	
    	$data['request'] = $request;
    	
    	//load request log model
		$this->load->model('St_request_log_m', 'request_log_m');
    	$request_logs = $this->request_log_m->get_by_search($request_id, "", "", "", "", "", "", "");
		$log_list = array();
		foreach($request_logs AS $request_log){
			$log['request_id'] = $request_log->id;
			//Action
			$operation = $request_log->operation;
			$action = "";
			if("save" == $operation){
				$action = "saved new request";
			}else if("apply" == $operation){
				$action = "applied request";
			}else if("update" == $operation){
				$action = "updated request";
			}else if("approve" == $operation){
				$action = "approved request";
			}else if("rejected" == $operation){
				$action = "rejected request";
			}else if("cancel" == $operation){
				$action = "canceled request";
			}
			$log['action'] = $action;
			//Actioned On
			$log['actioned_on'] = date(DATETIME_FORMAT, $request_log->operate_time);
			//Actioned By
			$log['actioned_by'] = $request_log->operator_first_name . " " . $request_log->operator_last_name;
			$log['action_ip'] = $request_log->operate_ip;
			$log_list[] = $log;
		}
		$data['request_logs'] = $log_list;
		$data['user_level'] = $this->session->userdata('user_level');
		$data['current_user_id'] = $this->session->userdata('user_id');
		
		// Get client visit order
//		if ($request->leave_type_id == LEAVE_TYPE_CLIENTVISIT){
			$this->load->model('St_client_visit_m', 'client_visit_m');
			$job = $this->client_visit_m->get_by_id($request_id,'request');
			
			if ($job){
				$data['order_no']	= $job->ordno;
				
				if ($job->ordid > 0){
					$data['order_url'] 	= SCORPY_HOME."/orders_forms.php?type=form&ordid=".$job->ordid;
					$data['client_qb_name'] = $job->client_qb_name;
					$data['street'] 		= $job->street;
					$data['suburb'] 		= $job->suburb;
					$data['total_km'] 		= $job->total_km;
				}else{
					$data['order_url'] = false;
				}
				
			}
			
//		}

		// Show actual clock in/out from actatek.
		if ($request->leave_type_id == LEAVE_TYPE_ONSITE || $request->leave_type_id == LEAVE_TYPE_OVERTIME || $request->leave_type_id == LEAVE_TYPE_INTERSTORE
		|| $request->leave_type_id == LEAVE_TYPE_MAKEUP || $request->leave_type_id == LEAVE_TYPE_LATE || $request->leave_type_id == LEAVE_TYPE_LUNCH){
			$job = false;
			$rows = false;
				
			$this->load->model('st_ac_clocking_log_m', 'ac_clocking_m');
			 
			if ($request->leave_type_id == LEAVE_TYPE_ONSITE){
				// Get client visit
				$this->load->model('St_client_visit_m', 'client_visit_m');
				$job = $this->client_visit_m->get_by_id($request_id,'request');
		
				if ($job){
		
					$data['order_url'] 	= SCORPY_HOME."/orders_forms.php?type=form&ordid=".$job->ordid;
					$data['order_no']	= $job->ordno;
					 
					$data['client_qb_name'] = $job->client_qb_name;
					$data['street'] 		= $job->street;
					$data['suburb'] 		= $job->suburb;
					$data['total_km'] 		= $job->total_km;
					 
					 
					$rows = $this->ac_clocking_m->get_actual_clock_by_user($request->user_name, $job->visit_date, $job->visit_date);
				}
			}else{
				$rows = $this->ac_clocking_m->get_actual_clock_by_user($request->user_name, $request->start_day, $request->end_day);
			}
			 
			if ($rows){
				$prev_date = false;
				$user = $this->users_m->get_by_id($request->user_id);
				foreach ($rows as $row){
					if ($prev_date != $row->date){
						$prev_date = $row->date;
						 
						$clocks[$row->date][$user->first_name." ".$user->last_name]['IN'] = array();
						$clocks[$row->date][$user->first_name." ".$user->last_name]['OUT'] = array();
						$clocks[$row->date][$user->first_name." ".$user->last_name]['total'] = 0.00;
						//    					$clocks[$row->date][$user->first_name." ".$user->last_name]['total'] = $this->ac_clocking_m->get_total_working_hours($user->user_name, $row->date);
						$total_hours = 0;
					}
					 
					if ($row->direction == 'IN'){
						array_push($clocks[$row->date][$user->first_name." ".$user->last_name]['IN'], $row->time);
						$in_time = strtotime($row->time);
					}else{
						array_push($clocks[$row->date][$user->first_name." ".$user->last_name]['OUT'], $row->time);
						$out_time = strtotime($row->time);
						 
						$total_hours += $out_time - $in_time;
					}
					 
					$clocks[$row->date][$user->first_name." ".$user->last_name]['last_dir'] = $row->direction;
					 
					if ($total_hours)
						$clocks[$row->date][$user->first_name." ".$user->last_name]['total'] = number_format($total_hours/3600, 2);
					 
				}
				ksort($clocks);
				$data['clock'] = $clocks;
			}
		}			
			
		$this->load->view('navigation', $data);
    	$this->load->view('request/view_request_4', $data);
    }
    
    /**
     * get departments by site id
     * @return json
     */
    function dept_by_site_ajax(){
    	$site_id = $this->input->post('site_id');
    	$select = $this->input->post('select');
    	$result = array();
    	$index = 0;
	    $result[$index]['id'] = "";
	    if($select){
	    	$result[$index]['name'] = "Please select..";
	    }else{
	    	$result[$index]['name'] = "All";
	    }
    	if(empty($site_id)){
	    	$departments = $this->department_m->get_distinct_name();
	    	foreach ($departments as $row){
	    		$index++;
	    		$result[$index]['id'] = $row->department;
	    		$result[$index]['name'] = $row->department;
	    	}
    	}else{
    		$departments = $this->department_m->get_by_site($site_id);
	    	foreach ($departments as $row){
	    		$index++;
	    		$result[$index]['id'] = $row->department;
	    		$result[$index]['name'] = $row->department;
	    	}
    	}
    	echo json_encode($result);
    }
    
    /**
     * get dept. by site id for add 
     */
    function dept_by_add_site_ajax(){
    	$site_id = $this->input->post('site_id');
    	$select = $this->input->post('select');
    	$result = array();
    	$index = 0;
	    $result[$index]['id'] = "";
	    if($select){
	    	$result[$index]['name'] = "Please select..";
	    }else{
	    	$result[$index]['name'] = "All dept.";
	    }
    	if(!empty($site_id)){
	    	$departments = $this->department_m->get_by_site($site_id);
	    	foreach ($departments as $row){
	    		$index++;
	    		$result[$index]['id'] = $row->id;
	    		$result[$index]['name'] = $row->department;
	    	}
    	}
    	echo json_encode($result);
    }
    
    function close() {
        $this->index();
    }
    
    
    function view_stats($user_id){
    	$this->load->model('St_users_m', 'users_m');
    
    	$user_info = $this->users_m->get_by_id($user_id);
    	$user_name = $user_info->user_name;
    	$data['user'] = $user_info;
    		
    	$now_time = now();
    	$now_day = date(DATE_FORMAT, $now_time);
    		
    	//calculate total annual/personal leave balance
    	$this->load->model('St_working_hours_m', 'working_hours_m');
    	$this->load->model('St_payroll_data_m', 'payroll_data_m');
    	$this->load->model('St_request_m', 'request_m');
    	$this->load->model('st_ac_clocking_log_m', 'ac_clocking_m');
    	$this->load->library('workinghour');
    	$this->load->model('st_config_m', 'config_m');
    
    	$payroll_data = $this->payroll_data_m->get_last_by_user($user_id);
    	$last_annual_leave_end = 0;
    	$last_personal_leave_end = 0;
    	$last_inlieu_end = 0;
    		
    	if($payroll_data){
    		$this->load->model('St_payroll_m', 'payroll_m');

    		$last_payroll = $this->payroll_m->get_last();
    
    		$annual_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_ANNUAL, true, false, $last_payroll->end_date);
    		$annual_upcoming_used = IS_NULL($annual_upcoming_used)?0:$annual_upcoming_used;
    
    		$personal_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, true, false, $last_payroll->end_date);
    		$personal_upcoming_used = IS_NULL($personal_upcoming_used)?0:$personal_upcoming_used;
    		
//    		$inlieu_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_INLIEU, true, false, $last_payroll->end_date);
   		
     		$days_payroll = $this->config_m->get_by_code("Days_Payroll_Parameter")->value;
     		$next_payroll_start_date = date_add('d', $days_payroll, date(DATE_FORMAT, $last_payroll->end_date));
     		$inlieu_upcoming_used = $this->workinghour->calculate_leave_used($user_id, LEAVE_TYPE_INLIEU, $last_payroll->end_date, $next_payroll_start_date);
     		$inlieu_upcoming_used += $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_INLIEU, true, false, $next_payroll_start_date);
     		
     		$unpaid_upcoming = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_UNPAID, true, false, $last_payroll->end_date);

    		$last_annual_leave_end = $payroll_data->annual_leave_end;
    		$last_personal_leave_end = $payroll_data->personal_leave_end;
    		$last_inlieu_end = $payroll_data->inlieu_end;
    	}else{
    		$this->load->model('St_working_hours_m', 'working_hours_m');
    		$total_working_stat = $this->working_hours_m->get_staff_working_hours($user_name);
    
    		$annual_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_ANNUAL, true);
    		$annual_upcoming_used = IS_NULL($annual_upcoming_used)?0:$annual_upcoming_used;
    
    		$personal_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, true);
    		$personal_upcoming_used = IS_NULL($personal_upcoming_used)?0:$personal_upcoming_used;
    		
    		$inlieu_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_INLIEU, true);
    		$inlieu_upcoming_used = IS_NULL($inlieu_upcoming_used)?0:$inlieu_upcoming_used;
    		
    		$unpaid_upcoming = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_UNPAID, true);
    
    		$last_annual_leave_end = get_number($total_working_stat->annual_hours_count);
    		$last_personal_leave_end = get_number($total_working_stat->personal_hours_count);
    	}
    
    	$annual_available = round($last_annual_leave_end, 2);
    	$personal_available = round($last_personal_leave_end, 2);
    
    
    	//calculate the annual leave balance
    	//		$annual_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_ANNUAL);
    	//    	$annual_upcoming_used = $this->request_m->get_upcoming_count_by_leave_type($user_name, LEAVE_TYPE_ANNUAL);
    	//    	$annual_upcoming_used = IS_NULL($annual_upcoming_used)?0:$annual_upcoming_used;
    	$data['annual_upcoming'] = $annual_upcoming_used;
    	$data['annual_available'] = round($annual_available, 2);
    	$data['total_annual'] = round($annual_available - $annual_upcoming_used, 2);
    	
    	$data['total_inlieu'] = round($last_inlieu_end - $inlieu_upcoming_used, 2);
    		
    	//		$personal_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL);
    	//    	$personal_upcoming_used = $this->request_m->get_upcoming_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL);
    	//    	$personal_upcoming_used = IS_NULL($personal_upcoming_used)?0:$personal_upcoming_used;
    	$data['personal_upcoming'] = $personal_upcoming_used;
    	$data['personal_available'] = round($personal_available, 2);
    	$data['total_personal'] = round($personal_available - $personal_upcoming_used, 2);
    		
    	$data['unpaid_upcoming'] = $unpaid_upcoming;
    
    		
    		
    	// Get past request stats.
    	$past_requests = past_request_types();
    	$past_request_days = past_request_days();
    		
    	foreach ($past_requests as $req_id => $req_name){
    		foreach ($past_request_days as $day){
    			$days_key =  $req_name."_".$day."_days";
    			$times_key = $req_name."_".$day."_times";
    
    			if ($day > 0){
    				$day = 0 - $day;
    				$p_stat = $this->request_m->get_stat_by_leave_type($user_name, $req_id, date_add('d', $day, $now_day), $now_time);
    				$d_stat = $this->request_m->get_avg_dept_stat_by_leave_type($user_info->department_id, $req_id, date_add('d', $day, $now_day), $now_time,$user_name);
    				$s_stat = $this->request_m->get_avg_store_stat_by_leave_type($user_info->site_id, $req_id, date_add('d', $day, $now_day), $now_time,$user_name);
    
    				if ($req_name == 'late' || $req_name == 'lunch' || $req_name == 'clock'){
    					$p_stat->timecount = $p_stat->timecount * 60;
    					$d_stat->timecount = $d_stat->timecount * 60;
    					$s_stat->timecount = $s_stat->timecount * 60;
    				}
    
    				$p_days[$days_key] = $p_stat->daycount;
    				$p_times[$times_key] = IS_NULL($p_stat->timecount) ? 0 : round($p_stat->timecount, 2);
    				$d_days[$days_key] = ceil($d_stat->daycount);
    				$d_times[$times_key] = IS_NULL($d_stat->timecount) ? 0 : round($d_stat->timecount, 2);
    				$s_days[$days_key] = ceil($s_stat->daycount);
    				$s_times[$times_key] = IS_NULL($s_stat->timecount) ? 0 : round($s_stat->timecount, 2);
    
    			}/* else{
    				$p_stat = $this->request_m->get_stat_by_leave_type($user_name, $req_id);
    				$d_stat = $this->request_m->get_avg_dept_stat_by_leave_type($user_info->department_id, $req_id,-1,-1,$user_name);
    				$s_stat = $this->request_m->get_avg_store_stat_by_leave_type($user_info->site_id, $req_id,-1,-1,$user_name);
    
    				if ($req_name == 'late' || $req_name == 'lunch' || $req_name == 'clock'){
    					$p_stat->timecount = $p_stat->timecount * 60;
    					$d_stat->timecount = $d_stat->timecount * 60;
    					$s_stat->timecount = $s_stat->timecount * 60;
    				}
    
    				$p_days[$days_key] = $p_stat->daycount;
    				$p_times[$times_key] = IS_NULL($p_stat->timecount) ? 0 : round($p_stat->timecount, 2);
    				$d_days[$days_key] = ceil($d_stat->daycount);
    				$d_times[$times_key] = IS_NULL($d_stat->timecount) ? 0 : round($d_stat->timecount, 2);
    				$s_days[$days_key] = ceil($s_stat->daycount);
    				$s_times[$times_key] = IS_NULL($s_stat->timecount) ? 0 : round($s_stat->timecount, 2);
    			} */
    		}
    
    		// This year
    		$days_key =  $req_name."_thisyear_days";
    		$times_key = $req_name."_thisyear_times";
    
    		$this_year = this_year();
    		$start_time = $this_year[0];
    		$end_time = $this_year[1];
    
    		$p_stat = $this->request_m->get_stat_by_leave_type($user_name, $req_id, $start_time, $end_time);
    		$d_stat = $this->request_m->get_avg_dept_stat_by_leave_type($user_info->department_id, $req_id, $start_time, $end_time,$user_name);
    		$s_stat = $this->request_m->get_avg_store_stat_by_leave_type($user_info->site_id, $req_id, $start_time, $end_time,$user_name);
    
    		if ($req_name == 'late' || $req_name == 'lunch' || $req_name == 'clock'){
    			$p_stat->timecount = $p_stat->timecount * 60;
    			$d_stat->timecount = $d_stat->timecount * 60;
    			$s_stat->timecount = $s_stat->timecount * 60;
    		}
    
    		$p_days[$days_key] = $p_stat->daycount;
    		$p_times[$times_key] = IS_NULL($p_stat->timecount) ? 0 : round($p_stat->timecount, 2);
    		$d_days[$days_key] = ceil($d_stat->daycount);
    		$d_times[$times_key] = IS_NULL($d_stat->timecount) ? 0 : round($d_stat->timecount, 2);
    		$s_days[$days_key] = ceil($s_stat->daycount);
    		$s_times[$times_key] = IS_NULL($s_stat->timecount) ? 0 : round($s_stat->timecount, 2);
    
    		// End this year
    	}
    		
    	$data['p_days'] = $p_days;
    	$data['p_times'] = $p_times;
    	$data['d_days'] = $d_days;
    	$data['d_times'] = $d_times;
    	$data['s_days'] = $s_days;
    	$data['s_times'] = $s_times;
    
    	$data['store'] = $user_info->store_name;
    	$data['department'] = $user_info->department;
    
    		
    	$data['form_id'] = 'past_leave';
    		
    		
    	$compasionate_used = $this->request_m->get_upcoming_count_by_leave_type($user_name, LEAVE_TYPE_COMPASIONATE);
    	$no_mc_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, false, true);
    		
    	$data['comp_used'] = IS_NULL($compasionate_used)?0:round($compasionate_used, 2);
    	$data['no_mc_used'] = IS_NULL($no_mc_used)?0:round($no_mc_used, 2);
    		
    	$max_no_mc = $this->workinghour->calculate_max_no_mc($user_id);
    	$data['max_no_mc'] = $max_no_mc;
    	$data['max_comp_leave'] = $max_no_mc;
    		
    	$data['no_mc_leave_left'] = $max_no_mc - $no_mc_used;
    	$data['comp_leave_left'] = $max_no_mc - $compasionate_used;
    		
    		
    	$data['no_mc_leave_left'] = round($data['no_mc_leave_left'],2);
    	$data['comp_leave_left'] = round($data['comp_leave_left'],2);
    		
    	// Get Leave History
    	$this->load->model('St_leave_type_m', 'leave_type_m');
    	$leave_types = $this->leave_type_m->get_dropdown();
    	$data['leave_types'] = $leave_types;
    		
    	$data['requests'] = $this->request_m->get_approved_requests_by_leave_type(FALSE, $user_name);
    		
    		
    	$this->load->view('request/view_stats', $data);
    		
    }
    
    function export_excel(){
    	
    	$user_level = $this->session->userdata('user_level');
    	
    	$form_id 		= $this->input->post('form_id');
    	$list_name 		= $this->input->post('list_name');
    	$leave_type_id 	= $this->input->post($form_id.'_filter_leave_type_id');
    	$site_id 		= $this->input->post($form_id.'_site_id');
    	$department_id 	= $this->input->post($form_id.'_department_id');
    	$time_period_id = $this->input->post($form_id.'_time_period_id');
    	$from_date 		= $this->input->post($form_id.'_from_date');
    	$to_date 		= $this->input->post($form_id.'_to_date');
    	$first_name 	= $this->input->post($form_id.'_first_name');
    	$last_name 		= $this->input->post($form_id.'_last_name');
    	
    	if (!empty($time_period_id)){
    		$time_period = get_time_period($time_period_id);
    	}else{
    		$time_period[0] = strtotime($from_date);
    		$time_period[1] = strtotime($to_date);
    	}
    	
     	$approvals = $this->request_m->get_approved_approvals_by_leave_type(false, $user_level, $site_id, $department_id, $leave_type_id, 
    																			$time_period[0], $time_period[1], $first_name, $last_name, $order_by='id', 
    																			$order="ASC", $page_no=1, $limit = 0);
    	 
    	//load our new PHPExcel library
    	$this->load->library('PHPExcel');
    	$objPHPExcel = new PHPExcel();
    	 
    	//activate worksheet number 1
    	$objPHPExcel->setActiveSheetIndex(0);
    	//name the worksheet
    	$objPHPExcel->getActiveSheet()->setTitle('Past '.$form_id.' Records');
    	
    	$objPHPExcel->getActiveSheet()->setCellValue('A1', $list_name);
    	$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
    	 
    	
    	$objPHPExcel->getActiveSheet()->setCellValue('A2', "#");
    	$objPHPExcel->getActiveSheet()->freezePane('A2');
    	$objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setSize(16);
    	$objPHPExcel->getActiveSheet()->setCellValue('B2', 'Request ID');
    	$objPHPExcel->getActiveSheet()->freezePane('B2');
    	$objPHPExcel->getActiveSheet()->getStyle('B2')->getFont()->setSize(16);
    	$objPHPExcel->getActiveSheet()->setCellValue('C2', 'Request Date');
    	$objPHPExcel->getActiveSheet()->freezePane('C2');
    	$objPHPExcel->getActiveSheet()->getStyle('C2')->getFont()->setSize(16);
    	$objPHPExcel->getActiveSheet()->setCellValue('D2', 'Applicant');
    	$objPHPExcel->getActiveSheet()->freezePane('D2');
    	$objPHPExcel->getActiveSheet()->getStyle('D2')->getFont()->setSize(16);
    	$objPHPExcel->getActiveSheet()->setCellValue('E2', 'Start Date');
    	$objPHPExcel->getActiveSheet()->freezePane('E2');
    	$objPHPExcel->getActiveSheet()->getStyle('E2')->getFont()->setSize(16);
    	$objPHPExcel->getActiveSheet()->setCellValue('F2', 'End Date');
    	$objPHPExcel->getActiveSheet()->freezePane('F2');
    	$objPHPExcel->getActiveSheet()->getStyle('F2')->getFont()->setSize(16);
    	$objPHPExcel->getActiveSheet()->setCellValue('G2', 'Department');
    	$objPHPExcel->getActiveSheet()->freezePane('G2');
    	$objPHPExcel->getActiveSheet()->getStyle('G2')->getFont()->setSize(16);
    	$objPHPExcel->getActiveSheet()->setCellValue('H2', 'Requested');
    	$objPHPExcel->getActiveSheet()->freezePane('H2');
    	$objPHPExcel->getActiveSheet()->getStyle('H2')->getFont()->setSize(16);
    	$objPHPExcel->getActiveSheet()->setCellValue('I2', 'Reason');
    	$objPHPExcel->getActiveSheet()->freezePane('I2');
    	$objPHPExcel->getActiveSheet()->getStyle('I2')->getFont()->setSize(16);
    	 
    	 
    	$n=3;
    	foreach ($approvals as $row){

   			$objPHPExcel->getActiveSheet()->setCellValue('A'.$n, $n-2);
   			$objPHPExcel->getActiveSheet()->setCellValue('B'.$n, $row->id);
   			$objPHPExcel->getActiveSheet()->setCellValue('C'.$n, date(DATETIME_FORMAT,$row->add_time));
   			$objPHPExcel->getActiveSheet()->setCellValue('D'.$n, $row->first_name." ".$row->last_name);
   			$objPHPExcel->getActiveSheet()->setCellValue('E'.$n, date(DATETIME_FORMAT, $row->start_date));
   			$objPHPExcel->getActiveSheet()->setCellValue('F'.$n, date(DATETIME_FORMAT, $row->end_date));
   			$objPHPExcel->getActiveSheet()->setCellValue('G'.$n, $row->department);
   			$hr = ($row->leave_type_id == LEAVE_TYPE_LUNCH || $row->leave_type_id == LEAVE_TYPE_LATE)? " Minutes" : " Hours";
   			$objPHPExcel->getActiveSheet()->setCellValue('H'.$n, round($row->hours_used,2) . $hr);
   			$objPHPExcel->getActiveSheet()->setCellValue('I'.$n, $row->reason);
    			 
    	
    		$n++;
    	}
    	 
    	 
       	header('Content-Type: application/vnd.ms-excel'); //mime type
    	header('Content-Disposition: attachment;filename="Past_Leave_Request.xls"'); //tell browser what's the file name
    	header('Cache-Control: max-age=0');
    	 
    	//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
    	//if you want to save it as .XLSX Excel 2007 format
    	$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
    	//force user to download the Excel file without writing it to server's HD
    	$objWriter->save('php://output');
    	
    }
    
}
