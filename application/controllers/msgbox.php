<?php

/**
 * login controller
 * @author Bai Yinbing
 */
class Msgbox extends CI_Controller {

	function Msgbox()
	{
		parent::__construct();
	}
	
	function index()
	{
		$this->load->view('msgbox');
	}
}
