<?php

class Config extends Controller {

	function Config()
	{
		parent::Controller();
		if(!$this->userauth->logined()){
			$infoMsg = array(
				'info' => 'Please login !'
			);
			$this->session->set_flashdata('auth', $this->load->view('msgbox/info', $infoMsg , True));
			redirect("login");
		}else{
			if(!$this->userauth->judge_priv("config")){
				$errorMsg = array(
					'error' => 'You have no privilege into this page !'
				);
				$this->session->set_flashdata('auth', $this->load->view('msgbox/error', $errorMsg , True));
				redirect("msgbox", 'location');
			}
		}
		$this->load->model('St_config_m', 'config_m');
	}
	
	function index()
	{
		
	}

	/**
	 * 
	 * @param $id
	 * @return view
	 */
	function leave_hours(){
		$data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('config/leave_hours')."'>Leave Hour Parameters</a>";
		
		$data['configs'] = $this->config_m->get_leave_hour_parameter();
        $this->load->view('navigation', $data);
        $this->load->view('admin/edit_leave_hour_parameters', $data);
	}
	
	/**
	 * 
	 * @return view
	 */
	function email(){
		$data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('config/email')."'>Email Parameters</a>";
		
		$data['configs'] = $this->config_m->get_email_parameter();
        $this->load->view('navigation', $data);
        $this->load->view('admin/edit_email_parameters', $data);
	}
		
	/**
	 * get payroll views
	 * @return view
	 */
	function payroll(){
		$data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('config/payroll')."'>Payroll Parameters</a>";
		
		$data['configs'] = $this->config_m->get_payroll_parameter();
        $this->load->view('navigation', $data);
        $this->load->view('admin/edit_payroll_parameters', $data);
	}
	
    /**
     * deal update a leave type
     * @return view
     */
	function update_leave_hours() {
		$this->form_validation->set_rules('Annual_Leave_Parameter', 'Annual Leave parameter', 'trim|required|numeric');
		$this->form_validation->set_rules('Personal_Leave_Parameter', 'Personal Leave parameter', 'trim|required|numeric');

		if ($this->form_validation->run() == FALSE) {
            $this->leave_hours();
        } else {
			$annual_Leave_Parameter= $this->input->post('Annual_Leave_Parameter');
			$personal_Leave_Parameter 	= $this->input->post('Personal_Leave_Parameter');
			
			$annual_Leave_inifo = array(
				'value' => $annual_Leave_Parameter,
			);
			$b_annual = $this->config_m->update_by_code('Annual_Leave_Parameter', $annual_Leave_inifo);
			
			$personal_Leave_inifo = array(
				'value' => $personal_Leave_Parameter,
			);
			$b_personal = $this->config_m->update_by_code('Personal_Leave_Parameter', $personal_Leave_inifo);
			
			if($b_annual && $b_personal){
				$this->session->set_flashdata('message', $this->load->view('msgbox/info', 
					array(
					'info' => 'Update successfully!'
					), True)
				);
				//redirect("cofing/leave_hours");
			}else{
				$this->session->set_flashdata('message', $this->load->view('msgbox/error', 
					array(
					'error' => 'Update failed!'
					), True)
				);
			}
			redirect("config/leave_hours");
		}
	}
	
	function update_email() {
		$this->form_validation->set_rules('SMTP_HOST', 'SMTP host', 'trim|required');
		$this->form_validation->set_rules('SMTP_PORT', 'SMTP server port', 'trim|required|numeric');
		$this->form_validation->set_rules('SMTP_USER', 'SMTP username', 'trim|required');
		$this->form_validation->set_rules('SMTP_PASS', 'SMTP password', 'trim|required');
		$this->form_validation->set_rules('MAIL_FROM', 'From email address', 'trim|required');
		$this->form_validation->set_rules('MAIL_FROM_NAME', 'From name', 'trim|required');
		if ($this->form_validation->run() == FALSE) {
            $this->leave_hours();
        } else {
			$SMTP_HOST= $this->input->post('SMTP_HOST');
			$SMTP_PORT 	= $this->input->post('SMTP_PORT');
			$SMTP_USER 	= $this->input->post('SMTP_USER');
			$SMTP_PASS 	= $this->input->post('SMTP_PASS');
			$MAIL_FROM 	= $this->input->post('MAIL_FROM');
			$MAIL_FROM_NAME 	= $this->input->post('MAIL_FROM_NAME');
			
			$SMTP_HOST_info = array(
				'value' => $SMTP_HOST,
			);
			$b_SMTP_HOST = $this->config_m->update_by_code('SMTP_HOST', $SMTP_HOST_info);
			
			$SMTP_PORT_info = array(
				'value' => $SMTP_PORT,
			);
			$b_SMTP_PORT = $this->config_m->update_by_code('SMTP_PORT', $SMTP_PORT_info);
			
			$SMTP_USER_info = array(
				'value' => $SMTP_USER,
			);
			$b_SMTP_USER = $this->config_m->update_by_code('SMTP_USER', $SMTP_USER_info);
			
			$SMTP_PASS_info = array(
				'value' => $SMTP_PASS,
			);
			$b_SMTP_PASS = $this->config_m->update_by_code('SMTP_PASS', $SMTP_PASS_info);
			
			$MAIL_FROM_info = array(
				'value' => $MAIL_FROM,
			);
			$b_MAIL_FROM = $this->config_m->update_by_code('MAIL_FROM', $MAIL_FROM_info);
			
			$MAIL_FROM_NAME_info = array(
				'value' => $MAIL_FROM_NAME,
			);
			$b_MAIL_FROM_NAME = $this->config_m->update_by_code('MAIL_FROM_NAME', $MAIL_FROM_NAME_info);
			
			if($b_SMTP_HOST && $b_SMTP_PORT && $b_SMTP_USER && $SMTP_PASS_info){
				$this->session->set_flashdata('message', $this->load->view('msgbox/info', 
					array(
					'info' => 'Update successfully!'
					), True)
				);
				//redirect("cofing/leave_hours");
			}else{
				$this->session->set_flashdata('message', $this->load->view('msgbox/error', 
					array(
					'error' => 'Update failed!'
					), True)
				);
			}
			redirect("config/email");
		}
	}

    /**
     * deal update payroll parameter
     * @return view
     */
	function update_payroll() {
		$this->form_validation->set_rules('Initial_Payroll_Date_Parameter', 'Initial Payroll Period Start date', 'trim|required');
		$this->form_validation->set_rules('Days_Payroll_Parameter', 'Days in payroll', 'trim|required|numeric');

		if ($this->form_validation->run() == FALSE) {
            $this->payroll();
        } else {
			$initial_Payroll_Date_Parameter= $this->input->post('Initial_Payroll_Date_Parameter');
			$days_Payroll_Parameter 	= $this->input->post('Days_Payroll_Parameter');
			
			$initial_Payroll_Date_Parameter = strtotime($initial_Payroll_Date_Parameter);
			$start_date_inifo = array(
				'value' => $initial_Payroll_Date_Parameter,
			);
			$b_start_date = $this->config_m->update_by_code('Initial_Payroll_Date_Parameter', $start_date_inifo);
			
			$days_Payroll_inifo = array(
				'value' => $days_Payroll_Parameter,
			);
			$b_days_payroll = $this->config_m->update_by_code('Days_Payroll_Parameter', $days_Payroll_inifo);
			
			if($b_start_date && $b_days_payroll){
				$this->session->set_flashdata('message', $this->load->view('msgbox/info', 
					array(
					'info' => 'Update successfully!'
					), True)
				);
			}else{
				$this->session->set_flashdata('message', $this->load->view('msgbox/error', 
					array(
					'error' => 'Update failed!'
					), True)
				);
			}
			redirect("config/payroll");
		}
	}
}