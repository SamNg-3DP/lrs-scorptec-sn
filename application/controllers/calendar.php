<?php

class Calendar extends CI_Controller {
	
	function Calendar() {
		parent::__construct();
	}
	
	function index() {
		//read site
    	$this->load->model('St_site_m', 'site_m');

    	$site_id = $this->session->userdata('site_id');
    	$data['user_level'] = $user_level = $this->session->userdata('user_level');
    	
    	// Admin see all users by default
    	if ($user_level >= 3){
    		$data['site_id'] = 0;	
    		$data['department_id'] = '0';
    	}else{
    		$data['site_id'] = $site_id;
//    		$data['department_id'] = $this->session->userdata('department');
    		$data['department_id'] = '0';
    	}
    	
    	$data['leave_id'] = 0;
    	$data['show_shift'] = 0;
    	
        $data['sites'] = $this->site_m->get_dropdown($ex_global=true);


		//load department model
		$this->load->model('St_department_m', 'department_m');
		//read department
	    $data['departments'] = $this->department_m->get_dropdown($site_id);

       	$this->load->model('St_leave_type_m', 'leave_type_m');
        $data['leave_types_id'] = $this->leave_type_m->get_dropdown();
        
        $data['leave_types'] = $this->leave_type_m->get_leave_types();

        
//      $this->output->enable_profiler(TRUE);
		$this->load->view('request/calendar', $data);
	}
	
	function department($site_id, $department_id, $leave_id, $show_shift=0){
		log_message("debug", "site_id...." . $site_id);
		log_message("debug", "department...." . $department_id);
		
		$department_id = preg_replace("/%20/", " ", $department_id);
		
		$data['user_level'] = $this->session->userdata('user_level');
		$data['site_id'] = $site_id;

		$data['department_id'] = $department_id;
		$data['leave_id'] = $leave_id;
		$data['show_shift'] = $show_shift;
		
		$this->load->model('St_site_m', 'site_m');
        $data['sites'] = $this->site_m->get_dropdown($ex_global=true);
		
		
		$this->load->model('St_department_m', 'department_m');
        $data['departments'] = $this->department_m->get_dropdown($site_id);
        
        $this->load->model('St_leave_type_m', 'leave_type_m');
        $data['leave_types_id'] = $this->leave_type_m->get_dropdown($leave_id);
        
        $data['leave_types'] = $this->leave_type_m->get_leave_types();
        
		$this->load->view('request/calendar', $data);
	}

	/**
	 * get all events for employment
	 * @param $department_id
	 * @return json
	 */
	function events($site_id=0,$department_id,$leave_id, $show_shift=0){
		log_message("debug", "events site_id...." . $site_id);
		log_message("debug", "events department...." . $department_id);
		
		$department_id = preg_replace("/%20/", " ", $department_id);
		
		$user_level = $this->session->userdata('user_level');
		$user_name = $this->session->userdata('user_name');
		$user_department = $this->session->userdata('department');
		
		
		
		//load request model
		$this->load->model('St_request_m', 'request_m');
		$this->load->model('St_publicholiday_m', 'publicholiday_m');
		
		// Don't include casual and overtime leaves.
		if (!$show_shift && $leave_id == 0){
			$leave_id = array(LEAVE_TYPE_ANNUAL,LEAVE_TYPE_UNPAID,LEAVE_TYPE_PERSONAL,LEAVE_TYPE_TRAINING,LEAVE_TYPE_LATE,LEAVE_TYPE_LUNCH,LEAVE_TYPE_PERSONAL_UNPAID,
								LEAVE_TYPE_CLIENTVISIT,LEAVE_TYPE_COMPASIONATE,LEAVE_TYPE_CLOCK,LEAVE_TYPE_INTERSTORE,LEAVE_TYPE_ONSITE,LEAVE_TYPE_INLIEU,LEAVE_TYPE_PARENTAL,LEAVE_TYPE_PERSONAL_UNPAID,LEAVE_TYPE_CASUAL_UNPAID);
		}

		$requests = $this->request_m->get_requests_by_calendar($site_id, $department_id, $leave_id, $this->input->get_post('start'), $this->input->get_post('end'), array(10,20,30,40));
		
		
		// Get onsite leave.
		if ($leave_id == LEAVE_TYPE_CLIENTVISIT){
			$request_onsite = $this->request_m->get_requests_by_calendar($site_id, $department_id, 14, $this->input->get_post('start'), $this->input->get_post('end'));
			$request_onsite_af = $this->request_m->get_requests_by_calendar($site_id, $department_id, 15, $this->input->get_post('start'), $this->input->get_post('end'));
				
			$requests = array_merge($requests, $request_onsite);
			$requests = array_merge($requests, $request_onsite_af);
		}		


		$list = array();
		foreach ($requests as $row) {
			$event = array();
			$event['id'] = $row->id;
			

			// Limit viewing requests
			if ($user_level >= 3){
				$event['url'] = site_url("request/view/".$row->id);
			}elseif ($user_level == 2 && $row->department == $user_department){
				$event['url'] = site_url("request/view/".$row->id);
			}elseif ($user_level == 1 && $row->user_name == $user_name){
				$event['url'] = site_url("request/view/".$row->id);
			}else{
				$event['url'] = false;
			}
			

			if ($row->state == 40)
				$event['className'] = "cal_leave_" . $row->leave_type_id;
			else
				$event['className'] = "cal_approve_" . $row->leave_type_id;
			if($row->request_type == 1){
				$event['title'] = $row->first_name . " " . $row->last_name;// ."[". $row->leave_type . "]";
				$event['start'] = date("Y-m-d", $row->start_day);
				$event['end'] = date("Y-m-d", $row->end_day);
			}else if($row->request_type == 2){
				$event['start'] = date("Y-m-d H:i", $row->start_date);
				$event['end'] = date("Y-m-d H:i", $row->end_date);
//				$event['title'] = date("H:i", $row->start_date). "-" . date("H:i", $row->end_date) . " " .  $row->first_name . " " . $row->last_name;// ."[". $row->leave_type . "]";

				$event['title'] = $row->start_time;
				if ($row->end_time)
					$event['title'] .= 	"-" . $row->end_time;
				$event['title'] .= 	" " .  $row->first_name . " " . $row->last_name;// ."[". $row->leave_type . "]";
					
				//$event['allDay'] = false;
			}
			$description = $row->first_name . " " . $row->last_name .
					"<br>Type: " . $row->leave_type . 
					"<br>Dept: " . $row->department . 
					"<br>Start: " . date("Y-m-d H:i", $row->start_date) .
					"<br>End: " . date("Y-m-d H:i", $row->end_date);
			if($row->leave_type_id == LEAVE_TYPE_LATE 
				|| $row->leave_type_id == LEAVE_TYPE_LUNCH){
				$description .= "<br>Minutes used: " . $row->hours_used * 60;
			}else{
				$description .= "<br>Hours used: " . $row->hours_used;
			}					
			$description .= "<br>Requested Date: ". date("Y-m-d H:i", $row->add_time);
			
			if ($row->manager_time){
				$description .= "<br>Approved Date: " . date("Y-m-d H:i", $row->manager_time) .
								"<br>Approved By: " . $row->mfirst_name . " " . $row->mlast_name;
			}elseif ($row->supervisor_time){
				$description .= "<br>Approved Date: " . date("Y-m-d H:i", $row->supervisor_time) .
								"<br>Approved By: " . $row->sfirst_name . " " . $row->slast_name;
			}
			
			// Get client visit information
			$this->load->model('St_client_visit_m', 'client_visit_m');
			
			$job = $this->client_visit_m->get_by_id($row->id,'request');
				
 			if ($job){
				$data['order_url'] 	= SCORPY_HOME."/orders_forms.php?type=form&ordid=".$job->ordid;
				$data['order_no']	= $job->ordno;
				
				$description .= "<br>Order No:" . $job->ordno . 
								"<br>QB Name: " . $job->client_qb_name;
			}
			
			
			$event['description'] = $description;
			$list[] = $event;
		}
		
		// Show shifts.
		if ($show_shift){
			$slot = false;
			
			$this->load->model('St_users_m', 'users_m');
			$this->load->model('St_daily_shift_slot_m', 'daily_shift_slot_m');
			// Get all active users.
			
			$users = $this->users_m->get_active_users($this->input->get_post('start'), $this->input->get_post('end'), $site_id, $department_id);
			
			log_message("debug", "Calendar Start: " . $this->input->get_post('start'));
			log_message("debug", "Calendar End: " . $this->input->get_post('end'));
//			$slot = $this->daily_shift_slot_m->get_calendar_by_user('11', '1360132563');
			
 			if ($slot){
				log_message("debug", "Slot Start: " . $slot->start_time);
				log_message("debug", "Slot End: " . $slot->end_time);
			}
			
			
			
			
			foreach ($users as $user){
				$date = $this->input->get_post('start');
				$date += 3600;  // Prevent glitch when daylight saving end.
				
				$start_date = false;
				$set_start = false;
				
				// Get start day and end day for each week.				
				while ($date < $this->input->get_post('end')){
						
					// Compare date with user start and terminate date.
					if ($user->hire_date <= $date && ($user->terminate_date >= $date || $user->terminate_date == 0 )){
					
						$slot = $this->daily_shift_slot_m->get_calendar_by_user($user->id, $date);
	
						if ($slot){
							$next_slot = $this->daily_shift_slot_m->get_calendar_by_user($user->id, $date + 86400);
							
							if ($set_start){
								$start_date = $date;
							}
							
							if (!$next_slot || $next_slot->start_time != $slot->start_time || $next_slot->end_time != $slot->end_time){
								$event['className'] = "cal_shift";
								$event['url'] = false;
								$event['title'] = $slot->start_time;
								$event['title'] .= 	"-" . $slot->end_time;
								$event['title'] .= 	" " .  $user->first_name . " " . $user->last_name;// ."[". $row->leave_type . "]";
								
								
								$event['start'] = date("Y-m-d", $start_date);
								$event['end'] = date("Y-m-d", $date);
								
								$event['description'] = "Daily Shift";
								
								$list[] = $event;
								
								$set_start = true;
							}else{
								$set_start = false;
							}
						}else{
							$set_start = true;
						}
					}
					
					$date += 86400;
				}
			}
				
			
		}
		
		
		
		// Get Public holiday
		$holidays = $this->publicholiday_m->get_publicholidays(false, $this->input->get_post('start'), $this->input->get_post('end'));
		
		foreach ($holidays as $holiday){
			$event = array();
			$event['className'] = "cal_public_holiday";

			$event['title'] = $holiday->holiday_name;
			$event['start'] = date("Y-m-d", $holiday->date);
			$event['end'] = date("Y-m-d", $holiday->date);
			$event['description'] = $holiday->holiday_name;
			
			$list[] = $event;
		}
		
		
		echo json_encode($list);
	}
}