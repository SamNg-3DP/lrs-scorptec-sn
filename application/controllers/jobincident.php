<?php

class JobIncident extends CI_Controller {

    function JobIncident() {
        parent::__construct();
    	if(!$this->userauth->logined()){
			$infoMsg = array(
				'info' => 'Please login !'
			);
			$this->session->set_flashdata('auth', $this->load->view('msgbox/info', $infoMsg , True));
			$this->session->set_flashdata('back_url', current_url());
			redirect("login");
		}else{
			if(!$this->userauth->judge_priv("jobincident")){
				$errorMsg = array(
					'error' => 'You have no privilege into this page !'
				);
				$this->session->set_flashdata('auth', $this->load->view('msgbox/error', $errorMsg , True));
				redirect("msgbox", 'location');
			}
		}
		
		$this->form_validation->set_error_delimiters('<p class="error">', '</p>');
		
		//load request model
		$this->load->model('St_job_incident_m', 'job_incident_m');
		$this->load->model('St_job_incident_file_m', 'job_incident_file_m');
		$this->load->model('st_department_m', 'department_m');
    }
	
    function index() {
    	$user_id = $this->session->userdata('user_id');
    	$data['navigation'] = "<a href='". site_url('admin/jobincident') ."' >Job Incident</a> > List";
        
        $data['new_jobs'] = $this->job_incident_m->get_new_by_user($user_id);
        $data['progress_jobs'] = $this->job_incident_m->get_progress_by_user($user_id);
        $data['completed_jobs'] = $this->job_incident_m->get_completed_by_user($user_id);
        
        $new_count = $this->job_incident_m->count_new_by_user($user_id);
        $progress_count = $this->job_incident_m->count_progress_by_user($user_id);
        $completed_count = $this->job_incident_m->count_completed_by_user($user_id);
        
        $data['new_count'] = $new_count;
        $data['progress_count'] = $progress_count;
        $data['completed_count'] = $completed_count;
        
        $new_page_count = 0;
    	if(($new_count % PAGE_SIZE) == 0){
    		$new_page_count = $new_count / PAGE_SIZE;
    	}else{
    		$new_page_count = floor($new_count / PAGE_SIZE) + 1;
    	}
        
    	$data['new_page_no'] = 1;
    	$data['new_page_count'] = $new_page_count;
    	
    	$progress_page_count = 0;
    	if(($progress_count % PAGE_SIZE) == 0){
    		$progress_page_count = $progress_count / PAGE_SIZE;
    	}else{
    		$progress_page_count = floor($progress_count / PAGE_SIZE) + 1;
    	}
        
    	$data['progress_page_no'] = 1;
    	$data['progress_page_count'] = $progress_page_count;
    	
    	$completed_page_count = 0;
    	if(($completed_count % PAGE_SIZE) == 0){
    		$completed_page_count = $completed_count / PAGE_SIZE;
    	}else{
    		$completed_page_count = floor($new_count / PAGE_SIZE) + 1;
    	}
        
    	$data['completed_page_no'] = 1;
    	$data['completed_page_count'] = $completed_page_count;
    	
        $this->load->view('navigation', $data);
        $this->load->view('jobincident/list_job_incident');
    }
    
    function list_jobs(){
    	$this->index();
    }
    
    function new_ajax(){
    	$user_id = $this->session->userdata('user_id');
    	$data['form_id'] = 'new';
    	
    	
    	$site_id = $this->input->post('new_filter_site_id');
    	$department_id = $this->input->post('new_filter_department_id');
    	$time_period_id = $this->input->post('new_filter_time_period_id');
    	$from_date = $this->input->post('new_filter_from_date');
    	$to_date = $this->input->post('new_filter_to_date');
    	$first_name = $this->input->post('new_filter_first_name');
    	$last_name = $this->input->post('new_filter_last_name');
    	 
        
    	$page_no = $this->input->post('new_page_no');
        
		$limit = $this->input->post('new_limit');
    	$data['page_limit'] = $limit;
    	
//     	$data['jobs'] = $this->job_incident_m->get_new_by_user($user_id,$page_no, $limit);
// 		$total_count = $this->job_incident_m->count_new_by_user($user_id);
		
		$data['jobs'] = $this->job_incident_m->get_approvals_by_new($user_level, $site_id, $department_id, $time_period[0], $time_period[1], $first_name, $last_name, $page_no, $limit);
		$count = $this->job_incident_m->count_approvals_by_new($user_level, $site_id, $department_id, $time_period[0], $time_period[1], $first_name, $last_name);
		$data['count'] = $count;
		
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
		
		$content = $this->load->view('jobincident/list_job_incident_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }
    
    function progress_ajax(){
    	$user_id = $this->session->userdata('user_id');
    	$data['form_id'] = 'progress';
    	
    	
    	$site_id = $this->input->post('progress_filter_site_id');
    	$department_id = $this->input->post('progress_filter_department_id');
    	$time_period_id = $this->input->post('progress_filter_time_period_id');
    	$from_date = $this->input->post('progress_filter_from_date');
    	$to_date = $this->input->post('progress_filter_to_date');
    	$first_name = $this->input->post('progress_filter_first_name');
    	$last_name = $this->input->post('progress_filter_last_name');
    	
        
    	$page_no = $this->input->post('progress_page_no');
        
		$limit = $this->input->post('progress_limit');
    	$data['page_limit'] = $limit;
    	
//     	$data['jobs'] = $this->job_incident_m->get_progress_by_user($user_id,$page_no, $limit);
// 		$total_count = $this->job_incident_m->count_progress_by_user($user_id);
		
		$data['jobs'] = $this->job_incident_m->get_approvals_by_progress($user_level, $site_id, $department_id, $time_period[0], $time_period[1], $first_name, $last_name, $page_no, $limit);
		$count = $this->job_incident_m->count_approvals_by_progress($user_level, $site_id, $department_id, $time_period[0], $time_period[1], $first_name, $last_name);
		$data['count'] = $count;
		
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
		
		$content = $this->load->view('jobincident/list_job_incident_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }
    
    function completed_ajax(){
    	$user_id = $this->session->userdata('user_id');
    	$user_level = $this->session->userdata('user_level');
    	
    	$site_id = $this->input->post('completed_filter_site_id');
    	$department_id = $this->input->post('completed_filter_department_id');
    	$time_period_id = $this->input->post('completed_filter_time_period_id');
    	$from_date = $this->input->post('completed_filter_from_date');
    	$to_date = $this->input->post('completed_filter_to_date');
    	$first_name = $this->input->post('completed_filter_first_name');
    	$last_name = $this->input->post('completed_filter_last_name');
    	 
    	
    	if (!empty($time_period_id)){
    		$time_period = get_time_period($time_period_id);
    	}else{
    		$time_period[0] = strtotime($from_date);
    		$time_period[1] = strtotime($to_date);
    	}
    	 

    	if($user_level > 2 && !$department_id &&!$site_id){
    		$site_id = 0;
    		$department_id = 0;
    	}
    	
    	$data['departments'] = $this->department_m->get_dropdown($site_id);

    	
    	$data['status'] = 'completed';
    	$data['user_level'] = $user_level;
        
    	$page_no = $this->input->post('completed_page_no');
        
		$limit = $this->input->post('completed_limit');
    	$data['page_limit'] = $limit;
    	
    	$data['jobs'] = $this->job_incident_m->get_approvals_by_completed($user_level, $site_id, $department_id, $time_period[0], $time_period[1], $first_name, $last_name, $page_no, $limit);
    	$count = $this->job_incident_m->count_approvals_by_completed($user_level, $site_id, $department_id, $time_period[0], $time_period[1], $first_name, $last_name);
    	$data['count'] = $count;
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($count % $limit) == 0){
	    		$page_count = $count / $limit;
	    	}else{
	    		$page_count = floor($count / $limit) + 1;
	    	}
    	}
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
		
		$content = $this->load->view('jobincident/list_job_incident_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }
    /**
     * prepare to add a new leave request
     * @return 
     */
    function add() {
        $data['navigation'] = "<a href='". site_url('admin/jobincident') ."' >Job Incident</a> > Add a new job incident";
        $this->load->model('St_department_m', 'department_m');
        $this->load->model('St_users_m', 'users_m');
        $dept = $this->department_m->get($this->session->userdata('department_id'));
        
        $data['department'] = $dept->department;
        
        $users = $this->users_m->get_select_dropdown(false,false,true);
        $data['users'] = $users;
        
        $this->load->view('navigation', $data);
        $this->load->view('jobincident/add_job_incident', $data);
    }
    
    
    /**
     * deal with add new a job incident
     * @return void
     */
	function do_add() {
		$job_id = $this->input->post('job_id');
        $this->form_validation->set_rules('incident_date', 'Date of incident', 'trim|required');
        $this->form_validation->set_rules('staff_id', 'Staff involved', 'trim|required');
    	$this->form_validation->set_rules('customer', 'Customer(s) involved', 'trim|required');
        $this->form_validation->set_rules('reference_no', 'Reference no - Invoice no', 'trim|required');
        $this->form_validation->set_rules('detail', 'Detail of the inicident', 'trim|required');
        $this->form_validation->set_rules('how_occur', 'How did the incident occur', 'trim|required');
        $this->form_validation->set_rules('how_prevent', 'What should have been done to prevent it from happening next time', 'trim|required');
        $this->form_validation->set_rules('how_repair', 'What action should be taken now to repair or recover from the incident', 'trim|required');
        $this->form_validation->set_rules('what_cost', 'What is the approximate time and cost involved as a result of this incident', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $this->add();
        } else {
        	$now_time = now();
        	$user_id = $this->session->userdata('user_id');
        	$user_name = $this->session->userdata('user_name');

        	$staff_id = $this->input->post('staff_id');
        	$operate = $this->input->post('operate');
        	$incident_date = $this->input->post('incident_date');
        	$incident_date = strtotime($incident_date);
        	
        	$customer = $this->input->post('customer');
        	$reference_no = $this->input->post('reference_no');
        	$detail = $this->input->post('detail');
        	$how_occur = $this->input->post('how_occur');
        	$how_prevent = $this->input->post('how_prevent');
        	$how_repair = $this->input->post('how_repair');
        	$what_cost = $this->input->post('what_cost');
        	$cost_value = $this->input->post('cost_value');
        	$emails = $this->input->post('emails');
        	
        	$state = 0;
        	if($operate == "save"){
        		$state = 1;
        	}else if($operate == "apply"){
        		$state = 2;
        	}
        	
        	$jobincident = array(
        		'user_id' => $user_id,
        		'staff_id' => $staff_id,
        		'incident_date' => $incident_date,
        		'customer' => $customer,
        		'reference_no' => $reference_no,
        		'detail' => $detail,
        		'how_occur' => $how_occur,
        		'how_prevent' => $how_prevent,
        		'how_repair' => $how_repair,
        		'what_cost' => $what_cost,
        		'cost_value' => $cost_value,
        		'state' => $state,
        		'incident_date' => $incident_date,
        		'emails' => $emails,
        		'status' => ACTIVE,
        		'add_time' => $now_time
        	);
        	if ($job_id){
        		$this->job_incident_m->update($job_id, $jobincident);
        		$insert_id = $job_id;
        	}else{
        		$insert_id = $this->job_incident_m->insert($jobincident);
        	}
        	
        	$this->load->model('St_users_m', 'users_m');
        	$job_info = $this->job_incident_m->get_info($insert_id);
        	
        	$staff = $this->users_m->get_by_id($staff_id);
        	$user_name = $staff->user_name;
        	
        	
        	$this->load->model('St_job_incident_log_m', 'job_incident_log_m');
        	if($state == 1){
        		$jobincident_log = array(
        			'id' => $insert_id,	
        			'operator_id' => $user_id,
        			'operation' => 'Save',
        			'operate_time' => $now_time,
        			'operate_ip' => $this->input->ip_address()
        		);
        		$jobincident_log = array_merge($jobincident, $jobincident_log);
        		$this->job_incident_log_m->insert($jobincident_log);
        	}else if($state == 2){
        		$jobincident_log = array(
        			'id' => $insert_id,
        			'operator_id' => $user_id,
        			'operation' => 'Apply',
        			'operate_time' => $now_time,
        			'operate_ip' => $this->input->ip_address()
        		);
        		$jobincident_log = array_merge($jobincident, $jobincident_log);
        		
        		$this->job_incident_log_m->insert($jobincident_log);
        		log_message("debug", "mail ...... 1");
        		$this->load->library('phpmail');
        		$this->phpmail->readConfig();
        		$user_level = $this->session->userdata("user_level");
        		
    			//need to send an email notification to supervisor
    			//read supervisor of current department
    			$hasSuper = false;
    			$supervisors = $this->users_m->get_supervisors_by_user_name($user_name);
    			if(!empty($supervisors)){
        			foreach($supervisors as $supervisor){
        				if(!empty($supervisor)){
        					if(!empty($supervisor->email)){
        						$this->phpmail->AddAddress($supervisor->email);
        						$hasSuper = true;
        					}
        				}
        			}
    			}
				
				if($hasSuper){
					$this->phpmail->setSubject("Job Incident id: " . $insert_id . " - " . $this->session->userdata('first_name') . " " . $this->session->userdata('last_name'));
        			$mail_body['appove_url'] = site_url('jobincident/deal/'.$insert_id);
	            	$mail_body['appove_name'] = "approve this job incident here.";
	            	$mail_body['jobincident'] = $job_info;
	            	
	    			$this->phpmail->setBody($this->load->view('mail/incident', $mail_body , True));
	    			if(!$this->phpmail->send()){
			        	log_message("debug", "send mail .... " . $this->phpmail->ErrorInfo);
			        }
			        $this->phpmail->ClearAllAddresses();
				}
				
        		//send mail to all manager and admin
        		$hasManager = false;
        		$managers = $this->users_m->get_managers_by_user_name($user_name);
    			if(!empty($managers)){
        			foreach($managers as $manager){
        				if(!empty($manager)){
        					if(!empty($manager->email)){
        						$this->phpmail->AddAddress($manager->email);
        						$hasManager = true;
        					}
        				}
        			}
    			}
    			
    			$operations = $this->users_m->get_operation_by_user_name($user_name);
    			if(!empty($operations)){
    				foreach($operations as $operation){
    					if(!empty($operation)){
    						if(!empty($operation->email) && $operation->user_name != $requestinfo->user_name ){
    							$this->phpmail->AddAddress($operation->email);
    							$hasManager = true;
    						}
    					}
    				}
    			}
    			
    			if (!empty($job_info->emails)){
    				$emails = explode(";",$job_info->emails);
    				foreach ($emails as $em)
    					$this->phpmail->AddAddress($em);
    			}
    			 
    			 
    			if($hasManager){
    				$this->phpmail->setSubject("Job Incident id: " . $insert_id . " - " . $this->session->userdata('first_name') . " " . $this->session->userdata('last_name'));
    				$mail_body['appove_url'] = site_url('jobincident/deal/'.$insert_id);
            		$mail_body['appove_name'] = "review this job incident here.";
            		$mail_body['jobincident'] = $job_info;
    				$this->phpmail->setBody($this->load->view('mail/incident', $mail_body , True));
	    			if(!$this->phpmail->send()){
			        	log_message("debug", "send mail .... " . $this->phpmail->ErrorInfo);
			        }
			        $this->phpmail->ClearAllAddresses();
    			}
    			
    			
    			$hasSelf = false;
        		$email = $this->session->userdata("email");
        		if(!empty($email)){
        			$this->phpmail->AddAddress($email);
        			log_message("debug", "email .... " . $email);
        			$hasSelf = true;
    			}

    			if($hasSelf){
    				$this->phpmail->setSubject("Job Incident id: " . $insert_id . " - " . $this->session->userdata('first_name') . " " . $this->session->userdata('last_name'));
	        		$mail_body['appove_url'] = site_url('jobincident/view/'.$insert_id);
            		$mail_body['appove_name'] = "view this job incident here.";
            		$mail_body['jobincident'] = $job_info;
    				$this->phpmail->setBody($this->load->view('mail/incident', $mail_body , True));
	    			if(!$this->phpmail->send()){
			        	log_message("debug", "send mail .... " . $insert_id . $this->phpmail->ErrorInfo);
			        }
			        $this->phpmail->ClearAllAddresses();
    			}
        	}
        	
        	
        	$upload_filename = "";
        	$fileElementName = 'upload_file';
        	$error = "";
         	if(!empty($_FILES[$fileElementName]['tmp_name']) && $_FILES[$fileElementName]['tmp_name'] != 'none'){

        		if(!empty($_FILES[$fileElementName]['error'])){
        			switch($_FILES[$fileElementName]['error']){
        				case '1':
        					$error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
        					break;
        				case '2':
        					$error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
        					break;
        				case '3':
        					$error = 'The uploaded file was only partially uploaded';
        					break;
        				case '4':
        					$error = 'No file was uploaded.';
        					break;
        					 
        				case '6':
        					$error = 'Missing a temporary folder';
        					break;
        				case '7':
        					$error = 'Failed to write file to disk';
        					break;
        				case '8':
        					$error = 'File upload stopped by extension';
        					break;
        				case '999':
        				default:
        					$error = 'No error code avaiable';
        			}
        		}else{
        			$tempFile = $_FILES[$fileElementName]['tmp_name'];
        			 
        			$now_dir = date('Y/m/d');
        			$targetPath = str_replace('//','/',DIR_JOB_INCIDENT_FILE . $now_dir . '/');
        			 
        			if(mkdirs($targetPath)){
        				$filename = $_FILES[$fileElementName]['name'];
        				 
        				// Get the extension from the filename.
        				$extension = substr($filename, strpos($filename,'.'), strlen($filename)-1);
        				 
        				$target_filename = random_filename($targetPath, $extension, 6);
        				$upload_filename = $now_dir . '/' . $target_filename;
        				 
        				$targetFile = $targetPath . $target_filename;
        				 
        				if (move_uploaded_file($tempFile,$targetFile)){
        					$jobincident_file = array(
        							'job_id' 	=> $insert_id,
        							'file' 		=> $upload_filename,
        							'add_time' 	=> $now_time,
        					);
        					 
        					$this->job_incident_file_m->insert($jobincident_file);
        				}
        				 
        			}else{
        				$error = "Can't create directory";
        			}
        		}
        	}
        	 
        	if($error != ""){
        		echo ' {"message":"'.$error.'", "status":"err"}';
        	}        	
        	
        	
        	
        	$this->incident_list('new');
        }
	}
    
    function incident_list($status) {
        $user_level = $this->session->userdata('user_level');
        $site_id = $this->session->userdata('site_id');
        $department_id = $this->session->userdata('department');

		//load site model
		$this->load->model('St_site_m', 'site_m');
		//load department model
		$this->load->model('St_department_m', 'department_m');
		
   		//read site
        $data['sites'] = $this->site_m->get_dropdown();
        
        //read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
        
        if($user_level > 2){
        	$site_id = 0;
        	$department_id = 0;
        }
        $data['site_id'] = $site_id;
        $data['department_id'] = $department_id;
        
        $count = 0;
        switch ($status){
        	case 'new':
        		$data['jobs'] = $this->job_incident_m->get_approvals_by_new($user_level, $site_id, $department_id);
        		$data['navigation'] = "<a href='". site_url('admin/jobincident') ."' target='_top'>Job Incident</a> > New";
        		$count = $this->job_incident_m->count_approvals_by_new($user_level, $site_id, $department_id); 
        		break;
        	case 'progress':
				$data['jobs'] = $this->job_incident_m->get_approvals_by_progress($user_level, $site_id, $department_id);
				$data['navigation'] = "<a href='". site_url('admin/jobincident') ."' target='_top'>Job Incident</a> > In Progress";
				$count = $this->job_incident_m->count_approvals_by_progress($user_level, $site_id, $department_id);
        		break;
        	case 'completed':
        		$data['jobs'] = $this->job_incident_m->get_approvals_by_completed($user_level, $site_id, $department_id);
        		$data['navigation'] = "<a href='". site_url('admin/jobincident') ."' target='_top'>Job Incident</a> > Completed";
        		$count = $this->job_incident_m->count_approvals_by_completed($user_level, $site_id, $department_id);
        		break;
        }
        
        $data['count'] = $count;
        $data['status'] = $status;
        $data['user_level'] = $user_level;
        
        $page_count = 0;
    	if(($count % PAGE_SIZE) == 0){
    		$page_count = $count / PAGE_SIZE;
    	}else{
    		$page_count = floor($count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	
    	
        $this->load->view('navigation', $data);
        $this->load->view('jobincident/list_job_incident');
    }
    
    /**
     * approve job incident
     */
    function deal($id) {
    	$data['navigation'] = "<a href='". site_url('admin/jobincident') ."' target='_top'>Job Incident</a> > Incident Approval";
        
        $job = $this->job_incident_m->get_info($id);
      	if(!$job){
        	show_404();
        }
        
        $order_master = $this->job_incident_m->get_scorpy_order_master($job->reference_no, $job->customer);
        if ($order_master)
        	$job->order_url = SCORPY_HOME."/orders_forms.php?type=form&ordid=".$order_master->ordid;
        else
        	$job->order_url = false;
        
        $data['job'] = $job;
        
        
        // Get attached files
        $data['attached_files'] = $this->job_incident_file_m->get_file($id);
        
        
        $this->load->view('navigation', $data);
        $this->load->view('jobincident/deal_job_incident');
    }
    
    /**
     * process approve job incident
     */
    function do_deal() {
    	$now_time = now();
    	$user_id = $this->session->userdata('user_id');
    	$user_level = $this->session->userdata('user_level');
    	$job_id = $this->input->post('job_id');
    	
    	if($user_level >= 2){
    		$supervisor_comment = $this->input->post('supervisor_comment');
    		$manager_comment = $this->input->post('manager_comment');	
    		$take_action = $this->input->post('take_action');
    		$resolved = $this->input->post('resolved');
    		$approved = $this->input->post('approved');
    		$emails = $this->input->post('emails');
    		
    		$jobincident = array(
	    		'supervisor_comment' => $supervisor_comment,
    			'manager_comment' => $manager_comment,
	    		'take_action' => $take_action,
    			'emails' => $emails,
	    		'resolved' => $resolved,
	    		'approved' => $approved,
	    		'state' => 3
	    	);
    		
    		if ($user_level == 2)
    			$jobincident['supervisor_user_name'] = $this->session->userdata('user_name');
    		else
    			$jobincident['manager_user_name'] = $this->session->userdata('user_name');
	    	
    		$this->job_incident_m->update($job_id, $jobincident);
    		
    		if($approved == 1){
    			$approve_str = "Approved ";
    		}else if($approved == 2){
    			$approve_str = "Not Approved ";
    		}
    		
    		
    		$this->load->model('St_job_incident_log_m', 'job_incident_log_m');
    		$jobincident_log = array(
    				'id' => $job_id,
    				'operator_id' => $user_id,
    				'operation' => $approve_str,
    				'operate_time' => $now_time,
    				'operate_ip' => $this->input->ip_address()
    		);
    		$jobincident_log = array_merge($jobincident, $jobincident_log);
    		$this->job_incident_log_m->insert($jobincident_log);
    		
    		
    		$this->load->model('St_users_m', 'users_m');
    		$job_info = $this->job_incident_m->get_info($job_id);
    		$this->load->library('phpmail');
        	$this->phpmail->readConfig();
        	$hasSuper = false;
    		$supervisors = $this->users_m->get_supervisors_by_user_name($job_info->staff_username);
    		if(!empty($supervisors)){
    			foreach($supervisors as $supervisor){
    				if(!empty($supervisor)){
    					if(!empty($supervisor->email)){
    						$this->phpmail->AddAddress($supervisor->email);
    						$hasSuper = true;
    					}
    				}
    			}
    		}
        	
			//send mail to all manager and admin
        	$managers = $this->users_m->get_managers_by_user_name($job_info->staff_username);
        	if(!empty($managers)){
        		foreach($managers as $manager){
        			if(!empty($manager)){
        				if(!empty($manager->email)){
        					$this->phpmail->AddAddress($manager->email);
        				}
        			}
        		}
        	}
        	
        	$operations = $this->users_m->get_operation_by_user_name($job_info->staff_username);
        	if(!empty($operations)){
        		foreach($operations as $operation){
        			if(!empty($operation)){
        				if(!empty($operation->email) && $operation->user_name != $requestinfo->user_name ){
        					$this->phpmail->AddAddress($operation->email);
        					$hasManager = true;
        				}
        			}
        		}
        	}
        	 
        	if(!empty($job_info->email)){
        		$this->phpmail->AddAddress($job_info->email);
        	}
        	
        	if(!empty($job_info->staff_email)){
        		$this->phpmail->AddAddress($job_info->staff_email);
        	}

			if (!empty($job_info->emails)){
        		$emails = explode(";",$job_info->emails);
        		foreach ($emails as $em)
        			$this->phpmail->AddAddress($em);
        	}
        	
//        	$this->phpmail->AddAddress("edwards@scorptec.com.au");
        	
        	
        	$this->phpmail->setSubject($approve_str . "Job Incident id: " . $job_id . " - " . $job_info->staff_firstname . " " . $job_info->staff_lastname);
			$mail_body['appove_url'] = site_url('jobincident/view/'.$job_id);
        	$mail_body['appove_name'] = "view this job incident here.";
        	$mail_body['jobincident'] = $job_info;
        	$mail_body['first_name'] = $this->session->userdata('first_name');
        	$mail_body['last_name'] = $this->session->userdata('last_name');
        	
			$this->phpmail->setBody($this->load->view('mail/incident2', $mail_body , True));
			if(!$this->phpmail->send()){
	        	log_message("debug", "send mail .... " . $this->phpmail->ErrorInfo);
	        }
	        $this->phpmail->ClearAllAddresses();
    		
    	}else if($user_level == 3){
    		$manager_comment = $this->input->post('manager_comment');	
    		$jobincident = array(
	    		'manager_comment' => $manager_comment
	    	);
	    	$this->job_incident_m->update($job_id, $jobincident);
	    	
	    	
    	}
    	
    	$this->incident_list('progress');
    }
    
    /**
     * approve job incident
     */
    function edit($id) {
    	$this->load->model('St_users_m', 'users_m');
    	
    	$data['navigation'] = "<a href='". site_url('admin/jobincident') ."' >Job Incident</a> > Incident Edit";
        
        $job = $this->job_incident_m->get_info($id);
        if(!$job){
        	show_404();
        }
        $data['job'] = $job;
        
        $users = $this->users_m->get_select_dropdown();
        $data['users'] = $users;
        
        // Get attached files
        $data['attached_files'] = $this->job_incident_file_m->get_file($id);
        
        
        $this->load->view('navigation', $data);
        $this->load->view('jobincident/edit_job_incident');
    }
    
    /**
     * process edit job incident
     */
    function do_edit() {
    	$now_time = now();
    	$job_id = $this->input->post('job_id');
    	$this->form_validation->set_rules('incident_date', 'Date of incident', 'trim|required');
    	$this->form_validation->set_rules('customer', 'Customer(s) involved', 'trim|required');
        $this->form_validation->set_rules('reference_no', 'Reference no - Invoice no', 'trim|required');
        $this->form_validation->set_rules('detail', 'Detail of the inicident', 'trim|required');
        $this->form_validation->set_rules('how_occur', 'How did the incident occur', 'trim|required');
        $this->form_validation->set_rules('how_prevent', 'What should have been done to prevent it from happening next time', 'trim|required');
        $this->form_validation->set_rules('how_repair', 'What action should be taken now to repair or recover from the incident', 'trim|required');
        $this->form_validation->set_rules('what_cost', 'What is the approximate time and cost involved as a result of this incident', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $this->edit($job_id);
        } else {
        	$user_id = $this->session->userdata('user_id');
        	$user_name = $this->session->userdata('user_name');
        	
        	$staff_id = $this->input->post('staff_id');
        	$operate = $this->input->post('operate');
        	$incident_date = $this->input->post('incident_date');
        	$incident_date = strtotime($incident_date);
        	
        	$customer = $this->input->post('customer');
        	$reference_no = $this->input->post('reference_no');
        	$detail = $this->input->post('detail');
        	$how_occur = $this->input->post('how_occur');
        	$how_prevent = $this->input->post('how_prevent');
        	$how_repair = $this->input->post('how_repair');
        	$what_cost = $this->input->post('what_cost');
        	$cost_value = $this->input->post('cost_value');
			$emails = $this->input->post('emails');
        	
        	
        	$jobincident = array(
        		'staff_id' => $staff_id,
        		'incident_date' => $incident_date,
        		'customer' => $customer,
        		'reference_no' => $reference_no,
        		'detail' => $detail,
        		'how_occur' => $how_occur,
        		'how_prevent' => $how_prevent,
        		'how_repair' => $how_repair,
        		'what_cost' => $what_cost,
        		'cost_value' => $cost_value,
        		'emails' => $emails,
        		'incident_date' => $incident_date,
        	);
        	$this->job_incident_m->update($job_id, $jobincident);
        	$this->load->model('St_users_m', 'users_m');
        	$job_info = $this->job_incident_m->get_info($job_id);
        	
        	$this->load->model('St_job_incident_log_m', 'job_incident_log_m');
       		$jobincident_log = array(
       			'id' => $job_id,	
       			'operator_id' => $user_id,
       			'operation' => 'Update',
       			'operate_time' => $now_time,
       			'operate_ip' => $this->input->ip_address()
       		);
       		$jobincident_log = array_merge($jobincident, $jobincident_log);
       		$this->job_incident_log_m->insert($jobincident_log);
        	
        	$upload_filename = "";
        	$fileElementName = 'upload_file';
        	$error = "";
        	if(!empty($_FILES[$fileElementName]['tmp_name']) && $_FILES[$fileElementName]['tmp_name'] != 'none'){

        		if(!empty($_FILES[$fileElementName]['error'])){
        			switch($_FILES[$fileElementName]['error']){
        				case '1':
        					$error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
        					break;
        				case '2':
        					$error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
        					break;
        				case '3':
        					$error = 'The uploaded file was only partially uploaded';
        					break;
        				case '4':
        					$error = 'No file was uploaded.';
        					break;
        	
        				case '6':
        					$error = 'Missing a temporary folder';
        					break;
        				case '7':
        					$error = 'Failed to write file to disk';
        					break;
        				case '8':
        					$error = 'File upload stopped by extension';
        					break;
        				case '999':
        				default:
        					$error = 'No error code avaiable';
        			}
        		}else{
        			$tempFile = $_FILES[$fileElementName]['tmp_name'];
        	
        			$now_dir = date('Y/m/d');
        			$targetPath = str_replace('//','/',DIR_JOB_INCIDENT_FILE . $now_dir . '/');
        	
        			if(mkdirs($targetPath)){
        				$filename = $_FILES[$fileElementName]['name'];
        	
        				// Get the extension from the filename.
        				$extension = substr($filename, strpos($filename,'.'), strlen($filename)-1);
        	
        				$target_filename = random_filename($targetPath, $extension, 6);
        				$upload_filename = $now_dir . '/' . $target_filename;
        	
        				$targetFile = $targetPath . $target_filename;
        	
        				if (move_uploaded_file($tempFile,$targetFile)){
        					$jobincident_file = array(
        							'job_id' 	=> $job_id,
        							'file' 		=> $upload_filename,
        							'add_time' 	=> $now_time,
        					);
        	
        					$this->job_incident_file_m->insert($jobincident_file);
        				}
        				 
        			}else{
        				$error = "Can't create directory";
        			}
        		}
        	}
        	
        	if($error != ""){
        		echo ' {"message":"'.$error.'", "status":"err"}';
        	}
        	
        	
//        	$this->incident_list('new');
        }
	}
    /**
     * approve job incident
     */
    function view($id, $from=1) {
    	$data['navigation'] = "<a href='". site_url('admin/jobincident') ."' target='_top'>Job Incident</a> > Incident View";
        
        $job = $this->job_incident_m->get_info($id);
        if(!$job){
        	show_404();
        }

        $job->manager_name = '';
        $job->supervisor_name = '';
        $this->load->model('St_users_m', 'users_m');
        if(!is_null($job->supervisor_user_name)){
        	$supervisor = $this->users_m->get_by_user_name($job->supervisor_user_name);
        	if(!empty($supervisor)){
        		$job->supervisor_name = $supervisor->first_name . " " . $supervisor->last_name;
        	}
        }
         
        if(!is_null($job->manager_user_name)){
        	$manager = $this->users_m->get_by_user_name($job->manager_user_name);
        	if(!empty($manager)){
        		$job->manager_name = $manager->first_name . " " . $manager->last_name;
        	}
        }

        $order_master = $this->job_incident_m->get_scorpy_order_master($job->reference_no, $job->customer);
        if ($order_master)
        	$job->order_url = SCORPY_HOME."/orders_forms.php?type=form&ordid=".$order_master->ordid;
        else
	       	$job->order_url = false;


        $data['job'] = $job;
        $data['from'] = $from;
        $data['user_level'] = $this->session->userdata('user_level');
        
        
        //load request log model
        $this->load->model('St_job_incident_log_m', 'job_incident_log_m');
        $request_logs = $this->job_incident_log_m->get_by_search($id, "", "", "", "", "", "", "");
        $log_list = array();
        foreach($request_logs AS $request_log){
        	$log['request_id'] = $request_log->id;
        	//Action
        	$log['action'] = $request_log->operation;
        	//Actioned On
        	$log['actioned_on'] = date(DATETIME_FORMAT, $request_log->operate_time);
        	//Actioned By
        	$log['actioned_by'] = $request_log->operator_first_name . " " . $request_log->operator_last_name;
        	$log['action_ip'] = $request_log->operate_ip;
        	$log_list[] = $log;
        }
        $data['job_incident_logs'] = $log_list;
        $data['user_level'] = $this->session->userdata('user_level');
        $data['current_user_id'] = $this->session->userdata('user_id');
        
        // Get attached files
        $data['attached_files'] = $this->job_incident_file_m->get_file($id);
        
        $this->load->view('navigation', $data);
        $this->load->view('jobincident/view_job_incident');
    }
    
    function new_approvals_ajax(){
    	log_message("debug", "new_approvals_ajax .....");
		$user_level = $this->session->userdata('user_level');
		
    	$data['form_id'] = 'new';
    	
    	$site_id = $this->input->post('new_filter_site_id');
    	$department_id = $this->input->post('new_filter_department_id');
    	$page_no = $this->input->post('new_page_no');
        
		$limit = $this->input->post('new_limit');
    	$data['page_limit'] = $limit;
    	
		$data['jobs'] = $this->job_incident_m->get_approvals_by_new($user_level, $site_id, $department_id, $page_no, $limit);
		$total_count = $this->job_incident_m->count_approvals_by_new($user_level, $site_id, $department_id);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
		
		$content = $this->load->view('jobincident/list_approve_job_incident_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }
    
    function progress_approvals_ajax(){
    	log_message("debug", "progress_approvals_ajax .....");
		$user_level = $this->session->userdata('user_level');
		
    	$data['form_id'] = 'progress';
    	
    	$site_id = $this->input->post('progress_filter_site_id');
    	$department_id = $this->input->post('progress_filter_department_id');
    	$page_no = $this->input->post('progress_page_no');
        
		$limit = $this->input->post('progress_limit');
    	$data['page_limit'] = $limit;
    	
		$data['jobs'] = $this->job_incident_m->get_approvals_by_progress($user_level, $site_id, $department_id, $page_no, $limit);
		$total_count = $this->job_incident_m->count_approvals_by_progress($user_level, $site_id, $department_id);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
		
		$content = $this->load->view('jobincident/list_approve_job_incident_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }
    
    function completed_approvals_ajax(){
    	log_message("debug", "completed_approvals_ajax .....");
		$user_level = $this->session->userdata('user_level');
		
    	$data['form_id'] = 'completed';
    	
    	$site_id = $this->input->post('completed_filter_site_id');
    	$department_id = $this->input->post('completed_filter_department_id');
    	$page_no = $this->input->post('completed_page_no');
        
		$limit = $this->input->post('completed_limit');
    	$data['page_limit'] = $limit;
    	
		$data['jobs'] = $this->job_incident_m->get_approvals_by_completed($user_level, $site_id, $department_id, $page_no, $limit);
		$total_count = $this->job_incident_m->count_approvals_by_completed($user_level, $site_id, $department_id);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
		
		$content = $this->load->view('jobincident/list_approve_job_incident_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }
}