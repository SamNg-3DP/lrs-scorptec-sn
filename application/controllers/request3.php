<?php

class Request3 extends CI_Controller {
	
	function Request3() {
		parent::__construct();
		$this->load->helper ( 'url' );
		$this->load->helper ( array ('form', 'url' ) );
		$this->load->library ( 'form_validation' );
		$this->form_validation->set_error_delimiters ( "<div style='font:Tahoma, Geneva, sans-serif; font-size:11px; color:#F00'>", "</div>" );
	}
	
	function index() {
		$data['navigation'] = "<a href='#'>Leave Request</a> > View";
        $this->load->view('navigation', $data);
		$this->load->view('request/view_request_3');
	}
	
	function detailed() {
		$data['navigation'] = "<a href='#'>Leave Request</a> > View";
        $this->load->view('navigation', $data);
		$this->load->view('request/detailed_request_3');
	}
	
	function close() {
		$this->index();
	}
}