<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Leave approval</title>
<base href="<?=base_url()?>"/>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.tools.tabs.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />
<style type="text/css">
<!--
#form1 table tr td table {
	text-align: left;
}
#form1 p {
	text-align: left;
}
#form1 table tr td table tr td {
    font-family: Tahoma, Geneva, sans-serif;
	font-size: 11px;
}
/* tab pane styling */
div.panes div {
	display:none;		
	padding:15px 10px;
	border:1px solid #999;
	border-top:0;
	font-size:14px;
	background-color:#fff;
}

div.pane {
	display:none;		
	padding:15px 10px;
	border:1px solid #999;
	border-top:0;
	font-size:14px;
	background-color:#fff;
}
-->
</style>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery.paginate.js"></script>
<script type="text/javascript" src="js/common.js"></script>
</head>
<body>
<div class="wrap">
	<div style="padding-left:10px;padding-bottom:5px;"> Current Leave Records </div>
	<!-- the tabs --> 
	<ul class="tabs">
	    <li><a href="#pending">Waiting Approval (<?=$waiting_count?>)</a></li>
	    <li><a href="#New">New (<?=$new_count?>)</a></li> 
	    <li><a href="#not_approved">Not Approved (<?=$not_approved_count?>)</a></li>
	    <li><a href="#not_approved">Upcoming (0)</a></li> 
	</ul> 
	 
	<!-- tab "panes" --> 
	<div class="panes">
	    <div style="display:block" id="tab_waiting"></div>
	    <div style="display:block" id="tab_new"></div>
	    <div style="display:block" id="tab_not_approved"></div>
	    <div style="display:block" id="tab_upcoming"></div>
	</div>
</div>

<div class="wrap">
	<div style="padding-top:10px;padding-left:10px;padding-bottom:5px;"> Past Leave Records</div>
	<!-- the tabs -->
	<ul class="tabs">
		<li><a href="#annual">Annual Leave</a></li>
		<li><a href="#personal">Personal Leave</a></li>
		<li><a href="#lateness">Lateness</a></li>
		<li><a href="#approved">Training/Seminar</a></li> 
	</ul>
	
	<!-- tab "panes" -->
	<div class="panes">
		<div id="tab_annual"></div>
	    <div id="tab_personal"></div>
	    <div id="tab_lateness"></div>
		<div id="tab_training"></div>
	</div>
</div>

<!-- This JavaScript snippet activates those tabs -->
<script type="text/javascript">
$(function() { 
    $("ul.tabs").tabs("div.panes > div");
    $("#tab_new").load("<?php echo site_url("approve/saved")?>");
    $("#tab_annual").load("<?php echo site_url("approve/approved_annual")?>");
    
    $("#tab_not_approved").load("<?php echo site_url("approve/not_approved")?>");
    $("#tab_waiting").load("<?php echo site_url("approve/waiting")?>");
    $("#tab_personal").load("<?php echo site_url("approve/approved_personal")?>");
    $("#tab_lateness").load("<?php echo site_url("approve/approved_lateness")?>");
    $("#tab_training").load("<?php echo site_url("approve/approved_training")?>");

    waitingRefreshPageinate(<?php echo $waiting_page_count;?>, 1)
});

function get_post_url(form_id){
    if(form_id == "waiting"){
        return "<?=site_url('approve/waiting_ajax')?>";
    }else{
        alert("Please select form id");
    }
}
function sort(form_id, orderby){
	if($("#" + form_id + "_order_by").val() == orderby){
		if($("#" + form_id + "_order").val() == "ASC"){
			$("#" + form_id + "_order").val("DESC");
		}else{
			$("#" + form_id + "_order").val("ASC");
		}
	}else{
		$("#" + form_id + "_order_by").val(orderby);
		$("#" + form_id + "_order").val("ASC");
	}
	alert(form_id);
	return submitForm(form_id);
}

function submitForm(form_id){
	if(form_id == "waiting"){
		var waiting_options = {
			dataType:  'json',
			success: waitingResponse,
			url: "<?=site_url('approve/waiting_ajax')?>"
		};
		$('#'+form_id).ajaxSubmit(waiting_options);
	}
	
	return false;
}

function waitingResponse(responseText, statusText, xhr, $form){
	//$("#list_waiting").html(responseText.content);
	$("#list_waiting").find("tr:gt(0)").remove();
	$('#list_waiting tr:last').after(responseText.content);
	$("#list_waiting").find("tr:eq(0)").remove();
	//refreshPageinate("waiting", responseText.page_count, responseText.page_no);
}

function search(form){
	$("#" + form + "_filter_department_id").val($("#department_id").val());
	$("#" + form + "_filter_user_level_id").val($("#user_level_id").val());
	$("#" + form + "_page_no").val(1);
	return submitForm(form);
}

function waitingRefreshPageinate(pageCount, pageNo){
	if(pageCount == 0 || pageCount == 1){
		$("#waitingCurrentPaginate").hide();
	}else{
		$("#waitingCurrentPaginate").show();
		$("#waitingjPaginate").show();
		
		$("#waitingjPaginate").paginate({
			count 		: pageCount,
			start 		: pageNo,
			display     : 12,
			border					: false,
			text_color  			: '#79B5E3',
			background_color    	: 'none',	
			text_hover_color  		: '#2573AF',
			background_hover_color	: 'none',
			rotate      : false,
			images		: false,
			mouse		: 'press',
			onChange 	: waitingGotoPage
		});
	}
}

function waitingGotoPage(pageNo){
	$("#page_no").val(pageNo);
	submitForm("waiting");
}
</script>

</body>
</html>