<?php



class Actatek{

	private  $client = false;
	private  $id = false;
	private  $tz_offset = 0;
	
	private $machine = array(	"00111DA05C39" => "Cl 4",
								"00111DA0506D" => "Cl 6",
								"00111DA06EEB" => "Row",
								"00111DA08C6F" => "Mal",
								"00111DA08C57" => "StM");
	
	function actatek(){
//		error_reporting(E_ALL);
		
		ini_set("soap.wsdl_cache_enabled", "0");
		
		$this->object =& get_instance();
		$this->object->load->database();
		
		$this->client = new SoapClient(NULL,
	    array(
	        "location" => "http://192.168.0.24/cgi-bin/rpcrouter",
	        "uri" => "http://www.hectrix.com/ACTAtek.xsd",
	        "style" => SOAP_RPC,
	        "use" => SOAP_LITERAL,
	        "trace" => 1, 
	        "exceptions" => 1));

	   	try {
		    $this->id = $this->client->login(
		        		new SoapParam('109', 'username'),
		        		new SoapParam('1111','password'));

		    // Set timezone offset by comparing ac to server time.
		    $result =& $this->client->getTerminalDateTime(new SoapParam($this->id,'sessionID') );
		    $ac_now = str_replace(array("T","Z")," ",$result);
			$this->tz_offset = strtotime("now") - strtotime($ac_now);
			$this->tz_offset = round($this->tz_offset/3600) * 3600 ; 
		    
	    } catch (Exception $e){
	        print $e->getMessage();
	        print "Error";
	        echo "REQUEST:<pre>\n" . $this->client->__getLastRequest() ."\n</pre>";
	        throw $e;
	    }
	}
	
	function get_users(){
	    try {
	        $result = $this->client->getUsers(
				        new SoapParam($this->id, 'sessionID'),
				        new SoapParam(NULL, 'getUsersCriteria')
				       );

			return $result->user;
	    } catch (Exception $e){
	        print $e->getMessage();
	        print "Error";
	        echo "REQUEST:<pre>\n" . $this->client->__getLastRequest() ."\n</pre>";
	        throw $e;
	    }
	}
	
	function get_logs($user_id=false, $from_time=false, $to_time=false){
		
		set_time_limit(600);
		
		$return = array();
		$cr_array = array();
		$use_criteria = false;
		
		
		$from_time = strtotime($from_time) - $this->tz_offset;
		$from_time = date('Y-m-d', $from_time)."T".date('H:i:s', $from_time)."Z";
		
		$to_time = strtotime($to_time) - $this->tz_offset;
		$to_time = date('Y-m-d', $to_time)."T".date('H:i:s', $to_time)."Z";
		
		
		
		if ($user_id){
			$use_criteria = true;
			$user_id = new SoapVar($user_id,XSD_STRING,NULL,NULL,'employeeID');
			array_push($cr_array, $user_id);
		}
		
		if ($from_time){
			$use_criteria = true;
			$from_time = new SoapVar($from_time,XSD_STRING,NULL,NULL,'from');
			array_push($cr_array, $from_time);
		}
		
		if ($to_time){
			$use_criteria = true;
			$to_time =	new SoapVar($to_time,XSD_STRING,NULL,NULL,'to');
			array_push($cr_array, $to_time);
		}
		
	    try {
	    	
	    	if ($use_criteria){
			    $criteria = new SoapVar( $cr_array, SOAP_ENC_OBJECT,NULL,NULL,'getLogsCriteria');
	    	} else {
	    		$criteria = NULL;
	    	}
			    						
	    			
	        $result =& $this->client->getLogs(
				       	new SoapParam($this->id,'sessionID'),
				        $criteria
				       );
	        
        	if (!empty($result)){
        		if (is_array($result->log)){
        			$i=0;
		        	foreach ($result->log as $res){
//		        		echo "<br>".$res->userID." ".$res->trigger." ".$res->timestamp;
		        		$return[$i]['trigger'] = $res->trigger;
		        		
		        		$res->timestamp = str_replace(array("T","Z")," ",$res->timestamp);
		        		$res->timestamp = strtotime($res->timestamp) + $this->tz_offset;
		        		$res->timestamp = date("H:i:s", $res->timestamp);
		        		
		        		$return[$i]['timestamp'] = $res->timestamp;
		        		$return[$i]['location'] = $this->machine[$res->terminalSN];
		        		$i++; 
		        	}
        		}else{
//        			echo "<br>".$result->log->userID." ".$result->log->trigger." ".$result->log->timestamp;
					$return[$result->log->logID]['trigger'] = $result->log->trigger;
					
					$result->log->timestamp = str_replace(array("T","Z")," ",$result->log->timestamp);
					$result->log->timestamp = strtotime($result->log->timestamp) + $this->tz_offset;
					$result->log->timestamp = date("H:i:s", $result->log->timestamp);
						
        			$return[$result->log->logID]['timestamp'] = $result->log->timestamp;
        			$return[$result->log->logID]['location'] = $this->machine[$result->log->terminalSN];
        		}
        	}
	    	
	    	return $return;
	    	
	    } catch (Exception $e){
	        print $e->getMessage();
	        print "Error";
	        echo "REQUEST:<pre>\n" . $this->client->__getLastRequest() ."\n</pre>";
	        throw $e;
	    }
	}
	
	function get_current_clock_by_actatek_id($actatek_id){
		if (!$actatek_id)
			return false;
		
		$at_start_time = date("Y-m-d")." 00:00:00";
		$at_end_time = date("Y-m-d")." 23:59:59";
		
		$u_date = date('U',strtotime(date("Y-m-d")));
		
		$logs = $this->get_logs($actatek_id, $at_start_time, $at_end_time);
		
		$returns = array();		
  		foreach ($logs as $log){
			$object = new stdClass();
			
			$object->date 		= $u_date;
			$object->time 		= $log['timestamp'];
			$object->direction 	= $log['trigger'];
			$object->location 	= $log['location'];
			
			array_push($returns, $object);
		}
		
		return $returns;
		
	}
	


}

?>