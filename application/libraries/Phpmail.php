<?php
require("class.phpmailer.php");

class Phpmail extends PHPMailer {
	
	public function Phpmail() { 
		$this->__construct();
		$this->object =& get_instance();
		$this->object->load->database();
	}
	
	var $SMTPAuth = false;
    // Set default variables for all new objects
	var $Mailer   = MAIL_PROTOCOL;                         // Alternative to IsSMTP()
    var $Host     = SMTP_HOST;
    var $Port 	  = SMTP_PORT;
    var $WordWrap = 75;
    var $CharSet = MAIL_CHARSET;
    var $Username = SMTP_USER;
    var $Password = SMTP_PASS;
    
    var $From     = MAIL_FROM;
    var $FromName = "Scorptec notification";

    function readConfig(){
    	$sql = "SELECT config.*  
				FROM st_config config
				WHERE config.type = 2 AND config.status = 1";
		$query = $this->object->db->query($sql);
	    foreach ($query->result() as $row)
		{
			if($row->code == "MAIL_PROTOCOL"){
				if(!empty($row->value)){
					$this->Mailer = $row->value;
				}
		   	}else if($row->code == "SMTP_HOST"){
				if(!empty($row->value)){
					$this->Host = $row->value;
				}
		   	}else if($row->code == "MAIL_CHARSET"){
				if(!empty($row->value)){
					$this->CharSet = $row->value;
				}
		   	}else if($row->code == "SMTP_USER"){
				if(!empty($row->value)){
					$this->Username = $row->value;
				}
		   	}else if($row->code == "SMTP_PASS"){
				if(!empty($row->value)){
					$this->Password = $row->value;
				}
		   	}else if($row->code == "MAIL_FROM"){
				if(!empty($row->value)){
					$this->From = $row->value;
				}
		   	}else if($row->code == "MAIL_FROM_NAME"){
				if(!empty($row->value)){
					$this->FromName = $row->value;
				}
		   	}else if($row->code == "MAIL_SENDMAIL_FOLDER"){
				if(!empty($row->value)){
					$this->Exchange_folder = $row->value;
				}
		   	}
		}
    }
    
    /**
     * set subject
     * @param $title
     * @return void
     */
    function setSubject($title){
    	$this->Subject = $title;
    }
    
    /**
     * set mail body
     * @param $body
     * @param $is_html
     * @return void
     */
    function setBody($body, $is_html = true){
    	if($is_html){
    		$this->MsgHTML($body);
    	}else{
    		$this->Body = $body;
    	}    	
    }
    
    /**
     * set html body
     * @param $html_body
     * @return void
     */
    function setHTMLBody($html_body){
    	$this->MsgHTML($html_body);
    }
    
	/**
	  * Clears all recipients assigned in the TO array.  Returns void.
	  * @return void
	  */
	public function ClearAllAddresses() {
	    $this->ClearAddresses();
	    $this->ClearCCs();
	    $this->ClearBCCs();
	}
    
    // Replace the default error_handler
    function error_handler($msg) {
        print("My Site Error");
        print("Description:");
        printf("%s", $msg);
        exit;
    }

    // Create an additional function
    function setRequestBody($fullname, $leave_type, $request_date, $start_date, $end_date, 
    	$reason, $amount_leave, $total_days, $appove_url, $appove_name) {
    	$html_body = '<p>Leave  Application</p>
			<table width="450" border="1">
			  <tr>
			    <th width="186" align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Employee: </span></th>
			    <td width="248">&nbsp;'
			    .$fullname.
			    '</td>
			  </tr>
			  <tr>
			    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Leave  Type:</span></th>
			    <td>&nbsp;
			    '.$leave_type.'
			    </td>
			  </tr>
			  <tr>
			    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Requested  On</span></th>
			    <td>&nbsp;'
			    .$request_date.
			    '</td>
			  </tr>
			  <tr>
			    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">From:</span></th>
			    <td>&nbsp;'
			    .$start_date.
			    '</td>
			  </tr>
			  <tr>
			    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">To:</span></th>
			    <td>&nbsp;'
			    .$end_date.
			    '</td>
			  </tr>
			  <tr>
			    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Reason:</span></th>
			    <td>&nbsp;'
			    .$reason.
			    '</td>
			  </tr>
			  <tr>
			    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Amount  of Leave Left (before this request)</span></th>
			    <td>&nbsp;'
			    .$amount_leave.
			    '</td>
			  </tr>
			  <tr>
			    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">This  leave is</span></th>
			    <td>&nbsp;'
			    .$total_days.
			    '</td>
			  </tr>
			</table>
			<p>Supervisors Response </p>
			<p>Please <a href="'
			.$appove_url.
			'">'
			.$appove_name.
			'</a></p>';
    	$this->MsgHTML($html_body);
        // Place your new code here
    }
    
	function setApproveBody($fullname, $leave_type, $request_date, $start_date, $end_date, 
    	$reason, $amount_leave, $total_days, $appove_url, $appove_name,
    	$hod=-1, $reason_acceptable=-1, $super_approved=-1, $Paid=-1, $manager_approve=-1
    	) {
    	$html_body = '<p>Leave  Application</p>
			<table width="450" border="1">
			  <tr>
			    <th width="186" align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Employee: </span></th>
			    <td width="248">&nbsp;'
			    .$fullname.
			    '</td>
			  </tr>
			  <tr>
			    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Leave  Type:</span></th>
			    <td>&nbsp;
			    '.$leave_type.'
			    </td>
			  </tr>
			  <tr>
			    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Requested  On</span></th>
			    <td>&nbsp;'
			    .$request_date.
			    '</td>
			  </tr>';			  
		if($leave_type == LEAVE_TYPE_LATE || $leave_type == LEAVE_TYPE_LUNCH){
			$html_body .= 
			'<tr>
			    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Arrival Day:</span></th>
			    <td>&nbsp;'
			    .$start_date.
			    '</td>
			  </tr>
			  <tr>
			    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">This  leave is</span></th>
			    <td>&nbsp;'
			    .$total_days.
			    '</td>
			  </tr>';
		}else{
			$html_body .= 
			'<tr>
			    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">From:</span></th>
			    <td>&nbsp;'
			    .$start_date.
			    '</td>
			  </tr>
			  <tr>
			    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">To:</span></th>
			    <td>&nbsp;'
			    .$end_date.
			    '</td>
			  </tr>';
			 $html_body .= 
			  '<tr>
			    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Reason:</span></th>
			    <td>&nbsp;'
			    .$reason.
			    '</td>
			  </tr>
			  <tr>
			    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Amount  of Leave Left (before this request)</span></th>
			    <td>&nbsp;'
			    .$amount_leave.
			    '</td>
			  </tr>
			  <tr>
			    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">This  leave is</span></th>
			    <td>&nbsp;'
			    .$total_days.
			    '</td>
			  </tr>';
		}
			 
		log_message("debug", "hod ===" . $hod );
		if($hod == 1 || $hod == 0){
			log_message("debug", "2hod ===" . $hod );
			$hod_str = "";
			if($hod == 1){
				$hod_str = "M.C. Provided";
			}else{
				$hod_str = "M.C. Not Provided";
			}
			$html_body .='
			  <tr>
			    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">HOD Information</span></th>
			    <td>&nbsp;'
			    .$hod_str.
			    '</td>
			  </tr>';
		}
    	if($reason_acceptable == 1 || $reason_acceptable == 0){
			$reason_str = "Acceptable";
			if($reason_acceptable == 0){
				$reason_str = "Not Acceptable";
			}
			$html_body .='
			  <tr>
			    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Reason</span></th>
			    <td>&nbsp;'
			    .$reason_str.
			    '</td>
			  </tr>';
		}
    	if($super_approved == 1 || $super_approved == 0){
			$super_str = "Approved";
			if($super_approved == 0){
				$super_str = "Not Approved";
			}
			$html_body .='
			  <tr>
			    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Supervisor approval</span></th>
			    <td>&nbsp;'
			    .$super_str.
			    '</td>
			  </tr>';
		}
    	if($Paid == 1 || $Paid == 0){
			$paid_str = "Paid";
			if($Paid == 0){
				$paid_str = "Unpaid";
			}
			$html_body .='
			  <tr>
			    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Paid</span></th>
			    <td>&nbsp;'
			    .$paid_str.
			    '</td>
			  </tr>';
		}
    	if($manager_approve == 1 || $manager_approve == 0){
			$approve_str = "Approve";
			if($Paid == 0){
				$approve_str = "Reject";
			}
			$html_body .='
			  <tr>
			    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Manager approval</span></th>
			    <td>&nbsp;'
			    .$approve_str.
			    '</td>
			  </tr>';
		}
		$html_body .= '</table>
			<p>Response</p>
			<p>Please <a href="'
			.$appove_url.
			'">'
			.$appove_name.
			'</a></p>';
    	$this->MsgHTML($html_body);
        // Place your new code here
    }
}


?>