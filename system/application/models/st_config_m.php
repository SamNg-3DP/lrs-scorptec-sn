<?php

/**
 * config for system
 * @author Bai Yinbing
 */
class St_config_m extends MY_Model {
	
	    
    /**
     * set active status for specail leave type
     * @param $leave_type_id
     * @return boolean
     */
    function set_active($id)
    {
    	return $this->update($id, 
    			array('status' => ACTIVE)
    	);
    }
    
    /**
     * set in-active status for specail leave type
     * @param $leave_type_id
     * @return boolean
     */
	function set_inactive($id)
	{
    	return $this->update($id,
    			array('status' => IN_ACTIVE)
    	);
    }
    
    /**
     * get leave hour parameters
     * @return array
     */
    function get_leave_hour_parameter(){
    	$sql = "SELECT *  
				FROM " . $this->table . " 
				WHERE code = 'Annual_Leave_Parameter' OR code ='Personal_Leave_Parameter'
				";
		$query = $this->db->query($sql);
    	return $query->result();
    }
    
    /**
     * get leave hour parameters
     * @return array
     */
    function get_email_parameter(){
    	$sql = "SELECT *  
				FROM " . $this->table . " 
				WHERE type = 2 AND status = 1";
		$query = $this->db->query($sql);
    	return $query->result();
    }
    
    /**
     * get payroll parameters
     * @return array
     */
    function get_payroll_parameter(){
    	$sql = "SELECT *  
				FROM " . $this->table . " 
				WHERE code = 'Initial_Payroll_Date_Parameter' OR code ='Days_Payroll_Parameter'
				";
		$query = $this->db->query($sql);
    	return $query->result();
    }
    
    /**
     * get value by code
     * @param $code
     * @return object
     */
    function get_by_code($code){
    	return $this->get_by(array('code'=>$code));
    }
    
	function update_by_code($code, $data){
    	return $this->update_by('code', $code, $data);
    }
}