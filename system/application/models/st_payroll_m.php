<?php

class St_payroll_m extends MY_Model {
	
	/**
	 * get all payrolls
	 */
	function get_payrolls($page_no=1, $limit=PAGE_SIZE){
		$sql =  " SELECT * " .
				" FROM st_payroll " .
				" WHERE status = " . ACTIVE .
				" ORDER BY id DESC "
				;
		if(empty($page_no)){
			$page_no = 1;
		}
		
		if(!empty($limit)){
			$start = ($page_no - 1) * $limit;
			$sql .= " LIMIT " . $start . ", " . $limit;
		}
		
		log_message("debug", "get_payrolls <<< " . $sql);
    	$query = $this->db->query($sql);
    	return $query->result();
	}
	
	/**
	 * get alll processed payrolls
	 */
	function get_processed($page_no=1, $limit=PAGE_SIZE){
		$sql =  " SELECT * " .
				" FROM st_payroll " .
				" WHERE status = " . ACTIVE .
				"    	AND processed = 1 " .
				" ORDER BY id DESC "
				;
		if(empty($page_no)){
			$page_no = 1;
		}
		
		if(!empty($limit)){
			$start = ($page_no - 1) * $limit;
			$sql .= " LIMIT " . $start . ", " . $limit;
		}
		
		log_message("debug", "get_payrolls <<< " . $sql);
    	$query = $this->db->query($sql);
    	return $query->result();
	}
	
	/**
	 * get alll unprocessed payrolls
	 */
	function get_unprocessed($page_no=1, $limit=PAGE_SIZE){
		$sql =  " SELECT * " .
				" FROM st_payroll " .
				" WHERE status = " . ACTIVE .
				"    	AND processed = 0 " .
				" ORDER BY id ASC "
				;
		if(empty($page_no)){
			$page_no = 1;
		}
		
		if(!empty($limit)){
			$start = ($page_no - 1) * $limit;
			$sql .= " LIMIT " . $start . ", " . $limit;
		}
		
		log_message("debug", "get_payrolls <<< " . $sql);
    	$query = $this->db->query($sql);
    	return $query->result();
	}
	
	/**
	 * get alll unprocessed payrolls
	 */
	function get_qb_exported($page_no=1, $limit=PAGE_SIZE){
		$sql =  " SELECT * " .
				" FROM st_payroll " .
				" WHERE status = " . ACTIVE .
				"    	AND processed = 2 " .
				" ORDER BY id DESC "
				;
		if(empty($page_no)){
			$page_no = 1;
		}
		
		if(!empty($limit)){
			$start = ($page_no - 1) * $limit;
			$sql .= " LIMIT " . $start . ", " . $limit;
		}
		
		log_message("debug", "get_payrolls <<< " . $sql);
    	$query = $this->db->query($sql);
    	return $query->result();
	}
	
	function get_adjustments($page_no=1, $limit=PAGE_SIZE){
		$sql =  " SELECT * " .
				" FROM st_payroll " .
				" WHERE status = " . ACTIVE .
				"    	AND processed = 3 " .
				" ORDER BY id ASC "
				;
		if(empty($page_no)){
			$page_no = 1;
		}
		
		if(!empty($limit)){
			$start = ($page_no - 1) * $limit;
			$sql .= " LIMIT " . $start . ", " . $limit;
		}
		
		log_message("debug", "get_payrolls <<< " . $sql);
    	$query = $this->db->query($sql);
    	return $query->result();
	}
	
	/**
	 * get total count from st payroll table
	 */
	function count_payrolls(){
    	$sql =  " SELECT count(id) AS num " .
				" FROM st_payroll " .
				" WHERE status = " . ACTIVE 
				;

		$total_count = 0;
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0){
			$row = $query->row();
			$total_count = $row->num;
		}
		
		log_message("debug", "count_payrolls <<< " . $sql);
		log_message("debug", "count_payrolls total_count <<< " . $total_count);
        return $total_count;
    }
    
    /**
	 * get processed total count from st payroll table
	 */
    function count_processed(){
    	$where = array(
    		'status' => ACTIVE,
    		'processed'  => 1
    	);
    	return $this->count_by($where);
    }
    
    /**
	 * get unprocessed total count from st payroll table
	 */
    function count_unprocessed(){
    	$where = array(
    		'status' => ACTIVE,
    		'processed'  => 0
    	);
    	return $this->count_by($where);
    }
    
    /**
	 * get unprocessed total count from st payroll table
	 */
    function count_qb_exported(){
    	$where = array(
    		'status' => ACTIVE,
    		'processed'  => 2
    	);
    	return $this->count_by($where);    	
    }
    
    /**
	 * get adjustment total count from st payroll table
	 */
    function count_adjustment(){
    	$where = array(
    		'status' => ACTIVE,
    		'processed'  => 3
    	);
    	return $this->count_by($where);
    }
	/**
     * get last payroll
     * @return array
     */
    function get_last(){
    	$sql = "SELECT *  
				FROM " . $this->table . " 
				ORDER BY id DESC
				LIMIT 1 ";
		$query = $this->db->query($sql);
    	return $query->row();
    }
	
	/**
	 * get QB Export list
	 */
	function get_qb_export($page_no=1, $limit=PAGE_SIZE){
		$sql =  " SELECT * " .
				" FROM st_payroll " .
				" WHERE status = " . ACTIVE .
				"    	AND processed = 1 " .
				" ORDER BY processed ASC "
				;
		if(empty($page_no)){
			$page_no = 1;
		}
		
		if(!empty($limit)){
			$start = ($page_no - 1) * $limit;
			$sql .= " LIMIT " . $start . ", " . $limit;
		}
		
		log_message("debug", "get_payrolls <<< " . $sql);
    	$query = $this->db->query($sql);
    	return $query->result();
	}
	
	/**
	 * set processed for special payroll id
	 */
	function setProcessed($payroll_id){
		$sets = array(
			'processed' => 1
		);
		return $this->update($payroll_id, $sets);
	}
	
	/**
	 * set not processed for special payroll id
	 */
	function setNotProcessed($payroll_id){
		$sets = array(
			'processed' => 0
		);
		return $this->update($payroll_id, $sets);
	}
	
	
	/**
	 * set QB exported for special payroll id
	 */
	function setQBexported($payroll_id){
		$sets = array(
			'processed' => 2
		);
		return $this->update($payroll_id, $sets);
	}
	
	/**
	 * set Adjustment for special payroll id
	 */
	function setAdjustment($payroll_id){
		$sets = array(
			'processed' => 3
		);
		return $this->update($payroll_id, $sets);
	}
	/**
     * get adjustment payroll
     * @return payroll
     */
    function get_adjustment(){
    	$sql =  " SELECT * " .
				" FROM st_payroll " .
				" WHERE status = " . ACTIVE .
				"    	AND processed = 3 ";
		log_message("debug", "get_adjustment <<< " . $sql);
		$query = $this->db->query($sql);
    	return $query->row();
    }
    
    /**
     * get first uprocessed payroll
     * @return payroll
     */
    function get_first_unprocessed(){
    	$sql =  " SELECT * " .
				" FROM st_payroll " .
				" WHERE status = " . ACTIVE .
				"    	AND processed = 0 " .
				" ORDER BY id ASC" .
				" LIMIT 1";
		log_message("debug", "get_first_unprocessed <<< " . $sql);
		$query = $this->db->query($sql);
    	return $query->row();
    }
    
    /**
     * get first uprocessed payroll
     * @return payroll
     */
    function get_history_by_id($id){
    	$sql =  " SELECT * " .
				" FROM st_payroll " .
				" WHERE status = " . ACTIVE .
				"    	AND processed = 2 " .
				"		AND id = " . $id .
				" ORDER BY id DESC" .
				" LIMIT 1";
		$query = $this->db->query($sql);
    	return $query->row();
    }
}