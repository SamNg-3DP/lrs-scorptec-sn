<?php

class St_payroll_data_m extends MY_Model {

    function get_by_payroll($payroll_id){
    	$sql = "SELECT d.*, user.first_name, user.last_name  
    			FROM st_payroll_data AS d, st_users AS user
    			WHERE d.payroll_id = " . $payroll_id . "    			 
    				AND d.user_id = user.id " .	
    			" ORDER BY d.id ASC";
    	$query = $this->db->query($sql);
		return $query->result();
    }
    
    /*
    function get_process_by_payroll($payroll_id){
    	$sql = "SELECT d.*, user.first_name, user.last_name  
    			FROM st_payroll_data AS d, st_users AS user
    			WHERE d.payroll_id = " . $payroll_id . "    			 
    				AND d.user_id = user.id 
    				AND d.processed = 0 " .	
    			" ORDER BY d.id ASC";
    	$query = $this->db->query($sql);
		return $query->result();
    }
    */
    
    function get_process_by_payroll($payroll_id, $page_no=1, $limit=PAGE_SIZE){
    	$sql = "SELECT d.*, user.first_name, user.last_name  
    			FROM st_payroll_data AS d, st_users AS user
    			WHERE d.payroll_id = " . $payroll_id . "    			 
    				AND d.user_id = user.id 
    				AND d.processed = 0 " .	
    			" ORDER BY d.id ASC";
		if(empty($page_no)){
			$page_no = 1;
		}
		
		if(!empty($limit)){
			$start = ($page_no - 1) * $limit;
			$sql .= " LIMIT " . $start . ", " . $limit;
		}
		
    	$query = $this->db->query($sql);
		return $query->result();
    }
    
    function count_by_payroll($payroll_id){
    	$where = array(
    		'payroll_id' => $payroll_id
    	);
    	return $this->count_by($where);
    }
    
    function count_process_by_payroll($payroll_id){
    	$sql =  " SELECT count(id) AS num " .
				" FROM st_payroll_data " .
				" WHERE processed = 0 AND payroll_id = " . $payroll_id
				;

		$total_count = 0;
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0){
			$row = $query->row();
			$total_count = $row->num;
		}
		
		log_message("debug", "count_process_by_payroll <<< " . $sql);
		log_message("debug", "count_process_by_payroll total_count <<< " . $total_count);
        return $total_count;
    }
    
    function get_qb_by_payroll($payroll_id,$page_no=1, $limit=PAGE_SIZE){
    	$sql = "SELECT d.*, user.first_name, user.last_name  
    			FROM st_payroll_data AS d, st_users AS user
    			WHERE d.payroll_id = " . $payroll_id . "    			 
    				AND d.user_id = user.id 
    			    AND d.processed = 1 " .			
    			" ORDER BY d.id ASC";
    	
    	if(empty($page_no)){
			$page_no = 1;
		}
		
    	if(!empty($limit)){
			$start = ($page_no - 1) * $limit;
			$sql .= " LIMIT " . $start . ", " . $limit;
		}
		
    	$query = $this->db->query($sql);
		return $query->result();
    }
    
    function count_proccessed_by_payroll($payroll_id){
    	$where = array(
    		'payroll_id' => $payroll_id,
    		'processed'  => 1
    	);
    	return $this->count_by($where);
    }
    
    function count_qb_by_payroll($payroll_id){
    	$where = array(
    		'payroll_id' => $payroll_id,
    		'processed'  => 2
    	);
    	return $this->count_by($where);
    }
    
    function get_process_count_by_payroll($payroll_id){
    	$where = array(
    		'payroll_id' => $payroll_id,
    		'processed'  => 0
    	);
    	return $this->count_by($where);
    }
    
    function get_adjustment_by_payroll($payroll_id){
    	$sql = "SELECT d.*, user.first_name, user.last_name  
    			FROM st_payroll_data AS d, st_users AS user
    			WHERE d.payroll_id = " . $payroll_id . "    			 
    				AND d.user_id = user.id " .
    			" AND d.processed = 3 " .	
    			" ORDER BY d.id ASC";
    	$query = $this->db->query($sql);
		return $query->result();
    }
    
    function count_adjustment_by_payroll($payroll_id){
    	$sql =  " SELECT count(id) AS num " .
				" FROM st_payroll_data " .
				" WHERE processed = 3 AND payroll_id = " . $payroll_id
				;

		$total_count = 0;
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0){
			$row = $query->row();
			$total_count = $row->num;
		}
		
		log_message("debug", "count_adjustment_by_payroll <<< " . $sql);
		log_message("debug", "count_adjustment_by_payroll total_count <<< " . $total_count);
        return $total_count;
    }
    
    function delete_by_payroll($payroll_id){
		$this->db->delete($this->table, array('payroll_id' => $payroll_id));
    }
    
    /**
     * get last record by user
     * @param  int $user_id
     */
    function get_last_by_user($user_id){
    	$sql = " SELECT * " .
    			" FROM st_payroll_data AS d" .
    			" WHERE user_id = " . $user_id .
    			" ORDER BY payroll_id desc, id desc " .
    			" LIMIT 1 ";
    	log_message("debug", "get_last_by_user == " . $sql);
    	$query = $this->db->query($sql);
		return $query->row();
    }
    
    function get_by_payroll_user($payroll_id, $user_id){
    	$sql = " SELECT d.* " .
    			" FROM st_payroll_data AS d" .
    			" WHERE d.user_id = " . $user_id .
    			"  AND d.payroll_id = " . $payroll_id;
    			
    	log_message("debug", "get_by_payroll_user == " . $sql);
    	$query = $this->db->query($sql);
		return $query->row();
    }
    
    function process($id){
    	return $this->update($id, array('processed' => 1));
    }
    
    function undo_process($id){
    	return $this->update($id, array('processed' => 3));
    }
    
    function qb_exported($id){
    	return $this->update($id, array('processed' => 2));
    }
}