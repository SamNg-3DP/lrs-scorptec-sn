<?php
class St_user_labor_contract_m extends MY_Model {
	
	/**
     * set active status for specail labor
     * @param $labor_id
     * @return boolean
     */
    function set_active($labor_id)
    {
    	return $this->update($labor_id, 
    			array('status' => ACTIVE)
    	);
    }
    
    /**
     * set in-active status for specail labor
     * @param $labor_id
     * @return boolean
     */
	function set_inactive($labor_id)
	{
    	return $this->update($labor_id,
    			array('status' => IN_ACTIVE)
    	);
    }
	
	/**
	 * get active labor contrat list
	 * @return array
	 */
    function get_active($user_name) {
		$this->db
			->where('user_name', $user_name)
    		->where('status', ACTIVE)
    		->order_by('add_time', 'DESC');
		return $this->get_all();
	}
	
	/**
	 * get current labor contract by user name
	 * @param $user_name
	 * @return object
	 */
	function get_current($user_name){
		$sql = " SELECT *  
			FROM st_user_labor_contract 
			WHERE user_name = '" . $user_name . "'
				AND status = " . ACTIVE. "
				AND entry_date < ". now();
		$sql .= " ORDER BY entry_date DESC";
		$sql .= " LIMIT 1";
		
		$query = $this->db->query($sql);
    	return $query->row();
	}
}

/* End of file st_user_labor_contract_m.php */
/* Location: ./system/application/models/st_user_labor_contract_m.php */