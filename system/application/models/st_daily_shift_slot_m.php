<?php

/**
 * daily shift time slots
 */
class St_daily_shift_slot_m extends MY_Model {
	
	/**
	 * get time slot by daily shift id
	 * @param int $daily_shift_id
	 */
	function get_by_daily_shift($daily_shift_id){
		$sql =  " SELECT slot.*, cat.description as cat_name " .
				" FROM st_daily_shift_slot AS slot, st_shift_category AS cat " .
				" WHERE slot.daily_shift_id = " . $daily_shift_id .
				"     AND slot.status = " . ACTIVE .
				"     AND slot.shift_category_code = cat.code " .
				" ORDER BY slot.id ASC"
				;
		$query = $this->db->query($sql);
		return $query->result();		
	}
	
	/**
	 * delete all slot which daily shift id is $daily_shift_id
	 * @param int $daily_shift_id
	 */
	function delete_by_daily_shift($daily_shift_id){
		$where = array(
			'daily_shift_id' => $daily_shift_id
		);
		return $this->delete_by($where);
	}
    
}