<?php
class St_user_labor_m extends MY_Model {
	
	/**
     * set active status for specail labor
     * @param $labor_id
     * @return boolean
     */
    function set_active($labor_id)
    {
    	return $this->update($labor_id, 
    			array('status' => ACTIVE)
    	);
    }
    
    /**
     * set in-active status for specail labor
     * @param $labor_id
     * @return boolean
     */
	function set_inactive($labor_id)
	{
    	return $this->update($labor_id,
    			array('status' => IN_ACTIVE)
    	);
    }
	
	/**
	 * get active labor contrat list
	 * @return array
	 */
    function get_active($user_id) {
    	$sql = "SELECT labor.*, pay.id AS payrollshift_id, pay.payrollshiftcode " .
    			" FROM st_user_labor AS labor, st_payrollshift AS pay " .
    			" WHERE labor.payrollshift_id = pay.id AND labor.status = " . ACTIVE .
    			" AND labor.user_id = " . $user_id .
    			" ORDER BY labor.add_time DESC";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	/**
	 * get user labor before end time
	 * @param int user_id 
	 * @param int end_time
	 * @return user_labor
	 */
	function get_by_end_time($user_id, $end_time){
		$sql = "SELECT * FROM st_user_labor AS labor" .
				" WHERE labor.user_id = " . $user_id .
				" AND labor.entry_date < " . $end_time . 
				" AND labor.status = " . ACTIVE  .
				" ORDER BY labor.entry_date DESC " .
				" LIMIT 1"
		;
		$query = $this->db->query($sql);
		return $query->row();
	}
}

/* End of file st_user_labor_contract_m.php */
/* Location: ./system/application/models/st_user_labor_contract_m.php */