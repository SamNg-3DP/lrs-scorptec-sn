<?php

class St_payrollshift_m extends MY_Model {
	
	function add($payrollcode, $description, $notes, $paydays) {
        $this->db->query("INSERT INTO st_payrollshift VALUES('$payrollcode', '$description', '$notes', '$paydays');"); 
	}
	
	function get_payroll() {
		$query = $this->db->query('SELECT * FROM st_payrollshift ORDER BY payrollcode;');
		return $query->result();
	}
	
	function get_payrollshifts() {
		$sql = "SELECT pay.*, site.name AS site_name " .
				" FROM st_payrollshift AS pay, st_site AS site " .
				" WHERE pay.site_id = site.id " .
				" ORDER BY pay.payrollshiftcode";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function get_single_dailyshift($shiftcode) {
		$query = $this->db->query("SELECT * FROM dailyshift WHERE shiftcode = '$shiftcode';");
		return $query->result();
	}
	
	function delete2($payrollshiftcode) {
        $this->db->query("DELETE FROM st_payrollshift WHERE payrollshiftcode = '$payrollshiftcode';"); 
	}
	
	function update2($shiftcode, $description, $notes, $site_id, $starttime, $endtime, $shiftcategory_code, $rate) {
		$this->db->query("UPDATE dailyshift SET description = '$description', notes = '$notes', site_id = '$site_id', starttime = '$starttime', endtime = '$endtime', shiftcategory_code = '$shiftcategory_code', rate = '$rate' WHERE shiftcode = '$shiftcode'");
	}
	
	/**
	 * check whether payrollshiftcode is unique except the special id
	 * @param $payrollshiftcode
	 * @param $id
	 * @return int
	 */
	function check_except($payrollshiftcode, $id){
		$sql = "SELECT * FROM st_payrollshift 
			WHERE id != " . $id . "
			AND payrollshiftcode = '" . $payrollshiftcode ."'";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	
	function get_select_dropdown($site_id){
		$sql = "SELECT * FROM st_payrollshift 
			WHERE site_id = " . $site_id;
		$query = $this->db->query($sql);
		$payrollshifts = $query->result();
		
		$options[''] = 'Please select..';
		foreach ($payrollshifts as $row){
			$options[$row->id] = $row->payrollshiftcode;
		}
		return $options;
	}
	
	/**
     * get payrollshifts by site id
     * @param $site_id
     * @return array
     */
    function get_by_site($site_id){
    	$sql = "SELECT * FROM st_payrollshift 
			WHERE site_id = " . $site_id ."
			ORDER BY payrollshiftcode ASC";
		$query = $this->db->query($sql);
		return $query->result();
    }
}