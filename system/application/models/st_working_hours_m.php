<?php
class St_working_hours_m extends MY_Model {
	
	/**
     * set active status for specail working hours
     * @param $id
     * @return boolean
     */
    function set_active($id)
    {
    	return $this->update($id, 
    			array('status' => ACTIVE)
    	);
    }
    
    /**
     * set in-active status for specail working hours
     * @param $id
     * @return boolean
     */
	function set_inactive($id)
	{
    	return $this->update($id,
    			array('status' => IN_ACTIVE)
    	);
    }
    
	/**
     * get working hours object by user name
     * @param $user_name
     * @return user object
     */
    function get_by_user_name($user_name){
    	$sql = "SELECT work.*
				FROM  " . $this->table ." AS work
				WHERE work.status = " .ACTIVE. "
					AND work.user_name =  '".$user_name."'
				ORDER BY work.id desc
				"
			;
    	
    	log_message("debug", "get_by_user_name === ".$sql);
    	$query = $this->db->query($sql);
    	
		return $query->result();
    }
    
    /**
     * get working hours stat for staff
     * @param $user_name
     * @return object
     */
	function get_staff_working_hours($user_name){
    	$sql = "SELECT SUM(work.working_hours) AS work_count , 
    				SUM(work.annual_hours_used) AS annual_used_count, 
    				SUM(work.personal_hours_used) AS personal_used_count,
    				SUM(work.accrual_annual_hours) AS annual_hours_count,
    				SUM(work.accrual_personal_hours) AS personal_hours_count
				FROM  " . $this->table ." AS work
				WHERE work.status = " .ACTIVE. "
				AND work.user_name =  '".$user_name."'"
			;
    	
    	log_message("debug", "get_staff_working_hours === ".$sql);
    	$query = $this->db->query($sql);
    	
    	return $query->row();
    }
    
    /**
     * get the initialization for the user
     * @param $user_name
     * @return object
     */
    function get_init_working_hours($user_name){
    	$sql = "SELECT work.*
				FROM  " . $this->table ." AS work
				WHERE work.status = " .ACTIVE. "
					AND is_init = ".ACTIVE. "
					AND work.user_name =  '".$user_name."'";
    	$query = $this->db->query($sql);    	
    	return $query->row();
    }
}

/* End of file st_working_hours_m.php */
/* Location: ./system/application/models/st_working_hours_m.php */