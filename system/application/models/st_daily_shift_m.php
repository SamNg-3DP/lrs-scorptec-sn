<?php

class St_daily_shift_m extends MY_Model {

    function add($shiftcode, $description, $notes, $site_id, $starttime, $endtime, $shiftcategory_code, $rate) {
        $this->db->query("INSERT INTO st_daily_shift VALUES('$shiftcode', '$description', '$notes', '$site_id', '$starttime', '$endtime', '$shiftcategory_code', '$rate');");
    }

    function get_dailyshift() {
        $sql = 'SELECT                                     '.
			   '    dailyshift.id,                  '.               
			   '    dailyshift.shiftcode,                  '.
        	   '    dailyshift.lunch_hour,                '.
               '    dailyshift.description,                '.
               '    dailyshift.notes,                      '.
               '    site.`name`                           '.
               'FROM                                       '.
               '    st_daily_shift AS dailyshift, st_site AS site '.
               'WHERE                                      '.
               '    dailyshift.site_id = site.id           ';
               
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_single_dailyshift($shiftcode) {
        $query = $this->db->query("SELECT * FROM st_daily_shift WHERE shiftcode = '$shiftcode';");
        return $query->row();
    }

	/**
	 * get dropdown list
	 * @return array
	 */
	function get_select_dropdown($site_id){
		$sql = "SELECT * FROM st_daily_shift " .
				" WHERE site_id = " . $site_id .
				" ORDER BY shiftcode ";
		log_message("debug", $sql);		
		$query = $this->db->query($sql);
		$daily_shifts = $query->result();
		$a_ret = array();
		$a_ret[''] = 'Please select..';
		foreach ($daily_shifts as $row){
			$a_ret[$row->id] = $row->shiftcode;
		}
		return $a_ret;
	}
}