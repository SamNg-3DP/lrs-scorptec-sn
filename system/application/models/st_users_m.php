<?php

class St_users_m extends MY_Model {

	/**
	 * get users order by first name , last name
     * @return array
	 */
    function get_users($limit = PAGE_SIZE) {
    	$sql = 'SELECT user.*, department.department, userlevel.user_level, employment_type.employment_type
            FROM st_users user, st_department department, st_user_level userlevel, st_employment_type employment_type 
            WHERE user.department_id=department.id 
            	AND user.user_level_id=userlevel.id 
            	AND user.employment_type_id = employment_type.id 
            	AND user.status = ' . ACTIVE . ' 
            ORDER BY user.first_name, user.last_name ' .
            'LIMIT ' .$limit;
        $query = $this->db->query($sql);
        
        return $query->result();
    }

	/**
	 * get all users
	 */
	function get_all_users() {
    	$sql = 'SELECT user.*, department.department, userlevel.user_level, employment_type.employment_type
            FROM st_users user, st_department department, st_user_level userlevel, st_employment_type employment_type 
            WHERE user.department_id = department.id 
            	AND user.user_level_id = userlevel.id 
            	AND user.employment_type_id = employment_type.id 
            	AND user.hire_date > 0 
            	AND user.status = ' . ACTIVE . ' 
            ORDER BY user.id ';
        log_message("debug", "get_all_users == ".$sql);
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    /**
	 * get all users, not include Casual/Trial
	 * 
	 */
	function get_by_payroll($start_date){
    	$sql = 'SELECT user.*, department.department, userlevel.user_level, employment_type.employment_type
            FROM st_users user, st_department department, st_user_level userlevel, st_employment_type employment_type 
            WHERE user.department_id = department.id 
            	AND user.user_level_id = userlevel.id 
            	AND user.employment_type_id = employment_type.id 
            	AND user.hire_date <= ' . $start_date . ' 
            	AND ( 
            		( ( user.terminate_date > 0 AND user.terminate_date >  ' . $start_date . ' ) 
						OR ( user.terminate_date = 0 OR user.terminate_date IS NULL )
					)
            	)
            	AND user.employment_type_id	!= 3
            	AND user.status = ' . ACTIVE . '
            ORDER BY user.id ';
        log_message("debug", "get_by_payroll == ".$sql);
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    /**
     * get users order by different field
     * @param $order_by : field name
     * @param $order : ASC/DESC
     * @return array
     */
	function get_users_order_by($site_id=0, $department_id=0, $user_level_id=0, $employee_type=0, $status=ACTIVE, $order_by=NULL, $order="ASC", $page_no=1, $limit = PAGE_SIZE) {
		$sql = 'SELECT user.*, department.department, userlevel.user_level, employment_type.employment_type
            FROM st_users user, st_department department, st_user_level userlevel, st_employment_type employment_type 
            WHERE user.department_id=department.id 
            	AND user.user_level_id=userlevel.id 
            	AND user.employment_type_id = employment_type.id ';
		
		
		
		if(!empty($site_id) && $site_id > 0){
			$sql .= " AND department.site_id = " . $site_id;
		}
		
		if(!empty($department_id) && $department_id > 0){
			$sql .= " AND department.id = " . $department_id;
		}
		
		if($user_level_id > 0){
			$sql .= " AND userlevel.id = " . $user_level_id;
		}
		
		if(!empty($employee_type) && $employee_type > 0){
			$sql .= " AND user.employment_type_id = " . $employee_type;
		}
		
		if($status != ""){
			if($status == IN_ACTIVE){
				$sql .= " AND ( user.status = " . $status;
				$sql .= " OR (user.terminate_date > 0 AND user.terminate_date <  " . now() . " ) ) ";
			}else{
				$sql .= " AND ( user.status = " . $status;
				$sql .= " AND ( ( user.terminate_date > 0 AND user.terminate_date >  " . now() . " ) 
								 OR ( user.terminate_date = 0 OR user.terminate_date IS NULL )
							) ) ";
			}
		}
		
		if(empty($order_by)){
			$sql .= " ORDER BY user.first_name asc, user.last_name asc ";
		}else{
			if($order_by == 'department_id'){
				$sql .= " ORDER BY department.department " . $order ;
			}else if($order_by == 'user_level_id'){
				$sql .= " ORDER BY userlevel.user_level " . $order ;
			}else{
				$sql .= " ORDER BY user." . $order_by . " " . $order ;
			}
		}
		
		if(empty($page_no)){
			$page_no = 1;
		}
		
		if(!empty($limit)){
			$start = ($page_no - 1) * $limit;
			$sql .= " LIMIT " . $start . ", " . $limit;
		}
		
		log_message("debug", "get_users_order_by <<" . $sql);
		$query = $this->db->query($sql);
        return $query->result();
    }
    
	function count_order_by($site_id=0, $department_id=0, $user_level_id=0, $employee_type=0, $status=ACTIVE ) {
		$sql = 'SELECT count(user.id) AS num
            FROM st_users user, st_department department, st_user_level userlevel WHERE user.department_id=department.id AND user.user_level_id=userlevel.id ';
		
		if($status != ""){
			$sql .= " AND user.status = " . $status;
		}
		
		if(!empty($site_id) && $site_id > 0){
			$sql .= " AND department.site_id = " . $site_id;
		}
		
		if($department_id > 0){
			$sql .= " AND department.id = " . $department_id;
		}
		
		if($user_level_id > 0){
			$sql .= " AND userlevel.id = " . $user_level_id;
		}
		
		if(!empty($employee_type) && $employee_type > 0){
			$sql .= " AND user.employment_type_id = " . $employee_type;
		}
		
		$total_count = 0;
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0){
			$row = $query->row();
			$total_count = $row->num;
		}
		
        return $total_count;
    }
    
    function insert1($username, $password, $firstname, $lastname, $address, $homephone, $mobile, $email,
                    $birthday, $kinname, $contact, $medical, $driverno, $tfn, $accountname, $bankname,
                    $bsb, $accountno, $department_id, $userlevel_id, $usertype) {

        $this->db->query("INSERT INTO user(username, password, department_id, userlevel_id, usertype, firstname,
                lastname, address, homephone, mobile, email, birthday, kin_name, kin_contact, medical_conditions,
                driver_license_no, tfn, bank_accountname, bank_name, bank_bsb, bank_accountno)
                VALUES('$username', '$password', $department_id, $userlevel_id, '$usertype', '$firstname', '$lastname',
                    '$address', '$homephone', '$mobile', '$email', '$birthday', '$kinname', '$contact', '$medical', '$driverno',
                    '$tfn', '$accountname', '$bankname', '$bsb', '$accountno');");
    }

    function insert_workingtime($username, $week, $starttime, $endtime) {
        $this->db->query("INSERT INTO user_week_workingtime_map VALUES('$username', '$week', '$starttime', '$endtime');"); 
    }

    function insert_contact($username, $entrydate, $expiredate) {
        $this->db->query("INSERT INTO user_contact VALUES('$username', '$entrydate', '$expiredate');");
    }

    function delete_user($username) {
//        $this->db->query("DELETE FROM user WHERE username = '$username';");
//        $this->db->query("DELETE FROM user_week_workingtime_map WHERE username = '$username';");
//        $this->db->query("DELETE FROM user_contact WHERE username = '$username';");

    	$this->db->query("UPDATE st_users SET status=".IN_ACTIVE." WHERE user_name='".$username."'");
    	
    }
    
    function get_user($username, $password){
    	$sql = " SELECT * "
    		 . " FROM user "
    		 . " WHERE username = '$username';";
    }
    
    /**
     * get user by user name
     * @param $user_name
     * @return user object
     */
    function get_by_user_name($user_name){
    	$sql = 'SELECT user.*, department.department, department.site_id,userlevel.user_level, employment_type.employment_type 
            	FROM st_users user, st_department department, st_user_level userlevel, st_employment_type employment_type 
            	WHERE user.department_id=department.id 
            		AND user.user_level_id=userlevel.id 
            		AND user.employment_type_id=employment_type.id 
            		AND user.user_name = "'.$user_name.'"'; 
        $query = $this->db->query($sql);
        return $query->row();
    	//return parent::get_by("user_name",$user_name);
    }
    
    /**
     * get user info by user id
     * @param user_id
     * @return object user
     */
    function get_by_id($user_id){
    	$sql = 'SELECT user.*, department.department, department.site_id,userlevel.user_level, employment_type.employment_type 
            	FROM st_users user, st_department department, st_user_level userlevel, st_employment_type employment_type 
            	WHERE user.department_id=department.id 
            		AND user.user_level_id=userlevel.id 
            		AND user.employment_type_id=employment_type.id 
            		AND user.id = ' . $user_id; 
        $query = $this->db->query($sql);
        return $query->row();
    }
    
	function check_email($user_name){
    	return parent::get_by("user_name",$user_name);
    }
    
    /**
     * get all supervisors by user name
     * @param $user_name
     * @return array
     */
    function get_supervisors_by_user_name($user_name){
    	$query = $this->db->query("
    		SELECT * 
    		FROM `st_users` 
    		WHERE user_level_id = 2
    			AND department_id IN (
	    			SELECT department_id 
	    			FROM `st_users` 
	    			WHERE user_name = '".$user_name."'
	    		)
	    ");
        return $query->result();
    }
    
	/**
     * get all mamagers by user name
     * @param $user_name
     * @return array
     */
    function get_managers_by_user_name($user_name){
    	$query = $this->db->query("
    		SELECT * 
    		FROM `st_users` 
    		WHERE user_level_id IN (3,4)
	    ");
        return $query->result();
    }
    
	/**
	 * get user name dropdown list
	 * @return array
	 */
	function get_select_dropdown(){
		$this->db
    		->where('status', ACTIVE)
    		->order_by('user_name', 'ASC');
		$leave_types = $this->get_all();
		$a_ret = array();
		$a_ret[''] = 'Please select';
		foreach ($leave_types as $row){
			$a_ret[$row->id] = $row->user_name;
		}
		return $a_ret;
	}    
    
}