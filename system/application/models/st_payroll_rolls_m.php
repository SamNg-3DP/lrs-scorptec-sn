<?php

class St_payroll_rolls_m extends MY_Model {

    function get_by_payroll($payroll_id){
    	$sql = "SELECT roll.*, daily.description 
    			FROM st_payroll_rolls AS roll, st_daily_shift AS daily
    			WHERE roll.payroll_id = " . $payroll_id . "    			 
    				AND roll.daily_shift_id = daily.id " .
    			" ORDER BY roll.start ASC";
    	$query = $this->db->query($sql);
		return $query->result();
    }
}