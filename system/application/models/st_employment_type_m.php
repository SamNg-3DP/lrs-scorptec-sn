<?php

/**
 * employment type class
 * @author Bai Yinbing
 */
class St_employment_type_m extends MY_Model {
	
	/**
	 * get leave type dropdown list
	 * @return array
	 */
	function get_dropdown(){
		$this->db
    		->where('status', ACTIVE);
		$employment_types = $this->get_all();
		$a_ret = array();
		$a_ret[''] = 'All';
		foreach ($employment_types as $row){
			$a_ret[$row->id] = $row->employment_type;
		}
		return $a_ret;
	}

	function get_select_dropdown(){
		$this->db
    		->where('status', ACTIVE);
		$employment_types = $this->get_all();
		$a_ret = array();
		$a_ret[''] = 'Please select ...';
		foreach ($employment_types as $row){
			$a_ret[$row->id] = $row->employment_type;
		}
		return $a_ret;
	}
	
	/**
	 * get employment type by id
	 * @param $employment_type_id
	 * @return object
	 */
	function get_employment_type_by_id($employment_type_id){
		return parent::get_by($employment_type_id);
	}
	
    /**
     * set active status for specail leave type
     * @param $leave_type_id
     * @return boolean
     */
    function set_active($employment_type_id)
    {
    	return $this->update($employment_type_id, 
    			array('status' => ACTIVE)
    	);
    }
    
    /**
     * set in-active status for specail leave type
     * @param $leave_type_id
     * @return boolean
     */
	function set_inactive($employment_type_id)
	{
    	return $this->update($employment_type_id,
    			array('status' => IN_ACTIVE)
    	);
    }
}