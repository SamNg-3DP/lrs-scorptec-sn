<?php

/**
 * request leave model class
 * @author Bai Yinbing
 */
class St_request_m extends MY_Model {
    /**
     * set active status for specail request
     * @param $request_id
     * @return boolean
     */
    function set_active($request_id)
    {
    	return $this->update($request_id, 
    			array('status' => ACTIVE)
    	);
    }
    
    /**
     * set in-active status for specail request
     * @param $request_id
     * @return boolean
     */
	function set_inactive($request_id)
	{
    	return $this->update($request_id,
    			array('status' => IN_ACTIVE)
    	);
    }

    /**
     * get request related info
     * @param $request_id
     * @return object
     */
    function get_info($request_id){
    	$sql = " SELECT request.*, 
    	  		user.first_name, user.last_name, user.user_level_id, user.email, user.personal_email, user.id as user_id,
    	   		leave_type.leave_type 
    		FROM st_request AS request, st_users AS user, st_leave_type AS leave_type
    		WHERE request.id = " . $request_id . "
    			AND request.user_name = user.user_name    
    			AND request.leave_type_id = leave_type.id ";
    	return $this->db->query($sql)->row();
    }
    
    /**
     * get request by user name and request id
     * @param $user_name
     * @param $request_id
     * @return request object
     */
    function get_by_user_name_and_id($user_name, $request_id){
    	$array = array('id' => $request_id, 'user_name' => $user_name, 'status' => ACTIVE);
    	return $this->get_by($array);
    }

    /**
     * update user by name and request id
     * @param $user_name
     * @param $request_id
     * @param $arr
     * @return unknown_type
     */
    function update_by_user_name_and_id($user_name, $request_id, $arr){
    	$this->db->where('id', $request_id);
    	$this->db->where('user_name', $user_name);
		return $this->db->update($this->table, $arr);
    }
    
    /**
     * get stat by leave type start date , end date
     * @param $user_name
     * @param $start_time
     * @param $end_time
     * @param $leave_type_id
     * @return object
     */
	function get_stat_by_leave_type($user_name, $leave_type_id, $start_time=-1, $end_time=-1){
    	$sql = "SELECT COUNT( request.id ) AS daycount, sum(request.hours_used) AS timecount 
				FROM  `st_request` AS request
				WHERE request.status = ".ACTIVE."
				AND request.state = 40 
				AND request.user_name =  '".$user_name."'
				AND request.leave_type_id = ".$leave_type_id				
				;
		
    	if($start_time > 0){
    		$sql .= " AND request.start_date >= ".$start_time;
    	}
    	
		if($end_time > 0){
    		$sql .= " AND request.start_date < ".$end_time;
    	}
    	
    	log_message("debug", "get_stat_by_leave_type === ".$sql);
    	$query = $this->db->query($sql);
    	
    	return $query->row();
    }
    
    /**
     * get used count by leave type
     * @param $user_name
     * @param $leave_type_id
     * @return int
     */
    function get_used_count_by_leave_type($user_name, $leave_type_id, $upcoming=false, $no_mc=false, $start_date=false){
    	$sql = "SELECT sum(request.hours_used) AS used_count 
				FROM  `st_request` AS request
				WHERE request.status = ".ACTIVE."
				AND request.user_name =  '".$user_name."'
				AND request.leave_type_id = ".$leave_type_id."
    			AND request.state = 40 "
				;
				
		if ($leave_type_id == LEAVE_TYPE_COMPASIONATE || $no_mc==true){
	    	if ($no_mc){
	    		$sql .= " AND (request.mc_provided=0 or request.mc_provided is null)";
	    	}
	    	
	    	$sql .= " AND year(FROM_UNIXTIME(request.start_date)) = year(now())";
		}
		else{
			if (!$start_date) $start_date=now();
	    	if($upcoming){
	    		$sql .= "AND request.start_date >= " . $start_date;
	    	}else{
	    		$sql .= "AND request.start_date < " . $start_date;
	    	}
		}
    	
    	
    	log_message("debug", "get_used_count_by_leave_type === ".$sql);
    	$query = $this->db->query($sql);
    	return $query->row()->used_count;
    }
    
    /**
     * get upcoming count 
     * @param $user_name
     * @param $leave_type_id
     * @return int
     */
    function get_upcoming_count_by_leave_type($user_name, $leave_type_id){
    	return $this->get_used_count_by_leave_type($user_name, $leave_type_id, true);
    }
    
    /**
     * get hours used count by period time
     * @param string $user_name
     * @param int $leave_type_id
     * @param int $start_time
     * @param int $end_time
     */
    function get_used_count_by_leave_type_time($user_name, $leave_type_id, $start_time, $end_time){
    	$sql = "SELECT sum(request.hours_used) AS used_count 
				FROM  `st_request` AS request
				WHERE request.status = ".ACTIVE."
				AND request.user_name =  '".$user_name."'
				AND request.leave_type_id = ".$leave_type_id."
    			AND request.state = 40 "
				;
				
		$sql .= " AND request.start_date >= " . $start_time;
		$sql .= " AND request.start_date < " . $end_time;
		
    	log_message("debug", "get_used_count_by_leave_type_time === ".$sql);
    	$query = $this->db->query($sql);
    	return $query->row()->used_count;
    }
    
	/**
     * get all saved request by user name
     * @param $user_name
     * @param $leave_type_id
     * @param $period_start_time
     * @param $period_end_time
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return array
     */
    function get_saved_requests($user_name, $leave_type_id=0, $period_start_time=0, $period_end_time=0, $order_by='id', $order="ASC", $page_no=1, $limit = PAGE_SIZE){
		//return $this->get_by_state_user_name(10, $user_name);
    	return $this->get_requests_by_state(10, $user_name, $leave_type_id, $period_start_time, $period_end_time, $order_by, $order, $page_no, $limit);
    }
    
    /**
     * count all saved request by user name
     * @param $user_name
     * @param $leave_type_id
     * @param $period_start_time
     * @param $period_end_time
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return int
     */
    function count_saved_requests($user_name, $leave_type_id=0, $period_start_time=0, $period_end_time=0, $order_by='id', $order="ASC", $page_no=1, $limit = PAGE_SIZE){
		//return $this->get_by_state_user_name(10, $user_name);
    	return $this->count_requests_by_state(10, $user_name, $leave_type_id, $period_start_time, $period_end_time, $order_by, $order, $page_no, $limit);
    }
    
	/**
     * get all waiting request by user name
     * @param $user_name
     * @param $leave_type_id
     * @param $period_start_time
     * @param $period_end_time
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return array
     */
    function get_waiting_requests($user_name, $leave_type_id=0, $period_start_time=0, $period_end_time=0, $order_by='id', $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	return $this->get_requests_by_state(array(20, 30), $user_name, $leave_type_id, $period_start_time, $period_end_time, $order_by, $order, $page_no, $limit);
    }
    
    /**
     * count all waiting request by user name
     * @param $user_name
     * @param $leave_type_id
     * @param $period_start_time
     * @param $period_end_time
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return int
     */
    function count_waiting_requests($user_name, $leave_type_id=0, $period_start_time=0, $period_end_time=0, $order_by='id', $order="ASC", $page_no=1, $limit = PAGE_SIZE){
		//return $this->get_by_state_user_name(10, $user_name);
    	return $this->count_requests_by_state(array(20, 30), $user_name, $leave_type_id, $period_start_time, $period_end_time, $order_by, $order, $page_no, $limit);
    }
    
/**
     * get all rejected request by user name
     * @param $user_name
     * @param $leave_type_id
     * @param $period_start_time
     * @param $period_end_time
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return array
     */
    function get_rejected_requests($user_name, $leave_type_id=0, $period_start_time=0, $period_end_time=0, $order_by='id', $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	return $this->get_requests_by_state(50, $user_name, $leave_type_id, $period_start_time, $period_end_time, $order_by, $order, $page_no, $limit);
    }
    
    /**
     * count all rejected request by user name
     * @param $user_name
     * @param $leave_type_id
     * @param $period_start_time
     * @param $period_end_time
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return int
     */
    function count_rejected_requests($user_name, $leave_type_id=0, $period_start_time=0, $period_end_time=0, $order_by='id', $order="ASC", $page_no=1, $limit = PAGE_SIZE){
		//return $this->get_by_state_user_name(10, $user_name);
    	return $this->count_requests_by_state(50, $user_name, $leave_type_id, $period_start_time, $period_end_time, $order_by, $order, $page_no, $limit);
    }
    
/**
	 * get all approved requests which are upcoming by user name
	 * @param $user_name
	 * @param $period_start_time
	 * @param $period_end_time
	 * @param $order_by
	 * @param $order
	 * @param $page_no
	 * @param $limit
	 * @return array
	 */
    function upcoming_approved_requests($user_name, $period_start_time=0, $period_end_time=0, $order_by='id', $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	return $this->get_approved_requests_by_leave_type(TRUE, $user_name, 0, $period_start_time, $period_end_time, $order_by, $order, $page_no, $limit);
    }
    
    /**
     * count all approved requests which are upcoming by user name
     * @param $user_name
     * @param $leave_type_id
     * @param $period_start_time
     * @param $period_end_time
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return int
     */
    function count_upcoming_approved_requests($user_name, $period_start_time=0, $period_end_time=0){
    	return $this->count_approved_requests_by_leave_type(TRUE, $user_name, 0, $period_start_time, $period_end_time);
    }
    
	/**
	 * get all approved requests which leave type is annual leave
	 * @param $user_name
	 * @param $period_start_time
	 * @param $period_end_time
	 * @param $order_by
	 * @param $order
	 * @param $page_no
	 * @param $limit
	 * @return array
	 */
    function past_approved_requests_annual($user_name, $period_start_time=0, $period_end_time=0, $order_by='id', $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	return $this->get_approved_requests_by_leave_type(FALSE, $user_name, LEAVE_TYPE_ANNUAL, $period_start_time, $period_end_time, $order_by, $order, $page_no, $limit);
    }
    
    /**
     * count all saved request by user name
     * @param $user_name
     * @param $leave_type_id
     * @param $period_start_time
     * @param $period_end_time
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return int
     */
    function count_past_approved_requests_annual($user_name, $period_start_time=0, $period_end_time=0){
    	return $this->count_approved_requests_by_leave_type(FALSE, $user_name, LEAVE_TYPE_ANNUAL, $period_start_time, $period_end_time);
    }
    
	/**
	 * get all approved requests which leave type is annual leave by user name
	 * @param $user_name
	 * @param $period_start_time
	 * @param $period_end_time
	 * @param $order_by
	 * @param $order
	 * @param $page_no
	 * @param $limit
	 * @return array
	 */
    function past_approved_requests_personal($user_name, $period_start_time=0, $period_end_time=0, $order_by='id', $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	return $this->get_approved_requests_by_leave_type(FALSE, $user_name, LEAVE_TYPE_PERSONAL, $period_start_time, $period_end_time, $order_by, $order, $page_no, $limit);
    }
    
    /**
     * count all approved requests which leave type is personal leave by user name
     * @param $user_name
     * @param $leave_type_id
     * @param $period_start_time
     * @param $period_end_time
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return int
     */
    function count_past_approved_requests_personal($user_name, $period_start_time=0, $period_end_time=0){
    	return $this->count_approved_requests_by_leave_type(FALSE, $user_name, LEAVE_TYPE_PERSONAL, $period_start_time, $period_end_time);
    }
    
	/**
	 * get all approved requests which leave type is client visit leave by user name
	 * @param $user_name
	 * @param $period_start_time
	 * @param $period_end_time
	 * @param $order_by
	 * @param $order
	 * @param $page_no
	 * @param $limit
	 * @return array
	 */
    function past_approved_requests_client($user_name, $period_start_time=0, $period_end_time=0, $order_by='id', $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	return $this->get_approved_requests_by_leave_type(FALSE, $user_name, LEAVE_TYPE_CLIENTVISIT, $period_start_time, $period_end_time, $order_by, $order, $page_no, $limit);
    }
    
    /**
     * count all approved requests which leave type is client visit leave by user name
     * @param $user_name
     * @param $leave_type_id
     * @param $period_start_time
     * @param $period_end_time
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return int
     */
    function count_past_approved_requests_client($user_name, $period_start_time=0, $period_end_time=0){
    	return $this->count_approved_requests_by_leave_type(FALSE, $user_name, LEAVE_TYPE_CLIENTVISIT, $period_start_time, $period_end_time);
    }
    
	/**
	 * get all approved requests which leave type is over time leave by user name
	 * @param $user_name
	 * @param $period_start_time
	 * @param $period_end_time
	 * @param $order_by
	 * @param $order
	 * @param $page_no
	 * @param $limit
	 * @return array
	 */
    function past_approved_requests_overtime($user_name, $period_start_time=0, $period_end_time=0, $order_by='id', $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	return $this->get_approved_requests_by_leave_type(FALSE, $user_name, LEAVE_TYPE_OVERTIME, $period_start_time, $period_end_time, $order_by, $order, $page_no, $limit);
    }
    
    /**
     * count all approved requests which leave type is over time leave by user name
     * @param $user_name
     * @param $leave_type_id
     * @param $period_start_time
     * @param $period_end_time
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return int
     */
    function count_past_approved_requests_overtime($user_name, $period_start_time=0, $period_end_time=0){
    	return $this->count_approved_requests_by_leave_type(FALSE, $user_name, LEAVE_TYPE_OVERTIME, $period_start_time, $period_end_time);
    }
    
	/**
	 * get all approved requests which leave type is unpaid leave by user name
	 * @param $user_name
	 * @param $period_start_time
	 * @param $period_end_time
	 * @param $order_by
	 * @param $order
	 * @param $page_no
	 * @param $limit
	 * @return array
	 */
    function past_approved_requests_unpaid($user_name, $period_start_time=0, $period_end_time=0, $order_by='id', $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	return $this->get_approved_requests_by_leave_type(FALSE, $user_name, LEAVE_TYPE_UNPAID, $period_start_time, $period_end_time, $order_by, $order, $page_no, $limit);
    }
    
    /**
     * count all approved requests which leave type is unpaid leave by user name
     * @param $user_name
     * @param $leave_type_id
     * @param $period_start_time
     * @param $period_end_time
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return int
     */
    function count_past_approved_requests_unpaid($user_name, $period_start_time=0, $period_end_time=0){
    	return $this->count_approved_requests_by_leave_type(FALSE, $user_name, LEAVE_TYPE_UNPAID, $period_start_time, $period_end_time);
    }
    
	/**
	 * get all approved requests which leave type is late leave by user name
	 * @param $user_name
	 * @param $period_start_time
	 * @param $period_end_time
	 * @param $order_by
	 * @param $order
	 * @param $page_no
	 * @param $limit
	 * @return array
	 */
    function past_approved_requests_late($user_name, $period_start_time=0, $period_end_time=0, $order_by='id', $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	return $this->get_approved_requests_by_leave_type(FALSE, $user_name, LEAVE_TYPE_LATE, $period_start_time, $period_end_time, $order_by, $order, $page_no, $limit);
    }
    
    /**
     * count all approved requests which leave type is late leave by user name
     * @param $user_name
     * @param $leave_type_id
     * @param $period_start_time
     * @param $period_end_time
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return int
     */
    function count_past_approved_requests_late($user_name, $period_start_time=0, $period_end_time=0){
    	return $this->count_approved_requests_by_leave_type(FALSE, $user_name, LEAVE_TYPE_LATE, $period_start_time, $period_end_time);
    }
    
	/**
	 * get all approved requests which leave type is late leave by user name
	 * @param $user_name
	 * @param $period_start_time
	 * @param $period_end_time
	 * @param $order_by
	 * @param $order
	 * @param $page_no
	 * @param $limit
	 * @return array
	 */
    function past_approved_requests_lunch($user_name, $period_start_time=0, $period_end_time=0, $order_by='id', $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	return $this->get_approved_requests_by_leave_type(FALSE, $user_name, LEAVE_TYPE_LUNCH, $period_start_time, $period_end_time, $order_by, $order, $page_no, $limit);
    }
    
    /**
     * count all approved requests which leave type is late leave by user name
     * @param $user_name
     * @param $leave_type_id
     * @param $period_start_time
     * @param $period_end_time
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return int
     */
    function count_past_approved_requests_lunch($user_name, $period_start_time=0, $period_end_time=0){
    	return $this->count_approved_requests_by_leave_type(FALSE, $user_name, LEAVE_TYPE_LUNCH, $period_start_time, $period_end_time);
    }

	/**
	 * get all approved requests which leave type is late leave by user name
	 * @param $user_name
	 * @param $period_start_time
	 * @param $period_end_time
	 * @param $order_by
	 * @param $order
	 * @param $page_no
	 * @param $limit
	 * @return array
	 */
    function past_approved_requests_clock($user_name, $period_start_time=0, $period_end_time=0, $order_by='id', $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	return $this->get_approved_requests_by_leave_type(FALSE, $user_name, LEAVE_TYPE_CLOCK, $period_start_time, $period_end_time, $order_by, $order, $page_no, $limit);
    }
    
    /**
     * count all approved requests which leave type is late leave by user name
     * @param $user_name
     * @param $leave_type_id
     * @param $period_start_time
     * @param $period_end_time
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return int
     */
    function count_past_approved_requests_clock($user_name, $period_start_time=0, $period_end_time=0){
    	return $this->count_approved_requests_by_leave_type(FALSE, $user_name, LEAVE_TYPE_CLOCK, $period_start_time, $period_end_time);
    }        
    
	/**
	 * get all approved requests which leave type is training leave by user name
	 * @param $user_name
	 * @param $period_start_time
	 * @param $period_end_time
	 * @param $order_by
	 * @param $order
	 * @param $page_no
	 * @param $limit
	 * @return array
	 */
    function past_approved_requests_training($user_name, $period_start_time=0, $period_end_time=0, $order_by='id', $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	return $this->get_approved_requests_by_leave_type(FALSE, $user_name, LEAVE_TYPE_TRAINING, $period_start_time, $period_end_time, $order_by, $order, $page_no, $limit);
    }
    
    /**
     * count all approved requests which leave type is training leave by user name
     * @param $user_name
     * @param $leave_type_id
     * @param $period_start_time
     * @param $period_end_time
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return int
     */
    function count_past_approved_requests_training($user_name, $period_start_time=0, $period_end_time=0){
    	return $this->count_approved_requests_by_leave_type(FALSE, $user_name, LEAVE_TYPE_TRAINING, $period_start_time, $period_end_time);
    }
    
    
	/**
     * get all pending request by user name
     * @param $user_name
     * @return array
     */
    function get_pending_requests($user_name){
    	$state = array(20, 30);
    	return $this->get_by_state_user_name($state, $user_name);
    }
        
	/**
     * get all not approved request by user name
     * @param $user_name
     * @return array
     */
    function get_not_approved_requests($user_name){
    	return $this->get_by_state_user_name(50, $user_name);
    }
    
	/**
     * get requests by state and user name
     * @param $state
     * @param $user_name
     * @return array
     */
    function get_by_state_user_name($state, $user_name){
    	$sql = "SELECT request.*, leave_type.leave_type 
    			FROM st_request AS request , st_leave_type AS leave_type 
    			WHERE request.user_name = '".$user_name."' 
    				AND request.status = " . ACTIVE . "
    				AND leave_type.id = request.leave_type_id "; 
    	if(is_array($state)){
    		$array_length = count($state);
    		if($array_length > 0){
    			$str = "(";
    			for ($i = 0; $i < $array_length; $i++){
			    	if(($i + 1) < $array_length){
			    		$str .= $state[$i] . ",";
			    	}else{
			    		$str .= $state[$i];
			    	}
				}
				$str .= ")";
				$sql .= " AND request.state IN " . $str;
    		}
    	}else{
    		$sql .= " AND request.state = " . $state;
    	}
    				    				
    	$sql .=" ORDER BY request.id ASC";
    	
    	$sql .=" LIMIT " . PAGE_SIZE;
    	
    	$query = $this->db->query($sql);
		return $query->result();
    }
        
	/**
	 * get requests by state
	 * @param $state
	 * @param $user_name
	 * @param $leave_type_id
	 * @param $period_start_time
	 * @param $period_end_time
	 * @param $order_by
	 * @param $order
	 * @param $page_no
	 * @param $limit
	 * @return array
	 */
	function get_requests_by_state($state, $user_name, $leave_type_id=0, $period_start_time=0, $period_end_time=0, $order_by='id', $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	$sql = "SELECT request.*,leave_type.leave_type, user.user_name
    				, user.first_name, user.last_name
    				, department.department  
    			FROM st_request AS request , st_leave_type AS leave_type
    				, st_department AS department, st_users AS user 
    			WHERE request.status = " . ACTIVE . "
    				AND leave_type.id = request.leave_type_id
    				AND department.id = user.department_id  
    				AND request.user_name = user.user_name 
    				AND user.user_name = '" . $user_name . "'";
    	
		if(is_array($state)){
    		$array_length = count($state);
    		if($array_length > 0){
    			$str = "(";
    			for ($i = 0; $i < $array_length; $i++){
			    	if(($i + 1) < $array_length){
			    		$str .= $state[$i] . ",";
			    	}else{
			    		$str .= $state[$i];
			    	}
				}
				$str .= ")";
				$sql .= " AND request.state IN " . $str;
    		}
    	}else{
    		$sql .= " AND request.state = " . $state;
    	}
		
		if(!empty($leave_type_id) && $leave_type_id > 0){
			$sql .= " AND request.leave_type_id = " . $leave_type_id;
		}
		
		if(!empty($period_start_time) && $period_start_time > 0){
			$sql .= " AND request.start_date >= " . $period_start_time;
		}
		
		if(!empty($period_end_time) && $period_end_time > 0){
			$sql .= " AND request.start_date < " . $period_end_time;
		}
		
		if(empty($order_by)){
			$sql .= " ORDER BY request.id ASC";
		}else{	
			if($order_by == 'applicant'){
				$sql .= " ORDER BY user.first_name " . $order . ", user.last_name";
			}else if($order_by == 'leave_type_id'){
				$sql .= " ORDER BY leave_type.leave_type " . $order ;
			}else{
				$sql .= " ORDER BY request." . $order_by . " " . $order ;
			}
		}

		if(empty($page_no)){
			$page_no = 1;
		}
		
		if(!empty($limit)){
			$start = ($page_no - 1) * $limit;
					
			$sql .= " LIMIT " . $start . ", " . $limit;
		}
		log_message("debug", "get_requests_by_state sql <<< ".$sql);
		
    	$query = $this->db->query($sql);
    	return $query->result();
    }
    
    /**
     * stat the count of requests for each employee by state 
     * @param $state
     * @param $user_level
     * @param $department_id
     * @param $leave_type_id
     * @return int
     */
	function count_requests_by_state($state, $user_name, $leave_type_id=0, $period_start_time=0, $period_end_time=0){
    	$sql = "SELECT count(request.id) AS num  
    			FROM st_request AS request , st_leave_type AS leave_type
    				, st_department AS department, st_users AS user 
    			WHERE request.status = " . ACTIVE . "
    				AND leave_type.id = request.leave_type_id
    				AND department.id = user.department_id  
    				AND request.user_name = user.user_name 
    				AND user.user_name = '" . $user_name . "'";
		if(is_array($state)){
    		$array_length = count($state);
    		if($array_length > 0){
    			$str = "(";
    			for ($i = 0; $i < $array_length; $i++){
			    	if(($i + 1) < $array_length){
			    		$str .= $state[$i] . ",";
			    	}else{
			    		$str .= $state[$i];
			    	}
				}
				$str .= ")";
				$sql .= " AND request.state IN " . $str;
    		}
    	}else{
    		$sql .= " AND request.state = " . $state;
    	}
		
		if(!empty($leave_type_id) && $leave_type_id > 0){
			$sql .= " AND request.leave_type_id = " . $leave_type_id;
		}
		
		if(!empty($period_start_time) && $period_start_time > 0){
			$sql .= " AND request.start_date >= " . $period_start_time;
		}
		
		if(!empty($period_end_time) && $period_end_time > 0){
			$sql .= " AND request.start_date < " . $period_end_time;
		}

		$total_count = 0;
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0){
			$row = $query->row();
			$total_count = $row->num;
		}
		log_message("debug", "count_requests_by_state <<< " . $sql);
        return $total_count;
    }
    
    /**
     * get request count for each state
     * @param $user_name
     * @return array
     */
	function get_requests_counts($user_name){
    	$sql = "SELECT state, COUNT( * ) AS icount
				FROM  `st_request` AS request
				WHERE request.status = ".ACTIVE."
				AND request.user_name =  '".$user_name."'
				GROUP BY request.state
				";
    	$query = $this->db->query($sql);
    	return $query->result();
    }
  
    /**
     * get approved request which is filtered by leave type and whether start time is past or upcoming
     * @param $upcoming : TRUE/FALSE
     * @param $user_level
     * @param $department_id
     * @param $leave_type_id
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return array
     */
	function get_approved_requests_by_leave_type($upcoming, $user_name, $leave_type_id=0, $period_start_time=0, $period_end_time=0, $order_by='id', $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	$sql = "SELECT request.*,leave_type.leave_type, user.user_name
    				, user.first_name, user.last_name
    			FROM st_request AS request , st_leave_type AS leave_type ,st_users AS user 
    			WHERE request.status = " . ACTIVE . "
    				AND request.state = 40  
    				AND leave_type.id = request.leave_type_id
    				AND request.user_name = user.user_name 
    				AND user.user_name = '" . $user_name . "'";
    	
    	if($upcoming){
    		$sql .= " AND request.start_date > " . now();
    	}else{
    		$sql .= " AND request.start_date <= " . now();
    	}
    	
		if(!empty($period_start_time) && $period_start_time > 0){
			$sql .= " AND request.start_date >= " . $period_start_time;
		}
		
		if(!empty($period_end_time) && $period_end_time > 0){
			$sql .= " AND request.start_date < " . $period_end_time;
		}    	
    	
		if(is_array($leave_type_id)){
    		$array_length = count($leave_type_id);
    		if($array_length > 0){
    			$str = "(";
    			for ($i = 0; $i < $array_length; $i++){
			    	if(($i + 1) < $array_length){
			    		$str .= $leave_type_id[$i] . ",";
			    	}else{
			    		$str .= $leave_type_id[$i];
			    	}
				}
				$str .= ")";
				$sql .= " AND request.leave_type_id IN " . $str;
    		}
    	}else{
	    	if(!empty($leave_type_id) && $leave_type_id > 0){
				$sql .= " AND request.leave_type_id = " . $leave_type_id;
			}
    	}
		
		if(empty($order_by)){
			$sql .= " ORDER BY request.id ASC";
		}else{
			if($order_by == 'department_id'){
				$sql .= " ORDER BY department.department " . $order ;
			}else if($order_by == 'applicant'){
				$sql .= " ORDER BY user.first_name " . $order . ", user.last_name";
			}else if($order_by == 'leave_type_id'){
				$sql .= " ORDER BY leave_type.leave_type " . $order ;
			}else{
				$sql .= " ORDER BY request." . $order_by . " " . $order ;
			}
		}

		if(empty($page_no)){
			$page_no = 1;
		}
		
		if(!empty($limit)){
			$start = ($page_no - 1) * $limit;
			$sql .= " LIMIT " . $start . ", " . $limit;
		}
		log_message("debug", "get_approved_requests_by_leave_type <<< " . $sql);
    	$query = $this->db->query($sql);
    	return $query->result();
    }
    
    /**
     * count approved request which is filtered by leave type and whether start time is past or upcoming
     * @param $upcoming
     * @param $user_level
     * @param $department_id
     * @param $leave_type_id
     * @return int
     */
	function count_approved_requests_by_leave_type($upcoming, $user_name, $leave_type_id=0, $period_start_time=0, $period_end_time=0){
    	$sql = "SELECT count(request.id) AS num  
    			FROM st_request AS request, st_leave_type AS leave_type, st_users AS user 
    			WHERE request.status = " . ACTIVE . "
    				AND request.state = 40
    				AND leave_type.id = request.leave_type_id
    				AND request.user_name = user.user_name 
    				AND user.user_name = '" . $user_name . "'";
    	
		if($upcoming){
    		$sql .= " AND request.start_date > " . now();
    	}else{
    		$sql .= " AND request.start_date <= " . now();
    	}
		
		if(!empty($period_start_time) && $period_start_time > 0){
			$sql .= " AND request.start_date >= " . $period_start_time;
		}
		
		if(!empty($period_end_time) && $period_end_time > 0){
			$sql .= " AND request.start_date < " . $period_end_time;
		}
		
		if(is_array($leave_type_id)){
    		$array_length = count($leave_type_id);
    		if($array_length > 0){
    			$str = "(";
    			for ($i = 0; $i < $array_length; $i++){
			    	if(($i + 1) < $array_length){
			    		$str .= $leave_type_id[$i] . ",";
			    	}else{
			    		$str .= $leave_type_id[$i];
			    	}
				}
				$str .= ")";
				$sql .= " AND request.leave_type_id IN " . $str;
    		}
    	}else{
	    	if(!empty($leave_type_id) && $leave_type_id > 0){
				$sql .= " AND request.leave_type_id = " . $leave_type_id;
			}
    	}

		$total_count = 0;
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0){
			$row = $query->row();
			$total_count = $row->num;
		}
		
		log_message("debug", "count_approved_requests_by_leave_type <<< " . $sql);
		log_message("debug", "count_approved_requests_by_leave_type total_count <<< " . $total_count);
        return $total_count;
    }
    
    /**
     * get save approvals
     * @param $user_level
     * @param $department_id
     * @param $leave_type_id
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return array
     */
    function get_saved_approvals($user_level, $site_id=0, $department_id=0, $leave_type_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name="", $order_by='id', $order="ASC",  $page_no=1, $limit = PAGE_SIZE){
    	return $this->get_approvals_by_state(10, $user_level, $site_id, $department_id, $leave_type_id, $period_start_time, $period_end_time, $first_name, $last_name,$order_by, $order, $page_no, $limit);
    }
    
    /**
     * count by save approvals
     * @param $user_level
     * @param $department_id
     * @param $leave_type_id
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return int
     */
	function count_saved_approvals($user_level, $site_id=0, $department_id=0, $leave_type_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name=""){
    	return $this->count_approvals_by_state(10, $user_level, $site_id, $department_id, $leave_type_id, $period_start_time, $period_end_time, $first_name, $last_name);
    }
    
	/**
     * get not approved approvals
     * @param $user_level
     * @param $department_id
     * @param $leave_type_id
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return array
     */
    function get_not_approved_approvals($user_level, $site_id=0, $department_id=0, $leave_type_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name="", $order_by='id', $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	return $this->get_approvals_by_state(50, $user_level, $site_id, $department_id, $leave_type_id, $period_start_time, $period_end_time, $first_name, $last_name, $order_by, $order, $page_no, $limit);
    }
    
    /**
     * count by not approved approvals
     * @param $user_level
     * @param $department_id
     * @param $leave_type_id
     * @return unknown_type
     */
	function count_not_approved_approvals($user_level, $site_id=0, $department_id=0, $leave_type_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name=""){
    	return $this->count_approvals_by_state(50, $user_level, $site_id, $department_id, $leave_type_id, $period_start_time, $period_end_time, $first_name, $last_name);
    }
    
	/**
     * get not approved approvals
     * @param $user_level
     * @param $department_id
     * @param $leave_type_id
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return array
     */
    function get_waiting_approvals($user_level, $site_id=0, $department_id=0, $leave_type_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name="", $order_by='id', $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	$state = array(20, 30);
    	return $this->get_approvals_by_state($state, $user_level, $site_id, $department_id, $leave_type_id, $period_start_time, $period_end_time, $first_name, $last_name, $order_by, $order, $page_no, $limit);
    }
    
    /**
     * count by not approved approvals
     * @param $user_level
     * @param $department_id
     * @param $leave_type_id
     * @return array
     */
	function count_waiting_approvals($user_level, $site_id=0, $department_id=0, $leave_type_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name=""){
		$state = array(20, 30);
    	return $this->count_approvals_by_state($state, $user_level, $site_id, $department_id, $leave_type_id, $period_start_time, $period_end_time, $first_name, $last_name);
    }
    
	/**
	 * get all approved approvals
	 * @param $user_level
	 * @param $department_id
	 * @param $leave_type_id
	 * @param $order_by
	 * @param $order
	 * @param $page_no
	 * @param $limit
	 * @return array
	 */
    function get_approved_approvals($user_level, $site_id=0, $department_id=0, $leave_type_id=0, $order_by='id', $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	return $this->get_approvals_by_state(40, $user_level, $site_id, $department_id, $leave_type_id, $order_by, $order, $page_no, $limit);
    }
    
    
    
    /**
     * get aprrovals by request state
     * @param $state
     * @param $user_level
     * @param $site_id
     * @param $department_id
     * @param $leave_type_id
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return array
     */
	function get_approvals_by_state($state, $user_level, $site_id=0, $department_id=0, $leave_type_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name="", $order_by='id', $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	$sql = "SELECT request.*,leave_type.leave_type, user.user_name
    				, user.first_name, user.last_name
    				, department.department, site.name AS site_name  
    			FROM st_request AS request , st_leave_type AS leave_type
    				, st_department AS department, st_users AS user, st_site AS site 
    			WHERE request.status = " . ACTIVE . "
    				AND leave_type.id = request.leave_type_id
    				AND department.id = user.department_id  
    				AND request.user_name = user.user_name 
    				AND department.site_id = site.id
    			";
		if(is_array($state)){
    		$array_length = count($state);
    		if($array_length > 0){
    			$str = "(";
    			for ($i = 0; $i < $array_length; $i++){
			    	if(($i + 1) < $array_length){
			    		$str .= $state[$i] . ",";
			    	}else{
			    		$str .= $state[$i];
			    	}
				}
				$str .= ")";
				$sql .= " AND request.state IN " . $str;
    		}
    	}else{
    		$sql .= " AND request.state = " . $state;
    	}

    	if($user_level == 2){
    		$sql .= " AND user.user_level_id != 3 AND user.user_level_id != 4";
    	}

		if(!empty($site_id) && $site_id > 0){
			$sql .= " AND department.site_id = " . $site_id;
		}
		
    	if(!empty($department_id)){
			$sql .= " AND department.department = '" . $department_id . "'";
		}
		
		if(!empty($leave_type_id) && $leave_type_id > 0){
			$sql .= " AND request.leave_type_id = " . $leave_type_id;
		}
		
		if(!empty($period_start_time) && $period_start_time > 0){
			$sql .= " AND request.start_date >= " . $period_start_time;
		}
		
		if(!empty($period_end_time) && $period_end_time > 0){
			$sql .= " AND request.start_date < " . $period_end_time;
		}
		
		if(!empty($first_name)){
			$sql .= " AND user.first_name like '%" . $first_name . "%'";
		}
		
		if(!empty($last_name)){
			$sql .= " AND user.last_name like '%" . $last_name . "%'";
		}
		
		if(empty($order_by)){
			$sql .= " ORDER BY request.id ASC";
		}else{	
			if($order_by == 'department_id'){
				$sql .= " ORDER BY department.department " . $order ;
			}else if($order_by == 'applicant'){
				$sql .= " ORDER BY user.first_name " . $order . ", user.last_name";
			}else if($order_by == 'leave_type_id'){
				$sql .= " ORDER BY leave_type.leave_type " . $order ;
			}else{
				$sql .= " ORDER BY request." . $order_by . " " . $order ;
			}
		}

		if(empty($page_no)){
			$page_no = 1;
		}
		
		if(!empty($limit)){
			$start = ($page_no - 1) * $limit;
			$sql .= " LIMIT " . $start . ", " . $limit;
		}
		log_message("debug", "get_approvals_by_state === " . $sql);
		
    	$query = $this->db->query($sql);
    	return $query->result();
    }
    
    /**
     * stat the count of approvals by state 
     * @param $state
     * @param $user_level
     * @param $department_id
     * @param $leave_type_id
     * @return int
     */
	function count_approvals_by_state($state, $user_level, $site_id=0, $department_id=0, $leave_type_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name=""){
    	$sql = "SELECT count(request.id) AS num  
    			FROM st_request AS request , st_leave_type AS leave_type
    				, st_department AS department, st_users AS user, st_site AS site 
    			WHERE request.status = " . ACTIVE . "
    				AND leave_type.id = request.leave_type_id
    				AND department.id = user.department_id  
    				AND request.user_name = user.user_name 
    				AND department.site_id = site.id
    			";
    	//AND user.user_level_id != 4
		if(is_array($state)){
    		$array_length = count($state);
    		if($array_length > 0){
    			$str = "(";
    			for ($i = 0; $i < $array_length; $i++){
			    	if(($i + 1) < $array_length){
			    		$str .= $state[$i] . ",";
			    	}else{
			    		$str .= $state[$i];
			    	}
				}
				$str .= ")";
				$sql .= " AND request.state IN " . $str;
    		}
    	}else{
    		$sql .= " AND request.state = " . $state;
    	}
    	
    	if($user_level == 2){
    		$sql .= " AND (user.user_level_id != 3 )";
    	}
    	
		if(!empty($site_id) && $site_id > 0){
			$sql .= " AND department.site_id = " . $site_id;
		}
		
    	if(!empty($department_id)){
			$sql .= " AND department.department = '" . $department_id . "'";
		}
		
		if(!empty($leave_type_id) && $leave_type_id > 0){
			$sql .= " AND request.leave_type_id = " . $leave_type_id;
		}
		
		if(!empty($period_start_time) && $period_start_time > 0){
			$sql .= " AND request.start_date >= " . $period_start_time;
		}
		
		if(!empty($period_end_time) && $period_end_time > 0){
			$sql .= " AND request.start_date < " . $period_end_time;
		}

		if(!empty($first_name)){
			$sql .= " AND user.first_name like '%" . $first_name . "%'";
		}
		
		if(!empty($last_name)){
			$sql .= " AND user.last_name like '%" . $last_name . "%'";
		}
		log_message("debug", "count_approvals_by_state === " . $sql);
		
		$total_count = 0;
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0){
			$row = $query->row();
			$total_count = $row->num;
		}
		
        return $total_count;
    }
    
    
    
    /**
     * get past all approved leave reqeust by supervisor/manager
     * @param $user_level
     * @param $department_id
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return array
     */
    function past_approved_all($user_level, $site_id=0, $department_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name="", $order_by='id', $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	return $this->get_approved_approvals_by_leave_type(FALSE, $user_level, $site_id, $department_id, 0, $period_start_time, $period_end_time, $first_name, $last_name, $order_by, $order, $page_no, $limit);

    }
    
    /**
     * get count past all approved leave reqeust by supervisor/manager
     * @param $user_level
     * @param $department_id
     * @return int
     */
    function count_past_approved_all($user_level, $site_id=0, $department_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name=""){
    	return $this->count_approved_approvals_by_leave_type(FALSE, $user_level, $site_id, $department_id, 0, $period_start_time, $period_end_time, $first_name, $last_name);
    }    
    
    
    /**
     * get past annual approved leave reqeust by supervisor/manager
     * @param $user_level
     * @param $department_id
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return array
     */
    function past_approved_annual($user_level, $site_id=0, $department_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name="", $order_by='id', $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	return $this->get_approved_approvals_by_leave_type(FALSE, $user_level, $site_id, $department_id, LEAVE_TYPE_ANNUAL, $period_start_time, $period_end_time, $first_name, $last_name, $order_by, $order, $page_no, $limit);
    }
    
    /**
     * get count past annual approved leave reqeust by supervisor/manager
     * @param $user_level
     * @param $department_id
     * @return int
     */
    function count_past_approved_annual($user_level, $site_id=0, $department_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name=""){
    	return $this->count_approved_approvals_by_leave_type(FALSE, $user_level, $site_id, $department_id, LEAVE_TYPE_ANNUAL, $period_start_time, $period_end_time, $first_name, $last_name);
    }
    
    /**
     * get past unpaid approved leave reqeust by supervisor/manager
     * @param $user_level
     * @param $department_id
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return array
     */
    function past_approved_unpaid($user_level, $site_id=0, $department_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name="", $order_by="id", $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	return $this->get_approved_approvals_by_leave_type(FALSE, $user_level, $site_id, $department_id, LEAVE_TYPE_UNPAID, $period_start_time, $period_end_time, $first_name, $last_name, $order_by, $order, $page_no, $limit);
    }
    
    /**
     * get count past unpaid approved leave reqeust by supervisor/manager
     * @param $user_level
     * @param $department_id
     * @return int
     */
    function count_past_approved_unpaid($user_level, $site_id=0, $department_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name=""){
    	return $this->count_approved_approvals_by_leave_type(FALSE, $user_level, $site_id, $department_id, LEAVE_TYPE_UNPAID, $period_start_time, $period_end_time, $first_name, $last_name);
    }
        
    /**
     * get past personal approved leave reqeust by supervisor/manager
     * @param $user_level
     * @param $department_id
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return array
     */
    function past_approved_personal($user_level, $site_id=0, $department_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name="", $order_by="id", $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	return $this->get_approved_approvals_by_leave_type(FALSE, $user_level, $site_id, $department_id, LEAVE_TYPE_PERSONAL, $period_start_time, $period_end_time, $first_name, $last_name, $order_by, $order, $page_no, $limit);
    }
    
    /**
     * get count past personal approved leave reqeust by supervisor/manager
     * @param $user_level
     * @param $department_id
     * @return int
     */
    function count_past_approved_personal($user_level, $site_id=0, $department_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name=""){
    	return $this->count_approved_approvals_by_leave_type(FALSE, $user_level, $site_id, $department_id, LEAVE_TYPE_PERSONAL, $period_start_time, $period_end_time, $first_name, $last_name);
    }    
    /**
     * get past training approved leave reqeust by supervisor/manager
     * @param $user_level
     * @param $department_id
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return array
     */
    function past_approved_training($user_level, $site_id=0, $department_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name="", $order_by="id", $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	return $this->get_approved_approvals_by_leave_type(FALSE, $user_level, $site_id, $department_id, LEAVE_TYPE_TRAINING, $period_start_time, $period_end_time, $first_name, $last_name, $order_by, $order, $page_no, $limit);
    }

    /**
     * get count past training approved leave reqeust by supervisor/manager
     * @param $user_level
     * @param $department_id
     * @return int
     */
    function count_past_approved_training($user_level, $site_id=0, $department_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name=""){
    	return $this->count_approved_approvals_by_leave_type(FALSE, $user_level, $site_id, $department_id, LEAVE_TYPE_TRAINING, $period_start_time, $period_end_time, $first_name, $last_name);
    }
    
    /**
     * get past lateness approved leave reqeust by supervisor/manager
     * @param $user_level
     * @param $department_id
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return array
     */
    function past_approved_late($user_level, $site_id=0, $department_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name="", $order_by="id", $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	return $this->get_approved_approvals_by_leave_type(FALSE, $user_level, $site_id, $department_id, LEAVE_TYPE_LATE, $period_start_time, $period_end_time, $first_name, $last_name, $order_by, $order, $page_no, $limit);
    }    

    /**
     * get count past lateness approved leave reqeust by supervisor/manager
     * @param $user_level
     * @param $department_id
     * @return int
     */
    function count_past_approved_late($user_level, $site_id=0, $department_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name=""){
    	return $this->count_approved_approvals_by_leave_type(FALSE, $user_level, $site_id, $department_id, LEAVE_TYPE_LATE, $period_start_time, $period_end_time, $first_name, $last_name);
    }
    
    /**
     * get past long lunch approved leave reqeust by supervisor/manager
     * @param $user_level
     * @param $department_id
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return array
     */
    function past_approved_lunch($user_level, $site_id=0, $department_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name="", $order_by="id", $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	return $this->get_approved_approvals_by_leave_type(FALSE, $user_level, $site_id, $department_id, LEAVE_TYPE_LUNCH, $period_start_time, $period_end_time, $first_name, $last_name, $order_by, $order, $page_no, $limit);
    }    

    /**
     * get count past long lunch approved leave reqeust by supervisor/manager
     * @param $user_level
     * @param $department_id
     * @return int
     */
    function count_past_approved_lunch($user_level, $site_id=0, $department_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name=""){
    	return $this->count_approved_approvals_by_leave_type(FALSE, $user_level, $site_id, $department_id, LEAVE_TYPE_LUNCH, $period_start_time, $period_end_time, $first_name, $last_name);
    }
    
    
    
    
    
    /**
     * get past manual clock in/out approved leave reqeust by supervisor/manager
     * @param $user_level
     * @param $department_id
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return array
     */
    function past_approved_clock($user_level, $site_id=0, $department_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name="", $order_by="id", $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	return $this->get_approved_approvals_by_leave_type(FALSE, $user_level, $site_id, $department_id, LEAVE_TYPE_CLOCK, $period_start_time, $period_end_time, $first_name, $last_name, $order_by, $order, $page_no, $limit);
    }    

    /**
     * get count past manual clock in/out approved leave reqeust by supervisor/manager
     * @param $user_level
     * @param $department_id
     * @return int
     */
    function count_past_approved_clock($user_level, $site_id=0, $department_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name=""){
    	return $this->count_approved_approvals_by_leave_type(FALSE, $user_level, $site_id, $department_id, LEAVE_TYPE_CLOCK, $period_start_time, $period_end_time, $first_name, $last_name);
    }

    
    
    
    
    
    
    
    /**
     * get past client visit approved leave reqeust by supervisor/manager
     * @param $user_level
     * @param $department_id
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return array
     */
    function past_approved_client($user_level, $site_id=0, $department_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name="", $order_by="id", $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	return $this->get_approved_approvals_by_leave_type(FALSE, $user_level, $site_id, $department_id, LEAVE_TYPE_CLIENTVISIT, $period_start_time, $period_end_time, $first_name, $last_name, $order_by, $order, $page_no, $limit);
    }    

    /**
     * get count past client visit approved leave reqeust by supervisor/manager
     * @param $user_level
     * @param $department_id
     * @return int
     */
    function count_past_approved_client($user_level, $site_id=0, $department_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name=""){
    	return $this->count_approved_approvals_by_leave_type(FALSE, $user_level, $site_id, $department_id, LEAVE_TYPE_CLIENTVISIT, $period_start_time, $period_end_time, $first_name, $last_name);
    }

    /**
     * get past overtime approved leave reqeust by supervisor/manager
     * @param $user_level
     * @param $department_id
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return array
     */
    function past_approved_overtime($user_level, $site_id=0, $department_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name="", $order_by="id", $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	return $this->get_approved_approvals_by_leave_type(FALSE, $user_level, $site_id, $department_id, LEAVE_TYPE_OVERTIME, $period_start_time, $period_end_time, $first_name, $last_name, $order_by, $order, $page_no, $limit);
    }    

    /**
     * get count past overtime approved leave reqeust by supervisor/manager
     * @param $user_level
     * @param $department_id
     * @return int
     */
    function count_past_approved_overtime($user_level, $site_id=0, $department_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name=""){
    	return $this->count_approved_approvals_by_leave_type(FALSE, $user_level, $site_id, $department_id, LEAVE_TYPE_OVERTIME, $period_start_time, $period_end_time, $first_name, $last_name);
    }
        
    /**
     * get upcoming approved leave reqeust by supervisor/manager
     * @param $user_level
     * @param $department_id
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return array
     */
    function upcoming_approved($user_level, $site_id=0, $department_id=0, $leave_type=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name="", $order_by="id", $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	return $this->get_approved_approvals_by_leave_type(TRUE, $user_level, $site_id, $department_id, $leave_type, $period_start_time, $period_end_time, $first_name, $last_name, $order_by, $order, $page_no, $limit);
    }

    /**
     * get count upcoming approved leave reqeust by supervisor/manager
     * @param $user_level
     * @param $department_id
     * @return int
     */
    function count_upcoming_approved($user_level, $site_id=0, $department_id=0, $leave_type=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name=""){
    	return $this->count_approved_approvals_by_leave_type(TRUE, $user_level, $site_id, $department_id, $leave_type, $period_start_time, $period_end_time, $first_name, $last_name);
    }
    
    /**
     * get approved request which is filtered by leave type and whether start time is past or upcoming
     * @param $upcoming : TRUE/FALSE
     * @param $user_level
     * @param $department_id
     * @param $leave_type_id
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return array
     */
	function get_approved_approvals_by_leave_type($upcoming, $user_level, $site_id=0, $department_id=0, $leave_type_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name="", $order_by='id', $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	$sql = "SELECT request.*,leave_type.leave_type, user.user_name
    				, user.first_name, user.last_name
    				, department.department  
    			FROM st_request AS request , st_leave_type AS leave_type
    				, st_department AS department, st_users AS user, st_site AS site 
    			WHERE request.status = " . ACTIVE . "
    				AND request.state = 40  
    				AND leave_type.id = request.leave_type_id
    				AND department.id = user.department_id  
    				AND request.user_name = user.user_name 
    				AND department.site_id = site.id
    			";
    	if($upcoming){
    		$sql .= " AND request.start_date > " . now();
    	}else{
    		$sql .= " AND request.start_date <= " . now();
    	}
    	
		if(!empty($period_start_time) && $period_start_time > 0){
			$sql .= " AND request.start_date >= " . $period_start_time;
		}
		
		if(!empty($period_end_time) && $period_end_time > 0){
			$sql .= " AND request.start_date < " . $period_end_time;
		}    	
    	
    	if($user_level == 2){
    		$sql .= " AND user.user_level_id <= 2 ";
    	}

		if(!empty($site_id) && $site_id > 0){
			$sql .= " AND department.site_id = " . $site_id;
		}
		
    	if(!empty($department_id)){
			$sql .= " AND department.department = '" . $department_id . "'";
		}
		
		if(!empty($leave_type_id) && $leave_type_id > 0){
			$sql .= " AND request.leave_type_id = " . $leave_type_id;
		}
		
		if(!empty($first_name)){
			$sql .= " AND user.first_name like '%" . $first_name . "%'";
		}
		
		if(!empty($last_name)){
			$sql .= " AND user.last_name like '%" . $last_name . "%'";
		}
		
		if(empty($order_by)){
			$sql .= " ORDER BY request.id ASC";
		}else{
			if($order_by == 'department_id'){
				$sql .= " ORDER BY department.department " . $order ;
			}else if($order_by == 'applicant'){
				$sql .= " ORDER BY user.first_name " . $order . ", user.last_name";
			}else if($order_by == 'leave_type_id'){
				$sql .= " ORDER BY leave_type.leave_type " . $order ;
			}else{
				$sql .= " ORDER BY request." . $order_by . " " . $order ;
			}
		}

		if(empty($page_no)){
			$page_no = 1;
		}
		
		if(!empty($limit)){
			$start = ($page_no - 1) * $limit;
			$sql .= " LIMIT " . $start . ", " . $limit;
		}
		log_message("debug", "get_approved_approvals_by_leave_type <<< " . $sql);
    	$query = $this->db->query($sql);
    	return $query->result();
    }
    
    /**
     * count approved request which is filtered by leave type and whether start time is past or upcoming
     * @param $upcoming
     * @param $user_level
     * @param $department_id
     * @param $leave_type_id
     * @return int
     */
	function count_approved_approvals_by_leave_type($upcoming, $user_level, $site_id=0, $department_id=0, $leave_type_id=0, $period_start_time=0, $period_end_time=0, $first_name="", $last_name=""){
    	$sql = "SELECT count(request.id) AS num  
    			FROM st_request AS request , st_leave_type AS leave_type
    				, st_department AS department, st_users AS user, st_site AS site  
    			WHERE request.status = " . ACTIVE . "
    				AND request.state = 40
    				AND leave_type.id = request.leave_type_id
    				AND department.id = user.department_id  
    				AND request.user_name = user.user_name 
    				AND department.site_id = site.id
    			";
    	
		if($upcoming){
    		$sql .= " AND request.start_date > " . now();
    	}else{
    		$sql .= " AND request.start_date <= " . now();
    	}
		
		if(!empty($period_start_time) && $period_start_time > 0){
			$sql .= " AND request.start_date >= " . $period_start_time;
		}
		
		if(!empty($period_end_time) && $period_end_time > 0){
			$sql .= " AND request.start_date < " . $period_end_time;
		}
		
    	if($user_level == 2){
    		$sql .= " AND user.user_level_id <= 2 ";
    	}

		if(!empty($site_id) && $site_id > 0){
			$sql .= " AND department.site_id = " . $site_id;
		}
		
    	if(!empty($department_id)){
			$sql .= " AND department.department = '" . $department_id . "'";
		}
		
		if(!empty($leave_type_id) && $leave_type_id > 0){
			$sql .= " AND request.leave_type_id = " . $leave_type_id;
		}
		
		if(!empty($first_name)){
			$sql .= " AND user.first_name like '%" . $first_name . "%'";
		}
		
		if(!empty($last_name)){
			$sql .= " AND user.last_name like '%" . $last_name . "%'";
		}
		
		$total_count = 0;
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0){
			$row = $query->row();
			$total_count = $row->num;
		}
		
		log_message("debug", "count_approved_approvals_by_leave_type <<< " . $sql);
		log_message("debug", "count_approved_approvals_by_leave_type total_count <<< " . $total_count);
        return $total_count;
    }
    
    /**
     * get colleague approved leave request
     * @param $leave_type_id
     * @param $start_time
     * @param $end_time
     * @param $department_id
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return array
     */
	function get_colleague_approved_by_leave_type($leave_type_id, $start_time, $end_time, $site_id=0, $department_id=0, $order_by='id', $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	$sql = "SELECT request.*,leave_type.leave_type, user.user_name
    				, user.first_name, user.last_name
    				, department.department  
    			FROM st_request AS request , st_leave_type AS leave_type
    				, st_department AS department, st_users AS user 
    			WHERE request.status = " . ACTIVE . "
    				AND request.state = 40  
    				AND leave_type.id = request.leave_type_id
    				AND department.id = user.department_id  
    				AND request.user_name = user.user_name 
    				AND user.user_level_id != 4
    				AND ( 
					   (request.start_date <= " . $start_time . " AND request.end_date >= " . $start_time . " ) 
					OR (request.start_date >= " . $start_time . " AND request.end_date <= " . $end_time . " )
					OR (request.start_date <= " . $end_time .   " AND request.end_date >= " . $end_time . " )
					)
    			";
		if(!empty($site_id) && $site_id > 0){
			$sql .= " AND department.site_id = " . $site_id;
		}
			
    	if(!empty($department_id) && $department_id > 0){
			$sql .= " AND department.id = " . $department_id;
		}
		
		if(is_array($leave_type_id)){
    		$array_length = count($leave_type_id);
    		if($array_length > 0){
    			$str = "(";
    			for ($i = 0; $i < $array_length; $i++){
			    	if(($i + 1) < $array_length){
			    		$str .= $leave_type_id[$i] . ",";
			    	}else{
			    		$str .= $leave_type_id[$i];
			    	}
				}
				$str .= ")";
				$sql .= " AND request.leave_type_id IN " . $str;
    		}
    	}else{
	    	if(!empty($leave_type_id) && $leave_type_id > 0){
				$sql .= " AND request.leave_type_id = " . $leave_type_id;
			}
    	}
		
		if(empty($order_by)){
			$sql .= " ORDER BY request.id ASC";
		}else{
			if($order_by == 'department_id'){
				$sql .= " ORDER BY department.department " . $order ;
			}else if($order_by == 'applicant'){
				$sql .= " ORDER BY user.first_name " . $order . ", user.last_name";
			}else if($order_by == 'leave_type_id'){
				$sql .= " ORDER BY leave_type.leave_type " . $order ;
			}else{
				$sql .= " ORDER BY request." . $order_by . " " . $order ;
			}
		}

		if(empty($page_no)){
			$page_no = 1;
		}
		
		$start = ($page_no - 1) * $limit;
				
		if ($limit)
			$sql .= " LIMIT " . $start . ", " . $limit;
		
		log_message("debug", "get_colleague_approved_by_leave_type <<< " . $sql);
    	$query = $this->db->query($sql);
    	return $query->result();
    }
    
    /**
     * get count of colleague approved leave requestes 
     * @param $leave_type_id
     * @param $start_time
     * @param $end_time
     * @param $department_id
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return int
     */
	function count_colleague_approved_by_leave_type($leave_type_id, $start_time, $end_time, $site_id=0, $department_id=0, $order_by='id', $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	$sql = "SELECT count(request.id) AS num  
    			FROM st_request AS request , st_leave_type AS leave_type
    				, st_department AS department, st_users AS user 
    			WHERE request.status = " . ACTIVE . "
    				AND request.state = 40  
    				AND leave_type.id = request.leave_type_id
    				AND department.id = user.department_id  
    				AND request.user_name = user.user_name 
    				AND ( 
					   (request.start_date <= " . $start_time . " AND request.end_date >= " . $start_time . " ) 
					OR (request.start_date >= " . $start_time . " AND request.end_date <= " . $end_time . " )
					OR (request.start_date <= " . $end_time .   " AND request.end_date >= " . $end_time . " )
					)			
    			";
    			
		if(is_array($leave_type_id)){
    		$array_length = count($leave_type_id);
    		if($array_length > 0){
    			$str = "(";
    			for ($i = 0; $i < $array_length; $i++){
			    	if(($i + 1) < $array_length){
			    		$str .= $leave_type_id[$i] . ",";
			    	}else{
			    		$str .= $leave_type_id[$i];
			    	}
				}
				$str .= ")";
				$sql .= " AND request.leave_type_id IN " . $str;
    		}
    	}else{
	    	if(!empty($leave_type_id) && $leave_type_id > 0){
				$sql .= " AND request.leave_type_id = " . $leave_type_id;
			}
    	}
		
		if(!empty($site_id) && $site_id > 0){
			$sql .= " AND department.site_id = " . $site_id;
		}
		
		if(!empty($department_id) && $department_id > 0){
			$sql .= " AND department.id = " . $department_id;
		}
		
		$total_count = 0;
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0){
			$row = $query->row();
			$total_count = $row->num;
		}
		
		log_message("debug", "count_colleague_approved_by_leave_type <<< " . $sql);
		log_message("debug", "count_colleague_approved_by_leave_type total_count <<< " . $total_count);
        return $total_count;
    }
    
    /**
     * get approvals count for each state
     * @param $user_name
     * @return array
     */
	function get_approvals_counts($user_name, $user_level ){
		$sql = "SELECT request.state, COUNT( * ) AS icount
    			FROM st_request AS request , st_leave_type AS leave_type, st_department AS department, st_users AS user 
    			WHERE request.status = " . ACTIVE . "
    				AND leave_type.id = request.leave_type_id
    				AND department.id = user.department_id 
    				AND request.user_name = user.user_name
				";
		
		if($user_level == 2){
			$sql .=	" AND (user.user_level_id != 3 AND user.user_level_id != 4) ";
			$sql .=	" AND user.department_id IN (
    					SELECT department_id 
    					FROM st_users 
    					WHERE user_name = '" . $user_name . "' 
    						AND user_level_id = 2 
    				)";
		}
    	$sql .=	" GROUP BY state ";
    	log_message("debug", "get_approvals_counts == " .$sql);
    	$query = $this->db->query($sql);
    	return $query->result();
    }
    
    
	function get_approvals_by_state_user_name($state, $user_name){
    	$sql = "SELECT request.*,leave_type.leave_type, user.user_name
    				, user.first_name, user.last_name
    				, department.department  
    			FROM st_request AS request , st_leave_type AS leave_type, st_department AS department, st_users AS user 
    			WHERE request.state = ".$state." 
    				AND request.status = " . ACTIVE . "
    				AND leave_type.id = request.leave_type_id
    				AND department.id = user.department_id 
    				AND request.user_name = user.user_name
    				AND (user.user_level_id != 3 AND user.user_level_id != 4)  
    				AND user.department_id IN (
    					SELECT department_id 
    					FROM st_users 
    					WHERE user_name = '" . $user_name . "' 
    						AND user_level_id = 2
    				)
    			ORDER BY request.id ASC
    			";
    	$query = $this->db->query($sql);
		return $query->result();
    }
    
	/**
     * get past approved count for each state before now
     * @param $department_id
     * @return array
     */
	function get_past_approved_counts($department_id ){
		$sql = "SELECT leave_type.id AS id, COUNT( * ) AS icount
    			FROM st_request AS request , st_leave_type AS leave_type, st_department AS department, st_users AS user 
    			WHERE request.status = " . ACTIVE . "
    				AND leave_type.id = request.leave_type_id
    				AND department.id = user.department_id 
    				AND request.user_name = user.user_name
    				AND request.state = 40 
				";
		$sql .= " AND request.start_date <= " . now();
		if($department_id > 0){
			$sql .=	" AND (user.user_level_id != 3 AND user.user_level_id != 4) ";
			$sql .=	" AND user.department_id = " . $department_id;
		}
		
    	$sql .=	" GROUP BY id ";
    	
    	log_message("debug", "get_approved_counts sql <<< ". $sql);
    	$query = $this->db->query($sql);
    	return $query->result();
    }
    
    /**
     * get request count for each leave type for each user
     * @param $user_name
     * @return array
     */
	function get_past_approved_requests_counts($user_name){
    	$sql = "SELECT request.leave_type_id, COUNT( * ) AS icount
				FROM  `st_request` AS request 
				WHERE request.status = ".ACTIVE."
				AND request.state = 40 
				AND request.user_name =  '".$user_name."'
				AND request.start_date < " . now()."
				GROUP BY request.leave_type_id
				";
    	$query = $this->db->query($sql);
    	return $query->result();
    }
    
    /**
     * get search request by any search condition
     * @param $department_id
     * @param $user_name
     * @param $leave_type_id
     * @param $state
     * @param $period_start_time
     * @param $period_end_time
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return unknown_type
     */
	function get_requests_by_search($site_id=0, $department_id=0, $first_name ="", $last_name ="",$leave_type_id=0, $state=0,  $period_start_time=0, $period_end_time=0, $order_by='id', $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	$sql = "SELECT request.*,leave_type.leave_type, user.user_name
    				, user.first_name, user.last_name
    				, department.department  
    			FROM st_request AS request , st_leave_type AS leave_type
    				, st_department AS department, st_users AS user, st_site AS site 
    			WHERE request.status = " . ACTIVE . "
    				AND leave_type.id = request.leave_type_id
    				AND department.id = user.department_id  
    				AND request.user_name = user.user_name
    				AND department.site_id = site.id ";
    				
		if(!empty($site_id) && $site_id > 0){
			$sql .= " AND department.site_id = " . $site_id;
		}
		
		 if(!empty($department_id)){
			$sql .= " AND department.department = '" . $department_id . "'";
		}
		
		if(!empty($first_name)){
			$sql .= " AND user.first_name like '%" . $first_name. "%'";
		}
		
		if(!empty($last_name)){
			$sql .= " AND user.last_name like '%" . $last_name. "%'";
		}
		
		if(!empty($leave_type_id) && $leave_type_id > 0){
			$sql .= " AND request.leave_type_id = " . $leave_type_id;
		}
		
		if(is_array($state)){
    		$array_length = count($state);
    		if($array_length > 0){
    			$str = "(";
    			for ($i = 0; $i < $array_length; $i++){
			    	if(($i + 1) < $array_length){
			    		$str .= $state[$i] . ",";
			    	}else{
			    		$str .= $state[$i];
			    	}
				}
				$str .= ")";
				$sql .= " AND request.state IN " . $str;
    		}
    	}else{
    		if(!empty($state) && $state > 0){
				$sql .= " AND request.state = " . $state;
			}
    	}
		
		if(!empty($period_start_time) && $period_start_time > 0){
			$sql .= " AND request.start_date >= " . $period_start_time;
		}
		
		if(!empty($period_end_time) && $period_end_time > 0){
			$sql .= " AND request.start_date < " . $period_end_time;
		}
		
		if(empty($order_by)){
			$sql .= " ORDER BY request.id ASC";
		}else{	
			if($order_by == 'applicant'){
				$sql .= " ORDER BY user.first_name " . $order . ", user.last_name";
			}else if($order_by == 'leave_type_id'){
				$sql .= " ORDER BY leave_type.leave_type " . $order ;
			}else{
				$sql .= " ORDER BY request." . $order_by . " " . $order ;
			}
		}

		if(empty($page_no)){
			$page_no = 1;
		}
		
		$start = ($page_no - 1) * $limit;
				
		$sql .= " LIMIT " . $start . ", " . $limit;
		
		log_message("debug", "get_requests_by_search sql <<< ".$sql);
		
    	$query = $this->db->query($sql);
    	return $query->result();
    }
    
    /**
     * search all condition request count
     * @param $department_id
     * @param $user_name
     * @param $leave_type_id
     * @param $state
     * @param $period_start_time
     * @param $period_end_time
     * @param $order_by
     * @param $order
     * @param $page_no
     * @param $limit
     * @return unknown_type
     */
	function count_requests_by_search($site_id=0, $department_id=0, $first_name ="", $last_name ="", $leave_type_id=0, $state=0,  $period_start_time=0, $period_end_time=0, $order_by='id', $order="ASC", $page_no=1, $limit = PAGE_SIZE){
    	$sql = "SELECT count(request.id) AS num  
    			FROM st_request AS request , st_leave_type AS leave_type
    				, st_department AS department, st_users AS user, st_site AS site 
    			WHERE request.status = " . ACTIVE . "
    				AND leave_type.id = request.leave_type_id
    				AND department.id = user.department_id  
    				AND request.user_name = user.user_name 
    				AND department.site_id = site.id";
		if(!empty($site_id) && $site_id > 0){
			$sql .= " AND department.site_id = " . $site_id;
		}
		
		if(!empty($department_id)){
			$sql .= " AND department.department = '" . $department_id . "'";
		}
		
		if(!empty($first_name)){
			$sql .= " AND user.first_name like '%" . $first_name. "%'";
		}
		
		if(!empty($last_name)){
			$sql .= " AND user.last_name like '%" . $last_name. "%'";
		}
		
		if(!empty($leave_type_id) && $leave_type_id > 0){
			$sql .= " AND request.leave_type_id = " . $leave_type_id;
		}
		
		if(is_array($state)){
    		$array_length = count($state);
    		if($array_length > 0){
    			$str = "(";
    			for ($i = 0; $i < $array_length; $i++){
			    	if(($i + 1) < $array_length){
			    		$str .= $state[$i] . ",";
			    	}else{
			    		$str .= $state[$i];
			    	}
				}
				$str .= ")";
				$sql .= " AND request.state IN " . $str;
    		}
    	}else{
    		if(!empty($state) && $state > 0){
				$sql .= " AND request.state = " . $state;
			}
    	}
		
		if(!empty($period_start_time) && $period_start_time > 0){
			$sql .= " AND request.start_date >= " . $period_start_time;
		}
		
		if(!empty($period_end_time) && $period_end_time > 0){
			$sql .= " AND request.start_date < " . $period_end_time;
		}

		$total_count = 0;
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0){
			$row = $query->row();
			$total_count = $row->num;
		}
		log_message("debug", "count_requests_by_search <<< " . $sql);
        return $total_count;
    }
    
	function get_requests_by_calendar($site_id,$department_id, $period_start_time=0, $period_end_time=0, $leave_type_id=0, $state=40,  $order_by='id', $order="ASC"){
    	/*
    	 $sql = "SELECT request.*,leave_type.leave_type, user.user_name
    				, user.first_name, user.last_name
    				, department.department  
    			FROM st_request AS request , st_leave_type AS leave_type
    				, st_department AS department, st_users AS user 
    			WHERE request.status = " . ACTIVE . "
    				AND leave_type.id = request.leave_type_id
    				AND department.id = user.department_id  
    				AND request.user_name = user.user_name";
    				*/
    	$sql = "SELECT request.*,leave_type.leave_type, user.user_name
    				, user.first_name, user.last_name
    				, department.department, manager.first_name AS mfirst_name, manager.last_name AS mlast_name 
    			FROM st_request AS request 
                    LEFT JOIN st_leave_type AS leave_type ON leave_type.id = request.leave_type_id 
                    LEFT JOIN st_users AS user ON request.user_name = user.user_name
                    LEFT JOIN st_department AS department ON department.id = user.department_id  
                    LEFT JOIN st_users AS manager ON request.manager_user_name = manager.user_name " .
              " WHERE request.status = " . ACTIVE ;

		if(!empty($site_id) && $site_id > 0){
			$sql .= " AND department.site_id = " . $site_id;
		}
		
		if(!empty($department_id)){
			$sql .= " AND department.department = '" . $department_id ."'";
		}
		
		if(!empty($leave_type_id) && $leave_type_id > 0){
			$sql .= " AND request.leave_type_id = " . $leave_type_id;
		}
		
		if(is_array($state)){
    		$array_length = count($state);
    		if($array_length > 0){
    			$str = "(";
    			for ($i = 0; $i < $array_length; $i++){
			    	if(($i + 1) < $array_length){
			    		$str .= $state[$i] . ",";
			    	}else{
			    		$str .= $state[$i];
			    	}
				}
				$str .= ")";
				$sql .= " AND request.state IN " . $str;
    		}
    	}else{
    		if(!empty($state) && $state > 0){
				$sql .= " AND request.state = " . $state;
			}
    	}
		
		if(!empty($period_start_time) && $period_start_time > 0){
			$sql .= " AND request.start_date >= " . $period_start_time;
		}
		
		if(!empty($period_end_time) && $period_end_time > 0){
			$sql .= " AND request.start_date < " . $period_end_time;
		}
		
		if(empty($order_by)){
			$sql .= " ORDER BY request.id ASC";
		}else{	
			if($order_by == 'applicant'){
				$sql .= " ORDER BY user.first_name " . $order . ", user.last_name";
			}else if($order_by == 'leave_type_id'){
				$sql .= " ORDER BY leave_type.leave_type " . $order ;
			}else{
				$sql .= " ORDER BY request." . $order_by . " " . $order ;
			}
		}
		
		log_message("debug", "get_requests_by_calendar sql <<< ".$sql);
		
    	$query = $this->db->query($sql);
    	return $query->result();
    }
    
    /**
     * get count which same person has requested a leave of the same type on the same period time
     * @param $user_name
     * @param $leave_type_id
     * @param $start_time
     * @param $end_time
     * @param $request_id
     * @return int
     */
	function get_same_by_leave_type($user_name, $leave_type_id, $start_time, $end_time, $request_id=0){
    	$sql = "SELECT count(*) AS num 
    			FROM st_request AS request , st_leave_type AS leave_type
    				, st_department AS department, st_users AS user 
    			WHERE request.status = " . ACTIVE . "
    				AND leave_type.id = request.leave_type_id
    				AND department.id = user.department_id  
    				AND request.user_name = user.user_name
    				AND request.state in ('20','30','40')
    				AND request.user_name = '" . $user_name . "'
    				AND ( 
					   (request.start_date <= " . $start_time . " AND request.end_date >= " . $start_time . " ) 
					OR (request.start_date >= " . $start_time . " AND request.end_date <= " . $end_time . " )
					OR (request.start_date <= " . $end_time .   " AND request.end_date >= " . $end_time . " )
					)
					AND request.id != " . $request_id;
		
		if(is_array($leave_type_id)){
    		$array_length = count($leave_type_id);
    		if($array_length > 0){
    			$str = "(";
    			for ($i = 0; $i < $array_length; $i++){
			    	if(($i + 1) < $array_length){
			    		$str .= $leave_type_id[$i] . ",";
			    	}else{
			    		$str .= $leave_type_id[$i];
			    	}
				}
				$str .= ")";
				$sql .= " AND request.leave_type_id IN " . $str;
    		}
    	}else{
	    	if(!empty($leave_type_id) && $leave_type_id > 0){
				$sql .= " AND request.leave_type_id = " . $leave_type_id;
			}
    	}
		
		log_message("debug", "get_same_by_leave_type <<< " . $sql);
		$query = $this->db->query($sql);
		$total_count = 0;
		if ($query->num_rows() > 0){
			$row = $query->row();
			$total_count = $row->num;
		}
		return $total_count;
    }
    
    function get_approved_by_period($user_id, $start_time, $end_time){
    	$sql = "SELECT request.*,leave_type.leave_type, user.user_name
    				, user.first_name, user.last_name
    				, department.department  
    			FROM st_request AS request , st_leave_type AS leave_type
    				, st_department AS department, st_users AS user 
    			WHERE request.status = " . ACTIVE . "
    				AND request.state = 40  
    				AND leave_type.id = request.leave_type_id
    				AND department.id = user.department_id  
    				AND request.user_name = user.user_name 
    				AND user.id = " . $user_id. "
    				AND ( 
					   (request.start_date <= " . $start_time . " AND request.end_date >= " . $start_time . " ) 
					OR (request.start_date >= " . $start_time . " AND request.end_date < " . $end_time . " )
					OR (request.start_date < " . $end_time .   " AND request.end_date >= " . $end_time . " )
					)
    			";
		
		log_message("debug", "get_approved_by_period <<< " . $sql);
    	$query = $this->db->query($sql);
    	return $query->result();
    }
}