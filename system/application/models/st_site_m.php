<?php

class St_site_m extends MY_Model {
	
	/**
	 * get dropdown list
	 * @return array
	 */
	function get_select_dropdown(){
		$this->db
    		->where('status', ACTIVE)
    		->order_by('name', 'ASC');
		$sites = $this->get_all();
		$a_ret = array();
		$a_ret[''] = 'Please select..';
		foreach ($sites as $row){
			$a_ret[$row->id] = $row->name;
		}
		return $a_ret;
	}
	/**
	 * get dropdown list
	 * @return array
	 */
	function get_dropdown(){
		$this->db
    		->where('status', ACTIVE)
    		->order_by('name', 'ASC');
		$sites = $this->get_all();
		$a_ret = array();
		$a_ret[''] = 'All';
		foreach ($sites as $row){
			$a_ret[$row->id] = $row->name;
		}
		return $a_ret;
	}
	function site() {
		$query = $this->db->query('SELECT * FROM st_site ORDER BY name;');
		return $query->result();
	}
	
	function get_active() {
		$query = $this->db->query('SELECT * FROM st_site WHERE status= ' . ACTIVE. ' ORDER BY name;');
		return $query->result();
	}

    function change_state($id, $state) {
        $this->db->query("UPDATE st_site SET status = '$state' WHERE id = '$id';");
	}
	
	/**
	 * remove id
	 * @param $id
	 * @return void
	 */
	function remove($id) {
        $this->db->query("DELETE FROM st_site WHERE id = '$id';");
	}
	
	/**
	 * check whether name is unique except the special id
	 * @param $name
	 * @param $id
	 * @return int
	 */
	function check_except($name, $id){
		$sql = "SELECT * FROM st_site 
			WHERE id != " . $id . "
			AND name = '" . $name ."'";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	
}