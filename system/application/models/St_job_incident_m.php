<?php

/**
 * job incident model class
 * @author Bai Yinbing
 */
class St_job_incident_m extends MY_Model {
    /**
     * set active status for specail job incident
     * @param $job_id
     * @return boolean
     */
    function set_active($job_id)
    {
    	return $this->update($job_id, 
    			array('status' => ACTIVE)
    	);
    }
    
    /**
     * set in-active status for specail job incident
     * @param $job_id
     * @return boolean
     */
	function set_inactive($job_id)
	{
    	return $this->update($job_id,
    			array('status' => IN_ACTIVE)
    	);
    }
    
    function get_info($id){
    	$sql = "SELECT job.*, user.user_name, user.first_name, user.last_name" .
    			" , user.user_level_id, user.email, user.personal_email, dept.department " .
    			" FROM st_job_incident AS job, st_users AS user, st_department AS dept " .
    			" WHERE job.id = " . $id .
    			" AND job.user_id = user.id " .
    			" AND user.department_id = dept.id";
    	log_message("debug", "get_info == ".$sql);
    	$query = $this->db->query($sql);
        return $query->row();
    }
    
    
    /**
     * get new job incidents
     */
    function get_new_by_user($user_id, $page_no=1, $limit=PAGE_SIZE){
    	return $this->get_by_state_user(1, $user_id, $page_no, $limit);
    }
    
    /**
     * get in progress job incidents
     */
    function get_progress_by_user($user_id, $page_no=1, $limit=PAGE_SIZE){
    	return $this->get_by_state_user(2, $user_id, $page_no, $limit);
    }
    
    /**
     * get completed job incidents
     */
    function get_completed_by_user($user_id, $page_no=1, $limit=PAGE_SIZE){
    	return $this->get_by_state_user(3, $user_id, $page_no, $limit);
    }
    
    /**
     * get count of new job incidents
     */
    function count_new_by_user($user_id){
    	return $this->count_by_state_user(1, $user_id);
    }
    
    /**
     * get count of in progress job incidents
     */
    function count_progress_by_user($user_id){
    	return $this->count_by_state_user(2, $user_id);
    }
    
    /**
     * get count of completed job incidents
     */
    function count_completed_by_user($user_id){
    	return $this->count_by_state_user(3, $user_id);
    }
        
    /**
	 * get count by state and user
	 */
    function count_by_state_user($state, $user_id){
    	$where = array(
    		'user_id' => $user_id,
    		'state' => $state,
    		'status' => ACTIVE
    	);
    	return $this->count_by($where);
    }
    
    /**
     * get job incidents by state and user
     */
    function get_by_state_user($state, $user_id, $page_no=1, $limit=PAGE_SIZE){
    	$sql = " SELECT job.*, user.first_name, user.last_name " .
    			" FROM st_job_incident AS job, st_users AS user " .
    			" WHERE job.user_id = user.id " .
    			"   AND job.state = " . $state .
    			"   AND job.status = " . ACTIVE .
    			" 	AND job.user_id = " . $user_id .
    			" ORDER BY job.id desc " ;
    	if(empty($page_no)){
			$page_no = 1;
		}
		
		if(!empty($limit)){
			$start = ($page_no - 1) * $limit;
			$sql .= " LIMIT " . $start . ", " . $limit;
		}
		
		log_message("debug", "get_by_state_user <<< " . $sql);
    	$query = $this->db->query($sql);
    	return $query->result();
    }
    
    /**
     * get new job incidents approvals
     */
    function get_approvals_by_new($user_level, $site_id=0, $department_id=0, $page_no=1, $limit=PAGE_SIZE){
    	return $this->get_approvals_by_state(1, $user_level, $site_id, $department_id, $page_no, $limit);
    }
    
    /**
     * get in progress job incidents approvals
     */
    function get_approvals_by_progress($user_level, $site_id=0, $department_id=0, $page_no=1, $limit=PAGE_SIZE){
    	return $this->get_approvals_by_state(2, $user_level, $site_id, $department_id, $page_no, $limit);
    }
    
    /**
     * get completed job incidents approvals
     */
    function get_approvals_by_completed($user_level, $site_id=0, $department_id=0, $page_no=1, $limit=PAGE_SIZE){
    	return $this->get_approvals_by_state(3, $user_level, $site_id, $department_id, $page_no, $limit);
    }
    
    /**
     * get approvals job incidents by state
     */
    function get_approvals_by_state($state, $user_level, $site_id=0, $department_id=0, $page_no=1, $limit=PAGE_SIZE){
    	$sql = " SELECT job.*, user.first_name, user.last_name " .
    			" FROM st_job_incident AS job, st_users AS user " .
    			" , st_department AS department, st_site AS site" .
    			" WHERE job.user_id = user.id " .
    			"   AND department.id = user.department_id " .
    			"  AND department.site_id = site.id " .
    			"   AND job.state = " . $state .
    			"   AND job.status = " . ACTIVE;
    	
    	if($user_level == 2){
    		$sql .= " AND user.user_level_id != 3 AND user.user_level_id != 4";
    	}

		if(!empty($site_id) && $site_id > 0){
			$sql .= " AND department.site_id = " . $site_id;
		}
		
    	if(!empty($department_id)){
			$sql .= " AND department.department = '" . $department_id . "'";
		}

    	$sql .= " ORDER BY job.id desc ";		
    	if(empty($page_no)){
			$page_no = 1;
		}
		
		if(!empty($limit)){
			$start = ($page_no - 1) * $limit;
			$sql .= " LIMIT " . $start . ", " . $limit;
		}
		
		log_message("debug", "get_by_state_user <<< " . $sql);
    	$query = $this->db->query($sql);
    	return $query->result();
    }
    
    /**
     * get count of new job incidents
     */
    function count_approvals_by_new($user_level, $site_id=0, $department_id=0){
    	return $this->count_approvals_by_state(1, $user_level, $site_id, $department_id);
    }
    
    /**
     * get count of in progress job incidents
     */
    function count_approvals_by_progress($user_level, $site_id=0, $department_id=0){
    	return $this->count_approvals_by_state(2, $user_level, $site_id, $department_id);
    }
    
    /**
     * get count of completed job incidents
     */
    function count_approvals_by_completed($user_level, $site_id=0, $department_id=0){
    	return $this->count_approvals_by_state(3, $user_level, $site_id, $department_id);
    }
    
    /**
     * get count approvals job incidents by state
     */
    function count_approvals_by_state($state, $user_level, $site_id=0, $department_id=0){
    	$sql = " SELECT count(job.id) AS num " .
    			" FROM st_job_incident AS job, st_users AS user " .
    			" , st_department AS department, st_site AS site" .
    			" WHERE job.user_id = user.id " .
    			"   AND department.id = user.department_id " .
    			"  AND department.site_id = site.id " .
    			"   AND job.state = " . $state .
    			"   AND job.status = " . ACTIVE;
    	
    	if($user_level == 2){
    		$sql .= " AND user.user_level_id != 3 AND user.user_level_id != 4";
    	}

		if(!empty($site_id) && $site_id > 0){
			$sql .= " AND department.site_id = " . $site_id;
		}
		
    	if(!empty($department_id)){
			$sql .= " AND department.department = '" . $department_id . "'";
		}
		
		$total_count = 0;
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0){
			$row = $query->row();
			$total_count = $row->num;
		}
		
        return $total_count;
    }
}