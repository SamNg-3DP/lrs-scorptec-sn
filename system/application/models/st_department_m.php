<?php

class St_department_m extends MY_Model {
	
    /**
     * get all the department data.
     * @return <type> 
     */
	function get_department() {
		//$this->order_by('department');
		//return $this->get_all();
		$query = $this->db->query("SELECT dep.*, site.name AS site_name FROM st_department AS dep, st_site AS site WHERE site.id = dep.site_id AND site.status = " . ACTIVE);
		return $query->result();
	}

	/**
	 * get active departments
	 * @return array
	 */
    function get_department_active() {
		$this->db
    		->where('status', ACTIVE)
    		->order_by('department', 'ASC');
		return $this->get_all();
	}

    function get_department_single($dapartmentid) {
		$query = $this->db->query("SELECT * FROM " .$this->table . " WHERE id = $dapartmentid;");
		return $query->result();
	}
	
	/**
	 * get department by department id
	 * @param $dapartmentid
	 * @return unknown_type
	 */
	function get_department_by_id($dapartmentid) {
		return parent::get_by("id",$dapartmentid);
	}
	
	/**
	 * get dropdown list
	 * @return array
	 */
	function get_dropdown($site_id){
		$departments = array();
		if(empty($site_id)){
			$departments = $this->get_distinct_name();
		}else{
			$this->db
    		->where('status', ACTIVE)
    		->where('site_id', $site_id)
    		->order_by('department', 'ASC');
			$departments = $this->get_all();
		}
		
		$a_ret = array();
		$a_ret['0'] = 'All';
		foreach ($departments as $row){
			$a_ret[$row->department] = $row->department;
		}
		return $a_ret;
	}

	/**
	 * get dropdown list
	 * @return array
	 */
	function get_select_dropdown($site_id){
		$this->db
    		->where('status', ACTIVE)
    		->where('site_id', $site_id)
    		->order_by('department', 'ASC');
		$departments = $this->get_all();
		$a_ret = array();
		$a_ret[''] = 'Please select..';
		foreach ($departments as $row){
			$a_ret[$row->id] = $row->department;
		}
		return $a_ret;
	}
	
	/**
	 * check whether name is unique except the special id
	 * @param $name
	 * @param $id
	 * @return int
	 */
	function check_except($name, $site_id, $id){
		$sql = "SELECT * FROM st_department 
			WHERE id != " . $id . "
			AND site_id = " . $site_id ."
			AND department = '" . $name ."'";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
    
    /**
     * set active status for specail department
     * @param $department_id
     * @return unknown_type
     */
    function set_active($department_id)
    {
    	return $this->update($department_id, 
    			array('status' => ACTIVE)
    	);
    }
    
    /**
     * set in-active status for specail department
     * @param $department_id
     * @return unknown_type
     */
	function set_inactive($department_id)
	{
    	return $this->update($department_id,
    			array('status' => IN_ACTIVE)
    	);
    }
    
    function update_department($department_id, $dapartment_name, $status)
    {
    	return $this->update($department_id,
    			array(
    				'status' => $status,
    				'department' => $dapartment_name
    			)
    	);
    }
    
    /**
     * get departments by site id
     * @param $site_id
     * @return array
     */
    function get_by_site($site_id){
    	$sql = "SELECT * FROM st_department 
			WHERE site_id = " . $site_id ."
			ORDER BY department ASC";
		$query = $this->db->query($sql);
		return $query->result();
    }
    
    function count_by_site($site_id){
    	$sql = "SELECT COUNT( id ) AS num  
				FROM st_department  
				WHERE site_id = " . $site_id;
		
    	$total_count = 0;
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0){
			$row = $query->row();
			$total_count = $row->num;
		}
		log_message("debug", "count_by_site <<< " . $sql);
        return $total_count;
    }
    
    function get_distinct_name(){
    	/*
    	$sql = "SELECT DISTINCT (
			department
			) AS name, dep. * 
			FROM st_department AS dep
			WHERE status = ".ACTIVE."		
			GROUP BY name
			ORDER BY dep.department";
		*/
		$sql = "SELECT DISTINCT (
			department
			) FROM st_department AS dep
			WHERE status = ".ACTIVE."		
			ORDER BY dep.department";
		$query = $this->db->query($sql);
		return $query->result();
    }
    
}