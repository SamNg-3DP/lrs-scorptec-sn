<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add Leave type</title>
<base href="<?=base_url()?>"/>
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/js_color_picker_v2.css" media="screen">
<script src="js/color_functions.js"></script> 
<script type="text/javascript" src="js/js_color_picker_v2.js"></script>
<style>
#input, select
{
	font-family: Tahoma, Helvetica, Sans-Serif;
	font-size: 11px;
	font-style: normal;
	font-weight: normal;
}
</style>
</head>

<body>

<form id="form1" name="form1" method="post" action="<?=site_url("leavetype/do_add")?>">

  <table width="100%" height="22" border="0" align="center" cellpadding="0" cellspacing="0" background="images/Footer.png">
    <tr>
      <td>
      <table width="100%" height="40" border="0" cellspacing="1" cellpadding="1">
        <tr bgcolor="#EBEBEB">
            <td width="50%" height="20" align="left" colspan="3" style="color: red"><? echo $this->session->flashdata('message'); ?></td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td width="80" height="20" align="left">Name:</td>
          <td width="400" height="20">
          	<label>
               <?php 
                $data = array(
	              'name'        => 'leave_type_name',
	              'id'          => 'leave_type_name',
	              'value'       =>  set_value('leave_type_name'),
	              'maxlength'   => '50',
	              'size'        => '40',
	              'style'       => 'font-size: 11px',
	            );
				echo form_input($data);
				?>
          	</label>
            <span style="color: #F00; font-weight: bold;">*</span>
          </td>
          <td><?php echo form_error('leave_type_name');?></td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td height="20" >Outcome(Working hours):</td>
          <td height="20"><label>
          	<?php
          	$js = "id='working_hours_outcome_id' style='font-size: 11px; width: 120px;'";
          	echo form_dropdown('working_hours_outcome_id', $working_hours_outcomes, set_value('working_hours_outcome_id'), $js);
           	?>
            </label>
            <span style="color: #F00; font-weight: bold;">*</span>
            </td>
            <td><?php echo form_error('working_hours_outcome_id');?></td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td height="20" >Outcome(Leave Balance):</td>
          <td height="20"><label>
          	<?php
          	$js = "id='leave_balance_outcome_id' style='font-size: 11px; width: 120px;'";
          	echo form_dropdown('leave_balance_outcome_id', $leave_balance_outcomes, set_value('leave_balance_outcome_id'), $js);
           	?>
            </label>
            <span style="color: #F00; font-weight: bold;">*</span>
            </td>
            <td><?php echo form_error('leave_balance_outcome_id');?></td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td height="20">Pay status:</td>
          <td height="20"><label>
            <?php
          	$js = "id='pay_status' style='font-size: 11px; width: 120px;'";
          	echo form_dropdown('pay_status', $pay_statuses, set_value('outcome_id'), $js);
           ?>
            </label>
            <span style="color: #F00; font-weight: bold;">*</span>
            </td>
            <td><?php echo form_error('pay_status');?></td>
        </tr>
        <tr bgcolor="#FFFFFF">
			<td>Color:</td>
			<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="60%"><input name="color" type="text" id="color" style="font-size: 11px"
						value="<?php
						echo set_value ( 'color' );
						?>"
						size="10" readonly="readonly" /><label
						style="color: #F00"><img src="images/action/bgColor.gif" width="18" height="18" border="0" align="top" onclick="showColorPicker(this,document.getElementById('color'))"/>＊</label></td>
					<td><?php
					echo form_error ( 'color' );
					?></td>
				</tr>
			</table>
			</td>
		</tr>
        <tr bgcolor="#FFFFFF">
          <td height="20" >Status:</td>
          <td height="20"><label>
            <?php
          	$js = "id='status' style='font-size: 11px; width: 120px;'";
          	echo form_dropdown('status', $statuses, set_value('status'), $js);
           ?>
            </label>
            <span style="color: #F00; font-weight: bold;">*</span>
            </td>
            <td><?php echo form_error('status');?></td>
        </tr>
      </table>
      </td>
    </tr>
  </table>
    <p align="center">
      <input style="font-size: 11px" type="submit" name="submit" id="submit" value="     Save     " />
  </p>
</form>
</body>
</html>