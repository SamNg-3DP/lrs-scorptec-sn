<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Edit user</title>
<base href="<?=base_url()?>" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />
<style type="text/css">
<!--
#form1 table tr td table {
	font-family: Tahoma, Helvetica, sans-serif;
	font-size: 11px;
}

#form1 p {
	text-align: center;
}

#input,select {
	font-family: Tahoma, Helvetica, Sans-Serif;
	font-size: 11px;
	font-style: normal;
	font-weight: normal;
}
/* tab pane styling */
div.panes div {
	display:none;		
	padding:15px 10px;
	border:1px solid #999;
	border-top:0;
	font-size:14px;
	background-color:#fff;
}

-->
</style>

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
    jQuery.validator.addMethod("gt", function( value, element, param ) {
    	var target = $(param);
		return value > target.val();
	}, "End time must be later than start time");
	
    $.validator.setDefaults({
    	//submitHandler: function() { alert("submitted!"); },
    	highlight: function(input) {
    		$(input).addClass("ui-state-highlight");
    	},
    	unhighlight: function(input) {
    		$(input).removeClass("ui-state-highlight");
    	}
    });
    
    $(function() {
        // setup ul.tabs to work as tabs for each div directly under div.panes 
        //$("ul.tabs").tabs("div.panes > div");
    	var wizard = $("#wizard");
    	// enable tabs that are contained within the wizard
    	$("ul.tabs", wizard).tabs("div.panes > div", function(event, index) {
    		// ensure that validation is correct.
    		if (index > 0 && !$("#edit_user").valid()){
    			// when false is returned, the user cannot advance to the next tab
    			return false;
    		}
    		// everything is ok. remove possible red highlight from the terms
    	});
     
    	// get handle to the tabs API
    	var api = $("ul.tabs", wizard).tabs(0);
     
    	// "next tab" button
    	$("button.next", wizard).click(function() {
    		api.next();
    	});
     
    	// "previous tab" button
    	$("button.prev", wizard).click(function() {
    		api.prev();
    	});

    	// validate signup form on keyup and submit
    	$("#edit_user").validate({
    		rules: {
				user_name: "required",

    			first_name: "required",
    			last_name: "required",
    			sex: "required",
    			password: {
    				required: true,
    				minlength: 3,
    				maxlength: 50
    			},
    			site_id: "required",
    			department_id: "required",
    			hire_date: "required",
    			address: "required",
    			home_phone: "required",
    			mobile: "required",
    			email: {
    				required: true,
    				email: true
    			},
    			personal_email: {
    				required: true,
    				email: true
    			},
    			birth_date: "required",
    			kin_name: "required",
    			kin_contact: "required",
    			tfn: "required",
    			bank_account_name: "required",
    			bank_name: "required",
    			bank_bsb: "required",
    			bank_account_no: "required"
    		},
    		messages: {
    			user_name: "Please enter user's user name",
        		
    			first_name: "Please enter user's first name",
    			last_name: "Please enter users's last name",
    			sex: "Please select sex",
    			password: {
    				required: "Please provide a password",
    				minlength: "Your password must be at least 3 characters long",
    				maxlength: "Please enter password, between 3 and 50 characters"
    			},
    			confirm_password: {
    				required: "Please provide a password",
    				minlength: "Your password must be at least 6 characters long",
    				equalTo: "Please enter the same password as above"
    			},
    			site_id: "Please select a site",
    			department_id: "Please select a department",
    			hire_date: "Please select hire date",
    			address: "Please enter address",
    			home_phone: "Please enter homephone",
    			mobile: "Please enter mobile",
    			bank_account_name: "Please enter account name",
    			bank_name: "Please enter bank name",
    			bank_bsb: "Please enter BSB",
    			bank_account_no: "Please enter Account No"
    		},
    		submitHandler: function(form) {
				jQuery(form).ajaxSubmit({
					dataType: 'json',
					success: saveinfoResponse
				});
				// !!! Important !!! 
		        // always return false to prevent standard browser submit and page navigation
				return false;
			}
    	});
    	$("#add_labor").validate({
    		rules: {
    			entry_date: "required",
    			payrollshift_id: "required",
    			hourly_rate: {
      				required: true,
      				number: true
    			}
    		},
    		messages: {
				entry_date: "Please enter entry date",
				payrollshift_id: "Please select payrollshift shift",
				hourly_rate: {
      				required: "Please enter hourly rate",
      				number: "Please enter a valid number"
    			}
    		},
    		submitHandler: function(form) {
				jQuery(form).ajaxSubmit({
					dataType: 'json',
					success: addLaborResponse
				});
				// !!! Important !!! 
		        // always return false to prevent standard browser submit and page navigation
				return false;
			}
    	});

    	$("#hire_date").datepicker({ 
        	disabled: true, 
        	dateFormat: '<?=JS_DATE_FORMAT?>',
        	changeMonth: true,
    		changeYear: true 
    	});
    	$("#terminate_date").datepicker({ 
        	disabled: true, 
        	dateFormat: '<?=JS_DATE_FORMAT?>',
        	changeMonth: true,
    		changeYear: true 
    	});
    	$("#review_date").datepicker({ 
        	disabled: true, 
        	dateFormat: '<?=JS_DATE_FORMAT?>',
        	changeMonth: true,
    		changeYear: true 
    	});    	
    	$("#birth_date").datepicker({ 
        	disabled: true, 
        	dateFormat: '<?=JS_DATE_FORMAT?>',
        	changeMonth: true,
    		changeYear: true,
    		yearRange: '1900:2010'
    	});
    	$("#entry_date").datepicker({ 
        	disabled: true, 
        	dateFormat: '<?=JS_DATE_FORMAT?>',
        	changeMonth: true,
    		changeYear: true 
    	});
		
		load_labors();
    });

    // post-submit callback 
    function saveinfoResponse(responseText, statusText, xhr, $form)  {
        //alert(responseText.message);
        var url = "<?=site_url('user')?>";
        window.location = url;
    }

	function addLaborResponse(responseText, statusText, xhr, $form)  {
    	if(responseText.status=='ok'){
        	$('#add_labor').resetForm();
        	load_labors();
        }
        alert(responseText.message);
    }

    function remove_labor(id){
    	if(confirm("Comfirm remove the labor?")){
    		$.ajax({
    		  type: 'POST',
    		  url: "<?=site_url("user/remove_labor_ajax")?>",
    		  data: {id: id},
    		  success: remove_response,
    		  dataType: 'json'
    		});
    	}
    }

    function remove_response(data){
    	if(data.status == "ok"){
    		alert(data.message);
    		load_labors();
    	}else{
    		alert(data.message);
    	}
    }

    function change_site(value, dept_id){
    	var dept_select = document.getElementById(dept_id);
    	dept_select.options.length=0;
    	$.ajax({
    		async: true,
    		type: 'POST',
    		url: "<?=site_url("approve/dept_by_add_site_ajax")?>",
    		data: {
    		  	site_id: value,
    		  	select: true
    		},
    		success: function(data){
    			$.each(data, function(i, dep){
    				dept_select.options.add(new Option(dep.name,dep.id));
    			});
    	  	},
    		dataType: 'json'
    	});
    	var payrollshift_select = document.getElementById("payrollshift_id");
    	payrollshift_select.options.length=0;
    	$.ajax({
			async: true,
			type: 'POST',
			url: "<?=site_url("payrollshift/get_payrollshifts_ajax")?>",
			data: {
			  	site_id: value,
			  	select: "select"
			},
			success: function(data){
				$.each(data, function(i, pay){
					payrollshift_select.options.add(new Option(pay.code, pay.id));
				});
		  	},
			dataType: 'json'
		});
    }
    
    var url_labors = "<? echo site_url("user/labors/" . $user->id)?>";
    function load_labors(){
    	$("#list_labor_contracts").load(url_labors);
    }

</script>
</head>
<body>

<div id="wizard">
<ul class="tabs"> 
	<li><a href="#info">User Info</a></li> 
    <li><a href="#labor">Labor Contract</a></li> 
</ul>
<div class="panes">
<div>
	<?php 
	echo form_open_multipart('user/edit_ajax', array('id'=>'edit_user'));
	$t = 1; 
	?>
		<table width="100%" height="568" border="0" cellspacing="1" cellpadding="1">
			<tr bgcolor="#EBEBEB">
				<td width="50%" height="20" colspan="2" bgcolor="#EBEBEB">Basic User Info</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td width="25%" height="20">Login username:</td>
				<td width="75%" height="20">
				
<!--				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input type="text" name="user_name" id="user_name"
							value="<?php echo $user->user_name;?>" style="font-size: 11px" />
							<?php 	echo form_error('username'); 
									echo form_hidden("id", $user->id);
							?>
							<label style="color: #F00">＊</label>
						</td>
					</tr>
				</table>				
-->				
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
						<?php 
						echo $user->user_name;
						echo form_hidden("user_name", $user->user_name);
						echo form_hidden("id", $user->id);
						?>
						<label style="color: #F00">＊</label></td>						
					</tr>
				</table>

				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">Login Password:</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="41%">
						<?php 
						echo form_input(array(
							'name' => 'password',
							'id' => 'password',
							'tabindex' => $t,
							'value' => $user->password,
							'style' => "font-size: 11px"
						));
						$t++;
						echo form_error('password');
						?>
						<label style="color: #F00">＊</label>
						<input type="button" name="generate" id="generate"
							value="Generate" style="font-size: 11px" onclick="javascript:generatePassword('password');" />
						</td>
					</tr>
				</table>
				</td>
			</tr>
			
			<tr bgcolor="#FFFFFF">
				<td height="20">Actatek ID:</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%">
						<?php 
						echo form_input(array(
							'name' => 'actatek_id',
							'id' => 'actatek_id',
							'tabindex' => $t,
							'value' => $user->actatek_id,
							'style' => "font-size: 11px"
						));
						$t++;
						echo form_error('actatek_id');
						?>
						<label style="color: #F00">＊</label></td>
					</tr>
				</table>
				</td>
			</tr>
						
			<tr bgcolor="#FFFFFF">
				<td height="20">First Name:</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%">
						<?php 
						echo form_input(array(
							'name' => 'first_name',
							'id' => 'first_name',
							'tabindex' => $t,
							'value' => $user->first_name,
							'style' => "font-size: 11px"
						));
						$t++;
						echo form_error('first_name');
						?>
						<label style="color: #F00">＊</label></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#FFFFFF">Last Name:</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%">
						<?php 
						echo form_input(array(
							'name' => 'last_name',
							'id' => 'last_name',
							'tabindex' => $t,
							'value' => $user->last_name,
							'style' => "font-size: 11px"
						));
						$t++;
						echo form_error('last_name');
						?>
						<label style="color: #F00">＊</label></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#FFFFFF">Sex:</td>
				<td height="20">
				<?php
				$js = 'id="sex" style="font-size: 11px; width: 120px;" ';
				echo form_dropdown("sex", $sexs, $user->sex, $js);
				$t++;
				?>
				<label style="color: #F00">＊</label>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#FFFFFF">Site:</td>
				<td height="20">
				<?php 
				$js = 'id="site_id" style="font-size: 11px; width: 120px;" onChange="change_site(this.value, \'department_id\')"';
				echo form_dropdown("site_id", $sites, $user->site_id, $js);
				$t++;
				?>
				<label style="color: #F00">＊</label>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#FFFFFF">Department:</td>
				<td height="20">
				<?php 
				echo form_dropdown('department_id', $departments, $user->department_id, "id='department_id' style='font-size: 11px; width: 120px;'");
				$t++;
				?>
				<label style="color: #F00">＊</label>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#FFFFFF">User Level:</td>
				<td height="20">
				<?php 
				echo form_dropdown('user_level_id', $user_levels, $user->user_level_id, "style='font-size: 11px; width: 120px;'");
				$t++;
				?>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#FFFFFF">Employment Type:</td>
				<td height="20">
				<?php 
				$js = 'id="employment_type_id" style="font-size: 11px; width: 120px;"';
				echo form_dropdown('employment_type_id', $employment_types, $user->employment_type_id, $js);
				$t++;
				?>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#FFFFFF">Residency Status:</td>
				<td height="20">
				<?php 
				$js = 'id="residency_status" style="font-size: 11px; width: 120px;"';
				echo form_dropdown('residency_status', $residency_statuses, $user->residency_status, $js);
				?>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#FFFFFF">Hire Date:</td>
				<td height="20">
					<?php 
					$hire_date = "";
					if($user->hire_date > 0){
						$hire_date = date(DATE_FORMAT, $user->hire_date);
					}
					echo form_input(array(
						'name' => 'hire_date',
						'id' => 'hire_date',
						'tabindex' => $t,
						'value' => $hire_date,
						'style' => "font-size: 11px"
					));
					$t++;
					echo form_error('hire_date');
					?>
					<label style="color: #F00">*</label>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#FFFFFF">Terminate Date:</td>
				<td height="20">
				<?php 
					$terminate_date = "";
					if($user->terminate_date > 0){
						$terminate_date = date(DATE_FORMAT, $user->terminate_date);
					}
					echo form_input(array(
						'name' => 'terminate_date',
						'id' => 'terminate_date',
						'tabindex' => $t,
						'value' => $terminate_date,
						'style' => "font-size: 11px"
					));
					$t++;
					echo form_error('terminate_date');
					?>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#FFFFFF">Last Review Date:</td>
				<td height="20">
				<?php 
					$review_date = "";
					if($user->review_date > 0){
						$review_date = date(DATE_FORMAT, $user->review_date);
					}
					echo form_input(array(
						'name' => 'review_date',
						'id' => 'review_date',
						'tabindex' => $t,
						'value' => $review_date,
						'style' => "font-size: 11px"
					));
					$t++;
					echo form_error('review_date');
					?>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">Photo:</td>
				<td height="20">
				<label><input name="photo_file" type="file" id="photo_file"
					style="font-size: 11px; font-weight: normal;" size="50"
					value="<?php echo set_value('photo_file');?>" /></label>
					<?php 
					if(!empty($user->photo_file)){
						$photo_url = DIR_PHOTO . $user->photo_file;
						echo "&nbsp;&nbsp;&nbsp;&nbsp;<a href='".$photo_url ."' target='_blank'>";
						echo "<img src='". $photo_url ."' hight='100' width='80'/></a>";
					}
					?>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#EBEBEB">Contact Details</td>
				<td height="20" bgcolor="#EBEBEB">&nbsp;</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Address:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="73%">
						<?php 
						echo form_input(array(
							'name' => 'address',
							'id' => 'address',
							'tabindex' => $t,
							'value' => $user->address,
							'style' => "font-size: 11px",
							'size' => 70
						));
						echo form_error('address');
						$t++;
						?>
						<label style="color: #F00">＊</label></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Home Phone:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%">
						<?php 
						echo form_input(array(
							'id' 		=> 'home_phone',
							'name' 		=> 'home_phone',
							'tabindex' 	=> $t,
							'value' 	=> $user->home_phone,
							'style' 	=> "font-size: 11px",
							'size'  	=> 20
						));
						echo form_error('home_phone');
						$t++;
						?>
						<label style="color: #F00">＊</label></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Mobile:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%">
						<?php 
						echo form_input(array(
							'id' 		=> 'mobile',
							'name' 		=> 'mobile',
							'tabindex' 	=> $t,
							'value' 	=> $user->mobile,
							'style' 	=> "font-size: 11px",
							'size'  	=> 30
						));
						echo form_error('home_phone');
						$t++;
						?>
						<label style="color: #F00">＊</label></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Email(work):</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="55%">
						<?php 
						echo form_input(array(
							'id' 		=> 'email',
							'name' 		=> 'email',
							'tabindex' 	=> $t,
							'value' 	=> $user->email,
							'style' 	=> "font-size: 11px",
							'size'  	=> 50
						));
						echo form_error('email');
						$t++;
						?>
						<label style="color: #F00">＊</label>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Email(personal):</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="55%">
						<?php 
						echo form_input(array(
							'id' 		=> 'personal_email',
							'name' 		=> 'personal_email',
							'tabindex' 	=> $t,
							'value' 	=> $user->personal_email,
							'style' 	=> "font-size: 11px",
							'size'  	=> 50
						));
						echo form_error('personal_email');
						$t++;
						?>
						<label style="color: #F00">＊</label>	
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" width="22%">Date of Birth:</td>
				<td height="20">
					<?php 
					$birth_date = "";
					if($user->birthday > 0){
						$birth_date = date(DATE_FORMAT, $user->birthday);
					}
					echo form_input(array(
						'name' => 'birth_date',
						'id' => 'birth_date',
						'tabindex' => $t,
						'value' => $birth_date,
						'style' => "font-size: 11px"
					));
					$t++;
					echo form_error('birth_date');
					?>
					<label style="color: #F00">*</label>
                </td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#EBEBEB">Next of Kin</td>
				<td height="20" bgcolor="#EBEBEB">&nbsp;</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Name:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<?php 
				echo form_input(array(
					'id' 		=> 'kin_name',
					'name' 		=> 'kin_name',
					'tabindex' 	=> $t,
					'value' 	=> $user->kin_name,
					'style' 	=> "font-size: 11px",
				));
				echo form_error('kin_name');
				$t++;
				?><label style="color: #F00">＊</label>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Contact details:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<?php 
				echo form_input(array(
					'id' 		=> 'kin_contact',
					'name' 		=> 'kin_contact',
					'tabindex' 	=> $t,
					'value' 	=> $user->kin_contact,
					'style' 	=> "font-size: 11px",
					'size'		=> 50
				));
				echo form_error('kin_contact');
				$t++;
				?><label style="color: #F00">＊</label>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">Medical conditions (if any):</td>
				<td height="20">
				<?php 
				echo form_input(array(
					'id' 		=> 'medical_conditions',
					'name' 		=> 'medical_conditions',
					'tabindex' 	=> $t,
					'value' 	=> $user->medical_conditions,
					'style' 	=> "font-size: 11px",
					'size'		=> 50
				));
				echo form_error('medical_conditions');
				$t++;
				?>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">Driver License No:</td>
				<td height="20">
				<?php 
				echo form_input(array(
					'id' 		=> 'driver_license_no',
					'name' 		=> 'driver_license_no',
					'tabindex' 	=> $t,
					'value' 	=> $user->driver_license_no,
					'style' 	=> "font-size: 11px"
				));
				echo form_error('driver_license_no');
				$t++;
				if(!empty($user->driver_license_file)){
					echo "&nbsp;&nbsp;&nbsp;&nbsp;<a href='".DIR_DRIVER_LICENSE . $user->driver_license_file ."' target='_blank'>Download</a>";
				}
				?>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">&nbsp;</td>
				<td height="20"><label> <input name="driverfile" type="file" id="driverfile"
					style="font-size: 11px; font-weight: normal;" size="50"
					value="<?php echo set_value('driverfile');?>" />(Please attach the copy of your driver
				license.)</label></td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">Passport No:</td>
				<td height="20">
				<?php 
				echo form_input(array(
					'id' 		=> 'passport_license_no',
					'name' 		=> 'passport_license_no',
					'tabindex' 	=> $t,
					'value' 	=> $user->passport_license_no,
					'style' 	=> "font-size: 11px"
				));
				echo form_error('passport_license_no');
				$t++;
				if(!empty($user->passport_license_file)){
					echo "&nbsp;&nbsp;&nbsp;&nbsp;<a href='".DIR_PASSPORT_LICENSE . $user->driver_license_file ."' target='_blank'>Download</a>";
				}
				?>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">&nbsp;</td>
				<td height="20"><label> <input name="passportfile" type="file" id="passportfile"
					style="font-size: 11px; font-weight: normal;" size="50"
					value="<?php echo set_value('passportfile');?>" />(Please attach the copy of your passport
				license.)</label></td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">TFN:</td>
				<td height="20">
				<?php 
				echo form_input(array(
					'id' 		=> 'tfn',
					'name' 		=> 'tfn',
					'tabindex' 	=> $t,
					'value' 	=> $user->tfn,
					'style' 	=> "font-size: 11px"
				));
				echo form_error('tfn');
				$t++;
				?><label style="color: #F00">＊</label>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#EBEBEB">Bank Details</td>
				<td height="20" bgcolor="#EBEBEB">&nbsp;</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Account Name:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%">
						<?php 
						echo form_input(array(
							'id' 		=> 'bank_account_name',
							'name' 		=> 'bank_account_name',
							'tabindex' 	=> $t,
							'value' 	=> $user->bank_account_name,
							'style' 	=> "font-size: 11px"
						));
						echo form_error('bank_account_name');
						$t++;
						?>
						<label
							style="color: #F00">＊</label></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Bank:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%">
						<?php 
						echo form_input(array(
							'id' 		=> 'bank_name',
							'name' 		=> 'bank_name',
							'tabindex' 	=> $t,
							'value' 	=> $user->bank_name,
							'style' 	=> "font-size: 11px"
						));
						echo form_error('bank_name');
						$t++;
						?>
						<?php echo form_error('bank_name'); ?><label
							style="color: #F00">＊</label></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">BSB:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%">
						<?php 
						echo form_input(array(
							'id' 		=> 'bank_bsb',
							'name' 		=> 'bank_bsb',
							'tabindex' 	=> $t,
							'value' 	=> $user->bank_bsb,
							'style' 	=> "font-size: 11px"
						));
						echo form_error('bank_bsb');
						$t++;
						?>
						<label style="color: #F00">＊</label></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Account No:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%">
						<?php 
						echo form_input(array(
							'id' 		=> 'bank_account_no',
							'name' 		=> 'bank_account_no',
							'tabindex' 	=> $t,
							'value' 	=> $user->bank_account_no,
							'style' 	=> "font-size: 11px"
						));
						echo form_error('bank_account_no');
						$t++;
						?>
						<label style="color: #F00">＊</label></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" colspan="2" align="center" bgcolor="#EBEBEB"><input style="font-size: 11px"
					type="submit" name="button" id="button" value="     Save & Close     " /></td>
			</tr>
		</table>
	<?php echo form_close(); ?>
</div>
<!-- tab labor contract start-->
<div>
	<table width="100%" border="0" cellspacing="1" cellpadding="0">
		<tr bgcolor="#FFFFFF">
			<td height="22" colspan="2" style="text-decoration:underline"><strong>Add labor contract</strong></td>
		</tr>			
	</table>
	
	<?php 
	echo form_open_multipart('user/save_labor_ajax', array('id'=>'add_labor')); 
	echo form_hidden("user_id", $user->id);	
	?>
	<input type="hidden" id="user_type_fulltime" name="user_type_fulltime" value="<?=$user->employment_type_id?>"/>
		
		<table width="100%" border="0" cellspacing="1" cellpadding="0">			
			<tr bgcolor="#FFFFFF">
				<td width="25%" height="22" bgcolor="#FFFFFF">Entry Date:</td>
				<td width="77%" height="22" bgcolor="#FFFFFF">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="100%">
							<input type="text" name="entry_date" id="entry_date" style="font-size:11px;" value="<?php echo set_value('entry_date');?>"/>
							<label style="color: #F00">*</label>
							</td>
						</tr>
					</table>
				</td>
	        </tr>
	        <tr bgcolor="#FFFFFF">
				<td height="22" bgcolor="#FFFFFF">Payroll:</td>
				<td bgcolor="#FFFFFF">
				<?
				$payrollshift_id = 'payrollshift_id';
				$js = 'id="'.$payrollshift_id.'" style="font-size: 11px;"';
				echo form_dropdown($payrollshift_id, $payrollshifts, "", $js);
				?>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="22" bgcolor="#FFFFFF">Hourly Rate:</td>
				<td bgcolor="#FFFFFF">
				<input type="text" name="hourly_rate" id="hourly_rate" style="font-size:11px;" value="<?php echo set_value('hourly_rate');?>"/>
				<label style="color: #F00">*</label>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF" >
				<td height="22" bgcolor="#FFFFFF">Labor Contract:</td>
				<td height="20">
					<label>
						<input type="file" name="contract_file" id="contract_file" style="font-size:11px;"/>
					</label>
				</td>
	        </tr>
	        <tr bgcolor="#FFFFFF" >
				<td colspan="2">&nbsp;</td>
	        </tr>
			<tr bgcolor="#FFFFFF">
				<td height="22" colspan="2" align="center" bgcolor="#EBEBEB">
				<label>
					<input type="submit" name="button" id="button" value="     Save     " style="font-size:11px;"/>
				</label>
				</td>
			</tr>
		</table>
	<?php echo form_close();?>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr >
			<td>&nbsp;</td>
        </tr>
		<tr>
	        <td width="90%">
	        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	          	<tr>
	            	<td height="20" style="text-decoration:underline"><strong>Labor Contract List</strong></td>
				</tr>
				</table>
			<p id="list_labor_contracts">
			
	        </p>
	        </td>
	      </tr>
	    </table>
</div><!-- end tab labor contract-->
</div><!-- end panes -->
</div><!-- end wizard -->
</body>
</html>
