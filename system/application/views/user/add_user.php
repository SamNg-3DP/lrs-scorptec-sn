<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add an employee</title>
<base href="<?=base_url()?>" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />
<style type="text/css">
<!--
#save table tr td table {
	font-family: Tahoma, Helvetica, sans-serif;
	font-size: 11px;
}

#save p {
	text-align: center;
}

#input,select {
	font-family: Tahoma, Helvetica, Sans-Serif;
	font-size: 11px;
	font-style: normal;
	font-weight: normal;
}
/* tab pane styling */
div.panes div {
	display:none;		
	padding:15px 10px;
	border:1px solid #999;
	border-top:0;
	font-size:14px;
	background-color:#fff;
}

label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	padding-left:20px;
	color: red;
	font-style: italic
}

-->
</style>

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
    $.validator.setDefaults({
    	highlight: function(input) {
    		$(input).addClass("ui-state-highlight");
    	},
    	unhighlight: function(input) {
    		$(input).removeClass("ui-state-highlight");
    	}
    });

    
    $(function() {
        // setup ul.tabs to work as tabs for each div directly under div.panes 
    	var wizard = $("#wizard");
    	// enable tabs that are contained within the wizard
    	$("ul.tabs", wizard).tabs("div.panes > div", function(event, index) {
    		// ensure that validation is correct.
    		if (index > 0 && !$("#save").valid())  {
    			// when false is returned, the user cannot advance to the next tab
    			return false;
    		}
    		if(index > 0 && !isSaved()){
        		alert("Please click this button 'Save and Continue -->' ");
        		return false;
    		}
    		// everything is ok. remove possible red highlight from the terms
    	});
     
    	// get handle to the tabs API
    	var api = $("ul.tabs", wizard).tabs(0);
     
    	// "next tab" button
    	$("button.next", wizard).click(function() {
    		api.next();
    	});
     
    	// "previous tab" button
    	$("button.prev", wizard).click(function() {
    		api.prev();
    	});

    	$("#hire_date").datepicker({ 
        	disabled: true, 
        	dateFormat: '<?=JS_DATE_FORMAT?>',
        	changeMonth: true,
    		changeYear: true 
    	});

    	$("#terminate_date").datepicker({ 
        	disabled: true, 
        	dateFormat: '<?=JS_DATE_FORMAT?>',
        	changeMonth: true,
    		changeYear: true 
    	});
    	
    	$("#birth_date").datepicker({ 
        	disabled: true, 
        	dateFormat: '<?=JS_DATE_FORMAT?>',
        	changeMonth: true,
    		changeYear: true,
    		yearRange: '1900:2010'
    	});
    	
    	// validate signup form on keyup and submit
    	$("#save").validate({
    		rules: {
    			user_name: {
					required: true,
					minlength: 4,
					remote: {
				        url: "<?php echo site_url('user/check_user_name_ajax');?>",
				        type: "post"
				    }
				},
				password: {
    				required: true,
    				minlength: 6,
    				maxlength: 50
    			},
    			first_name: "required",
    			last_name: "required",
    			sex: "required",
    			site_id: "required",
    			department_id: "required",
    			employment_type_id: "required",
    			hire_date: "required",
//    			address: "required",
//    			home_phone: "required",
//    			mobile: "required",
    			email: {
    				required: true,
    				email: true
    			},
//    			personal_email: {
//    				required: true,
//    				email: true
//    			},
//    			birth_date: "required",
//    			kin_name: "required",
//    			kin_contact: "required",
//    			tfn: "required",
//    			bank_account_name: "required",
//   			bank_name: "required",
//    			bank_bsb: "required",
//    			bank_account_no: "required"
    		},
    		messages: {
    			user_name: {
					required: "Please enter a username",
					minlength: "Your username must consist of at least 4 characters",
					remote: "Ther username already exists, please enter another username."
				},
				
    			first_name: "Please enter user's firstname",
    			last_name: "Please enter users's lastname",
    			sex: "Please select sex",
    			password: {
    				required: "Please provide a password",
    				minlength: "Your password must be at least 6 characters long",
    				maxlength: "Please enter password, between 6 and 50 characters"
    			},
    			site_id: "Please select a site",
    			department_id: "Please select a department",
    			employment_type_id: "Please select an employment type",
    			hire_date: "Please select hire date",
//    			address: "Please enter address",
//    			home_phone: "Please enter homephone",
//    			mobile: "Please enter mobile",
//    			bank_account_name: "Please enter account name",
//    			bank_name: "Please enter bank name",
//    			bank_bsb: "Please enter BSB",
//    			bank_account_no: "Please enter Account No"
    		},
    		submitHandler: function(form) {
				jQuery(form).ajaxSubmit({
					dataType: 'json',
					success: saveinfoResponse
				});
				// !!! Important !!! 
		        // always return false to prevent standard browser submit and page navigation
				return false;
			}
    	});
    });

    // post-submit callback 
    function saveinfoResponse(responseText, statusText, xhr, $form)  {
        
        if(responseText.status=='ok'){
            //redirect to edit this user page
            //var url = "<?php echo base_url()."/".index_page();?>/user/edit/" + $("#user_name").val() + "#labor";
        	var url = "<?php echo base_url()."/".index_page();?>/user";
            window.location = url;
            return;
        }else{
        	alert(responseText.message);
        }
    }

    //check if tab 0 is saved
    function isSaved(){
        var is_save = $('#is_save').val();
        if(is_save == "ok"){
            return true;
        }
        return false;
    }

    function change_site(value, dept_id){
    	var dept_select = document.getElementById(dept_id);
    	dept_select.options.length=0;
    	$.ajax({
    		async: true,
    		type: 'POST',
    		url: "<?=site_url("approve/dept_by_add_site_ajax")?>",
    		data: {
    		  	site_id: value,
    		  	select: true
    		},
    		success: function(data){
    			$.each(data, function(i, dep){
    				dept_select.options.add(new Option(dep.name,dep.id));
    			});
    	  	},
    		dataType: 'json'
    	});
    }
</script>
</head>
<body>

<div id="wizard">
<ul class="tabs"> 
	<li><a href="#">User Info</a></li> 
    <li><a href="#">Labor Contract</a></li> 
</ul>
<div class="panes">
<div>
	<?php echo form_open_multipart('user/save', array('id'=>'save', 'name'=>'save')); 
	?>
		<input type="hidden" name="is_save" id="is_save" value="" />
		<table width="100%" height="568" border="0" cellspacing="1" cellpadding="1">
			<tr bgcolor="#EBEBEB">
				<td width="50%" height="20" colspan="2" align="left" bgcolor="#EBEBEB" >Basic User Info</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td width="25%" height="20">Login username:</td>
				<td width="75%" height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><input type="text" name="user_name" id="user_name"
							value="<?php echo set_value('user_name');?>" style="font-size: 11px" />
							<?php echo form_error('username'); ?>
							<label style="color: #F00">＊</label>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">Login Password:</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="41%"><input type="text" name="password" id="password"
							value="<?php echo set_value('password');?>" style="font-size: 11px" /><?php echo form_error('password'); ?><label
							style="color: #F00">＊</label><input type="button" name="generate" id="generate"
							value="Generate" style="font-size: 11px" onclick="javascript:generatePassword('password');" />
						</td>
					</tr>
				</table>
				</td>
			</tr>
			
			<tr bgcolor="#FFFFFF">
				<td height="20">Actatek ID:</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%"><input type="text" name="actatek_id" id="actatek_id"
							value="<?php echo set_value('actatek_id');?>" style="font-size: 11px" /><?php echo form_error('actatek_id'); ?><label
							style="color: #F00">＊</label></td>
					</tr>
				</table>
				</td>
			</tr>			
			<tr bgcolor="#FFFFFF">
				<td height="20">First Name:</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%"><input type="text" name="first_name" id="first_name"
							value="<?php echo set_value('first_name');?>" style="font-size: 11px" /><?php echo form_error('first_name'); ?><label
							style="color: #F00">＊</label></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#FFFFFF">Last Name:</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%"><input type="text" name="last_name" id="last_name"
							value="<?php echo set_value('last_name');?>" style="font-size: 11px" /><?php echo form_error('last_name'); ?><label
							style="color: #F00">＊</label></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#FFFFFF">Sex:</td>
				<td height="20">
				<?php
				$js = 'id="sex" style="font-size: 11px; width: 120px;" ';
				echo form_dropdown("sex", $sexs, "", $js);
				?>
				<label style="color: #F00">＊</label>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#FFFFFF">Site:</td>
				<td height="20">
				<?php 
				$js = 'id="site_id" style="font-size: 11px; width: 120px;" onChange="change_site(this.value, \'department_id\')"';
				echo form_dropdown("site_id", $sites, "", $js);
				?>
				<label style="color: #F00">＊</label>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#FFFFFF">Department:</td>
				<td height="20">
				<?php 
				$js = 'id="department_id" style="font-size: 11px; width: 120px;"';
				echo form_dropdown('department_id', $departments, "", $js);
				?>
				<label style="color: #F00">＊</label>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#FFFFFF">User Level:</td>
				<td height="20"><select name="user_level_id" id="user_level_id"
					style="font-size: 11px; width: 120px;">
					<? foreach ($user_level as $row): ?>
					<option value="<?=$row->id?>"><?=$row->user_level?></option>
					<? endforeach; ?>
				</select>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#FFFFFF">Employment Type:</td>
				<td height="20">
				<?php 
				$js = 'id="employment_type_id" onChange="javascript:changeUserType(this.value);" style="font-size: 11px; width: 120px;"';
				echo form_dropdown('employment_type_id', $employment_types, "", $js);
				?>
				<label style="color: #F00">＊</label>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#FFFFFF">Residency Status:</td>
				<td height="20">
				<?php 
				$js = 'id="residency_status" style="font-size: 11px; width: 120px;"';
				$residency_status  = set_value("residency_status");
				echo form_dropdown('residency_status', $residency_statuses, $residency_status, $js);
				?>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#FFFFFF">Hire Date:</td>
				<td height="20">
					<input type="text" name="hire_date" id="hire_date" style="font-size:11px;" value="<?php echo set_value('hire_date');?>"/>
					<label style="color: #F00">*</label>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#FFFFFF">Terminate date:</td>
				<td height="20">
					<input type="text" name="terminate_date" id="terminate_date" style="font-size:11px;" value="<?php echo set_value('terminate_date');?>"/>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">Photo:</td>
				<td height="20">
				<label><input name="photo_file" type="file" id="photo_file"
					style="font-size: 11px; font-weight: normal;" size="50"
					value="<?php echo set_value('photo_file');?>" /></label>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#EBEBEB">Contact Details</td>
				<td height="20" bgcolor="#EBEBEB">&nbsp;</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Address:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="73%"><input name="address" type="text" id="address" style="font-size: 11px"
							value="<?php echo set_value('address');?>" size="70" /><?php echo form_error('address'); ?><label style="color: #F00"></label></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Home Phone:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%"><input name="home_phone" type="text" id="home_phone" style="font-size: 11px"
							value="<?php echo set_value('home_phone');?>" size="20" /><?php echo form_error('home_phone'); ?><label style="color: #F00"></label></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Mobile:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%"><input name="mobile" type="text" id="mobile" style="font-size: 11px"
							value="<?php echo set_value('mobile');?>" size="30" /><?php echo form_error('mobile'); ?><label style="color: #F00"></label></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Email(work):</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="55%"><input name="email" type="text" id="email" style="font-size: 11px"
							value="<?php echo set_value('email');?>" size="50" /><?php echo form_error('email'); ?><label style="color: #F00">＊</label></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Email(personal):</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="55%"><input name="personal_email" type="text" id="personal_email" style="font-size: 11px"
							value="<?php echo set_value('personal_email');?>" size="50" /><?php echo form_error('personal_email'); ?><label style="color: #F00"></label></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">Date of Birth:</td>
				<td height="20">
					<input type="text" name="birth_date" id="birth_date" style="font-size:11px;" value="<?php echo set_value('birth_date');?>"/>
					<label style="color: #F00"></label>
                </td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#EBEBEB">Next of Kin</td>
				<td height="20" bgcolor="#EBEBEB">&nbsp;</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Name:</td>
					</tr>
				</table>
				</td>
				<td height="20"><input type="text" name="kin_name" id="kin_name" style="font-size: 11px"
					value="<?php echo set_value('kin_name');?>" /><label style="color: #F00"></label></td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Contact details:</td>
					</tr>
				</table>
				</td>
				<td height="20"><input name="kin_contact" type="text" id="kin_contact" size="50" style="font-size: 11px"
					value="<?php echo set_value('kin_contact');?>" /><label style="color: #F00"></label></td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">Medical conditions (if any):</td>
				<td height="20"><input name="medical_conditions" type="text" id="medical_conditions" size="50" style="font-size: 11px"
					value="<?php echo set_value('medical_conditions');?>" /></td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">Driver License No:</td>
				<td height="20"><input type="text" name="driver_license_no" id="driver_license_no" style="font-size: 11px"
					value="<?php echo set_value('driver_license_no');?>" /></td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">&nbsp;</td>
				<td height="20"><label> <input name="driverfile" type="file" id="driverfile"
					style="font-size: 11px; font-weight: normal;" size="50"
					value="<?php echo set_value('driverfile');?>" />(Please attach the copy of your driver
				license.)</label></td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">Passport No:</td>
				<td height="20"><input type="text" name="passport_license_no" id="passport_license_no" style="font-size: 11px"
					value="<?php echo set_value('passport_license_no');?>" /></td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">&nbsp;</td>
				<td height="20"><label> <input name="passportfile" type="file" id="passportfile"
					style="font-size: 11px; font-weight: normal;" size="50"
					value="<?php echo set_value('passportfile');?>" />(Please attach the copy of your passport
				license.)</label></td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">TFN:</td>
				<td height="20"><input type="text" name="tfn" id="tfn" style="font-size: 11px"
					value="<?php echo set_value('tfn');?>" /><label style="color: #F00"></label></td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#EBEBEB">Bank Details</td>
				<td height="20" bgcolor="#EBEBEB">&nbsp;</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Account Name:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%"><input type="text" name="bank_account_name" id="bank_account_name"
							value="<?php echo set_value('bank_account_name');?>" style="font-size: 11px" /><?php echo form_error('bank_account_name'); ?><label
							style="color: #F00"></label></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Bank:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%"><input type="text" name="bank_name" id="bank_name"
							value="<?php echo set_value('bank_name');?>" style="font-size: 11px" /><?php echo form_error('bank_name'); ?><label
							style="color: #F00"></label></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">BSB:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%"><input type="text" name="bank_bsb" id="bank_bsb" value="<?php echo set_value('bank_bsb');?>"
							style="font-size: 11px" /><?php echo form_error('bank_bsb'); ?><label style="color: #F00"></label></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Account No:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%"><input type="text" name="bank_account_no" id="bank_account_no" maxlength="100"
							value="<?php echo set_value('bank_account_no');?>" style="font-size: 11px" /><?php echo form_error('bank_account_no'); ?><label
							style="color: #F00"></label></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" colspan="2" align="center" bgcolor="#EBEBEB"><input style="font-size: 11px"
					type="submit" name="button" id="button" value="     Save & Close     " /></td>
			</tr>
		</table>
	<?php form_close();?>
</div>
<!-- tab labor contract start-->
<div>
	<p>
	<?php echo form_open('user/save_fulltime'); ?>
		<table width="100%" border="0" cellspacing="1" cellpadding="0">
			<tr bgcolor="#FFFFFF">
				<td height="22" colspan="2" style="text-decoration:underline"><strong>Add labor contract</strong></td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td width="25%" height="22" bgcolor="#FFFFFF">Entry Date:</td>
				<td width="77%" height="22" bgcolor="#FFFFFF">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="34%"><input type="text" name="entrydate" id="entrydate" style="font-size:11px;" value="<?php echo set_value('entrydate');?>"/></td>
							<td width="66%"><?php echo form_error('entrydate'); ?></td>
						</tr>
					</table>
				</td>
	        </tr>
	        <tr bgcolor="#FFFFFF">
				<td height="22" colspan="2" bgcolor="#EBEBEB">Working Time</td>
			</tr>
			<tr>
				<td height="22" bgcolor="#FFFFFF">Monday To Friday:</td>
				<td height="22" bgcolor="#FFFFFF">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="9%">StartTime</td>
							<td width="18%">
								<select name="mstarttime" id="mstarttime" style="font-size:11px; width:100px;">
									<option value="9:30" selected="selected">9:30</option>
		                			<option value="10:00">10:00</option>
		                		</select>
		            		</td>
		            		<td width="8%">EndTime</td>
							<td width="65%">
								<select name="mendtime" id="mendtime" style="font-size:11px; width:100px;">
									<option value="18:00">18:00</option>
		                            <option value="18:30">18:30</option>
		                      	</select>
		                    </td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
			    <td height="22" bgcolor="#FFFFFF">Saturday:</td>
			    <td height="22" bgcolor="#FFFFFF">
			        <table width="100%" border="0" cellspacing="0" cellpadding="0">
			            <tr>
			                <td width="9%">StartTime</td>
			                <td width="18%"><select name="sstarttime" id="sstarttime" style="font-size:11px; width:100px;">
			                  <option value="10:00">10:00</option>
			                  <option value="10:30">10:30</option>
			                </select></td>
			                <td width="8%">EndTime</td>
			                <td width="65%"><select name="sendtime" id="sendtime" style="font-size:11px; width:100px;">
			                  <option value="15:00">15:00</option>
			                  <option value="15:30">15:30</option>
			                </select></td>
			            </tr>
			        </table>
			    </td>
			</tr>
			<tr bgcolor="#FFFFFF" >
				<td height="22" bgcolor="#FFFFFF">Labor Contract:</td>
				<td height="20">
					<label>
						<input type="file" name="contract" id="contract" style="font-size:11px;"/>
					</label>
				</td>
	        </tr>
	        <tr bgcolor="#FFFFFF" >
				<td colspan="2">&nbsp;</td>
	        </tr>
			<tr bgcolor="#FFFFFF">
				<td height="22" colspan="2" align="center" bgcolor="#EBEBEB">
				<label>
					<input type="submit" name="button" id="button" value="     Save     " style="font-size:11px;"/>
				</label>
				</td>
			</tr>
		</table>
	<?php echo form_close(); ?>
	</p>
	<p>
	<?php echo form_open('user/save_other'); ?>
		<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0"
		       background="images/Footer.png">
		<tr>
		<td>
		<table width="100%" border="0" cellspacing="1" cellpadding="0">
		<tr bgcolor="#FFFFFF">
		    <td height="22" colspan="2" bgcolor="#EBEBEB"></td>
		</tr>
		<tr bgcolor="#FFFFFF">
		    <td width="25%" height="22" bgcolor="#FFFFFF">Contract Period:</td>
		    <td width="77%" height="22" bgcolor="#FFFFFF">
		        <table width="100%" border="0" cellspacing="0" cellpadding="0">
		            <tr>
		                <td width="7%">From</td>
		                <td width="25%"><input type="text" name="entrydate" id="entrydate" style="font-size:11px"
		                                       value="<?php echo set_value('entrydate');?>"/></td>
		                <td width="4%">To</td>
		                <td width="30%"><input type="text" name="expiredate" id="expiredate" style="font-size:11px"
		                                       value="<?php echo set_value('expiredate');?>"/></td>
		                <td width="34%"><?php echo form_error('entrydate'); ?> <?php echo form_error('expiredate'); ?></td>
		            </tr>
		        </table>
		    </td>
		</tr>
		<tr bgcolor="#FFFFFF">
		    <td height="22" colspan="2" bgcolor="#EBEBEB">Working Time</td>
		</tr>
		<tr>
		    <td height="22" bgcolor="#FFFFFF">Monday:</td>
		    <td height="22" bgcolor="#FFFFFF"> StartTime
		        <label>
		            <select name="monstarttime" id="monstarttime" style="font-size:11px; width:60px">
		                <option value="6:00">6:00</option>
		                <option value="6:30">6:30</option>
		                <option value="7:00">7:00</option>
		                <option value="7:30">7:30</option>
		                <option value="8:00">8:00</option>
		                <option value="8:30">8:30</option>
		                <option value="9:00">9:00</option>
		                <option value="9:30" selected="selected">9:30</option>
		                <option value="10:00">10:00</option>
		                <option value="10:30">10:30</option>
		                <option value="11:00">11:00</option>
		                <option value="11:30">11:30</option>
		                <option value="12:00">12:00</option>
		                <option value="12:30">12:30</option>
		                <option value="13:00">13:00</option>
		                <option value="13:30">13:30</option>
		                <option value="14:00">14:00</option>
		                <option value="14:30">14:30</option>
		                <option value="15:00">15:00</option>
		                <option value="15:30">15:30</option>
		                <option value="16:00">16:00</option>
		                <option value="16:30">16:30</option>
		                <option value="17:00">17:00</option>
		                <option value="17:30">17:30</option>
		                <option value="18:00">18:00</option>
		                <option value="18:30">18:30</option>
		                <option value="19:00">19:00</option>
		                <option value="19:30">19:30</option>
		                <option value="20:00">20:00</option>
		                <option value="20:30">20:30</option>
		                <option value="21:00">21:00</option>
		                <option value="21:30">21:30</option>
		                <option value="22:00">22:00</option>
		                <option value="22:30">22:30</option>
		                <option value="23:00">23:00</option>
		                <option value="23:30">23:30</option>
		            </select>
		            EndTime
		            <select name="monendtime" id="monendtime" style="font-size:11px; width:60px">
		                <option value="6:00">6:00</option>
		                <option value="6:30">6:30</option>
		                <option value="7:00">7:00</option>
		                <option value="7:30">7:30</option>
		                <option value="8:00">8:00</option>
		                <option value="8:30">8:30</option>
		                <option value="9:00">9:00</option>
		                <option value="9:30">9:30</option>
		                <option value="10:00">10:00</option>
		                <option value="10:30">10:30</option>
		                <option value="11:00">11:00</option>
		                <option value="11:30">11:30</option>
		                <option value="12:00">12:00</option>
		                <option value="12:30">12:30</option>
		                <option value="13:00">13:00</option>
		                <option value="13:30">13:30</option>
		                <option value="14:00">14:00</option>
		                <option value="14:30">14:30</option>
		                <option value="15:00">15:00</option>
		                <option value="15:30">15:30</option>
		                <option value="16:00">16:00</option>
		                <option value="16:30">16:30</option>
		                <option value="17:00">17:00</option>
		                <option value="17:30">17:30</option>
		                <option value="18:00" selected="selected">18:00</option>
		                <option value="18:30">18:30</option>
		                <option value="19:00">19:00</option>
		                <option value="19:30">19:30</option>
		                <option value="20:00">20:00</option>
		                <option value="20:30">20:30</option>
		                <option value="21:00">21:00</option>
		                <option value="21:30">21:30</option>
		                <option value="22:00">22:00</option>
		                <option value="22:30">22:30</option>
		                <option value="23:00">23:00</option>
		                <option value="23:30">23:30</option>
		            </select>
		        </label></td>
		</tr>
		<tr>
		    <td height="22" bgcolor="#FFFFFF">Tuesday:</td>
		    <td height="22" bgcolor="#FFFFFF">StartTime
		        <label>
		            <select name="tuestarttime" id="tuestarttime" style="font-size:11px; width:60px">
		                <option value="6:00">6:00</option>
		                <option value="6:30">6:30</option>
		                <option value="7:00">7:00</option>
		                <option value="7:30">7:30</option>
		                <option value="8:00">8:00</option>
		                <option value="8:30">8:30</option>
		                <option value="9:00">9:00</option>
		                <option value="9:30" selected="selected">9:30</option>
		                <option value="10:00">10:00</option>
		                <option value="10:30">10:30</option>
		                <option value="11:00">11:00</option>
		                <option value="11:30">11:30</option>
		                <option value="12:00">12:00</option>
		                <option value="12:30">12:30</option>
		                <option value="13:00">13:00</option>
		                <option value="13:30">13:30</option>
		                <option value="14:00">14:00</option>
		                <option value="14:30">14:30</option>
		                <option value="15:00">15:00</option>
		                <option value="15:30">15:30</option>
		                <option value="16:00">16:00</option>
		                <option value="16:30">16:30</option>
		                <option value="17:00">17:00</option>
		                <option value="17:30">17:30</option>
		                <option value="18:00">18:00</option>
		                <option value="18:30">18:30</option>
		                <option value="19:00">19:00</option>
		                <option value="19:30">19:30</option>
		                <option value="20:00">20:00</option>
		                <option value="20:30">20:30</option>
		                <option value="21:00">21:00</option>
		                <option value="21:30">21:30</option>
		                <option value="22:00">22:00</option>
		                <option value="22:30">22:30</option>
		                <option value="23:00">23:00</option>
		                <option value="23:30">23:30</option>
		            </select>
		            EndTime
		            <select name="tueendtime" id="tueendtime" style="font-size:11px; width:60px">
		                <option value="6:00">6:00</option>
		                <option value="6:30">6:30</option>
		                <option value="7:00">7:00</option>
		                <option value="7:30">7:30</option>
		                <option value="8:00">8:00</option>
		                <option value="8:30">8:30</option>
		                <option value="9:00">9:00</option>
		                <option value="9:30">9:30</option>
		                <option value="10:00">10:00</option>
		                <option value="10:30">10:30</option>
		                <option value="11:00">11:00</option>
		                <option value="11:30">11:30</option>
		                <option value="12:00">12:00</option>
		                <option value="12:30">12:30</option>
		                <option value="13:00">13:00</option>
		                <option value="13:30">13:30</option>
		                <option value="14:00">14:00</option>
		                <option value="14:30">14:30</option>
		                <option value="15:00">15:00</option>
		                <option value="15:30">15:30</option>
		                <option value="16:00">16:00</option>
		                <option value="16:30">16:30</option>
		                <option value="17:00">17:00</option>
		                <option value="17:30">17:30</option>
		                <option value="18:00" selected="selected">18:00</option>
		                <option value="18:30">18:30</option>
		                <option value="19:00">19:00</option>
		                <option value="19:30">19:30</option>
		                <option value="20:00">20:00</option>
		                <option value="20:30">20:30</option>
		                <option value="21:00">21:00</option>
		                <option value="21:30">21:30</option>
		                <option value="22:00">22:00</option>
		                <option value="22:30">22:30</option>
		                <option value="23:00">23:00</option>
		                <option value="23:30">23:30</option>
		            </select>
		        </label></td>
		</tr>
		<tr>
		    <td height="22" bgcolor="#FFFFFF">Wednesday:</td>
		    <td height="22" bgcolor="#FFFFFF">StartTime
		        <label>
		            <select name="wedstarttime" id="wedstarttime" style="font-size:11px; width:60px">
		                <option value="6:00">6:00</option>
		                <option value="6:30">6:30</option>
		                <option value="7:00">7:00</option>
		                <option value="7:30">7:30</option>
		                <option value="8:00">8:00</option>
		                <option value="8:30">8:30</option>
		                <option value="9:00">9:00</option>
		                <option value="9:30" selected="selected">9:30</option>
		                <option value="10:00">10:00</option>
		                <option value="10:30">10:30</option>
		                <option value="11:00">11:00</option>
		                <option value="11:30">11:30</option>
		                <option value="12:00">12:00</option>
		                <option value="12:30">12:30</option>
		                <option value="13:00">13:00</option>
		                <option value="13:30">13:30</option>
		                <option value="14:00">14:00</option>
		                <option value="14:30">14:30</option>
		                <option value="15:00">15:00</option>
		                <option value="15:30">15:30</option>
		                <option value="16:00">16:00</option>
		                <option value="16:30">16:30</option>
		                <option value="17:00">17:00</option>
		                <option value="17:30">17:30</option>
		                <option value="18:00">18:00</option>
		                <option value="18:30">18:30</option>
		                <option value="19:00">19:00</option>
		                <option value="19:30">19:30</option>
		                <option value="20:00">20:00</option>
		                <option value="20:30">20:30</option>
		                <option value="21:00">21:00</option>
		                <option value="21:30">21:30</option>
		                <option value="22:00">22:00</option>
		                <option value="22:30">22:30</option>
		                <option value="23:00">23:00</option>
		                <option value="23:30">23:30</option>
		            </select>
		            EndTime
		            <select name="wedendtime" id="wedendtime" style="font-size:11px; width:60px">
		                <option value="6:00">6:00</option>
		                <option value="6:30">6:30</option>
		                <option value="7:00">7:00</option>
		                <option value="7:30">7:30</option>
		                <option value="8:00">8:00</option>
		                <option value="8:30">8:30</option>
		                <option value="9:00">9:00</option>
		                <option value="9:30">9:30</option>
		                <option value="10:00">10:00</option>
		                <option value="10:30">10:30</option>
		                <option value="11:00">11:00</option>
		                <option value="11:30">11:30</option>
		                <option value="12:00">12:00</option>
		                <option value="12:30">12:30</option>
		                <option value="13:00">13:00</option>
		                <option value="13:30">13:30</option>
		                <option value="14:00">14:00</option>
		                <option value="14:30">14:30</option>
		                <option value="15:00">15:00</option>
		                <option value="15:30">15:30</option>
		                <option value="16:00">16:00</option>
		                <option value="16:30">16:30</option>
		                <option value="17:00">17:00</option>
		                <option value="17:30">17:30</option>
		                <option value="18:00" selected="selected">18:00</option>
		                <option value="18:30">18:30</option>
		                <option value="19:00">19:00</option>
		                <option value="19:30">19:30</option>
		                <option value="20:00">20:00</option>
		                <option value="20:30">20:30</option>
		                <option value="21:00">21:00</option>
		                <option value="21:30">21:30</option>
		                <option value="22:00">22:00</option>
		                <option value="22:30">22:30</option>
		                <option value="23:00">23:00</option>
		                <option value="23:30">23:30</option>
		            </select>
		        </label></td>
		</tr>
		<tr>
		    <td height="22" bgcolor="#FFFFFF">Thursday</td>
		    <td height="22" bgcolor="#FFFFFF">StartTime
		        <label>
		            <select name="thustarttime" id="thustarttime" style="font-size:11px; width:60px">
		                <option value="6:00">6:00</option>
		                <option value="6:30">6:30</option>
		                <option value="7:00">7:00</option>
		                <option value="7:30">7:30</option>
		                <option value="8:00">8:00</option>
		                <option value="8:30">8:30</option>
		                <option value="9:00">9:00</option>
		                <option value="9:30" selected="selected">9:30</option>
		                <option value="10:00">10:00</option>
		                <option value="10:30">10:30</option>
		                <option value="11:00">11:00</option>
		                <option value="11:30">11:30</option>
		                <option value="12:00">12:00</option>
		                <option value="12:30">12:30</option>
		                <option value="13:00">13:00</option>
		                <option value="13:30">13:30</option>
		                <option value="14:00">14:00</option>
		                <option value="14:30">14:30</option>
		                <option value="15:00">15:00</option>
		                <option value="15:30">15:30</option>
		                <option value="16:00">16:00</option>
		                <option value="16:30">16:30</option>
		                <option value="17:00">17:00</option>
		                <option value="17:30">17:30</option>
		                <option value="18:00">18:00</option>
		                <option value="18:30">18:30</option>
		                <option value="19:00">19:00</option>
		                <option value="19:30">19:30</option>
		                <option value="20:00">20:00</option>
		                <option value="20:30">20:30</option>
		                <option value="21:00">21:00</option>
		                <option value="21:30">21:30</option>
		                <option value="22:00">22:00</option>
		                <option value="22:30">22:30</option>
		                <option value="23:00">23:00</option>
		                <option value="23:30">23:30</option>
		            </select>
		            EndTime
		            <select name="thuendtime" id="thuendtime" style="font-size:11px; width:60px">
		                <option value="6:00">6:00</option>
		                <option value="6:30">6:30</option>
		                <option value="7:00">7:00</option>
		                <option value="7:30">7:30</option>
		                <option value="8:00">8:00</option>
		                <option value="8:30">8:30</option>
		                <option value="9:00">9:00</option>
		                <option value="9:30">9:30</option>
		                <option value="10:00">10:00</option>
		                <option value="10:30">10:30</option>
		                <option value="11:00">11:00</option>
		                <option value="11:30">11:30</option>
		                <option value="12:00">12:00</option>
		                <option value="12:30">12:30</option>
		                <option value="13:00">13:00</option>
		                <option value="13:30">13:30</option>
		                <option value="14:00">14:00</option>
		                <option value="14:30">14:30</option>
		                <option value="15:00">15:00</option>
		                <option value="15:30">15:30</option>
		                <option value="16:00">16:00</option>
		                <option value="16:30">16:30</option>
		                <option value="17:00">17:00</option>
		                <option value="17:30">17:30</option>
		                <option value="18:00" selected="selected">18:00</option>
		                <option value="18:30">18:30</option>
		                <option value="19:00">19:00</option>
		                <option value="19:30">19:30</option>
		                <option value="20:00">20:00</option>
		                <option value="20:30">20:30</option>
		                <option value="21:00">21:00</option>
		                <option value="21:30">21:30</option>
		                <option value="22:00">22:00</option>
		                <option value="22:30">22:30</option>
		                <option value="23:00">23:00</option>
		                <option value="23:30">23:30</option>
		            </select>
		        </label></td>
		</tr>
		<tr>
		    <td height="22" bgcolor="#FFFFFF">Friday:</td>
		    <td height="22" bgcolor="#FFFFFF">StartTime
		        <label>
		            <select name="fristarttime" id="fristarttime" style="font-size:11px; width:60px">
		                <option value="6:00">6:00</option>
		                <option value="6:30">6:30</option>
		                <option value="7:00">7:00</option>
		                <option value="7:30">7:30</option>
		                <option value="8:00">8:00</option>
		                <option value="8:30">8:30</option>
		                <option value="9:00">9:00</option>
		                <option value="9:30" selected="selected">9:30</option>
		                <option value="10:00">10:00</option>
		                <option value="10:30">10:30</option>
		                <option value="11:00">11:00</option>
		                <option value="11:30">11:30</option>
		                <option value="12:00">12:00</option>
		                <option value="12:30">12:30</option>
		                <option value="13:00">13:00</option>
		                <option value="13:30">13:30</option>
		                <option value="14:00">14:00</option>
		                <option value="14:30">14:30</option>
		                <option value="15:00">15:00</option>
		                <option value="15:30">15:30</option>
		                <option value="16:00">16:00</option>
		                <option value="16:30">16:30</option>
		                <option value="17:00">17:00</option>
		                <option value="17:30">17:30</option>
		                <option value="18:00">18:00</option>
		                <option value="18:30">18:30</option>
		                <option value="19:00">19:00</option>
		                <option value="19:30">19:30</option>
		                <option value="20:00">20:00</option>
		                <option value="20:30">20:30</option>
		                <option value="21:00">21:00</option>
		                <option value="21:30">21:30</option>
		                <option value="22:00">22:00</option>
		                <option value="22:30">22:30</option>
		                <option value="23:00">23:00</option>
		                <option value="23:30">23:30</option>
		            </select>
		            EndTime
		            <select name="friendtime" id="friendtime" style="font-size:11px; width:60px">
		                <option value="6:00">6:00</option>
		                <option value="6:30">6:30</option>
		                <option value="7:00">7:00</option>
		                <option value="7:30">7:30</option>
		                <option value="8:00">8:00</option>
		                <option value="8:30">8:30</option>
		                <option value="9:00">9:00</option>
		                <option value="9:30">9:30</option>
		                <option value="10:00">10:00</option>
		                <option value="10:30">10:30</option>
		                <option value="11:00">11:00</option>
		                <option value="11:30">11:30</option>
		                <option value="12:00">12:00</option>
		                <option value="12:30">12:30</option>
		                <option value="13:00">13:00</option>
		                <option value="13:30">13:30</option>
		                <option value="14:00">14:00</option>
		                <option value="14:30">14:30</option>
		                <option value="15:00">15:00</option>
		                <option value="15:30">15:30</option>
		                <option value="16:00">16:00</option>
		                <option value="16:30">16:30</option>
		                <option value="17:00">17:00</option>
		                <option value="17:30">17:30</option>
		                <option value="18:00" selected="selected">18:00</option>
		                <option value="18:30">18:30</option>
		                <option value="19:00">19:00</option>
		                <option value="19:30">19:30</option>
		                <option value="20:00">20:00</option>
		                <option value="20:30">20:30</option>
		                <option value="21:00">21:00</option>
		                <option value="21:30">21:30</option>
		                <option value="22:00">22:00</option>
		                <option value="22:30">22:30</option>
		                <option value="23:00">23:00</option>
		                <option value="23:30">23:30</option>
		            </select>
		        </label></td>
		</tr>
		<tr>
		    <td height="22" bgcolor="#FFFFFF">Saturday:</td>
		    <td height="22" bgcolor="#FFFFFF">StartTime
		        <label>
		            <select name="satstarttime" id="satstarttime" style="font-size:11px; width:60px">
		                <option value="6:00">6:00</option>
		                <option value="6:30">6:30</option>
		                <option value="7:00">7:00</option>
		                <option value="7:30">7:30</option>
		                <option value="8:00">8:00</option>
		                <option value="8:30">8:30</option>
		                <option value="9:00">9:00</option>
		                <option value="9:30" selected="selected">9:30</option>
		                <option value="10:00">10:00</option>
		                <option value="10:30">10:30</option>
		                <option value="11:00">11:00</option>
		                <option value="11:30">11:30</option>
		                <option value="12:00">12:00</option>
		                <option value="12:30">12:30</option>
		                <option value="13:00">13:00</option>
		                <option value="13:30">13:30</option>
		                <option value="14:00">14:00</option>
		                <option value="14:30">14:30</option>
		                <option value="15:00">15:00</option>
		                <option value="15:30">15:30</option>
		                <option value="16:00">16:00</option>
		                <option value="16:30">16:30</option>
		                <option value="17:00">17:00</option>
		                <option value="17:30">17:30</option>
		                <option value="18:00">18:00</option>
		                <option value="18:30">18:30</option>
		                <option value="19:00">19:00</option>
		                <option value="19:30">19:30</option>
		                <option value="20:00">20:00</option>
		                <option value="20:30">20:30</option>
		                <option value="21:00">21:00</option>
		                <option value="21:30">21:30</option>
		                <option value="22:00">22:00</option>
		                <option value="22:30">22:30</option>
		                <option value="23:00">23:00</option>
		                <option value="23:30">23:30</option>
		            </select>
		            EndTime
		            <select name="satendtime" id="satendtime" style="font-size:11px; width:60px">
		                <option value="6:00">6:00</option>
		                <option value="6:30">6:30</option>
		                <option value="7:00">7:00</option>
		                <option value="7:30">7:30</option>
		                <option value="8:00">8:00</option>
		                <option value="8:30">8:30</option>
		                <option value="9:00">9:00</option>
		                <option value="9:30">9:30</option>
		                <option value="10:00">10:00</option>
		                <option value="10:30">10:30</option>
		                <option value="11:00">11:00</option>
		                <option value="11:30">11:30</option>
		                <option value="12:00">12:00</option>
		                <option value="12:30">12:30</option>
		                <option value="13:00">13:00</option>
		                <option value="13:30">13:30</option>
		                <option value="14:00">14:00</option>
		                <option value="14:30">14:30</option>
		                <option value="15:00">15:00</option>
		                <option value="15:30">15:30</option>
		                <option value="16:00">16:00</option>
		                <option value="16:30">16:30</option>
		                <option value="17:00">17:00</option>
		                <option value="17:30">17:30</option>
		                <option value="18:00" selected="selected">18:00</option>
		                <option value="18:30">18:30</option>
		                <option value="19:00">19:00</option>
		                <option value="19:30">19:30</option>
		                <option value="20:00">20:00</option>
		                <option value="20:30">20:30</option>
		                <option value="21:00">21:00</option>
		                <option value="21:30">21:30</option>
		                <option value="22:00">22:00</option>
		                <option value="22:30">22:30</option>
		                <option value="23:00">23:00</option>
		                <option value="23:30">23:30</option>
		            </select>
		        </label></td>
		</tr>
		<tr>
		    <td height="22" bgcolor="#FFFFFF">Sunday:</td>
		    <td height="22" bgcolor="#FFFFFF">StartTime
		        <label>
		            <select name="sunstarttime" id="sunstarttime" style="font-size:11px; width:60px">
		                <option value="6:00">6:00</option>
		                <option value="6:30">6:30</option>
		                <option value="7:00">7:00</option>
		                <option value="7:30">7:30</option>
		                <option value="8:00">8:00</option>
		                <option value="8:30">8:30</option>
		                <option value="9:00">9:00</option>
		                <option value="9:30" selected="selected">9:30</option>
		                <option value="10:00">10:00</option>
		                <option value="10:30">10:30</option>
		                <option value="11:00">11:00</option>
		                <option value="11:30">11:30</option>
		                <option value="12:00">12:00</option>
		                <option value="12:30">12:30</option>
		                <option value="13:00">13:00</option>
		                <option value="13:30">13:30</option>
		                <option value="14:00">14:00</option>
		                <option value="14:30">14:30</option>
		                <option value="15:00">15:00</option>
		                <option value="15:30">15:30</option>
		                <option value="16:00">16:00</option>
		                <option value="16:30">16:30</option>
		                <option value="17:00">17:00</option>
		                <option value="17:30">17:30</option>
		                <option value="18:00">18:00</option>
		                <option value="18:30">18:30</option>
		                <option value="19:00">19:00</option>
		                <option value="19:30">19:30</option>
		                <option value="20:00">20:00</option>
		                <option value="20:30">20:30</option>
		                <option value="21:00">21:00</option>
		                <option value="21:30">21:30</option>
		                <option value="22:00">22:00</option>
		                <option value="22:30">22:30</option>
		                <option value="23:00">23:00</option>
		                <option value="23:30">23:30</option>
		            </select>
		            EndTime
		            <select name="sunendtime" id="sunendtime" style="font-size:11px; width:60px">
		                <option value="6:00">6:00</option>
		                <option value="6:30">6:30</option>
		                <option value="7:00">7:00</option>
		                <option value="7:30">7:30</option>
		                <option value="8:00">8:00</option>
		                <option value="8:30">8:30</option>
		                <option value="9:00">9:00</option>
		                <option value="9:30">9:30</option>
		                <option value="10:00">10:00</option>
		                <option value="10:30">10:30</option>
		                <option value="11:00">11:00</option>
		                <option value="11:30">11:30</option>
		                <option value="12:00">12:00</option>
		                <option value="12:30">12:30</option>
		                <option value="13:00">13:00</option>
		                <option value="13:30">13:30</option>
		                <option value="14:00">14:00</option>
		                <option value="14:30">14:30</option>
		                <option value="15:00">15:00</option>
		                <option value="15:30">15:30</option>
		                <option value="16:00">16:00</option>
		                <option value="16:30">16:30</option>
		                <option value="17:00">17:00</option>
		                <option value="17:30">17:30</option>
		                <option value="18:00" selected="selected">18:00</option>
		                <option value="18:30">18:30</option>
		                <option value="19:00">19:00</option>
		                <option value="19:30">19:30</option>
		                <option value="20:00">20:00</option>
		                <option value="20:30">20:30</option>
		                <option value="21:00">21:00</option>
		                <option value="21:30">21:30</option>
		                <option value="22:00">22:00</option>
		                <option value="22:30">22:30</option>
		                <option value="23:00">23:00</option>
		                <option value="23:30">23:30</option>
		            </select>
		        </label></td>
		</tr>
		<tr bgcolor="#FFFFFF">
		    <td height="22" colspan="2" align="center" bgcolor="#EBEBEB"><label>
		        <input type="submit" name="button" id="button" value="     Save     " style="font-size:11px;"/>
		    </label></td>
		</tr>
		</table>
		</td>
		</tr>
		</table>
	<?php echo form_close(); ?>
	</p>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr >
			<td>&nbsp;</td>
        </tr>
		<tr>
	        <td width="90%">
	        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	          	<tr>
	            	<td height="20" style="text-decoration:underline"><strong>Labor Contract List</strong></td>
				</tr>
				</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0" background="images/Footer.png">
	          <tr>
	            <td><table width="100%" border="0" cellspacing="1" cellpadding="1">
	              <tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
	                <td width="29" height="20">#</td>
	                <td width="113">Contract ID</td>
	                <td width="60">Entry Date</td>
	                <td width="200">Working Time</td>
	                <td width="117">Add Date</td>
	                <td width="30" height="20" align="center" style="color:#FFF">Action</td>
	              </tr>
	              <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
	                <td height="20">1</td>
	                <td>C042001</td>
	                <td>2010/03/10</td>
	                <td>Monday To Friday: 09:30 - 18:00<br>Saturday : 10:00 - 15:00
	                </td>
	                <td>2010/03/10 9:00</td>
	                <td align="center"><a href="javascript:void(0)">view</a>&nbsp;&nbsp;<a href="javascript:void(0)">edit</a></td>
	              </tr>
	              <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
	                <td height="20">2</td>
	                <td>C042002</td>
	                <td>2010/03/10</td>
	                <td>Monday To Friday: 10:30 - 18:00<br>Saturday : 10:00 - 15:00
	                </td>
	                <td>2010/03/10 9:00</td>
	                <td align="center"><a href="javascript:void(0)">view</a>&nbsp;&nbsp;<a href="javascript:void(0)">edit</a></td>
	              </tr>
	              <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
	                <td height="20">3</td>
	                <td>C042003</td>
	                <td>2010/03/10</td>
	                <td>Monday To Friday: 09:30 - 18:00<br>Saturday : 10:00 - 15:00
	                </td>
	                <td>2010/03/10 9:00</td>
	                <td align="center"><a href="javascript:void(0)">view</a>&nbsp;&nbsp;<a href="javascript:void(0)">edit</a></td>
	              </tr>
	            </table></td>
	          </tr>
	          <tr bgcolor="#FFFFFF" >
	          	<td align="right"><br />
	          		Total Records: 208&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)">First</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)">Previous</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)">Next</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)">Last</a>&nbsp;&nbsp;
	          	</td>
	          </tr>
	        </table>
	        </td>
	      </tr>
	    </table>
</div><!-- end tab labor contract-->
<!-- tab working hours start-->
</div><!-- end panes -->
</div><!-- end wizard -->
</body>
</html>
