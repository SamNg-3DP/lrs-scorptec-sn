<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <META HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
		<META HTTP-EQUIV="Expires" CONTENT="0">
        <title>History log of <?=$userinfo->first_name?> <?=$userinfo->last_name?></title>
        <base href="<?=base_url()?>"/>
        <link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />
        <style type="text/css">
            <!--
            #users_form table tr td table {
                text-align: left;
            }
            #users_form p {
                text-align: left;
            }
            #users_form table tr td table tr td {
                font-family: Tahoma, Helvetica, sans-serif;
                font-size: 11px;
            }
			
            -->
        </style>
    </head>
    <body>
	<?php 
	$style = " style='background-color:#FFFF00;color:red;' ";
	?>    	
    		<table width="100%" border="0" cellspacing="1" cellpadding="1">
				<tr bgcolor="#EBEBEB">
					<td height="20" colspan="14" align="left">
					change history log of
						<label style="color:blue;font-size: 14px">
							<?=$userinfo->first_name?> <?=$userinfo->last_name?>   
						</label>( red font: indicate change)
             		</td>
              	</tr>
         	</table>
            <table border="0" align="center" cellpadding="0" cellspacing="0" background="images/Footer.png">
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="1" cellpadding="1">
                            <tr bgcolor="#6495ED" style="font-family: Tahoma, Helvetica, sans-serif; font-size: 12px;">
                                <td width="20" height="20">#</td>
                                <td height="20">Operator</td>
                                <td height="20">Operation</td>
                                <td height="20">Operate Time</td>
                                <td height="20">First Name</td>
                                <td height="20">Last Name</td>
                                <td height="20">Password</td>
                                <td height="20">Department</td>
                                <td height="20">User Level</td>
                                <td height="20">Employment Type</td>
                                <td height="20">Hire Date</td>
                                <td height="20">Address</td>
                                <td height="20">Home Phone</td>
                                <td height="20">Mobile</td>
                                <td height="20">email(work)</td>
                                <td height="20">email(presonal)</td>
                                <td height="20">Date of Birth</td>
                                <td height="20">Kin name</td>
                                <td height="20">Contact details</td>
                                <td height="20">MC</td>
                                <td height="20">Driver License No</td>
                                <td height="20">Passport No</td>
                                <td height="20">TFN</td>
                                <td height="20">Account Name</td>
                                <td height="20">Bank</td>
                                <td height="20">BSB</td>
                                <td height="20">Account No:</td>
                            </tr>
                            <? if (empty($user_logs)) { ?>
                            <tr bgcolor="#FFFFFF">
                                <td height="20" colspan="11" style="color: #F00">No history log!</td>
                            </tr>
                                <? } else { 
                                $num = 0; 
                                $prev = "";
                                foreach ($user_logs as $row): 
    								$num = $num + 1;
    								$b_prev = false;
									if(!empty($prev)){
										$b_prev = true;
									}
    						?>
                            <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
                                <td height="20"><?=$num?></td>
                                <td height="20"><?=$row->operator?></td>
                                <td height="20" <?
                                if($b_prev){
                                	echo " style='color:red;' ";
                                }
                                ?>
                                ><?=$row->operation?></td>
                                <td height="20"><?=date(DATETIME_FORMAT, $row->operate_time)?></td>
                                <td height="20"
                                <?php 
                                if($b_prev){
                                	if($prev->first_name != $row->first_name){
                                		echo $style;
                                	}
                                }
                                ?>
                                ><?=$row->first_name?></td>
                                <td height="20"
                                <?php 
                                if($b_prev){
                                	if($prev->last_name != $row->last_name){
                                		echo $style;
                                	}
                                }
                                ?>
                                ><?=$row->last_name?></td>
                                <td height="20"
                                <?php 
                                if($b_prev){
                                	if($prev->password != $row->password){
                                		echo $style;
                                	}
                                }
                                ?>
                                ><?=$row->password?></td>
                                <td height="20"
                                <?php 
                                if($b_prev){
                                	if($prev->password != $row->password){
                                		echo $style;
                                	}
                                }
                                ?>
                                ><?=$row->department?></td>
                                <td height="20"
                                <?php 
                                if($b_prev){
                                	if($prev->user_level != $row->user_level){
                                		echo $style;
                                	}
                                }
                                ?>
                                ><?=$row->user_level?></td>
                                <td height="20"
                                <?php 
                                if($b_prev){
                                	if($prev->employment_type != $row->employment_type){
                                		echo $style;
                                	}
                                }
                                ?>
                                ><?=$row->employment_type?></td>
                                <td height="20" 
                                <?php 
                                if($b_prev){
                                	if($prev->hire_date != $row->hire_date){
                                		echo $style;
                                	}
                                }
                                ?>
                                >
                                <?
                                if($row->hire_date > 0){
                                	echo date(DATE_FORMAT, $row->hire_date);
                                }else{
                                	echo "";
                                }
                                ?>
                                </td>
                                <td height="20"
                                <?php 
                                if($b_prev){
                                	if($prev->address != $row->address){
                                		echo $style;
                                	}
                                }
                                ?>
                                ><?=$row->address?></td>
                                <td height="20"
                                <?php 
                                if($b_prev){
                                	if($prev->home_phone != $row->home_phone){
                                		echo $style;
                                	}
                                }
                                ?>
                                ><?=$row->home_phone?></td>
                                <td height="20"
                                <?php 
                                if($b_prev){
                                	if($prev->mobile != $row->mobile){
                                		echo $style;
                                	}
                                }
                                ?>
                                ><?=$row->mobile?></td>
                                <td height="20"
                                <?php 
                                if($b_prev){
                                	if($prev->email != $row->email){
                                		echo $style;
                                	}
                                }
                                ?>
                                ><?=$row->email?></td>
                                <td height="20"
                                <?php 
                                if($b_prev){
                                	if($prev->personal_email != $row->personal_email){
                                		echo $style;
                                	}
                                }
                                ?>
                                ><?=$row->personal_email?></td>
                                <td height="20"
                                <?php 
                                if($b_prev){
                                	if($prev->birthday != $row->birthday){
                                		echo $style;
                                	}
                                }
                                ?>
                                >
                                <?php 
                                if(!empty($row->birthday)){
                                	echo date(DATE_FORMAT, $row->birthday);
                                }
                                ?>
                                </td>
                                <td height="20"
                                <?php 
                                if($b_prev){
                                	if($prev->kin_name != $row->kin_name){
                                		echo $style;
                                	}
                                }
                                ?>
                                ><?=$row->kin_name?></td>
                                <td height="20"
                                <?php 
                                if($b_prev){
                                	if($prev->kin_contact != $row->kin_contact){
                                		echo $style;
                                	}
                                }
                                ?>
                                ><?=$row->kin_contact?></td>
                                <td height="20"
                                <?php 
                                if($b_prev){
                                	if($prev->medical_conditions != $row->medical_conditions){
                                		echo $style;
                                	}
                                }
                                ?>
                                ><?=$row->medical_conditions?></td>
                                <td height="20"
                                <?php 
                                if($b_prev){
                                	if($prev->driver_license_no != $row->driver_license_no){
                                		echo $style;
                                	}
                                }
                                ?>
                                ><?=$row->driver_license_no?></td>
                                <td height="20"
                                <?php 
                                if($b_prev){
                                	if($prev->passport_license_no != $row->passport_license_no){
                                		echo $style;
                                	}
                                }
                                ?>
                                ><?=$row->passport_license_no?></td>
                                <td height="20"
                                <?php 
                                if($b_prev){
                                	if($prev->tfn != $row->tfn){
                                		echo $style;
                                	}
                                }
                                ?>
                                ><?=$row->tfn?></td>
                                <td height="20"
                                <?php 
                                if($b_prev){
                                	if($prev->bank_account_name != $row->bank_account_name){
                                		echo $style;
                                	}
                                }
                                ?>
                                ><?=$row->bank_account_name?></td>
                                <td height="20"
                                <?php 
                                if($b_prev){
                                	if($prev->bank_name != $row->bank_name){
                                		echo $style;
                                	}
                                }
                                ?>
                                ><?=$row->bank_name?></td>
                                <td height="20"
                                <?php 
                                if($b_prev){
                                	if($prev->bank_bsb != $row->bank_bsb){
                                		echo $style;
                                	}
                                }
                                ?>
                                ><?=$row->bank_bsb?></td>
                                <td height="20"
                                <?php 
                                if($b_prev){
                                	if($prev->bank_account_no != $row->bank_account_no){
                                		echo $style;
                                	}
                                }
                                ?>
                                ><?=$row->bank_account_no?></td>
                            </tr>
    						<?
    							$prev = $row; 
    							endforeach; 
                            	} 
                            ?>
                        </table>
                     </td>
                </tr>
            </table>
    </body>
</html>