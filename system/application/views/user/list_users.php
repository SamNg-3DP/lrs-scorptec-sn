<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <META HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
		<META HTTP-EQUIV="Expires" CONTENT="0">
        <title>User List</title>
        <base href="<?=base_url()?>"/>
        <link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />
        <style type="text/css">
            <!--
            #users_form table tr td table {
                text-align: left;
            }
            #users_form p {
                text-align: left;
            }
            #users_form table tr td table tr td {
                font-family: Tahoma, Helvetica, sans-serif;
                font-size: 11px;
            }
			
            -->
        </style>
        <script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/jquery.form.js"></script>
		<script type="text/javascript">
		function sort(form_id, orderby){
			$("#page_no").val(1);
			if($("#order_by").val() == orderby){
				if($("#order").val() == "ASC"){
					$("#order").val("DESC");
				}else{
					$("#order").val("ASC");
				}
			}else{
				$("#order_by").val(orderby);
				$("#order").val("ASC");
			}
			return submitForm(form_id);
		}

		function submitForm(form_id){
			var list_users_options = {
				dataType:  'json',
				success: userResponse,
				url: "<?=site_url("user/list_users_ajax")?>"
			};
			$('#'+form_id).ajaxSubmit(list_users_options);
			return false;
		}
		
		function userResponse(responseText, statusText, xhr, $form){
			$("#list_users").html(responseText.content);
			//refreshPageinate(responseText.page_count, responseText.page_no);
		}
		function pagelimit(form){
			$("#page_no").val(1);
			submitForm("users_form");
		}
		function gotopage(form_id, pageNo){
			$("#page_no").val(pageNo);
			submitForm("users_form");
		}
		function users_page(pageNo){
			$("#page_no").val(pageNo);
			submitForm("users_form");
		}

		function search(form_id){
        	$("#order_by").val("");
        	$("#order").val("");
        	$("#filter_site_id").val($("#site_id").val());
        	$("#filter_status_id").val($("#status_id").val());
			$("#filter_department_id").val($("#department_id").val());
			$("#filter_user_level_id").val($("#user_level_id").val());
			$("#filter_employment_type_id").val($("#employment_type_id").val());
			
			$("#page_no").val(1);
			return submitForm(form_id);
		}

		function change_site(value, dept_id){
			var dept_select = document.getElementById(dept_id);
			dept_select.options.length=0;
			$.ajax({
				async: false,
				type: 'POST',
				url: "<?=site_url("approve/dept_by_site_ajax")?>",
				data: {
				  	site_id: value
				},
				success: function(data){
					$.each(data, function(i, dep){
						dept_select.options.add(new Option(dep.name,dep.id));
					});
			  	},
				dataType: 'json'
			});
			search('users_form');
		}
		
		function change_dept(){
			search('users_form');
		}
		
		function change_user_level(){
			search('users_form');
		}
		
		function change_employee_type(){
			search('users_form');
		}
		
		function change_status(){
			search('users_form');
		}
		
		</script>
    </head>
    <body>
        <form id="users_form" name="users_form" method="post" action="">
        	<input type="hidden" id="order_by" name="order_by" value=""/>
        	<input type="hidden" id="order" name="order" value=""/>
        	<input type="hidden" id="filter_status_id" name="filter_status_id" value="<?=$status?>"/>
        	<input type="hidden" id="filter_site_id" name="filter_site_id" value="<?=$site_id?>"/>
        	<input type="hidden" id="filter_department_id" name="filter_department_id" value=""/>
        	<input type="hidden" id="filter_user_level_id" name="filter_user_level_id" value=""/>
        	<input type="hidden" id="filter_employment_type_id" name="filter_employment_type_id" value=""/>
        	<input type="hidden" id="page_no" name="page_no" value=""/>
        	
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" background="images/Footer.png">
                <tr>
                    <td>
                    	<table width="100%" border="0" cellspacing="1" cellpadding="1">
                            <tr bgcolor="#EBEBEB">
                                <td height="20" colspan="14" align="right">
                                    <label>
                                        <a href="<?=site_url('user/add')?>" style="font-size:11px">ADD NEW</a>
                                    </label>
                                </td>
                            </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="1" cellpadding="1">
                        	<tr bgcolor="#FFFFFF">
                        		<td align="center">
                        		Site: 
	                            <?php 
	                            $js = 'id="site_id" style="font-size: 11px; width: 120px;" onChange="change_site(this.value, \'department_id\')"';
								echo form_dropdown("site_id", $sites, "", $js);?>
                        		Department: 
                                <?php 
                                $js = 'id="department_id" style="font-size: 11px; width: 120px;" onChange="change_dept()"';
								echo form_dropdown('department_id', $departments, '', $js);
								?>
								User Level: 
                                <?php
                                $js = 'id="user_level_id" style="font-size: 11px; width: 120px;" onChange="change_user_level()"'; 
								echo form_dropdown('user_level_id', $user_levels, '', $js);
								?>
								Employment type: 
                                <?php
                                $js = 'id="employment_type_id" style="font-size: 11px; width: 120px;" onChange="change_employee_type()"'; 
								echo form_dropdown('employment_type_id', $employment_types, "", $js);
								?>
								Status: 
                                <?php
                                $js = 'id="status_id" style="font-size: 11px; width: 120px;" onChange="change_status()"'; 
								echo form_dropdown('status_id', $statuses, $status, $js);
								?>
								</td>
                            </tr>
                        </table>
                        <div id="list_users">
                        <table width="100%" border="0" cellspacing="1" cellpadding="1">
                            <tr bgcolor="#FFFFFF" style="white-space:nowrap;background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 12px;">
                                <td width="20" height="20">#</td>
                                <td width="20" height="20">Photo</td>
                                <td height="20"><a href="javascript:void(0)" onclick="sort('users_form','user_name');">User Name</a></td>
                                <td height="20"><a href="javascript:void(0)" onclick="sort('users_form','first_name');">First Name</a></td>
                                <td height="20"><a href="javascript:void(0)" onclick="sort('users_form','last_name');">Last Name</a></td>
                                <td height="20">Hire Date</td>
                                <td height="20"><a href="javascript:void(0)" onclick="sort('users_form','review_date');">Review Date</a></td>
                                <td height="20">Age</td>
                                <td height="20">Employment Period</td>
                                <td height="20"><a href="javascript:void(0)" onclick="sort('users_form','birthday');">Date of Birth</a></td>
                                <td height="20"><a href="javascript:void(0)" onclick="sort('users_form','department_id');">Department</a></td>
                                <td height="20"><a href="javascript:void(0)" onclick="sort('users_form','user_level_id');">User Level</a></td>
                                <td height="20">Employment Type</td>
                                <td height="20">email</td>
                                <td height="20">Last Login</td>
                                <td height="20" colspan="6" align="center" style="color:#EEF8FC">Action</td>
                            </tr>
                            <? if (empty($users)) { ?>
                            <tr bgcolor="#FFFFFF">
                                <td height="20" colspan="19" style="color: #F00">No Users!</td>
                            </tr>
                                <? } else { 
                                $num = 0; ?>
    						<? foreach ($users as $row): $num = $num + 1; ?>
                            <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
                                <td height="20"><?=$num?></td>
                                <td height="20">
                                <?
                                if(empty($row->photo_file)){
                                	$photo_url = DIR_PHOTO . "/nophoto.jpg";
									echo "<img src='". $photo_url ."' hight='50' width='40'/>";
                                }else{
									$photo_url = DIR_PHOTO . $row->photo_file;
									echo "<a href='".$photo_url ."' target='_blank'><img src='". $photo_url ."' hight='50' width='40'/></a>";
								}
								?></td>
                                <td height="20"><?=$row->user_name?></td>
                                <td height="20"><?=$row->first_name?></td>
                                <td height="20"><?=$row->last_name?></td>
                                <td height="20">
                                <?
                                if($row->hire_date > 0){
                                	echo date(DATE_FORMAT, $row->hire_date);
                                }else{
                                	echo "";
                                }
                                ?>
                                </td>
                                <td height="20">
                                <?
                                if($row->review_date > 0){
                                	echo date(DATE_FORMAT, $row->review_date);
                                }else{
                                	echo "";
                                }
                                ?>
                                </td>
                                
                                <td>
                                <?
                                	echo calculate_age($row->birthday);
                                ?>
                                </td>
                                <td height="20">
                                <?
                                	echo employment_period($row->hire_date, now());
                                ?>
                                </td>
                                <td height="20">
                                <?php 
                                if(!empty($row->birthday)){
                                	echo date(DATE_FORMAT, $row->birthday);
                                }
                                ?>
                                </td>
                                <td height="20"><?=$row->department?></td>
                                <td height="20"><?=$row->user_level?></td>
                                <td height="20"><?=$row->employment_type?></td>
                                <td height="20"><?=$row->email?></td>
                                <td height="20">
                                <?php 
                                if($row->last_login_time > 0){
                                	echo date(DATETIME_FORMAT, $row->last_login_time); 
                                }else{
                                	echo "";
                                }
                                ?>
                                </td>
                                <td width="20"><a href="<?=site_url("user/history/".$row->user_name)?>" target="_blank">History</a></td>
                                <td width="20"><a href="<?=site_url("user/working_hours/".$row->user_name)?>">Working</a></td>
                                <td width="20"><a href="<?=site_url("user/view_stats/".$row->id)?>">Stats</a></td>
                                <td width="20"><a href="<?=site_url("userinfo/detail/".$row->user_name)?>"><img border="0" src="images/action/b_select.png" alt="" width="16" height="16" /></a></td>
                                <td width="20"><a href="<?php echo site_url("user/edit/".$row->id)?>"><img border="0" src="images/action/b_edit.png" alt="" width="16" height="16" /></a></td>
                                <td width="20" height="22"><a href="<?php echo site_url("user/remove/".$row->user_name)?>"><img border="0" src="images/action/b_drop.png" width="16" height="16" /></a></td>
                            </tr>
    						<? endforeach;?>
    						<tr bgcolor="#FFFFFF" font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                                <td align="right" colspan="19">
                                <?php 
                                	$space = "&nbsp;&nbsp;&nbsp;";

                            		if($page_count > 1){
	//                            		if($page_count > 1){
	//                                		echo $page_no ." / " . $page_count.$space;
	//                                	}
	//                                	if($page_no == 1){
	//                                		echo "First";
	//                                	}else{
	//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(1);'>First</a>";
	//                                	}
	                                	echo $space;
	                                	if($page_no > 1){
	                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . ($page_no - 1) . ");'>Pre.</a>";
	                                	}else{
	                                		echo "Pre.";
	                                	}
	                                	echo $space;
	                                	if($page_no + 1 <= $page_count){
	                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . ($page_no + 1) . ");'>Next</a>";
	                                	}else{
	                                		echo "Next";
	                                	}
	//                                	echo $space;
	//                                	if($page_no == $page_count || $page_count == 0){
	//                                		echo "Last";
	//                                	}else{
	//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . $page_count . ")'>Last</a>";
	//                                	}                            			
                            			echo $space. "GoTo:";
                            			$page_dropdown_id = $form_id.'_page_dropdown';
		                                $js = 'id="'.$page_dropdown_id.'" style="font-size: 11px;" onChange="gotopage(\''.$form_id.'\', this.value)"';
		                                $page_dropdown = array();
		                                for($i = 1; $i <= $page_count; $i++){
		                                	$page_dropdown[$i] = $i;
		                                }
										echo form_dropdown($page_dropdown_id, $page_dropdown, $page_no, $js);
                                	}
                                	echo $space. "Items Per Page:";
                                	$page_limit_id = $form_id.'_limit';
                                	$js = 'id="'.$page_limit_id.'" style="font-size: 11px;" onChange="pagelimit(\''.$form_id.'\')"';
                                	echo form_dropdown($page_limit_id, pagination_option(), PAGE_SIZE, $js);
                                	
                                ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                            </tr>
    						<?
                                } ?>
                        </table>
                        </div>
                     </td>
                </tr>
            </table>
            
        </form>
    </body>
</html>