		<table width="100%" border="0" cellpadding="0" cellspacing="0" background="images/Footer.png" id="labor_list">
	          <tr>
	            <td><table width="100%" border="0" cellspacing="1" cellpadding="1">
	              <tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
	                <td width="29" height="20">#</td>
	                <td width="113">Contract ID</td>
	                <td width="60">From Date</td>
	                <td width="60">To Date</td>
	                <td width="200">Working Time</td>
					<td width="60">Labor</td>
	                <td width="117">Add Date</td>
	                <td width="30" height="20" align="center" style="color:#FFF">Action</td>
	              </tr>
	              <? if (empty($labor_list)) { ?>
					<tr bgcolor="#FFFFFF">
						<td height="20" colspan="11" style="color: #F00">No labor contract!</td>
					</tr>
						<? } else { 
						$num = 0; ?>
    			<? foreach ($labor_list as $row): $num = $num + 1; ?>
	              <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
	                <td height="20"><?=$num?></td>
	                <td>
	                <?php 
	                if($row->employment_type_id == 1){
	                	echo "CF".$row->id;
	                }else if($row->employment_type_id == 2){
	                	echo "CP".$row->id;
	                }else if($row->employment_type_id == 3){
	                	echo "CC".$row->id;
	                }
	                ?></td>
	                <td><?php echo date(DATE_FORMAT, $row->entry_date);?></td>
	                <td>
	                <?php 
	                if($row->expire_date > 0){
	                	echo date(DATE_FORMAT, $row->expire_date);
	                }else{
	                	echo "-";
	                }
	                ?>
	                </td>
	                <td>
	                <?php 
                    if($row->employment_type_id == 1){
                    	$s_date = "";
	                	if(!empty($row->m_start_time)){
	                		$s_date = "Monday To Friday: ".$row->m_start_time." - " . $row->m_end_time;
	                	}
                    	if(!empty($row->s_start_time)){
                    		if(!empty($s_date)){
                    			$s_date .= "<br>";
                    		}
	                		$s_date .= "Saturday: ".$row->m_start_time." - " . $row->s_end_time;
	                	}
	                	echo $s_date;
	                }else if($row->employment_type_id == 2 || $row->employment_type_id == 3){
	                	$s_date = "";
	                	if(!empty($row->monday_start_time)){
	                		$s_date = "Monday: ".$row->monday_start_time." - " . $row->monday_end_time;
	                	}
                    	if(!empty($row->tuesday_start_time)){
                    		if(!empty($s_date)){
                    			$s_date .= "<br>";
                    		}
	                		$s_date .= "Tuesday: ".$row->tuesday_start_time." - " . $row->tuesday_end_time;
	                	}
	                	if(!empty($row->wednesday_start_time)){
                    		if(!empty($s_date)){
                    			$s_date .= "<br>";
                    		}
	                		$s_date .= "Wednesday: ".$row->wednesday_start_time." - " . $row->wednesday_end_time;
	                	}
	                	if(!empty($row->thursday_start_time)){
                    		if(!empty($s_date)){
                    			$s_date .= "<br>";
                    		}
	                		$s_date .= "Thursday: ".$row->thursday_start_time." - " . $row->thursday_end_time;
	                	}
	                	if(!empty($row->friday_start_time)){
                    		if(!empty($s_date)){
                    			$s_date .= "<br>";
                    		}
	                		$s_date .= "Friday: ".$row->friday_start_time." - " . $row->friday_end_time;
	                	}
	                	if(!empty($row->saturday_start_time)){
                    		if(!empty($s_date)){
                    			$s_date .= "<br>";
                    		}
	                		$s_date .= "Saturday: ".$row->saturday_start_time." - " . $row->saturday_end_time;
	                	}
	                	if(!empty($row->sunday_start_time)){
                    		if(!empty($s_date)){
                    			$s_date .= "<br>";
                    		}
	                		$s_date .= "Sunday: ".$row->sunday_start_time." - " . $row->sunday_end_time;
	                	}
	                	echo $s_date;
	                }
	                
	                ?>
	                </td>
					<td>
					<?php 
						if(!is_null($row->labor_contract_file)){
							echo "<a target=_blank href= '".DIR_LABOR_CONTRACTOR . $row->labor_contract_file . "' >download</a>";
						}else{
							echo "-";
						}
					?></td>
	                <td><?php echo date(DATE_FORMAT, $row->add_time);?></td>
	                <td align="center"><a href="javascript:void(0)" onclick="remove_labor(<?=$row->id?>);"><img border="0" src="images/action/b_drop.png" width="16" height="16" /></a></td>
	              </tr>
	              <? endforeach; } ?>
	            </table></td>
	          </tr>	          
	        </table>