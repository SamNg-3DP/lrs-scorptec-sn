<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Edit working hours</title>
<base href="<?=base_url()?>" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />
<style type="text/css">
<!--
#form1 table tr td table {
	font-family: Tahoma, Helvetica, sans-serif;
	font-size: 11px;
}

#form1 p {
	text-align: center;
}

#input,select {
	font-family: Tahoma, Helvetica, Sans-Serif;
	font-size: 11px;
	font-style: normal;
	font-weight: normal;
}
/* tab pane styling */
div.panes div {
	display:none;		
	padding:15px 10px;
	border:1px solid #999;
	border-top:0;
	font-size:14px;
	background-color:#fff;
}

-->
</style>

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
    $.validator.setDefaults({
    	//submitHandler: function() { alert("submitted!"); },
    	highlight: function(input) {
    		$(input).addClass("ui-state-highlight");
    	},
    	unhighlight: function(input) {
    		$(input).removeClass("ui-state-highlight");
    	}
    });
    
    $(function() {
		$("#add_working_hours").validate({
    		rules: {
				working_hours: {
					required: true,
					number: true
				},
				annual_hours_used: {
					required: true,
					number: true
				},
				personal_hours_used: {
					required: true,
					number: true
				},
				accrual_annual_hours: {
					required: true,
					number: true
				},
				accrual_personal_hours: {
					required: true,
					number: true
				}
    		},
    		messages: {
				//entry_date: "Please enter entry date"
    		},
    		submitHandler: function(form) {
				jQuery(form).ajaxSubmit({
					dataType: 'json',
					success: addWorkingHoursResponse,
					resetForm: true
				});
				// !!! Important !!! 
		        // always return false to prevent standard browser submit and page navigation
				return false;
			}
    	});
		workingHoursOnBlur();
    });

    function addWorkingHoursResponse(responseText, statusText, xhr, $form){
        if(responseText.status == "ok"){
        	alert(responseText.message);
        	$("#hours_used").hide();
        	$("#is_init").val(0);
        	$("#history_working_hours").find("tr:gt(0)").remove();
        	$('#history_working_hours tr:last').after(responseText.content);
        	$("#history_working_hours").find("tr:eq(0)").remove();
        }else{
        	alert(responseText.message);
        }
    }

    function workingHoursOnBlur(){
    	var working_hours = $("#working_hours").val();
        if(working_hours && working_hours > 0){
			$("#accrual_annual_hours").val(Math.round(working_hours / $("#annual_Leave_Parameter").val()*100)/100);
			$("#accrual_personal_hours").val(Math.round(working_hours / $("#personal_Leave_Parameter").val()*100)/100);
        }else{
        	$("#accrual_annual_hours").val("");
			$("#accrual_personal_hours").val("");
        }
    }
</script>
</head>
<body>
<!-- tab working hours start-->
<form action="<?=site_url('user/save_working_hours_ajax')?>" method="post" id="add_working_hours" name="add_working_hours">
<input type="hidden" name="user_name" value="<?=$userinfo->user_name?>" id="user_name"/>
<input type="hidden" name="is_init" value="<?=$is_init?>" id="is_init"/>
<input type="hidden" id="annual_Leave_Parameter" name="annual_Leave_Parameter" value="<?=$annual_Leave_Parameter?>"/>
<input type="hidden" id="personal_Leave_Parameter" name="personal_Leave_Parameter" value="<?=$personal_Leave_Parameter?>"/>
<table width="100%" border="0" cellspacing="1" cellpadding="0">
	<tr bgcolor="#FFFFFF">
        <td height="22" colspan="4">User: <?=($userinfo->first_name . " " . $userinfo->last_name)?></td>
    </tr>
    <tr bgcolor="#FFFFFF">
        <td height="22" colspan="4" bgcolor="#EBEBEB"><font size="4">Update User Balance</font></td>
    </tr>
    <?
    	$t = 0;
        $work_hours = "";
        $accrual_hours = "";
        $annual_hours_used = "";
        $left_annual_hours = "";
        $personal_hours_used = "";
        if(!empty($init_working_hour)){
        	$work_hours = $init_working_hour->working_hours;
        	$annual_hours_used = $init_working_hour->annual_hours_used;
        	$personal_hours_used = $init_working_hour->personal_hours_used;;
        }
		?>
    <tr bgcolor="#FFFFFF">
        <td width="100" bgcolor="#FFFFFF">Working hours:</td>
        <td bgcolor="#FFFFFF" width="150">
        <?php
		echo form_input(array(
			'name' => 'working_hours',
			'id' => 'working_hours',
			'tabindex' => $t,
			'value' => $work_hours,
			'style' => "font-size: 11px",
			'onBlur' => 'workingHoursOnBlur()'
		));
		$t++;
		?>
        </td>
        <td width="120" bgcolor="#FFFFFF" align="right">Accrual Annual hours:</td>
        <td bgcolor="#FFFFFF">
        <?php 
		echo form_input("accrual_annual_hours", "", ' id="accrual_annual_hours" style="font-size:11px;"');
		$t++;
		?> (working hours / <a href="<?=site_url('config/leave_hours')?>"><?=$annual_Leave_Parameter?></a>)
        </td>
    </tr>
    <tr bgcolor="#FFFFFF">
        <td width="100" bgcolor="#FFFFFF"></td>
        <td bgcolor="#FFFFFF">
        </td>
        <td width="120" bgcolor="#FFFFFF" align="right">Accrual Personal hours:</td>
        <td bgcolor="#FFFFFF">
        <?php 
		echo form_input("accrual_personal_hours", "", ' id="accrual_personal_hours" style="font-size:11px;"');
		$t++;
		?> (working hours / <a href="<?=site_url('config/leave_hours')?>"><?=$personal_Leave_Parameter?></a>)
        </td>
    </tr>
</table>    
<div id="hours_used">
<table width="100%" border="0" cellspacing="1" cellpadding="0">
    <?php
	if($is_init){
    ?>
    <tr bgcolor="#FFFFFF">
        <td width="60" >Annual Leave hours used:</td>
        <td width="150" align="left">
        <?php 
		echo form_input(array(
			'name' => 'annual_hours_used',
			'id' => 'annual_hours_used',
			'tabindex' => $t,
			'value' => $annual_hours_used,
			'style' => "font-size: 11px"
		));
		$t++;
		echo form_error('annual_hours_used');
		?>
		</td>
		<td width="120"></td>
		<td width="120"></td>
    </tr>
    <tr bgcolor="#FFFFFF">
        <td bgcolor="#FFFFFF">Personal Leave hours used:</td>
        <td bgcolor="#FFFFFF">
        <?php 
		echo form_input(array(
			'name' => 'personal_hours_used',
			'id' => 'personal_hours_used',
			'tabindex' => $t,
			'value' => $personal_hours_used,
			'style' => "font-size: 11px"
		));
		$t++;
		echo form_error('personal_hours_used');
		?>
		</td>
		<td width="120"></td>
		<td width="120"></td>
    </tr>
    <?php }?>
</table>
</div>
<table width="100%" border="0" cellspacing="1" cellpadding="0">
    <tr bgcolor="#FFFFFF">
		<td height="22" colspan="4" align="center" bgcolor="#EBEBEB">
			<label>
				<input type="submit" name="button" id="button" value="     Save     " style="font-size:11px;"/>
			</label>
		</td>
	</tr>
</table>
</form>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr >
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td width="90%">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td height="20" style="text-decoration:underline"><strong>Add history list</strong></td>
        </tr>
    </table>
    <table width="100%" background="images/Footer.png" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td>
        <table width="100%" border="0" cellpadding="1" cellspacing="1" id="history_working_hours">
          <tr style="background-image: url(&quot;images/TableHeader.png&quot;); font-family: Tahoma,Helvetica,sans-serif; font-size: 11px;" bgcolor="#ffffff">
            <td width="29" height="20">#</td>
            <td width="113">Working hours</td>
            <td width="113">Accrual Annual hours</td>
            <td width="113">Accrual Personal hours</td>
            <td width="117">Add Date</td>
            <td style="color: rgb(255, 255, 255);" width="30" align="center" height="20">Action</td>
          </tr>
           	<? 
			if(!empty($working_hourses)){
           		$num = 0;
				foreach ($working_hourses as $row): 
					$num++;
			?>
          	<tr onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';" bgcolor="#ffffff">
            	<td height="20">&nbsp;<?=$num?></td>
            	<td>
            	<?
            	if($row->is_init){
            		echo "Initial value:";
            		echo "<br>&nbsp;working hours:". $row->working_hours;
            		echo "<br>&nbsp;Annual Leave hours used:". $row->annual_hours_used;
            		echo "<br>&nbsp;Personal Leave hours used:". $row->personal_hours_used;
            	}else{
            		echo $row->working_hours;
            	}
            	?>
            	</td>
            	<td>&nbsp;<?=$row->accrual_annual_hours?></td>
            	<td>&nbsp;<?=$row->accrual_personal_hours?></td>
            	<td><?
            	if($row->add_time > 0){
            		echo date(DATETIME_FORMAT, $row->add_time);
            	}
            	?></td>
            	<td align="center"><a href="<?=site_url('user/remove_working/'.$user_name.'/'.$row->id)?>"><img border="0" src="images/action/b_drop.png" width="16" height="16" title="Remove" /></a></td>
          	</tr>
          <?php 
           		endforeach;
			}else{?>
			<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
				<td height="20" colspan="6">No record</td>
            </tr>
			<?
			}
           ?>
        </table>
      	</td>
	  </tr>
    </table>	    
    </td>
  </tr>
</table>
</body>
</html>
