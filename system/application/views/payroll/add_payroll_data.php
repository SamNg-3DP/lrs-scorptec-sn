<tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
	<td width="30" height="20">Staff ID</td>
    <td width="70">Staff Name</td>
    <td width="90">Total Working hours</td>
    <td width="90">Accrued Annual Leave</td>
    <td width="90">Accrued Person Leave</td>
    <td width="90">Annual Leave Used</td>
    <td width="90">Personal Leave Used</td>
    <td width="190">Total Annual Leave Balance at the End of Payroll</td>
    <td width="190">Total Personal Leave Balance at the End of Payroll</td>
    <td colspan="2" align="center">Action</td>
</tr>
<? 
$num = 0;
if(!empty($users)){

foreach ($users as $row): 
?>
<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
    <td height="20"><?=$row['user_id']?></td>
    <td><?=$row['name']?></td>
    <td id="total_working_hours<?=$num?>"><?=$row['total_working_hours']?></td>
    <td id="total_working_hours<?=$num?>"><?=$row['accrued_annual_leave']?></td>
    <td id="total_working_hours<?=$num?>"><?=$row['accrued_personal_leave']?></td>
    <td id="total_working_hours<?=$num?>"><?=$row['annual_leave_used']?></td>
    <td id="total_working_hours<?=$num?>"><?=$row['personal_leave_used']?></td>
    <td id="total_working_hours<?=$num?>"><?=$row['annual_leave_end']?></td>
    <td id="total_working_hours<?=$num?>"><?=$row['personal_leave_end']?></td>
    <td class="request" pdid="<?=$num?>" uid="<?=$row['user_id']?>" name="<?=$row['name']?>">Reqeuests</td>
    <td><p id="process<?=$num?>"><a onClick="process(<?=$num?>)" href="javascript:void(0)" >Precess..</a></p>
    </td>
</tr>
<input type="hidden" name="huser_id<?=$num?>" value="<?=$row['user_id']?>" />
<input type="hidden" name="htotal_working_hours<?=$num?>" value="<?=$row['total_working_hours']?>" />
<input type="hidden" name="haccrued_annual_leave<?=$num?>" value="<?=$row['accrued_annual_leave']?>" />
<input type="hidden" name="haccrued_personal_leave<?=$num?>" value="<?=$row['accrued_personal_leave']?>" />
<input type="hidden" name="hannual_leave_used<?=$num?>" value="<?=$row['annual_leave_used']?>" />
<input type="hidden" name="hpersonal_leave_used<?=$num?>" value="<?=$row['personal_leave_used']?>" />
<input type="hidden" name="hannual_leave_end<?=$num?>" value="<?=$row['annual_leave_end']?>" />
<input type="hidden" name="hpersonal_leave_end<?=$num?>" value="<?=$row['personal_leave_end']?>" />
<? 
$num++;
endforeach;
?>
<script type="text/javascript">
//<![CDATA[

var requesturl = "<?=site_url('payroll/get_requests_by_add')?>";
var start_date = $("#start_date").val();
var end_date = $("#end_date").val();
var payroll_id = "0";

function changeRequest(id, user_id, payroll_data_id){
	var leave_type = $("#leave_type_id" + id); //leave_type_id16
	if(leave_type.length > 0){
		var leave_type_value = leave_type.val();
		$.ajax({
			async: false,
			type: 'POST',
			url: "<?=site_url("payroll/update_payroll_request_ajax")?>",
			data: {
				user_id: user_id,
			  	id: id, //request id
			  	leave_type: leave_type_value,
			  	payroll_id: payroll_id,
			  	start_date: start_date,
			  	end_date: end_date,
			  	payroll_data_id: payroll_data_id
			},
			success: function(data){
				$("#total_working_hours"+payroll_data_id).html(data.total_working_hours);
				$("#accrued_annual_leave"+payroll_data_id).html(data.accrued_annual_leave);
				$("#accrued_personal_leave"+payroll_data_id).html(data.accrued_personal_leave);
				$("#annual_leave_used"+payroll_data_id).html(data.annual_leave_used);
				$("#personal_leave_used"+payroll_data_id).html(data.personal_leave_used);
				$("#annual_leave_end"+payroll_data_id).html(data.annual_leave_end);
				$("#personal_leave_end"+payroll_data_id).html(data.personal_leave_end);
		  	},
			dataType: 'json'
		});
	}
}

$(function() {	
	$('table td.request:not(:empty)').each(function()
	{
	   var self = this;   
	   // Setup title   
	   var title = $(self).attr('name');
	   var uid = $(self).attr('uid');
	   var pdid = $(self).attr('pdid');
	   
	   $(self).qtip({
	      content: {
	         text: "<img src='images/loading.gif'/>",
	         url: requesturl,
	         data: { 
	         	id: uid,
	         	payroll_data: pdid,
	         	start_date: start_date,
	         	end_date: end_date 
	         },
	         method: 'post',
	         title: {
	            text: title,
	            button: 'Close'
	         }
	      },
	      position: {
	         corner: {
	            target: 'leftMiddle',
	            tooltip: 'rightMiddle'
	         }
	      },
	      show: 'click',
	      hide: 'unfocus',//'inactive',
	      style: {
	         border: {
	            width: 2
	         },
	         tip: {
	            corner: 'rightMiddle'
	         },
	         name: 'green',
	         width: {
	            max: 650,
	            min: 500
	         },
	         padding: 14
	      }
	   })
	   .html('<a href="javascript:void(0)">Requests</a>')
	   //.html('Requests')
	   .show();
	});
});
//]]>
</script>

<?
}else{
?>
<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
    <td height="20" colspan="9">No record</td>
</tr>
<?php }
?>
<input type="hidden" id="user_count" name="user_count" value="<?=$num?>" />
