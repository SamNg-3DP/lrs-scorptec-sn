<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add new payroll</title>
<base href="<?=base_url ()?>" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />
</head>
<body>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="25" valign="top" style="font-family: Tahoma, Geneva, sans-serif;font-size: 11px;"><?=$navigation?></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="90%">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="20" style="text-decoration:underline"><strong></strong></td>
                </tr>
            </table>
            <?php echo form_open ( 'payroll/do_add', 'onsubmit="return check()"' ); ?>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" background="images/Footer.png">
                <tr>
                    <td>
                    	<table width="100%" border="0" cellspacing="1" cellpadding="1">
                        	<tr bgcolor="#FFFFFF">
                        		<td align="center">
                        		Start Date:&nbsp;
                        		<input type="text" name="start_date" id="start_date" style="font-size:11px"/>
                        		&nbsp;&nbsp;&nbsp;End Date:&nbsp;
                        		<input type="text" name="end_date" id="end_date" style="font-size:11px"/>
								&nbsp;&nbsp;&nbsp;<input type="button" value="search" onclick="search();" />
								</td>
                            </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="1" cellpadding="1" id="list_data">
                            <tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                            	<td width="30" height="20">Staff ID</td>
                                <td width="70">Staff Name</td>
                                <td width="90">Total Working hours</td>
                                <td width="90">Accrued Annual Leave</td>
                                <td width="90">Accrued Personal Leave</td>
                                <td width="90">Annual Leave Used</td>
                                <td width="90">Personal Leave Used</td>
                                <td width="190">Total Annual Leave Balance at the End of Payroll</td>
                                <td width="190">Total Personal Leave Balance at the End of Payroll</td>
                                <td colspan="2" align="center">Action</td>
                            </tr>
                            <tr bgcolor="#FFFFFF" height="200"><td colspan="11">&nbsp;</td></tr>
                        </table>
                    </td>
                </tr>
                
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                	<td align="center"><input type="submit" name="name" value="process"/></td>
                </tr>
            </table>
            
            </form>
        </td>
    </tr>
</table>
<script type="text/javascript" src="js/jquery-1.3.2.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type='text/javascript' src='js/jquery.qtip.js'></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
//<![CDATA[

function search(){
	var start_date = $("#start_date").val();
	var end_date = $("#end_date").val();
	if(!start_date){
		alert("Please select start date");
		return false;
	}
	if(!end_date){
		alert("Please select end date");
		return false;
	}
	//$(":hidden").remove();
	$("#list_data").find("tr:gt(0)").remove();
	$('#list_data tr:last').after("<tr><td colspan='11' align='center'><img src='images/loading.gif' /></td></tr>");
				
	$.ajax({
		async: false,
		type: 'POST',
		url: "<?=site_url("payroll/get_staff_ajax")?>",
		data: {
		  	start_date: start_date, 
		  	end_date: end_date
		},
		success: function(data){
			var tr0 = $("#list_data").find("tr:gt(0)");
			if(tr0){
				$("#list_data").find("tr:gt(0)").remove();
				$('#list_data tr:last').after(data.content);
				$("#list_data").find("tr:eq(0)").remove();
			}else{
				//$("#list_data").find("tr:gt(0)").remove();
				$('#list_data').html(data.content);
				//$("#list_data").find("tr:eq(0)").remove();
			}
	  	},
		dataType: 'json'
	});
}

function changeRequest(id, user_id, payroll_data_id){
	var leave_type = $("#leave_type_id" + id); //leave_type_id16
	if(leave_type.length > 0){
		var leave_type_value = leave_type.val();
		$.ajax({
			async: false,
			type: 'POST',
			url: "<?=site_url("payroll/update_payroll_request_ajax")?>",
			data: {
				user_id: user_id,
			  	id: id, //request id
			  	leave_type: leave_type_value,
			  	payroll_id: payroll_id,
			  	start_date: start_date,
			  	end_date: end_date,
			  	payroll_data_id: payroll_data_id
			},
			success: function(data){
				$("#total_working_hours"+payroll_data_id).html(data.total_working_hours);
				$("#accrued_annual_leave"+payroll_data_id).html(data.accrued_annual_leave);
				$("#accrued_personal_leave"+payroll_data_id).html(data.accrued_personal_leave);
				$("#annual_leave_used"+payroll_data_id).html(data.annual_leave_used);
				$("#personal_leave_used"+payroll_data_id).html(data.personal_leave_used);
				$("#annual_leave_end"+payroll_data_id).html(data.annual_leave_end);
				$("#personal_leave_end"+payroll_data_id).html(data.personal_leave_end);
		  	},
			dataType: 'json'
		});
	}
}

function check(){
	var user_count = $("#user_count").val();
	if(!user_count || user_count == 0){
		return false;
	}
	return true;
}

function process(id){
	alert("Please click button 'process' firstly, then edit this payroll data from View Payroll History page");
}

$(function() {
	$("#start_date").datepicker({ 
		disabled: true, 
		dateFormat: '<?=JS_DATE_FORMAT?>',
		changeMonth: true,
		changeYear: true 
	});
	$("#end_date").datepicker({ 
		disabled: true, 
		dateFormat: '<?=JS_DATE_FORMAT?>',
		changeMonth: true,
		changeYear: true 
	});
});
//]]>
</script>
</body>
</html>