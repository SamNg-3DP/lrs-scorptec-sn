<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>View Payroll Data</title>
<base href="<?=base_url ()?>" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="90%">
        	<table width="100%" border="0" cellspacing="1" cellpadding="1">
            	<tr bgcolor="#FFFFFF">
            		<td align="left">
            		Payroll ID:&nbsp;<?=$payroll->id?>&nbsp;&nbsp;&nbsp;
            		Start Date:&nbsp;<?=date(DATE_FORMAT, $payroll->start_date)?>&nbsp;&nbsp;&nbsp;
            		&nbsp;&nbsp;&nbsp;End Date:&nbsp;
            		<?=date(DATE_FORMAT, $payroll->end_date)?>
					</td>
                </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="20" style="text-decoration:underline"><strong></strong></td>
                </tr>
            </table>
            <form>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" background="images/Footer.png">
                <tr>
                    <td>                    	
                        <table width="100%" border="0" cellspacing="1" cellpadding="1" id="list_data">
                            <tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                            	<td width="30" height="20">Staff ID</td>
                                <td width="70">Staff Name</td>
                                <td width="90">Total Actual Working hours</td>
                                <td width="90">Total Working hours</td>
                                <td width="90">Accrued Annual Leave</td>
                                <td width="90">Accrued Personal Leave</td>
                                <td width="90">Annual Leave Used</td>
                                <td width="90">Personal Leave Used</td>
                                <td width="90">Unpaid Leave Used</td>
                                <td width="190">Total Annual Leave Balance</td>
                                <td width="190">Total Personal Leave Balance</td>
                                <td width="20">Salary</td>
                                <td colspan="1" align="center">Status</td>
                            </tr>
                            <? 
							$num = 0;
							if(!empty($payrolldatas)){
							
							foreach ($payrolldatas as $row): 
							?>
							<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
							    <td height="20"><?=$row->user_id?></td>
							    <td><? echo $row->first_name . " " . $row->last_name;?></td>
							    <td id="total_working_hours<?=$row->id?>"><?=$row->total_working_hours?></td>
							    <td id="total_working_hours_accrued<?=$row->id?>"><?=$row->total_working_hours_accrued?></td>
							    <td id="accrued_annual_leave<?=$row->id?>"><?=$row->accrued_annual_leave?></td>
							    <td id="accrued_personal_leave<?=$row->id?>"><?=$row->accrued_personal_leave?></td>
							    <td id="annual_leave_used<?=$row->id?>"><?=$row->annual_leave_used?></td>
							    <td id="personal_leave_used<?=$row->id?>"><?=$row->personal_leave_used?></td>
							    <td id="unpaid_leave_used<?=$row->id?>"><?=$row->unpaid_leave_used?></td>
							    <td id="annual_leave_end<?=$row->id?>"><?=$row->annual_leave_end?></td>
							    <td id="personal_leave_end<?=$row->id?>"><?=$row->personal_leave_end?></td>
							    <td id="salary<?=$row->id?>"><?=$row->salary?></td>
							    <td><?
							    	if($row->processed == 0){
							    		?>
							    		<p id="process<?=$row->id?>">Process..</p>
							    		<?
							    	}else if($row->processed == 1){
							    		?>
							    		<p id="process<?=$row->id?>">Processed</p>
							    		<?
							    	}else if($row->processed == 2){
							    		?>
							    		<p id="process<?=$row->id?>">QB Exported</p>
							    		<?
							    	}
							    	?>
							    </td>
							</tr>
							<? 
							$num++;
							endforeach;
							?>
							
							<?
							}else{
							?>
							<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
							    <td height="20" colspan="11">No record</td>
							</tr>
							<?php }
							?>
							<input type="hidden" id="user_count" name="user_count" value="<?=$num?>" />
                        </table>
                    </td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
</table>
</body>
</html>