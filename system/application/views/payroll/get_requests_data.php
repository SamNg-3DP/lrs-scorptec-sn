<table width="100%" border="0" cellspacing="1" cellpadding="1" id="list_data">
    <tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
    	<td width="10" height="20">#</td>
        <td width="70">Request ID</td>
        <td width="90">Start Date</td>
        <td width="117">End Date</td>
        <td width="90">Leave Type</td>
        <td width="90">Requested</td>
        <td width="90">Action</td>
    </tr>
    <? 
	$num = 0;
	if(!empty($requests)){
	$num = 0;
	foreach ($requests as $row): 
		$num++;
		$start_date = "";
		$end_date = "";
		if($row->request_type == 1){
			$start_date = date(DATE_FORMAT,$row->start_date);
			$end_date = date(DATE_FORMAT, $row->end_date);
		}else if($row->request_type == 2){
			$start_date = date(DATETIME_FORMAT,$row->start_date);
			$end_date = date(DATETIME_FORMAT, $row->end_date);
		}
	?>
	<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
	    <td height="20"><?=$num?></td>
        <td height="20">R<?=$row->id?></td>
        <td><?=$start_date?></td>
        <td>
        <?
        	if($row->end_date > 0){
        		echo $end_date;
        	}
        ?>
        </td>
        <td>
        <?
        	$leave_type_id = 'leave_type_id'.$row->id;
        	if($row->leave_type_id == LEAVE_TYPE_ANNUAL
        		or $row->leave_type_id == LEAVE_TYPE_PERSONAL){
        		$js = 'id="'.$leave_type_id.'" style="font-size: 11px; width: 120px;"'; 
            	echo form_dropdown($leave_type_id, $leave_types, $row->leave_type_id, $js);		
        	}else{
        		echo $row->leave_type;
        	}
        ?>
        </td>
        <td>
        <?
        
        if($row->leave_type_id == LEAVE_TYPE_LUNCH
        	|| $row->leave_type_id == LEAVE_TYPE_LATE){
        	echo round($row->hours_used * 60, 2);
        	echo " Minutes";
        }else{
        	echo $row->hours_used;
        	echo " Hours";
        }
        ?>
        </td>
        <td>
        <?
        	$leave_type_id = 'leave_type_id'.$row->id;
        	if($row->leave_type_id == LEAVE_TYPE_ANNUAL
        		or $row->leave_type_id == LEAVE_TYPE_PERSONAL){
        		?>
        		<input type="button" value="Change" onclick="changeRequest('<?=$row->id?>','<?=$user_id?>','<?=$payroll_data_id?>')"/>
        		<?		
        	}else{
        		echo "-";
        	}
        ?>
        </td>
	</tr>
	<? 
	endforeach;
	}else{
	?>
	<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
	    <td height="20" colspan="8">No leave record</td>
	</tr>
	<?php }
	?>
</table>