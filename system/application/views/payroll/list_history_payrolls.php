<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>List Payroll</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<base href="<?=base_url()?>"/>
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript">
function gotopage(form, pageNo){
	$("#" + form + "_page_no").val(pageNo);
	submitForm(form);
}

function pagelimit(form){
	$("#" + form + "_page_no").val(1);
	submitForm(form);
}

function submitForm(form_id){
	if(form_id == "payroll"){
		var payroll_options = {
			dataType:  'json',
			success: payroll_response,
			url: "<?=site_url('payroll/get_history_payrolls_ajax')?>"
		};
		$('#'+form_id).ajaxSubmit(payroll_options);
	}
	
	return false;
}

function payroll_response(responseText, statusText, xhr, $form){
	$("#list_payroll").find("tr:gt(0)").remove();
	$('#list_payroll tr:last').after(responseText.content);
	$("#list_payroll").find("tr:eq(0)").remove();
}

function payroll_page(pageNo){
	$("#payroll_page_no").val(pageNo);
	submitForm("payroll");
}
</script>
</head>

<body>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
    	<td height="20"><strong>Total: <?=$total_count?></strong></td>
	</tr>
  </table>
<form id="<?=$form_id?>" name="<?=$form_id?>" method="post" action="">
  <input type="hidden" id="<?=$form_id?>_page_no" name="<?=$form_id?>_page_no" value=""/>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" background="images/Footer.png">
    <tr>
      <td>
      <table width="100%" border="0" cellspacing="1" cellpadding="0" id="list_<?=$form_id?>">
        <tr style="background-image:url('images/TableHeader.png');">
          <td height="20">ID</td>
          <td>Start Date</td>
          <td>End Date</td>
          <td>Status</td>
          <td colspan="1" align="center">Action</td>
        </tr>
        <? 
		if(count($payrolls) > 0){        
        foreach ($payrolls as $row): ?>
        <tr bgcolor="#FFFFFF">
          <td ><?=$row->id?></td>
          <td ><?=date(DATE_FORMAT,$row->start_date)?></td>
          <td ><?=date(DATE_FORMAT, ($row->end_date - 86400))?></td>
          <td >
          <?
          	echo "QB Exported";
          ?>
          </td>
           <td align="center">
           <a href="<?php echo site_url("payroll/view_history/".$row->id)?>"><img src="images/action/b_select.png" alt="" width="16" height="16" border="0" title="View"/></a>
           </td>
        </tr>
        <? endforeach; ?>
        <tr bgcolor="#FFFFFF" font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
            <td align="right" colspan="6">
            <?php 
            	$space = "&nbsp;&nbsp;&nbsp;";

        		if($page_count > 1){
//                            		if($page_count > 1){
//                                		echo $page_no ." / " . $page_count.$space;
//                                	}
//                                	if($page_no == 1){
//                                		echo "First";
//                                	}else{
//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(1);'>First</a>";
//                                	}
                	echo $space;
                	if($page_no > 1){
                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . ($page_no - 1) . ");'>Pre.</a>";
                	}else{
                		echo "Pre.";
                	}
                	echo $space;
                	if($page_no + 1 <= $page_count){
                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . ($page_no + 1) . ");'>Next</a>";
                	}else{
                		echo "Next";
                	}
//                                	echo $space;
//                                	if($page_no == $page_count || $page_count == 0){
//                                		echo "Last";
//                                	}else{
//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . $page_count . ")'>Last</a>";
//                                	}                            			
        			echo $space. "GoTo:";
        			$page_dropdown_id = $form_id.'_page_dropdown';
                    $js = 'id="'.$page_dropdown_id.'" style="font-size: 11px;" onChange="gotopage(\''.$form_id.'\', this.value)"';
                    $page_dropdown = array();
                    for($i = 1; $i <= $page_count; $i++){
                    	$page_dropdown[$i] = $i;
                    }
					echo form_dropdown($page_dropdown_id, $page_dropdown, $page_no, $js);
            	}
            	echo $space. "Items Per Page:";
            	$page_limit_id = $form_id.'_limit';
            	$js = 'id="'.$page_limit_id.'" style="font-size: 11px;" onChange="pagelimit(\''.$form_id.'\')"';
            	echo form_dropdown($page_limit_id, pagination_option(), PAGE_SIZE, $js);
            	
            ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
        </tr>
        <?
		}else{
			?>
			<tr bgcolor="#FFFFFF">
			    <td height="20" colspan="6">No records.</td>
			</tr>
			<?
		}
        ?>
      </table></td>
    </tr>
  </table>
</form>
</body>
</html>