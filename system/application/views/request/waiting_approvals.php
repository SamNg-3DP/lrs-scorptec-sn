<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="90%">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="20" style="text-decoration:underline"><strong>Waiting Approval</strong></td>
                </tr>
            </table>
            <form id="waiting" name="waiting" method="post" action="">
        	<input type="hidden" id="waiting_order_by" name="waiting_order_by" value=""/>
        	<input type="hidden" id="waiting_order" name="waiting_order" value=""/>
        	<input type="hidden" id="waiting_filter_department_id" name="waiting_filter_department_id" value="<?=$department_id?>"/>
        	<input type="hidden" id="waiting_filter_leave_type_id" name="waiting_filter_leave_type_id" value=""/>
        	<input type="hidden" id="waiting_page_no" name="waiting_page_no" value=""/>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" background="images/Footer.png">
                <tr>
                    <td>
                    	<table width="100%" border="0" cellspacing="1" cellpadding="1">
                        	<tr bgcolor="#FFFFFF">
                        		<td align="center">
                        		Department: 
                                <?php 
                                $js = 'id="waiting_department_id" style="font-size: 11px; width: 120px;"';
								echo form_dropdown('waiting_department_id', $departments, $department_id, $js);
								?>  
								&nbsp;&nbsp;Leave Type: 
                                <?php
                                $js = 'id="waiting_leave_type_id" style="font-size: 11px; width: 120px;"'; 
								echo form_dropdown('waiting_leave_type_id', $leave_types, '', $js);
								?>
								<input type="button" value="search" onclick="search('waiting');" />
								</td>
                            </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="1" cellpadding="1" id="list_waiting">
                            <tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                                <td width="70"><a href="javascript:void(0)" onclick="sort('waiting','id');">Request ID</a></td>
                                <td width="100"><a href="javascript:void(0)" onclick="sort('waiting','add_time');">Request Date</a></td>
                                <td width="100"><a href="javascript:void(0)" onclick="sort('waiting','applicant');">Applicant</a></td>
                                <td width="117"><a href="javascript:void(0)" onclick="sort('waiting','start_date');">Start Date</a></td>
                                <td width="117">End Date</td>
                                <td width="117"><a href="javascript:void(0)" onclick="sort('waiting','department_id');">Department</a></td>
                                <td width="117">Hours Requested</td>
                                <td width="190"><a href="javascript:void(0)" onclick="sort('waiting','leave_type_id');">Leave Type</a></td>
                                <td width="190">Reason</td>
                                <td width="30" height="20" align="center" style="color:#FFF">Action</td>
                            </tr>
                            <? 
                            if(!empty($approvals)){
                            foreach ($approvals as $row): ?>
                            <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
                                <td height="20"><?=$row->id?></td>
                                <td><?=date(DATETIME_FORMAT,$row->add_time)?></td>
                                <td height="20"><?=$row->first_name?> <?=$row->last_name?></td>
                                <td><?=(date(DATE_FORMAT, $row->start_day) . " " . $row->start_time)?></td>
                                <td><?=(date(DATE_FORMAT, $row->end_day) . " " . $row->end_time)?></td>
                                <td><?=$row->department?></td>
                                <td>TODO..</td>
                                <td><?=$row->leave_type?></td>
                                <td><?=$row->reason?></td>
                                <td align="center"><a href="<?=site_url("approve/deal/" . $row->id)?>">approve</a></td>
                            </tr>
                            <? 
                            endforeach;
                            ?>
                            <tr bgcolor="#FFFFFF" font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                                <td align="right" colspan="10">
                                <?php 
                                	$space = "&nbsp;&nbsp;&nbsp;";
                                	if($page_no == 1){
                                		echo "First";
                                	}else{
                                		echo "<a href='javascript:void()' onclick='waitingGotoPage(1);'>First</a>";
                                	}
                                	echo $space;
                                	if($page_no > 1){
                                		echo "<a href='javascript:void()' onclick='waitingGotoPage(" . ($page_no - 1) . ");'>previous</a>";
                                	}else{
                                		echo "previous";
                                	}
                                	echo $space;
                                	if($page_no < $page_count){
                                		echo "<a href='javascript:void()' onclick='waitingGotoPage(" . ($page_no + 1) . ");'>next</a>";
                                	}else{
                                		echo "next";
                                	}
                                	echo $space;
                                	if($page_no == $page_count || $page_count == 0){
                                		echo "Last";
                                	}else{
                                		echo "<a href='javascript:void()' onclick='waitingGotoPage(" . $page_count . ")'>Last</a>";
                                	}
                                ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                            </tr>
                            <?php 
                            }else{
                            ?>
							<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
                                <td height="20" colspan="10">No record</td>
                            </tr>
                            <?php }?>
                        </table>
                    </td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
</table>