<base href="<?=base_url()?>"/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="90%">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="20" style="text-decoration:underline"><strong><?php echo $state_name;?></strong></td>
                </tr>
            </table>
            <form id="<?=$form_id?>" name="<?=$form_id?>" method="post" action="">
        	<input type="hidden" id="<?=$form_id?>_order_by" name="<?=$form_id?>_order_by" value=""/>
        	<input type="hidden" id="<?=$form_id?>_order" name="<?=$form_id?>_order" value=""/>
        	<input type="hidden" id="<?=$form_id?>_filter_site_id" name="<?=$form_id?>_filter_site_id" value="<?=$this->session->userdata('site_id')?>"/>
        	<input type="hidden" id="<?=$form_id?>_filter_department_id" name="<?=$form_id?>_filter_department_id" value="<?=$department_id?>"/>
        	<input type="hidden" id="<?=$form_id?>_filter_leave_type_id" name="<?=$form_id?>_filter_leave_type_id" value=""/>
        	<input type="hidden" id="<?=$form_id?>_filter_time_period_id" name="<?=$form_id?>_filter_time_period_id" value=""/>
        	<input type="hidden" id="<?=$form_id?>_filter_first_name" name="<?=$form_id?>_filter_first_name" value=""/>
        	<input type="hidden" id="<?=$form_id?>_filter_last_name" name="<?=$form_id?>_filter_last_name" value=""/>
        	<input type="hidden" id="<?=$form_id?>_page_no" name="<?=$form_id?>_page_no" value=""/>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" background="images/Footer.png">
                <tr>
                    <td>
                    	<table width="100%" border="0" cellspacing="1" cellpadding="1">
                        	<tr bgcolor="#FFFFFF">
                        		<td align="center">
                        		<?
                        		$site = $form_id.'_site_id';
                        		$department = $form_id.'_department_id';
                        		if($this->session->userdata('site_id') == 0){
                        		?>
	                        		Site: 
	                                <?php 
	                                $js = 'id="'.$site.'" style="font-size: 11px; width: 120px;" onChange="change_site(this.value, \'' . $department . '\')"';
									echo form_dropdown($site, $sites, "", $js);
									echo "Dept.: ";
									$js = 'id="'.$department.'" style="font-size: 11px; width: 120px;"';
									echo form_dropdown($department, $departments, "", $js);
                        		}else{
                        			echo '<input type="hidden" id="'. $site .'" name="'. $site .'" value="'. $this->session->userdata('site_id') .'"/>';
                        			echo "Dept.: ";
									$js = 'id="'.$department.'" style="font-size: 11px; width: 120px;"';
									echo form_dropdown($department, $departments, $department_id, $js);
                        		}
								?>
								&nbsp;&nbsp;Time period: 
								<?php
                                $time_period_id = $form_id.'_time_period_id';
                                $js = 'id="'.$time_period_id.'" style="font-size: 11px; width: 120px;"'; 
								echo form_dropdown($time_period_id, time_period(), '', $js);
								?>
								First Name:<input type="text" name="<?=$form_id?>_first_name" id="<?=$form_id?>_first_name" style="font-size:11px; width: 50px;"/>
								Last Name:<input type="text" name="<?=$form_id?>_last_name" id="<?=$form_id?>_last_name" style="font-size:11px; width: 50px;"/>
								<input type="button" value="search" onclick="search_past('<?=$form_id?>');" />
								</td>
                            </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="1" cellpadding="1" id="list_<?=$form_id?>">
                            <tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;" >
                                <td width="20">#</td>
                                <td width="70"><a href="javascript:void(0)" onclick="sort('<?=$form_id?>','id');">Request ID</a></td>
                                <td width="100"><a href="javascript:void(0)" onclick="sort('<?=$form_id?>','add_time');">Request Date</a></td>
                                <td width="100"><a href="javascript:void(0)" onclick="sort('<?=$form_id?>','applicant');">Applicant</a></td>
                                <td width="117"><a href="javascript:void(0)" onclick="sort('<?=$form_id?>','start_date');">Start Date</a></td>
                                <td width="117">End Date</td>
                                <td width="117"><a href="javascript:void(0)" onclick="sort('<?=$form_id?>','department_id');">Department</a></td>
                                <td width="117">Requested</td>
                                <td width="190"><a href="javascript:void(0)" onclick="sort('<?=$form_id?>','leave_type_id');">Leave Type</a></td>
                                <td width="190">Reason</td>
                                <td width="30" height="20" align="center" style="color:#FFF">Action</td>
                            </tr>
                            <? 
                            if(!empty($approvals)){
                            $num = 0;
                            foreach ($approvals as $row): 
                            	$num++;
                            ?>
                            <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';" onclick="view_request(<?=$row->id?>)">
                                <td height="20"><?=$num?></td>
                                <td height="20">R<?=$row->id?></td>
                                <td><?=date(DATETIME_FORMAT,$row->add_time)?></td>
                                <td height="20"><?=$row->first_name?> <?=$row->last_name?></td>
                                <td><?=(date(DATE_FORMAT, $row->start_day) . " " . $row->start_time)?></td>
                                <td>
                                <?
                                	if($row->end_date > 0){
                                		echo date(DATETIME_FORMAT, $row->end_date);
                                	}
                                ?>
                                </td>
                                <td><?=$row->department?></td>
                                <td>
                                <?
                                if($row->leave_type_id == LEAVE_TYPE_LUNCH
                                	|| $row->leave_type_id == LEAVE_TYPE_LATE){
                                	$hours_used = $row->hours_used * 60; 
                                	echo $hours_used." Minutes";
                                }else{
                                	echo $row->hours_used." Hours";
                                }
                                ?>
                                </td>
                                <td><?=$row->leave_type?></td>
                                <td><?=$row->reason?></td>
                                <td align="center"><a href="<?=site_url("approve/view/" . $row->id)?>">view</a></td>
                            </tr>
                            <? 
                            endforeach;
                            ?>
                            <tr bgcolor="#FFFFFF" font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                                <td align="right" colspan="11">
                                <?php 
                                	$space = "&nbsp;&nbsp;&nbsp;";

                            		if($page_count > 1){
	//                            		if($page_count > 1){
	//                                		echo $page_no ." / " . $page_count.$space;
	//                                	}
	//                                	if($page_no == 1){
	//                                		echo "First";
	//                                	}else{
	//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(1);'>First</a>";
	//                                	}
	                                	echo $space;
	                                	if($page_no > 1){
	                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . ($page_no - 1) . ");'>Pre.</a>";
	                                	}else{
	                                		echo "Pre.";
	                                	}
	                                	echo $space;
	                                	if($page_no + 1 <= $page_count){
	                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . ($page_no + 1) . ");'>Next</a>";
	                                	}else{
	                                		echo "Next";
	                                	}
	//                                	echo $space;
	//                                	if($page_no == $page_count || $page_count == 0){
	//                                		echo "Last";
	//                                	}else{
	//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . $page_count . ")'>Last</a>";
	//                                	}                            			
                            			echo $space. "GoTo:";
                            			$page_dropdown_id = $form_id.'_page_dropdown';
		                                $js = 'id="'.$page_dropdown_id.'" style="font-size: 11px;" onChange="gotopage(\''.$form_id.'\', this.value)"';
		                                $page_dropdown = array();
		                                for($i = 1; $i <= $page_count; $i++){
		                                	$page_dropdown[$i] = $i;
		                                }
										echo form_dropdown($page_dropdown_id, $page_dropdown, $page_no, $js);
                                	}
                                	echo $space. "Items Per Page:";
                                	$page_limit_id = $form_id.'_limit';
                                	$js = 'id="'.$page_limit_id.'" style="font-size: 11px;" onChange="pagelimit(\''.$form_id.'\')"';
                                	echo form_dropdown($page_limit_id, pagination_option(), PAGE_SIZE, $js);
                                	
                                ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                            </tr>
                            <?php 
                            }else{
                            ?>
							<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
                                <td height="20" colspan="11">No record</td>
                            </tr>
                            <?php }?>
                        </table>
                    </td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
</table>