<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="90%">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="20" style="text-decoration:underline"><strong><?php echo $state_name;?></strong></td>
                </tr>
            </table>
            <form id="<?=$form_id?>" name="<?=$form_id?>" method="post" action="">
        	<input type="hidden" id="<?=$form_id?>_order_by" name="<?=$form_id?>_order_by" value=""/>
        	<input type="hidden" id="<?=$form_id?>_order" name="<?=$form_id?>_order" value=""/>
        	<input type="hidden" id="<?=$form_id?>_filter_time_period_id" name="<?=$form_id?>_filter_time_period_id" value=""/>
        	<input type="hidden" id="<?=$form_id?>_page_no" name="<?=$form_id?>_page_no" value=""/>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" background="images/Footer.png">
                <tr>
                    <td>
                    	<table width="100%" border="0" cellspacing="1" cellpadding="1">
                        	<tr bgcolor="#FFFFFF">
                        		<td align="center">
                        		Time period: 
								<?php
                                $time_period_id = $form_id.'_time_period_id';
                                $js = 'id="'.$time_period_id.'" style="font-size: 11px; width: 120px;"'; 
								echo form_dropdown($time_period_id, time_period(), '', $js);
								?>
								<input type="button" value="search" onclick="search_past('<?=$form_id?>');" />
								</td>
                            </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="1" cellpadding="1" id="list_<?=$form_id?>">
                            <tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                            	<td width="20" height="20">#</td>
                                <td width="80"><a href="javascript:void(0)" onclick="sort('<?=$form_id?>','id');">Request ID</a></td>
                                <td width="90">Start Date</td>
                                <td width="90">End Date</td>
								<!--<td width="190">Leave Type</td>-->
                                <td width="117">Requested</td>
                                <td>Reason</td>
                                <td width="90">Request Date</td>
                                <td height="20" width="50" align="center" style="color:#FFF">Action</td>
                            </tr>
                            <? 
                            if(!empty($requests)){
                            	$num = 0;
                            foreach ($requests as $row): 
                            	$num++;
                            ?>
                            <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';" onclick="view_request(<?=$row->id?>)">
                                <td height="20"><?=$num?></td>
                                <td>R<?=$row->id?></td>
                                <td><?=(date(DATE_FORMAT, $row->start_day) . " " . $row->start_time)?></td>
                                <td><?=(date(DATE_FORMAT, $row->end_day) . " " . $row->end_time)?></td>
                                <!--<td><?=$row->leave_type?></td>-->
                                <td>
                                <?
                                if($row->leave_type_id == LEAVE_TYPE_LUNCH
                                	|| $row->leave_type_id == LEAVE_TYPE_LATE){
                                	$hours_used = $row->hours_used * 60; 
                                	echo round($hours_used, 2)." Minutes";
                                }else{
                                	echo $row->hours_used." Hours";
                                }
                                ?>
                                </td>
                                <td><?=$row->reason?></td>
                                <td><?=date(DATETIME_FORMAT,$row->add_time)?></td>
                                <td align="center">
                                	<?
                                	if($row->state == 10){
                                		?>
                                		<a href="<?=site_url('request/edit/'.$row->id)?>">edit</a>&nbsp;
                                		<a href="javascript:void(0)" onclick="cancel_request(<?=$row->id?>);">cancel</a>
                                		<?
                                	}else if($row->state == 20 || $row->state == 30){
                                		?>
                                		<a href="<?=site_url('request/view/'.$row->id)?>">view</a>&nbsp;
                                		<a href="javascript:void(0)" onclick="cancel_request('<?=$form_id?>', <?=$row->id?>);">cancel</a>
                                		<?
                                	}else{
                                	?>
                                		<a href="<?=site_url('request/view/'.$row->id)?>">view</a>&nbsp;
                                	<?
                                	}
                                	?>
                                </td>
                            </tr>
                            <? 
                            endforeach;
                            ?>
                            <tr bgcolor="#FFFFFF" font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                                <td align="right" colspan="9">
                                <?php 
                                	$space = "&nbsp;&nbsp;&nbsp;";

                            		if($page_count > 1){
	//                            		if($page_count > 1){
	//                                		echo $page_no ." / " . $page_count.$space;
	//                                	}
	//                                	if($page_no == 1){
	//                                		echo "First";
	//                                	}else{
	//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(1);'>First</a>";
	//                                	}
	                                	echo $space;
	                                	if($page_no > 1){
	                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . ($page_no - 1) . ");'>Pre.</a>";
	                                	}else{
	                                		echo "Pre.";
	                                	}
	                                	echo $space;
	                                	if($page_no + 1 <= $page_count){
	                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . ($page_no + 1) . ");'>Next</a>";
	                                	}else{
	                                		echo "Next";
	                                	}
	//                                	echo $space;
	//                                	if($page_no == $page_count || $page_count == 0){
	//                                		echo "Last";
	//                                	}else{
	//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . $page_count . ")'>Last</a>";
	//                                	}                            			
                            			echo $space. "GoTo:";
                            			$page_dropdown_id = $form_id.'_page_dropdown';
		                                $js = 'id="'.$page_dropdown_id.'" style="font-size: 11px;" onChange="gotopage(\''.$form_id.'\', this.value)"';
		                                $page_dropdown = array();
		                                for($i = 1; $i <= $page_count; $i++){
		                                	$page_dropdown[$i] = $i;
		                                }
										echo form_dropdown($page_dropdown_id, $page_dropdown, $page_no, $js);
                                	}
                                	echo $space. "Items Per Page:";
                                	$page_limit_id = $form_id.'_limit';
                                	$js = 'id="'.$page_limit_id.'" style="font-size: 11px;" onChange="pagelimit(\''.$form_id.'\')"';
                                	echo form_dropdown($page_limit_id, pagination_option(), PAGE_SIZE, $js);
                                	
                                ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                            </tr>
                            
                            <?
                            }else{
                            ?>
							<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
                                <td height="20" colspan="9">No record</td>
                            </tr>
                            <?php }?>
                        </table>
                    </td>
                </tr>
            </table>
            </form>
        </td>
    </tr>
</table>