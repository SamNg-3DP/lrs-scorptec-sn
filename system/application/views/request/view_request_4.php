<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>View leave request</title>
<base href="<?=base_url()?>" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>


<script type="text/javascript">
//<![CDATA[
function count() {
	var request_full_day = document.getElementById('request_full_day');
	var request_partial_day = document.getElementById('request_partial_day');
	if(request_full_day.checked){
		var start_day = document.getElementById("start_day").value;
	    var end_day = document.getElementById("end_day").value;
	    if(start_day && end_day){
			var start = start_day.split('-');
			var dstart = new Date(start[2], start[1], start[0]);
			var end = end_day.split('-');
			var dend = new Date(end[2], end[1], end[0]);
			if(dstart.getTime() > dend.getTime()){
	        	alert("Start day shouldn't be later than end day.");        
	        	return false;
			}
			calculate();
	    }
	}
	if(request_partial_day.checked){
		var start_day = document.getElementById("start_day").value;
	    var start_time = document.getElementById("start_time").value;
	    var end_time = document.getElementById("end_time").value;
	    
	    if(start_day){
	        if(start_time && end_time && start_time > end_time){
	            alert("Start date shouldn't be later than end date.");
	            return false;
	        }
	        if(start_time && end_time && start_time == end_time){
	            alert("Leave time must be greater than zero!");
	            return false;
	        }
	        if(start_day && start_time && end_time){
		    	calculate();
		    }
	    }
	}
    return true;
}

function calculate(){
	var request_full_day = document.getElementById('request_full_day');
	var request_partial_day = document.getElementById('request_partial_day');
	var user_id = document.getElementById('user_id').value;
	var start_day = "";
	var end_day = "";
	var start_time = "";
	var end_time = "";
	var include_break = false;
	var request_type = 0;

	
	if(request_full_day.checked){
		start_day = jQuery("#start_day").val();
		end_day = jQuery("#end_day").val();
		start_time = "00:00";
		end_time = "23:59";
		request_type = 1;
	}else if(request_partial_day.checked){
		start_day = jQuery("#start_day").val();
		end_day = start_day;
		start_time = jQuery("#start_time").val();
		end_time = jQuery("#end_time").val();
		var include_break_ojb = document.getElementById('include_break');
		include_break = include_break_ojb.checked;
		request_type = 2;
	}
	jQuery.ajax({
		async: false,
		type: 'POST',
		url: "<?=site_url("request/calculate_working_hours_ajax")?>",
		data: {
		  	start_day: start_day, 
		  	start_time: start_time,
		  	end_day: end_day,
		  	end_time: end_time,
		  	request_type: request_type,
		  	include_break: include_break,
		  	user_id: user_id
		},
		success: function(data){
			jQuery("#hours_used").val(data.used_hours_count);
	  	},
		dataType: 'json'
	});
}

function calculate_late(){
	jQuery.ajax({
		async: false,
		type: 'POST',
		url: "<?=site_url("request/calculate_minutes_late_ajax")?>",
		data: {
			arrival_day: jQuery("#arrival_day").val(), 
			arrival_time: jQuery("#arrival_time").val()
		},
		success: function(data){
			if(data.status == "ok"){
				jQuery("#minutes_used").val(data.used_minutes_count);
			}
	  	},
		dataType: 'json'
	});
}


function changeLeaveType(value){

    if(value == <?=LEAVE_TYPE_LATE?>
		|| value == <?=LEAVE_TYPE_LUNCH?>
		|| value == <?=LEAVE_TYPE_CLOCK?>){
	    jQuery("#common_type").hide();
	    jQuery("#medicalCertificate").hide();
	    jQuery("#carer").hide();
	    jQuery("#late_type").show();
	    
	    if (value == <?=LEAVE_TYPE_CLOCK?>){
	    	jQuery("#minute_used").hide();
	    }else{
	    	jQuery("#minute_used").show();
	    }
	}else{
		jQuery("#common_type").show();
	    jQuery("#late_type").hide();
	    jQuery("#medicalCertificate").hide();
	    jQuery("#carer").hide();
	    jQuery("#minute_used").show();
		if(value == "<?=LEAVE_TYPE_PERSONAL?>"){
	        jQuery("#medicalCertificate").show();
	        jQuery("#carer").show();
	    }
	}
}

function fullDayClick(value){
	if(value == 1){
		jQuery("#start_time_type").hide();
		jQuery("#end_time_type").hide();
		jQuery("#include_break_type").hide();
	    jQuery("#end_day_type").show();
	}else{
		jQuery("#end_day_type").hide();
		jQuery("#start_time_type").show();
		jQuery("#end_time_type").show();
		jQuery("#include_break_type").show();
	}
}

function breakClick(){
	count();
}

function isNumber(n) {
	  return !isNaN(parseFloat(n)) && isFinite(n);
}

function isTime(strTime) {
	var strSeparator = ":";
	var intHour;
	var intMinute;
	var strTimeArray = strTime.split(strSeparator);
	if(strTimeArray.length!=2){   
		alert('Prompt\r\nPlease enter correct format time,E.g. HH:mm');
		return false;   
	}
	intHour = parseInt(strTimeArray[0],10);
	intMinute = parseInt(strTimeArray[1],10);
	if(!isNumber(intHour) || !isNumber(intMinute) 
			|| intHour < 1 || intHour > 23 
			|| intMinute < 0 || intMinute > 59){
		alert('Prompt\r\nPlease enter correct format time,E.g. HH:mm');
		return false;
	}
	return true;
}

function blurTime(){
	if(!jQuery("#arrival_day").val()){
		alert("Please select arrival day!");
		return false;
	}
	if(!jQuery("#arrival_time").val()){
		alert("Please enter arrival time!");
		return false;
	}
	if(isTime(jQuery("#arrival_time").val())){
		var leave_type = jQuery("#leave_type").val();
		if(leave_type && leave_type == "<?=LEAVE_TYPE_LATE?>"){
			return calculate_late();
		}
	}
	return true;
}

function count_late(){
	if(!jQuery("#arrival_day").val()){
		alert("Please select arrival day!");
		return false;
	}
	if(jQuery("#arrival_time").val()){
		return blurTime();
	}
	return true;
}


jQuery(function() {
    jQuery("#start_day").datepicker({ 
    	disabled: true, 
    	dateFormat: '<?=JS_DATE_FORMAT?>',
    	changeMonth: true,
		changeYear: true 
	});
    jQuery("#end_day").datepicker({ 
    	disabled: true, 
    	dateFormat: '<?=JS_DATE_FORMAT?>',
    	changeMonth: true,
		changeYear: true 
	});
    jQuery("#arrival_day").datepicker({ 
    	disabled: true, 
    	dateFormat: '<?=JS_DATE_FORMAT?>',
    	changeMonth: true,
		changeYear: true 
	});
//    changeLeaveType(jQuery("#leave_type").val());
});


function confirm_delete(value){
	if(confirm("Do you confirm deleting this request?") ){
		jQuery("#operate").val(value);
		
		var theForm = document.getElementById("deal");
		theForm.submit();
	}
}

//]]>
</script>
    </head>

    <body onLoad="<?php echo "changeLeaveType(".$request->leave_type_id."); "; if ($request->request_type == 1 ) echo "fullDayClick(1)"; else echo "fullDayClick(0)";?>">
    <div class="wrap">
	  <!-- the tabs --> 
	  <ul class="tabs">
	    <li><a href="#View">View</a></li>
	    <li><a href="#Logs">Logs</a></li> 
	  </ul>
	  <!-- tab "panes" --> 
	  <div class="panes">
	    <div class="pane" style="display:block" id="tab_update">
	    <?php 
			echo form_open('approve/do_update_deal', array('id'=>'deal', 'name'=>'deal' )); 
	 		echo form_hidden("request_id", $request->id);
 		?>
 		<input type="hidden" id="operate" name="operate" value=""/>
 		<input type="hidden" id="user_id" value="<?php echo $request->user_id;?>" >
        <table width="100%" height="22" border="0" align="center" cellpadding="0" cellspacing="0" background="images/Footer.png">
            <tr>
                <td>
                    <table width="100%" height="50" border="0" cellspacing="1" cellpadding="1">
                        <tr bgcolor="#EBEBEB">
                            <td width="50%" height="20" colspan="2" align="right" bgcolor="#EBEBEB" style="color: red">&nbsp;</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20"  align="right">Name:</td>
                            <td width="80%" height="20"><?php echo $request->first_name ." ". $request->last_name?></td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="right">Leave Type:</td>
                            <td width="80%" height="20">
                            	<?php 
                            		echo "<input type='hidden' name='leave_type' value=".$request->leave_type_id.">";
                            		
                            		$js = 'style="font-size:11px" ';
                           			if ($user_level != 4)
                            			$js.= 'disabled="disabled"';
                            		else 
                            			$js .="onchange='changeLeaveType(this.value)'";	

                            		echo form_dropdown('leave_type', $leave_types, $request->leave_type_id, $js);
                            	?>                            
                            </td>
                        </tr>
                     </table>
                     <table width="100%" height="150" border="0" cellspacing="1" cellpadding="1" id="common_type" >
                     	<tr bgcolor="#FFFFFF" id="request_type">
                            <td height="20" align="right"></td>
                            <td height="20" align="left">
                            	<input type="radio" name="request_type" id="request_full_day" value="1" onclick="fullDayClick(1)" 
                            		<?php if ($user_level != 4) echo 'disabled="disabled"'; ?> 
                            		<?php if ($request->request_type == 1 ){?>checked="checked"<?php ;}?>/>FULL day
                            	<input type="radio" name="request_type" id="request_partial_day" value="2" onclick="fullDayClick(0)"
                            		<?php if ($user_level != 4) echo 'disabled="disabled"'; ?>
                            		<?php if ($request->request_type == 2 ){?>checked="checked"<?php ;}?>/>PARTIAL day
                            </td>
                        </tr>
                     	<tr bgcolor="#FFFFFF" id="medicalCertificate" style="display:none">
                            <td height="20" align="right">Medical Certificate (MC):</td>
                            <td height="20"><label>
                            		<input type="checkbox" name="mc_provided" value="1" <?php if ($request->mc_provided == 1 ){?>checked="checked"<?php ;}?> />
<!--                                     <input type="file" name="medical_certificate" id="medical_certificate" style="font-size:11px;"/> -->
                                </label></td>
                        </tr>
                     	<tr bgcolor="#FFFFFF" id="carer" style="display:none">
                            <td height="20" align="right">Carer's Leave:</td>
                            <td height="20"><label>
                            		<input type="checkbox" name="carer_leave" value="1" <?php if ($request->carer_leave == 1 ){?>checked="checked"<?php ;}?> />
                                </label></td>
                        </tr>                        
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="right">Start Day:</td>
                            <td width="80%" height="20">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="100%">
                                        	<?php 
                                        	if ($user_level == 4){ ?> 
	                                        	<input type="text" name="start_day" id="start_day"
	                                            value="<?php echo date(DATE_FORMAT, $request->start_day);?>" style="font-size: 11px"
	                                            onchange="javascript: count();"/>
	                                    		<label style="color: #F00">*</label><?php echo form_error('start_day'); ?>
	                                    	<?php 
                                        	}else{
                                        		echo date(DATE_FORMAT, $request->start_day);
                                        	}
                                        	?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF" id="end_day_type">
                            <td height="20" align="right">End Day:</td>
                            <td height="20">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="100%">
                                        	<?php 
                                        	if ($user_level == 4){ ?> 
	                                        	<input type="text" name="end_day" id="end_day"
	                                            value="<?php echo date(DATE_FORMAT, $request->end_day);?>" style="font-size: 11px"
	                                            onchange="javascript: count();"/>
	                                    		<label style="color: #F00">*</label><?php echo form_error('end_day'); ?>
	                                    	<?php 
                                        	}else{
                                        		echo date(DATE_FORMAT, $request->end_day);
                                        	}
                                        	?>                                        
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF" id="start_time_type" style="display:none">
                            <td height="20" align="right">Start Time:</td>
                            <td height="20">
                            	<input type="text" name="start_time" id="start_time" value="<?php echo $request->start_time ?>"
                            		<?php if ($user_level == 4){ ?>onchange="count();"<?php }
                            		else{?> 'disabled=disabled'<?php }?> 
                            	/>                            
							</td>
                        </tr>                        
                        <tr bgcolor="#FFFFFF" id="end_time_type" style="display:none">
                            <td height="20" align="right">End Time:</td>
                            <td height="20">
                            	<input type="text" name="end_time" id="end_time" value="<?php echo $request->end_time ?>"
                            		<?php if ($user_level == 4){ ?>onchange="count();"<?php }
                            		else{?> 'disabled=disabled'<?php }?> 
                            	/>                            
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF" id="include_break_type" style="display:none">
                            <td height="20"></td>
							<td height="20" align="left">
                            	<input type="checkbox" name="include_break" id="include_break" onclick="breakClick()" value="1"
                            	<?php 
                            	if($request->include_break == 1){
                            		echo "checked='checked'";
                            	}
                            	?>
                            	/> deduct lunch							
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td height="20" align="right">Hours Used:</td>
                            <td height="20">
                            	<input name="hours_used" type="text" id="hours_used" value="<?=$request->hours_used?>" style="font-size:11px; color:#000" size="7" />
							</td>
                        </tr>
                 	</table>
                	<table width="100%" height="100" border="0" cellspacing="1" cellpadding="1" id="late_type" style="display:none;">                       
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="right">Arrival Day:</td>
                            <td width="80%" height="20">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="100%">
                                        	<?php 
                                        	if ($user_level == 4){ ?> 
	                                        	<input type="text" name="arrival_day" id="arrival_day"
	                                            value="<?php echo date(DATE_FORMAT, $request->start_day);?>" style="font-size: 11px"
	                                            onchange="javascript: count();"/>
	                                    		<label style="color: #F00">*</label><?php echo form_error('start_day'); ?>
	                                    	<?php 
                                        	}else{
                                        		echo date(DATE_FORMAT, $request->start_day);
                                        	}
                                        	?>                                        
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td height="20" align="right">Actual Arrival Time:</td>
                            <td height="20">
                            	<input type="text" name="arrival_time" id="arrival_time" onchange="blurTime();" value="<?php echo $request->start_time ?>"
                            		<?php if ($user_level == 4){ ?>onchange="count();"<?php }
                            		else{?> 'disabled=disabled'<?php }?> 
                            	/>                                
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td height="20" align="right">Minutes Used:</td>
                            <td height="20">
								<input name="minutes_used" type="text" id="minutes_used" value="<?=$request->hours_used?>" style="background-color:#F5F5F5; font-size:11px; color:#000" size="7" />                            
							</td>
                        </tr>
                    </table>
              		<table width="100%" height="50" border="0" cellspacing="1" cellpadding="1">
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="right">Reason:</td>
                            <td width="80%" height="20">
	                        <?php 
							echo form_textarea(array(
								'name' => 'reason',
								'id' => 'reason',
								'value' => $request->reason,
								'cols' => "45",
								'rows' => "5",
								'style'=> "font-size:11px"
							));
							?>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td height="20" colspan="2" align="center" bgcolor="#EBEBEB">
					            <?php if ($user_level == 4){
					            ?>
		    	                    <input style="font-size: 11px" type="submit"  name="save" id="save" value="     Save     "/>
			                        <input style="font-size: 11px" type="button" onClick="confirm_delete('delete');" name="delete" id="delete" value="     Delete     "/>
								<?php } ?>                            
							</td>
                        </tr>
                    </table>
                    </td>
            </tr>
        </table>
        <?php echo form_close();?>
        </div>
        
        <div class="pane" style="display:block" id="tab_upcoming">
        <table width="100%" border="0" cellspacing="1" cellpadding="1">
                        <tr bgcolor="#EBEBEB">
                            <td width="50%" height="20" colspan="5" align="left" bgcolor="#EBEBEB" >&nbsp;Request Logs</td>
                        </tr>
                        <tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                            <td width="20" height="20">#</td>
		                    <td width="100">Action</td>
		                    <td width="100">Actioned On</td>
		                    <td width="117">Actioned By</td>
		                    <td width="20">IP</td>
                        </tr>
                        <? 
                    if(count($request_logs) > 0){
                            $num = 0;
                            foreach ($request_logs as $row): 
                            $num++;
                            ?>
                            <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
			                    <td height="20"><?=$num?></td>
			                    <td><?=$row['action']?></td>
			                    <td height="20"><?=$row['actioned_on']?></td>
			                    <td><?=$row['actioned_by']?></td>
			                    <td><?=$row['action_ip']?></td>
			                </tr>
                            <? 
                            endforeach;
                    }
                            ?>
                    </table>
    	</div>
	</div>      
    </body>
    
<script type="text/javascript">
//<![CDATA[
$(function() {
	$("ul.tabs").tabs("div.panes > .pane");
});
//]]>
</script>    
</html>