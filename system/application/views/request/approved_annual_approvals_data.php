							<tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                                <td width="70"><a href="javascript:void(0)" onclick="sort('past_annual','id');">Request ID</a><?=$sort_id?></td>
                                <td width="100"><a href="javascript:void(0)" onclick="sort('past_annual','add_time');">Request Date</a><?=$sort_add_time?></td>
                                <td width="100"><a href="javascript:void(0)" onclick="sort('past_annual','applicant');">Applicant</a><?=$sort_applicant?></td>
                                <td width="117"><a href="javascript:void(0)" onclick="sort('past_annual','start_date');">Start Date</a><?=$sort_start_date?></td>
                                <td width="117">End Date</td>
                                <td width="117"><a href="javascript:void(0)" onclick="sort('past_annual','department_id');">Department</a><?=$sort_department_id?></td>
                                <td width="117">Hours Requested</td>
                                <td width="190"><a href="javascript:void(0)" onclick="sort('past_annual','leave_type_id');">Leave Type</a><?=$sort_leave_type_id?></td>
                                <td width="190">Reason</td>
                                <td width="30" height="20" align="center" style="color:#FFF">Action</td>
                            </tr>
                            <? 
                            if(!empty($approvals)){
                            foreach ($approvals as $row): ?>
                            <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
                                <td height="20"><?=$row->id?></td>
                                <td><?=date(DATETIME_FORMAT,$row->add_time)?></td>
                                <td height="20"><?=$row->first_name?> <?=$row->last_name?></td>
                                <td><?=(date(DATETIME_FORMAT, $row->start_date))?></td>
                                <td><?=(date(DATETIME_FORMAT, $row->end_date))?></td>
                                <td><?=$row->department?></td>
                                <td>TODO..</td>
                                <td><?=$row->leave_type?></td>
                                <td><?=$row->reason?></td>
                                <td align="center"><a href="<?=site_url("approve/deal/" . $row->id)?>">approve</a></td>
                            </tr>
                            <? 
                            endforeach;
                            }else{
                            ?>
							<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
                                <td height="20" colspan="10">No record</td>
                            </tr>
                            <?php }?>
                            <tr bgcolor="#FFFFFF" font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                                <td align="right" colspan="10">
                                <?php 
                                	$space = "&nbsp;&nbsp;&nbsp;";
                                	if($page_no == 1){
                                		echo "First";
                                	}else{
                                		echo "<a href='javascript:void()' onclick='past_annual_page(1);'>First</a>";
                                	}
                                	echo $space;
                                	if($page_no > 1){
                                		echo "<a href='javascript:void()' onclick='past_annual_page(" . ($page_no - 1) . ");'>previous</a>";
                                	}else{
                                		echo "previous";
                                	}
                                	echo $space;
                                	if($page_no < $page_count){
                                		echo "<a href='javascript:void()' onclick='past_annual_page(" . ($page_no + 1) . ");'>next</a>";
                                	}else{
                                		echo "next";
                                	}
                                	echo $space;
                                	if($page_no == $page_count || $page_count == 0){
                                		echo "Last";
                                	}else{
                                		echo "<a href='javascript:void()' onclick='past_annual_page(" . $page_count . ")'>Last</a>";
                                	}
                                ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                            </tr>