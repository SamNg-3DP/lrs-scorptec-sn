<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>View leave request</title>
<base href="<?=base_url()?>" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>


<script type="text/javascript">
//<![CDATA[
function count() {
	var request_full_day = document.getElementById('request_full_day');
	var request_partial_day = document.getElementById('request_partial_day');
	if(request_full_day.checked){
		var start_day = document.getElementById("start_day").value;
	    var end_day = document.getElementById("end_day").value;
	    if(start_day && end_day){
			var start = start_day.split('-');
			var dstart = new Date(start[2], start[1], start[0]);
			var end = end_day.split('-');
			var dend = new Date(end[2], end[1], end[0]);
			if(dstart.getTime() > dend.getTime()){
	        	alert("Start day shouldn't be later than end day.");        
	        	return false;
			}
			calculate();
	    }
	}
	if(request_partial_day.checked){
		var start_day = document.getElementById("start_day").value;
	    var start_time = document.getElementById("start_time").value;
	    var end_time = document.getElementById("end_time").value;
	    
	    if(start_day){
	        if(start_time && end_time && start_time > end_time){
	            alert("Start date shouldn't be later than end date.");
	            return false;
	        }
	        if(start_time && end_time && start_time == end_time){
	            alert("Leave time must be greater than zero!");
	            return false;
	        }
	        if(start_day && start_time && end_time){
		    	calculate();
		    }
	    }
	}
    return true;
}

function calculate(){
	var request_full_day = document.getElementById('request_full_day');
	var request_partial_day = document.getElementById('request_partial_day');
	var user_id = document.getElementById('user_id').value;
	var start_day = "";
	var end_day = "";
	var start_time = "";
	var end_time = "";
	var include_break = false;
	var request_type = 0;

	
	if(request_full_day.checked){
		start_day = jQuery("#start_day").val();
		end_day = jQuery("#end_day").val();
		start_time = "00:00";
		end_time = "23:59";
		request_type = 1;
	}else if(request_partial_day.checked){
		start_day = jQuery("#start_day").val();
		end_day = start_day;
		start_time = jQuery("#start_time").val();
		end_time = jQuery("#end_time").val();
		var include_break_ojb = document.getElementById('include_break');
		include_break = include_break_ojb.checked;
		request_type = 2;
	}
	jQuery.ajax({
		async: false,
		type: 'POST',
		url: "<?=site_url("request/calculate_working_hours_ajax")?>",
		data: {
		  	start_day: start_day, 
		  	start_time: start_time,
		  	end_day: end_day,
		  	end_time: end_time,
		  	request_type: request_type,
		  	include_break: include_break,
		  	user_id: user_id
		},
		success: function(data){
			jQuery("#hours_used").val(data.used_hours_count);
	  	},
		dataType: 'json'
	});
}


jQuery(function() {
    jQuery("#start_day").datepicker({ 
    	disabled: true, 
    	dateFormat: '<?=JS_DATE_FORMAT?>',
    	changeMonth: true,
		changeYear: true 
	});
    jQuery("#end_day").datepicker({ 
    	disabled: true, 
    	dateFormat: '<?=JS_DATE_FORMAT?>',
    	changeMonth: true,
		changeYear: true 
	});
    jQuery("#arrival_day").datepicker({ 
    	disabled: true, 
    	dateFormat: '<?=JS_DATE_FORMAT?>',
    	changeMonth: true,
		changeYear: true 
	});
//    changeLeaveType(jQuery("#leave_type").val());
});
//]]>
</script>
    </head>

    <body>
    <div class="wrap">
	  <!-- the tabs --> 
	  <ul class="tabs">
	    <li><a href="#View">View</a></li>
	    <li><a href="#Logs">Logs</a></li> 
	  </ul>
	  <!-- tab "panes" --> 
	  <div class="panes">
	    <div class="pane" style="display:block" id="tab_update">
	    <?php 
			echo form_open('approve/do_update_deal', array('id'=>'deal', 'name'=>'deal' )); 
	 		echo form_hidden("request_id", $request->id);
 		?>
 		<input type="hidden" id="user_id" value="<?php echo $request->user_id;?>" >
        <table width="100%" height="22" border="0" align="center" cellpadding="0" cellspacing="0" background="images/Footer.png">
            <tr>
                <td>
                    <table width="100%" height="211" border="0" cellspacing="1" cellpadding="1">
                        <tr bgcolor="#EBEBEB">
                            <td width="50%" height="20" colspan="2" align="left" bgcolor="#EBEBEB" style="color: red">&nbsp;</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="right">Leave Type:</td>
                            <td width="80%" height="20">
                            	<?php 
                            		echo "<input type='hidden' name='leave_type' value=".$request->leave_type_id.">";
                            		
                            		$js = 'style="font-size:11px" ';
	                           		if ($user_level != 4)
                            			$js.= 'disabled="disabled"';

                            		echo form_dropdown('leave_type', $leave_types, $request->leave_type_id, $js);
                            	?>
                            </td>
                        </tr>
                     	<tr bgcolor="#FFFFFF" id="request_type">
                            <td height="20" align="right"></td>
                            <td height="20" align="left">
                            	<input type="radio" name="request_type" id="request_full_day" value="1" 
                            		<?php if ($user_level != 4) echo 'disabled="disabled"'; ?> 
                            		<?php if ($request->request_type == 1 ){?>checked="checked"<?php ;}?>/>FULL day
                            	<input type="radio" name="request_type" id="request_partial_day" value="2" 
                            		<?php if ($user_level != 4) echo 'disabled="disabled"'; ?>
                            		<?php if ($request->request_type == 2 ){?>checked="checked"<?php ;}?>/>PARTIAL day
                            </td>
                        </tr>
                        <?php 
				        $mc_provided = FALSE;
				        //
				        if($request->leave_type_id == LEAVE_TYPE_PERSONAL){
				        	if(!empty($request->medical_certificate_file)){
				        		$mc_file = $request->medical_certificate_file;
				        		$mc_link = DIR_MEDICAL_CERTIFICATE . "$request->medical_certificate_file";
				        		
				        		if(strrchr(strtolower($mc_file),".jpg") == ".jpg" 
				        			|| strrchr(strtolower($mc_file),".gif") == ".gif" 
				        			|| strrchr(strtolower($mc_file),".png") == ".png" 
				        			|| strrchr(strtolower($mc_file),".bmp") == ".bmp" 
				        		){
				        			$mc_view = '<a href="'.$mc_link.'" target="_blank"><img src="'.$mc_link.'" alt="Medical Certificate Photo" width="60" height="100" border="0"/> download</a>';
				        		}else{
				        			$mc_view = '<a href="'.$mc_link.'" target="_blank">download</a>';
				        		}
				        		?>
				        		<tr bgcolor="#FFFFFF">
						          <td height="20" align="right">Medical Certificate (MC):</td>
						          <td height="20"><label>
						            <?php echo $mc_view;?>
						          </label></td>
						        </tr>
				        		<?php         		
				        	}else{
				        		?>
				        		<tr bgcolor="#FFFFFF">
						          <td height="20" align="right">Medical Certificate (MC):</td>
						          <td height="20"><label>
						            No MC file
						          </label></td>
						        </tr>
				        		<?php 
				        	}
				        }
				        ?>
                        <tr bgcolor="#FFFFFF">
                            <td height="20" align="right">
                            <?php
	                        if($request->leave_type_id == LEAVE_TYPE_LATE
	                        	|| $request->leave_type_id == LEAVE_TYPE_LUNCH){
	                        	echo "Arrival Day:";
	                        }else{
	                        	echo "Start Day:";
	                        }
	                        ?>
                            </td>
                            <td height="20">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="100%" style="font-size: 11px">
                                        	<?php 
                                        	if ($user_level == 4){ ?> 
	                                        	<input type="text" name="start_day" id="start_day"
	                                            value="<?php echo date(DATE_FORMAT, $request->start_day);?>" style="font-size: 11px"
	                                            onchange="javascript: count();"/>
	                                    		<label style="color: #F00">*</label><?php echo form_error('start_day'); ?>
	                                    	<?php 
                                        	}else{
                                        		echo date(DATE_FORMAT, $request->start_day);
                                        	}
                                        	?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <?php 
                        if($request->request_type == 1 ){
                        }else{
                        ?>
                        <tr bgcolor="#FFFFFF">
                            <td height="20" align="right">
                            <?php
	                        if($request->leave_type_id == LEAVE_TYPE_LATE
	                        	|| $request->leave_type_id == LEAVE_TYPE_LUNCH){
	                        	echo "Arrival Time:";
	                        }else{
	                        	echo "Start Time:";
	                        }
	                        ?></td>
                            <td height="20">
                            	<input type="text" name="start_time" id="start_time" value="<?php echo $request->start_time ?>"
                            		<?php if ($user_level == 4){ ?>onchange="count();"<?php }
                            		else{?> 'disabled=disabled'<?php }?> 
                            	/>    
<!--                              	                        
                            	<?php 
                            	$js = 'id="start_time" style="font-size:11px" ';
                            	if ($user_level == 4){$js .= 'onchange="count();"';}
                            	else{$js .= 'disabled=disabled';}
                            	echo form_dropdown('start_time', hour_time(), $request->start_time, $js);
                            	?>
-->                            	
							</td>
                        </tr>
                        <?php }
                        if($request->leave_type_id == LEAVE_TYPE_LATE
                        	|| $request->leave_type_id == LEAVE_TYPE_LUNCH){
                        }else{
                        ?>
                        <tr bgcolor="#FFFFFF">
                            <td height="20" align="right">End Day:</td>
                            <td height="20">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                       <td width="100%" style="font-size: 11px">
                                        	<?php 
                                        	if ($user_level == 4){ ?> 
	                                        	<input type="text" name="end_day" id="end_day"
	                                            value="<?php echo date(DATE_FORMAT, $request->end_day);?>" style="font-size: 11px"
	                                            onchange="javascript: count();"/>
	                                    		<label style="color: #F00">*</label><?php echo form_error('end_day'); ?>
	                                    	<?php 
                                        	}else{
                                        		echo date(DATE_FORMAT, $request->end_day);
                                        	}
                                        	?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <?php 
                        }
                        if($request->request_type == 1 
                        	|| $request->leave_type_id == LEAVE_TYPE_LATE
                        	|| $request->leave_type_id == LEAVE_TYPE_LUNCH){
                        }else{
                        ?>
                        <tr bgcolor="#FFFFFF">
                            <td height="20" align="right">End Time:</td>
                            <td height="20">
                            	<input type="text" name="end_time" id="end_time" value="<?php echo $request->end_time ?>"
                            		<?php if ($user_level == 4){ ?>onchange="count();"<?php }
                            		else{?> 'disabled=disabled'<?php }?> 
                            	/>
<!--                              	
                            	<?php 
                            	$js = 'id="end_time" style="font-size:11px"  ';
                            	if ($user_level == 4){$js .= 'onchange="count();"';}
                            	else{$js .= 'disabled=disabled';}
                            	echo form_dropdown('end_time', hour_time(), $request->end_time, $js);
                            	?>
-->                            	
							</td>
                        </tr>
                        <?php }?>
                        <?php 
                        if($request->request_type == 2 ){
                        ?>
                        <tr bgcolor="#FFFFFF">
                            <td height="20" align="right"></td>
                            <td height="20">
                            	<input type="checkbox" name="include_break" id="include_break" onclick="this.checked=!this.checked"
                            	<?php 
                            	if($request->include_break == 1){
                            		echo "checked='checked'";
                            	}
                            	?>
                            	/> deduct lunch 
							</td>
                        </tr>
                        <?php }?>
                        <tr bgcolor="#FFFFFF">
                            <td height="20" align="right">
                            <?php
	                        if($request->leave_type_id == LEAVE_TYPE_LATE
	                        	|| $request->leave_type_id == LEAVE_TYPE_LUNCH){
	                        	echo "Minutes Used:";
	                        }else{
	                        	echo "Hours Used:";
	                        }
	                        ?>
                            </td>
                            <td height="20">
								<input name="hours_used" type="text" id="hours_used" value="<?=$request->hours_used?>" style="background-color:#F5F5F5; font-size:11px; color:#000" size="7" />
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td height="20" align="right">Reason:</td>
                            <td height="20">
	                        <?php 
							echo form_textarea(array(
								'name' => 'reason',
								'id' => 'reason',
								'value' => $request->reason,
								'cols' => "45",
								'rows' => "5",
								'style'=> "font-size:11px",
								'disabled' => "disabled"
							));
							?>
							</td>
                        </tr>
                        <? 
                        if($request->state == 30 || $request->state == 40 || $request->state == 50 ){
                        ?>
                        <tr bgcolor="#FFFFFF">
				          <td height="20" align="right" bgcolor="#EBEBEB">Approval by Supervisor (<span style="color: #F00; font-weight: bold;">*</span>):</td>
				          <td bgcolor="#EBEBEB"><?=$supervisor_name?></td>
				        </tr>
				        <tr bgcolor="#FFFFFF">
                            <td align="right"></td>
                            <td height="20">
                            	<input type="radio" name="reason" id="reason_acceptable" <?php if(!is_null($request->reason_acceptable) && $request->reason_acceptable == 1){ ?> checked="checked"<? }?>  disabled="disabled"/>Reason Acceptable
								<input type="radio" name="reason" id="reason_not_acceptable" <?php if(!is_null($request->reason_acceptable) && $request->reason_acceptable == 2){ echo 'checked="checked"'; }?> disabled="disabled"/>Reason Not Acceptable
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                        	<td align="right"></td>
                            <td height="20">
                            	<input type="radio" name="super_approved" id="supervisor_approved" <?php if(!is_null($request->supervisor_approved) && $request->supervisor_approved == 1){ ?>checked="checked"<? }?> disabled="disabled"/>Approved
								<input type="radio" name="super_approved" id="supervisor_not_approved" <?php if(!is_null($request->supervisor_approved) && $request->supervisor_approved == 2){ ?>checked="checked"<? }?> disabled="disabled"/>Not Approved
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
			              <td align="right">Comments(by Supervisor):</td>
			              <td>
			              	<?php 
								echo $request->supervisor_comment;
							?>
			              </td>
			            </tr>
			            <?php }?>
			            <? 
                        if($request->state == 40 || $request->state == 50){
                        ?>
                        <tr bgcolor="#FFFFFF">
				          <td height="20" align="right" bgcolor="#EBEBEB">Approval by Manager (<span style="color: #F00; font-weight: bold;">*</span>):</td>
				          <td bgcolor="#EBEBEB"><?=$manager_name?></td>
				        </tr>
				        <tr bgcolor="#FFFFFF">
                            <td align="right"></td>
                            <td height="20">
                            	<input type="radio" name="paid" id="paid" <?php if(!is_null($request->paid) && $request->paid == 1){ ?> checked="checked"<? }?>  disabled="disabled"/>Paid<?php if($request->leave_type_id == LEAVE_TYPE_LATE 
	              			|| $request->leave_type_id == LEAVE_TYPE_LUNCH ){
	              		echo "/Make Up"; }?>
								<input type="radio" name="radio_uppaid" id="radio_uppaid" <?php if(!is_null($request->paid) && $request->paid == 2){ echo 'checked="checked"'; }?> disabled="disabled"/>Unpaid
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td align="right"></td>
                            <td height="20">
                            	<input type="radio" name="approve" id="approve_approve" <?php if(!is_null($request->approve) && $request->approve == 1){ ?>checked="checked"<? }?> disabled="disabled"/>Approved
								<input type="radio" name="approve" id="approve_reject" <?php if(!is_null($request->approve) && $request->approve == 2){ ?>checked="checked"<? }?> disabled="disabled"/>Not Approved
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
			              <td align="right">Comments(by Manager):</td>
			              <td>
			              	<?php 
								echo $request->manager_comment;
							?>
			              </td>
			            </tr>
			            <?php }?>
			            
			            <tr bgcolor="#FFFFFF">
			            <td height="20" colspan="2" align="center" bgcolor="#EBEBEB">
			            
			            <?php if ($user_level == 4){
			            ?>
    	                    <input style="font-size: 11px" type="submit"  name="save" id="save" value="     Save     "/>
			            <?php }
			            if ($current_user_id == $request->user_id || $user_level == 4){
			            ?>
	                        <input style="font-size: 11px" type="submit"  name="cancel" id="cancel" value="     Cancel     "/>
						<?php } ?>
						</td>			            
			            </tr>
                    </table>
                    
                    
                 </td>
            </tr>
        </table>
        <?php echo form_close();?>
        </div>
        <div class="pane" style="display:block" id="tab_upcoming">
        <table width="100%" border="0" cellspacing="1" cellpadding="1">
                        <tr bgcolor="#EBEBEB">
                            <td width="50%" height="20" colspan="5" align="left" bgcolor="#EBEBEB" >&nbsp;Request Logs</td>
                        </tr>
                        <tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                            <td width="20" height="20">#</td>
		                    <td width="100">Action</td>
		                    <td width="100">Actioned On</td>
		                    <td width="117">Actioned By</td>
		                    <td width="20">IP</td>
                        </tr>
                        <? 
                    if(count($request_logs) > 0){
                            $num = 0;
                            foreach ($request_logs as $row): 
                            $num++;
                            ?>
                            <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
			                    <td height="20"><?=$num?></td>
			                    <td><?=$row['action']?></td>
			                    <td height="20"><?=$row['actioned_on']?></td>
			                    <td><?=$row['actioned_by']?></td>
			                    <td><?=$row['action_ip']?></td>
			                </tr>
                            <? 
                            endforeach;
                    }
                            ?>
                    </table>
    	</div>
	</div>      
    </body>
    
<script type="text/javascript">
//<![CDATA[
$(function() {
	$("ul.tabs").tabs("div.panes > .pane");
});
//]]>
</script>    
</html>