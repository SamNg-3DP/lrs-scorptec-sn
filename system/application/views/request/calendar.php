<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<base href="<?=base_url()?>"/>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/qtip.css" />
<link rel='stylesheet' type='text/css' href='css/fullcalendar.css' />
<script type="text/javascript" src="js/jquery-1.3.2.js"></script>
<script type='text/javascript' src='js/jquery.qtip.js'></script>
<script type='text/javascript' src='js/fullcalendar.min.js'></script>


<script type='text/javascript'>
	function changeDept(value){
		var site = $("#site_id").val();
		if(site == ""){
			site = 0;
		}
		window.location = "<?=site_url('calendar/department')?>/" + site + "/"+ value + "/"
	}

	function change_site(value, dept_id){
		var dept_select = document.getElementById(dept_id);
		dept_select.options.length=0;
		$.ajax({
			async: true,
			type: 'POST',
			url: "<?=site_url("approve/dept_by_site_ajax")?>",
			data: {
			  	site_id: value
			},
			success: function(data){
				$.each(data, function(i, dep){
					dept_select.options.add(new Option(dep.name,dep.id));
				});
		  	},
			dataType: 'json'
		});
	}

	$(document).ready(function() {
		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,basicWeek,basicDay'
			},
			//allDayDefault:false,
			editable: false,
			//viewDisplay: function(view) {
				//alert('The new title of the view is ' + view.title);
				//alert('The start of the view is ' + view.start);
				//alert('The end of the view is ' + view.end);
			//},			
			events: "<?=site_url('calendar/events/' . $site_id . '/'. $department_id ).'/'?>"
			,eventClick: function(event) {
		        if (event.url) {
		            window.open(event.url);
		            return false;
		        }
		    }
			,eventRender: function(event, element, view)
		    {
		        //alert(element.html());
		        element.qtip({ 
		        	content: event.description,
					style: { name: 'cream' }, //refer to $.fn.qtip.styles
		        	position: { adjust: { screen: true } }
		        });
		    }
			//,agenda: 'h:mm{ - h:mm}', // 5:00 - 6:30
		});
	});

</script>
<style type='text/css'>

	body {
		margin-top: 40px;
		text-align: center;
		font-size: 14px;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		}

	#calendar {
		width: 900px;
		margin: 0 auto;
	}
<?
foreach($leave_types as $leave){
	?>
.cal_leave_<?=$leave->id?>{
	background-color: <?=$leave->color?>;
	font-size: 10px;
}
	<?
}
?>	
</style>
</head>
<body>
<div align="right">
	<?php if ($user_level == 4){?>
	<label><a href="index.php/request/add" style="font-size:11px">ADD REQUEST</a></label><br/><br/>
	<?php }?>
	Site: 
    <?php 
    $js = 'id="site_id" style="font-size: 11px; width: 120px;" onChange="change_site(this.value, \'department_id \')"';
	echo form_dropdown('site_id', $sites, $site_id, $js);
	echo "Dept.: ";
	$js = 'id="department_id" style="font-size: 11px; width: 120px;" onchange="changeDept(this.value)"';
	
	echo form_dropdown('department_id', $departments, $department_id, $js);
	
	?>
</div>
	<br/>
<div id='calendar'></div>
</body>
</html>
