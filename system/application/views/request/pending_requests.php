<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="90%">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="20" style="text-decoration:underline"><strong>Pending Approval</strong></td>
                </tr>
            </table>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" background="images/Footer.png">
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="1" cellpadding="1">
                            <tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                                <td width="113">Request ID</td>
                                <td width="117">Start Date</td>
                                <td width="117">End Date</td>
                                <td width="190">Leave Type</td>
                                <td width="190">Reason</td>
                                <td width="250">Request Date</td>
                                <td width="30" height="20" align="center" style="color:#FFF">Action</td>
                            </tr>
                            <? 
                            if(!empty($requests)){
                            foreach ($requests as $row): ?>
                            <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
                                <td height="20"><?=$row->id?></td>
                                <td><?=(date(DATE_FORMAT, $row->start_day) . " " . $row->start_time)?></td>
                                <td><?=(date(DATE_FORMAT, $row->end_day) . " " . $row->end_time)?></td>
                                <td><?=$row->leave_type?></td>
                                <td><?=$row->reason?></td>
                                <td><?=date(DATETIME_FORMAT,$row->add_time)?></td>
                                <td align="center"><a href="index.php/request/index_edit">edit</a>&nbsp;<a href="javascript:void(0)" onclick="cancelRequest(1);">cancel</a></td>
                            </tr>
                            <? 
                            endforeach;
                            }else{
                            ?>
							<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
                                <td height="20" colspan="7">No record</td>
                            </tr>
                            <?php }?>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>