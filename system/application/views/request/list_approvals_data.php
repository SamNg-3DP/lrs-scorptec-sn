							<tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                                <td width="20" height="20">#</td>
                                <td width="70"><a href="javascript:void(0)" onclick="sort('<?=$form_id?>','id');">Request ID</a><?=$sort_id?></td>
                                <td width="100"><a href="javascript:void(0)" onclick="sort('<?=$form_id?>','add_time');">Request Date</a><?=$sort_add_time?></td>
                                <td width="100"><a href="javascript:void(0)" onclick="sort('<?=$form_id?>','applicant');">Applicant</a><?=$sort_applicant?></td>
                                <td width="117"><a href="javascript:void(0)" onclick="sort('<?=$form_id?>','start_date');">Start Date</a><?=$sort_start_date?></td>
                                <td width="117">End Date</td>
                                <td width="117"><a href="javascript:void(0)" onclick="sort('<?=$form_id?>','department_id');">Department</a><?=$sort_department_id?></td>
                                <td width="117">Requested</td>
                                <td width="190"><a href="javascript:void(0)" onclick="sort('<?=$form_id?>','leave_type_id');">Leave Type</a><?=$sort_leave_type_id?></td>
                                <td width="190">Reason</td>
                                <td width="30" height="20" align="center" style="color:#FFF">Action</td>
                            </tr>
                            <? 
                            if(!empty($approvals)){
                            $num = 0;
                            foreach ($approvals as $row): 
                            $num++;
                            ?>
                            <tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';" onclick="view_request(<?=$row->id?>)">
                                <td height="20"><?=$num?></td>
                                <td height="20">R<?=$row->id?></td>
                                <td><?=date(DATETIME_FORMAT,$row->add_time)?></td>
                                <td height="20"><?=$row->first_name?> <?=$row->last_name?></td>
                                <td><?=(date(DATETIME_FORMAT, $row->start_date) )?></td>
                                <td>
                                <?
                                	if($row->end_date > 0){
                                		echo date(DATETIME_FORMAT, $row->end_date);
                                	}
                                ?>
                                </td>
                                <td><?=$row->department?></td>
                                <td>
                                <?
                                echo $row->hours_used;
                                if($row->leave_type_id == LEAVE_TYPE_LUNCH
                                	|| $row->leave_type_id == LEAVE_TYPE_LATE){
                                	echo " Minutes";
                                }else{
                                	echo " Hours";
                                }
                                ?>
                                </td>
                                <td><?=$row->leave_type?></td>
                                <td><?=$row->reason?></td>
                                <td align="center">
                                	<a href="<?=site_url('approve/view/'.$row->id)?>">view</a>&nbsp;
                                	<?
                                	if($row->state == 20){
                                		?>
                                		<a href="<?=site_url("approve/deal/" . $row->id)?>">review</a>
                                		<?
                                	}else if($row->state == 30){
                                		if($this->session->userdata('user_level') == 2){
                                		?>
                                		<a href="<?=site_url("approve/deal/" . $row->id)?>">review</a>
                                		<?
                                		}
                                		if($this->session->userdata('user_level') == 3 || $this->session->userdata('user_level') == 4){
                                		?>
                                		<a href="<?=site_url("approve/deal/" . $row->id)?>">review</a>
                                		<?
                                		}
                                	}else if($row->state == 40 || $row->state == 50){
                                		if($this->session->userdata('user_level') == 3 || $this->session->userdata('user_level') == 4){
                                	?>
                                		<a href="<?=site_url("approve/deal/" . $row->id)?>">review</a>
                                	<?
                                		}
                                	}
                                	?>
                                </td>
                            </tr>
                            <? 
                            endforeach;
                            }else{
                            ?>
							<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
                                <td height="20" colspan="11">No record</td>
                            </tr>
                            <?php }?>
                            <tr bgcolor="#FFFFFF" font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                                <td align="right" colspan="11">
                                <?php 
                                	$space = "&nbsp;&nbsp;&nbsp;";

                                	if($page_count > 1){
                                		//                                	if($page_count > 1){
//                                		echo $page_no ." / " . $page_count.$space;
	//                                	}
	//                                	if($page_no == 1){
	//                                		echo "First";
	//                                	}else{
	//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(1);'>First</a>";
	//                                	}
	//                                	echo $space;
	                                	if($page_no > 1){
	                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . ($page_no - 1) . ");'>Pre.</a>";
	                                	}else{
	                                		echo "Pre.";
	                                	}
	                                	echo $space;
	                                	if($page_no+1 <= $page_count){
	                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . ($page_no + 1) . ");'>Next</a>";
	                                	}else{
	                                		echo "Next";
	                                	}
	//                                	echo $space;
	//                                	if($page_no == $page_count || $page_count == 0){
	//                                		echo "Last";
	//                                	}else{
	//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . $page_count . ")'>Last</a>";
	//                                	}
                            			echo $space;
                            			$page_dropdown_id = $form_id.'_page_dropdown';
		                                $js = 'id="'.$page_dropdown_id.'" style="font-size: 11px;" onChange="gotopage(\''.$form_id.'\', this.value)"';
		                                $page_dropdown = array();
		                                for($i = 1; $i <= $page_count; $i++){
		                                	$page_dropdown[$i] = $i;
		                                }
		                                echo "GoTo:&nbsp;";
										echo form_dropdown($page_dropdown_id, $page_dropdown, $page_no, $js);
										
	                                	
                                	}
                                	echo $space. "Items Per Page:&nbsp;";
                                	$page_limit_id = $form_id.'_limit';
	                                $js = 'id="'.$page_limit_id.'" style="font-size: 11px;" onChange="pagelimit(\''.$form_id.'\')"';
	                                echo form_dropdown($page_limit_id, pagination_option(), $page_limit, $js);
                                ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                            </tr>