<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>List Job Incident</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<base href="<?=base_url()?>"/>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.tools.tabs.css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript">
function gotopage(form, pageNo){
	$("#" + form + "_page_no").val(pageNo);
	submitForm(form);
}

function pagelimit(form){
	$("#" + form + "_page_no").val(1);
	submitForm(form);
}

function submitForm(form_id){
	if(form_id == "new"){
		var new_options = {
			dataType:  'json',
			success: new_response,
			url: "<?=site_url('jobincident/new_ajax')?>"
		};
		$('#'+form_id).ajaxSubmit(new_options);
	}else if(form_id == "progress"){
		var progress_options = {
			dataType:  'json',
			success: progress_response,
			url: "<?=site_url('jobincident/progress_ajax')?>"
		};
		$('#'+form_id).ajaxSubmit(progress_options);
	}else if(form_id == "completed"){
		var completed_options = {
			dataType:  'json',
			success: completed_response,
			url: "<?=site_url('jobincident/completed_ajax')?>"
		};
		$('#'+form_id).ajaxSubmit(completed_options);
	}
	
	return false;
}

function new_response(responseText, statusText, xhr, $form){
	$("#list_new").find("tr:gt(0)").remove();
	$('#list_new tr:last').after(responseText.content);
	$("#list_new").find("tr:eq(0)").remove();
}

function new_page(pageNo){
	$("#new_page_no").val(pageNo);
	submitForm("new");
}

function progress_response(responseText, statusText, xhr, $form){
	$("#list_progress").find("tr:gt(0)").remove();
	$('#list_progress tr:last').after(responseText.content);
	$("#list_progress").find("tr:eq(0)").remove();
}

function progress_page(pageNo){
	$("#progress_page_no").val(pageNo);
	submitForm("progress");
}

function completed_response(responseText, statusText, xhr, $form){
	$("#list_completed").find("tr:gt(0)").remove();
	$('#list_completed tr:last').after(responseText.content);
	$("#list_completed").find("tr:eq(0)").remove();
}

function completed_page(pageNo){
	$("#completed_page_no").val(pageNo);
	submitForm("completed");
}

$(function() {
	$("ul.tabs").tabs("div.panes > .pane");
});

</script>
</head>

<body>

  <table width="100%" border="0" cellspacing="1" cellpadding="1">
    <tr bgcolor="#EBEBEB">
        <td height="20" colspan="14" align="right">
            <label>
                <a href="<?=site_url('jobincident/add')?>" style="font-size:11px">ADD NEW</a>
            </label>
        </td>
    </tr>
</table>
  
  <div class="wrap">

	<div class="panes">

		<form id="new" name="new" method="post" action="">
  		  <input type="hidden" id="new_page_no" name="new_page_no" value=""/>
		  <table width="100%" border="0" cellspacing="0" cellpadding="0" background="images/Footer.png">
		    <tr>
		      <td>
		      <table width="100%" border="0" cellspacing="1" cellpadding="0" id="list_new">
		        <tr style="background-image:url('images/TableHeader.png');">
		          <td height="20">No</td>
		          <td>Incident ID</td>
		          <td>Staff</td>
		          <td>Date</td>
		          <td>Customer(s)</td>
		          <td>Reference/Invoice No</td>
		          <td colspan="2" align="center">Action</td>
		        </tr>
		        <? 
				if($count > 0){
					$num = 0;     
		        foreach ($jobs as $row): ?>
		        <tr bgcolor="#FFFFFF">
		          <td ><?=++$num?></td>
		          <td ><?=$row->id?></td>
		          <td >
		          <?=$row->first_name.' '.$row->last_name?>
		          </td>
		          <td ><?=date(DATE_FORMAT, $row->incident_date)?></td>
		          <td >
		          <?=$row->customer?>
		          </td>
		          <td >
		          <?=$row->reference_no?>
		          </td>
		          <td align="center">
		          <?php if ($status=='new'){?>
						<a href="<?php echo site_url("jobincident/edit/".$row->id);?>">Edit</a>
						&nbsp;&nbsp;&nbsp;
				  <?php }?>
			      <?php if ($user_level >=3 && ($status=='progress')){?>
						<a href="<?php echo site_url("jobincident/deal/".$row->id);?>">Review</a>
						&nbsp;&nbsp;&nbsp;
				  <?php }?>
		          <a href="<?php echo site_url("jobincident/view/".$row->id);?>">View</a>
		          </td>
		        </tr>
		        <? endforeach; ?>
		        <tr bgcolor="#FFFFFF" font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
		            <td align="right" colspan="8">
		            <?php 
		            	$space = "&nbsp;&nbsp;&nbsp;";
		
		        		if($page_count > 1){
		//                            		if($page_count > 1){
		//                                		echo $page_no ." / " . $page_count.$space;
		//                                	}
		//                                	if($page_no == 1){
		//                                		echo "First";
		//                                	}else{
		//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(1);'>First</a>";
		//                                	}
		                	echo $space;
		                	if($page_no > 1){
		                		if ($status=='new')
		                			echo "<a href='javascript:void(0)' onclick='new_page(" . ($page_no - 1) . ");'>Pre.</a>";
		                		elseif ($status=='progress')
		                			echo "<a href='javascript:void(0)' onclick='progress_page(" . ($page_no - 1) . ");'>Pre.</a>";
		                		elseif ($status=='completed') 
		                			echo "<a href='javascript:void(0)' onclick='completed_page(" . ($page_no - 1) . ");'>Pre.</a>";
		                	}else{
		                		echo "Pre.";
		                	}
		                	echo $space;
		                	if($page_no + 1 <= $page_count){
		                		if ($status=='new')
		                			echo "<a href='javascript:void(0)' onclick='new_page(" . ($page_no + 1) . ");'>Pre.</a>";
		                		elseif ($status=='progress')
		                			echo "<a href='javascript:void(0)' onclick='progress_page(" . ($page_no + 1) . ");'>Pre.</a>";
		                		elseif ($status=='completed') 
		                			echo "<a href='javascript:void(0)' onclick='completed_page(" . ($page_no + 1) . ");'>Pre.</a>";
		                	}else{
		                		echo "Next";
		                	}
		//                                	echo $space;
		//                                	if($page_no == $page_count || $page_count == 0){
		//                                		echo "Last";
		//                                	}else{
		//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . $page_count . ")'>Last</a>";
		//                                	}                            			
		        			echo $space. "GoTo:";
		        			$page_dropdown_id = 'new_page_dropdown';
	                		if ($status=='new')
	                			$js = 'id="'.$page_dropdown_id.'" style="font-size: 11px;" onChange="gotopage(\'new\', this.value)"';
	                		elseif ($status=='progress')
		                    	$js = 'id="'.$page_dropdown_id.'" style="font-size: 11px;" onChange="gotopage(\'progress\', this.value)"';
	                		elseif ($status=='completed') 
		                    	$js = 'id="'.$page_dropdown_id.'" style="font-size: 11px;" onChange="gotopage(\'completed\', this.value)"';
		        			

		                    $page_dropdown = array();
		                    for($i = 1; $i <= $page_count; $i++){
		                    	$page_dropdown[$i] = $i;
		                    }
							echo form_dropdown($page_dropdown_id, $page_dropdown, $page_no, $js);
		            	}
		            	echo $space. "Items Per Page:";
		            	
		            	if ($status=='new'){
			            	$page_limit_id = 'new_limit';
			            	$js = 'id="'.$page_limit_id.'" style="font-size: 11px;" onChange="pagelimit(\'new\')"';
		            	}elseif ($status=='progress'){
			            	$page_limit_id = 'progress_limit';
			            	$js = 'id="'.$page_limit_id.'" style="font-size: 11px;" onChange="pagelimit(\'progress\')"';
		            	}elseif ($status=='completed'){
			            	$page_limit_id = 'completed_limit';
			            	$js = 'id="'.$page_limit_id.'" style="font-size: 11px;" onChange="pagelimit(\'completed\')"';
						}
						
		            	echo form_dropdown($page_limit_id, pagination_option(), PAGE_SIZE, $js);
		            	
		            ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		            </td>
		        </tr>
		        <?}?>
		      </table></td>
		    </tr>
		  </table>
		  </form>

	</div>
</div>
  

</body>
</html>