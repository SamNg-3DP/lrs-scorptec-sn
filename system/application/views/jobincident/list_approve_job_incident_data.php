				<tr style="background-image:url('images/TableHeader.png');">
		          <td height="20">ID</td>
		          <td>Incident ID</td>
		          <td>Date</td>
		          <td>Customer(s)</td>
		          <td>Reference/Invoice No</td>
		          <td>Added by</td>
		          <td align="center">Action</td>
		        </tr>
		        <? 
		        $num = 0;
		        foreach ($jobs as $row): ?>
		        <tr bgcolor="#FFFFFF">
		          <td ><?=++$num?></td>
		          <td ><?=$row->id?></td>
		          <td ><?=date(DATE_FORMAT, $row->incident_date)?></td>
		          <td >
		          <?=$row->customer?>
		          </td>
		          <td >
		          <?=$row->reference_no?>
		          </td>
		          <td><?=$row->first_name . " " . $row->last_name?></td>
		           <td align="center">
		           <?if($form_id == 'progress'){?>
		           	<a href="<?php echo site_url("jobincident/deal/".$row->id)?>">review</a><br>
		           	<?}?>
		           <?if($form_id == 'completed' && $this->session->userdata('user_level') == 3){?>
		           	<a href="<?php echo site_url("jobincident/deal/".$row->id)?>">review</a><br>
		           <?}?>
		           <a href="<?php echo site_url("jobincident/view/".$row->id)?>">view</a>
		           </td>
		        </tr>
		        <? endforeach; ?>
		        <tr bgcolor="#FFFFFF" font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
		            <td align="right" colspan="<?=($form_id=='progress')?8:7?>">
		            <?php 
		            	$space = "&nbsp;&nbsp;&nbsp;";
		
		        		if($page_count > 1){
		//                            		if($page_count > 1){
		//                                		echo $page_no ." / " . $page_count.$space;
		//                                	}
		//                                	if($page_no == 1){
		//                                		echo "First";
		//                                	}else{
		//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(1);'>First</a>";
		//                                	}
		                	echo $space;
		                	if($page_no > 1){
		                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . ($page_no - 1) . ");'>Pre.</a>";
		                	}else{
		                		echo "Pre.";
		                	}
		                	echo $space;
		                	if($page_no + 1 <= $page_count){
		                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . ($page_no + 1) . ");'>Next</a>";
		                	}else{
		                		echo "Next";
		                	}
		//                                	echo $space;
		//                                	if($page_no == $page_count || $page_count == 0){
		//                                		echo "Last";
		//                                	}else{
		//                                		echo "<a href='javascript:void(0)' onclick='".$form_id."_page(" . $page_count . ")'>Last</a>";
		//                                	}                            			
		        			echo $space. "GoTo:";
		        			$page_dropdown_id = $form_id.'_page_dropdown';
		                    $js = 'id="'.$page_dropdown_id.'" style="font-size: 11px;" onChange="gotopage(\''.$form_id.'\', this.value)"';
		                    $page_dropdown = array();
		                    for($i = 1; $i <= $page_count; $i++){
		                    	$page_dropdown[$i] = $i;
		                    }
							echo form_dropdown($page_dropdown_id, $page_dropdown, $page_no, $js);
		            	}
		            	echo $space. "Items Per Page:";
		            	$page_limit_id = $form_id.'_limit';
		            	$js = 'id="'.$page_limit_id.'" style="font-size: 11px;" onChange="pagelimit(\''.$form_id.'\')"';
		            	echo form_dropdown($page_limit_id, pagination_option(), $page_limit, $js);
		            	
		            ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		            </td>
		        </tr>