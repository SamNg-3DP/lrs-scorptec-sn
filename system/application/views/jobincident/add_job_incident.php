<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add a job incident</title>
<base href="<?=base_url()?>" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>

<script type="text/javascript">
//<![CDATA[

function submit_click(action){
	var incident_date = $("#incident_date").val();
	if($.trim(incident_date) == ""){
		alert("Please select date of incident!");
		return false;
	}
	
	var customer = $("#customer").val();
	if($.trim(customer) == ""){
		alert("Please enter Customer(s) involved!");
		return false;
	}
	
	var reference_no = $("#reference_no").val();
	if($.trim(reference_no) == ""){
		alert("Please enter Reference no - Invoice no!");
		return false;
	}
	
	var detail = $("#detail").val();
	if($.trim(detail) == ""){
		alert("Please enter Detail of the inicident!");
		return false;
	}
	
	var how_occur = $("#how_occur").val();
	if($.trim(how_occur) == ""){
		alert("Please enter How did the incident occur!");
		return false;
	}
	
	var how_prevent = $("#how_prevent").val();
	if($.trim(how_prevent) == ""){
		alert("Please enter What should have been done to prevent it from happening next time!");
		return false;
	}
	
	var how_repair = $("#how_repair").val();
	if($.trim(how_repair) == ""){
		alert("Please enter What action should be taken now to repair or recover from the incident!");
		return false;
	}
	
	var what_cost = $("#what_cost").val();
	if($.trim(what_cost) == ""){
		alert("Please enter What is the approximate time and cost involved as a result of this incident!");
		return false;
	}
	
	$("#operate").val(action);
	
	var theForm = document.getElementById("add_jobincident");
	theForm.submit();
}

jQuery(function() {
    $("#incident_date").datepicker({ 
    	disabled: true, 
    	dateFormat: '<?=JS_DATE_FORMAT?>',
    	changeMonth: true,
		changeYear: true 
	});
});
//]]>
</script>
    </head>
    <body>
    	<div>
        <?php 
        echo form_open('jobincident/do_add', array('id'=>'add_jobincident')); 
        ?>
        <input type="hidden" id="operate" name="operate" value=""/>
        <table width="100%" height="22" border="0" align="center" cellpadding="0" cellspacing="0" background="images/Footer.png">
            <tr>
                <td>
                     <table width="100%" height="150" border="0" cellspacing="1" cellpadding="1" id="common_type" >
                        <tr bgcolor="#FFFFFF">
                            <td height="20" colspan="2"><p>The purpose of this form is to make note and address any potential issues that can cause job incidents and prevent it from happening in the future. A repeat of similar incident by the same person will not be tolerated and will be deemed as a failure to observe instructions and may result in sanctions being applied</p></td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Date of incident:</td>
                            <td width="80%" height="20">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="100%">
                                        	<input type="text" name="incident_date" id="incident_date"
                                            value="<?php echo set_value('incident_date');?>" style="font-size: 11px"
                                            />
                                    		<label style="color: #F00">*</label><?php echo form_error('incident_date'); ?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Staff name involved:</td>
                            <td width="80%" height="20">
                                <? echo $this->session->userdata('first_name') . " " . $this->session->userdata('last_name') ?>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Department:</td>
                            <td width="80%" height="20">
                                <?=$department?>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF" id="end_day_type">
                            <td height="20" align="left">Customer(s) involved:</td>
                            <td height="20">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="100%">
                                        	<input type="text" name="customer" id="customer"
                                                               value="<?php echo set_value('customer');?>" class="divinput"
                                                               />
                                            <label style="color: #F00">*</label><?php echo form_error('customer'); ?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF" id="end_day_type">
                            <td height="20" align="left">Reference no - Invoice no:</td>
                            <td height="20">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="100%">
                                        	<input type="text" name="reference_no" id="reference_no"
                                                               value="<?php echo set_value('reference_no');?>" class="divinput"
                                                               />
                                            <label style="color: #F00">*</label><?php echo form_error('reference_no'); ?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Detail of the incident:</td>
                            <td width="80%" height="20">
	                        <?php 
							echo form_textarea(array(
								'name' => 'detail',
								'id' => 'detail',
								'value' => set_value('detail'),
								'cols' => "45",
								'rows' => "5",
								'class'=> "divtextarea"
							));
							?>
							<label style="color: #F00">*</label><?php echo form_error('detail'); ?>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">How did the incident occur? </td>
                            <td width="80%" height="20">
	                        <?php 
							echo form_textarea(array(
								'name' => 'how_occur',
								'id' => 'how_occur',
								'value' => set_value('how_occur'),
								'cols' => "45",
								'rows' => "5",
								'class'=> "divtextarea"
							));
							?>
							<label style="color: #F00">*</label><?php echo form_error('how_occur'); ?>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">What should have been done to prevent it from happening next time?</td>
                            <td width="80%" height="20">
	                        <?php 
							echo form_textarea(array(
								'name' => 'how_prevent',
								'id' => 'how_prevent',
								'value' => set_value('how_prevent'),
								'cols' => "45",
								'rows' => "5",
								'class'=> "divtextarea"
							));
							?>
							<label style="color: #F00">*</label><?php echo form_error('how_prevent'); ?>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">What action should be taken now to repair or recover from the incident</td>
                            <td width="80%" height="20">
	                        <?php 
							echo form_textarea(array(
								'name' => 'how_repair',
								'id' => 'how_repair',
								'value' => set_value('how_repair'),
								'cols' => "45",
								'rows' => "5",
								'class'=> "divtextarea"
							));
							?>
							<label style="color: #F00">*</label><?php echo form_error('how_repair'); ?>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">What is the approximate time and cost involved as a result of this incident:</td>
                            <td width="80%" height="20">
	                        <?php 
							echo form_textarea(array(
								'name' => 'what_cost',
								'id' => 'what_cost',
								'value' => set_value('what_cost'),
								'cols' => "45",
								'rows' => "5",
								'class'=> "divtextarea"
							));
							?>
							<label style="color: #F00">*</label><?php echo form_error('what_cost'); ?>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td height="20" colspan="2" align="center" bgcolor="#EBEBEB">
                                <input style="font-size: 11px" type="button" name="save" id="save" onclick="submit_click('save');" value="     Save     "/>
                                <input style="font-size: 11px" type="button" name="apply" id="apply" onclick="submit_click('apply');" value="     Submit     "/>
                            </td>
                        </tr>
                    </table>
               </td>
            </tr>
        </table>
        <?php echo form_close();?>
        </div>
    </body>
</html>