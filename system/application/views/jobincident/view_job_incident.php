<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Approve a job incident</title>
<base href="<?=base_url()?>" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />
<script type="text/javascript">
//<![CDATA[
function goto_index(){
	<? if($from == 2){?>
		window.location="<?=site_url("jobincident/approve")?>";
	<?
	}else{?>
		window.location="<?=site_url("jobincident/list_jobs")?>";
	<?
	}
	?>	
}
//]]>
</script>
</head>
    <body>
    	<div>
        <table width="100%" height="22" border="0" align="center" cellpadding="0" cellspacing="0" background="images/Footer.png">
            <tr>
                <td>
                     <table width="100%" height="150" border="0" cellspacing="1" cellpadding="1" id="common_type" >
                        <tr bgcolor="#FFFFFF">
                            <td height="20" colspan="2"><p>The purpose of this form is to make note and address any potential issues that can cause job incidents and prevent it from happening in the future. A repeat of similar incident by the same person will not be tolerated and will be deemed as a failure to observe instructions and may result in sanctions being applied</p></td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Date of incident:</td>
                            <td width="80%" height="20">
                                <?=date(DATE_FORMAT, $job->incident_date)?>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Staff name involved:</td>
                            <td width="80%" height="20">
                                <?=$job->first_name . " " . $job->last_name?>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Department:</td>
                            <td width="80%" height="20">
                                <?=$job->department?>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF" id="end_day_type">
                            <td height="20" align="left">Customer(s) involved:</td>
                            <td height="20">
                                 <p><?=$job->customer?></p>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF" id="end_day_type">
                            <td height="20" align="left">Reference no - Invoice no:</td>
                            <td height="20">
                                <p><?=$job->reference_no?></p>
                            </td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Detail of the incident:</td>
                            <td width="80%" height="20">
		                       <p><?=$job->detail?></p>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">How did the incident occur? </td>
                            <td width="80%" height="20">
	                        <p><?=$job->how_occur?></p>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">What should have been done to prevent it from happening next time?</td>
                            <td width="80%" height="20">
	                        <p><?=$job->how_prevent?></p>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">What action should be taken now to repair or recover from the incident</td>
                            <td width="80%" height="20">
	                        <p><?=$job->how_repair?></p>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">What is the approximate time and cost involved as a result of this incident:</td>
                            <td width="80%" height="20">
	                        <p><?=$job->what_cost?></p>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Supervisor comment:</td>
                            <td width="80%" height="20">
                            <p><?=$job->supervisor_comment?></p>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">Manager comment:</td>
                            <td width="80%" height="20">
	                        	<p><?=$job->manager_comment?></p>
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td width="20%" height="20" align="left">(Recommend and/or approve action to be taken)</td>
                            <td width="80%" height="20">
                            <p><?=$job->take_action?></p>	                        
							</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                              <td ><input type="radio" name="resolved" id="not_resolved" value="2" <?php if(!is_null($job->resolved) && $job->resolved == 2){ ?>checked="checked"<? }?> disabled="disabled" /> Not Resolved</td>
				              <td ><input type="radio" name="resolved" id="resolved" value="1" <?php if(!is_null($job->resolved) && $job->resolved == 1){ ?>checked="checked"<? }?> disabled="disabled" /> Resolved</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                              <td ><input type="radio" name="approved" id="approved" value="2" <?php if(!is_null($job->approved) && $job->resolved == 2){ ?>checked="checked"<? }?> disabled="disabled"/> Not Approved</td>
				              <td ><input type="radio" name="approved" id="not_approved" value="1" <?php if(!is_null($job->approved) && $job->resolved == 1){ ?>checked="checked"<? }?> disabled="disabled"/> Approved</td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                            <td height="20" colspan="2" align="center" bgcolor="#EBEBEB">
                                <input style="font-size: 11px" type="button" name="Close" id="Close" onclick="goto_index();return false;" value="     Close     "/>
                            </td>
                        </tr>
                    </table>
            	</td>
            </tr>
        </table>
        </div>
    </body>
</html>