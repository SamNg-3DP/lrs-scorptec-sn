<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Edit shift category</title>
<base href="<?=base_url ()?>" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" href="css/js_color_picker_v2.css" media="screen">
<script src="js/color_functions.js"></script> 
<script type="text/javascript" src="js/js_color_picker_v2.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
function check_unique(){
	var exist = false;
	$.ajax({
		async: false,
		type: 'POST',
		url: "<?=site_url("shiftcategory/check_code_ajax")?>",
		data: {
			code: $("#code").val(),
			id: $("#id").val()
		},
		success: function(data){
			if(data.status == "exist"){				
				exist = true;
			}
	  	},
		dataType: 'json'
	});
	return exist;
}

function check(){
	var code = $("#code").val();
	if($.trim(code) == ""){
		alert("Please enter code");
		return false;
	}else{
		if(check_unique()){
			alert("Code has existed. Please enter again.");
			return false;
		}
	}
	
	return true;
}
</script>
</head>

<body >
<?php echo form_open ( 'shiftcategory/do_edit', 'onsubmit="return check()"'  ); ?>
  <input type="hidden" name="id" id="id" value="<?=$shift_category->id?>" />
  <table width="100%" border="0" cellspacing="0" cellpadding="0"
	background="images/Footer.png">
	<tr>
		<td>
		<table width="100%" border="0" cellspacing="1" cellpadding="0">
			<tr>
				<td height="20" colspan="2" align="left" bgcolor="#EBEBEB">&nbsp;</td>
			</tr>
			<tr>
				<td width="20%" height="20" bgcolor="#FFFFFF">Code:</td>
				<td height="20" bgcolor="#FFFFFF">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="60%"><input name="code" type="text" id="code"
							style="font-size: 11px" size="20"
							value="<?php echo set_value ( 'code', $shift_category->code );?>" /><label
							style="color: #F00">＊</label></td>
						<td><?php echo form_error ( 'code' );?></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td width="20%" height="20" bgcolor="#FFFFFF">Description:</td>
				<td height="20" bgcolor="#FFFFFF">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="60%"><input name="description" type="text"
							id="description" style="font-size: 11px" size="60"
							value="<?php
							echo set_value ( 'description', $shift_category->description );
							?>" /><label
							style="color: #F00">＊</label></td>
						<td><?php
						echo form_error ( 'description' );
						?></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td width="20%" height="20" bgcolor="#FFFFFF">Color:</td>
				<td height="20" bgcolor="#FFFFFF">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="60%"><input name="color" type="text" id="color" style="font-size: 11px"
							value="<?php
							echo set_value ( 'color',$shift_category->color );
							?>"
							size="10" readonly="readonly" /><label
							style="color: #F00"><img src="images/action/bgColor.gif" width="18" height="18" border="0" align="top" onclick="showColorPicker(this,document.getElementById('color'))"/>＊</label></td>
						<td><?php
						echo form_error ( 'color' );
						?></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td width="20%" height="20" bgcolor="#FFFFFF">Total:</td>
				<td height="20" bgcolor="#FFFFFF">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
					  <td width="4%"><input type="radio" name="radio" id="radio"
							value="0" <?=$shift_category->total==0?'checked="checked"':''?>/></td>
						<td width="6%">					  -</td>
						<td width="4%"><input name="radio" type="radio" id="radio2"
							value="1" <?=$shift_category->total==1?'checked="checked"':''?> /></td>
						<td width="6%">					  0</td>
						<td width="4%"><input type="radio" name="radio" id="radio3"
							value="2" <?=$shift_category->total==2?'checked="checked"':''?> /></td>
						<td width="76%"> +</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td height="20" colspan="2" align="center" bgcolor="#EBEBEB"><label>
				<input type="submit" name="button" id="button"
					value="     Save     " style="font-size: 11px" /> </label></td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</form>
</body>
</html>
