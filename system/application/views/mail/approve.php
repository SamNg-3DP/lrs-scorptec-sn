<p>Leave  Application</p>
<table width="450" border="1">
  <tr>
    <th width="186" align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Employee: </span></th>
    <td width="248">&nbsp;
    <? echo $request->first_name . " " . $request->last_name;?>
    </td>
  </tr>
  <tr>
    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Leave  Type:</span></th>
    <td>&nbsp;
    <?=$request->leave_type?>
    </td>
  </tr>
  <tr>
      <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Requested  On</span></th>
      <td>&nbsp;
      <?=date(DATETIME_FORMAT, $request->add_time)?>
      </td>
  </tr>
<?php if($request->leave_type_id == LEAVE_TYPE_LATE || $request->leave_type_id == LEAVE_TYPE_LUNCH){ ?>
    <tr>
        <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Arrival Day:</span></th>
        <td>&nbsp;
        <?=date(DATETIME_FORMAT, $request->start_date)?>
        </td>
      </tr>
      <tr>
        <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Minutes used</span></th>
        <td>&nbsp;
        <?=round($request->hours_used*60,2)?>
        </td>
      </tr>
<?php }else{ 
	$day_count = (floor(($request->end_date - $request->start_date) / 86400) + 1);
	$total_days = "";
	if($day_count > 1){
        $total_days = $day_count. " days";
    } else{
    	$total_days = $day_count. " days";
    }
?>
    <tr>
        <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">From:</span></th>
        <td>&nbsp;
        <?=date(DATETIME_FORMAT, $request->start_date)?>
        </td>
      </tr>
      <tr>
        <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">To:</span></th>
        <td>&nbsp;
		<?=date(DATETIME_FORMAT, $request->end_date)?>
        </td>
      </tr> 
      <?
  if(!empty($amount_leave_left)){
  ?>
  <tr>
    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Amount  of Leave Left (before this request)</span></th>
    <td>&nbsp;
    <?=$amount_leave_left?>
    </td>
  </tr>
  <?
  }
  ?>
      <tr>
        <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Leave duration</span></th>
        <td>&nbsp;
        <? //=$total_days
	    if ($request->request_type == 1){
		    $day_count = (floor(($request->end_date - $request->start_date) / 86400) + 1);
			$total_days = "";
			if($day_count > 1){
		        $total_days = $day_count. " days";
		    } else{
		    	$total_days = $day_count. " day";
		    }
		    echo $total_days . "(".$request->hours_used." hours)";
	    }elseif ($request->request_type == 2){
	    	echo $request->hours_used." hours";
	    }        
        
        ?>
        </td>
      </tr>
<?php } ?>


      <tr>
        <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Reason:</span></th>
        <td>&nbsp;
        <?=$request->reason?>
        </td>
      </tr>


<?php 
     $html_body = "";
     
$hod = IS_NULL($request->mc_provided)?-1:$request->mc_provided; 
$reason_acceptable = IS_NULL($request->reason_acceptable)?-1:$request->reason_acceptable;
$super_approved =IS_NULL($request->supervisor_approved)?-1:$request->supervisor_approved;
$Paid = IS_NULL($request->paid)?-1:$request->paid;
$manager_approve =IS_NULL($request->approve)?-1:$request->approve;

if($hod == 1 || $hod == 0){
    log_message("debug", "2hod ===" . $hod );
    $hod_str = "";
    if($hod == 1){
        $hod_str = "M.C. Provided";
    }else{
        $hod_str = "M.C. Not Provided";
    }
    $html_body .='
      <tr>
        <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">HOD Information</span></th>
        <td>&nbsp;'
        .$hod_str.
        '</td>
      </tr>';
}

if($reason_acceptable == 1 || $reason_acceptable == 2){
    $reason_str = "Acceptable";
    if($reason_acceptable == 2){
        $reason_str = "Not Acceptable";
    }
    $html_body .='
      <tr>
        <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Reason</span></th>
        <td>&nbsp;'
        .$reason_str.
        '</td>
      </tr>';
}
if($super_approved == 1 || $super_approved == 2){
    $super_str = "Approved";
    if($super_approved == 2){
        $super_str = "Not Approved";
    }
    
    $html_body .='
      <tr>
        <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Supervisor comment</span></th>
        <td>&nbsp;'
        .$request->supervisor_comment.
        '</td>
      </tr>';    
    
    
    
    $html_body .='
      <tr>
        <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Supervisor approval</span></th>
        <td>&nbsp;'
        .$super_str.
        '</td>
      </tr>';
}
if($Paid == 1 || $Paid == 2){
    $paid_str = "Paid";
    if($Paid == 2){
        $paid_str = "Unpaid";
    }
    $html_body .='
      <tr>
        <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Paid</span></th>
        <td>&nbsp;'
        .$paid_str.
        '</td>
      </tr>';
}
if($manager_approve == 1 || $manager_approve == 2){
    $approve_str = "Approve";
    if($manager_approve == 2){
        $approve_str = "Reject";
    }
    
    $html_body .='
      <tr>
        <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Manager comment</span></th>
        <td>&nbsp;'
        .$request->manager_comment.
        '</td>
      </tr>';    
    
    
    $html_body .='
      <tr>
        <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Manager approval</span></th>
        <td>&nbsp;'
        .$approve_str.
        '</td>
      </tr>';
}
$html_body .= '</table>
    <p>Response</p>
    <p>Please <a href="'
    .$appove_url.
    '">'
    .$appove_name.
    '</a>
    </p>';
    
    echo $html_body;
?>    