<p>Leave  Application</p>
<table width="450" border="1">
  <tr>
    <th width="186" align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Employee: </span></th>
    <td width="248">&nbsp;
    <? echo $request->first_name . " " . $request->last_name;?>
    </td>
  </tr>
  <tr>
    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Leave  Type:</span></th>
    <td>&nbsp;
    <?=$request->leave_type?>
    </td>
  </tr>
  <tr>
    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Requested  On</span></th>
    <td>&nbsp;
    <?=date(DATETIME_FORMAT, $request->add_time)?>
    </td>
  </tr>
<?php if($request->leave_type_id == LEAVE_TYPE_LATE || $request->leave_type_id == LEAVE_TYPE_LUNCH){ ?>
    <tr>
        <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Arrival Day:</span></th>
        <td>&nbsp;
        <?=date(DATETIME_FORMAT, $request->start_date)?>
        </td>
      </tr>
      <tr>
        <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Minutes used</span></th>
        <td>&nbsp;
        <?=round($request->hours_used*60,2)?>
        </td>
      </tr>
	 <tr>
	   <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Reason:</span></th>
	   <td>&nbsp;
	   <?=$request->reason?>
	   </td>
	 </tr>      
<?php }else{ ?>  
  <tr>
    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">From:</span></th>
    <td>&nbsp;
     <?=date(DATETIME_FORMAT, $request->start_date)?>
    </td>
  </tr>
  <tr>
    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">To:</span></th>
    <td>&nbsp;
    <?=date(DATETIME_FORMAT, $request->end_date)?>
    </td>
  </tr>
  <tr>
    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Reason:</span></th>
    <td>&nbsp;
    <?=$request->reason?>
    </td>
  </tr>
  <?
  if(!empty($amount_leave_left)){
  ?>
  <tr>
    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Available Leave (before this request)</span></th>
    <td>&nbsp;
    <?=$amount_leave_left?>
    </td>
  </tr>
  <?
  }
  ?>
  <tr>
    <th align="left" scope="row"><span style="font-family: Tahoma; font-size: 10pt;">Leave duration</span></th>
    <td>&nbsp;
    <?
    if ($request->request_type == 1){
	    $day_count = (floor(($request->end_date - $request->start_date) / 86400) + 1);
		$total_days = "";
		if($day_count > 1){
	        $total_days = $day_count. " days";
	    } else{
	    	$total_days = $day_count. " day";
	    }
	    echo $total_days . "(".$request->hours_used." hours)";
    }elseif ($request->request_type == 2){
    	echo $request->hours_used." hours";
    }
    
    ?>
    </td>
  </tr>
<?php }?>  
</table>
<p>Response </p>
<p>Please <a href="<?=$appove_url?>"><?=$appove_name?></a></p>