<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US" xml:lang="en">
<head>
	<title>LRS Left Menu</title>
	<base href="<?=base_url()?>"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
</head>
<body>
<div class="Block">
	<div class="Block-tl"></div>
    <div class="Block-tr"></div>
    <div class="Block-bl"></div>
    <div class="Block-br"></div>
    <div class="Block-tc"></div>
    <div class="Block-bc"></div>
    <div class="Block-cl"></div>
    <div class="Block-cr"></div>
    <div class="Block-cc"></div>
	<div class="Block-body">
		<div class="BlockHeader">
			<div class="l"></div>
        	<div class="r"></div>
        	<div class="t">Leave Request</div>
      	</div>
      	<p><a href="<?=site_url('request')?>" target="main-frame">Leave Statement</a></p>
      	<?php if($this->session->userdata('user_level') == 1){ ?>      	
      	<p><a href="index.php/calendar" target="main-frame">Calendar</a></p>
      	<?php } else if($this->session->userdata('user_level') == 2){?>
      	<p><a href="<?=site_url('approve')?>" target="main-frame">Leave Approval</a></p>
      	<p><a href="<?=site_url('calendar')?>" target="main-frame">Calendar</a></p>      	
      	<?php } else if($this->session->userdata('user_level') == 3){?>
      	<p><a href="<?=site_url('approve')?>" target="main-frame">Leave Approval</a></p>
      	<p><a href="<?=site_url('calendar')?>" target="main-frame">Calendar</a></p>
      	<?php } else if($this->session->userdata('user_level') == 4){?>
      	<p><a href="<?=site_url('approve')?>" target="main-frame">Leave Approval</a></p>
      	<p><a href="<?=site_url('calendar')?>" target="main-frame">Calendar</a></p>      	
      	<?php }?>
      	<p><a href="<?=site_url('search')?>" target="main-frame">Filter</a></p>
	</div>
	<?php if($this->session->userdata('user_level') > 1){?>
  	<div class="Block-body">
		<div class="BlockHeader">
	        <div class="l"></div>
	        <div class="r"></div>
	        <div class="t">History</div>
	    </div>
	    <?php if($this->session->userdata('user_level') > 3){?>
	    <p><a href="<?=site_url('history/request')?>" target="main-frame">Leave Request</a></p>
	    <?}?>
	    <p><a href="<?=site_url('approve/past')?>" target="main-frame">Past Leave Records</a></p>
	</div>
	<?php }?>
	<div class="Block-body">
	<div class="BlockHeader">
        <div class="l"></div>
        <div class="r"></div>
        <div class="t"></div>
    </div>
	</div>
  </div>
</body>
</html>
