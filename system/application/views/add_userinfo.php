<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<base href="<?=base_url()?>" />
<script type="text/javascript">
            function randomPassword() {
                var seed = new Array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z',
                'a','b','c','d','e','f','g','h','i','j','k','m','n','p','Q','r','s','t','u','v','w','x','y','z',
                '2','3','4','5','6','7','8','9');
                seedlength = seed.length;
                var createPassword = '';
                for (i=0; i<10; i++) {
                    j = Math.floor(Math.random() * seedlength);
                    createPassword += seed[j];
                }
                return createPassword;
            }

            function generatePassword() {
                document.forms[0].password.value = randomPassword();
                document.forms[0].password.focus();
            }

            function DateSelector(selYear, selMonth, selDay)
            {
                this.selYear = selYear;
                this.selMonth = selMonth;
                this.selDay = selDay;
                this.selYear.Group = this;
                this.selMonth.Group = this;
                if(window.document.all != null) // IE
                {
                    this.selYear.attachEvent("onchange", DateSelector.Onchange);
                    this.selMonth.attachEvent("onchange", DateSelector.Onchange);
                }
                else // Firefox
                {
                    this.selYear.addEventListener("change", DateSelector.Onchange, false);
                    this.selMonth.addEventListener("change", DateSelector.Onchange, false);
                }

                if(arguments.length == 4)
                    this.InitSelector(arguments[3].getFullYear(),
                arguments[3].getMonth() + 1, arguments[3].getDate());
                else if(arguments.length == 6)
                    this.InitSelector(arguments[3], arguments[4], arguments[5]);
                else
                {
                    var dt = new Date();
                    this.InitSelector(dt.getFullYear(), dt.getMonth() + 1, dt.getDate());
                }
            }

            DateSelector.prototype.MinYear = 1900;

            DateSelector.prototype.MaxYear = (new Date()).getFullYear();

            DateSelector.prototype.InitYearSelect = function()
            {
                for(var i = this.MaxYear; i >= this.MinYear; i--)
                {
                    var op = window.document.createElement("OPTION");
                    op.value = i;
                    op.innerHTML = i;
                    this.selYear.appendChild(op);
                }
            }

            DateSelector.prototype.InitMonthSelect = function()
            {
                for(var i = 1; i < 13; i++)
                {
                    var op = window.document.createElement("OPTION");
                    op.value = i;
                    op.innerHTML = i;
                    this.selMonth.appendChild(op);
                }
            }

            DateSelector.DaysInMonth = function(year, month)
            {
                var date = new Date(year, month, 0);
                return date.getDate();
            }

            DateSelector.prototype.InitDaySelect = function()
            {
                var year = parseInt(this.selYear.value);
                var month = parseInt(this.selMonth.value);

                var daysInMonth = DateSelector.DaysInMonth(year, month);

                this.selDay.options.length = 0;
                for(var i = 1; i <= daysInMonth ; i++)
                {
                    var op = window.document.createElement("OPTION");
                    op.value = i;
                    op.innerHTML = i;
                    this.selDay.appendChild(op);
                }
            }

            DateSelector.Onchange = function(e)
            {
                var selector = window.document.all != null ? e.srcElement : e.target;
                selector.Group.InitDaySelect();
            }

            DateSelector.prototype.InitSelector = function(year, month, day)
            {
                this.selYear.options.length = 0;
                this.selMonth.options.length = 0;

                this.InitYearSelect();
                this.InitMonthSelect();

                this.selYear.selectedIndex = this.MaxYear - year;
                this.selMonth.selectedIndex = month - 1;

                this.InitDaySelect();

                this.selDay.selectedIndex = day - 1;
            }
        </script>
<style type="text/css">
<!--
#form1 table tr td table {
	font-family: Tahoma, Helvetica, sans-serif;
	font-size: 11px;
}

#form1 p {
	text-align: center;
}

#input,select {
	font-family: Tahoma, Helvetica, Sans-Serif;
	font-size: 11px;
	font-style: normal;
	font-weight: normal;
}
-->
</style>
</head>

<body>
<?php 
echo $this->session->flashdata('user');
echo form_open('user/save_info');
?>
<table width="100%" height="500" border="0" align="center" cellpadding="0" cellspacing="0"
	background="images/Footer.png">
	<tr>
		<td>
		<table width="100%" height="568" border="0" cellspacing="1" cellpadding="1">
			<tr bgcolor="#EBEBEB">
				<td width="50%" height="20" colspan="2" align="right" bgcolor="#EBEBEB" style="color: red">&nbsp;</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td width="25%" height="20">Login Username:</td>
				<td width="75%" height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%"><input type="text" name="user_name" id="user_name"
							value="<?php echo set_value('user_name');?>" style="font-size: 11px" /> <label
							style="color: #F00">＊</label></td>
						<td width="66%"><?php echo form_error('user_name'); ?></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">Login Password:</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="41%"><input type="text" name="password" id="password"
							value="<?php echo set_value('password');?>" style="font-size: 11px" /> <label
							style="color: #F00">＊</label><input type="button" name="generate" id="generate"
							value="Generate" style="font-size: 11px" onclick="javascript:generatePassword();" /></td>
						<td width="59%"><?php echo form_error('password'); ?></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">First Name:</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%"><input type="text" name="first_name" id="first_name"
							value="<?php echo set_value('first_name');?>" style="font-size: 11px" /> <label
							style="color: #F00">＊</label></td>
						<td width="66%"><?php echo form_error('first_name'); ?></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#FFFFFF">Last Name:</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%"><input type="text" name="last_name" id="last_name"
							value="<?php echo set_value('last_name');?>" style="font-size: 11px" /> <label
							style="color: #F00">＊</label></td>
						<td width="66%"><?php echo form_error('last_name'); ?></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#FFFFFF">Department:</td>
				<td height="20"><select name="department_id" id="department_id"
					style="font-size: 11px; width: 120px;">
					<? foreach ($department as $row): ?>
					<option value="<?=$row->id?>"><?=$row->department?></option>
					<? endforeach; ?>
				</select></td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#FFFFFF">User Level:</td>
				<td height="20"><select name="user_level_id" id="user_level_id"
					style="font-size: 11px; width: 120px;">
					<? foreach ($userlevel as $row): ?>
					<option value="<?=$row->id?>"><?=$row->user_level?></option>
					<? endforeach; ?>
				</select></td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#FFFFFF">User Type:</td>
				<td height="20"><select name="user_type" id="user_type" style="font-size: 11px; width: 120px"
					onchange="javascript:changeUserType(this.value);">
					<option value="Full Time" selected="selected">Full Time</option>
					<option value="Part Time">Part Time</option>
					<option value="Casual/Trial">Casual/Trial</option>
				</select></td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#EBEBEB">Contact Details</td>
				<td height="20" bgcolor="#EBEBEB">&nbsp;</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Address:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="73%"><input name="address" type="text" id="address" style="font-size: 11px"
							value="<?php echo set_value('address');?>" size="70" /> <label style="color: #F00">＊</label></td>
						<td width="27%"><?php echo form_error('address'); ?></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Home Phone:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%"><input name="home_phone" type="text" id="home_phone" style="font-size: 11px"
							value="<?php echo set_value('home_phone');?>" size="20" /> <label style="color: #F00">＊</label></td>
						<td width="66%"><?php echo form_error('home_phone'); ?></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Mobile:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%"><input name="mobile" type="text" id="mobile" style="font-size: 11px"
							value="<?php echo set_value('mobile');?>" size="30" /> <label style="color: #F00">＊</label></td>
						<td width="66%"><?php echo form_error('mobile'); ?></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Email:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="55%"><input name="email" type="text" id="email" style="font-size: 11px"
							value="<?php echo set_value('email');?>" size="50" /> <label style="color: #F00">＊</label></td>
						<td width="45%"><?php echo form_error('email'); ?></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">Date of Birth:</td>
				<td height="20"><select id="selYear" name="selYear"></select> <select id="selMonth"
					name="selMonth"></select> <select id="selDay" name="selDay"></select> <script
					type="text/javascript">
                                    var selYear = window.document.getElementById("selYear");
                                    var selMonth = window.document.getElementById("selMonth");
                                    var selDay = window.document.getElementById("selDay");

                                    new DateSelector(selYear, selMonth ,selDay, 2005, 1, 1);
                                </script></td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#EBEBEB">Next of Kin</td>
				<td height="20" bgcolor="#EBEBEB">&nbsp;</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Name:</td>
					</tr>
				</table>
				</td>
				<td height="20"><input type="text" name="kinname" id="kinname" style="font-size: 11px"
					value="<?php echo set_value('kinname');?>" /></td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Contact details:</td>
					</tr>
				</table>
				</td>
				<td height="20"><input name="contact" type="text" id="contact" size="50" style="font-size: 11px"
					value="<?php echo set_value('contact');?>" /></td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">Medical conditions (if any):</td>
				<td height="20"><input name="medical" type="text" id="medical" size="50" style="font-size: 11px"
					value="<?php echo set_value('medical');?>" /></td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">Driver License No:</td>
				<td height="20"><input type="text" name="driverno" id="driveno" style="font-size: 11px"
					value="<?php echo set_value('driverno');?>" /></td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">&nbsp;</td>
				<td height="20"><label> <input name="driverfile" type="file" id="driverfile"
					style="font-size: 11px; font-weight: normal;" size="50"
					value="<?php echo set_value('driverfile');?>" />(Please attach the copy of your driver
				license.)</label></td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">TFN:</td>
				<td height="20"><input type="text" name="tfn" id="tfn" style="font-size: 11px"
					value="<?php echo set_value('tfn');?>" /></td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" bgcolor="#EBEBEB">Bank Details</td>
				<td height="20" bgcolor="#EBEBEB">&nbsp;</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Account Name:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%"><input type="text" name="accountname" id="accountname"
							value="<?php echo set_value('accountname');?>" style="font-size: 11px" /> <label
							style="color: #F00">＊</label></td>
						<td width="66%"><?php echo form_error('accountname'); ?></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Bank:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%"><input type="text" name="bankname" id="bankname"
							value="<?php echo set_value('bankname');?>" style="font-size: 11px" /> <label
							style="color: #F00">＊</label></td>
						<td width="66%"><?php echo form_error('bankname'); ?></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">BSB:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%"><input type="text" name="bsb" id="bsb" value="<?php echo set_value('bsb');?>"
							style="font-size: 11px" /> <label style="color: #F00">＊</label></td>
						<td width="66%"><?php echo form_error('bsb'); ?></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="22%">&nbsp;</td>
						<td width="78%">Account No:</td>
					</tr>
				</table>
				</td>
				<td height="20">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="34%"><input type="text" name="accountno" id="accountno"
							value="<?php echo set_value('accountno');?>" style="font-size: 11px" /> <label
							style="color: #F00">＊</label></td>
						<td width="66%"><?php echo form_error('accountno'); ?></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td height="20" colspan="2" align="center" bgcolor="#EBEBEB"><input style="font-size: 11px"
					type="submit" name="button" id="button" value="     Continue -->     " /></td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</form>
</body>
</html>
