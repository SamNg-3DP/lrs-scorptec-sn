<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add new daily shift</title>
<base href="<?=base_url ()?>" />

<script type="text/javascript" src="js/mootools.v1.11.js"></script>
<script type="text/javascript" src="js/nogray_time_picker_min.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
jQuery.noConflict(); 

window.addEvent("domready", function (){
var tp = new TimePicker('starttime0_picker', 'starttime0', 'starttime0_toggler', {format24:true});
new TimePicker('endtime0_picker', 'endtime0', 'endtime0_toggler', {format24:true});
});

var num = 0;
function add_slot(){
	num++;
	jQuery.ajax({
		async: false,
		type: 'POST',
		url: "<?=site_url("dailyshift/get_new_slot_ajax")?>",
		data: {
		  	num: num,
		},
		success: function(data){
			var i = num-1;
			while(i >= 0){
				var obj = jQuery("#slot"+i);
				if(obj.length > 0){
					obj.after(data.content);
					break;
				}
				i--;
			}
	  	},
		dataType: 'json'
	});
}

function remove_slot(num){
	jQuery("#slot"+num).remove();
}

function check(){
	var shiftcode = jQuery.trim(jQuery("#shiftcode").val());
	if(!shiftcode){
		alert("Please enter shift code");
		return false;
	}
	
	var description = jQuery.trim(jQuery("#description").val());
	if(!description){
		alert("Please enter description");
		return false;
	}
	var i = 0;
	var bslot = false;
	while(i <= num){
		var obj = jQuery("#slot"+i);
		if(obj.length > 0){
			var starttime = jQuery.trim(jQuery("#starttime"+i).val());
			var endtime = jQuery.trim(jQuery("#endtime"+i).val());
			var shiftcategory = jQuery("#shiftcategory"+i).val();
			if(starttime == ""){
				alert("Please select start time");
				bslot = true;
				break;
			}
			if(endtime == ""){
				alert("Please select end time");
				bslot = true;
				break;
			}
			if(endtime == "00:00"){
				alert("Please select end time again");
				bslot = true;
				break;
			}
			if(starttime == endtime){
				alert("start time should not equal to end time");
				bslot = true;
				break;
			}
			if(starttime > endtime){
				alert("start time should not greater than end time");
				bslot = true;
				break;
			}
			if(shiftcategory == ""){
				alert("Please select shift category");
				bslot = true;
				break;
			}
		}
		i++;
	}
	if(bslot){
		return false;		
	}
	jQuery("#slot_num").val(num);
	return true;
}
	
</script>
</head>

<body>
<?php echo form_open ( 'dailyshift/do_add', 'onsubmit="return check()"' ); ?>
  <input type="hidden" name="slot_num" id="slot_num" value="0" />
  <table width="100%" border="0" cellspacing="0" cellpadding="0" background="images/Footer.png">
    <tr>
      <td>
      <table width="100%" border="0" cellspacing="1" cellpadding="0">
        <tr>
          <td height="20" colspan="3" bgcolor="#EBEBEB">&nbsp;</td>
        </tr>
        <tr>
          <td width="20%" height="20" bgcolor="#FFFFFF">Shift Code:</td>
          <td height="20" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="30%"><input name="shiftcode" type="text" id="shiftcode" style="font-size:11px" size="20" value="<?php echo set_value ( 'shiftcode' );?>"/>
                <span style="color: #F00">＊</span></td>
                <td><?php echo form_error ( 'shiftcode' );?></td>
              </tr>
          </table></td>
          <td height="20" bgcolor="#FFFFFF"></td>
        </tr>
        <tr>
          <td height="20" bgcolor="#FFFFFF">Description:</td>
          <td height="20" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="65%"><input name="description" type="text" id="description" style="font-size:11px" size="70" value="<?php echo set_value ( 'description' );?>"/>
                <span style="color: #F00">＊</span></td>
                <td><?php echo form_error ( 'description' );?></td>
              </tr>
          </table></td>
          <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
        <tr>
          <td height="20" bgcolor="#FFFFFF">Notes:</td>
          <td height="20" bgcolor="#FFFFFF"><label>
            <textarea name="notes" id="notes" cols="45" rows="5" style="font-size:11px; font-family:Tahoma, Geneva, sans-serif"></textarea>
          </label></td>
          <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
        <tr>
          <td height="20" bgcolor="#FFFFFF">Site:</td>
          <td height="20" bgcolor="#FFFFFF"><label>
            <select name="site" id="site" style="font-size:11px">
            	<? foreach($site as $row): ?>
              		<option value="<?=$row->id?>"><?=$row->name?></option>
                <? endforeach; ?>
            </select>
          </label></td>
          <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
	    <tr>
	      <td height="20" bgcolor="#FFFFFF">Lunch Hour:</td>
	      <td height="20" bgcolor="#FFFFFF">
		      <input type="text" size="2" name="lunch" id="lunch" />
	      </td>
	      <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
	    </tr>
        
        <tr><td colspan="2">
        	&nbsp;
        </td></tr>
        
        
        <tr>
          <td height="20" colspan="2" bgcolor="#EBEBEB">Time Slot&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void(0)' title="Add new time slot" style="text-decoration:none; " onclick='add_slot();'>+</a></td>
          <td height="20" bgcolor="#FFFFFF" >&nbsp;</td>
        </tr>
        
        <tr><td colspan="2">
		        <tbody id="slot0">
			        <tr>
			          <td height="20" bgcolor="#FFFFFF">Start Time:</td>
			          <td height="20" bgcolor="#FFFFFF">
			          <input type="text" name="starttime0" id="starttime0" /> <a href="#" id="starttime0_toggler">Open time picker</a>
						<div id="starttime0_picker" class="time_picker_div"/>
			          </td>
			          <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
			        </tr>
			        <tr>
			          <td height="20" bgcolor="#FFFFFF">End Time:</td>
			          <td height="20" bgcolor="#FFFFFF">
			          <input type="text" name="endtime0" id="endtime0" /> <a href="#" id="endtime0_toggler">Open time picker</a>
						<div id="endtime0_picker" class="time_picker_div"/>
			          </td>
			          <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
			        </tr>
			        <tr>
			          <td height="20" bgcolor="#FFFFFF">Shift Category:</td>
			          <td height="20" bgcolor="#FFFFFF">
			          <label>
			            <?
			            $js = 'id="shiftcategory0" style="font-size:11px" ';
			            echo form_dropdown('shiftcategory0', $shiftcategory, set_value('shiftcategory0'), $js);
			            ?>
			          </label>
			          </td>
			          <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
			        </tr>
			        <tr>
			          <td height="20" bgcolor="#FFFFFF">
			          Rate:
			          </td>
			          <td height="20" bgcolor="#FFFFFF">
			          <label>
			          	<?
			            $js = 'id="rate0" style="font-size:11px" ';
			            echo form_dropdown('rate0', $rates, set_value('rate0'), $js);
			            ?>
			          </label>
			          </td>
			          <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
			        </tr>
	        	</tbody>
        	</td>
        </tr>
        <tr>
          <td height="20" colspan="3" align="center" bgcolor="#EBEBEB"><label>
            <input type="submit" name="button" id="button" value="     Save     " style="font-size:11px"/>
          </label></td>
        </tr>
      </table>
      </td>
    </tr>
  </table>
</form>
</body>
</html>