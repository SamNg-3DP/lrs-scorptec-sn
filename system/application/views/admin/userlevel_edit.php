<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<base href="<?=base_url()?>"/>
<style>
#input, select
{
	font-family: Tahoma, Helvetica, Sans-Serif;
	font-size: 11px;
	font-style: normal;
	font-weight: normal;
}
</style>
</head>

<body onload="document.forms[0].user_level.focus();">
<form id="form1" name="form1" method="post" action="index.php/user_level/edit">
  <table width="100%" height="22" border="0" align="center" cellpadding="0" cellspacing="0" background="images/Footer.png">
    <tr>
      <td>
      <? foreach ($user_level as $row): ?>
      <table width="100%" height="85" border="0" cellspacing="1" cellpadding="1">
        <tr bgcolor="#EBEBEB">
            <td width="50%" height="20" colspan="2" align="right" bgcolor="#EBEBEB" style="color: red">&nbsp;</td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td width="50%" height="20" align="right">User Level Name:</td>
          <td width="50%" height="20">
          	<label>
                <input name="user_level" type="text" id="userlevel" size="30" style="font-size: 11px" value="<?=$row->user_level?>" />
          	</label>
            <span style="color: #F00;">*<span style="color: red">
            <?=$status?>
            </span></span></td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td height="20" align="right">Status:</td>
          <td height="20"><select name="status" id="status">
            <? if ($row->status == ACTIVE) { ?>
            <option value="<?=ACTIVE?>" selected="selected">Active</option>
            <option value="<?=IN_ACTIVE?>">Inactive</option>
            <? } else { ?>
            <option value="<?=ACTIVE?>">Active</option>
            <option value="<?=IN_ACTIVE?>" selected="selected">Inactive</option>
            <? } ?>
          </select></td>
        </tr>
        <tr bgcolor="#FFFFFF">
          <td height="20" colspan="2" align="center" bgcolor="#EBEBEB"><input style="font-size: 11px" type="submit" name="submit" id="submit" value="     Edit     " /></td>
          </tr>
      </table>
          <input type="hidden" name="id" id="id" value="<?=$row->id?>" />
      <? endforeach; ?>
      </td>
    </tr>
  </table>
</form>
</body>
</html>