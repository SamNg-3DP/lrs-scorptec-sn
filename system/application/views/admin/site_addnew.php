<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add New Site</title>
<base href="<?=base_url()?>" />
</head>

<body onload="document.forms[0].site.focus();">
<?php echo form_open ( 'site/addnew' ); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" background="images/Footer.png">
	<tr>
		<td>
		<table width="100%" border="0" cellspacing="1" cellpadding="0">
			<tr>
				<td height="20" colspan="3" bgcolor="#EBEBEB">&nbsp;</td>
			</tr>
			<tr>
				<td width="40%" height="20" align="right" bgcolor="#FFFFFF">Site Name:</td>
				<td height="20" bgcolor="#FFFFFF">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="35%"><input name="name" type="text" id="name" style="font-size: 11px" size="30"
							value="<?php echo set_value ( 'name' ); ?>"/></td>
						<td width="65%"><?php echo form_error('name'); ?></td>
					</tr>
				</table>
				<label> </label></td>
				<td height="20" bgcolor="#FFFFFF">&nbsp;</td>
			</tr>
			<tr>
				<td width="40%" height="20" align="right" bgcolor="#FFFFFF">Status:</td>
				<td height="20" bgcolor="#FFFFFF">
					<select id="state" name="state" style="font-size: 11px">
						<option value="1">Active</option>
						<option value="0">Inactive</option>
					</select>
				</td>
				<td height="20" bgcolor="#FFFFFF">&nbsp;</td>
			</tr>
			<tr>
				<td height="20" colspan="3" align="center" bgcolor="#EBEBEB"><label> <input type="submit"
					name="button" id="button" value="     Save     " style="font-size: 11px" /> </label></td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</form>
</body>
</html>
