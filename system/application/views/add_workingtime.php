<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <base href="<?=base_url()?>"/>
        <script type="text/javascript" src="js/mootools.js"></script>
        <script type="text/javascript" src="js/calendar.rc4.js"></script>
        <script type="text/javascript">
            //<![CDATA[
            window.addEvent('domready', function() {
                myCal1 = new Calendar({ fromdate: 'd/m/Y' }, { direction: 1, tweak: {x: 6, y: 0} });
                myCal2 = new Calendar({ todate: 'd/m/Y' }, { direction: 1, tweak: {x: 6, y: 0} });
            });
            //]]>
        </script>
        <link rel="stylesheet" type="text/css" href="css/iframe.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/calendar.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/dashboard.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/i-heart-ny.css" media="screen" />
        <style type="text/css">
            <!--
            td {
                font-family: Tahoma, Geneva, sans-serif;
            }
            td {
                font-size: 11px;
            }
            p {
                text-align: center;
            }
            -->
        </style>
        <script language="javascript" type="text/javascript" src="WdatePicker.js"></script>
    </head>
    <body>
        <?php echo form_open('user/save_workingtime'); ?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" background="images/Footer.png">
                <tr>
                    <td><table width="100%" border="0" cellspacing="1" cellpadding="0">
                            <tr>
                                <td height="22" colspan="2" align="right" bgcolor="#EBEBEB">&nbsp;</td>
                            </tr>
                            <tr bgcolor="#FFFFFF">
                                <td width="25%" height="22" bgcolor="#FFFFFF">Department:</td>
                                <td width="77%" height="22" bgcolor="#FFFFFF"><label>
                                        <select name="department_id" id="department_id" style="font-size: 11px">
                                            <? foreach ($department as $row): ?>
                                            <option value="<?=$row->id?>">
                                                    <?=$row->department?>
                                            </option>
                                            <? endforeach; ?>
                                        </select>
                                    </label></td>
                            </tr>
                            <tr bgcolor="#FFFFFF">
                                <td height="22" bgcolor="#FFFFFF">User Level:</td>
                                <td height="22" bgcolor="#FFFFFF"><label>
                                        <select name="userlevel_id" id="userlevel_id" style="font-size: 11px">
                                            <? foreach ($userlevel as $row): ?>
                                            <option value="<?=$row->id?>">
                                                    <?=$row->userlevel?>
                                            </option>
                                            <? endforeach; ?>
                                        </select>
                                    </label></td>
                            </tr>
                            <tr>
                                <td width="25%" height="22" bgcolor="#FFFFFF">User Type:</td>
                                <td width="77%" height="22" bgcolor="#FFFFFF"><label>
                                        <select name="usertype" id="usertype" style="font-size: 11px" onchange="javascript:changeUserType(this.value);">
                                            <option value="Full Time">Full Time</option>
                                            <option value="Part Time">Part Time</option>
                                            <option value="Casual/Trial">Casual/Trial</option>
                                        </select>
                                    </label></td>
                            </tr>
                            <tr>
                                <td height="22" bgcolor="#FFFFFF">Labor Contract Period:</td>
                                <td height="22" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                <td width="30">From</td>
                                    <td width="150"><input id="fromdate" name="fromdate" type="text" /></td>
                                        <td width="20">To</td>
                                        <td><input id="todate" name="todate" type="text" /></td>
                                  </tr>
                                    </table>
                        </tr>
                        <tr>
                            <td height="22" colspan="2" align="center" bgcolor="#EBEBEB"><input type="button" name="button2" id="button4" value="     Back     " style="font-size:11px">                <input type="submit" name="button3" id="button5" value="      Save     " style="font-size:11px"></td>
                        </tr>
                    </table></td>
                </tr>
            </table>
        </form>

    </body>
</html>
