<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Edit payroll</title>
<base href="<?=base_url ()?>" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.ui.css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.tools.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">
//<![CDATA[
function change_site(){
	var site = $("#site_id").val();
	if(site == ""){
		alert("Please select a site");
		return false;
	}
	var start_day = $("#start_day").val();
	var rolldays = $("#rolldays").val();
	if(start_day && rolldays > 0){
		change();
	}
}
function change(){
	var site = $("#site_id").val();
	if(site == ""){
		alert("Please select a site");
		return false;
	}
	
	var start_day = $("#start_day").val();
	var rolldays = $("#rolldays").val();
	if(!start_day){
		alert("Please select start date");
		return false;
	}
	if(start_day && rolldays > 0 && site){
		$.ajax({
			async: false,
			type: 'POST',
			url: "<?=site_url("payrollshift/get_rolldays_ajax")?>",
			data: {
			  	start_day: start_day, 
			  	rolldays: rolldays,
			  	site:site
			},
			success: function(data){
				$("#list_roll").find("tr:gt(0)").remove();
				$('#list_roll tr:last').after(data.content);
		  	},
			dataType: 'json'
		});
	}else{
		$("#list_roll").find("tr:gt(0)").remove();
	}
}

function check_unique(){
	var exist = false;
	$.ajax({
		async: false,
		type: 'POST',
		url: "<?=site_url("payrollshift/check_code_ajax")?>",
		data: {
			payrollshiftcode: $("#payrollshiftcode").val(),
			id: $("#payrollshift_id").val()
		},
		success: function(data){
			if(data.status == "exist"){
				exist = true;
			}
	  	},
		dataType: 'json'
	});
	return exist;
}

function show_desc(value, index){
	if(value!=""){
		$.ajax({
			type: 'POST',
			url: "<?=site_url("dailyshift/get_daily_shift_ajax")?>",
			data: {
			  	id: value
			},
			success: function(data){
				$("#desc_"+index).html(data.description);
		  	},
			dataType: 'json'
		});
	}else{
		$("#desc_"+index).html("");
	}
}

function check(){
	var site = $("#site_id").val();
	if(site == ""){
		alert("Please select a site");
		return false;
	}
	var payrollshiftcode = $("#payrollshiftcode").val();
	if($.trim(payrollshiftcode) == ""){
		alert("Please enter payrollshift code");
		return false;
	}else{
		if(check_unique()){
			alert("Payroll code has existed. Please enter again.");
			return false;
		}
	}
	var description = $("#description").val();
	if($.trim(description) == ""){
		alert("Please enter description");
		return false;
	}
	
	var paydays = $("#paydays").val();
	if(paydays == ""){
		alert("Please select pay days");
		return false;
	}
	
	var start_day = $("#start_day").val();
	if($.trim(start_day) == ""){
		alert("Please enter start date");
		return false;
	}
	
	var rolldays = $("#rolldays").val();
	if(rolldays == ""){
		alert("Please select roll days");
		return false;
	}
	if(rolldays > 0){
		var shift_0 = $("#shift_0");
		if(shift_0.length <= 0){
			if(start_day && rolldays > 0){
				$.ajax({
					async: false,
					type: 'POST',
					url: "<?=site_url("payrollshift/get_rolldays_ajax")?>",
					data: {
					  	start_day: start_day, 
					  	rolldays: rolldays,
					  	site:site
					},
					success: function(data){
						$("#list_roll").find("tr:gt(0)").remove();
						$('#list_roll tr:last').after(data.content);
				  	},
					dataType: 'json'
				});
			}
		}
		for(var i = 0; i < rolldays; i++){
			var shift = $("#shift_"+i).val();
			if(shift == ""){
				alert("Every shift must be selected. Please select.");
				return false;
			}
		}
	}
	return true;
}
$(function() {
    $("#start_day").datepicker({ 
    	disabled: true, 
    	dateFormat: '<?=JS_DATE_FORMAT?>',
    	changeMonth: true,
		changeYear: true 
	});    
});

//]]>
</script>
</head>
<body>
<?php echo form_open ( 'payrollshift/do_edit', 'onsubmit="return check()"' ); ?>
  <input type="hidden" name="payrollshift_id" id="payrollshift_id" value="<?=$payrollshift->id?>"/>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" background="images/Footer.png">
    <tr>
      <td><table width="100%" border="0" cellspacing="1" cellpadding="0">
        <tr>
          <td height="20" colspan="3" bgcolor="#EBEBEB">&nbsp;</td>
        </tr>
        <tr>
          <td height="20" bgcolor="#FFFFFF">Site:</td>
          <td height="20" bgcolor="#FFFFFF"><label>
		  <?php 
	      	$site = 'site_id';
			$js = 'id="'.$site.'" onchange="change_site()" style="font-size: 11px;" ';
			echo form_dropdown($site, $sites, set_value($site,$payrollshift->site_id), $js);
           ?>
          </label>
          <span style="color: #F00">＊</span>
          </td>
          <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
        <tr>
          <td width="20%" height="20" bgcolor="#FFFFFF">Payroll Shift Code:</td>
          <td height="20" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="30%"><input name="payrollshiftcode" type="text" id="payrollshiftcode" style="font-size:11px" size="20" value="<?php echo $payrollshift->payrollshiftcode;?>"/>
                <span style="color: #F00">＊</span></td>
                <td><?php echo form_error ( 'payrollshiftcode' );?></td>
              </tr>
          </table></td>
          <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
        <tr>
          <td height="20" bgcolor="#FFFFFF">Description:</td>
          <td height="20" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="65%"><input name="description" type="text" id="description" style="font-size:11px" size="70" value="<?php echo $payrollshift->description;?>"/>
                <span style="color: #F00">＊</span></td>
                <td><?php echo form_error ( 'description' );?></td>
              </tr>
          </table></td>
          <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
        <tr>
          <td height="20" bgcolor="#FFFFFF">Notes:</td>
          <td height="20" bgcolor="#FFFFFF"><label>
            <textarea name="notes" id="notes" cols="45" rows="5" style="font-size:11px; font-family:Tahoma, Geneva, sans-serif"><?=$payrollshift->notes?></textarea>
          </label></td>
          <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
        <tr>
          <td height="20" bgcolor="#FFFFFF">Pay (Days):</td>
          <td height="20" bgcolor="#FFFFFF">
          <label>
          <?php 
          	$js = 'id="paydays" style="font-size:11px"';
          	echo form_dropdown("paydays" , $paydayses, $payrollshift->paydays, $js);
          ?>
          </label>
          <span style="color: #F00">＊</span>
          </td>
          <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
        <tr>
          <td height="20" bgcolor="#FFFFFF">Start Date:</td>
          <td height="20" bgcolor="#FFFFFF">
          	<label>
            <input type="text" name="start_day" id="start_day"
				value="<?php echo date(DATE_FORMAT, $payrollshift->start_date);?>" style="font-size: 11px"
                onchange="javascript: change();"/>
          	</label>
          	<span style="color: #F00">＊</span>
          </td>
          <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
        <tr>
          <td height="20" bgcolor="#FFFFFF">Roll (Days):</td>
          <td height="20" bgcolor="#FFFFFF">
          <label>
          <?php 
          	$js = 'id="rolldays" onchange="change()" style="font-size:11px"';
          	echo form_dropdown("rolldays" , $rolldayses, $payrollshift->rolldays, $js);
          ?>
          </label>
          <span style="color: #F00">＊</span>
          </td>
          <td height="20" bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
        <tr>
        	<td colspan="3" bgcolor="#FFFFFF">
        		<table width="600" border="0" cellspacing="1" cellpadding="1" id="list_roll" >
                	<tr bgcolor="#FFFFFF" style="background-image:url(images/TableHeader.png); font-family: Tahoma, Helvetica, sans-serif; font-size: 11px;">
                    	<td width="20" height="20">Start</td>
                        <td width="80">Name</td>
                     	<td width="40">Shift</td>
                     	<td >Description</td>
                	</tr>
                	<? 
                	$weekarray =  array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
                	$i = 0;
                	foreach ($rolls as $row): ?>
                	<tr bgcolor="#FFFFFF" onmouseover="javascript:this.bgColor = '#EBEBEB';" onmouseout="javascript:this.bgColor = '#FFFFFF';">
				        <td height="20"><?=$row->start?></td>
				        <td height="20"><?
				        echo $weekarray[$row->name];
				        ?></td>
				        <td><?php 
				        $shift = "shift_" . $i;
				        $js = 'id="'.$shift.'" style="font-size: 11px; width: 120px;" onChange="show_desc(this.value, \''.$i.'\')"';
						echo form_dropdown($shift, $daily_shifts, $row->daily_shift_id, $js);
				        ?></td>
				        <td ><div id="desc_<?=$i?>"><?=$row->description?></div></td>
				        <input type="hidden" name="start_<?=$i?>" value="<?=$row->start?>" />
				        <input type="hidden" name="name_<?=$i?>" value="<?=$row->name?>" />
				    </tr>
                	<? 
                	$i++;
                	endforeach; ?>
        		</table>
        	</td>
        </tr>
        <tr>
          <td height="20" colspan="3" align="center" bgcolor="#EBEBEB"><label>
            <input type="submit" name="button" id="button" value="     Save     " style="font-size:11px"/>
          </label></td>
        </tr>
      </table></td>
    </tr>
  </table>
</form>
</body>
</html>