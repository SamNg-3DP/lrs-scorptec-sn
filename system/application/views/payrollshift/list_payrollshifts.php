<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<base href="<?=base_url()?>"/>
<title>List Payroll Shift</title>
</head>

<body>
<form id="form1" name="form1" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" background="images/Footer.png">
    <tr>
      <td><table width="100%" border="0" cellspacing="1" cellpadding="0">
        <tr>
          <td height="20" colspan="9" align="right" bgcolor="#EBEBEB"><a href="<?=site_url('payrollshift/add')?>">ADD NEW</a></td>
        </tr>
        <tr style="background-image:url('images/TableHeader.png');">
          <td height="20">Site</td>
          <td>Payroll Code</td>
          <td>Description</td>
          <td>Pay (Days)</td>
          <td>Start Date</td>
          <td>Roll (Days)</td>
          <td colspan="3" align="center">Action</td>
        </tr>
        <? foreach ($payrollshifts as $row): ?>
        <tr bgcolor="#FFFFFF">
          <td ><?=$row->site_name?></td>
          <td ><?=$row->payrollshiftcode?></td>
          <td ><?=$row->description?></td>
          <td ><?=$row->paydays/7?> Weeks</td>
          <td ><?=date(DATE_FORMAT,$row->start_date)?></td>
          <td ><?=$row->rolldays/7?> Weeks</td>
          <td align="center">
           <a href="<?php echo site_url("payrollshift/view/".$row->id)?>"><img src="images/action/b_select.png" alt="" width="16" height="16" border="0" title="View"/></a>
           </td>
           <td align="center">
           <a href="<?php echo site_url("payrollshift/edit/".$row->id)?>"><img src="images/action/b_edit.png" alt="" width="16" height="16" border="0" title="Edit"/></a>
           </td>
          <td align="center">
          	<a href="<?php echo site_url("payrollshift/remove/".$row->id)?>"><img src="images/action/b_drop.png" alt="" width="16" height="16" border="0" title="Remove"/></a>
          </td>
        </tr>
        <? endforeach; ?>
      </table></td>
    </tr>
  </table>
</form>
</body>
</html>