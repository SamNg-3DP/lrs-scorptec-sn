<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US" xml:lang="en">
<head>
	<title>LRS Left Menu</title>
	<base href="<?=base_url()?>"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
</head>
<body>
<div class="Block">
	<div class="Block-tl"></div>
    <div class="Block-tr"></div>
    <div class="Block-bl"></div>
    <div class="Block-br"></div>
    <div class="Block-tc"></div>
    <div class="Block-bc"></div>
    <div class="Block-cl"></div>
    <div class="Block-cr"></div>
    <div class="Block-cc"></div>
	<div class="Block-body">
		<div class="BlockHeader">
			<div class="l"></div>
        	<div class="r"></div>
        	<div class="t">Job Incident</div>
      	</div>
	    <p><a href="<?=site_url('jobincident/incident_list/new')?>" target="main-frame">Saved (<?php echo $new_count?>)</a></p>
	    <p><a href="<?=site_url('jobincident/incident_list/progress')?>" target="main-frame">Submitted (<?php echo $progress_count?>)</a></p>
	    <p><a href="<?=site_url('jobincident/incident_list/completed')?>" target="main-frame">Completed (<?php echo $completed_count?>)</a></p>

	</div>
	<div class="Block-body">
	<div class="BlockHeader">
        <div class="l"></div>
        <div class="r"></div>
        <div class="t"></div>
    </div>
	</div>
  </div>
</body>
</html>
