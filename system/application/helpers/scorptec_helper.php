<?php

/**
 * database table record status 
 */
define('ACTIVE', 1);
define('IN_ACTIVE', 0);

define('ROLE_STAFF', 1);
define('ROLE_SUPERVISOR', 2);
define('ROLE_MANAGER', 3);
define('ROLE_ADMINISTRAOTR', 4);

/*
 * date format for PHP
 */
define('DATE_FORMAT', "d-m-Y");
define('DATETIME_FORMAT', "d-m-Y H:i");
define('TIME_FORMAT', "H:i"); //e.g. 09:30

/**
 * date formate for JavaScript
 */
define('JS_DATE_FORMAT', "dd-mm-yy");

/**
 * dirctory of Driver License
 */ 
define('DIR_PHOTO', './upload/photo/');

/**
 * dirctory of Driver License
 */ 
define('DIR_DRIVER_LICENSE', './upload/driver_license/');

/**
 * directory of Passport Lincense 
 */
define('DIR_PASSPORT_LICENSE', './upload/passport_license/');


/**
 * dirctory of labor contractor
 */
define('DIR_LABOR_CONTRACTOR', './upload/labor_contractor/');

/**
 * dirctory of medical certificate
 */
define('DIR_MEDICAL_CERTIFICATE', './upload/medical_certificate/');

/**
 * Number of items per page
 */
define('PAGE_SIZE', 50);

/**
 * === mail config ===
 */
define('MAIL_PROTOCOL', 'smtp');
define('SMTP_HOST', 'smtp.163.com');
define('SMTP_USER', 'scorptec');
define('SMTP_PASS', 'scorptec.com');
define('SMTP_PORT', 25);
define('WORDWRAP', TRUE);
define('MAILTYPE', 'html');
define('MAIL_CHARSET', 'utf-8');
define('MAIL_FROM', 'scorptec@163.com');

/**
 * operation constants
 */
define('OP_ADD', 'add');
define('OP_REMOVE', 'remove');
define('OP_UPDATE', 'update');

/**
 * leave type values
 */
define('LEAVE_TYPE_ANNUAL', 	1);
define('LEAVE_TYPE_UNPAID', 	2);
define('LEAVE_TYPE_PERSONAL', 	3);
define('LEAVE_TYPE_TRAINING', 	4);
define('LEAVE_TYPE_LATE', 		5);
define('LEAVE_TYPE_LUNCH', 		6);
define('LEAVE_TYPE_CLIENTVISIT',7);
define('LEAVE_TYPE_OVERTIME', 	8);
define('LEAVE_TYPE_COMPASIONATE',9);
define('LEAVE_TYPE_CLOCK',		10);

define('MAX_NO_MC_HOURS', 		15);
define('MAX_COMPASIONATE_HOURS',15);


/**
 * employment_type
 */

define('EMPLOYMENT_TYPE_FULLTIME', 		1);
define('EMPLOYMENT_TYPE_PARTTIME', 		2);
define('EMPLOYMENT_TYPE_CASUAL', 		3);

/**
 * work hours for each employment type
 */

//full time work hours for from monday to friday
define('FULLTIME_HOURS_M_F', 		7.75);
define('FULLTIME_HOURS_SATURDAY', 	4.75);
define('FULLTIME_BREAK_M-F', 		0.75);
define('FULLTIME_BREAK_SATURDAY', 	0.25);




