<?php

/**
 * create directory recursively 
 * @access	public
 * @param	string	path to source
 * @param	
 */
if ( ! function_exists('mkdirs'))
{
	function mkdirs($dir, $mode = 0777, $recursive = true) {
		if( is_null($dir) || $dir === "" ){
			return FALSE;
		}
		if( is_dir($dir) || $dir === "/" ){
			return TRUE;
		}
		
		return mkdir($dir, $mode, $recursive);
	}
}

/**
 * generate unique random file name in directory which file name length is specified
 * 
 */
if ( ! function_exists('random_filename'))
{
	function random_filename($dir, $extension, $filename_length = 6){
		if(!empty($extension)){
			$start_str = substr($extension, 0, 1);
			if($start_str != "."){
				$extension = "." . $extension;
			}
		}
		
		$exist = TRUE;
		$random_name = "";
		do{
			$random_name = _genRandomString($filename_length) . $extension;
			if(!file_exists( $dir . "/" . $random_name)){
				$exist = FALSE;
			}
		}while($exist);
		return $random_name;
	}
}

function _genRandomString($length = 6) {
    $characters = "0123456789abcdefghijklmnopqrstuvwxyz";
    $string = "";    
    for ($p = 0; $p < $length; $p++) {
        $string .= $characters[mt_rand(0, strlen($characters))];
    }
    return $string;
}

/* End of file MY_directory_helper.php */
/* Location: ./system/applicatioin/helpers/MY_directory_helper.php */