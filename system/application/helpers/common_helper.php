<?php

if ( ! function_exists('status_list'))
{
	function status_list() 
	{   
	    $start = array(
			"" => "Please select...", 	
			ACTIVE => "Active",
			IN_ACTIVE => "Inactive"
		);
		return $start;  
	} 
}

/**
 * "Employment Period" (the number of years and days from the original 
 * hire	date until today next to it, for example 2 Year, 5 days.
 * @access	public
 * @param	string	
 * @param	
 */
if ( ! function_exists('flash_message'))
{
	function flash_message() 
	{   
	    // get flash message from CI instance   
	    $ci =& get_instance();   
	    $flashmsg = $ci->session->flashdata('message');   
	   
	    $html = '';   
	    if (is_array($flashmsg))   
	    {   
	        $html = '<div id="flashmessage" class="'.$flashmsg[type].'">   
	            <img style="float: right; cursor: pointer" id="closemessage" src="'.base_url().'images/cross.png" />   
	            <strong>'.$flashmsg['title'].'</strong>   
	            <p>'.$flashmsg['content'].'</p>   
	            </div>';   
	    }   
	    return $html;   
	} 
}

if ( ! function_exists('calculate_working_hours'))
{
	function calculate_working_hours($labor, $start_time, $end_time, $include_break="false") 
	{   
	    //
	    if(empty($labor)){
	    	return 0;
	    }
	    
	    if($start_time > $end_time){
	    	return 0;
	    }
	    
		if($start_time == $end_time){
	    	return 0;
	    }
	    
	    //$a_start = getdate($start_time);
	    //$a_end = getdate($end_time);
	    
	    $hour_count = 0;
	    
		if($labor->employment_type_id == EMPLOYMENT_TYPE_FULLTIME){
			$b_calculate = false;
			$week_count = floor(($end_time - $start_time ) / ( 7 * 86400));
			if($week_count > 0){
				$hour_count += $week_count * (FULLTIME_HOURS_M_F * 5 + FULLTIME_HOURS_SATURDAY);
			}
			
			$first_week_end_time = $end_time - $week_count * 7 * 86400;
			
			$first_start_time = $start_time;
			//while($first_start_time < $first_week_end_time){
			$b_calculate = true;
			$i_start = 0; //indicate first time
			while($b_calculate){
				$tmp_a_start = getdate($first_start_time);
				$tmp_wday = $tmp_a_start['wday'];
				if($tmp_wday == 0){ 
					//sunday holiday, if start day is sunday ,then the day is holiday, 
					//and end time is not sunday, then move it into monday start time
					$tmp_start_day = date(DATE_FORMAT, $first_start_time);
					$tmp_next_date = date_add('d', 1 , $tmp_start_day);
					$tmp_next_day = date(DATE_FORMAT, $tmp_next_date) . " " . $labor->m_start_time;
					$first_start_time = strtotime($tmp_next_day);
					log_message("debug", "calculate_working_hours sunday next day == " . $tmp_next_day);
					if($first_start_time >= $first_week_end_time){
						$b_calculate = false;
					}
				}else if($tmp_wday >= 1 && $tmp_wday <= 5){
					$tmp_start_day = date(DATE_FORMAT, $first_start_time);//d-m-y
					$tmp_day_start = $tmp_start_day . " " . $labor->m_start_time; //h:s
					$tmp_day_end = $tmp_start_day . " " . $labor->m_end_time;
					
					log_message("debug", "calculate first_start_time == " . $tmp_start_day);
					log_message("debug", "calculate tmp_day_start == " . $tmp_day_start);
					log_message("debug", "calculate tmp_day_end == " . $tmp_day_end);
					log_message("debug", "calculate first_week_end_time == " . date(DATETIME_FORMAT, $first_week_end_time));

					if($i_start == 0){ //start time 
						if(strtotime($tmp_day_end) > $first_week_end_time){// in a day
							log_message("debug", "calculate m-f 1 ... $tmp_day_end > $first_week_end_time ");
							$tmp_hours = 0;
							if($first_start_time > strtotime($tmp_day_start)){
								$tmp_hours = ($first_week_end_time - $first_start_time) / 3600;
							}else{
								$tmp_hours = ($first_week_end_time - strtotime($tmp_day_start)) / 3600;
							}
							if($tmp_hours > FULLTIME_HOURS_M_F){//TODO check this
								$tmp_hours = FULLTIME_HOURS_M_F;
							}
							$hour_count += $tmp_hours; 
							$b_calculate = false;
						}else if(strtotime($tmp_day_end) == $first_week_end_time){// in a day
							log_message("debug", "calculate m-f 2 ... $tmp_day_end > $first_week_end_time ");
							$tmp_hours = 0;
							if($first_start_time > strtotime($tmp_day_start)){
								$tmp_hours = ($first_week_end_time - $first_start_time) / 3600;
							}else{
								$tmp_hours = FULLTIME_HOURS_M_F;
							}
							if($tmp_hours > FULLTIME_HOURS_M_F){//TODO check this
								$tmp_hours = FULLTIME_HOURS_M_F;
							}
							$hour_count += $tmp_hours;
							$b_calculate = false;
						}else{
							log_message("debug", "calculate m-f 3 ... ".strtotime($tmp_day_end)." < $first_week_end_time ");
							log_message("debug", "calculate m-f 3 ... ".$tmp_day_end." < " . date(DATETIME_FORMAT,$first_week_end_time));
							$tmp_hours= (strtotime($tmp_day_end) - $first_start_time) / 3600;
							if($tmp_hours > FULLTIME_HOURS_M_F){//TODO check this
								$tmp_hours = FULLTIME_HOURS_M_F;
							}
							$hour_count += $tmp_hours; 
							$next_date = date_add('d', 1 , $tmp_start_day);
							$next_date_time = $next_date;
							
							if($next_date_time >= $first_week_end_time){
								$b_calculate = false;
							}
							$first_start_time = $next_date_time;
						}
						log_message("debug", "calculate_working_hours hour_count 5...." . $hour_count);
					}else{
						log_message("debug", "calculate m-f b1 ... $tmp_day_end > $first_week_end_time ");
						if(strtotime($tmp_day_end) > $first_week_end_time){// in a day
							log_message("debug", "calculate m-f b2 ... $tmp_day_end > $first_week_end_time ");
							$tmp_hours = ($first_week_end_time - strtotime($tmp_day_start)) / 3600; //TODO calculate lunch?
							if($tmp_hours > FULLTIME_HOURS_M_F){//TODO check this
								$tmp_hours = FULLTIME_HOURS_M_F;
							}
							$hour_count += $tmp_hours; 
							$b_calculate = false;
						}else if(strtotime($tmp_day_end) == $first_week_end_time){// in a day
							log_message("debug", "calculate m-f b3 ... $tmp_day_end > $first_week_end_time ");
							$hour_count += FULLTIME_HOURS_M_F;
							$b_calculate = false;
						}else{
							log_message("debug", "calculate m-f b4 ... $tmp_day_end > $first_week_end_time ");
							$next_date = date_add('d', 1 , $tmp_start_day);
							log_message("debug", "calculate next_date == " . $next_date);
							$next_date_time = $next_date;
							
							$hour_count += FULLTIME_HOURS_M_F;
							log_message("debug", "2.. calculate hour_count == " . $hour_count);
							if($next_date_time >= $first_week_end_time){
								$b_calculate = false;
							}
							log_message("debug", "calculate next_date_time == " . date(DATETIME_FORMAT,$next_date_time));
							$first_start_time = $next_date_time;
						}
					}
				}else if($tmp_wday == 6){
					$tmp_start_day = date(DATE_FORMAT, $first_start_time);//d-m-y
					$tmp_day_start = $tmp_start_day . " " . $labor->s_start_time; //h:s
					$tmp_day_end = $tmp_start_day . " " . $labor->s_end_time;
					if($i_start == 0){
						if(strtotime($tmp_day_end) > $first_week_end_time){// in a day
							if($first_start_time > strtotime($tmp_day_start)){
								$hour_count += ($first_week_end_time - $first_start_time) / 3600;
							}else{
								$hour_count += ($first_week_end_time - strtotime($tmp_day_start)) / 3600;
							}
							$b_calculate = false;
						}else if(strtotime($tmp_day_end) == $first_week_end_time){// in a day
							if($first_start_time > strtotime($tmp_day_start)){
								$hour_count += ($first_week_end_time - $first_start_time) / 3600;
							}else{
								$hour_count += FULLTIME_HOURS_SATURDAY;
							}
							$b_calculate = false;
						}else{
							$next_date = date_add('d', 1 , $tmp_start_day);
							$next_date_time = $next_date;
							$hour_count += FULLTIME_HOURS_SATURDAY;
							if($next_date_time >= $first_week_end_time){
								$b_calculate = false;
							}else{
								$first_start_time = $next_date_time;
							}
						}
					}else{
						if(strtotime($tmp_day_end) > $first_week_end_time){// in a day
							$hour_count += ($first_week_end_time - strtotime($tmp_day_start)) / 3600; //TODO calculate lunch?
							$b_calculate = false;
						}else if(strtotime($tmp_day_end) == $first_week_end_time){// in a day
							$hour_count += FULLTIME_HOURS_M_F;
							$b_calculate = false;
						}else{
							$next_date = date_add('d', 1 , $tmp_start_day);
							$next_date_time = $next_date;
							
							if($next_date_time >= $first_week_end_time){
								$hour_count += FULLTIME_HOURS_M_F;
								$b_calculate = false;
							}else{
								$hour_count += FULLTIME_HOURS_M_F;
								$first_start_time = $next_date_time;
							}
						}
					}
				}
				log_message("debug", "3...calculate first_start_time == " . date(DATETIME_FORMAT,$first_start_time));
							 
				$i_start++;
				if($i_start > 1000){
					log_message("debug", "fulltime exception ......");
					$b_calculate = false;
				}
			}
			
			//only deduct break once for PARTIAL day 
			if($include_break == "true"){
				$break_count = 0;
				$tmp_a_start = getdate($start_time);
				$tmp_wday = $tmp_a_start['wday'];
				if($tmp_wday >= 1 && $tmp_wday <= 5){
					$break_count = 0.75;
				}else if($tmp_wday == 6){
					$break_count = 0.25;
				}
				if($hour_count > $break_count){
					$hour_count = $hour_count - $break_count;
				}
			}
		}else if($labor->employment_type_id == EMPLOYMENT_TYPE_PARTTIME){
			$b_calculate = false;
			$week_count = floor(($end_time - $start_time ) / ( 7 * 86400));
			if($week_count > 0){
				$hour_count += $week_count * (FULLTIME_HOURS_M_F * 5 + FULLTIME_HOURS_SATURDAY);
			}
			
			$first_week_end_time = $end_time - $week_count * 7 * 86400;
			
			$first_start_time = $start_time;
			//while($first_start_time < $first_week_end_time){
			$b_calculate = true;
			$i_start = 0; //indicate first time
			while($b_calculate){
				$tmp_a_start = getdate($first_start_time);
				$tmp_wday = $tmp_a_start['wday'];
				if($tmp_wday >= 0 && $tmp_wday <= 6){
					$tmp_start_day = date(DATE_FORMAT, $first_start_time);
					$tmp_day_start 	= ""; //h:s
					$tmp_day_end 	= "";
					if($tmp_wday == 0){
						//Sunday
						$tmp_day_start 	= $tmp_start_day . " " . $labor->sunday_start_time;
						$tmp_day_end 	= $tmp_start_day . " " . $labor->sunday_end_time;
					}else if($tmp_wday == 1){
						//Monday
						$tmp_day_start 	= $tmp_start_day . " " . $labor->monday_start_time;
						$tmp_day_end 	= $tmp_start_day . " " . $labor->monday_end_time;
					}else if($tmp_wday == 2){
						//Tuesday
						$tmp_day_start 	= $tmp_start_day . " " . $labor->tuesday_start_time; //h:s
						$tmp_day_end 	= $tmp_start_day . " " . $labor->tuesday_end_time;
					}else if($tmp_wday == 3){
						//Tuesday
						$tmp_day_start 	= $tmp_start_day . " " . $labor->wednesday_start_time; //h:s
						$tmp_day_end 	= $tmp_start_day . " " . $labor->wednesday_end_time;
					}else if($tmp_wday == 4){
						//Tuesday
						$tmp_day_start 	= $tmp_start_day . " " . $labor->thursday_start_time; //h:s
						$tmp_day_end 	= $tmp_start_day . " " . $labor->thursday_end_time;
					}else if($tmp_wday == 5){
						//Tuesday
						$tmp_day_start 	= $tmp_start_day . " " . $labor->friday_start_time; //h:s
						$tmp_day_end 	= $tmp_start_day . " " . $labor->friday_end_time;
					}else if($tmp_wday == 6){
						//Saturday
						$tmp_day_start 	= $tmp_start_day . " " . $labor->saturday_start_time; //h:s
						$tmp_day_end 	= $tmp_start_day . " " . $labor->saturday_end_time;
					}
					
					log_message("debug", "calculate first_start_time == " . $tmp_start_day);
					log_message("debug", "calculate tmp_day_start == " . $tmp_day_start);
					log_message("debug", "calculate tmp_day_end == " . $tmp_day_end);
					log_message("debug", "calculate first_week_end_time == " . date(DATETIME_FORMAT, $first_week_end_time));
					if($tmp_day_start == $tmp_day_end){
						$first_start_time = date_add('d', 1 , $tmp_start_day);
					}else{
						if($i_start == 0){ //start time 
							if(strtotime($tmp_day_end) > $first_week_end_time){// in a day
								log_message("debug", "calculate m-f 1 ... $tmp_day_end > $first_week_end_time ");
								$tmp_hours = 0;
								if($first_start_time > strtotime($tmp_day_start)){
									$tmp_hours = ($first_week_end_time - $first_start_time) / 3600;
								}else{
									$tmp_hours = ($first_week_end_time - strtotime($tmp_day_start)) / 3600;
								}
								if($tmp_hours > FULLTIME_HOURS_M_F){//TODO check this
									$tmp_hours = FULLTIME_HOURS_M_F;
								}
								$hour_count += $tmp_hours; 
								$b_calculate = false;
							}else if(strtotime($tmp_day_end) == $first_week_end_time){// in a day
								log_message("debug", "calculate m-f 2 ... $tmp_day_end > $first_week_end_time ");
								$tmp_hours = 0;
								if($first_start_time > strtotime($tmp_day_start)){
									$tmp_hours = ($first_week_end_time - $first_start_time) / 3600;
								}else{
									$tmp_hours = FULLTIME_HOURS_M_F;
								}
								if($tmp_hours > FULLTIME_HOURS_M_F){//TODO check this
									$tmp_hours = FULLTIME_HOURS_M_F;
								}
								$hour_count += $tmp_hours;
								$b_calculate = false;
							}else{
								log_message("debug", "calculate m-f 3 ... ".strtotime($tmp_day_end)." < $first_week_end_time ");
								log_message("debug", "calculate m-f 3 ... ".$tmp_day_end." < " . date(DATETIME_FORMAT,$first_week_end_time));
								
								$tmp_hours= (strtotime($tmp_day_end) - $first_start_time) / 3600;
								
								log_message("debug", "calculate m-f 4 tmp_hours... ".$tmp_hours);
								
								if($tmp_hours > FULLTIME_HOURS_M_F){//TODO check this
									$tmp_hours = FULLTIME_HOURS_M_F;
								}
								$hour_count += $tmp_hours; 
								$next_date = date_add('d', 1 , $tmp_start_day);
								$next_date_time = $next_date;
								
								if($next_date_time >= $first_week_end_time){
									$b_calculate = false;
								}
								$first_start_time = $next_date_time;
							}
							log_message("debug", "par time calculate_working_hours hour_count 5...." . $hour_count);
						}else{
							log_message("debug", "calculate m-f b1 ... $tmp_day_end > $first_week_end_time ");
							if(strtotime($tmp_day_end) > $first_week_end_time){// in a day
								log_message("debug", "calculate m-f b2 ... $tmp_day_end > $first_week_end_time ");
								$tmp_hours = ($first_week_end_time - strtotime($tmp_day_start)) / 3600; //TODO calculate lunch?
								if($tmp_hours > FULLTIME_HOURS_M_F){//TODO check this
									$tmp_hours = FULLTIME_HOURS_M_F;
								}
								$hour_count += $tmp_hours; 
								$b_calculate = false;
							}else if(strtotime($tmp_day_end) == $first_week_end_time){// in a day
								log_message("debug", "calculate m-f b3 ... $tmp_day_end > $first_week_end_time ");
								$hour_count += FULLTIME_HOURS_M_F;
								$b_calculate = false;
							}else{
								log_message("debug", "calculate m-f b4 ... $tmp_day_end > $first_week_end_time ");
								$next_date = date_add('d', 1 , $tmp_start_day);
								log_message("debug", "calculate next_date == " . $next_date);
								$next_date_time = $next_date;
								
								$hour_count += FULLTIME_HOURS_M_F;
								log_message("debug", "2.. calculate hour_count == " . $hour_count);
								if($next_date_time >= $first_week_end_time){
									$b_calculate = false;
								}
								log_message("debug", "calculate next_date_time == " . date(DATETIME_FORMAT,$next_date_time));
								$first_start_time = $next_date_time;
							}
						}
					}
				}
				
				log_message("debug", "3...calculate first_start_time == " . date(DATETIME_FORMAT,$first_start_time));			 
				$i_start++;
				if($i_start > 1000){
					log_message("debug", "part time exception ...... " );//TODO remove this
					$b_calculate = false;
				}
			}
			
			//only deduct break once for PARTIAL day 
			if($include_break == "true"){
				$break_count = 0;
				$tmp_a_start = getdate($start_time);
				$tmp_wday = $tmp_a_start['wday'];
				if($tmp_wday >= 1 && $tmp_wday <= 5){
					$break_count = 0.75;
				}else if($tmp_wday == 6){
					$break_count = 0.25;
				}
				if($hour_count > $break_count){
					$hour_count = $hour_count - $break_count;
				}
			}
		}elseif($labor->employment_type_id == EMPLOYMENT_TYPE_CASUAL){
			$b_calculate = false;
			$week_count = floor(($end_time - $start_time ) / ( 7 * 86400));
			if($week_count > 0){
				$hour_count += $week_count * (FULLTIME_HOURS_M_F * 5 + FULLTIME_HOURS_SATURDAY);
			}
			
			$first_week_end_time = $end_time - $week_count * 7 * 86400;
			
			$first_start_time = $start_time;
			//while($first_start_time < $first_week_end_time){
			$b_calculate = true;
			$i_start = 0; //indicate first time
			while($b_calculate){
				$tmp_a_start = getdate($first_start_time);
				$tmp_wday = $tmp_a_start['wday'];
				if($tmp_wday >= 0 && $tmp_wday <= 6){
					$tmp_start_day 	= date(DATE_FORMAT, $first_start_time);//d-m-y
					$tmp_day_start 	= ""; //h:s
					$tmp_day_end 	= "";
					if($tmp_wday == 0){
						//Sunday
						$tmp_day_start 	= $tmp_start_day . " " . $labor->sunday_start_time;
						$tmp_day_end 	= $tmp_start_day . " " . $labor->sunday_end_time;
					}else if($tmp_wday == 1){
						//Monday
						$tmp_day_start 	= $tmp_start_day . " " . $labor->monday_start_time;
						$tmp_day_end 	= $tmp_start_day . " " . $labor->monday_end_time;
					}else if($tmp_wday == 2){
						//Tuesday
						$tmp_day_start 	= $tmp_start_day . " " . $labor->tuesday_start_time; //h:s
						$tmp_day_end 	= $tmp_start_day . " " . $labor->tuesday_end_time;
					}else if($tmp_wday == 3){
						//Tuesday
						$tmp_day_start 	= $tmp_start_day . " " . $labor->wednesday_start_time; //h:s
						$tmp_day_end 	= $tmp_start_day . " " . $labor->wednesday_end_time;
					}else if($tmp_wday == 4){
						//Tuesday
						$tmp_day_start 	= $tmp_start_day . " " . $labor->thursday_start_time; //h:s
						$tmp_day_end 	= $tmp_start_day . " " . $labor->thursday_end_time;
					}else if($tmp_wday == 5){
						//Tuesday
						$tmp_day_start 	= $tmp_start_day . " " . $labor->friday_start_time; //h:s
						$tmp_day_end 	= $tmp_start_day . " " . $labor->friday_end_time;
					}else if($tmp_wday == 6){
						//Saturday
						$tmp_day_start 	= $tmp_start_day . " " . $labor->saturday_start_time; //h:s
						$tmp_day_end 	= $tmp_start_day . " " . $labor->saturday_end_time;
					}
					
					log_message("debug", "calculate first_start_time == " . $tmp_start_day);
					log_message("debug", "calculate tmp_day_start == " . $tmp_day_start);
					log_message("debug", "calculate tmp_day_end == " . $tmp_day_end);
					log_message("debug", "calculate first_week_end_time == " . date(DATETIME_FORMAT, $first_week_end_time));
					if($tmp_day_start == $tmp_day_end){
						$first_start_time = date_add('d', 1 , $tmp_start_day);
					}else{
						if($i_start == 0){ //start time 
							if(strtotime($tmp_day_end) > $first_week_end_time){// in a day
								log_message("debug", "calculate m-f 1 ... $tmp_day_end > $first_week_end_time ");
								$tmp_hours = 0;
								if($first_start_time > strtotime($tmp_day_start)){
									$tmp_hours = ($first_week_end_time - $first_start_time) / 3600;
								}else{
									$tmp_hours = ($first_week_end_time - strtotime($tmp_day_start)) / 3600;
								}
								if($tmp_hours > FULLTIME_HOURS_M_F){//TODO check this
									$tmp_hours = FULLTIME_HOURS_M_F;
								}
								$hour_count += $tmp_hours; 
								$b_calculate = false;
							}else if(strtotime($tmp_day_end) == $first_week_end_time){// in a day
								log_message("debug", "calculate m-f 2 ... $tmp_day_end > $first_week_end_time ");
								$tmp_hours = 0;
								if($first_start_time > strtotime($tmp_day_start)){
									$tmp_hours = ($first_week_end_time - $first_start_time) / 3600;
								}else{
									$tmp_hours = FULLTIME_HOURS_M_F;
								}
								if($tmp_hours > FULLTIME_HOURS_M_F){//TODO check this
									$tmp_hours = FULLTIME_HOURS_M_F;
								}
								$hour_count += $tmp_hours;
								$b_calculate = false;
							}else{
								log_message("debug", "calculate m-f 3 ... ".strtotime($tmp_day_end)." < $first_week_end_time ");
								log_message("debug", "calculate m-f 3 ... ".$tmp_day_end." < " . date(DATETIME_FORMAT,$first_week_end_time));
								$tmp_hours= (strtotime($tmp_day_end) - $first_start_time) / 3600;
								if($tmp_hours > FULLTIME_HOURS_M_F){//TODO check this
									$tmp_hours = FULLTIME_HOURS_M_F;
								}
								$hour_count += $tmp_hours; 
								$next_date = date_add('d', 1 , $tmp_start_day);
								$next_date_time = $next_date;
								
								if($next_date_time >= $first_week_end_time){
									$b_calculate = false;
								}
								$first_start_time = $next_date_time;
							}
							log_message("debug", "calculate_working_hours hour_count 5...." . $hour_count);
						}else{
							log_message("debug", "calculate m-f b1 ... $tmp_day_end > $first_week_end_time ");
							if(strtotime($tmp_day_end) > $first_week_end_time){// in a day
								log_message("debug", "calculate m-f b2 ... $tmp_day_end > $first_week_end_time ");
								$tmp_hours = ($first_week_end_time - strtotime($tmp_day_start)) / 3600; //TODO calculate lunch?
								if($tmp_hours > FULLTIME_HOURS_M_F){//TODO check this
									$tmp_hours = FULLTIME_HOURS_M_F;
								}
								$hour_count += $tmp_hours; 
								$b_calculate = false;
							}else if(strtotime($tmp_day_end) == $first_week_end_time){// in a day
								log_message("debug", "calculate m-f b3 ... $tmp_day_end > $first_week_end_time ");
								$hour_count += FULLTIME_HOURS_M_F;
								$b_calculate = false;
							}else{
								log_message("debug", "calculate m-f b4 ... $tmp_day_end > $first_week_end_time ");
								$next_date = date_add('d', 1 , $tmp_start_day);
								log_message("debug", "calculate next_date == " . $next_date);
								$next_date_time = $next_date;
								
								$hour_count += FULLTIME_HOURS_M_F;
								log_message("debug", "2.. calculate hour_count == " . $hour_count);
								if($next_date_time >= $first_week_end_time){
									$b_calculate = false;
								}
								log_message("debug", "calculate next_date_time == " . date(DATETIME_FORMAT,$next_date_time));
								$first_start_time = $next_date_time;
							}
						}
					}
				}
				$i_start++;
				if($i_start > 1000){
					log_message("debug", "casual/trial exception ...... " );//TODO remove this
					$b_calculate = false;
				}
			}
			
			//only deduct break once for PARTIAL day 
			if($include_break == "true"){
				$break_count = 0.5; //TODO confirm again , refer to MSN chat log
				if($hour_count > $break_count){
					$hour_count = $hour_count - $break_count;
				}
			}
		}

		return $hour_count;
	} 
}

/**
 * calculate_late_minutes
 */
if ( ! function_exists('calculate_late_minutes'))
{
	function calculate_late_minutes($labor, $arriva_date) 
	{   
	    //
	    if(empty($labor)){
	    	return 0;
	    }
	    
	    if(empty($arriva_date)){
	    	return 0;
	    }
	    	    
	    $a_arriva = getdate($arriva_date);
	    
	    $minutes_count = 0;
	    $tmp_wday = $a_arriva['wday'];
	    $tmp_start_day = date(DATE_FORMAT, $arriva_date);//d-m-y
		if($labor->employment_type_id == EMPLOYMENT_TYPE_FULLTIME){
			if($tmp_wday == 0){
				$minutes_count = 0;
			}else if($tmp_wday >= 1 && $tmp_wday <= 5){
				$tmp_day_start = $tmp_start_day . " " . $labor->m_start_time;
				$minutes_count = floor(round(($arriva_date - strtotime($tmp_day_start)) / 60));
			}else if($tmp_wday == 6){
				$tmp_start_day = date(DATE_FORMAT, $arriva_date);//d-m-y
				$tmp_day_start = $tmp_start_day . " " . $labor->s_start_time;
				$minutes_count = floor(round(($arriva_date - strtotime($tmp_day_start)) / 60));
			}
		}else if($labor->employment_type_id == EMPLOYMENT_TYPE_PARTTIME 
			|| $labor->employment_type_id == EMPLOYMENT_TYPE_CASUAL){
				
			if($tmp_wday == 0){
				//Sunday
				$tmp_day_start 	= $tmp_start_day . " " . $labor->sunday_start_time;
			}else if($tmp_wday == 1){
				//Monday
				$tmp_day_start 	= $tmp_start_day . " " . $labor->monday_start_time;
			}else if($tmp_wday == 2){
				//Tuesday
				$tmp_day_start 	= $tmp_start_day . " " . $labor->tuesday_start_time; //h:s
			}else if($tmp_wday == 3){
				//Tuesday
				$tmp_day_start 	= $tmp_start_day . " " . $labor->wednesday_start_time; //h:s
			}else if($tmp_wday == 4){
				//Tuesday
				$tmp_day_start 	= $tmp_start_day . " " . $labor->thursday_start_time; //h:s
			}else if($tmp_wday == 5){
				//Tuesday
				$tmp_day_start 	= $tmp_start_day . " " . $labor->friday_start_time; //h:s
			}else if($tmp_wday == 6){
				//Saturday
				$tmp_day_start 	= $tmp_start_day . " " . $labor->saturday_start_time; //h:s
			}
			$minutes_count = floor(round(($arriva_date - strtotime($tmp_day_start)) / 60)); 
		}

		return $minutes_count;
	} 
}

/**
 * caculate age: format Age 29 and 9 months
 */
if ( ! function_exists('calculate_age'))
{
	function calculate_age($birthday) 
	{   
		if(empty($birthday)){
			return "";
		}
	    $birth = getdate($birthday);
	    $year = date('Y') - $birth['year'];
	    $month = date('n') - $birth['mon'];
	    if($month < 0){
	    	$year--;
	    	$month = 12 + $month;
	    }
	    if($year <= 0){
	    	return "";
	    }
	    return $year . " years, " . $month . " months";
	} 
}

if ( ! function_exists('pagination_option'))
{
	function pagination_option() {
		$option = array(
			"" => "All", 	
			"10" => "10",
			"20" => "20",
			"50" => "50"
		);
		return $option;
	}
}

if ( ! function_exists('get_number'))
{
	function get_number($num) {
		if(empty($num)){
			return 0;
		}
		return $num;
	}
}
/* End of file common_helper.php */
/* Location: ./system/applicatioin/helpers/MY_common_helper.php */