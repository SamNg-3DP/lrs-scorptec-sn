<?php

/**
 * user auth class
 * @author Bai Yinbing
 */
class Userauth{
	//roles vars
	var $priv_staff 		= array('userinfo', 'request', 'jobincident');
	var $priv_supervisor 	= array('userinfo', 'request', 'approve', 'jobincident');
	var $priv_manager 		= array('userinfo', 'request', 'approve', 'jobincident');
	var $priv_admin 		= array('userinfo', 'request', 'approve', 'department', 'userlevel', 'site', 'user'
								   ,'shiftcategory', 'dailyshift', 'payrollshift', 'leavetype', 'config'
								   ,'history', 'payroll', 'jobincident');
	
	function Userauth(){
		$this->object =& get_instance();
		$this->object->load->database();
		log_message('debug','User Authentication Class Initialised via '.get_class($this->object));
	}
	
	/**
	 * Logout user and reset session data
	 */
	function logout(){
		$sessdata = array('user_name'=>NULL, 'loggedin'=>FALSE );
		$this->object->session->unset_userdata($sessdata);
	}
	
	/**
	 * check whether current user is logined 
	 * @return unknown_type
	 */
	function logined(){
		if($this->object->session->userdata('user_name') != NULL && $this->object->session->userdata('loggedin')){
			return true;
		}
		$this->object->session->set_flashdata('back_url', current_url());
		return false;
	}
	
	/**
	 * Try and validate a login and optionally set session data
	 *
	 * @param		string		$username		Username to login
	 * @param		string		$password		Password to match user
	 * @param		bool		$session (true)	Set session data here. False to set your own
	 */
	function trylogin($user_name, $password){
		if( $user_name != '' && $password != ''){
			// Check details in DB
			$this->object->db->select(
				 'users.*'			
			);
			
			$this->object->db->from("st_users users");
			$this->object->db->where('users.user_name', $user_name);
			$this->object->db->where('users.password', $password);
			$this->object->db->where('users.status', 1);
			$this->object->db->limit(1);
			$query = $this->object->db->get();
			
			log_message('debug', 'Trylogin query: '.$this->object->db->last_query() );

			// If user/pass is OK then should return 1 row containing username,fullname
			$return = $query->num_rows();
			
			// Log message
			log_message('debug', "Userauth: Query result: '$return'");
			
			if($return == 1){
				// 1 row returned with matching user & pass = validated!
				
				// Get row from query (fullname, email)
				$row = $query->row();
				
				// Update the DB with the last login time (now)..
				$timestamp = now();
				$sql =	"UPDATE st_users ".
						" SET last_login_time='".$timestamp."' ".
						" WHERE user_name='".$row->user_name."'";
				$this->object->db->query($sql);
				
				// Log
				log_message('debug',"Last login by $user_name SQL: $sql");
				//TODO record user login history
				
				$sessdata['user_name'] 	= $user_name;
				$sessdata['user_id'] 	= $row->id;
				$sessdata['first_name'] = $row->first_name;
				$sessdata['last_name'] 	= $row->last_name;
				$sessdata['last_login_time'] = $timestamp;
				$sessdata['user_level'] = $row->user_level_id;
				
				$this->object->db->select(
				 'dept.*'			
				);
				
				$this->object->db->from("st_department dept");
				$this->object->db->where('dept.id', $row->department_id);
				$this->object->db->where('dept.status', 1);
				$this->object->db->limit(1);
				$query = $this->object->db->get();
				log_message('debug', 'st_department query: '.$this->object->db->last_query() );
				$return = $query->num_rows();
			
				$dept = null;
				if($return == 1){
					$dept = $query->row();
				}
				if($row->user_level_id == 3 || $row->user_level_id == 4 ){
					if($dept){
						$sessdata['department'] = $dept->department;
						$sessdata['department_id'] = $dept->id;
					}else{
						$sessdata['department'] = "";
						$sessdata['department_id'] = "";
					}
					
					$sessdata['site_id'] = 0;
				}else{
					if($dept){
						$sessdata['department'] = $dept->department;
						$sessdata['department_id'] = $dept->id;
					}else{
						$sessdata['department'] = "";
						$sessdata['department_id'] = "";
					}
					$sessdata['site_id'] 	= $this->get_site($this->object->input->ip_address());
				}
				$sessdata['email'] = $row->email;
				$sessdata['personal_email'] = $row->personal_email;
				$sessdata['loggedin'] = true;
				
				// Set the session
				$this->object->session->set_userdata($sessdata);
				return true;
			} else {
				// no rows with matching user & pass - ACCESS DENIED!!
				return false;
			}
		} else {
			return false;
		}
	}

	function CheckAuthLevel( $allowed, $level = NULL ){
		if($level == NULL){
			#$level = $this->getAuthLevel( $this->object->session->userdata('schoolcode'), $this->object->session->userdata('username') );
			$query_str = "SELECT authlevel FROM users WHERE user_id='".$this->object->session->userdata('user_id')."' LIMIT 1";
			$query = $this->object->db->query($query_str);
			if($query->num_rows() == 1){
				$row = $query->row();
				$level = $row->authlevel;
			} else {
				return false;
			}
		}
		if( !( $allowed & $level )  ){
			return false ;
		} else {
			return true ;
		}
	}
	
	function GetAuthLevel($user_id = NULL){
		if($user_id == NULL){ $user_id = $this->object->session->userdata('user_id'); }
		$query_str = "SELECT authlevel FROM users WHERE user_id='$user_id' LIMIT 1";
		$query = $this->object->db->query($query_str);
		if($query->num_rows() == 1){
			$row = $query->row();
			$level = $row->authlevel;
			return $level;
		} else {
			return false;
		}
	}
	
	/**
	 * judge where the current user has the privilege for the operator  
	 * @param $action
	 * @return unknown_type
	 */
	function judge_priv($action)
	{
		$user_level = $this->object->session->userdata('user_level');
		if($user_level == ROLE_STAFF) {
			if(in_array($action, $this->priv_staff)){
				return TRUE;
			}
		} else if($user_level == ROLE_SUPERVISOR){
			if(in_array($action, $this->priv_supervisor)){
				return TRUE;
			}
		} else if($user_level == ROLE_MANAGER){
			if(in_array($action, $this->priv_manager)){
				return TRUE;
			}
		} else if($user_level == ROLE_ADMINISTRAOTR){
			if(in_array($action, $this->priv_admin)){
				return TRUE;
			}
		}
		return FALSE;
	}
	
	function get_site($ip){
		$ip = substr($ip, 0,strrpos($ip,".")+1);
		$sql = "SELECT id FROM st_site WHERE ip_range like '%" . $ip ."%'";
		$query = $this->object->db->query($sql);
		if($query->num_rows() == 1){
			$row = $query->row();
			return $row->id;
		} else {
			return -1;
		}
	}
}

?>