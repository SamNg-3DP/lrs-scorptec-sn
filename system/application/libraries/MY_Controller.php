<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// Code here is run before ALL controllers
class MY_Controller extends Controller
{
	var $module;
	var $controller;
	var $method;
	
	function MY_Controller()
	{
		parent::Controller();
		
        // Load the user model and get user data
        $this->load->model('pt_users_m');
        $this->load->library('users/ion_auth');
        
        $this->data->user = $this->ion_auth->get_user();
        
        // Work out module, controller and method and make them accessable throught the CI instance
//        $this->module 				= $this->router->fetch_module();
//        $this->controller			= $this->router->fetch_class();
//        $this->method 				= $this->router->fetch_method();
//        
//        $this->data->module 		=& $this->module;
//        $this->data->controller 	=& $this->controller;
//        $this->data->method 		=& $this->method;
		
        
	}
}