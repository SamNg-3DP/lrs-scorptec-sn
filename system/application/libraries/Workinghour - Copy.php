<?php

/**
 * Working hour class
 * @author Bai Yinbing
 */
class Workinghour{

	function Workinghour(){
		$this->object =& get_instance();
		$this->object->load->database();
	}
	
	/**
	 * calculate working hours used for a requst
	 *
	 * @param		int			$user_id		user id
	 * @param		int			$start_time		start time
	 * @param		int 		$end_time		end time
	 * @param		bool		$session (true)	Set session data here. False to set your own
	 */

	function calculate_hours_used( $user_id, $start_time, $end_time, $request_type, $include_break="false", $leave_type="false" ){
		$hour_used = 0;
		if (!$end_time) return false;
		
		// log_message("debug", "start_time === " . $start_time);
		// log_message("debug", "end_time === " . $end_time);
				
		$sql = "SELECT * FROM st_user_labor AS labor" .
				//" WHERE labor.entry_date < " . now() . 
				" WHERE labor.entry_date < " . $end_time .
				" AND user_id = " . $user_id .
				" AND labor.status = " . ACTIVE  .
				" ORDER BY entry_date DESC " .
				" LIMIT 1"
				;
		// log_message("debug", "labor sql == " . $sql);
		$query = $this->object->db->query($sql);
		$labor = $query->row();
		if($labor){
			//read payroll
			$sql = "SELECT * FROM st_payrollshift AS pay " .
					" WHERE pay.id = " . $labor->payrollshift_id;
			// log_message("debug", "pay sql == " . $sql);
			$query = $this->object->db->query($sql);
			$payroll = $query->row();
			if($payroll){
				// log_message("debug", "pay start_date == " . $payroll->start_date);
				$payroll_date = $payroll->start_date;
				$rolldays = $payroll->rolldays;
				
				if($payroll_date >= $end_time){
					// log_message("debug", "payroll shift start date is invalid");
					return 0;
				}else if($payroll_date >= $start_time && $payroll_date < $end_time ){
					$start_time = $payroll_date;
				}
				
				// log_message("debug", "pay request_type == " . $request_type);
				//request type is FULL day
				if($request_type == 1){
					//unit is day
					$period = intval(floor(($end_time - $start_time) / 86400));

					if($period > $rolldays){
						$payroll_hours = $this->calculate_payroll_hours($payroll->id);
						$times = floor($period / $rolldays);
						$hour_used = $times * $payroll_hours;
						$start_time += $times * $rolldays * 86400;
					}
					// log_message("debug", "slot start time === " . $start_time);
					// log_message("debug", "slot end time === " . $end_time);
					
					while($start_time <= $end_time){
						$start_day_no = intval(floor(($start_time - $payroll_date) / 86400));
						$remainder = $start_day_no % $rolldays;
						$start = $remainder * 24;
						
						// Temp solution to calculate full day overtime. Use Clayton's Mon-Sat timetable.
						if ($leave_type == 8){
							$payroll_id = 2;
							
							$sql = "SELECT slot.*, roll.name AS wday " .
									" FROM st_payrollshift_rolls AS roll, st_shift_category AS cat, st_daily_shift_slot AS slot " .
									" WHERE roll.payrollshift_id = " . $payroll_id .
									" AND roll.start = " . $start . 
									" AND roll.daily_shift_id = slot.daily_shift_id " .
									" AND (cat.total = 2 OR cat.total = 3) AND slot.shift_category_code = cat.code ";
							
						} else {
							$sql = "SELECT slot.*, roll.name AS wday " .
									" FROM st_payrollshift_rolls AS roll, st_shift_category AS cat, st_daily_shift_slot AS slot " .
									" WHERE roll.payrollshift_id = " . $payroll->id .
									" AND roll.start = " . $start . 
									" AND roll.daily_shift_id = slot.daily_shift_id " .
									" AND cat.total = 2 AND slot.shift_category_code = cat.code ";
						}
						
						$rquery = $this->object->db->query($sql);
						$slots = $rquery->result();
						$wday = -1;
						log_message("debug", "1 slots sql === ".$sql);
						foreach($slots AS $slot){
							$wday = $slot->wday;
							if($wday == 0){
								break;
							}
							$tmp_start_time = strtotime($slot->start_time);
							$tmp_end_time = strtotime($slot->end_time);
							$slot_hours = round(($tmp_end_time - $tmp_start_time)/3600,2);
							$hour_used += $slot_hours;
						}
						
						if ($slots){
							if($wday > 0 && $wday < 6){
								$hour_used -= 1;
							}else if($wday == 6){
//								$hour_used -= 0.25;
							}
						}
						$start_time += 86400;
						
						
					}
					// log_message("debug", "end hour_used === " . $hour_used);
				}else if($request_type == 2){//PARTIAL day 
					$start_day_no = intval(floor(($start_time - $payroll_date) / 86400));
					$remainder = $start_day_no % $rolldays;
					$start = $remainder * 24;
					//
					$sql = "SELECT slot.*, roll.name AS wday " .
							" FROM st_payrollshift_rolls AS roll, st_shift_category AS cat, st_daily_shift_slot AS slot " .
							" WHERE roll.payrollshift_id = " . $payroll->id .
							" AND roll.start = " . $start . 
							" AND roll.daily_shift_id = slot.daily_shift_id " .
							" AND cat.total = 2 AND slot.shift_category_code = cat.code " .
							" ORDER BY start_time ASC";
					// log_message("debug", "2 slots sql === ".$sql);
					$rquery = $this->object->db->query($sql);
					$slots = $rquery->result();
					$wday = -1;
					$start_date = date(DATE_FORMAT, $start_time);
					foreach($slots AS $slot){
						$wday = $slot->wday;
						if($wday == 0){
							break;
						}
						$slot_hours = 0;
						$tmp_start_time = strtotime($start_date . " " . $slot->start_time);
						$tmp_end_time = strtotime($start_date . " " . $slot->end_time);
						// log_message("debug", "slots tmp_start_time === ".$tmp_start_time . "  " . $start_date . " " . $slot->start_time);
						// log_message("debug", "slots start_time === ".$start_time . "  " . date(DATETIME_FORMAT, $start_time));
						if($tmp_start_time >= $start_time){
							// log_message("debug", "slots sql tmp_start_time >= start_time ");
							if($tmp_end_time < $end_time){
								$slot_hours = round(($tmp_end_time - $tmp_start_time)/3600,2);
								$hour_used += $slot_hours;
							}else{
								$slot_hours = round(($end_time - $tmp_start_time)/3600,2);
								$hour_used += $slot_hours;
								break;
							}
						}else{
							if($tmp_end_time < $end_time){
								$slot_hours = round(($tmp_end_time - $start_time)/3600,2);
								$hour_used += $slot_hours;
							}else{
								$slot_hours = round(($end_time - $start_time)/3600,2);
								$hour_used += $slot_hours;
								break;
							}
						}
					}
					
					if($include_break == "true" && $slots){
						if($wday > 0 && $wday < 6){
							$hour_used -= 1;
						}else if($wday == 6){
//							$hour_used -= 0.25;
						}
					}
				}
			}
		}
		
		
		if($hour_used < 0){
			$hour_used = 0;
		}
		return $hour_used;
	}
	
	/**
	 * calculate the payroll working hours
	 */
	function calculate_payroll_hours($payrollshift_id){
		$total_hours = 0;
		$sql = "SELECT roll.* " .
				" FROM st_payrollshift_rolls AS roll " .
				" WHERE roll.payrollshift_id = " . $payrollshift_id;
		$query = $this->object->db->query($sql);
		$rolls = $query->result();
		foreach($rolls AS $roll){
			$wday = $roll->name;
			if($wday == 0){
				continue;
			}
			$rsql = "SELECT slot.* " .
					" FROM st_daily_shift AS daily, st_shift_category AS cat, st_daily_shift_slot AS slot " .
					" WHERE cat.total = 2 AND slot.shift_category_code = cat.code AND daily.id = slot.daily_shift_id" .
					" AND daily.id = " . $roll->daily_shift_id;
			$rquery = $this->object->db->query($rsql);
			$slots = $rquery->result();
			foreach($slots AS $slot){
				$start_time = strtotime($slot->start_time);
				$end_time = strtotime($slot->end_time);
				$slot_hours = round(($end_time - $start_time)/3600,2);
				$total_hours += $slot_hours;
			}
			if ($slots){
				if($wday > 0 && $wday < 6){
					$total_hours -= 1;
				}else if($wday == 6){
//					$total_hours -= 0.25;
				}
			}
		}
		return $total_hours;
	}
	
	/**
	 * calculate how many hours the leave type used
	 */
	function calculate_leave_used($user_id, $leave_type, $start_time, $end_time){
		$hour_used = 0;
/*		if($start_time == $end_time){
			return 0;
		}*/
		$sql = " SELECT * FROM st_users WHERE id = " . $user_id;
		$query = $this->object->db->query($sql);
		$user = $query->row();
		
		$leave_where = "";
		
		if ($leave_type == LEAVE_TYPE_ANNUAL){
			$leave_type = array(LEAVE_TYPE_ANNUAL);
			$leave_where .= " AND request.paid=1";
		}
		elseif ($leave_type == LEAVE_TYPE_UNPAID){
			$leave_type = array(LEAVE_TYPE_UNPAID, LEAVE_TYPE_LATE, LEAVE_TYPE_LUNCH);
			$leave_where .= " AND request.paid=2";
		}		
		
		
		if(is_array($leave_type)){
    		$array_length = count($leave_type);
    		if($array_length > 0){
    			$str = "(";
    			for ($i = 0; $i < $array_length; $i++){
			    	if(($i + 1) < $array_length){
			    		$str .= $leave_type[$i] . ",";
			    	}else{
			    		$str .= $leave_type[$i];
			    	}
				}
				$str .= ")";
				$leave_where .= " AND request.leave_type_id IN " . $str;
    		}
    	}else{
    		$leave_where .= " AND request.leave_type_id = " . $leave_type;
    	}
		
		//all
		$sql =  " SELECT SUM(request.hours_used) AS used_count " .
				" FROM st_request AS request " .
				" WHERE request.status = " . ACTIVE .
					$leave_where . 
					" AND request.approve = 1".
					" AND request.user_name = '" . $user->user_name . "'" .					  
					" AND ( 
				   ( request.start_date <= " . $start_time . " AND request.end_date >= " . $start_time . " ) 
					OR (request.start_date >= " . $start_time . " AND request.end_date < " . $end_time . " )
					OR (request.start_date < " . $end_time .   " AND request.end_date >=" . $end_time . " )
				)";

		log_message("debug", "request sql == " . $sql);
		$query = $this->object->db->query($sql);
		$hour_used = $query->row()->used_count;
		
		if($hour_used && $hour_used > 0){
			$sql =  " SELECT request.* " .
					" FROM st_request AS request " .
					" WHERE request.status = " . ACTIVE .
						$leave_where .
						" AND request.approve = 1". 
						" AND request.user_name = '" . $user->user_name . "'" .	
						" AND request.start_date <= " . $start_time . 
						" AND request.end_date >= " . $start_time;
						
			log_message("debug", "before leave sql == " . $sql);
			$query = $this->object->db->query($sql);
			if ($query->num_rows() > 0){
				$row = $query->row();
				$start_time -= 86400;
				$before_start_used = $this->calculate_hours_used( $user_id, $row->start_date, $start_time, 1, "true" );
				$hour_used -= $before_start_used;
				log_message("debug", "before_start_used == " . $before_start_used);
			}
					
			$sql =  " SELECT request.* " .
					" FROM st_request AS request " .
					" WHERE request.status = " . ACTIVE .
						$leave_where . 
						" AND request.approve = 1".
						" AND request.user_name = '" . $user->user_name . "'" .	
						" AND request.start_date < " . $end_time.
						" AND request.end_date >= " . $end_time;
						
			log_message("debug", "after leave sql == " . $sql);						
			$query = $this->object->db->query($sql);
			if ($query->num_rows() > 0){
				$row = $query->row();
				$after_end_used = $this->calculate_hours_used( $user_id, $end_time, $row->end_date, 1, "true" );
				$hour_used -= $after_end_used;
				log_message("debug", "after_end_end time == " . date(DATETIME_FORMAT,$end_time));
				log_message("debug", "after_end_end date == " . date(DATETIME_FORMAT,$row->end_date));
				log_message("debug", "after_end_used == " . $after_end_used);
			}
		}else{
			$hour_used = 0;
		}
		return $hour_used;
	}
	
	/**
	 * calculate how many increase work hours which all leave type used
	 */
	function calculate_increase_work_hours_by_leave($user_id, $start_time, $end_time){
		return $this->calculate_hours_by_outcome(4, $user_id, $start_time, $end_time);
	}
	
	/**
	 * calculate how many deduct work hours which all leave type used
	 */
	function calculate_deduct_work_hours_by_leave($user_id, $start_time, $end_time){
		return $this->calculate_hours_by_outcome(5, $user_id, $start_time, $end_time);
	}
	
	/**
	 * calcuate how many work hours which leave type's outcome is equal to outcome id
	 */
	function calculate_hours_by_outcome($outcome_id, $user_id, $start_time, $end_time){
		$sql = " SELECT * FROM st_leave_type WHERE working_hours_outcome_id = " . $outcome_id;
		$query = $this->object->db->query($sql);
		$leave_types = $query->result();
		
		$a_leave_type = array();		
		if($leave_types){
			foreach($leave_types AS $leave_type){
				array_push($a_leave_type, $leave_type->id);
			}
		}
		
		if(count($a_leave_type) > 0){
			return $this->calculate_leave_used($user_id, $a_leave_type, $start_time, $end_time);
		}else{
			return 0;
		}
	}
	
	
	function calculate_overtime_hours($user_id, $start_time, $end_time){
		$leave_hours = 0;
		
		$return_hours = array("OT0" => '', "OT1" => '', "OT2" => '', "leave_hours" => '');
		
		$sql = " SELECT * FROM st_users WHERE id = " . $user_id;
		$query = $this->object->db->query($sql);
		$user = $query->row();		
		
		$sql = "SELECT * FROM st_user_labor AS labor" .
				" WHERE labor.entry_date < " . $end_time .
				" AND user_id = " . $user_id .
				" AND labor.status = " . ACTIVE  .
				" ORDER BY entry_date DESC " .
				" LIMIT 1";

		$query = $this->object->db->query($sql);
		$labor = $query->row();
		
		if ($labor){
			$sql = "SELECT roll.* " .
					" FROM st_payrollshift_rolls AS roll, st_daily_shift AS daily " .
					" WHERE roll.daily_shift_id=daily.id AND (daily.shiftcode like 'OT%' OR  daily.shiftcode like 'SINOT%') 
					AND roll.payrollshift_id = " . $labor->payrollshift_id;
			
			$query = $this->object->db->query($sql);
			$rolls = $query->result();
			
			foreach($rolls AS $roll){
				
				// Find day set as OT.			
				$ot_start_time = $start_time + ($roll->start * 3600);
				$ot_end_time = $ot_start_time + 86399;
				$ot_date = date(DATE_FORMAT, $ot_start_time);
				
				$wday = $roll->name;
				if($wday == 0){
					continue;
				}
				$rsql = "SELECT slot.*, cat.description as cat_desc " .
						" FROM st_daily_shift AS daily, st_shift_category AS cat, st_daily_shift_slot AS slot  " .
						" WHERE cat.total = 3 AND slot.shift_category_code = cat.code AND daily.id = slot.daily_shift_id" .
						" AND daily.id = " . $roll->daily_shift_id .
						" AND cat.description like 'OT%'";
				
				$rquery = $this->object->db->query($rsql);
				$slots = $rquery->result();
	
				$slot_start_time_day = 0;
				$slot_end_time_day = 0;
				foreach($slots AS $slot){
					$slot_start_time = strtotime($ot_date . " " . $slot->start_time);
					$slot_end_time = strtotime($ot_date . " " . $slot->end_time);
					$slot_hours = round(($slot_end_time - $slot_start_time)/3600,2);
					$slot_hours = round(($slot_hours * $slot->rate),2);
					
					$return_hours[$slot->cat_desc] += $slot_hours;
					
					
					if ($slot_start_time_day > $slot_start_time || $slot_start_time_day == 0)
						$slot_start_time_day	= $slot_start_time;
					if ($slot_end_time_day < $slot_end_time || $slot_end_time_day == 0)
						$slot_end_time_day	= $slot_end_time;
	
				}
				
				//Get requested leave.
				$sql =  " SELECT request.* " .
						" FROM st_request AS request " .
						" WHERE request.status = " . ACTIVE .
						" AND request.approve = 1". 
						" AND request.user_name = '" . $user->user_name . "'" .	
						" AND request.leave_type_id NOT IN (11,10,8,6,5)".
						" AND IF(request.`request_type`=1, 
							(request.start_date <= {$ot_start_time}  AND request.end_date >= {$ot_start_time}), 
							(request.start_date >= {$ot_start_time}  AND request.end_date <= {$ot_end_time}))";
				
							
				$query = $this->object->db->query($sql);
				$leave = $query->row();
				
				
				if ($leave){
					$ot_leave_start_time = $ot_start_time;
					$ot_leave_end_time = $ot_start_time;
					
					
					if ($leave->request_type == 2){
						$ot_leave_start_time = ($leave->start_date > $slot_start_time_day) ? $leave->start_date : $slot_start_time_day;
						$ot_leave_end_time = ($leave->end_date < $slot_end_time_day) ? $leave->end_date : $slot_end_time_day ;
					}else{
							$ot_leave_start_time = $slot_start_time_day;
							$ot_leave_end_time = $slot_end_time_day;
					}
					
					
					$leave_hours = round(($ot_leave_end_time - $ot_leave_start_time)/3600,2);
					
					if (!empty($return_hours['OT1']) && !empty($return_hours['OT2'])){
						if ($leave_hours > 2){
							$return_hours['OT2'] -= 4;
							$return_hours['OT1'] -= ($leave_hours - 2) * 1.5;
						}else{
							$return_hours['OT2'] -= $leave_hours * 2;
						}
					}elseif (!empty($return_hours['OT0'])){
						$return_hours['OT0'] -= $leave_hours;
					}
					
					$return_hours['leave_hours'] = $leave_hours;
					
					
					log_message("debug", "leave end time == " . date(DATETIME_FORMAT,$leave->end_date));
					
					log_message("debug", "OT leave start time == " . date(DATETIME_FORMAT, $ot_leave_start_time));
					log_message("debug", "OT leave end time == " . date(DATETIME_FORMAT, $ot_leave_end_time));			
					
					log_message("debug", "OT leave hours == " . $leave_hours);				
					
				}
				log_message("debug", "slot start time == " . date(DATETIME_FORMAT,$slot_start_time_day));
				log_message("debug", "slot end time == " . date(DATETIME_FORMAT,$slot_end_time_day));			
			}
			
			// Find additional overtime done through request.
			$sql =  " SELECT request.* " .
				" FROM st_request AS request " .
				" WHERE request.status = " . ACTIVE .
				" AND request.approve = 1". 
				" AND request.user_name = '" . $user->user_name . "'" .	
				" AND request.leave_type_id = 8".
				" AND request.start_date >= {$start_time}  AND request.end_date <= {$end_time} ";
			
			$query = $this->object->db->query($sql);
			$requests = $query->result();

			if ($requests){
				foreach ($requests as $request){
					if ($request->hours_used > 3){
						$return_hours['OT1'] +=  3 * 1.5;
						$return_hours['OT2'] += ($request->hours_used - 3) * 2; 
					}
					else{
						$return_hours['OT1'] +=  $request->hours_used * 1.5;
					}
				}
			}
		}
		if ($return_hours)
			return $return_hours;
		else 	
			return false;	
		
	}
	
	
	function calculate_total_rest_break($user_id, $start_time, $end_time){
		$day_work_hour = 0;
		$rest_break = 0.0;
		
		while ($start_time < $end_time){
			// Only monday-friday
			
			$day = date('D', $start_time);
			if  ($day != 'Sat' && $day != 'Sun'){
				$day_work_hour 		= $this->calculate_hours_used($user_id,$start_time,$start_time+86399,1);
				
				$day_work_hour 	-= $this->calculate_leave_used($user_id,LEAVE_TYPE_ANNUAL,$start_time,$start_time+86399);
				$day_work_hour 	-= $this->calculate_leave_used($user_id,LEAVE_TYPE_PERSONAL,$start_time,$start_time+86399);
				$day_work_hour 	-= $this->calculate_leave_used($user_id,LEAVE_TYPE_UNPAID,$start_time,$start_time+86399);
				
				if ($day_work_hour < 10 && $day_work_hour >= 7)
					$rest_break += 1;
				elseif ($day_work_hour < 7 && $day_work_hour >= 4)
					$rest_break += 0.5;
			} 
			
			$start_time += 86400;
		}
		
		return $rest_break;
	}
	
	
	
	
}

?>