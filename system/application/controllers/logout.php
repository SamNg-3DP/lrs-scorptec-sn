<?php

/**
 * login controller
 * @author Bai Yinbing
 */
class Logout extends Controller {

	function Logout()
	{
		parent::Controller();
	}
	
	function index()
	{
		if($this->userauth->logined()){
			//
			$this->load->model('St_user_log_m', 'user_log_m');
				
			$a_user_log = array(
				'user_name' => $this->session->userdata('user_name'),
				'log_info'  => 'logout'
			);
			
			$this->user_log_m->add($a_user_log);
			$this->userauth->logout();
			redirect('login','location');
		}else{
			redirect('login','location');
		}
	}
}
