<?php
class Dailyshift extends Controller {
	
	function Dailyshift()
	{
		parent::Controller ();
		if(!$this->userauth->logined()){
			$infoMsg = array(
				'info' => 'You have no privilege into this page !'
			);
			$this->session->set_flashdata('auth', $this->load->view('msgbox/info', $infoMsg , True));
			redirect("login");
		}else{
			if(!$this->userauth->judge_priv("dailyshift")){
				$errorMsg = array(
					'error' => 'You have no privilege into this page !'
				);
				$this->session->set_flashdata('auth', $this->load->view('msgbox/error', $errorMsg , True));
				redirect("msgbox", 'location');
			}
		}
		$this->form_validation->set_error_delimiters ( "<div style='font:Tahoma, Geneva, sans-serif; font-size:11px; color:#F00'>", "</div>" );
		//load daily_shift_m model
		$this->load->model('St_daily_shift_m', 'daily_shift_m');
	}
	
	function index()
	{
		$daily_shifts = $this->daily_shift_m->get_dailyshift();
		$data['dailyshift'] = $daily_shifts;
		$this->load->model('St_daily_shift_slot_m', 'daily_shift_slot_m');
		foreach($daily_shifts as $daily){
			$label = "daily".$daily->id;
			$data[$label] = $this->daily_shift_slot_m->get_by_daily_shift($daily->id);
		}
		
		$data ['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > Shift/Roster > <a href='".site_url('dailyshift')."'>Daily Shift</a> > View";
		$this->load->view ( 'navigation', $data );
		$this->load->view ( 'dailyshift/list_dailyshifts', $data );
	}
	
	function add()
	{
		$this->load->model('St_site_m', 'site_m');
		$data['site'] = $this->site_m->site();
		$data['rates'] = $this->_get_rate_dropdown();
		$this->load->model('St_shift_category_m', 'shift_category_m');
		$data['shiftcategory'] = $this->shift_category_m->get_select_dropdown();
		$data ['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > Shift/Roster > <a href='".site_url('dailyshift')."'>Daily Shift</a> > Add New";
		$this->load->view ( 'navigation', $data );
		$this->load->view ( 'dailyshift/add_dailyshift', $data);
	}
	
	function do_add()
	{
		$this->form_validation->set_rules ( 'shiftcode', 'Shift Code', 'trim|required' );
		$this->form_validation->set_rules ( 'description', 'Description', 'trim|required' );
		if ($this->form_validation->run () == FALSE)
		{
			$this->add ();
		}
		else {
			$shiftcode = $this->input->post ( 'shiftcode' );
			$description = $this->input->post ( 'description' );
			$notes = $this->input->post ( 'notes' );
			$site = $this->input->post ( 'site' );
			$lunch = $this->input->post ( 'lunch' );
			
			$starttime = $this->input->post ( 'starttime' );
			$endtime = $this->input->post ( 'endtime' );
			$shiftcategory = $this->input->post ( 'shiftcategory' );
			$rate = $this->input->post ( 'rate' );
			$daily_shift =array(
				'shiftcode' => $shiftcode,
				'description' => $description,
				'lunch_hour' => $lunch,
				'notes' => $notes,
				'site_id' => $site
			);
			$daily_shift_id = $this->daily_shift_m->insert($daily_shift);
			
			$slot_num = $this->input->post ( 'slot_num' );
			$now = now();
			$i = 0;
			$this->load->model('St_daily_shift_slot_m', 'daily_shift_slot_m');
			while($i <= $slot_num){
				$p_starttime = "starttime".$i;
				$p_endtime = "endtime".$i;
				$p_shiftcategory = "shiftcategory".$i;
				$p_rate = "rate".$i;
				$starttime = $this->input->post ($p_starttime);
				$endtime = $this->input->post ($p_endtime);
				$shiftcategory = $this->input->post ($p_shiftcategory);
				$rate = $this->input->post ($p_rate);
				if(!empty($starttime) && !empty($endtime) && !empty($shiftcategory) && !empty($rate)){
					$slot = array(
						'daily_shift_id' => $daily_shift_id,
						'start_time' => $starttime,
						'end_time' => $endtime,
						'shift_category_code' => $shiftcategory,
						'rate' => $rate,
						'add_time' => $now,
						'status' => ACTIVE
					);
					$this->daily_shift_slot_m->insert($slot);
				}
				$i++;
			}
			
			$this->index ();
		}
	}
	
	function remove($id)
	{
		$this->load->model('St_daily_shift_slot_m', 'daily_shift_slot_m');
		$this->daily_shift_m->delete ($id);
		$this->daily_shift_slot_m->delete_by_daily_shift($id);
		$this->index ();
	}
	
	function edit($id)
	{
		$this->load->model('St_site_m', 'site_m');
		$data['sites'] = $this->site_m->get_select_dropdown();
		$data['rates'] = $this->_get_rate_dropdown();
		$this->load->model('St_shift_category_m', 'shift_category_m');
		$data['shiftcategory'] = $this->shift_category_m->get_select_dropdown();
		$data['dailyshift'] = $this->daily_shift_m->get($id);
		$this->load->model('St_daily_shift_slot_m', 'daily_shift_slot_m');
		$data['slots'] = $this->daily_shift_slot_m->get_by_daily_shift($id);
		$data ['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > Shift/Roster > <a href='".site_url('dailyshift')."'>Daily Shift</a> > Edit";
		$this->load->view ( 'navigation', $data );
		$this->load->view ( 'dailyshift/edit_dailyshift', $data);
	}
	
	function do_edit()
	{
		$this->form_validation->set_rules ( 'shiftcode', 'Shift Code', 'trim|required' );
		$this->form_validation->set_rules ( 'description', 'Description', 'trim|required' );
		$shiftcode = $this->input->post ( 'shiftcode' );
		$description = $this->input->post ( 'description' );
		$notes = $this->input->post ( 'notes' );
		if ($this->form_validation->run () == FALSE)
		{
			$this->edit($shiftcode);
		}
		else {
			$daily_shift_id = $this->input->post ( 'id' );
			$shiftcode = $this->input->post ( 'shiftcode' );
			$description = $this->input->post ( 'description' );
			$notes = $this->input->post ( 'notes' );
			$site = $this->input->post ( 'site' );
			$lunch = $this->input->post ( 'lunch' );
			
			$daily_shift =array(
				'shiftcode' => $shiftcode,
				'description' => $description,
				'lunch_hour' => $lunch,
				'notes' => $notes,
				'site_id' => $site
			);
			$this->daily_shift_m->update($daily_shift_id, $daily_shift);
			
			$slot_num = $this->input->post ( 'slot_num' );
			$now = now();
			$i = 0;
			$this->load->model('St_daily_shift_slot_m', 'daily_shift_slot_m');
			$this->daily_shift_slot_m->delete_by_daily_shift($daily_shift_id);
			while($i <= $slot_num){
				$p_starttime = "starttime".$i;
				$p_endtime = "endtime".$i;
				$p_shiftcategory = "shiftcategory".$i;
				$p_rate = "rate".$i;
				$starttime = $this->input->post ($p_starttime);
				$endtime = $this->input->post ($p_endtime);
				$shiftcategory = $this->input->post ($p_shiftcategory);
				$rate = $this->input->post ($p_rate);
				if(!empty($starttime) && !empty($endtime) && !empty($shiftcategory) && !empty($rate)){
					$slot = array(
						'daily_shift_id' => $daily_shift_id,
						'start_time' => $starttime,
						'end_time' => $endtime,
						'shift_category_code' => $shiftcategory,
						'rate' => $rate,
						'add_time' => $now,
						'status' => ACTIVE
					);
					$this->daily_shift_slot_m->insert($slot);
				}
				$i++;
			}
			$this->index ();
		}
	}
	
	/**
	 * get daily shift description by id
	 */
	function get_daily_shift_ajax(){
		$id = $this->input->post('id');
		$dailyshift = $this->daily_shift_m->get($id);
		$result['description'] = $dailyshift->description;
		echo json_encode($result);		
	}
	
	/**
	 * get new slot content
	 */
	function get_new_slot_ajax(){
		$data['rates'] = $this->_get_rate_dropdown();
		$num = $this->input->post('num');
		$data['num'] = $num;
		$this->load->model('St_shift_category_m', 'shift_category_m');
		$data['shiftcategory'] = $this->shift_category_m->get_select_dropdown();
		$content = $this->load->view('dailyshift/slot_data', $data, true);
		$result = array(
        	"content" 	=> $content
        );
        echo json_encode($result);
	}
	
	function _get_rate_dropdown(){
    	$options = array(
    		''  => "Please select",
    		'1' => "1",
    		'1.5' => "1.5",
    		'2' => "2"
    	);
    	return $options;
    }	
}
