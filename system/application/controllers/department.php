<?php

class Department extends Controller {

	function Department()
	{
		parent::Controller();
		if(!$this->userauth->logined()){
			$infoMsg = array(
				'info' => 'You have no privilege into this page !'
			);
			$this->session->set_flashdata('auth', $this->load->view('msgbox/info', $infoMsg , True));
			redirect("login");
		}else{
			if(!$this->userauth->judge_priv("department")){
				$errorMsg = array(
					'error' => 'You have no privilege into this page !'
				);
				$this->session->set_flashdata('auth', $this->load->view('msgbox/error', $errorMsg , True));
				redirect("msgbox", 'location');
			}
		}
		$this->form_validation->set_error_delimiters ( "<div style='font:Tahoma, Geneva, sans-serif; font-size:11px; color:#F00'>", "</div>" );
		$this->load->model('St_department_m', 'department_m');
	}
	
	function index()
	{
		$data['department'] = $this->department_m->get_department();
        $data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('department')."'>Department</a> > List";
        $this->load->view('navigation', $data);
        $this->load->view('list_departments', $data);
	}

    function add() {
        $data['status'] = '';
        $data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('department')."'>Department</a> > Add New";
        
        //read site
        $this->load->model('St_site_m', 'site_m');
        $data['sites'] = $this->site_m->get_select_dropdown(); 
        $this->load->view('navigation', $data);
        $this->load->view('add_department', $data);
    }

    function do_add() {
    	$this->form_validation->set_rules('site_id', 'Site', 'trim|required');
		$this->form_validation->set_rules('department_name', 'Department Name', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');

		$data['status'] = '';
        if ($this->form_validation->run() == FALSE) {
            $this->add();
        } else {
            $name = $this->input->post('department_name');
            $site_id = $this->input->post('site_id');
            
        	//check unique
        	$b_exist = $this->department_m->check_except($name, $site_id, 0);
        	if($b_exist > 0){
        		$data['status'] = 'The department already exists!';
        		$data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('department')."'>Department</a> > Add New";
        		//read site
        		$this->load->model('St_site_m', 'site_m');
        		$data['sites'] = $this->site_m->get_select_dropdown(); 
        		$this->load->view('navigation', $data);
        		$this->load->view('add_department', $data);
        	}else{
        		$ip_range = $this->input->post('ip_range');
            	$status = $this->input->post('status');
            	$site_data = array(
	                'department' 			=> $name,
	                'site_id' 		=> $site_id,
	                'status' 		=> $this->input->post('status'),
        			'operator_id'	=> $this->session->userdata('user_id')
	            );
            
            	$this->department_m->insert($site_data);
            	$this->index();
        	}
        }
	}
	
	/**
	 * update department status
	 */
	function change_status()
	{
		$department_id = $this->uri->segment(3);
		$status = trim($this->uri->segment(4));
		
		if ($status == ACTIVE) {
			$this->department_m->set_inactive($department_id);
		}
		else {
			$this->department_m->set_active($department_id);
		}
		
		$data['department'] = $this->department_m->get_department();
        $data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('department')."'>Department</a> > List";
        $this->load->view('navigation', $data);
		$this->load->view('view_department', $data);
	}
	
	function edit($department_id) {
		$data['department'] = $this->department_m->get_department_single($department_id);
		//read site
        $this->load->model('St_site_m', 'site_m');
        $data['sites'] = $this->site_m->get_select_dropdown(); 
        $data['status'] = '';
        $data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('department')."'>Department</a> > Edit Department";
        $this->load->view('navigation', $data);
        $this->load->view('edit_department', $data);
    }
    
	function do_edit() {
		$this->form_validation->set_rules('department_name', 'Department Name', 'trim|required');
		$this->form_validation->set_rules('site_id', 'Site', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		
		$department_id = $this->input->post('department_id');
		$site_id = $this->input->post('site_id');
		if ($this->form_validation->run() == FALSE) {
            $this->edit($department_id);
        }else{
        	$name = $this->input->post('department_name');
        	//check unique
        	$b_exist = $this->department_m->check_except($name, $site_id, $department_id);
        	if($b_exist > 0){
        		$data['status'] = 'The department already exists!';
        		$this->load->model('St_site_m', 'site_m');
        		$data['sites'] = $this->site_m->get_select_dropdown(); 
        		$data['department'] = $this->department_m->get_department_single($department_id);
		        $data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('department')."'>Department</a> > Edit Department";
        		$this->load->view('navigation', $data);
        		$this->load->view('edit_department', $data);
        	}else{
        		$site_data = array(
	                'department' => $name,
	                'site_id' 		=> $site_id,
	                'status' 		=> $this->input->post('status'),
        			'operator_id'	=> $this->session->userdata('user_id')
	            );
        		$b_update = $this->department_m->update($department_id, $site_data);
        		$this->index();
        	}
        }
	}
	
	function remove()
	{
		$department_id = $this->uri->segment(3);
		$query = $this->db->query("DELETE FROM st_department WHERE id = '$department_id';");
		
		$this->index();
	}
}