<?php

class Calendar extends Controller {
	
	function Calendar() {
		parent::Controller();
	}
	
	function index() {
		$site_id = $this->session->userdata('site_id');
		//read site
    	$this->load->model('St_site_m', 'site_m');
        $data['sites'] = $this->site_m->get_dropdown($site_id);
//	    $data['site_id'] = $site_id;
	    $data['site_id'] = 0;

//		$data['department_id'] = $this->session->userdata('department');
		$data['department_id'] = '0';

		//load department model
		$this->load->model('St_department_m', 'department_m');
		//read department
	    $data['departments'] = $this->department_m->get_dropdown($site_id);

       	$this->load->model('St_leave_type_m', 'leave_type_m');
        $data['leave_types'] = $this->leave_type_m->get_leave_types();
        
        $data['user_level'] = $this->session->userdata('user_level');
        
//      $this->output->enable_profiler(TRUE);
		$this->load->view('request/calendar', $data);
	}
	
	function department($site_id, $department=0){
		log_message("debug", "site_id...." . $site_id);
		log_message("debug", "department...." . $department);
		$data['user_level'] = $this->session->userdata('user_level');
		$data['site_id'] = $site_id;
		$data['department_id'] = $department;
		$this->load->model('St_site_m', 'site_m');
        $data['sites'] = $this->site_m->get_dropdown();
		//load department model
		$this->load->model('St_department_m', 'department_m');
		//read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
        $this->load->model('St_leave_type_m', 'leave_type_m');
        $data['leave_types'] = $this->leave_type_m->get_leave_types();
		$this->load->view('request/calendar', $data);
	}

	/**
	 * get all events for employment
	 * @param $department_id
	 * @return json
	 */
	function events($site_id=0,$department_id=0){
		log_message("debug", "events site_id...." . $site_id);
		log_message("debug", "events department...." . $department_id);
		
		//load request model
		$this->load->model('St_request_m', 'request_m');
		
		$requests = $this->request_m->get_requests_by_calendar($site_id, $department_id, $this->input->get_post('start'), $this->input->get_post('end'));
		
		// Get personal leave that haven't been approved yet.
		$request_personal = $this->request_m->get_requests_by_calendar($site_id, $department_id, $this->input->get_post('start'), $this->input->get_post('end'), 3, array(10,20,30));
		
		$requests = array_merge($requests, $request_personal);
		 

		$list = array();
		foreach ($requests as $row) {
			$event = array();
			$event['id'] = $row->id;
			
			$event['url'] = site_url("request/view/".$row->id);
			$event['className'] = "cal_leave_" . $row->leave_type_id;
			if($row->request_type == 1){
				$event['title'] = $row->first_name . " " . $row->last_name;// ."[". $row->leave_type . "]";
				$event['start'] = date("Y-m-d", $row->start_day);
				$event['end'] = date("Y-m-d", $row->end_day);
			}else if($row->request_type == 2){
				$event['start'] = date("Y-m-d H:i", $row->start_date);
				$event['end'] = date("Y-m-d H:i", $row->end_date);
//				$event['title'] = date("H:i", $row->start_date). "-" . date("H:i", $row->end_date) . " " .  $row->first_name . " " . $row->last_name;// ."[". $row->leave_type . "]";

				$event['title'] = $row->start_time;
				if ($row->end_time)
					$event['title'] .= 	"-" . $row->end_time;
				$event['title'] .= 	" " .  $row->first_name . " " . $row->last_name;// ."[". $row->leave_type . "]";
					
				//$event['allDay'] = false;
			}
			$description = $row->first_name . " " . $row->last_name .
					"<br>Type: " . $row->leave_type . 
					"<br>Dept: " . $row->department . 
					"<br>Start: " . date("Y-m-d H:i", $row->start_date) .
					"<br>End: " . date("Y-m-d H:i", $row->end_date);
			if($row->leave_type_id == LEAVE_TYPE_LATE 
				|| $row->leave_type_id == LEAVE_TYPE_LUNCH){
				$description .= "<br>Minutes used: " . $row->hours_used * 60;
			}else{
				$description .= "<br>Hours used: " . $row->hours_used;
			}					
			$description .= "<br>Requested Date: ". date("Y-m-d H:i", $row->add_time) .
						"<br>Approved Date: " . date("Y-m-d H:i", $row->manager_time) .
						"<br>Approved By: " . $row->mfirst_name . " " . $row->mlast_name
					;
			$event['description'] = $description;
			$list[] = $event;
//			log_message("debug", "Event title:" . $event['title']);
		}
		
		echo json_encode($list);
	}
}