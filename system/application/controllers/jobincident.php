<?php

class JobIncident extends Controller {

    function JobIncident() {
        parent::Controller ();
    	if(!$this->userauth->logined()){
			$infoMsg = array(
				'info' => 'Please login !'
			);
			$this->session->set_flashdata('auth', $this->load->view('msgbox/info', $infoMsg , True));
			$this->session->set_flashdata('back_url', current_url());
			redirect("login");
		}else{
			if(!$this->userauth->judge_priv("jobincident")){
				$errorMsg = array(
					'error' => 'You have no privilege into this page !'
				);
				$this->session->set_flashdata('auth', $this->load->view('msgbox/error', $errorMsg , True));
				redirect("msgbox", 'location');
			}
		}
		
		$this->form_validation->set_error_delimiters('<p class="error">', '</p>');
		
		//load request model
		$this->load->model('St_job_incident_m', 'job_incident_m');
    }
	
    function index() {
    	$user_id = $this->session->userdata('user_id');
    	$data['navigation'] = "<a href='". site_url('admin/jobincident') ."' >Job Incident</a> > List";
        
        $data['new_jobs'] = $this->job_incident_m->get_new_by_user($user_id);
        $data['progress_jobs'] = $this->job_incident_m->get_progress_by_user($user_id);
        $data['completed_jobs'] = $this->job_incident_m->get_completed_by_user($user_id);
        
        $new_count = $this->job_incident_m->count_new_by_user($user_id);
        $progress_count = $this->job_incident_m->count_progress_by_user($user_id);
        $completed_count = $this->job_incident_m->count_completed_by_user($user_id);
        
        $data['new_count'] = $new_count;
        $data['progress_count'] = $progress_count;
        $data['completed_count'] = $completed_count;
        
        $new_page_count = 0;
    	if(($new_count % PAGE_SIZE) == 0){
    		$new_page_count = $new_count / PAGE_SIZE;
    	}else{
    		$new_page_count = floor($new_count / PAGE_SIZE) + 1;
    	}
        
    	$data['new_page_no'] = 1;
    	$data['new_page_count'] = $new_page_count;
    	
    	$progress_page_count = 0;
    	if(($progress_count % PAGE_SIZE) == 0){
    		$progress_page_count = $progress_count / PAGE_SIZE;
    	}else{
    		$progress_page_count = floor($progress_count / PAGE_SIZE) + 1;
    	}
        
    	$data['progress_page_no'] = 1;
    	$data['progress_page_count'] = $progress_page_count;
    	
    	$completed_page_count = 0;
    	if(($completed_count % PAGE_SIZE) == 0){
    		$completed_page_count = $completed_count / PAGE_SIZE;
    	}else{
    		$completed_page_count = floor($new_count / PAGE_SIZE) + 1;
    	}
        
    	$data['completed_page_no'] = 1;
    	$data['completed_page_count'] = $completed_page_count;
    	
        $this->load->view('navigation', $data);
        $this->load->view('jobincident/list_job_incident');
    }
    
    function list_jobs(){
    	$this->index();
    }
    
    function new_ajax(){
    	$user_id = $this->session->userdata('user_id');
    	$data['form_id'] = 'new';
        
    	$page_no = $this->input->post('new_page_no');
        
		$limit = $this->input->post('new_limit');
    	$data['page_limit'] = $limit;
    	
    	$data['jobs'] = $this->job_incident_m->get_new_by_user($user_id,$page_no, $limit);
		$total_count = $this->job_incident_m->count_new_by_user($user_id);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
		
		$content = $this->load->view('jobincident/list_job_incident_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }
    
    function progress_ajax(){
    	$user_id = $this->session->userdata('user_id');
    	$data['form_id'] = 'progress';
        
    	$page_no = $this->input->post('progress_page_no');
        
		$limit = $this->input->post('progress_limit');
    	$data['page_limit'] = $limit;
    	
    	$data['jobs'] = $this->job_incident_m->get_progress_by_user($user_id,$page_no, $limit);
		$total_count = $this->job_incident_m->count_progress_by_user($user_id);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
		
		$content = $this->load->view('jobincident/list_job_incident_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }
    
    function completed_ajax(){
    	$user_id = $this->session->userdata('user_id');
    	$data['form_id'] = 'completed';
        
    	$page_no = $this->input->post('completed_page_no');
        
		$limit = $this->input->post('completed_limit');
    	$data['page_limit'] = $limit;
    	
    	$data['jobs'] = $this->job_incident_m->get_completed_by_user($user_id,$page_no, $limit);
		$total_count = $this->job_incident_m->count_completed_by_user($user_id);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
		
		$content = $this->load->view('jobincident/list_job_incident_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }
    /**
     * prepare to add a new leave request
     * @return 
     */
    function add() {
        $data['navigation'] = "<a href='". site_url('admin/jobincident') ."' >Job Incident</a> > Add a new job incident";
        $this->load->model('St_department_m', 'department_m');
        $dept = $this->department_m->get($this->session->userdata('department_id'));
        
        $data['department'] = $dept->department;
        
        $this->load->view('navigation', $data);
        $this->load->view('jobincident/add_job_incident', $data);
    }
    
    
    /**
     * deal with add new a job incident
     * @return void
     */
	function do_add() {
        $this->form_validation->set_rules('incident_date', 'Date of incident', 'trim|required');
    	$this->form_validation->set_rules('customer', 'Customer(s) involved', 'trim|required');
        $this->form_validation->set_rules('reference_no', 'Reference no - Invoice no', 'trim|required');
        $this->form_validation->set_rules('detail', 'Detail of the inicident', 'trim|required');
        $this->form_validation->set_rules('how_occur', 'How did the incident occur', 'trim|required');
        $this->form_validation->set_rules('how_prevent', 'What should have been done to prevent it from happening next time', 'trim|required');
        $this->form_validation->set_rules('how_repair', 'What action should be taken now to repair or recover from the incident', 'trim|required');
        $this->form_validation->set_rules('what_cost', 'What is the approximate time and cost involved as a result of this incident', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $this->add();
        } else {
        	$now_time = now();
        	$user_id = $this->session->userdata('user_id');
        	$user_name = $this->session->userdata('user_name');
        	
        	$operate = $this->input->post('operate');
        	$incident_date = $this->input->post('incident_date');
        	$incident_date = strtotime($incident_date);
        	
        	$customer = $this->input->post('customer');
        	$reference_no = $this->input->post('reference_no');
        	$detail = $this->input->post('detail');
        	$how_occur = $this->input->post('how_occur');
        	$how_prevent = $this->input->post('how_prevent');
        	$how_repair = $this->input->post('how_repair');
        	$what_cost = $this->input->post('what_cost');
        	
        	$state = 0;
        	if($operate == "save"){
        		$state = 1;
        	}else if($operate == "apply"){
        		$state = 2;
        	}
        	
        	$jobincident = array(
        		'user_id' => $user_id,
        		'incident_date' => $incident_date,
        		'customer' => $customer,
        		'reference_no' => $reference_no,
        		'detail' => $detail,
        		'how_occur' => $how_occur,
        		'how_prevent' => $how_prevent,
        		'how_repair' => $how_repair,
        		'what_cost' => $what_cost,
        		'state' => $state,
        		'incident_date' => $incident_date,
        		'status' => ACTIVE,
        		'add_time' => $now_time
        	);
        	$insert_id = $this->job_incident_m->insert($jobincident);
        	$this->load->model('St_users_m', 'users_m');
        	$job_info = $this->job_incident_m->get_info($insert_id);
        	
        	$this->load->model('St_job_incident_log_m', 'job_incident_log_m');
        	if($state == 1){
        		$jobincident_log = array(
        			'operator_id' => $user_id,
        			'operation' => 'save',
        			'operate_time' => $now_time,
        			'operate_ip' => $this->input->ip_address()
        		);
        		$jobincident_log = array_merge($jobincident, $jobincident_log);
        		$this->job_incident_log_m->insert($jobincident_log);
        	}else if($state == 2){
        		$jobincident_log = array(
        			'operator_id' => $user_id,
        			'operation' => 'apply',
        			'operate_time' => $now_time,
        			'operate_ip' => $this->input->ip_address()
        		);
        		$jobincident_log = array_merge($jobincident, $jobincident_log);
        		
        		$this->job_incident_log_m->insert($jobincident_log);
        		log_message("debug", "mail ...... 1");
        		$this->load->library('phpmail');
        		$this->phpmail->readConfig();
        		$user_level = $this->session->userdata("user_level");
        		
    			//need to send an email notification to supervisor
    			//read supervisor of current department
    			$hasSuper = false;
    			$supervisors = $this->users_m->get_supervisors_by_user_name($user_name);
    			if(!empty($supervisors)){
        			foreach($supervisors as $supervisor){
        				if(!empty($supervisor)){
        					if(!empty($supervisor->email)){
        						$this->phpmail->AddAddress($supervisor->email);
        						$hasSuper = true;
        					}
//        					if(!empty($supervisor->personal_email)){
//       						$this->phpmail->AddCC($supervisor->personal_email);
//      						$hasSuper = true;
//	     					}
        				}
        			}
        			
    			}
				
				if($hasSuper){
					$this->phpmail->setSubject("Job Incident id: " . $insert_id . " - " . $this->session->userdata('first_name') . " " . $this->session->userdata('last_name'));
        			$mail_body['appove_url'] = site_url('jobincident/deal/'.$insert_id);
	            	$mail_body['appove_name'] = "approve this job incident here.";
	            	$mail_body['jobincident'] = $job_info;
	            	
	    			$this->phpmail->setBody($this->load->view('mail/incident', $mail_body , True));
	    			if(!$this->phpmail->send()){
			        	log_message("debug", "send mail .... " . $this->phpmail->ErrorInfo);
			        }
			        $this->phpmail->ClearAllAddresses();
				}
				
        		//send mail to all manager and admin
        		$hasManager = false;
        		$managers = $this->users_m->get_managers_by_user_name($user_name);
    			if(!empty($managers)){
        			foreach($managers as $manager){
        				if(!empty($manager)){
        					if(!empty($manager->email)){
        						$this->phpmail->AddAddress($manager->email);
        						$hasManager = true;
        					}
//        					if(!empty($manager->personal_email)){
//        						$this->phpmail->AddCC($manager->personal_email);
//        						$hasManager = true;
//        					}
        				}
        			}
    			}
    			if($hasManager){
    				$this->phpmail->setSubject("Job Incident id: " . $insert_id . " - " . $this->session->userdata('first_name') . " " . $this->session->userdata('last_name'));
    				$mail_body['appove_url'] = site_url('jobincident/deal/'.$insert_id);
            		$mail_body['appove_name'] = "review this job incident here.";
            		$mail_body['jobincident'] = $job_info;
    				$this->phpmail->setBody($this->load->view('mail/incident', $mail_body , True));
	    			if(!$this->phpmail->send()){
			        	log_message("debug", "send mail .... " . $this->phpmail->ErrorInfo);
			        }
			        $this->phpmail->ClearAllAddresses();
    			}
    			
    			$hasSelf = false;
        		$email = $this->session->userdata("email");
        		if(!empty($email)){
        			$this->phpmail->AddAddress($email);
        			log_message("debug", "email .... " . $email);
        			$hasSelf = true;
    			}
    			$personal_email = $this->session->userdata("personal_email");
    			if(!empty($personal_email)){
    				$this->phpmail->AddCC($personal_email);
    				log_message("debug", "personal_email .... " . $personal_email);
    				$hasSelf = true;
    			}
    			if($hasSelf){
    				$this->phpmail->setSubject("Job Incident id: " . $insert_id . " - " . $this->session->userdata('first_name') . " " . $this->session->userdata('last_name'));
	        		$mail_body['appove_url'] = site_url('jobincident/view/'.$insert_id);
            		$mail_body['appove_name'] = "view this job incident here.";
            		$mail_body['jobincident'] = $job_info;
    				$this->phpmail->setBody($this->load->view('mail/incident', $mail_body , True));
	    			if(!$this->phpmail->send()){
			        	log_message("debug", "send mail .... " . $insert_id . $this->phpmail->ErrorInfo);
			        }
			        $this->phpmail->ClearAllAddresses();
    			}
        	}
        	$this->incident_list('new');
        }
	}
    
    function incident_list($status) {
        $user_level = $this->session->userdata('user_level');
        $site_id = $this->session->userdata('site_id');
        $department_id = $this->session->userdata('department');

		//load site model
		$this->load->model('St_site_m', 'site_m');
		//load department model
		$this->load->model('St_department_m', 'department_m');
		
   		//read site
        $data['sites'] = $this->site_m->get_dropdown();
        
        //read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
        
        if($user_level > 2){
        	$site_id = 0;
        	$department_id = 0;
        }
        $data['site_id'] = $site_id;
        $data['department_id'] = $department_id;
        
        switch ($status){
        	case 'new':
        		$data['jobs'] = $this->job_incident_m->get_approvals_by_new($user_level, $site_id, $department_id);
        		$data['navigation'] = "<a href='". site_url('admin/jobincident') ."' target='_top'>Job Incident</a> > New";
        		$count = $this->job_incident_m->count_approvals_by_new($user_level, $site_id, $department_id); 
        		break;
        	case 'progress':
				$data['jobs'] = $this->job_incident_m->get_approvals_by_progress($user_level, $site_id, $department_id);
				$data['navigation'] = "<a href='". site_url('admin/jobincident') ."' target='_top'>Job Incident</a> > In Progress";
				$count = $this->job_incident_m->count_approvals_by_progress($user_level, $site_id, $department_id);
        		break;
        	case 'completed':
        		$data['jobs'] = $this->job_incident_m->get_approvals_by_completed($user_level, $site_id, $department_id);
        		$data['navigation'] = "<a href='". site_url('admin/jobincident') ."' target='_top'>Job Incident</a> > Completed";
        		$count = $this->job_incident_m->count_approvals_by_completed($user_level, $site_id, $department_id);
        		break;
        
        }
        
        $data['count'] = $count;
        $data['status'] = $status;
        $data['user_level'] = $user_level;
        
        $page_count = 0;
    	if(($count % PAGE_SIZE) == 0){
    		$page_count = $count / PAGE_SIZE;
    	}else{
    		$page_count = floor($count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	
    	
        $this->load->view('navigation', $data);
        $this->load->view('jobincident/list_job_incident');
    }
    
    /**
     * approve job incident
     */
    function deal($id) {
    	$data['navigation'] = "<a href='". site_url('admin/jobincident') ."' target='_top'>Job Incident</a> > <a href='". site_url('jobincident/approve') ."'>Incident Approval</a> ";
        
        $job = $this->job_incident_m->get_info($id);
        if(!$job){
        	show_404();
        }
        $data['job'] = $job;
        
        $this->load->view('navigation', $data);
        $this->load->view('jobincident/deal_job_incident');
    }
    
    /**
     * process approve job incident
     */
    function do_deal() {
    	$now_time = now();
    	$user_id = $this->session->userdata('user_id');
    	$user_level = $this->session->userdata('user_level');
    	$job_id = $this->input->post('job_id');
    	
    	if($user_level >= 2){
    		$supervisor_comment = $this->input->post('supervisor_comment');
    		$manager_comment = $this->input->post('manager_comment');	
    		$take_action = $this->input->post('take_action');
    		$resolved = $this->input->post('resolved');
    		$approved = $this->input->post('approved');
    		
    		$jobincident = array(
	    		'supervisor_comment' => $supervisor_comment,
    			'manager_comment' => $manager_comment,
	    		'take_action' => $take_action,
	    		'resolved' => $resolved,
	    		'approved' => $approved,
	    		'state' => 3
	    	);
	    	
    		$this->job_incident_m->update($job_id, $jobincident);
    		
    		$this->load->model('St_users_m', 'users_m');
    		$job_info = $this->job_incident_m->get_info($job_id);
    		$this->load->library('phpmail');
        	$this->phpmail->readConfig();
        	$hasSuper = false;
    		$supervisors = $this->users_m->get_supervisors_by_user_name($job_info->user_name);
    		if(!empty($supervisors)){
    			foreach($supervisors as $supervisor){
    				if(!empty($supervisor)){
    					if(!empty($supervisor->email)){
    						$this->phpmail->AddAddress($supervisor->email);
    						$hasSuper = true;
    					}
//    					if(!empty($supervisor->personal_email)){
//    						$this->phpmail->AddCC($supervisor->personal_email);
//    						$hasSuper = true;
//    					}
    				}
    			}
    		}
        	
			//send mail to all manager and admin
        	$managers = $this->users_m->get_managers_by_user_name($job_info->user_name);
        	if(!empty($managers)){
        		foreach($managers as $manager){
        			if(!empty($manager)){
        				if(!empty($manager->email)){
        					$this->phpmail->AddAddress($manager->email);
        				}
//        				if(!empty($manager->personal_email)){
//        					$this->phpmail->AddCC($manager->personal_email);
//        				}
        			}
        		}
        	}
        	if(!empty($job_info->email)){
        		$this->phpmail->AddAddress($job_info->email);
        	}
//    		if(!empty($job_info->personal_email)){
//        		$this->phpmail->AddAddress($job_info->personal_email);
//        	}
        	
        	$approve_str = "";
        	if($approved == 1){
        		$approve_str = "Approved ";
        	}else if($approved == 2){
        		$approve_str = "Not Approved ";
        	}
        	
        	$this->phpmail->setSubject($approve_str . "Job Incident id: " . $job_id . " - " . $job_info->first_name . " " . $job_info->last_name);
			$mail_body['appove_url'] = site_url('jobincident/view/'.$job_id);
        	$mail_body['appove_name'] = "view this job incident here.";
        	$mail_body['jobincident'] = $job_info;
        	$mail_body['first_name'] = $this->session->userdata('first_name');
        	$mail_body['last_name'] = $this->session->userdata('last_name');
        	
			$this->phpmail->setBody($this->load->view('mail/incident2', $mail_body , True));
			if(!$this->phpmail->send()){
	        	log_message("debug", "send mail .... " . $this->phpmail->ErrorInfo);
	        }
	        $this->phpmail->ClearAllAddresses();
    		
    	}else if($user_level == 3){
    		$manager_comment = $this->input->post('manager_comment');	
    		$jobincident = array(
	    		'manager_comment' => $manager_comment
	    	);
	    	$this->job_incident_m->update($job_id, $jobincident);
	    	
	    	
    	}
    	
    	$this->approve();
    }
    
    /**
     * approve job incident
     */
    function edit($id) {
    	$data['navigation'] = "<a href='". site_url('admin/jobincident') ."' >Job Incident</a> > <a href='". site_url('jobincident/approve') ."'>Incident Approval</a> ";
        
        $job = $this->job_incident_m->get_info($id);
        if(!$job){
        	show_404();
        }
        $data['job'] = $job;
        
        $this->load->view('navigation', $data);
        $this->load->view('jobincident/edit_job_incident');
    }
    
    /**
     * process edit job incident
     */
    function do_edit() {
    	$now_time = now();
    	$job_id = $this->input->post('job_id');
    	$this->form_validation->set_rules('incident_date', 'Date of incident', 'trim|required');
    	$this->form_validation->set_rules('customer', 'Customer(s) involved', 'trim|required');
        $this->form_validation->set_rules('reference_no', 'Reference no - Invoice no', 'trim|required');
        $this->form_validation->set_rules('detail', 'Detail of the inicident', 'trim|required');
        $this->form_validation->set_rules('how_occur', 'How did the incident occur', 'trim|required');
        $this->form_validation->set_rules('how_prevent', 'What should have been done to prevent it from happening next time', 'trim|required');
        $this->form_validation->set_rules('how_repair', 'What action should be taken now to repair or recover from the incident', 'trim|required');
        $this->form_validation->set_rules('what_cost', 'What is the approximate time and cost involved as a result of this incident', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $this->edit($job_id);
        } else {
        	$user_id = $this->session->userdata('user_id');
        	$user_name = $this->session->userdata('user_name');
        	
        	$operate = $this->input->post('operate');
        	$incident_date = $this->input->post('incident_date');
        	$incident_date = strtotime($incident_date);
        	
        	$customer = $this->input->post('customer');
        	$reference_no = $this->input->post('reference_no');
        	$detail = $this->input->post('detail');
        	$how_occur = $this->input->post('how_occur');
        	$how_prevent = $this->input->post('how_prevent');
        	$how_repair = $this->input->post('how_repair');
        	$what_cost = $this->input->post('what_cost');
        	
        	$state = 0;
        	if($operate == "save"){
        		$state = 1;
        	}else if($operate == "apply"){
        		$state = 2;
        	}
        	
        	$jobincident = array(
        		'user_id' => $user_id,
        		'incident_date' => $incident_date,
        		'customer' => $customer,
        		'reference_no' => $reference_no,
        		'detail' => $detail,
        		'how_occur' => $how_occur,
        		'how_prevent' => $how_prevent,
        		'how_repair' => $how_repair,
        		'what_cost' => $what_cost,
        		'state' => $state,
        		'incident_date' => $incident_date,
        	);
        	$this->job_incident_m->update($job_id, $jobincident);
        	$this->load->model('St_users_m', 'users_m');
        	$job_info = $this->job_incident_m->get_info($job_id);
        	
        	$this->load->model('St_job_incident_log_m', 'job_incident_log_m');
        	if($state == 1){
        		$jobincident_log = array(
        			'operator_id' => $user_id,
        			'operation' => 'update',
        			'operate_time' => $now_time,
        			'operate_ip' => $this->input->ip_address()
        		);
        		$jobincident_log = array_merge($jobincident, $jobincident_log);
        		$this->job_incident_log_m->insert($jobincident_log);
        	}else if($state == 2){
        		$jobincident_log = array(
        			'operator_id' => $user_id,
        			'operation' => 'apply',
        			'operate_time' => $now_time,
        			'operate_ip' => $this->input->ip_address()
        		);
        		$jobincident_log = array_merge($jobincident, $jobincident_log);
        		
        		$this->job_incident_log_m->insert($jobincident_log);
        		log_message("debug", "mail ...... 1");
        		$this->load->library('phpmail');
        		$this->phpmail->readConfig();
        		$user_level = $this->session->userdata("user_level");
        		
        		//need to send an email notification to supervisor
    			//read supervisor of current department
    			$hasSuper = false;
    			$supervisors = $this->users_m->get_supervisors_by_user_name($user_name);
    			if(!empty($supervisors)){
        			foreach($supervisors as $supervisor){
        				if(!empty($supervisor)){
        					if(!empty($supervisor->email)){
        						$this->phpmail->AddAddress($supervisor->email);
        						$hasSuper = true;
        					}
//        					if(!empty($supervisor->personal_email)){
//        						$this->phpmail->AddCC($supervisor->personal_email);
//        						$hasSuper = true;
//        					}
        				}
        			}
        			
    			}
				
				if($hasSuper){
					$this->phpmail->setSubject("Job Incident id: " . $job_id . " - " . $this->session->userdata('first_name') . " " . $this->session->userdata('last_name'));
        			$mail_body['appove_url'] = site_url('jobincident/deal/'.$job_id);
	            	$mail_body['appove_name'] = "approve this job incident here.";
	            	$mail_body['jobincident'] = $job_info;
	            	
	    			$this->phpmail->setBody($this->load->view('mail/incident', $mail_body , True));
	    			if(!$this->phpmail->send()){
			        	log_message("debug", "send mail .... " . $this->phpmail->ErrorInfo);
			        }
			        $this->phpmail->ClearAllAddresses();
				}
				
        		//send mail to all manager and admin
        		$hasManager = false;
        		$managers = $this->users_m->get_managers_by_user_name($user_name);
    			if(!empty($managers)){
        			foreach($managers as $manager){
        				if(!empty($manager)){
        					if(!empty($manager->email)){
        						$this->phpmail->AddAddress($manager->email);
        						$hasManager = true;
        					}
//        					if(!empty($manager->personal_email)){
//        						$this->phpmail->AddCC($manager->personal_email);
//        						$hasManager = true;
//        					}
        				}
        			}
    			}
    			if($hasManager){
    				$this->phpmail->setSubject("Job Incident id: " . $job_id . " - " . $this->session->userdata('first_name') . " " . $this->session->userdata('last_name'));
    				$mail_body['appove_url'] = site_url('jobincident/deal/'.$job_id);
            		$mail_body['appove_name'] = "review this job incident here.";
            		$mail_body['jobincident'] = $job_info;
    				$this->phpmail->setBody($this->load->view('mail/incident', $mail_body , True));
	    			if(!$this->phpmail->send()){
			        	log_message("debug", "send mail .... " . $this->phpmail->ErrorInfo);
			        }
			        $this->phpmail->ClearAllAddresses();
    			}
    			
    			$hasSelf = false;
        		$email = $this->session->userdata("email");
        		if(!empty($email)){
        			$this->phpmail->AddAddress($email);
        			$hasSelf = true;
    			}
    			$personal_email = $this->session->userdata("personal_email");
    			if(!empty($personal_email)){
    				$this->phpmail->AddCC($personal_email);
    				$hasSelf = true;
    			}
    			if($hasSelf){
    				$this->phpmail->setSubject("Job Incident id: " . $job_id . " - " . $this->session->userdata('first_name') . " " . $this->session->userdata('last_name'));
	        		$mail_body['appove_url'] = site_url('jobincident/view/'.$job_id);
            		$mail_body['appove_name'] = "view this job incident here.";
            		$mail_body['jobincident'] = $job_info;
    				$this->phpmail->setBody($this->load->view('mail/incident', $mail_body , True));
	    			if(!$this->phpmail->send()){
			        	log_message("debug", "send mail .... " . $job_id . $this->phpmail->ErrorInfo);
			        }
			        $this->phpmail->ClearAllAddresses();
    			}
        	}
        	$this->incident_list('new');
        }
	}
    /**
     * approve job incident
     */
    function view($id, $from=1) {
    	$data['navigation'] = "<a href='". site_url('admin/jobincident') ."' target='_top'>Job Incident</a> > <a href='". site_url('jobincident') ."'>Incident Approval</a> ";
        
        $job = $this->job_incident_m->get_info($id);
        if(!$job){
        	show_404();
        }
        $data['job'] = $job;
        $data['from'] = $from;
        $this->load->view('navigation', $data);
        $this->load->view('jobincident/view_job_incident');
    }
    
    function new_approvals_ajax(){
    	log_message("debug", "new_approvals_ajax .....");
		$user_level = $this->session->userdata('user_level');
		
    	$data['form_id'] = 'new';
    	
    	$site_id = $this->input->post('new_filter_site_id');
    	$department_id = $this->input->post('new_filter_department_id');
    	$page_no = $this->input->post('new_page_no');
        
		$limit = $this->input->post('new_limit');
    	$data['page_limit'] = $limit;
    	
		$data['jobs'] = $this->job_incident_m->get_approvals_by_new($user_level, $site_id, $department_id, $page_no, $limit);
		$total_count = $this->job_incident_m->count_approvals_by_new($user_level, $site_id, $department_id);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
		
		$content = $this->load->view('jobincident/list_approve_job_incident_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }
    
    function progress_approvals_ajax(){
    	log_message("debug", "progress_approvals_ajax .....");
		$user_level = $this->session->userdata('user_level');
		
    	$data['form_id'] = 'progress';
    	
    	$site_id = $this->input->post('progress_filter_site_id');
    	$department_id = $this->input->post('progress_filter_department_id');
    	$page_no = $this->input->post('progress_page_no');
        
		$limit = $this->input->post('progress_limit');
    	$data['page_limit'] = $limit;
    	
		$data['jobs'] = $this->job_incident_m->get_approvals_by_progress($user_level, $site_id, $department_id, $page_no, $limit);
		$total_count = $this->job_incident_m->count_approvals_by_progress($user_level, $site_id, $department_id);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
		
		$content = $this->load->view('jobincident/list_approve_job_incident_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }
    
    function completed_approvals_ajax(){
    	log_message("debug", "completed_approvals_ajax .....");
		$user_level = $this->session->userdata('user_level');
		
    	$data['form_id'] = 'completed';
    	
    	$site_id = $this->input->post('completed_filter_site_id');
    	$department_id = $this->input->post('completed_filter_department_id');
    	$page_no = $this->input->post('completed_page_no');
        
		$limit = $this->input->post('completed_limit');
    	$data['page_limit'] = $limit;
    	
		$data['jobs'] = $this->job_incident_m->get_approvals_by_completed($user_level, $site_id, $department_id, $page_no, $limit);
		$total_count = $this->job_incident_m->count_approvals_by_completed($user_level, $site_id, $department_id);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
		
		$content = $this->load->view('jobincident/list_approve_job_incident_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }
}