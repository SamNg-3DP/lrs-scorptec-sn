<?php

class User extends Controller {

    function User() {
        parent::Controller();
    	if(!$this->userauth->logined()){
			$infoMsg = array(
				'info' => 'Please login!'
			);
			$this->session->set_flashdata('auth', $this->load->view('msgbox/info', $infoMsg , True));
			redirect("login");
		}else{
			if(!$this->userauth->judge_priv("user")){
				$errorMsg = array(
					'error' => 'You have no privilege into this page !'
				);
				$this->session->set_flashdata('auth', $this->load->view('msgbox/error', $errorMsg , True));
				redirect("msgbox", 'location');
			}
		}
        $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
        $this->load->model('St_users_m', 'users_m');
    }

    function index() {
        $data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='". site_url('user') ."'>User</a> > List";
        $this->load->view('navigation', $data);
        
        $site_id = $this->session->userdata('site_id');
        
        //read site
        $this->load->model('St_site_m', 'site_m');
        $data['sites'] = $this->site_m->get_dropdown();
        $data['site_id'] = $site_id;
        //read department
    	$this->load->model('St_department_m', 'department_m');
        $data['departments'] = $this->department_m->get_dropdown($site_id);
        
        //read userlevels
        $this->load->model('St_user_level_m', 'user_level_m');
        $data['user_levels'] = $this->user_level_m->get_dropdown();
        
        $this->load->model('St_employment_type_m', 'employment_type_m');
	    $data['employment_types'] = $this->employment_type_m->get_dropdown();
	        
        $data['status'] = ACTIVE;
    	$data['statuses'] = $this->_get_status_dropdown();
    	
        $data['form_id'] = "users";
        
        $data['users'] = $this->users_m->get_users_order_by();
        
        //get total count
    	$total_count = $this->users_m->count_order_by();

    	//compute page count
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
    	log_message("debug", "page count === " . $page_count);
        $data['page_count'] = $page_count;
        $data['page_no'] = 1;
        
        $this->load->view('user/list_users', $data);
        //$this->output->cache(1);
    }
    
    /**
     * list users by custom order by
     * @return json
     */
    function list_users_ajax(){
    	$data['form_id'] = "users";
    	$order_by = $this->input->post('order_by');
    	$order = $this->input->post('order');
    	$status = $this->input->post('filter_status_id');
    	$site_id = $this->input->post('filter_site_id');
    	$department_id = $this->input->post('filter_department_id');
    	$user_level_id = $this->input->post('filter_user_level_id');
    	$employment_type_id = $this->input->post('filter_employment_type_id');
    	
    	$page_no = $this->input->post('page_no');
    	
    	$sort_name = "sort_" . $order_by;
    	
    	//indicat the sort order by image
    	$data['sort_user_name'] = "";
    	$data['sort_first_name'] = "";
    	$data['sort_last_name'] = "";
    	$data['sort_department_id'] = "";
    	$data['sort_user_level_id'] = "";
    	$data['sort_birthday'] = "";
    	$data['sort_review_date'] = "";
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
    	
    	if(empty($page_no)){
			$page_no = 1;
		}
		$data['page_no'] = $page_no;
    	$limit = $this->input->post('users_limit');
    	$data['page_limit'] = $limit;
    	
    	$data['users'] = $this->users_m->get_users_order_by($site_id, $department_id, $user_level_id, $employment_type_id, $status, $order_by, $order, $page_no, $limit);
    	$total_count = $this->users_m->count_order_by($site_id, $department_id, $user_level_id, $employment_type_id, $status);
    	
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	$data['page_count'] = $page_count;
    	
    	$str = $this->load->view('user/list_users_data', $data, true);
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }

    function add() {
        $data['status'] = '';
        //read site
        $this->load->model('St_site_m', 'site_m');
        $data['sites'] = $this->site_m->get_select_dropdown();
        $this->load->model('St_department_m', 'department_m');
        $data['departments'] = array(""=>"Please select..");//$this->department_m->get_select_dropdown();
        $this->load->model('St_user_level_m', 'user_level_m');
        $data['user_level'] = $this->user_level_m->get_my_user_level($this->session->userdata('user_level'));
        $this->load->model('St_employment_type_m', 'employment_type_m');
        $data['employment_types'] = $this->employment_type_m->get_select_dropdown();
        $data['residency_statuses'] = $this->_get_residency_status();
        $data['sexs'] = $this->_get_sex_dropdown();
        $data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='". site_url('user') ."'>User</a> > Add New";
        $this->load->view('navigation', $data);
        //$this->load->view('add_userinfo', $data);
        $this->load->view('user/add_user', $data);
    }

    /**
     * save user info
     * @return json
     */
	function save() {
        $this->form_validation->set_rules('user_name', 'Username', 'required|max_length[20]|min_length[2]');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[20]|min_length[3]');
        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        $this->form_validation->set_rules('sex', 'Sex', 'required');
        $this->form_validation->set_rules('department_id', 'Department', 'required');
        $this->form_validation->set_rules('user_level_id', 'User Level', 'required');
        $this->form_validation->set_rules('employment_type_id', 'Employement Type', 'required');
//        $this->form_validation->set_rules('residency_status', 'Residency Status', '');
        $this->form_validation->set_rules('hire_date', 'Hire Date', 'required');
//        $this->form_validation->set_rules('address', 'Address', 'required');
//        $this->form_validation->set_rules('home_phone', 'Home Phone', 'required');
//        $this->form_validation->set_rules('mobile', 'Mobile', 'required');
        $this->form_validation->set_rules('email', 'Email(work)', 'required|valid_email');
//        $this->form_validation->set_rules('personal_email', 'Email(personal)', 'required|valid_email');
//        $this->form_validation->set_rules('bank_account_name', 'Account Name', 'required');
//        $this->form_validation->set_rules('bank_name', 'Bank Name', 'required');
//        $this->form_validation->set_rules('bank_bsb', 'BSB', 'required');
//        $this->form_validation->set_rules('bank_account_no', 'Account No', 'required');

		log_message("debug", "employment_type_id == ". $this->input->post('employment_type_id'));        
        if ($this->form_validation->run() == FALSE) {
            $this->index_add();
        }
        else {
        	$actatek_id = $this->input->post('actatek_id');
            $user_name = $this->input->post('user_name');
            $password = $this->input->post('password');
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $sex = $this->input->post('sex');
            $department_id = $this->input->post('department_id');
            $user_level_id = $this->input->post('user_level_id');
            $employment_type_id = $this->input->post('employment_type_id');
            $residency_status = $this->input->post('residency_status');
            $hire_date = $this->input->post('hire_date');
            $terminate_date = $this->input->post('terminate_date');
            $review_date = $this->input->post('review_date');
            $address = $this->input->post('address');
            $home_phone = $this->input->post('home_phone');
            $mobile = $this->input->post('mobile');
            $email = $this->input->post('email');
            $personal_email = $this->input->post('personal_email');
            $birthday = $this->input->post('birth_date');
            if(!empty($birthday)){
	        	$birthday = strtotime($birthday);
            }else{
            	$birthday = 0;
            }
            $kinname = $this->input->post('kin_name');
            $contact = $this->input->post('kin_contact');
            $medical = $this->input->post('medical_conditions');
            $driverno = $this->input->post('driver_license_no');
            $passportno = $this->input->post('passport_license_no');
            $tfn = $this->input->post('tfn');
            $accountname = $this->input->post('bank_account_name');
            $bankname = $this->input->post('bank_name');
            $bsb = $this->input->post('bank_bsb');
            $accountno = $this->input->post('bank_account_no');
			
        	$photo_filename = "";
	        $fileElementName = 'photo_file';
	        $error = "";
	        if(empty($_FILES[$fileElementName]['tmp_name']) || $_FILES[$fileElementName]['tmp_name'] == 'none')
			{}else{
				if(!empty($_FILES[$fileElementName]['error']))
				{
					switch($_FILES[$fileElementName]['error'])
					{
						case '1':
							$error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
							break;
						case '2':
							$error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
							break;
						case '3':
							$error = 'The uploaded file was only partially uploaded';
							break;
						case '4':
							$error = 'No file was uploaded.';
							break;
			
						case '6':
							$error = 'Missing a temporary folder';
							break;
						case '7':
							$error = 'Failed to write file to disk';
							break;
						case '8':
							$error = 'File upload stopped by extension';
							break;
						case '999':
						default:
							$error = 'No error code avaiable';
					}
				}else 
				{
					$tempFile = $_FILES[$fileElementName]['tmp_name'];
					
					$now_dir = date('Y/m/d');
					$targetPath = str_replace('//','/',DIR_PHOTO . $now_dir . '/');
					
					if(mkdirs($targetPath)){
						$filename = $_FILES[$fileElementName]['name'];
						
						// Get the extension from the filename.
						$extension = substr($filename, strpos($filename,'.'), strlen($filename)-1);
						
						//$targetFile =  str_replace('//','/',$targetPath) . $_FILES[$fileElementName]['name'];
						$target_filename = random_filename($targetPath, $extension, 6);
						$photo_filename = $now_dir . '/' . $target_filename;
						
						$targetFile = $targetPath . $target_filename;
						
						move_uploaded_file($tempFile,$targetFile);
					}else{
						$error = "Can't create directory";
					}
				}
			}
			
            $driver_filename = "";
	        $fileElementName = 'driverfile';
	        $error = "";
	        if(empty($_FILES[$fileElementName]['tmp_name']) || $_FILES[$fileElementName]['tmp_name'] == 'none')
			{}else{
				if(!empty($_FILES[$fileElementName]['error']))
				{
					switch($_FILES[$fileElementName]['error'])
					{
						case '1':
							$error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
							break;
						case '2':
							$error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
							break;
						case '3':
							$error = 'The uploaded file was only partially uploaded';
							break;
						case '4':
							$error = 'No file was uploaded.';
							break;
			
						case '6':
							$error = 'Missing a temporary folder';
							break;
						case '7':
							$error = 'Failed to write file to disk';
							break;
						case '8':
							$error = 'File upload stopped by extension';
							break;
						case '999':
						default:
							$error = 'No error code avaiable';
					}
				}else 
				{
					$tempFile = $_FILES[$fileElementName]['tmp_name'];
					
					$now_dir = date('Y/m/d');
					$targetPath = str_replace('//','/',DIR_DRIVER_LICENSE . $now_dir . '/');
					
					if(mkdirs($targetPath)){
						$filename = $_FILES[$fileElementName]['name'];
						
						// Get the extension from the filename.
						$extension = substr($filename, strpos($filename,'.'), strlen($filename)-1);
						
						//$targetFile =  str_replace('//','/',$targetPath) . $_FILES[$fileElementName]['name'];
						$target_filename = random_filename($targetPath, $extension, 6);
						$driver_filename = $now_dir . '/' . $target_filename;
						
						$targetFile = $targetPath . $target_filename;
						
						//TODO save driver in different name in driver dir
						log_message("debug", $targetFile);
						move_uploaded_file($tempFile,$targetFile);
					}else{
						$error = "Can't create directory";
					}
				}
			}

        	$passport_filename = "";
	        $fileElementName = 'passportfile';
	        if(empty($_FILES[$fileElementName]['tmp_name']) || $_FILES[$fileElementName]['tmp_name'] == 'none')
			{}else{
				if(!empty($_FILES[$fileElementName]['error']))
				{
					switch($_FILES[$fileElementName]['error'])
					{
						case '1':
							$error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
							break;
						case '2':
							$error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
							break;
						case '3':
							$error = 'The uploaded file was only partially uploaded';
							break;
						case '4':
							$error = 'No file was uploaded.';
							break;
			
						case '6':
							$error = 'Missing a temporary folder';
							break;
						case '7':
							$error = 'Failed to write file to disk';
							break;
						case '8':
							$error = 'File upload stopped by extension';
							break;
						case '999':
						default:
							$error = 'No error code avaiable';
					}
				}else 
				{
					$tempFile = $_FILES[$fileElementName]['tmp_name'];
					
					$now_dir = date('Y/m/d');
					$targetPath = str_replace('//','/',DIR_PASSPORT_LICENSE . $now_dir . '/');
					
					if(mkdirs($targetPath)){
						$filename = $_FILES[$fileElementName]['name'];
						
						// Get the extension from the filename.
						$extension = substr($filename, strpos($filename,'.'), strlen($filename)-1);
						
						//$targetFile =  str_replace('//','/',$targetPath) . $_FILES[$fileElementName]['name'];
						$target_filename = random_filename($targetPath, $extension, 6);
						$passport_filename = $now_dir . '/' . $target_filename;
						
						$targetFile = $targetPath . $target_filename;
						
						log_message("debug", $targetFile);
						move_uploaded_file($tempFile,$targetFile);
					}else{
						$error = "Can't create directory";
					}
				}
			}
			$now = now();
			if($error != ""){
				echo ' {"message":"'.$error.'", "status":"err"}';
			}else{
				$userinfo_data = array(
					'actatek_id' => $actatek_id,
	                'user_name' => $user_name,
	                'password' => $password,
	                'first_name' => $first_name,
	                'last_name' => $last_name,
					'sex' => $sex,
	                'department_id' => $department_id,
	                'user_level_id' => $user_level_id,
	                'employment_type_id' => $employment_type_id,
					'residency_status' => $residency_status,
					'hire_date' 	=> strtotime($hire_date),
					'terminate_date' 	=> strtotime($terminate_date),
					'review_date' 	=> strtotime($review_date),
	                'address' => $address,
	                'home_phone' => $home_phone,
	                'mobile' => $mobile,
	                'email' => $email,
					'personal_email' => $personal_email,
	                'birthday' => $birthday,
	                'kin_name' => $kinname,
	                'kin_contact' => $contact,
	                'medical_conditions' => $medical,
	                'driver_license_no' => $driverno,
					'passport_license_no' => $passportno,
	                'tfn' => $tfn,
	                'bank_account_name' => $accountname,
	                'bank_name' => $bankname,
	                'bank_bsb' => $bsb,
	                'bank_account_no' => $accountno,
					'status' => ACTIVE,
					'add_time' => $now
	            );
				if(!empty($photo_filename)){
	            	$a_photo = array(
	            		'photo_file' => $photo_filename
	            	);
	            	$userinfo_data = array_merge($userinfo_data, $a_photo);
	            }
	            if(!empty($driver_filename)){
	            	$a_driver = array(
	            		'driver_license_file' => $driver_filename
	            	);
	            	$userinfo_data = array_merge($userinfo_data, $a_driver);
	            }
				if(!empty($passport_filename)){
	            	$a_passport = array(
	            		'passport_license_file' => $passport_filename
	            	);
	            	$userinfo_data = array_merge($userinfo_data, $a_passport);
	            }
	            
	            // Check if username already exists
				$user_row = $this->users_m->get_by_user_name($this->input->post('user_name'));
				if ($user_row){
					$error = "Username already exists";
				} else{
					$insert_id = $this->users_m->insert($userinfo_data);
					
		            if($insert_id > 0){
		            	//record ADD log
	            		$userinfo_log = array(
	            				'operator' => $this->session->userdata('user_name'),
	            				'operation'=> OP_ADD,
	            				'operate_time' => $now
	            			);
	            		$userinfo_log = array_merge($userinfo_log, $userinfo_data);
	            		$this->load->model('St_users_log_m', 'users_log_m');            		
	            		$this->users_log_m->insert($userinfo_log);
		            }else{
		            	$error = "User info save failed.";
		            }
	            }
	            
	            //return json 
	            if($error != ""){
					echo ' {"message":"'.$error.'", "status":"fail"}';	
				}else{
	            	echo ' {"message":"User info save successfully.", "status":"ok"}';	
	            }
			}
        }
    }
    
    /**
     * update user info by ajax
     * @return json object
     */
	function edit_ajax() {
		$error = "";
        $this->form_validation->set_rules('user_name', 'Username', 'required|max_length[20]|min_length[2]');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[20]|min_length[3]');
        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        $this->form_validation->set_rules('sex', 'Sex', 'required');
        $this->form_validation->set_rules('hire_date', 'Hire Date', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('home_phone', 'Home Phone', 'required');
        $this->form_validation->set_rules('mobile', 'Mobile', 'required');
        $this->form_validation->set_rules('email', 'Email(work)', 'required|valid_email');
        $this->form_validation->set_rules('personal_email', 'Email(personal)', 'required|valid_email');
        
        $this->form_validation->set_rules('bank_account_name', 'Account Name', 'required');
        $this->form_validation->set_rules('bank_name', 'Bank Name', 'required');
        $this->form_validation->set_rules('bank_bsb', 'BSB', 'required');
        $this->form_validation->set_rules('bank_account_no', 'Account No', 'required');
        
        log_message("debug", "edit ajax === " . $this->input->post('user_name'));
        if ($this->form_validation->run() == FALSE) {
        	log_message("debug", "edit ajax form_validation false == " . $this->input->post('user_name'));
            $this->edit($this->input->post('user_name'));
        }
        else {
        	$id = $this->input->post('id');
        	$actatek_id = $this->input->post('actatek_id');
            $user_name = $this->input->post('user_name');
            $password = $this->input->post('password');
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $sex = $this->input->post('sex');
            $department_id = $this->input->post('department_id');
            $user_level_id = $this->input->post('user_level_id');
            $employment_type_id = $this->input->post('employment_type_id');
            $residency_status = $this->input->post('residency_status');
            $hire_date = $this->input->post('hire_date');
            $terminate_date = $this->input->post('terminate_date');
            $review_date = $this->input->post('review_date');
            $address = $this->input->post('address');
            $home_phone = $this->input->post('home_phone');
            $mobile = $this->input->post('mobile');
            $email = $this->input->post('email');
            $personal_email = $this->input->post('personal_email');
        	$birthday = $this->input->post('birth_date');
            if(!empty($birthday)){
	        	$birthday = strtotime($birthday);
            }else{
            	$birthday = 0;
            }
            $kinname = $this->input->post('kin_name');
            $contact = $this->input->post('kin_contact');
            $medical = $this->input->post('medical_conditions');
            $driverno = $this->input->post('driver_license_no');
            $passportno = $this->input->post('passport_license_no');
            $tfn = $this->input->post('tfn');
            $accountname = $this->input->post('bank_account_name');
            $bankname = $this->input->post('bank_name');
            $bsb = $this->input->post('bank_bsb');
            $accountno = $this->input->post('bank_account_no');
	        
        $photo_filename = "";
	        $fileElementName = 'photo_file';
	        $error = "";
	        if(empty($_FILES[$fileElementName]['tmp_name']) || $_FILES[$fileElementName]['tmp_name'] == 'none')
			{}else{
				if(!empty($_FILES[$fileElementName]['error']))
				{
					switch($_FILES[$fileElementName]['error'])
					{
						case '1':
							$error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
							break;
						case '2':
							$error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
							break;
						case '3':
							$error = 'The uploaded file was only partially uploaded';
							break;
						case '4':
							$error = 'No file was uploaded.';
							break;
			
						case '6':
							$error = 'Missing a temporary folder';
							break;
						case '7':
							$error = 'Failed to write file to disk';
							break;
						case '8':
							$error = 'File upload stopped by extension';
							break;
						case '999':
						default:
							$error = 'No error code avaiable';
					}
				}else 
				{
					$tempFile = $_FILES[$fileElementName]['tmp_name'];
					
					$now_dir = date('Y/m/d');
					$targetPath = str_replace('//','/',DIR_PHOTO . $now_dir . '/');
					
					if(mkdirs($targetPath)){
						$filename = $_FILES[$fileElementName]['name'];
						
						// Get the extension from the filename.
						$extension = substr($filename, strpos($filename,'.'), strlen($filename)-1);
						
						//$targetFile =  str_replace('//','/',$targetPath) . $_FILES[$fileElementName]['name'];
						$target_filename = random_filename($targetPath, $extension, 6);
						$photo_filename = $now_dir . '/' . $target_filename;
						
						$targetFile = $targetPath . $target_filename;
						
						move_uploaded_file($tempFile,$targetFile);
					}else{
						$error = "Can't create directory";
					}
				}
			}
			
	        $driver_filename = "";
	        $fileElementName = 'driverfile';
	        if(empty($_FILES[$fileElementName]['tmp_name']) || $_FILES[$fileElementName]['tmp_name'] == 'none')
			{}else{
				if(!empty($_FILES[$fileElementName]['error']))
				{
					switch($_FILES[$fileElementName]['error'])
					{
						case '1':
							$error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
							break;
						case '2':
							$error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
							break;
						case '3':
							$error = 'The uploaded file was only partially uploaded';
							break;
						case '4':
							$error = 'No file was uploaded.';
							break;
			
						case '6':
							$error = 'Missing a temporary folder';
							break;
						case '7':
							$error = 'Failed to write file to disk';
							break;
						case '8':
							$error = 'File upload stopped by extension';
							break;
						case '999':
						default:
							$error = 'No error code avaiable';
					}
				}else 
				{
					$tempFile = $_FILES[$fileElementName]['tmp_name'];
					
					$now_dir = date('Y/m/d');
					$targetPath = str_replace('//','/',DIR_DRIVER_LICENSE . $now_dir . '/');
					
					if(mkdirs($targetPath)){
						$filename = $_FILES[$fileElementName]['name'];
						
						// Get the extension from the filename.
						$extension = substr($filename, strpos($filename,'.'), strlen($filename)-1);
						
						$target_filename = random_filename($targetPath, $extension, 6);
						$driver_filename = $now_dir . '/' . $target_filename;
						
						$targetFile = $targetPath . $target_filename;
						
						log_message("debug", $targetFile);
						move_uploaded_file($tempFile,$targetFile);
					}else{
						$error = "Can't create directory";
					}
				}
			}
			
        	$passport_filename = "";
	        $fileElementName = 'passportfile';
	        if(empty($_FILES[$fileElementName]['tmp_name']) || $_FILES[$fileElementName]['tmp_name'] == 'none')
			{}else{
				if(!empty($_FILES[$fileElementName]['error']))
				{
					switch($_FILES[$fileElementName]['error'])
					{
						case '1':
							$error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
							break;
						case '2':
							$error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
							break;
						case '3':
							$error = 'The uploaded file was only partially uploaded';
							break;
						case '4':
							$error = 'No file was uploaded.';
							break;
			
						case '6':
							$error = 'Missing a temporary folder';
							break;
						case '7':
							$error = 'Failed to write file to disk';
							break;
						case '8':
							$error = 'File upload stopped by extension';
							break;
						case '999':
						default:
							$error = 'No error code avaiable';
					}
				}else 
				{
					$tempFile = $_FILES[$fileElementName]['tmp_name'];
					
					$now_dir = date('Y/m/d');
					$targetPath = str_replace('//','/',DIR_PASSPORT_LICENSE . $now_dir . '/');
					
					if(mkdirs($targetPath)){
						$filename = $_FILES[$fileElementName]['name'];
						
						// Get the extension from the filename.
						$extension = substr($filename, strpos($filename,'.'), strlen($filename)-1);
						
						$target_filename = random_filename($targetPath, $extension, 6);
						$passport_filename = $now_dir . '/' . $target_filename;
						
						$targetFile = $targetPath . $target_filename;
						
						log_message("debug", $targetFile);
						move_uploaded_file($tempFile,$targetFile);
					}else{
						$error = "Can't create directory";
					}
				}
			}

			if($error != ""){
				echo ' {"message":"'.$error.'", "status":"err"}';
			}else{
				$userinfo_data = array(
					'actatek_id' 	=> $actatek_id,
	                'user_name' 	=> $user_name,
	                'password' 		=> $password,
	                'first_name' 	=> $first_name,
	                'last_name' 	=> $last_name,
					'sex' 			=> $sex,
	                'department_id' => $department_id,
	                'user_level_id' => $user_level_id,
	                'employment_type_id' => $employment_type_id,
					'residency_status' => $residency_status,
					'hire_date' 	=> strtotime($hire_date),
					'terminate_date' => strtotime($terminate_date),
					'review_date' 	=> strtotime($review_date),
	                'address' 		=> $address,
	                'home_phone' 	=> $home_phone,
	                'mobile' 		=> $mobile,
	                'email' 		=> $email,
					'personal_email' =>$personal_email,
	                'birthday' 		=> $birthday,
	                'kin_name' 		=> $kinname,
	                'kin_contact' 	=> $contact,
	                'medical_conditions' => $medical,
	                'driver_license_no' => $driverno,
					'passport_license_no' => $passportno,
	                'tfn' 			=> $tfn,
	                'bank_account_name' => $accountname,
	                'bank_name' 	=> $bankname,
	                'bank_bsb' 		=> $bsb,
	                'bank_account_no' => $accountno
	            );
	            
				if(!empty($photo_filename)){
	            	$a_photo = array(
	            		'photo_file' => $photo_filename
	            	);
	            	$userinfo_data = array_merge($userinfo_data, $a_photo);
	            }
	            if(!empty($driver_filename)){
	            	$a_driver = array(
	            		'driver_license_file' => $driver_filename
	            	);
	            	$userinfo_data = array_merge($userinfo_data, $a_driver);
	            }
	            
				if(!empty($passport_filename)){
	            	$a_driver = array(
	            		'passport_license_file' => $passport_filename
	            	);
	            	$userinfo_data = array_merge($userinfo_data, $a_driver);
	            }
	            
	            //check whether current user name has been added
//				$user_row = $this->users_m->get_by_user_name($this->input->post('user_name'));
				$user_row = $this->users_m->get_by_id($this->input->post('id'));
            	$b_update = $this->users_m->update($user_row->id, $userinfo_data);
            	if($b_update){
            		//record update log
            		$userinfo_log = array(
            				'user_name' => $this->input->post('user_name'),
            				'operator' => $this->session->userdata('user_name'),
            				'operation'=> OP_UPDATE,
            				'operate_time' => now()
            			);
            		
            		$userinfo_log = array_merge($userinfo_log, $userinfo_data);
            		$this->load->model('St_users_log_m', 'users_log_m');            		
            		$this->users_log_m->insert($userinfo_log);
            	}else{
            		$error = "User info save failed.";
            	}
	            log_message("debug", "update user info ");
	            //return json 
	            if($error != ""){
					echo ' {"message":"'.$error.'", "status":"fail"}';	
				}else{
	            	echo ' {"message":"User info save successfully.", "status":"ok"}';	
	            }
			}
        }
    }

    /*
     * list the user's change history
     * @param $user_name
     * @return vew
     */
    function history($user_name){
    	$data['navigation'] = "User History";
    	$data['userinfo'] = $this->users_m->get_by("user_name",$user_name);
    	//read history log
    	$this->load->model('St_users_log_m', 'users_log_m');
    	$data['user_logs'] = $this->users_log_m->history($user_name);
    	$this->load->view('user/user_history', $data);
    }
    
    /**
     * save full time labor contract
     * @return json object
     */
	function save_fulltime_ajax(){
		$this->form_validation->set_rules('entry_date', 'Entry date', 'required');

		$user_name = $this->input->post('user_name');
		if ($this->form_validation->run() == FALSE) {
            $this->edit($this->input->post('user_name'));
        }

        $error = "";
        $this->load->model('St_user_labor_contract_m', 'labor_contract_m');
        
        $employment_type_id = $this->input->post('user_type_fulltime');
        $entry_date = $this->input->post('entry_date');
        $m_start_time = $this->input->post('m_start_time');
        $m_end_time = $this->input->post('m_end_time');
        $s_start_time = $this->input->post('s_start_time');
        $s_end_time = $this->input->post('s_end_time');
        
        $fulltime_data = array(
                'user_name' 	=> $user_name,
        		'employment_type_id' 			=> $employment_type_id,
                'entry_date' 	=> strtotime($entry_date),
                'm_start_time' 	=> $m_start_time,
                'm_end_time' 	=> $m_end_time,
                's_start_time' 	=> $s_start_time,
                's_end_time' 	=> $s_end_time,
        		'add_time'		=> now(),
				'status' 		=> ACTIVE
        );
        
		$labor_contract_file = "";
        $fileElementName = 'contract_file';
        if(empty($_FILES[$fileElementName]['tmp_name']) || $_FILES[$fileElementName]['tmp_name'] == 'none')
		{}else{
			if(!empty($_FILES[$fileElementName]['error']))
			{
				switch($_FILES[$fileElementName]['error'])
				{
					case '1':
						$error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
						break;
					case '2':
						$error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
						break;
					case '3':
						$error = 'The uploaded file was only partially uploaded';
						break;
					case '4':
						$error = 'No file was uploaded.';
						break;
		
					case '6':
						$error = 'Missing a temporary folder';
						break;
					case '7':
						$error = 'Failed to write file to disk';
						break;
					case '8':
						$error = 'File upload stopped by extension';
						break;
					case '999':
					default:
						$error = 'No error code avaiable';
				}
			}else 
			{
				$tempFile = $_FILES[$fileElementName]['tmp_name'];
				
				$now_dir = date('Y/m/d');
				$targetPath = str_replace('//','/',DIR_LABOR_CONTRACTOR . $now_dir . '/');
				
				if(mkdirs($targetPath)){
					$filename = $_FILES[$fileElementName]['name'];
					
					// Get the extension from the filename.
					$extension = substr($filename, strpos($filename,'.'), strlen($filename)-1);
					
					//$targetFile =  str_replace('//','/',$targetPath) . $_FILES[$fileElementName]['name'];
					$target_filename = random_filename($targetPath, $extension, 6);
					$labor_contract_file = $now_dir . '/' . $target_filename;
					
					$targetFile = $targetPath . $target_filename;
					
					log_message("debug", $targetFile);
					move_uploaded_file($tempFile,$targetFile);
				}else{
					$error = "Can't create directory";
				}
			}
		}
		
		if(!empty($labor_contract_file)){
            $a_file = array(
        	   	'labor_contract_file' => $labor_contract_file
           	);
       		$fulltime_data = array_merge($fulltime_data, $a_file);
        }
	            
		$insert_id = $this->labor_contract_m->insert($fulltime_data);
        if($insert_id <= 0){
            $error = "Save labor contract failed.";
        }
        
        log_message("debug", "save full time .....");
		if(!empty($error)){
			echo ' {"message":"'.$error.'", "status":"fail"}';	
		}else{
            echo ' {"message":"Labor contract save successfully.", "status":"ok"}';	
        }
    }
    
    /**
     * save full time labor contract
     * @return json ojbect
     */
	function save_othertime_ajax(){
		$this->form_validation->set_rules('from_date', 'Entry date', 'required');
		$this->form_validation->set_rules('expire_date', 'Expire date', 'required');

		$user_name = $this->input->post('user_name');
		if ($this->form_validation->run() == FALSE) {
            $this->edit($this->input->post('user_name'));
        }

        $error = "";
        $this->load->model('St_user_labor_contract_m', 'labor_contract_m');
        
        $employment_type_id = $this->input->post('user_type_othertime');
        $entry_date = $this->input->post('from_date');
        $expire_date = $this->input->post('expire_date');
        $monday_start_time = $this->input->post('monday_start_time');
        $monday_end_time = $this->input->post('monday_end_time');
        $tuesday_start_time = $this->input->post('tuesday_start_time');
        $tuesday_end_time = $this->input->post('tuesday_end_time');
        $wednesday_start_time = $this->input->post('wednesday_start_time');
        $wednesday_end_time = $this->input->post('wednesday_end_time');
        $thursday_start_time = $this->input->post('thursday_start_time');
        $thursday_end_time = $this->input->post('thursday_end_time');
		$friday_start_time = $this->input->post('friday_start_time');
        $friday_end_time = $this->input->post('friday_end_time');
        $saturday_start_time = $this->input->post('saturday_start_time');
        $saturday_end_time = $this->input->post('saturday_end_time');
        $sunday_start_time = $this->input->post('sunday_start_time');
        $sunday_end_time = $this->input->post('sunday_end_time');
        
        $othertime_data = array(
                'user_name' 			=> $user_name,
        		'employment_type_id' 			=> $employment_type_id,
                'entry_date' 			=> strtotime($entry_date),
        		'expire_date' 			=> strtotime($expire_date),
                'monday_start_time' 	=> $monday_start_time,
                'monday_end_time' 		=> $monday_end_time,
                'tuesday_start_time' 	=> $tuesday_start_time,
                'tuesday_end_time' 		=> $tuesday_end_time,
        		'wednesday_start_time' 	=> $wednesday_start_time,
                'wednesday_end_time' 	=> $wednesday_end_time,
        		'thursday_start_time' 	=> $thursday_start_time,
                'thursday_end_time' 	=> $thursday_end_time,
                'friday_start_time' 	=> $friday_start_time,
        		'friday_end_time' 		=> $friday_end_time,
                'saturday_start_time' 	=> $saturday_start_time,
        		'saturday_end_time' 	=> $saturday_end_time,
                'sunday_start_time' 	=> $sunday_start_time,
        		'sunday_end_time' 		=> $sunday_end_time,
        		'add_time'				=> now(),
				'status' 				=> ACTIVE
        );
        
		$labor_contract_file = "";
        $fileElementName = 'contract_file';
        if(empty($_FILES[$fileElementName]['tmp_name']) || $_FILES[$fileElementName]['tmp_name'] == 'none')
		{}else{
			if(!empty($_FILES[$fileElementName]['error']))
			{
				switch($_FILES[$fileElementName]['error'])
				{
					case '1':
						$error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
						break;
					case '2':
						$error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
						break;
					case '3':
						$error = 'The uploaded file was only partially uploaded';
						break;
					case '4':
						$error = 'No file was uploaded.';
						break;
		
					case '6':
						$error = 'Missing a temporary folder';
						break;
					case '7':
						$error = 'Failed to write file to disk';
						break;
					case '8':
						$error = 'File upload stopped by extension';
						break;
					case '999':
					default:
						$error = 'No error code avaiable';
				}
			}else 
			{
				$tempFile = $_FILES[$fileElementName]['tmp_name'];
				
				$now_dir = date('Y/m/d');
				$targetPath = str_replace('//','/',DIR_LABOR_CONTRACTOR . $now_dir . '/');
				
				if(mkdirs($targetPath)){
					$filename = $_FILES[$fileElementName]['name'];
					
					// Get the extension from the filename.
					$extension = substr($filename, strpos($filename,'.'), strlen($filename)-1);
					
					//$targetFile =  str_replace('//','/',$targetPath) . $_FILES[$fileElementName]['name'];
					$target_filename = random_filename($targetPath, $extension, 6);
					$labor_contract_file = $now_dir . '/' . $target_filename;
					
					$targetFile = $targetPath . $target_filename;
					
					log_message("debug", $targetFile);
					move_uploaded_file($tempFile,$targetFile);
				}else{
					$error = "Can't create directory";
				}
			}
		}
		
		if(!empty($labor_contract_file)){
            $a_file = array(
        	   	'labor_contract_file' => $labor_contract_file
           	);
       		$othertime_data = array_merge($othertime_data, $a_file);
        }

		$insert_id = $this->labor_contract_m->insert($othertime_data);
        if($insert_id <= 0){
            $error = "Labor contract save failed. Please check.";
        }
		if(empty($error)){
			echo json_encode(
				array(
					'status' => 'ok',
					'message'=> "Labor contract save successfully."
				)
			);
		}else{
			echo json_encode(
				array(
					'status' => 'fail',
					'message'=> $error
				)
			);
        }
    }
    
    function save_labor_ajax(){
		$this->form_validation->set_rules('entry_date', 'Entry date', 'required');
		$this->form_validation->set_rules('payrollshift_id', 'Payroll', 'required');
		$this->form_validation->set_rules('hourly_rate', 'Hourly Rate', 'required');

		$user_id = $this->input->post('user_id');
		if ($this->form_validation->run() == FALSE) {
            $this->edit($this->input->post('user_id'));
        }

        $error = "";
        $this->load->model('St_user_labor_m', 'user_labor_m');
        
        $entry_date = $this->input->post('entry_date');
        $payrollshift_id = $this->input->post('payrollshift_id');
        $hourly_rate = $this->input->post('hourly_rate');
        
        $fulltime_data = array(
                'user_id' 	=> $user_id,
                'entry_date' 	=> strtotime($entry_date),
                'payrollshift_id' 	=> $payrollshift_id,
                'hourly_rate' 	=> $hourly_rate,
        		'add_time'		=> now(),
				'status' 		=> ACTIVE
        );
        
        log_message("debug", "payrollshift_id===".$payrollshift_id);
		$labor_contract_file = "";
        $fileElementName = 'contract_file';
        if(empty($_FILES[$fileElementName]['tmp_name']) || $_FILES[$fileElementName]['tmp_name'] == 'none')
		{}else{
			if(!empty($_FILES[$fileElementName]['error']))
			{
				switch($_FILES[$fileElementName]['error'])
				{
					case '1':
						$error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
						break;
					case '2':
						$error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
						break;
					case '3':
						$error = 'The uploaded file was only partially uploaded';
						break;
					case '4':
						$error = 'No file was uploaded.';
						break;
		
					case '6':
						$error = 'Missing a temporary folder';
						break;
					case '7':
						$error = 'Failed to write file to disk';
						break;
					case '8':
						$error = 'File upload stopped by extension';
						break;
					case '999':
					default:
						$error = 'No error code avaiable';
				}
			}else 
			{
				$tempFile = $_FILES[$fileElementName]['tmp_name'];
				
				$now_dir = date('Y/m/d');
				$targetPath = str_replace('//','/',DIR_LABOR_CONTRACTOR . $now_dir . '/');
				
				if(mkdirs($targetPath)){
					$filename = $_FILES[$fileElementName]['name'];
					
					// Get the extension from the filename.
					$extension = substr($filename, strpos($filename,'.'), strlen($filename)-1);
					
					//$targetFile =  str_replace('//','/',$targetPath) . $_FILES[$fileElementName]['name'];
					$target_filename = random_filename($targetPath, $extension, 6);
					$labor_contract_file = $now_dir . '/' . $target_filename;
					
					$targetFile = $targetPath . $target_filename;
					
					log_message("debug", $targetFile);
					move_uploaded_file($tempFile,$targetFile);
				}else{
					$error = "Can't create directory";
				}
			}
		}
		
		if(!empty($labor_contract_file)){
            $a_file = array(
        	   	'labor_contract_file' => $labor_contract_file
           	);
       		$fulltime_data = array_merge($fulltime_data, $a_file);
        }
	            
		$insert_id = $this->user_labor_m->insert($fulltime_data);
        if($insert_id <= 0){
            $error = "Save labor contract failed.";
        }
        
        log_message("debug", "save save_labor_ajax .....");
		if(!empty($error)){
			echo ' {"message":"'.$error.'", "status":"fail"}';	
		}else{
            echo ' {"message":"Labor contract save successfully.", "status":"ok"}';	
        }
    }
    
    /**
     * edit working hours page
     * @param $user_name
     * @return view
     */
    function working_hours($user_name){
    	$data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='". site_url('user') ."'>User</a> > Edit working hours";
    	$userinfo = $this->users_m->get_by("user_name",$user_name);
    	if(empty($userinfo)){
    		show_404();
    	}
    	
    	$data['userinfo'] = $userinfo;
    	//load working hours model
    	$this->load->model('St_working_hours_m', 'working_hours_m');
    	
    	//get config leave hour parameter
    	$this->load->model('St_config_m', 'config_m');
    	$annual_row = $this->config_m->get_by_code('Annual_Leave_Parameter');
    	$data['annual_Leave_Parameter'] = $annual_row->value;
    	$personal_row = $this->config_m->get_by_code('Personal_Leave_Parameter');
    	$data['personal_Leave_Parameter'] = $personal_row->value;
    	    	
    	//get initialization for the user
    	//$data['init_working_hour']  = $this->working_hours_m->get_init_working_hours($user_name);
    	$is_init = false;
    	$init_working_hours = $this->working_hours_m->get_init_working_hours($user_name);
    	if(empty($init_working_hours)){
    		$is_init = true;
    	}
		$data['working_hourses'] = $this->working_hours_m->get_by_user_name($user_name);
		$data['user_name'] = $user_name;
		$data['is_init'] = $is_init;
    	$this->load->view('navigation', $data);
    	$this->load->view('user/edit_working_hours', $data);
    }
    
    function list_working_hours($user_name){
    	$data[''] = "";
    	$this->load->view('user/list_working_hours', $data);
    }
    /**
     * save full time labor contract
     * @return json ojbect
     */
	function save_working_hours_ajax(){
		$error = "";
		$user_name = $this->input->post('user_name');
        $working_hours 		= $this->input->post('working_hours');
        
        if(!empty($working_hours)){
	        $this->load->model('St_working_hours_m', 'working_hours_m');        
	        
	        $is_init = $this->input->post('is_init');
	        $annual_hours_used = 0;
	        $personal_hours_used = 0;
	        $accrual_personal_hours = 0;
	        $accrual_annual_hours 	= $this->input->post('accrual_annual_hours');
	        $accrual_personal_hours= $this->input->post('accrual_personal_hours');
	        if(empty($accrual_annual_hours)){
	        	$accrual_annual_hours = 0;
	        }
        	if(empty($accrual_personal_hours)){
	        	$accrual_personal_hours = 0;
	        }
	        
	        if($is_init){
				$annual_hours_used 	= $this->input->post('annual_hours_used');
	        	$personal_hours_used= $this->input->post('personal_hours_used');
	        }
	        $a_working_hours = array(
	        	'user_name' 			=> $user_name,
	        	'working_hours' 		=> $working_hours,
	        	'annual_hours_used' 	=> $annual_hours_used,
	        	'personal_hours_used'	=> $personal_hours_used,
	        	'accrual_annual_hours' 	=> $accrual_annual_hours,
	        	'accrual_personal_hours'=> $accrual_personal_hours,
	        	'is_init' 				=> $is_init,
	        	'status'				=> ACTIVE,
	        	'add_time'				=> now()
	        );
	        $insert_id = $this->working_hours_m->insert($a_working_hours);
	        if($insert_id <= 0){
	        	$error = "Save working hours failed.";
	        }
        }else{
        	$error = "Save working hours failed.";
        }
        
        $data['user_name'] = $user_name;
		if(!empty($error)){
			echo json_encode(array("status" => "err", "message" => $error));
		}else{
			$data['working_hourses'] = $this->working_hours_m->get_by_user_name($user_name);
			$content = $this->load->view('user/list_working_hours_data', $data, true);
			echo json_encode(
				array(
					"status"	=> "ok",
					"message"	=> "Working hours save successfully.",
					"content"	=> $content
				)
			);
        }
	}
	
	function save_last_working_hours_ajax(){
		$error = "";
		
		$user_name = $this->input->post('user_name');
        
        $working_hours 		= $this->input->post('last_working_hours');
        
        $this->load->model('St_working_hours_m', 'working_hours_m');        
        
        $a_working_hours = array(
        	'user_name' 			=> $user_name,
        	'working_hours' 		=> $working_hours,
        	'status'				=> ACTIVE,
        	'add_time'				=> now()
        );
        $insert_id = $this->working_hours_m->insert($a_working_hours);
        if($insert_id <= 0){
        	$error = "Save working hours failed.";
        }
        
		if(!empty($error)){
			echo json_encode(array("status" => "err", "message" => $error));
		}else{
			$data['user_name'] = $user_name;
			$data['working_hourses'] = $this->working_hours_m->get_by_user_name($user_name);
			$content = $this->load->view('user/list_working_hours_data', $data, true);
			echo json_encode(
				array(
					"status"	=> "ok",
					"message"	=> "Working hours save successfully.",
					"content"	=> $content
				)
			);
        }
	}
	
	/**
	 * remove working hours record
	 * @param $user_name
	 * @param $id
	 * @return view
	 */
	function remove_working($user_name,$id){
		$this->load->model('St_working_hours_m', 'working_hours_m');
		$this->working_hours_m->set_inactive($id);
		redirect('user/working_hours/'.$user_name);
	}
	
    function labors($user_id){
    	$this->load->model('St_user_labor_m', 'user_labor_m');
    	$data['labor_list'] = $this->user_labor_m->get_active($user_id);
        $this->load->view('user/list_labors', $data);
    }
    
//    function labors($user_name){
//    	$this->load->model('St_user_labor_contract_m', 'labor_contract_m');
//    	$data['labor_list'] = $this->labor_contract_m->get_active($user_name);
//        $this->load->view('user/list_labor_contracts', $data);
//    }
    function remove_labor_ajax(){
    	$this->load->model('St_user_labor_m', 'user_labor_m');
    	
    	$labor_id = $this->input->post('id');
    	
    	log_message("debug", "remove_labor_ajax ..." . $labor_id);
    	
    	$data['status'] = "err";
		if(is_null($labor_id)){
			$data['message'] = "Invalid data";
		}else{
			$b_update = $this->user_labor_m->set_inactive($labor_id);
			$data['status'] = "ok";
			$data['message'] = "Remove successfully!";
		}
		echo json_encode($data);
    }
    
    function remove_labor_ajaxold(){
    	$this->load->model('St_user_labor_contract_m', 'labor_contract_m');
    	
    	$labor_id = $this->input->post('id');
    	
    	log_message("debug", "remove_labor_ajax ..." . $labor_id);
    	
    	$data['status'] = "err";
		if(is_null($labor_id)){
			$data['message'] = "Invalid data";
		}else{
			$labor = $this->labor_contract_m->get($labor_id);
			if(empty($labor)){
				$data['message'] = "Invalid data";
			}else{
				//remove
				$b_update = $this->labor_contract_m->set_inactive($labor_id);
				if($b_update == 1){
					//add hisotry log
	            	$this->load->model('St_user_labor_contract_log_m', 'labor_contract_log_m');
	            	$log = array(
	            		'id' => $labor_id,
	            		'operator' => $this->session->userdata('user_name'),
	            		'operation' => 'remove',
	            		'operate_time' => now()
	            	);
	            	$this->labor_contract_log_m->insert($log);
            	
					$data['status'] = "ok";
					$data['message'] = "Remove successfully!";
				}else{
					$data['message'] = "Remove failed!";
				}
			}
		}
		echo json_encode($data);
    }
    
    function index_fulltime() {
        $data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='". site_url('user') ."'>User</a> > Add New > Working Time";
        $this->load->view('navigation', $data);
        $this->load->view('add_user_fulltime');
    }

    function index_othertime() {
        $data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='". site_url('user') ."'>User</a> > Add New > Working Time";
        $this->load->view('navigation', $data);
        $this->load->view('add_user_othertime');
    }
    function save_workingtime() {
        $user_name = $this->session->userdata('user_name');
        $password = $this->session->userdata('password');
        $first_name = $this->session->userdata('first_name');
        $last_name = $this->session->userdata('last_name');
        $department_id = $this->session->userdata('department_id');
        $user_level_id = $this->session->userdata('user_level_id');
        $user_type = $this->session->userdata('user_type');
        $address = $this->session->userdata('address');
        $home_phone = $this->session->userdata('home_phone');
        $mobile = $this->session->userdata('mobile');
        $email = $this->session->userdata('email');
        $birthday = $this->session->userdata('birthday');
        $kinname = $this->session->userdata('kinname');
        $contact = $this->session->userdata('contact');
        $medical = $this->session->userdata('medical');
        $driverno = $this->session->userdata('driverno');
        $tfn = $this->session->userdata('tfn');
        $accountname = $this->session->userdata('accountname');
        $bankname = $this->session->userdata('bankname');
        $bsb = $this->session->userdata('bsb');
        $accountno = $this->session->userdata('accountno');

        if ($user_type == "Full Time") {
            $this->form_validation->set_rules('entrydate', 'Entry Date', 'required');

            if ($this->form_validation->run() == FALSE) {
                $this->index_fulltime();
            }
            else {
                $entrydate = $this->input->post('entrydate');
                $mstarttime = $this->input->post('mstarttime');
                $mendtime = $this->input->post('mendtime');
                $sstarttime = $this->input->post('sstarttime');
                $sendtime = $this->input->post('sendtime');

                $this->users_m->insert($user_name, $password, $first_name, $last_name, $address, $home_phone, $mobile, $email,
                    $birthday, $kinname, $contact, $medical, $driverno, $tfn, $accountname, $bankname,
                    $bsb, $accountno, $department_id, $user_level_id, $user_type);

                $this->users_m->insert_contact($user_name, $entrydate, '');

                $this->users_m->insert_workingtime($user_name, 'Monday', $mstarttime, $mendtime);
                $this->users_m->insert_workingtime($user_name, 'Tuesday', $mstarttime, $mendtime);
                $this->users_m->insert_workingtime($user_name, 'Wednesday', $mstarttime, $mendtime);
                $this->users_m->insert_workingtime($user_name, 'Thursday', $mstarttime, $mendtime);
                $this->users_m->insert_workingtime($user_name, 'Friday', $mstarttime, $mendtime);
                $this->users_m->insert_workingtime($user_name, 'Saturday', $sstarttime, $sendtime);

                $this->index();
            }
        }
        else {
            $this->form_validation->set_rules('entrydate', 'Entry Date', 'required');
            $this->form_validation->set_rules('expiredate', 'Expire Date', 'required');

            if ($this->form_validation->run() == FALSE) {
                $this->index_othertime();
            }
            else {
                $entrydate = $this->input->post('entrydate');
                $expiredate = $this->input->post('expiredate');
                $monstarttime = $this->input->post('monstarttime');
                $monendtime = $this->input->post('monendtime');
                $tuestarttime = $this->input->post('tuestarttime');
                $tueendtime = $this->input->post('tueendtime');
                $wedstarttime = $this->input->post('wedstarttime');
                $wedendtime = $this->input->post('wedendtime');
                $thustarttime = $this->input->post('thustarttime');
                $thuendtime = $this->input->post('thuendtime');
                $fristarttime = $this->input->post('fristarttime');
                $friendtime = $this->input->post('friendtime');
                $satstarttime = $this->input->post('satstarttime');
                $satendtime = $this->input->post('satendtime');
                $sunstarttime = $this->input->post('sunstarttime');
                $sunendtime = $this->input->post('sunendtime');

                $this->users_m->insert($user_name, $password, $first_name, $last_name, $address, $home_phone, $mobile, $email,
                    $birthday, $kinname, $contact, $medical, $driverno, $tfn, $accountname, $bankname,
                    $bsb, $accountno, $department_id, $user_level_id, $user_type);

                $this->users_m->insert_contact($user_name, $entrydate, $expiredate);

                $this->users_m->insert_workingtime($user_name, 'Monday', $monstarttime, $monendtime);
                $this->users_m->insert_workingtime($user_name, 'Tuesday', $tuestarttime, $tueendtime);
                $this->users_m->insert_workingtime($user_name, 'Wednesday', $wedstarttime, $wedendtime);
                $this->users_m->insert_workingtime($user_name, 'Thursday', $thustarttime, $thuendtime);
                $this->users_m->insert_workingtime($user_name, 'Friday', $fristarttime, $friendtime);
                $this->users_m->insert_workingtime($user_name, 'Saturday', $satstarttime, $satendtime);
                $this->users_m->insert_workingtime($user_name, 'Sunday', $sunstarttime, $sunendtime);

                $this->index();
            }
        }
    }

    function remove() {
        $user_name = $this->uri->segment(3);
        $this->users_m->delete_user($user_name);

        redirect('user');

    }
    
    function edit_account(){
    	$this->load->model('St_department_m', 'department_m');
        $data['department'] = $this->department_m->get_department_active();
        $this->load->model('St_user_level_m', 'user_level_m');
        $data['userlevel'] = $this->user_level_m->get_userlevel_active();
    	$data['navigation'] = "<a href='". site_url('user') ."'>User</a> > User Account";
        $this->load->view('navigation', $data);
        $this->load->view('user/edit_account');
    }
    
    /**
     * edit user info
     * @param $user_name
     * @return object
     */
    function edit($user_id){
    	if(empty($user_id)){
    		$this->index();
    	}
    	//read current user info
    	$user_info = $this->users_m->get_by_id($user_id);
    	if($user_info){
    		$data['user'] = $user_info;
    		
    		//read site
        	$this->load->model('St_site_m', 'site_m');
        	$data['sites'] = $this->site_m->get_select_dropdown();
    		//read department
	    	$this->load->model('St_department_m', 'department_m');
	        $data['departments'] = $this->department_m->get_select_dropdown($user_info->site_id);
	        
	        //read userlevels
	        $this->load->model('St_user_level_m', 'user_level_m');
	        $data['user_levels'] = $this->user_level_m->get_dropdown();
	        
	        $this->load->model('St_employment_type_m', 'employment_type_m');
	        $data['employment_types'] = $this->employment_type_m->get_select_dropdown();
	        
	        $data['sexs'] = $this->_get_sex_dropdown();
	        $data['residency_statuses'] = $this->_get_residency_status();
	        	        
//	        $this->load->model('St_working_hours_m', 'working_hours_m');
//	        $data['working_hours'] = $this->working_hours_m->get_by_user_name($user_info->user_name);
	        
	        $this->load->model('St_payrollshift_m', 'payrollshift_m');
	        $data['payrollshifts'] = $this->payrollshift_m->get_select_dropdown($user_info->site_id);
	    	$data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='". site_url('user') ."'>User</a> > Edit user";
	    	
	        $this->load->view('navigation', $data);
	        $this->load->view('user/edit_user');
    	}
    }
    
    function view_stats($user_id){
    	$user_info = $this->users_m->get_by_id($user_id);
		$user_name = $user_info->user_name;    	
		$data['user'] = $user_info;
    	
    	$now_time = now();
    	$now_day = date(DATE_FORMAT, $now_time);
    	
    	//calculate total annual/personal leave balance
    	$this->load->model('St_working_hours_m', 'working_hours_m');    	
    	$this->load->model('St_payroll_data_m', 'payroll_data_m');
    	$this->load->model('St_request_m', 'request_m');

		$payroll_data = $this->payroll_data_m->get_last_by_user($user_id);
    	
    	if($payroll_data){
    		$this->load->model('St_payroll_m', 'payroll_m');
    		
    		$last_payroll = $this->payroll_m->get_last();
    		
	    	$annual_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_ANNUAL, true, false, $last_payroll->end_date);
	    	$annual_upcoming_used = IS_NULL($annual_upcoming_used)?0:$annual_upcoming_used;
    	
    		$personal_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, true, false, $last_payroll->end_date);
    		$personal_upcoming_used = IS_NULL($personal_upcoming_used)?0:$personal_upcoming_used;
    		
			$last_annual_leave_end = $payroll_data->annual_leave_end;
			$last_personal_leave_end = $payroll_data->personal_leave_end;
		}else{
			$this->load->model('St_working_hours_m', 'working_hours_m');
			$total_working_stat = $this->working_hours_m->get_staff_working_hours($user_name);
			
	    	$annual_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_ANNUAL, true);
	    	$annual_upcoming_used = IS_NULL($annual_upcoming_used)?0:$annual_upcoming_used;
    	
    		$personal_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, true);
    		$personal_upcoming_used = IS_NULL($personal_upcoming_used)?0:$personal_upcoming_used;
			
			$last_annual_leave_end = get_number($total_working_stat->annual_hours_count);
			$last_personal_leave_end = get_number($total_working_stat->personal_hours_count);
		}

    	//calculate the annual leave balance
    	$data['annual_available'] = round($last_annual_leave_end - $annual_upcoming_used, 2);
    	$data['personal_available'] = round($last_personal_leave_end - $personal_upcoming_used, 2);

    	
    	//annual
    	$annual_7_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_ANNUAL, date_add('d', -7, $now_day), $now_time);
    	$data['annual_7_days'] = $annual_7_stat->daycount;
    	$data['annual_7_times'] = IS_NULL($annual_7_stat->timecount)?0:round($annual_7_stat->timecount, 2);
    	
    	$annual_30_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_ANNUAL, date_add('d', -30, $now_day), $now_time);
    	$data['annual_30_days'] = $annual_30_stat->daycount;
    	$data['annual_30_times'] = IS_NULL($annual_30_stat->timecount)?0:round($annual_30_stat->timecount, 2);
    	
    	$annual_90_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_ANNUAL, date_add('d', -90, $now_day), $now_time);
    	$data['annual_90_days'] = $annual_90_stat->daycount;
    	$data['annual_90_times'] = IS_NULL($annual_90_stat->timecount)?0:round($annual_90_stat->timecount, 2);
    	
    	$annual_180_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_ANNUAL, date_add('d', -180, $now_day), $now_time);
    	$data['annual_180_days'] = $annual_180_stat->daycount;
    	$data['annual_180_times'] = IS_NULL($annual_180_stat->timecount)?0:round($annual_180_stat->timecount, 2);
    	
    	$annual_360_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_ANNUAL, date_add('d', -360, $now_day), $now_time);
    	$data['annual_360_days'] = $annual_360_stat->daycount;
    	$data['annual_360_times'] = IS_NULL($annual_360_stat->timecount)?0:round($annual_360_stat->timecount, 2);
    	
    	$annual_all_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_ANNUAL);
    	$data['annual_all_days'] = $annual_all_stat->daycount;
    	$data['annual_all_times'] = IS_NULL($annual_all_stat->timecount)?0:round($annual_all_stat->timecount, 2);
    	
    	$personal_7_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, date_add('d', -7, $now_day), $now_time);
    	$data['personal_7_days'] = $personal_7_stat->daycount;
    	$data['personal_7_times'] = IS_NULL($personal_7_stat->timecount)?0:round($personal_7_stat->timecount, 2);
    	
    	$personal_30_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, date_add('d', -30, $now_day), $now_time);
    	$data['personal_30_days'] = $personal_30_stat->daycount;
    	$data['personal_30_times'] = IS_NULL($personal_30_stat->timecount)?0:round($personal_30_stat->timecount, 2);
    	
    	$personal_90_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, date_add('d', -90, $now_day), $now_time);
    	$data['personal_90_days'] = $personal_90_stat->daycount;
    	$data['personal_90_times'] = IS_NULL($personal_90_stat->timecount)?0:round($personal_90_stat->timecount, 2);
    	
    	$personal_180_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, date_add('d', -180, $now_day), $now_time);
    	$data['personal_180_days'] = $personal_180_stat->daycount;
    	$data['personal_180_times'] = IS_NULL($personal_180_stat->timecount)?0:round($personal_180_stat->timecount, 2);
    	
    	$personal_360_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, date_add('d', -360, $now_day), $now_time);
    	$data['personal_360_days'] = $personal_30_stat->daycount;
    	$data['personal_360_times'] = IS_NULL($personal_360_stat->timecount)?0:round($personal_360_stat->timecount, 2);
    	
    	$personal_all_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_PERSONAL);
    	$data['personal_all_days'] = $personal_all_stat->daycount;
    	$data['personal_all_times'] = IS_NULL($personal_all_stat->timecount)?0:round($personal_all_stat->timecount, 2);
    	
    	$late_7_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LATE, date_add('d', -7, $now_day), $now_time);
    	$data['late_7_days'] = $late_7_stat->daycount;
    	$data['late_7_times'] = IS_NULL($late_7_stat->timecount)?0:round($late_7_stat->timecount * 60, 2);
    	
    	$late_30_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LATE, date_add('d', -30, $now_day), $now_time);
    	$data['late_30_days'] = $late_30_stat->daycount;
    	$data['late_30_times'] = IS_NULL($late_30_stat->timecount)?0:round($late_30_stat->timecount * 60, 2);
    	
    	$late_90_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LATE, date_add('d', -90, $now_day), $now_time);
    	$data['late_90_days'] = $late_90_stat->daycount;
    	$data['late_90_times'] = IS_NULL($late_90_stat->timecount)?0:round($late_90_stat->timecount * 60, 2);
    	
    	$late_180_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LATE, date_add('d', -180, $now_day), $now_time);
    	$data['late_180_days'] = $late_180_stat->daycount;
    	$data['late_180_times'] = IS_NULL($late_180_stat->timecount)?0:round($late_180_stat->timecount * 60, 2);
    	
    	$late_360_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LATE, date_add('d', -360, $now_day), $now_time);
    	$data['late_360_days'] = $late_360_stat->daycount;
    	$data['late_360_times'] = IS_NULL($late_360_stat->timecount)?0:round($late_360_stat->timecount * 60, 2);
    	
    	$late_all_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LATE);
    	$data['late_all_days'] = $late_all_stat->daycount;
    	$data['late_all_times'] = IS_NULL($late_all_stat->timecount)?0:round($late_all_stat->timecount * 60, 2);
    	
    	$lunch_7_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LUNCH, date_add('d', -7, $now_day), $now_time);
    	$data['lunch_7_days'] = $lunch_7_stat->daycount;
    	$data['lunch_7_times'] = IS_NULL($lunch_7_stat->timecount)?0:round($lunch_7_stat->timecount * 60, 2);
    	
    	$lunch_30_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LUNCH, date_add('d', -30, $now_day), $now_time);
    	$data['lunch_30_days'] = $lunch_30_stat->daycount;
    	$data['lunch_30_times'] = IS_NULL($lunch_30_stat->timecount)?0:round($lunch_30_stat->timecount * 60, 2);
    	
    	$lunch_90_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LUNCH, date_add('d', -90, $now_day), $now_time);
    	$data['lunch_90_days'] = $lunch_90_stat->daycount;
    	$data['lunch_90_times'] = IS_NULL($lunch_90_stat->timecount)?0:round($lunch_90_stat->timecount * 60, 2);
    	
    	$lunch_180_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LUNCH, date_add('d', -180, $now_day), $now_time);
    	$data['lunch_180_days'] = $lunch_180_stat->daycount;
    	$data['lunch_180_times'] = IS_NULL($lunch_180_stat->timecount)?0:round($lunch_180_stat->timecount * 60, 2);
    	
    	$lunch_360_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LUNCH, date_add('d', -360, $now_day), $now_time);
    	$data['lunch_360_days'] = $lunch_360_stat->daycount;
    	$data['lunch_360_times'] = IS_NULL($lunch_360_stat->timecount)?0:round($lunch_360_stat->timecount * 60, 2);

    	$lunch_all_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LUNCH);
    	$data['lunch_all_days'] = $lunch_all_stat->daycount;
    	$data['lunch_all_times'] = IS_NULL($lunch_all_stat->timecount)?0:round($lunch_all_stat->timecount * 60, 2);
    	
    	$data['form_id'] = 'past_leave';
    	
    	
    	$compasionate_used = $this->request_m->get_upcoming_count_by_leave_type($user_name, LEAVE_TYPE_COMPASIONATE);
    	$no_mc_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, false, true);
    	
    	$data['comp_used'] = IS_NULL($compasionate_used)?0:round($compasionate_used, 2);
    	$data['no_mc_used'] = IS_NULL($no_mc_used)?0:round($no_mc_used, 2);
    	
    	
    	
    	$data['no_mc_leave_left'] = MAX_NO_MC_HOURS - $no_mc_used;
    	$data['comp_leave_left'] = MAX_COMPASIONATE_HOURS - $compasionate_used;
    	
    	
    	$data['no_mc_leave_left'] = round($data['no_mc_leave_left'],2);
    	$data['comp_leave_left'] = round($data['comp_leave_left'],2);
    	
    	// Get Leave History
    	$this->load->model('St_leave_type_m', 'leave_type_m');
    	$leave_types = $this->leave_type_m->get_dropdown();
    	$data['leave_types'] = $leave_types;
    	
    	$data['requests'] = $this->request_m->get_approved_requests_by_leave_type(FALSE, $user_name);
    	
    	
    	$this->load->view('user/view_stats', $data);
    	
    }
    
    
    /**
     * list the reqeust history
     * @return json
     */
	function past_leave_ajax(){
    	log_message("debug", "request past_leave_ajax  .....");
		
    	$data['form_id'] = 'past_leave';
    	$user_name = $this->input->post('user_name');
    	
    	$order_by = $this->input->post('past_leave_order_by');        
        $order = $this->input->post('past_leave_order');
    	$leave_type_id = $this->input->post('past_leave_filter_leave_type_id');
    	$time_period_id = $this->input->post('past_leave_filter_time_period_id');
    	$page_no = $this->input->post('past_leave_page_no');

		$limit = $this->input->post('past_leave_limit');
    	$data['page_limit'] = $limit;
    	log_message("debug", "past_leave_ajax user_name ==== " .$user_name );
    	log_message("debug", "past_leave_ajax leave_type_id ==== " .$leave_type_id );
    	log_message("debug", "past_leave_ajax order_by ==== " .$order_by );
    	
    	$time_period = get_time_period($time_period_id);
		$data['requests'] = $this->request_m->get_approved_requests_by_leave_type(FALSE, $user_name,$leave_type_id, $time_period[0], $time_period[1], $order_by, $order, $page_no, $limit);
		
		$total_count = $this->request_m->count_approved_requests_by_leave_type(FALSE, $user_name,$leave_type_id, $time_period[0],$time_period[1]);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
		//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
		
		log_message("debug", "saved ajax request/list_requests_data....");
		
		$content = $this->load->view('request/list_requests_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }    

    /**
     * check whether user name exists
     * @return unknown_type
     */
    function check_user_name_ajax(){
    	$user_row = $this->users_m->get_by_user_name($this->input->post('user_name'));
    	if($user_row){
    		echo "false";
    	}else{
    		echo "true";
    	}
    }
    
    function login_log(){
    	$data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > Users log ";
    	$data['form_id'] = 'log';
    	//read site
    	$this->load->model('St_user_log_m', 'user_log_m');
		
		$data['logs'] = $this->user_log_m->get_logs();
		$total_count = $this->user_log_m->count_logs();
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	$this->load->view('navigation', $data);
    	$this->load->view('user/list_logs', $data);
    }
    
    function login_log_ajax(){
    	$data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > Login log ";
    	$data['form_id'] = 'log';
        
    	$from_date = $this->input->post('log_filter_from_date');
    	$to_date = $this->input->post('log_filter_to_date');
    	$page_no = $this->input->post('log_page_no');
        $limit = $this->input->post('log_limit');
    	$data['page_limit'] = $limit;
    	
    	log_message("debug", "log limit ==== " . $limit);
    	
		if(!empty($from_date)){
    		$from_date = strtotime($from_date);
    	}
		if(!empty($to_date)){
    		$to_date = strtotime($to_date);
    		$to_date += 86400;
    	}
    	log_message("debug", " from_date ==== " . $from_date);
    	log_message("debug", " to_date ==== " . $to_date);
    	
    	$this->load->model('St_user_log_m', 'user_log_m');
		$data['logs'] = $this->user_log_m->get_logs($from_date, $to_date, $page_no, $limit);
		$total_count = $this->user_log_m->count_logs($from_date, $to_date);
		
    	$page_count = 0;
    	if(($total_count % $limit) == 0){
    		$page_count = $total_count / $limit;
    	}else{
    		$page_count = floor($total_count / $limit) + 1;
    	}
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	$str = $this->load->view('user/list_logs_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }
    
	function _get_residency_status(){
    	$options = array(
    			  ''		=> 'Please select ...',
                  'Citizen' => 'Citizen',
                  'Permanent Resident'  => 'Permanent Resident',
                  'Student' => 'Student',
    			  'TR'  	=> 'TR'
                );
        return $options;
    }
    
    function _get_sex_dropdown(){
    	$options = array(
    		''  => "Please select..",
    		'1' => "Male",
    		'2' => "Female"
    	);
    	return $options;
    }
    
	function _get_status_dropdown(){
    	$options = array(
    		''  => "All",
    		'1' => "Active",
    		'0' => "InActive"
    	);
    	return $options;
    }
}

