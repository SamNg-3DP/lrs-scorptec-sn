<?php

class Approve extends Controller {

    function Approve() {
        parent::Controller ();
    	if(!$this->userauth->logined()){
			$infoMsg = array(
				'info' => 'Please login !'
			);
			$this->session->set_flashdata('auth', $this->load->view('msgbox/info', $infoMsg , True));
			redirect("login");
		}else{
			if(!$this->userauth->judge_priv("approve")){
				$errorMsg = array(
					'error' => 'You have no privilege into this page !'
				);
				$this->session->set_flashdata('auth', $this->load->view('msgbox/error', $errorMsg , True));
				redirect("msgbox", 'location');
			}
		}
		
		$this->form_validation->set_error_delimiters('<p class="error">', '</p>');
		
		//load request model
		$this->load->model('St_request_m', 'request_m');
		//load site model
		$this->load->model('St_site_m', 'site_m');
		//load department model
		$this->load->model('St_department_m', 'department_m');
		//load leave type model
		$this->load->model('St_leave_type_m', 'leave_type_m');
    }

    function index() {
		$data['navigation'] = "<a href='". site_url('admin/leave') ."' target='_top'>Leave Request</a> > <a href='". site_url('approve') ."'>Leave Approval</a> > Current Leave Records";
		//read each leave request state count
    	$approval_counts = $this->request_m->get_approvals_counts($this->session->userdata('user_name'), $this->session->userdata('user_level'));
    	$new_count = 0;
    	$not_approved_count = 0;
    	$approved_count = 0;
    	$waiting_count = 0;
    	foreach ($approval_counts as $row){
    		if($row->state == 10){
    			$new_count = $row->icount;
    		}else if($row->state == 20 || $row->state == 30){
    			$waiting_count += $row->icount;
    		}else if($row->state == 40){
    			$approved_count = $row->icount;
    		}else if($row->state == 50){
    			$not_approved_count = $row->icount;
    		}
    	}
    	
		$data['new_count'] = $new_count;
		$data['not_approved_count'] = $not_approved_count;
		$data['approved_count'] = $approved_count;
		$data['waiting_count'] = $waiting_count;
		
		$department_id = $this->session->userdata('department');
    	if($this->session->userdata('user_level') == 3 
    		|| $this->session->userdata('user_level') == 4){
    		$department_id = "";
    	}
    	
		$upcoming_count = $this->request_m->count_upcoming_approved($this->session->userdata('user_level'), 0, $department_id);
		$data['upcoming_count'] = $upcoming_count;
    	
        $this->load->view('navigation', $data);
		$this->load->view('request/approve');
    }
    
    /**
     * past leave records
     */
    function past() {
		$data['navigation'] = "<a href='". site_url('admin/leave') ."' target='_top'>Leave Request</a> > <a href='". site_url('approve/past') ."'>Past Leave Records</a> > List";
		//read each leave request state count
    	
		$department_id = $this->session->userdata('department');
    	if($this->session->userdata('user_level') == 3 
    		|| $this->session->userdata('user_level') == 4){
    		$department_id = "";
    	}

		$approved_counts = $this->request_m->get_past_approved_counts($department_id);
		$total_count = 0;
    	$past_annual_count = 0;
    	$past_personal_count = 0;
    	$past_late_count = 0;
    	$past_lunch_count = 0;
    	$past_training_count = 0;
    	$past_client_count = 0;
    	$past_overtime_count = 0;
    	$past_unpaid_count = 0;
    	$past_clock_count = 0;
    	foreach ($approved_counts as $row){
    		if($row->id == LEAVE_TYPE_ANNUAL){
    			$past_annual_count = $row->icount;
    			$total_count += $row->icount;
    		}else if($row->id == LEAVE_TYPE_PERSONAL){
    			$past_personal_count = $row->icount;
    			$total_count += $row->icount;
    		}else if($row->id == LEAVE_TYPE_LATE){
    			$past_late_count = $row->icount;
    			$total_count += $row->icount;
    		}else if($row->id == LEAVE_TYPE_LUNCH){
    			$past_lunch_count = $row->icount;
    			$total_count += $row->icount;    			
    		}else if($row->id == LEAVE_TYPE_TRAINING){
    			$past_training_count = $row->icount;
    			$total_count += $row->icount;
    		}else if($row->id == LEAVE_TYPE_CLIENTVISIT){
    			$past_client_count = $row->icount;
    			$total_count += $row->icount;
    		}else if($row->id == LEAVE_TYPE_OVERTIME){
    			$past_overtime_count = $row->icount;
    			$total_count += $row->icount;
    		}else if($row->id == LEAVE_TYPE_UNPAID){
    			$past_unpaid_count = $row->icount;
    			$total_count += $row->icount;
    		}else if($row->id == LEAVE_TYPE_CLOCK){
    			$past_clock_count = $row->icount;
    			$total_count += $row->icount;
    		}
    	}
    	$data['past_all_count'] = $total_count;
		$data['past_annual_count'] = $past_annual_count;
		$data['past_personal_count'] = $past_personal_count;
		$data['past_late_count'] = $past_late_count;
		$data['past_lunch_count'] = $past_lunch_count;
		$data['past_training_count'] = $past_training_count;
		$data['past_client_count'] = $past_client_count;
		$data['past_overtime_count'] = $past_overtime_count;
		$data['past_unpaid_count'] = $past_unpaid_count;
		$data['past_clock_count'] = $past_clock_count;
		
        $this->load->view('navigation', $data);
		$this->load->view('request/approve_past');
    }

    /**
     * list request which are saved, but not apply
     */
    function saved(){
    	$department_id = $this->session->userdata('department');
    	$site_id = $this->session->userdata('site_id');
    	$user_level = $this->session->userdata('user_level');
    	
    	$data['state_name'] = 'New';
    	$data['form_id'] = 'saved';
		//read site
        $data['sites'] = $this->site_m->get_dropdown();
        
        //read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
        
        //read leave types
        $data['leave_types'] = $this->leave_type_m->get_dropdown();
		    	
    	
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	if($user_level == 3 
    		|| $user_level == 4){
    		$department_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		$data['approvals'] = $this->request_m->get_saved_approvals(
			$user_level,
			$site_id,
			$department_id
		);
		
		$total_count = $this->request_m->count_saved_approvals($user_level, $site_id, $department_id);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	$this->load->view('request/list_approvals', $data);
    }
    
	function saved_ajax(){
		log_message("debug", "<<<saved_ajax");
		$user_level = $this->session->userdata('user_level');
    	$data['form_id'] = 'saved';
        $order_by = $this->input->post('saved_order_by');
        
        $order = $this->input->post('saved_order');
        $site_id = $this->input->post('saved_filter_site_id');
    	$department_id = $this->input->post('saved_filter_department_id');
    	$leave_type_id = $this->input->post('saved_filter_leave_type_id');
    	$time_period_id = $this->input->post('saved_filter_time_period_id');
    	$first_name = $this->input->post('saved_filter_first_name');
    	$last_name = $this->input->post('saved_filter_last_name');
    	$page_no = $this->input->post('saved_page_no');
        
		$limit = $this->input->post('saved_limit');
    	$data['page_limit'] = $limit;
    	log_message("debug", "department_id  === ". $department_id);
    	$time_period = get_time_period($time_period_id);
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
		$data['approvals'] = $this->request_m->get_saved_approvals(
			$user_level,$site_id,
			$department_id, $leave_type_id, $time_period[0], $time_period[1], $first_name, $last_name, $order_by, $order, $page_no, $limit
		);
		
		$total_count = $this->request_m->count_saved_approvals(
			$user_level, $site_id, $department_id, $leave_type_id, $time_period[0], $time_period[1], $first_name, $last_name);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
        
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	$str = $this->load->view('request/list_approvals_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }
    
	/**
     * list request which are not approved
     */
    function not_approved(){
    	$department_id = $this->session->userdata('department');
    	$site_id = $this->session->userdata('site_id');
    	$user_level = $this->session->userdata('user_level');
    	
    	$data['state_name'] = 'Not Approved Approval';
    	$data['form_id'] = 'not_approved';
    	//read site
        $data['sites'] = $this->site_m->get_dropdown();
		//read department
        $data['departments'] = $this->department_m->get_dropdown($this->session->userdata('site_id'));
        
        //read leave types
        $data['leave_types'] = $this->leave_type_m->get_dropdown();
		    	
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	if($user_level == 3 
    		|| $user_level == 4){
    		$department_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		$data['approvals'] = $this->request_m->get_not_approved_approvals(
			$user_level, 
			$site_id,
			$department_id
		);
		
		$total_count = $this->request_m->count_not_approved_approvals($user_level, $site_id, $department_id);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	$this->load->view('request/list_approvals', $data);
    }
    
	function not_approved_ajax(){
		log_message("debug", "<<<not_approved_ajax");
		$user_level = $this->session->userdata('user_level');
		$data['form_id'] = 'not_approved';
        $order_by = $this->input->post('not_approved_order_by');
        
        $order = $this->input->post('not_approved_order');
        $site_id = $this->input->post('not_approved_filter_site_id');
    	$department_id = $this->input->post('not_approved_filter_department_id');
    	$leave_type_id = $this->input->post('not_approved_filter_leave_type_id');
    	$time_period_id = $this->input->post('not_approved_filter_time_period_id');
    	$first_name = $this->input->post('not_approved_filter_first_name');
    	$last_name = $this->input->post('not_approved_filter_last_name');
    	$page_no = $this->input->post('not_approved_page_no');
        
		$limit = $this->input->post('not_approved_limit');
    	$data['page_limit'] = $limit;
    	
    	$time_period = get_time_period($time_period_id);
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
		$data['approvals'] = $this->request_m->get_not_approved_approvals(
			$user_level, $site_id,
			$department_id, $leave_type_id, $time_period[0], $time_period[1], $first_name, $last_name, $order_by, $order, $page_no, $limit
		);
		
		$total_count = $this->request_m->count_not_approved_approvals($user_level, $site_id, $department_id, $leave_type_id, $time_period[0], $time_period[1], $first_name, $last_name);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	log_message("debug", "sort order by === " . $sort_name);
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
        
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	$str = $this->load->view('request/list_approvals_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }
    /**
     * list request which are waiting approval
     */
	function waiting(){
		$department_id = $this->session->userdata('department');
    	$site_id = $this->session->userdata('site_id');
    	$user_level = $this->session->userdata('user_level');
    	
		$data['state_name'] = 'Waiting Approval';
    	$data['form_id'] = 'waiting';
    	//read site
        $data['sites'] = $this->site_m->get_dropdown();
		//read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
        
        //read leave types
        $data['leave_types'] = $this->leave_type_m->get_dropdown();
		    	
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
		if($user_level == 3 
    		|| $user_level == 4){
    		$department_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		$data['approvals'] = $this->request_m->get_waiting_approvals(
			$user_level,
			$site_id,
			$department_id
		);
		$total_count = $this->request_m->count_waiting_approvals($user_level, $site_id, $department_id);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	//$this->load->view('request/waiting_approvals', $data);
    	$this->load->view('request/list_approvals', $data);
    }
    
	function waiting_ajax(){
		$user_level = $this->session->userdata('user_level');
		$data['form_id'] = 'waiting';
        $order_by = $this->input->post('waiting_order_by');
        
        $order = $this->input->post('waiting_order');
        $site_id = $this->input->post('waiting_filter_site_id');
    	$department_id = $this->input->post('waiting_filter_department_id');
    	$leave_type_id = $this->input->post('waiting_filter_leave_type_id');
    	$time_period_id = $this->input->post('waiting_filter_time_period_id');
    	$first_name = $this->input->post('waiting_filter_first_name');
    	$last_name = $this->input->post('waiting_filter_last_name');
    	$page_no = $this->input->post('waiting_page_no');
        
		$limit = $this->input->post('waiting_limit');
    	$data['page_limit'] = $limit;
    	
    	$time_period = get_time_period($time_period_id);
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
		$data['approvals'] = $this->request_m->get_waiting_approvals(
			$user_level,$site_id, 
			$department_id, $leave_type_id, $time_period[0], $time_period[1], $first_name, $last_name, $order_by, $order, $page_no, $limit
		);
		
		$total_count = $this->request_m->count_waiting_approvals($user_level, $site_id, $department_id, $leave_type_id, $time_period[0], $time_period[1], $first_name, $last_name);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	log_message("debug", "sort order by === " . $sort_name);
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
    	
    	//$str = $this->load->view('request/waiting_approvals_data', $data, true);
    	$str = $this->load->view('request/list_approvals_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }
    
    /**
     * list request which are approved
     */
    function approved(){
    	$site_id = $this->session->userdata('site_id');
    	$department_id = $this->session->userdata('department');
    	$user_level = $this->session->userdata('user_level');
    	//read site
        $data['sites'] = $this->site_m->get_dropdown();
    	//read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
        
        //read leave types
        $data['leave_types'] = $this->leave_type_m->get_dropdown();
		    	
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	if($user_level == 3 
    		|| $user_level == 4){
    		$department_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		$data['approvals'] = $this->request_m->get_approved_approvals(
			$user_level,
			$department_id,
			'', '', ''
		);
		
		$total_count = $this->request_m->count_approved_approvals($this->session->userdata('user_level'), $department_id, '');
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	$this->load->view('request/approved_approvals', $data);
    }
    
    
    
    function approved_all(){
    	$site_id = $this->session->userdata('site_id');
    	$department_id = $this->session->userdata('department');
    	$user_level = $this->session->userdata('user_level');
    	$data['state_name'] = 'All';
    	$data['form_id'] = 'past_all';
    	//read site
        $data['sites'] = $this->site_m->get_dropdown();
    	//read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
        
        //read leave types
        $data['leave_types'] = $this->leave_type_m->get_dropdown();
		    	
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	if($user_level == 3 
    		|| $user_level == 4){
    		$department_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		$data['approvals'] = $this->request_m->past_approved_all(
			$user_level,
			$site_id,
			$department_id
		);
		
		$total_count = $this->request_m->count_past_approved_all($user_level, $site_id, $department_id);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	$this->load->view('request/list_approved', $data);
    }
    
	function approved_all_ajax(){
		log_message("debug", "<<< approved_all_ajax");
		$user_level = $this->session->userdata('user_level');
		$data['form_id'] = 'past_all';
        $order_by = $this->input->post('past_all_order_by');
        
        $order = $this->input->post('past_all_order');
        $site_id = $this->input->post('past_all_filter_site_id');
    	$department_id = $this->input->post('past_all_filter_department_id');
    	$time_period_id = $this->input->post('past_all_filter_time_period_id');
    	$first_name = $this->input->post('past_all_filter_first_name');
    	$last_name = $this->input->post('past_all_filter_last_name');
    	log_message("debug", "department_id == " . $department_id);
    	
    	$page_no = $this->input->post('past_all_page_no');
    	
		$limit = $this->input->post('past_all_limit');
    	$data['page_limit'] = $limit;
    	
    	$time_period = get_time_period($time_period_id);
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
		$data['approvals'] = $this->request_m->past_approved_all(
			$user_level,$site_id,
			$department_id, $time_period[0], $time_period[1], $first_name, $last_name, $order_by, $order, $page_no, $limit
		);
		
		$total_count = $this->request_m->count_past_approved_all($user_level, $site_id, $department_id, $time_period[0], $time_period[1], $first_name, $last_name);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
    	
    	//$str = $this->load->view('request/approved_annual_approvals_data', $data, true);
    	$str = $this->load->view('request/list_approved_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }    
    
    
    
    
    
            
    /**
     * list request which are approved and leave type is anuual leave
     * @return page
     */
    function approved_annual(){
    	$site_id = $this->session->userdata('site_id');
    	$department_id = $this->session->userdata('department');
    	$user_level = $this->session->userdata('user_level');
    	$data['state_name'] = 'Annual Leave';
    	$data['form_id'] = 'past_annual';
    	//read site
        $data['sites'] = $this->site_m->get_dropdown();
    	//read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
        
        //read leave types
        $data['leave_types'] = $this->leave_type_m->get_dropdown();
		    	
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	if($user_level == 3 
    		|| $user_level == 4){
    		$department_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		$data['approvals'] = $this->request_m->past_approved_annual(
			$user_level,
			$site_id,
			$department_id
		);
		
		$total_count = $this->request_m->count_past_approved_annual($user_level, $site_id, $department_id);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	$this->load->view('request/list_approved', $data);
    }
    
	function approved_annual_ajax(){
		log_message("debug", "<<< approved_annual_ajax");
		$user_level = $this->session->userdata('user_level');
		$data['form_id'] = 'past_annual';
        $order_by = $this->input->post('past_annual_order_by');
        
        $order = $this->input->post('past_annual_order');
        $site_id = $this->input->post('past_annual_filter_site_id');
    	$department_id = $this->input->post('past_annual_filter_department_id');
    	$time_period_id = $this->input->post('past_annual_filter_time_period_id');
    	$first_name = $this->input->post('past_annual_filter_first_name');
    	$last_name = $this->input->post('past_annual_filter_last_name');
    	log_message("debug", "department_id == " . $department_id);
    	
    	$page_no = $this->input->post('past_annual_page_no');
    	
		$limit = $this->input->post('past_annual_limit');
    	$data['page_limit'] = $limit;
    	
    	$time_period = get_time_period($time_period_id);
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
		$data['approvals'] = $this->request_m->past_approved_annual(
			$user_level,$site_id,
			$department_id, $time_period[0], $time_period[1], $first_name, $last_name, $order_by, $order, $page_no, $limit
		);
		
		$total_count = $this->request_m->count_past_approved_annual($user_level, $site_id, $department_id, $time_period[0], $time_period[1], $first_name, $last_name);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
    	
    	//$str = $this->load->view('request/approved_annual_approvals_data', $data, true);
    	$str = $this->load->view('request/list_approved_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }    
    
    function approved_personal(){
    	$site_id = $this->session->userdata('site_id');
    	$department_id = $this->session->userdata('department');
    	$user_level = $this->session->userdata('user_level');
    	$data['state_name'] = 'Personal Leave';
		$data['form_id'] = 'past_personal';
    	//read site
        $data['sites'] = $this->site_m->get_dropdown();
        //read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
		    	
    	$department_id = $this->session->userdata('department');
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	if($user_level == 3 
    		|| $user_level == 4){
    		$department_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		$data['approvals'] = $this->request_m->past_approved_personal(
			$user_level,
			$site_id, 
			$department_id
		);
		
		$total_count = $this->request_m->count_past_approved_personal($user_level, $site_id, $department_id);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	$this->load->view('request/list_approved', $data);
    }
    
	function approved_personal_ajax(){
		log_message("debug", "<<< approved_personal_ajax");
		$user_level = $this->session->userdata('user_level');
		$data['form_id'] = 'past_personal';
		
        $order_by = $this->input->post('past_personal_order_by');
        
        $order = $this->input->post('past_personal_order');
        $site_id = $this->input->post('past_personal_filter_site_id');
    	$department_id = $this->input->post('past_personal_filter_department_id');
    	$time_period_id = $this->input->post('past_personal_filter_time_period_id');
    	$first_name = $this->input->post('past_personal_filter_first_name');
    	$last_name = $this->input->post('past_personal_filter_last_name');
    	$page_no = $this->input->post('past_personal_page_no');
    	
		$limit = $this->input->post('past_personal_limit');
    	$data['page_limit'] = $limit;
    	
    	$time_period = get_time_period($time_period_id);
		$data['approvals'] = $this->request_m->past_approved_personal(
			$user_level,$site_id, 
			$department_id, $time_period[0], $time_period[1], $first_name, $last_name, $order_by, $order, $page_no, $limit
		);
		
		$total_count = $this->request_m->count_past_approved_personal($user_level, $site_id, $department_id, $time_period[0], $time_period[1], $first_name, $last_name);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
    	
		$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	$str = $this->load->view('request/list_approved_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }
    
    function approved_late(){
    	$site_id = $this->session->userdata('site_id');
    	$department_id = $this->session->userdata('department');
    	$user_level = $this->session->userdata('user_level');
    	$data['state_name'] = 'Lateness';
		$data['form_id'] = 'past_late';
    	//read site
        $data['sites'] = $this->site_m->get_dropdown();
        //read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
		    	
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	if($user_level == 3 
    		|| $user_level == 4){
    		$department_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		$data['approvals'] = $this->request_m->past_approved_late(
			$user_level,
			$site_id,
			$department_id
		);
		
		$total_count = $this->request_m->count_past_approved_late($user_level, $site_id, $department_id);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	$this->load->view('request/list_approved', $data);
    }
    
	function approved_late_ajax(){
		log_message("debug", "<<< approved_late_ajax");
		$data['form_id'] = 'past_late';
        $order_by = $this->input->post('past_late_order_by');
        
        $order = $this->input->post('past_late_order');
        $site_id = $this->input->post('past_late_filter_site_id');
    	$department_id = $this->input->post('past_late_filter_department_id');
    	$time_period_id = $this->input->post('past_late_filter_time_period_id');
    	$first_name = $this->input->post('past_late_filter_first_name');
    	$last_name = $this->input->post('past_late_filter_last_name');
    	
    	$page_no = $this->input->post('past_late_page_no');
    	
		$limit = $this->input->post('past_late_limit');
    	$data['page_limit'] = $limit;
    	
    	$time_period = get_time_period($time_period_id);
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
		$data['approvals'] = $this->request_m->past_approved_late(
			$this->session->userdata('user_level'),$site_id, 
			$department_id, $time_period[0], $time_period[1], $first_name, $last_name, $order_by, $order, $page_no, $limit
		);
		
		$total_count = $this->request_m->count_past_approved_late($this->session->userdata('user_level'), $site_id, $department_id, $time_period[0], $time_period[1], $first_name, $last_name);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
    	
		$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	$str = $this->load->view('request/list_approved_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }    
    
    function approved_lunch(){
    	$site_id = $this->session->userdata('site_id');
    	$department_id = $this->session->userdata('department');
    	$user_level = $this->session->userdata('user_level');
    	$data['state_name'] = 'Lunch';
		$data['form_id'] = 'past_lunch';
    	//read site
        $data['sites'] = $this->site_m->get_dropdown();
        //read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
		    	
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	if($user_level == 3 
    		|| $user_level == 4){
    		$department_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		$data['approvals'] = $this->request_m->past_approved_lunch(
			$user_level,
			$site_id,
			$department_id
		);
		
		$total_count = $this->request_m->count_past_approved_lunch($user_level, $site_id, $department_id);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	$this->load->view('request/list_approved', $data);
    }
    
    
	function approved_lunch_ajax(){
		log_message("debug", "<<< approved_lunch_ajax");
		$data['form_id'] = 'past_lunch';
        $order_by = $this->input->post('past_lunch_order_by');
        
        $order = $this->input->post('past_lunch_order');
        $site_id = $this->input->post('past_lunch_filter_site_id');
    	$department_id = $this->input->post('past_lunch_filter_department_id');
    	$time_period_id = $this->input->post('past_lunch_filter_time_period_id');
    	$first_name = $this->input->post('past_lunch_filter_first_name');
    	$last_name = $this->input->post('past_lunch_filter_last_name');
    	
    	$page_no = $this->input->post('past_lunch_page_no');
    	
		$limit = $this->input->post('past_lunch_limit');
    	$data['page_limit'] = $limit;
    	
    	$time_period = get_time_period($time_period_id);
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
		$data['approvals'] = $this->request_m->past_approved_lunch(
			$this->session->userdata('user_level'),$site_id, 
			$department_id, $time_period[0], $time_period[1], $first_name, $last_name, $order_by, $order, $page_no, $limit
		);
		
		$total_count = $this->request_m->count_past_approved_lunch($this->session->userdata('user_level'), $site_id, $department_id, $time_period[0], $time_period[1], $first_name, $last_name);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
    	
		$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	$str = $this->load->view('request/list_approved_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }    
    
    
    
    
    function approved_clock(){
    	$site_id = $this->session->userdata('site_id');
    	$department_id = $this->session->userdata('department');
    	$user_level = $this->session->userdata('user_level');
    	$data['state_name'] = 'Clock';
		$data['form_id'] = 'past_clock';
    	//read site
        $data['sites'] = $this->site_m->get_dropdown();
        //read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
		    	
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	if($user_level == 3 
    		|| $user_level == 4){
    		$department_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		$data['approvals'] = $this->request_m->past_approved_clock(
			$user_level,
			$site_id,
			$department_id
		);
		
		$total_count = $this->request_m->count_past_approved_clock($user_level, $site_id, $department_id);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	$this->load->view('request/list_approved', $data);
    }
    
    
	function approved_clock_ajax(){
		log_message("debug", "<<< approved_clock_ajax");
		$data['form_id'] = 'past_clock';
        $order_by = $this->input->post('past_clock_order_by');
        
        $order = $this->input->post('past_clock_order');
        $site_id = $this->input->post('past_clock_filter_site_id');
    	$department_id = $this->input->post('past_clock_filter_department_id');
    	$time_period_id = $this->input->post('past_clock_filter_time_period_id');
    	$first_name = $this->input->post('past_clock_filter_first_name');
    	$last_name = $this->input->post('past_clock_filter_last_name');
    	
    	$page_no = $this->input->post('past_clock_page_no');
    	
		$limit = $this->input->post('past_clock_limit');
    	$data['page_limit'] = $limit;
    	
    	$time_period = get_time_period($time_period_id);
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
		$data['approvals'] = $this->request_m->past_approved_clock(
			$this->session->userdata('user_level'),$site_id, 
			$department_id, $time_period[0], $time_period[1], $first_name, $last_name, $order_by, $order, $page_no, $limit
		);
		
		$total_count = $this->request_m->count_past_approved_clock($this->session->userdata('user_level'), $site_id, $department_id, $time_period[0], $time_period[1], $first_name, $last_name);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
    	
		$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	$str = $this->load->view('request/list_approved_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }    
    

    
	function approved_training(){
		$site_id = $this->session->userdata('site_id');
    	$department_id = $this->session->userdata('department');
    	$user_level = $this->session->userdata('user_level');
		$data['state_name'] = 'Training/Seminar';
		$data['form_id'] = 'past_training';
		//read site
        $data['sites'] = $this->site_m->get_dropdown();
        //read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
		    	
    	$department_id = $this->session->userdata('department');
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	if($user_level == 3 
    		|| $user_level == 4){
    		$department_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		$data['approvals'] = $this->request_m->past_approved_training(
			$user_level,
			$site_id,
			$department_id
		);
		
		$total_count = $this->request_m->count_past_approved_training($user_level, $site_id, $department_id);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	$this->load->view('request/list_approved', $data);
    }
    
	function approved_training_ajax(){
		log_message("debug", "<<< approved_training_ajax");
		$data['form_id'] = 'past_training';
        $order_by = $this->input->post('past_training_order_by');
        
        $order = $this->input->post('past_training_order');
        $site_id = $this->input->post('past_training_filter_site_id');
    	$department_id = $this->input->post('past_training_filter_department_id');
    	$time_period_id = $this->input->post('past_training_filter_time_period_id');
    	$first_name = $this->input->post('past_training_filter_first_name');
    	$last_name = $this->input->post('past_training_filter_last_name');
    	
    	$page_no = $this->input->post('past_training_page_no');
    	
		$limit = $this->input->post('past_training_limit');
    	$data['page_limit'] = $limit;
    	
    	$time_period = get_time_period($time_period_id);
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
		$data['approvals'] = $this->request_m->past_approved_training(
			$this->session->userdata('user_level'),$site_id, 
			$department_id, $time_period[0], $time_period[1], $first_name, $last_name, $order_by, $order, $page_no, $limit
		);
		
		$total_count = $this->request_m->count_past_approved_training($this->session->userdata('user_level'), $site_id, $department_id, $time_period[0], $time_period[1], $first_name, $last_name);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
    	
		$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	$str = $this->load->view('request/list_approved_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }
    
	function approved_client(){
    	$site_id = $this->session->userdata('site_id');
    	$department_id = $this->session->userdata('department');
    	$user_level = $this->session->userdata('user_level');
    	$data['state_name'] = 'Client visit';
		$data['form_id'] = 'past_client';
    	//read site
        $data['sites'] = $this->site_m->get_dropdown();
        //read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
		    	
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	if($user_level == 3 
    		|| $user_level == 4){
    		$department_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		$data['approvals'] = $this->request_m->past_approved_client(
			$user_level,
			$site_id,
			$department_id
		);
		
		$total_count = $this->request_m->count_past_approved_client($user_level, $site_id, $department_id);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	$this->load->view('request/list_approved', $data);
    }
    
	function approved_client_ajax(){
		log_message("debug", "<<< approved_client_ajax");
		$data['form_id'] = 'past_client';
        $order_by = $this->input->post('past_client_order_by');
        
        $order = $this->input->post('past_client_order');
        $site_id = $this->input->post('past_client_filter_site_id');
    	$department_id = $this->input->post('past_client_filter_department_id');
    	$time_period_id = $this->input->post('past_client_filter_time_period_id');
    	$first_name = $this->input->post('past_client_filter_first_name');
    	$last_name = $this->input->post('past_client_filter_last_name');
    	
    	$page_no = $this->input->post('past_client_page_no');
    	
		$limit = $this->input->post('past_client_limit');
    	$data['page_limit'] = $limit;
    	
    	$time_period = get_time_period($time_period_id);
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
		$data['approvals'] = $this->request_m->past_approved_client(
			$this->session->userdata('user_level'),$site_id, 
			$department_id, $time_period[0], $time_period[1], $first_name, $last_name, $order_by, $order, $page_no, $limit
		);
		
		$total_count = $this->request_m->count_past_approved_client($this->session->userdata('user_level'), $site_id, $department_id, $time_period[0], $time_period[1], $first_name, $last_name);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
        
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	log_message("debug", "approved_client_ajax page_count" . $page_count);
    	
    	$str = $this->load->view('request/list_approved_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }
    
	function approved_overtime(){
    	$site_id = $this->session->userdata('site_id');
    	$department_id = $this->session->userdata('department');
    	$user_level = $this->session->userdata('user_level');
    	$data['state_name'] = 'Overtime';
		$data['form_id'] = 'past_overtime';
    	//read site
        $data['sites'] = $this->site_m->get_dropdown();
        //read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
		    	
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	if($user_level == 3 
    		|| $user_level == 4){
    		$department_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		$data['approvals'] = $this->request_m->past_approved_overtime(
			$user_level,
			$site_id,
			$department_id
		);
		
		$total_count = $this->request_m->count_past_approved_overtime($user_level, $site_id, $department_id);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	$this->load->view('request/list_approved', $data);
    }
    
	function approved_overtime_ajax(){
		log_message("debug", "<<< approved_overtime_ajax");
		$data['form_id'] = 'past_overtime';
        $order_by = $this->input->post('past_overtime_order_by');
        
        $order = $this->input->post('past_overtime_order');
        $site_id = $this->input->post('past_overtime_filter_site_id');
    	$department_id = $this->input->post('past_overtime_filter_department_id');
    	$time_period_id = $this->input->post('past_overtime_filter_time_period_id');
    	$first_name = $this->input->post('past_overtime_filter_first_name');
    	$last_name = $this->input->post('past_overtime_filter_last_name');
    	
    	$page_no = $this->input->post('past_overtime_page_no');
    	
		$limit = $this->input->post('past_overtime_limit');
    	$data['page_limit'] = $limit;
    	
    	$time_period = get_time_period($time_period_id);
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
		$data['approvals'] = $this->request_m->past_approved_overtime(
			$this->session->userdata('user_level'),$site_id, 
			$department_id, $time_period[0], $time_period[1], $first_name, $last_name, $order_by, $order, $page_no, $limit
		);
		
		$total_count = $this->request_m->count_past_approved_overtime($this->session->userdata('user_level'), $site_id, $department_id, $time_period[0], $time_period[1], $first_name, $last_name);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
        
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	$str = $this->load->view('request/list_approved_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }
    
	function approved_unpaid(){
    	$site_id = $this->session->userdata('site_id');
    	$department_id = $this->session->userdata('department');
    	$user_level = $this->session->userdata('user_level');
    	$data['state_name'] = 'Unpaid';
		$data['form_id'] = 'past_unpaid';
    	//read site
        $data['sites'] = $this->site_m->get_dropdown();
        //read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
		    	
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	if($user_level == 3 
    		|| $user_level == 4){
    		$department_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		$data['approvals'] = $this->request_m->past_approved_unpaid(
			$user_level,
			$site_id,
			$department_id
		);
		
		$total_count = $this->request_m->count_past_approved_unpaid($user_level, $site_id, $department_id);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	$this->load->view('request/list_approved', $data);
    }
    
	function approved_unpaid_ajax(){
		log_message("debug", "<<< approved_unpaid_ajax");
		$data['form_id'] = 'past_unpaid';
        $order_by = $this->input->post('past_unpaid_order_by');
        
        $order = $this->input->post('past_unpaid_order');
        $site_id = $this->input->post('past_unpaid_filter_site_id');
    	$department_id = $this->input->post('past_unpaid_filter_department_id');
    	$time_period_id = $this->input->post('past_unpaid_filter_time_period_id');
    	$first_name = $this->input->post('past_unpaid_filter_first_name');
    	$last_name = $this->input->post('past_unpaid_filter_last_name');
    	
    	$page_no = $this->input->post('past_unpaid_page_no');
    	
		$limit = $this->input->post('past_unpaid_limit');
    	$data['page_limit'] = $limit;
    	
    	$time_period = get_time_period($time_period_id);
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
		$data['approvals'] = $this->request_m->past_approved_unpaid(
			$this->session->userdata('user_level'),$site_id, 
			$department_id, $time_period[0], $time_period[1], $first_name, $last_name, $order_by, $order, $page_no, $limit
		);
		
		$total_count = $this->request_m->count_past_approved_unpaid($this->session->userdata('user_level'), $site_id, $department_id, $time_period[0], $time_period[1], $first_name, $last_name);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
        
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	$str = $this->load->view('request/list_approved_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }
    
    function approved_upcoming(){
    	$site_id = $this->session->userdata('site_id');
    	$department_id = $this->session->userdata('department');
    	$user_level = $this->session->userdata('user_level');
    	$data['state_name'] = 'Upcoming';
    	$data['form_id'] = 'upcoming';
    	//read site
        $data['sites'] = $this->site_m->get_dropdown();
        //read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
        
        //read leave types
        $data['leave_types'] = $this->leave_type_m->get_dropdown();
		    	
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	if($user_level == 3 
    		|| $user_level == 4){
    		$department_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		$data['approvals'] = $this->request_m->upcoming_approved(
			$user_level,$site_id,
			$department_id, 0
		);
		
		$total_count = $this->request_m->count_upcoming_approved($user_level, $site_id, $department_id);
    	
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	//$this->load->view('request/approved_upcoming_approvals', $data);
    	$this->load->view('request/list_approvals', $data);
    	
    }
    
	function approved_upcoming_ajax(){
		log_message("debug", "<<< approved_upcoming_ajax");
		$user_level = $this->session->userdata('user_level');
		$data['form_id'] = 'upcoming';
		
        $order_by = $this->input->post('upcoming_order_by');
        
        $order = $this->input->post('upcoming_order');
        $site_id = $this->input->post('upcoming_filter_site_id');
    	$department_id = $this->input->post('upcoming_filter_department_id');
    	$leave_type_id = $this->input->post('upcoming_filter_leave_type_id');
    	
    	$page_no = $this->input->post('upcoming_page_no');
    	
		$limit = $this->input->post('upcoming_limit');
    	$data['page_limit'] = $limit;
    	
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
		$data['approvals'] = $this->request_m->upcoming_approved(
			$user_level,$site_id, 
			$department_id, $leave_type_id, 0, 0, $order_by, $order, $page_no, $limit
		);
		
		$total_count = $this->request_m->count_upcoming_approved($user_level, $site_id, $department_id, $leave_type_id);

    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
        
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	$str = $this->load->view('request/list_approvals_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }
    
    
    
    /**
     * list the colleague leave statement
     * include all approved records
     * @return view
     */
    function colleague_approved($request_id){
    	$request = $this->request_m->get($request_id);
    	if(empty($request)){
    		show_404();
    	}
    	
    	$data['request'] = $request; 
    	$user_name = $request->user_name;
    	
    	$this->load->model('St_users_m', 'users_m');
    	$user = $this->users_m->get_by_user_name($user_name);
    	
		$department_id = $user->department_id;
    	
		$data['form_id'] = 'past_colleague';
    	
		$site_id = $user->site_id;
		//read department
        $data['departments'] = $this->department_m->get_dropdown($site_id);
        
        //read leave types
        $leave_types = $this->leave_type_m->get_dropdown();
		$data['leave_types'] = array("" => "Please select", 
			LEAVE_TYPE_ANNUAL => $leave_types[LEAVE_TYPE_ANNUAL],
			LEAVE_TYPE_PERSONAL => $leave_types[LEAVE_TYPE_PERSONAL],
			);
		    	
    	
    	//$user_level, $department_id=0, $leave_type_id=0, $order_by, $order="ASC", $page_no=1, $limit = PAGE_SIZE
    	if($user->user_level_id == 3){
    		$department_id = "";
    	}
    	
    	$data['site_id'] = $site_id;
    	$data['department_id'] = $department_id;
		$data['approvals'] = $this->request_m->get_colleague_approved_by_leave_type(
			array(LEAVE_TYPE_ANNUAL, LEAVE_TYPE_PERSONAL),
			$request->start_date, $request->end_date, $site_id, $department_id
		);
		
		$total_count = $this->request_m->count_colleague_approved_by_leave_type(
			array(LEAVE_TYPE_ANNUAL, LEAVE_TYPE_PERSONAL),
			$request->start_date, $request->end_date, $site_id, $department_id
		);
		
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
        
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	$this->load->view('request/list_colleague_approved', $data);
    }
    
	function colleague_approved_ajax(){
		log_message("debug", "<<< colleague_approved_ajax");
		$data['form_id'] = 'past_colleague';
		
        $order_by = $this->input->post('past_colleague_order_by');
        
        $order = $this->input->post('past_colleague_order');
        $site_id = $this->input->post('past_colleague_filter_site_id');
    	$department_id = $this->input->post('past_colleague_department_id');
    	$leave_type_id = $this->input->post('past_colleague_leave_type_id');
    	$start_date = $this->input->post('past_colleague_start_date');
    	$end_date = $this->input->post('past_colleague_end_date');
    	
    	$page_no = $this->input->post('past_colleague_page_no');
    	
		$limit = $this->input->post('past_colleague_limit');
    	$data['page_limit'] = $limit;

		
		//$total_count = $this->request_m->count_past_approved_personal($this->session->userdata('user_level'), $department_id, $time_period[0], $time_period[1]);
    	$a_leave_type = array(LEAVE_TYPE_ANNUAL, LEAVE_TYPE_PERSONAL);
    	if(!empty($leave_type_id)){
    		$a_leave_type = $leave_type_id;
    	}
    	
		$data['approvals'] = $this->request_m->get_colleague_approved_by_leave_type(
			$a_leave_type, strtotime($start_date), strtotime($end_date), $site_id, $department_id, 
			$order_by, $order, $page_no, $limit  
		);
		
		$total_count = $this->request_m->count_colleague_approved_by_leave_type(
			$a_leave_type,strtotime($start_date), strtotime($end_date), $site_id, $department_id
		);
		
    	//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_department_id'] = "";
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
    	
		$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
        
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	$str = $this->load->view('request/list_approved_data', $data, true);
    	
        $result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $str
        );
        echo json_encode($result);
    }
    
    
    
    
    
    
    /**
     * prepare to add a new leave request
     * @return 
     */
    function add() {
        $data['navigation'] = "<a href='". site_url('admin/leave') ."' target='_top'>Leave Request</a> > <a href='". site_url('request') ."'>Leave Statement</a> > Add";
        
        $leave_types = $this->leave_type_m->get_dropdown();
        $data['leave_types'] = $leave_types;
        
        $this->load->view('navigation', $data);
        $this->load->view('request/add_request');
    }
    
    function deal($request_id){
    	$leave_types = $this->leave_type_m->get_dropdown();
    	$data['leave_types'] = $leave_types;
    	
    	$request = $this->request_m->get($request_id);
    	if(empty($request)){
    		show_404();
    	}
    	
    	
    	if($request->leave_type_id == LEAVE_TYPE_LATE || $request->leave_type_id == LEAVE_TYPE_LUNCH)
    		$request->hours_used = $request->hours_used * 60;
    		    	
		$data['navigation'] = "<a href='". site_url('admin/leave') ."' target='_top'>Leave Request</a> > <a href='". site_url('approve') ."'>Leave Approval</a> > Approve R". $request->id;
    	$data['request'] = $request; 
    	$user_name = $request->user_name;

    	$this->load->model('St_users_m', 'users_m');
    	$user = $this->users_m->get_by_user_name($user_name);
    	$data['user'] = $user;
    	
    	$data['departments'] = $this->department_m->get_dropdown($user->site_id);
    	
    	$now_time = now();
    	$now_day = date(DATE_FORMAT, $now_time);
    	
    	
    	
    	//calculate total annual/personal leave balance
/*    	$this->load->model('St_working_hours_m', 'working_hours_m');    	
	   	$total_working_stat = $this->working_hours_m->get_staff_working_hours($user_name);
    	
	   	$work_count = $total_working_stat->work_count;
    	$total_annual = round($work_count / 13, 1);
    	$total_personal = round($work_count / 26, 1);
		$total_annual = $total_working_stat->annual_hours_count;
		$total_personal = $total_working_stat->personal_hours_count;
    	
    	$annual_used_count = $total_working_stat->annual_used_count;
    	$personal_used_count = $total_working_stat->personal_used_count;*/    	
    	
    	
        $this->load->model('St_payroll_data_m', 'payroll_data_m');
		$payroll_data = $this->payroll_data_m->get_last_by_user($user->id);
		$last_annual_leave_end = 0;
		$last_personal_leave_end = 0;
		if($payroll_data){
    		$this->load->model('St_payroll_m', 'payroll_m');    		
    		
    		$last_payroll = $this->payroll_m->get_last();
			
			$last_annual_leave_end = $payroll_data->annual_leave_end;
			$last_personal_leave_end = $payroll_data->personal_leave_end;
			
	    	$annual_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_ANNUAL, true, false, $last_payroll->end_date);
	    	$annual_upcoming_used = IS_NULL($annual_upcoming_used)?0:$annual_upcoming_used;

    		$personal_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, true, false, $last_payroll->end_date);
    		$personal_upcoming_used = IS_NULL($personal_upcoming_used)?0:$personal_upcoming_used;
    		
			$last_annual_leave_end = $payroll_data->annual_leave_end;
			$last_personal_leave_end = $payroll_data->personal_leave_end;
			
			log_message("debug", "1 annual_hours_count === ". $last_annual_leave_end);
			log_message("debug", "1 personal_hours_count === ". $last_personal_leave_end);
		}else{
			$this->load->model('St_working_hours_m', 'working_hours_m');
			$total_working_stat = $this->working_hours_m->get_staff_working_hours($user_name);
			
	    	$annual_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_ANNUAL, true);
	    	$annual_upcoming_used = IS_NULL($annual_upcoming_used)?0:$annual_upcoming_used;
    	
    		$personal_upcoming_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, true);
    		$personal_upcoming_used = IS_NULL($personal_upcoming_used)?0:$personal_upcoming_used;
			
			log_message("debug", "2 annual_hours_count === ". $total_working_stat->annual_hours_count);
			log_message("debug", "2 personal_hours_count === ". $total_working_stat->personal_hours_count);
			$last_annual_leave_end = get_number($total_working_stat->annual_hours_count);
			$last_personal_leave_end = get_number($total_working_stat->personal_hours_count);
		}    	
    	
		$annual_available = round($last_annual_leave_end, 2);
		$personal_available = round($last_personal_leave_end, 2);
    	
    	//calculate the annual leave balance
    	$annual_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_ANNUAL);
//    	$annual_upcoming_used = $this->request_m->get_upcoming_count_by_leave_type($user_name, LEAVE_TYPE_ANNUAL);
//    	$annual_upcoming_used = IS_NULL($annual_upcoming_used)?0:$annual_upcoming_used;
    	$data['annual_upcoming'] = $annual_upcoming_used;
    	$data['annual_available'] = round($annual_available - $annual_upcoming_used, 2);
    	$data['total_annual'] = round($annual_available - $annual_upcoming_used, 2);
    	
    	$personal_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL);
//    	$personal_upcoming_used = $this->request_m->get_upcoming_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL);
//    	$personal_upcoming_used = IS_NULL($personal_upcoming_used)?0:$personal_upcoming_used;
    	$data['personal_upcoming'] = $personal_upcoming_used;
    	$data['personal_available'] = round($personal_available - $personal_upcoming_used, 2);
    	$data['total_personal'] = round($personal_available - $personal_upcoming_used, 2);
    	
    	//annual
    	$annual_7_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_ANNUAL, date_add('d', -7, $now_day), $now_time);
    	$data['annual_7_days'] = $annual_7_stat->daycount;
    	$data['annual_7_times'] = IS_NULL($annual_7_stat->timecount)?0:round($annual_7_stat->timecount, 2);
    	
    	$annual_30_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_ANNUAL, date_add('d', -30, $now_day), $now_time);
    	$data['annual_30_days'] = $annual_30_stat->daycount;
    	$data['annual_30_times'] = IS_NULL($annual_30_stat->timecount)?0:round($annual_30_stat->timecount, 2);
    	
    	$annual_90_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_ANNUAL, date_add('d', -90, $now_day), $now_time);
    	$data['annual_90_days'] = $annual_90_stat->daycount;
    	$data['annual_90_times'] = IS_NULL($annual_90_stat->timecount)?0:round($annual_90_stat->timecount, 2);
    	
    	$annual_180_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_ANNUAL, date_add('d', -180, $now_day), $now_time);
    	$data['annual_180_days'] = $annual_180_stat->daycount;
    	$data['annual_180_times'] = IS_NULL($annual_180_stat->timecount)?0:round($annual_180_stat->timecount, 2);
    	
    	$annual_360_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_ANNUAL, date_add('d', -360, $now_day), $now_time);
    	$data['annual_360_days'] = $annual_360_stat->daycount;
    	$data['annual_360_times'] = IS_NULL($annual_360_stat->timecount)?0:round($annual_360_stat->timecount, 2);
    	
    	$annual_all_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_ANNUAL);
    	$data['annual_all_days'] = $annual_all_stat->daycount;
    	$data['annual_all_times'] = IS_NULL($annual_all_stat->timecount)?0:round($annual_all_stat->timecount, 2);
    	
    	$personal_7_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, date_add('d', -7, $now_day), $now_time);
    	$data['personal_7_days'] = $personal_7_stat->daycount;
    	$data['personal_7_times'] = IS_NULL($personal_7_stat->timecount)?0:round($personal_7_stat->timecount, 2);
    	
    	$personal_30_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, date_add('d', -30, $now_day), $now_time);
    	$data['personal_30_days'] = $personal_30_stat->daycount;
    	$data['personal_30_times'] = IS_NULL($personal_30_stat->timecount)?0:round($personal_30_stat->timecount, 2);
    	
    	$personal_90_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, date_add('d', -90, $now_day), $now_time);
    	$data['personal_90_days'] = $personal_90_stat->daycount;
    	$data['personal_90_times'] = IS_NULL($personal_90_stat->timecount)?0:round($personal_90_stat->timecount, 2);
    	
    	$personal_180_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, date_add('d', -180, $now_day), $now_time);
    	$data['personal_180_days'] = $personal_180_stat->daycount;
    	$data['personal_180_times'] = IS_NULL($personal_180_stat->timecount)?0:round($personal_180_stat->timecount, 2);
    	
    	$personal_360_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, date_add('d', -360, $now_day), $now_time);
    	$data['personal_360_days'] = $personal_30_stat->daycount;
    	$data['personal_360_times'] = IS_NULL($personal_360_stat->timecount)?0:round($personal_360_stat->timecount, 2);
    	
    	$personal_all_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_PERSONAL);
    	$data['personal_all_days'] = $personal_all_stat->daycount;
    	$data['personal_all_times'] = IS_NULL($personal_all_stat->timecount)?0:round($personal_all_stat->timecount, 2);
    	
    	$late_7_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LATE, date_add('d', -7, $now_day), $now_time);
    	$data['late_7_days'] = $late_7_stat->daycount;
    	$data['late_7_times'] = IS_NULL($late_7_stat->timecount)?0:round($late_7_stat->timecount * 60, 2);
    	
    	$late_30_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LATE, date_add('d', -30, $now_day), $now_time);
    	$data['late_30_days'] = $late_30_stat->daycount;
    	$data['late_30_times'] = IS_NULL($late_30_stat->timecount)?0:round($late_30_stat->timecount * 60, 2);
    	
    	$late_90_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LATE, date_add('d', -90, $now_day), $now_time);
    	$data['late_90_days'] = $late_90_stat->daycount;
    	$data['late_90_times'] = IS_NULL($late_90_stat->timecount)?0:round($late_90_stat->timecount * 60, 2);
    	
    	$late_180_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LATE, date_add('d', -180, $now_day), $now_time);
    	$data['late_180_days'] = $late_180_stat->daycount;
    	$data['late_180_times'] = IS_NULL($late_180_stat->timecount)?0:round($late_180_stat->timecount * 60, 2);
    	
    	$late_360_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LATE, date_add('d', -360, $now_day), $now_time);
    	$data['late_360_days'] = $late_360_stat->daycount;
    	$data['late_360_times'] = IS_NULL($late_360_stat->timecount)?0:round($late_360_stat->timecount * 60, 2);
    	
    	$late_all_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LATE);
    	$data['late_all_days'] = $late_all_stat->daycount;
    	$data['late_all_times'] = IS_NULL($late_all_stat->timecount)?0:round($late_all_stat->timecount * 60, 2);
    	
    	$lunch_7_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LUNCH, date_add('d', -7, $now_day), $now_time);
    	$data['lunch_7_days'] = $lunch_7_stat->daycount;
    	$data['lunch_7_times'] = IS_NULL($lunch_7_stat->timecount)?0:round($lunch_7_stat->timecount * 60, 2);
    	
    	$lunch_30_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LUNCH, date_add('d', -30, $now_day), $now_time);
    	$data['lunch_30_days'] = $lunch_30_stat->daycount;
    	$data['lunch_30_times'] = IS_NULL($lunch_30_stat->timecount)?0:round($lunch_30_stat->timecount * 60, 2);
    	
    	$lunch_90_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LUNCH, date_add('d', -90, $now_day), $now_time);
    	$data['lunch_90_days'] = $lunch_90_stat->daycount;
    	$data['lunch_90_times'] = IS_NULL($lunch_90_stat->timecount)?0:round($lunch_90_stat->timecount * 60, 2);
    	
    	$lunch_180_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LUNCH, date_add('d', -180, $now_day), $now_time);
    	$data['lunch_180_days'] = $lunch_180_stat->daycount;
    	$data['lunch_180_times'] = IS_NULL($lunch_180_stat->timecount)?0:round($lunch_180_stat->timecount * 60, 2);
    	
    	$lunch_360_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LUNCH, date_add('d', -360, $now_day), $now_time);
    	$data['lunch_360_days'] = $lunch_360_stat->daycount;
    	$data['lunch_360_times'] = IS_NULL($lunch_360_stat->timecount)?0:round($lunch_360_stat->timecount * 60, 2);

    	$lunch_all_stat = $this->request_m->get_stat_by_leave_type($user_name, LEAVE_TYPE_LUNCH);
    	$data['lunch_all_days'] = $lunch_all_stat->daycount;
    	$data['lunch_all_times'] = IS_NULL($lunch_all_stat->timecount)?0:round($lunch_all_stat->timecount * 60, 2);
    	
    	$data['form_id'] = 'past_leave';
    	$data['requests'] = $this->request_m->get_approved_requests_by_leave_type(FALSE, $user_name, $request->leave_type_id);
    	
    	
    	$compasionate_used = $this->request_m->get_upcoming_count_by_leave_type($user_name, LEAVE_TYPE_COMPASIONATE);
    	$no_mc_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL, false, true);

    	$data['comp_used'] = round($compasionate_used, 2);
    	$data['no_mc_used'] = round($no_mc_used, 2);
    	
    	$data['no_mc_leave_left'] = MAX_NO_MC_HOURS - $no_mc_used;
    	$data['comp_leave_left'] = MAX_COMPASIONATE_HOURS - $compasionate_used;
    	
    	if ($request->leave_type_id == LEAVE_TYPE_PERSONAL)
    		$data['no_mc_leave_left'] -= $request->hours_used;
    	elseif ($request->leave_type_id == LEAVE_TYPE_COMPASIONATE)
    		$data['comp_leave_left'] -= $request->hours_used;
    	
    	$data['no_mc_leave_left'] = round($data['no_mc_leave_left'],2);
    	$data['comp_leave_left'] = round($data['comp_leave_left'],2);
    	
    	
    	$total_count = $this->request_m->count_approved_requests_by_leave_type(FALSE, $user_name,$request->leave_type_id);
    	$page_count = 0;
    	if(($total_count % PAGE_SIZE) == 0){
    		$page_count = $total_count / PAGE_SIZE;
    	}else{
    		$page_count = floor($total_count / PAGE_SIZE) + 1;
    	}
    	$data['page_no'] = 1;
    	$data['page_count'] = $page_count;
    	
    	log_message("debug", "deal page count ==== " . $page_count);
    	
    	//load request log model
		$this->load->model('St_request_log_m', 'request_log_m');
    	$request_logs = $this->request_log_m->get_by_search($request_id, "", "", "", "", "", "", "");
		$log_list = array();
		foreach($request_logs AS $request_log){
			$log['request_id'] = $request_log->id;
			//Action
			$operation = $request_log->operation;
			$action = "";
			if("save" == $operation){
				$action = "saved new request";
			}else if("apply" == $operation){
				$action = "applied request";
			}else if("update" == $operation){
				$action = "updated request";
			}else if("approve" == $operation){
				$action = "approved request";
			}else if("rejected" == $operation){
				$action = "rejected request";
			}else if("cancel" == $operation){
				$action = "canceled request";
			}
			$log['action'] = $action;
			//Actioned On
			$log['actioned_on'] = date(DATETIME_FORMAT, $request_log->operate_time);
			//Actioned By
			$log['actioned_by'] = $request_log->oprator_first_name . " " . $request_log->oprator_last_name;
			$log['action_ip'] = $request_log->operate_ip;
			$log_list[] = $log;
		}
		$data['request_logs'] = $log_list;
		
    	$this->load->view('navigation', $data);
    	$this->load->view('request/deal_request', $data);
    }

    /**
     * list the reqeust history
     * @return json
     */
	function past_leave_ajax(){
    	log_message("debug", "request past_leave_ajax  .....");
		
    	$data['form_id'] = 'past_leave';
    	$user_name = $this->input->post('user_name');
    	
    	$order_by = $this->input->post('past_leave_order_by');        
        $order = $this->input->post('past_leave_order');
    	$leave_type_id = $this->input->post('past_leave_filter_leave_type_id');
    	$time_period_id = $this->input->post('past_leave_filter_time_period_id');
    	$page_no = $this->input->post('past_leave_page_no');

		$limit = $this->input->post('past_leave_limit');
    	$data['page_limit'] = $limit;
    	log_message("debug", "past_leave_ajax user_name ==== " .$user_name );
    	log_message("debug", "past_leave_ajax leave_type_id ==== " .$leave_type_id );
    	log_message("debug", "past_leave_ajax order_by ==== " .$order_by );
    	
    	$time_period = get_time_period($time_period_id);
		$data['requests'] = $this->request_m->get_approved_requests_by_leave_type(FALSE, $user_name,$leave_type_id, $time_period[0], $time_period[1], $order_by, $order, $page_no, $limit);
		
		$total_count = $this->request_m->count_approved_requests_by_leave_type(FALSE, $user_name,$leave_type_id, $time_period[0],$time_period[1]);
		
    	$page_count = 0;
    	if($limit > 0){
	    	if(($total_count % $limit) == 0){
	    		$page_count = $total_count / $limit;
	    	}else{
	    		$page_count = floor($total_count / $limit) + 1;
	    	}
    	}
    	
		//indicat the sort order by image
    	$sort_name = "sort_" . $order_by;
    	$data['sort_id'] 			= "";
    	$data['sort_add_time'] 		= "";
    	$data['sort_applicant'] 	= ""; //first_name, last_name
    	$data['sort_start_date'] 	= "";
    	$data['sort_leave_type_id']	= "";
    	
    	$data['page_no'] = empty($page_no)?1:$page_no;
    	$data['page_count'] = $page_count;
    	
    	//set value with image tag
		$data[$sort_name] = '<img src="images/' . ($order == "DESC" ? 'sort_desc.gif' : 'sort_asc.gif') . '"/>';
		
		log_message("debug", "saved ajax request/list_requests_data....");
		
		$content = $this->load->view('request/list_requests_data', $data, true);
		$result = array(
        	"page_count"=> $page_count,
        	"page_no" 	=> $page_no,
        	"content" 	=> $content
        );
        echo json_encode($result);
    }
	function do_deal(){
		$dnow = now();
		$this->load->model('St_users_m', 'users_m');
    	$request_id = $this->input->post('request_id');
    	$request_hours = $this->input->post('request_hours');
        
    	$operator = $this->session->userdata('user_name');
		$this->load->library('phpmail');
		$this->phpmail->readConfig();
    	//user level 2
    	if($this->session->userdata('user_level') == 2){
    		$mc_provided = $this->input->post('mc_provided');
	    	$reason_acceptable = $this->input->post('reason_acceptable');
	    	$supervisor_approved = $this->input->post('supervisor_approved');
	    	$supervisor_comment = $this->input->post('supervisor_comment');
	    	if($reason_acceptable != 1 && $reason_acceptable != 2){
	    		$this->session->set_flashdata('message', $this->load->view('msgbox/error', array('error' => 'Please select a reason acceptable option !') , True));
	    		redirect("approve/deal/" . $request_id);
	    	}
    		if($supervisor_approved != 1 && $supervisor_approved != 2){
	    		$this->session->set_flashdata('message', $this->load->view('msgbox/error', array('error' => 'Please select a approved option !') , True));
	    		redirect("approve/deal/" . $request_id);
	    	}
	    	
	    	$state = 20;
	    	if($supervisor_approved == 2){
	    		$state = 50;
	    	}else if($supervisor_approved == 1){
	    		// If leave hours less than 37.5 supervisor approval only.
	    		if ($request_hours <= 37.5)
	    			$state = 40;
	    		else
	    			$state = 30;
	    	}
	    	
	    	$request_data = array(
	    		'reason_acceptable'     => $reason_acceptable,
	    		'supervisor_approved'   => $supervisor_approved,
	    		'supervisor_comment'    => $supervisor_comment,
	    		'mc_provided'			=> empty($mc_provided) ? 0 : $mc_provided ,
	    		'supervisor_user_name'  => $operator,
	    		'state'  				=> $state,
	    		'supervisor_time' 		=> $dnow
	    	);
	    	$b_update = $this->request_m->update($request_id, $request_data);
	    	
	    	if($b_update){
	    		//add hisotry log
	    		$approve_log = "";
    			if($state == 50){
    				$approve_log = "rejected";
    			}else{
    				$approve_log = "approve";
    			}
            	$this->load->model('St_request_log_m', 'request_log_m');
            	$log = array(
            		'id' => $request_id,
            		'operator_id' => $this->session->userdata('user_id'),
            		'operation' => 'approve',
            		'operate_time' => $dnow,
            		'operate_ip' => $this->input->ip_address()
            	);
            	$log = array_merge($request_data, $log);
            	$this->request_log_m->insert($log);
	    		//send a mail
	    		$requestinfo = $this->request_m->get_info($request_id);
            	
            	if($requestinfo->user_level_id == 1){
    				$supervisors = $this->users_m->get_supervisors_by_user_name($requestinfo->user_name);
            		if(!empty($supervisors)){
            			foreach($supervisors as $supervisor){
            				if(!empty($supervisor)){
            					if(!empty($supervisor->email)){
            						$this->phpmail->AddAddress($supervisor->email);
            					}
//            					if(!empty($supervisor->personal_email)){
//           						$this->phpmail->AddCC($supervisor->personal_email);
//            					}
            				}
            			}
            		}
            	}
            	
    			//send mail to all manager and admin
            	$managers = $this->users_m->get_managers_by_user_name($requestinfo->user_name);
            	if(!empty($managers)){
            		foreach($managers as $manager){
            			if(!empty($manager)){
            				if(!empty($manager->email)){
            					$this->phpmail->AddAddress($manager->email);
            				}
//            				if(!empty($manager->personal_email)){
//           					$this->phpmail->AddCC($manager->personal_email);
//            				}
            			}
            		}
            	}
	    		
            	$amount_leave = "";
            	if($requestinfo->leave_type_id == LEAVE_TYPE_ANNUAL 
            		|| $requestinfo->leave_type_id == LEAVE_TYPE_PERSONAL){
            		//calculate total annual/personal leave balance
			    	$this->load->model('St_payroll_data_m', 'payroll_data_m');
					$payroll_data = $this->payroll_data_m->get_last_by_user($requestinfo->id);
					$last_annual_leave_end = 0;
					$last_personal_leave_end = 0;
					if($payroll_data){
						$last_annual_leave_end = $payroll_data->annual_leave_end;
						$last_personal_leave_end = $payroll_data->personal_leave_end;
						log_message("debug", "1 annual_hours_count === ". $last_annual_leave_end);
						log_message("debug", "1 personal_hours_count === ". $last_personal_leave_end);
					}else{
						$this->load->model('St_working_hours_m', 'working_hours_m');
						$total_working_stat = $this->working_hours_m->get_staff_working_hours($requestinfo->user_name);
						
						log_message("debug", "2 annual_hours_count === ". $total_working_stat->annual_hours_count);
						log_message("debug", "2 personal_hours_count === ". $total_working_stat->personal_hours_count);
						$last_annual_leave_end = get_number($total_working_stat->annual_hours_count);
						$last_personal_leave_end = get_number($total_working_stat->personal_hours_count);
					}
					
					$annual_available = round($last_annual_leave_end, 2);
					$personal_available = round($last_personal_leave_end, 2);
			    	
			    	
			    	//$this->load->model('St_working_hours_m', 'working_hours_m');    	
			    	//$total_working_stat = $this->working_hours_m->get_staff_working_hours($requestinfo->user_name);
			    	
			    	//$work_count = $total_working_stat->work_count;
			    	
			    	if($requestinfo->leave_type_id == LEAVE_TYPE_ANNUAL){
			    		//$total_annual = $total_working_stat->annual_hours_count;
			    		//$annual_used_count = $total_working_stat->annual_used_count;
			    		//calculate the annual leave balance
			    		//$annual_used = $this->request_m->get_used_count_by_leave_type($requestinfo->user_name, LEAVE_TYPE_ANNUAL);
			    		$annual_upcoming_used = $this->request_m->get_upcoming_count_by_leave_type($requestinfo->user_name, LEAVE_TYPE_ANNUAL);
			    		$annual_upcoming_used = IS_NULL($annual_upcoming_used)?0:$annual_upcoming_used;
			    		$amount_leave_count = $annual_available - $annual_upcoming_used;
			    		$amount_leave_count = round($amount_leave_count, 2);
			    		$amount_leave = ($amount_leave_count > 0)?$amount_leave_count:0;
			    	}else if($requestinfo->leave_type_id == LEAVE_TYPE_PERSONAL){
			    		//$total_personal = $total_working_stat->personal_hours_count;
			    		//$personal_used_count = $total_working_stat->personal_used_count;
			    		//calculate the annual leave balance
			    		//$personal_used = $this->request_m->get_used_count_by_leave_type($requestinfo->user_name, LEAVE_TYPE_PERSONAL);
			    		$personal_upcoming_used = $this->request_m->get_upcoming_count_by_leave_type($requestinfo->user_name, LEAVE_TYPE_PERSONAL);
			    		$personal_upcoming_used = IS_NULL($personal_upcoming_used)?0:$personal_upcoming_used;
			    		$amount_leave_count = $personal_available - $personal_upcoming_used;
			    		$amount_leave_count = round($amount_leave_count, 2);
			    		$amount_leave = ($amount_leave_count > 0)?$amount_leave_count:0;
			    	}
            	}
            	if($amount_leave > 1){
	    			$amount_leave = $amount_leave . " Hours";
	    		}else{
	    			$amount_leave = $amount_leave . " Hour";
	    		}
            	$mail_body['amount_leave_left'] = $amount_leave;
            	
            	$approve_operate = "Approved";
            	if($state == 50){
    				$approve_operate = "Rejected";
    			}
    			
	    		$subject = $approve_operate . " ". $requestinfo->leave_type . " Request R". $request_id ." - "
            						.$requestinfo->first_name . " " . $requestinfo->last_name
            						." From ". date(DATE_FORMAT, $requestinfo->start_day)." to ".date(DATE_FORMAT, $requestinfo->end_day);
            						
            						
	    		$this->phpmail->setSubject($subject);
            	$mail_body['appove_url'] = site_url('approve/deal/'.$request_id);
            	$mail_body['appove_name'] = "approve this leave application here.";
            	$mail_body['request'] = $requestinfo;
            	$this->phpmail->setBody($this->load->view('mail/approve', $mail_body , True));
            	if(!$this->phpmail->send()){
		        	log_message("debug", "send mail .... ".$subject . $this->phpmail->ErrorInfo);
		        }
		        $this->phpmail->ClearAllAddresses();
		        
    			if(!empty($requestinfo->email)){
            		$this->phpmail->AddAddress($requestinfo->email);
            	}
	    		if(!empty($requestinfo->personal_email)){
            		$this->phpmail->AddAddress($requestinfo->personal_email);
            	}
            	$mail_body['appove_url'] = site_url('request/view/'.$request_id);
            	$mail_body['appove_name'] = "view this leave application here.";
            	$this->phpmail->setBody($this->load->view('mail/approve', $mail_body , True));
    			if(!$this->phpmail->send()){
		        	log_message("debug", "send mail .... ".$subject . $this->phpmail->ErrorInfo);
		        }
	    		$this->phpmail->ClearAllAddresses();
	    	}else{
	    		$this->session->set_flashdata('message', $this->load->view('msgbox/error', array('error' => 'Operation failed !') , True));
	    		redirect("approve/deal/" . $request_id);
	    	}
	    	//TODO record approve history
    	}else if($this->session->userdata('user_level') == 3 
    		|| $this->session->userdata('user_level') == 4){
    			
    		$mc_provided = $this->input->post('mc_provided');
    		$paid = $this->input->post('paid');
	    	$approve = $this->input->post('approve');
	    	$manager_comment = $this->input->post('manager_comment');
	    	
	    	if($paid != 1 && $paid != 2){
	    		$this->session->set_flashdata('message', $this->load->view('msgbox/error', array('error' => 'Please select a paid option !') , True));
	    		redirect("approve/deal/" . $request_id);
	    	}
    		if($approve != 1 && $approve != 2){
	    		$this->session->set_flashdata('message', $this->load->view('msgbox/error', array('error' => 'Please select a approved option !') , True));
	    		redirect("approve/deal/" . $request_id);
	    	}
	    	
    		$state = 30;
	    	if($approve == 2){
	    		$state = 50;
	    	}else if($approve == 1){
	    		$state = 40;
	    	}
	    	
	    	$request_data = array(
	    		'paid'     			=> $paid,
	    		'approve'   		=> $approve,
	    		'manager_comment'  	=> $manager_comment,
	    		'manager_user_name' => $operator,
	    		'mc_provided'   	=> $mc_provided,
	    		'state'  			=> $state,
	    		'manager_time' 		=> $dnow
	    	);
	    	
	    	// Change leave type to Unpaid if $paid==2 (approval is unpaid)
	    	if ($paid==2)
	    		$request_data['leave_type_id'] = LEAVE_TYPE_UNPAID;
	    	
	    	$b_update = $this->request_m->update($request_id, $request_data);
    		if($b_update){
    			//add hisotry log
    			$approve_log = "";
    			if($state == 50){
    				$approve_log = "rejected";
    			}else if($state == 40){
    				$approve_log = "approve";
    			}
            	$this->load->model('St_request_log_m', 'request_log_m');
            	$log = array(
            		'id' => $request_id,
            		'operator_id' => $this->session->userdata('user_id'),
            		'operation' => $approve_log,
            		'operate_time' => $dnow,
            		'operate_ip' => $this->input->ip_address()
            	);
            	$log = array_merge($request_data, $log);
            	$this->request_log_m->insert($log);
            	
    			//send a mail
	    		$requestinfo = $this->request_m->get_info($request_id);
            	
            	if($requestinfo->user_level_id == 1){
    				$supervisors = $this->users_m->get_supervisors_by_user_name($requestinfo->user_name);
            		if(!empty($supervisors)){
            			foreach($supervisors as $supervisor){
            				if(!empty($supervisor)){
            					if(!empty($supervisor->email)){
            						$this->phpmail->AddAddress($supervisor->email);
            					}
            				}
            			}
            		}
            	}
            	
    			//send mail to all manager and admin
            	$managers = $this->users_m->get_managers_by_user_name($requestinfo->user_name);
            	if(!empty($managers)){
            		foreach($managers as $manager){
            			if(!empty($manager)){
            				if(!empty($manager->email)){
            					$this->phpmail->AddAddress($manager->email);
            				}
            			}
            		}
            	}
	    		
	    		$approve_operate = "Approved";
            	if($state == 50){
    				$approve_operate = "Rejected";
    			}
    			
	    		$subject = $approve_operate . " ". $requestinfo->leave_type . " Request R". $request_id ." - "
            						.$requestinfo->first_name . " " . $requestinfo->last_name
            						." From ". date(DATE_FORMAT, $requestinfo->start_day)." to ".date(DATE_FORMAT, $requestinfo->end_day);
	    		
	    		$this->phpmail->setSubject($subject);
            	$mail_body['appove_url'] = site_url('approve/deal/'.$request_id);
            	$mail_body['appove_name'] = "approve this leave application here.";
            	$mail_body['request'] = $requestinfo;
            	$this->phpmail->setBody($this->load->view('mail/approve', $mail_body , True));
            	if(!$this->phpmail->send()){
		        	log_message("debug", "send mail .... ".$subject . $this->phpmail->ErrorInfo);
		        }
		        $this->phpmail->ClearAllAddresses();
		        
    			if(!empty($requestinfo->email)){
            		$this->phpmail->AddAddress($requestinfo->email);
            	}
	    		if(!empty($requestinfo->personal_email)){
            		$this->phpmail->AddAddress($requestinfo->personal_email);
            	}
            	$mail_body['appove_url'] = site_url('request/view/'.$request_id);
            	$mail_body['appove_name'] = "view this leave application here.";
            	$this->phpmail->setBody($this->load->view('mail/approve', $mail_body , True));
    			if(!$this->phpmail->send()){
		        	log_message("debug", "send mail .... ".$subject . $this->phpmail->ErrorInfo);
		        }
	    	}else{
	    		$this->session->set_flashdata('message', $this->load->view('msgbox/error', array('error' => 'Operation failed !') , True));
	    		redirect("approve/deal/" . $request_id);
	    	}
	    	//TODO record approve history
    	}
    	
    	//TODO send an email notification
		redirect("approve#waiting", "refresh");
    }
    
    function do_update_deal(){
    	$dnow = now();
		$this->load->model('St_users_m', 'users_m');
    	$request_id = $this->input->post('request_id');
        
    	$operator = $this->session->userdata('user_name');
		$this->load->library('phpmail');
		$this->phpmail->readConfig();
    	
		if ($this->input->post('save')){
            $start_day 		= $this->input->post('start_day');
            $end_day 		= $this->input->post('end_day');
            $start_time 	= $this->input->post('start_time');
            $end_time 		= $this->input->post('end_time');
            $hours_used 	= $this->input->post('hours_used');
            $mc_provided 	= $this->input->post('mc_provided');
            $carer_leave 	= $this->input->post('carer_leave');
            $leave_type 	= $this->input->post('leave_type');
            $reason 		= $this->input->post('reason');
            
            $request_type = $this->input->post('request_type');
            
            $mc_filename = "";
            if($this->input->post('leave_type') == LEAVE_TYPE_LATE || $this->input->post('leave_type') == LEAVE_TYPE_LUNCH || $this->input->post('leave_type') == LEAVE_TYPE_CLOCK){
            	$start_day = $this->input->post('arrival_day');
            	$start_time = $this->input->post('arrival_time');
            	$hours_used = $this->input->post('minutes_used')/60;//The field is repaced with minutes here
            	$end_day = $this->input->post('arrival_day');
            	$end_time = "";
            	$request_type = 2;
            }
            
			$include_break = 0;
            
			if($request_type == "1"){
            	$start_time = "";
            	$end_time = "";
            }else if($request_type == "2"){
            	$end_day = $start_day;
            	$include_break = $this->input->post('include_break');
            }
			
			$request_data = array(
				'add_time' 		=> now(),
				'leave_type_id' => $leave_type,
				'request_type'  => $request_type,
				'include_break' => $include_break,
                'start_day' 	=> strtotime($start_day),
                'end_day' 		=> strtotime($end_day),
                'start_time' 	=> $start_time,
                'end_time' 		=> $end_time,
				'start_date' 	=> strtotime($start_day ." " . $start_time),
				'end_date' 		=> strtotime($end_day . " " . $end_time),
                'hours_used' 	=> $hours_used,
				'mc_provided'	=> $mc_provided,
				'carer_leave' 	=> $carer_leave,
				'reason'	 	=> $reason
            );
			
	    	$b_update = $this->request_m->update($request_id, $request_data);
			
	    	if ($b_update){
	           	$this->load->model('St_request_log_m', 'request_log_m');
	           	$log = array(
	           		'id' => $request_id,
	           		'operator_id' => $this->session->userdata('user_id'),
	           		'operation' => 'update',
	           		'operate_time' => $dnow,
	           		'operate_ip' => $this->input->ip_address()
	           	);
	           	$log = array_merge($request_data, $log);
	           	$this->request_log_m->insert($log);
	    	}
	    	redirect("request/view/".$request_id);
		}
		else if($this->input->post('operate')=='delete'){
		    //send a mail
	   		$requestinfo = $this->request_m->get_info($request_id);
	          	
	       	if($requestinfo->user_level_id == 1){
				$supervisors = $this->users_m->get_supervisors_by_user_name($requestinfo->user_name);
	       		if(!empty($supervisors)){
	       			foreach($supervisors as $supervisor){
	       				if(!empty($supervisor)){
	       					if(!empty($supervisor->email)){
	       						$this->phpmail->AddAddress($supervisor->email);
	       					}
/*	       					if(!empty($supervisor->personal_email)){
	      						$this->phpmail->AddCC($supervisor->personal_email);
	       					}
*/	       				}
	       			}
	       		}
	       	}
	       	
			//send mail to all manager and admin
	       	$managers = $this->users_m->get_managers_by_user_name($requestinfo->user_name);
	       	if(!empty($managers)){
	       		foreach($managers as $manager){
	       			if(!empty($manager)){
	       				if(!empty($manager->email)){
	       					$this->phpmail->AddAddress($manager->email);
	       				}
/*	       				if(!empty($manager->personal_email)){
	      					$this->phpmail->AddCC($manager->personal_email);
	       				}
*/	       			}
	       		}
	       	}
	       	
	       	// Send email to requested person.
		    if(!empty($requestinfo->email)){
            	$this->phpmail->AddAddress($requestinfo->email);
            }
	   		
    		$subject = "DELETED ". $requestinfo->leave_type . " Request R". $request_id ." - "
           						.$requestinfo->first_name . " " . $requestinfo->last_name
           						." From ". date(DATE_FORMAT, $requestinfo->start_day)." to ".date(DATE_FORMAT, $requestinfo->end_day);
	   		
    		$this->phpmail->setSubject($subject);
           	$mail_body['appove_url'] = "";
           	$mail_body['appove_name'] = "";
           	$mail_body['request'] = $requestinfo;
           	$this->phpmail->setBody($this->load->view('mail/approve', $mail_body , True));
		   	if(!$this->phpmail->send()){
	        	log_message("debug", "send mail .... ".$subject . $this->phpmail->ErrorInfo);
   			}           	
	        $this->phpmail->ClearAllAddresses();
			
			
			$this->request_m->delete($request_id);
			redirect("calendar");
		}
    }
    
    
    function approve_edit($request_id){
    	$data['navigation'] = "<a href='". site_url('admin/leave') ."' target='_top'>Leave Request</a> > <a href='". site_url('approve') ."'>Leave Approval</a> > Approve Edit";
    	$request = $this->request_m->get_info($request_id);
    	
    	if(empty($request)){
    		show_404();
    	}
    	
    	//load leave type model
		$this->load->model('St_leave_type_m', 'leave_type_m');
		
    	$leave_types = $this->leave_type_m->get_select_dropdown();
    	$data['leave_types'] = $leave_types;
    	
    	$data['request'] = $request;
    	
    	//load request log model
		$this->load->model('St_request_log_m', 'request_log_m');
    	$request_logs = $this->request_log_m->get_by_search($request_id, "", "", "", "", "", "", "");
		$log_list = array();
		foreach($request_logs AS $request_log){
			$log['request_id'] = $request_log->id;
			//Action
			$operation = $request_log->operation;
			$action = "";
			if("save" == $operation){
				$action = "saved new request";
			}else if("apply" == $operation){
				$action = "applied request";
			}else if("update" == $operation){
				$action = "updated request";
			}else if("approve" == $operation){
				$action = "approved request";
			}else if("rejected" == $operation){
				$action = "rejected request";
			}else if("cancel" == $operation){
				$action = "canceled request";
			}
			$log['action'] = $action;
			//Actioned On
			$log['actioned_on'] = date(DATETIME_FORMAT, $request_log->operate_time);
			//Actioned By
			$log['actioned_by'] = $request_log->oprator_first_name . " " . $request_log->oprator_last_name;
			$log['action_ip'] = $request_log->operate_ip;
			$log_list[] = $log;
		}
		$data['request_logs'] = $log_list;
    	$this->load->view('navigation', $data);
    	$this->load->view('request/approve_edit_request', $data);
    }
    
    
    /**
     * change approve request
     * @return page
     */
    function do_approve_edit(){
        $this->form_validation->set_rules('leave_type', 'Leave Type', 'required');

        $request_id = $this->input->post('request_id');
        $this->load->model('St_users_m', 'users_m');
        if ($this->form_validation->run() == FALSE) {
            $this->edit($request_id);
        } else {
        	$error = "";
        	$operate = $this->input->post('operate');
            $leave_type = $this->input->post('leave_type');
            $start_day = $this->input->post('start_day');
            $end_day = $this->input->post('end_day');
            $start_time = $this->input->post('start_time');
            $end_time = $this->input->post('end_time');
            $hours_used = $this->input->post('hours_used');
            $reason = $this->input->post('reason');
            
            $mc_filename = "";
            if($leave_type == LEAVE_TYPE_LATE
            	|| $leave_type == LEAVE_TYPE_LUNCH){
            	$start_day = $this->input->post('arrival_day');
            	$start_time = $this->input->post('arrival_time');
            	$hours_used = $this->input->post('minutes_used');//The field is repaced with minutes here
            	$end_day = "";
            	$end_time = "";
            }else if($leave_type == LEAVE_TYPE_PERSONAL){
		        $fileElementName = 'medical_certificate';

		        if(empty($_FILES[$fileElementName]['tmp_name']) || $_FILES[$fileElementName]['tmp_name'] == 'none')
				{}else{
					if(!empty($_FILES[$fileElementName]['error']))
					{
						switch($_FILES[$fileElementName]['error'])
						{
							case '1':
								$error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
								break;
							case '2':
								$error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
								break;
							case '3':
								$error = 'The uploaded file was only partially uploaded';
								break;
							case '4':
								$error = 'No file was uploaded.';
								break;
				
							case '6':
								$error = 'Missing a temporary folder';
								break;
							case '7':
								$error = 'Failed to write file to disk';
								break;
							case '8':
								$error = 'File upload stopped by extension';
								break;
							case '999':
							default:
								$error = 'No error code avaiable';
						}
					}else 
					{
						$tempFile = $_FILES[$fileElementName]['tmp_name'];
						
						$now_dir = date('Y/m/d');
						$targetPath = str_replace('//','/',DIR_MEDICAL_CERTIFICATE . $now_dir . '/');
						log_message("debug","targetpath === " .$targetPath);
						if(mkdirs($targetPath)){
							$filename = $_FILES[$fileElementName]['name'];
							
							// Get the extension from the filename.
							$extension = substr($filename, strpos($filename,'.'), strlen($filename)-1);
							
							$target_filename = random_filename($targetPath, $extension, 6);
							$mc_filename = $now_dir . '/' . $target_filename;
							
							$targetFile = $targetPath . $target_filename;
							
							move_uploaded_file($tempFile,$targetFile);
						}else{
							$error = "Can't create directory";
						}
					}
				}
            }

        	if($error != ""){
				$this->session->set_flashdata('auth', $this->load->view('msgbox/error', $error , True));
				$this->edit($request_id);
			}else{
				$include_break = 0;
	            $request_type = $this->input->post('request_type');
				if($request_type == "1"){
	            	$start_time = "";
	            	$end_time = "";
	            }else if($request_type == "2"){
	            	$end_day = $start_day;
	            	$include_break = $this->input->post('include_break');
	            }
	            
	            /*
	             * read user type
	             * read work time
	             * 
	             */
	            $dnow = now();
				$request_data = array(
	                'leave_type_id' => $leave_type,
					'request_type'  => $request_type,
					'include_break' => $include_break,
	                'start_day' 	=> strtotime($start_day),
	                'end_day' 		=> strtotime($end_day),
	                'start_time' 	=> $start_time,
	                'end_time' 		=> $end_time,
					'start_date' 	=> strtotime($start_day ." " . $start_time),
					'end_date' 		=> strtotime($end_day . " " . $end_time),
	                'hours_used' 	=> $hours_used,
	                'reason' 		=> $reason
	            );
	            
	            if(!empty($mc_filename)){
	            	$a_mc = array(
	            		'medical_certificate_file' => $mc_filename
	            	);
	            	$request_data = array_merge($request_data, $a_mc);
	            }
	            
	            //update a request
	            log_message("debug", "update a request id ...... " . $request_id);
	            
				$b_update = $this->request_m->update($request_id, $request_data);
	            if(!$b_update){
	            	$error = "Leave request update failed.";
	            }else{
	            	//add hisotry log
	            	$this->load->model('St_request_log_m', 'request_log_m');
	            	$log = array(
	            		'id' => $request_id,
	            		'operator_id' => $this->session->userdata('user_id'),
	            		'operation' => "changed",
	            		'operate_time' => $dnow,
	            		'operate_ip' => $this->input->ip_address()
	            	);
	            	$log = array_merge($request_data, $log);
	            	$this->request_log_m->insert($log);
	            	
	            	$requestinfo = $this->request_m->get_info($request_id);
	            	$user_name = $requestinfo->user_name;
            		$this->load->library('phpmail');
            		$this->phpmail->readConfig();
            		
            		$supervisors = $this->users_m->get_supervisors_by_user_name($user_name);
        			if(!empty($supervisors)){
            			foreach($supervisors as $supervisor){
            				if(!empty($supervisor)){
            					if(!empty($supervisor->email)){
            						$this->phpmail->AddAddress($supervisor->email);
            					}
//            					if(!empty($supervisor->personal_email)){
//            						$this->phpmail->AddCC($supervisor->personal_email);
//            					}
            				}
            			}
        			}
            		
            		//send mail to all manager and admin
            		$managers = $this->users_m->get_managers_by_user_name($user_name);
        			if(!empty($managers)){
            			foreach($managers as $manager){
            				if(!empty($manager)){
            					if(!empty($manager->email)){
            						$this->phpmail->AddAddress($manager->email);
            					}
//            					if(!empty($manager->personal_email)){
//            						$this->phpmail->AddCC($manager->personal_email);
//            					}
            				}
            			}
        			}
        			$amount_leave = "";
        			if($requestinfo->leave_type_id == LEAVE_TYPE_ANNUAL 
        				|| $requestinfo->leave_type_id == LEAVE_TYPE_PERSONAL){
            			//calculate total annual/personal leave balance
				    	$this->load->model('St_working_hours_m', 'working_hours_m');    	
				    	$total_working_stat = $this->working_hours_m->get_staff_working_hours($user_name);
				    	
				    	$work_count = $total_working_stat->work_count;
				    	
				    	if($requestinfo->leave_type_id == LEAVE_TYPE_ANNUAL){
				    		$total_annual = $total_working_stat->annual_hours_count;
				    		$annual_used_count = $total_working_stat->annual_used_count;
				    		//calculate the annual leave balance
				    		$annual_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_ANNUAL);
				    		$annual_upcoming_used = $this->request_m->get_upcoming_count_by_leave_type($user_name, LEAVE_TYPE_ANNUAL);
				    		$annual_upcoming_used = IS_NULL($annual_upcoming_used)?0:$annual_upcoming_used;
				    		$amount_leave_count = $total_annual - $annual_used - $annual_upcoming_used;
				    		$amount_leave_count = round($amount_leave_count, 2);
				    		$amount_leave = ($amount_leave_count > 0)?$amount_leave_count:0;
				    	}
				    	else if($requestinfo->leave_type_id == LEAVE_TYPE_PERSONAL){
				    		$total_personal = $total_working_stat->personal_hours_count;
				    		$personal_used_count = $total_working_stat->personal_used_count;
				    		//calculate the annual leave balance
				    		$personal_used = $this->request_m->get_used_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL);
				    		$personal_upcoming_used = $this->request_m->get_upcoming_count_by_leave_type($user_name, LEAVE_TYPE_PERSONAL);
				    		$personal_upcoming_used = IS_NULL($personal_upcoming_used)?0:$personal_upcoming_used;
				    		$amount_leave_count = $total_personal - $personal_used - $personal_upcoming_used;
				    		$amount_leave_count = round($amount_leave_count, 2);
				    		$amount_leave = ($amount_leave_count > 0)?$amount_leave_count:0;
				    	}
        			}
        			if($amount_leave > 1){
		    			$amount_leave = $amount_leave . " Hours";
		    		}else{
		    			$amount_leave = $amount_leave . " Hour";
		    		}
        			$mail_body['amount_leave_left'] = $amount_leave;
        			$subject = "Changed ". $requestinfo->leave_type ." Requested - " 
        				. $requestinfo->first_name . " " . $requestinfo->last_name
        				." From ".$start_day." to ".$end_day;
		    		$this->phpmail->setSubject($subject);
		    		
	            	$mail_body['appove_url'] = site_url('approve/deal/'.$request_id);
	            	$mail_body['appove_name'] = "approve this leave application here.";
	            	$mail_body['request'] = $requestinfo;
	            	
        			$this->phpmail->setBody($this->load->view('mail/request', $mail_body , True));
        			if(!$this->phpmail->send()){
			        	log_message("debug", "send mail .... ".$subject . $this->phpmail->ErrorInfo);
			        }
			        
			        //
			        $this->phpmail->ClearAllAddresses();
            		$email = $this->session->userdata("email");
            		if(!empty($email)){
            			$this->phpmail->AddAddress($email);
        			}
        			$personal_email = $this->session->userdata("personal_email");
        			if(!empty($personal_email)){
        				$this->phpmail->AddCC($personal_email);	
        			}
        			$mail_body['appove_url'] = site_url('request/view/'.$request_id);
        			$mail_body['appove_name'] = "view this leave application here.";
            		$this->phpmail->setBody($this->load->view('mail/request', $mail_body , True));
        			if(!$this->phpmail->send()){
			        	log_message("debug", "send mail .... ".$subject . $this->phpmail->ErrorInfo);
			        }
	            }
	            
	            //return json 
//	            $result = array(
//		        	"message"=> "",
//		        	"status" 	=> ""
//		        );
		        
//				if(!empty($error)){
//					$result['message'] = $error;
//					$result['status'] = "fail";
//				}else{
//					$result['message'] = "Leave update successfully.";
//					$result['status'] = "ok";
//		        }
//		        echo json_encode($result);
				if(!empty($error)){
					$this->session->set_flashdata('auth', $this->load->view('msgbox/error', $error , True));
					$this->approve_edit($request_id);
				}else{
	            	redirect("approve");
	            }
			}
        }
    }
    
    
	/**
     * view a request
     */
	function view($request_id){
    	$data['navigation'] = "<a href='". site_url('admin/leave') ."' target='_top'>Leave Request</a> > <a href='". site_url('approve') ."'>Leave Approval</a> > View";
    	$request = $this->request_m->get_info($request_id);
    	    	
    	if(empty($request)){
    		show_404();
    	}
    	
    	$supervisor_name = "";
    	$manager_name = "";
    	
    	$this->load->model('St_users_m', 'users_m');
    	if(!is_null($request->supervisor_user_name)){
    		$supervisor = $this->users_m->get_by_user_name($request->supervisor_user_name);
    		if(!empty($supervisor)){
    			$data['supervisor_name'] = $supervisor->first_name . " " . $supervisor->last_name;
    		}
    	}
    	$data['supervisor_name'] = $supervisor_name;
    	
		if(!is_null($request->manager_user_name)){
    		$manager = $this->users_m->get_by_user_name($request->manager_user_name);
    		if(!empty($manager)){
    			$data['manager_name'] = $manager->first_name . " " . $manager->last_name;
    		}
    	}
    	$data['manager_name'] = $manager_name;
    	
    	//load leave type model
		$this->load->model('St_leave_type_m', 'leave_type_m');
		
    	$leave_types = $this->leave_type_m->get_dropdown();
    	$data['leave_types'] = $leave_types;
    	
    	if($request->leave_type_id == LEAVE_TYPE_LATE || $request->leave_type_id == LEAVE_TYPE_LUNCH)
    		$request->hours_used = $request->hours_used * 60;
    	
    	$data['request'] = $request;
    	
    	//load request log model
		$this->load->model('St_request_log_m', 'request_log_m');
    	$request_logs = $this->request_log_m->get_by_search($request_id, "", "", "", "", "", "", "");
		$log_list = array();
		foreach($request_logs AS $request_log){
			$log['request_id'] = $request_log->id;
			//Action
			$operation = $request_log->operation;
			$action = "";
			if("save" == $operation){
				$action = "saved new request";
			}else if("apply" == $operation){
				$action = "applied request";
			}else if("update" == $operation){
				$action = "updated request";
			}else if("approve" == $operation){
				$action = "approved request";
			}else if("rejected" == $operation){
				$action = "rejected request";
			}else if("cancel" == $operation){
				$action = "canceled request";
			}
			$log['action'] = $action;
			//Actioned On
			$log['actioned_on'] = date(DATETIME_FORMAT, $request_log->operate_time);
			//Actioned By
			$log['actioned_by'] = $request_log->oprator_first_name . " " . $request_log->oprator_last_name;
			$log['action_ip'] = $request_log->operate_ip;
			$log_list[] = $log;
		}
		$data['request_logs'] = $log_list;
		$data['user_level'] = $this->session->userdata('user_level');
		$data['current_user_id'] = $this->session->userdata('user_id');
			
		$this->load->view('navigation', $data);
    	$this->load->view('request/view_request_4', $data);
    }
    
    /**
     * get departments by site id
     * @return json
     */
    function dept_by_site_ajax(){
    	$site_id = $this->input->post('site_id');
    	$select = $this->input->post('select');
    	$result = array();
    	$index = 0;
	    $result[$index]['id'] = "";
	    if($select){
	    	$result[$index]['name'] = "Please select..";
	    }else{
	    	$result[$index]['name'] = "All";
	    }
    	if(empty($site_id)){
	    	$departments = $this->department_m->get_distinct_name();
	    	foreach ($departments as $row){
	    		$index++;
	    		$result[$index]['id'] = $row->department;
	    		$result[$index]['name'] = $row->department;
	    	}
    	}else{
    		$departments = $this->department_m->get_by_site($site_id);
	    	foreach ($departments as $row){
	    		$index++;
	    		$result[$index]['id'] = $row->department;
	    		$result[$index]['name'] = $row->department;
	    	}
    	}
    	echo json_encode($result);
    }
    
    /**
     * get dept. by site id for add 
     */
    function dept_by_add_site_ajax(){
    	$site_id = $this->input->post('site_id');
    	$select = $this->input->post('select');
    	$result = array();
    	$index = 0;
	    $result[$index]['id'] = "";
	    if($select){
	    	$result[$index]['name'] = "Please select..";
	    }else{
	    	$result[$index]['name'] = "All dept.";
	    }
    	if(!empty($site_id)){
	    	$departments = $this->department_m->get_by_site($site_id);
	    	foreach ($departments as $row){
	    		$index++;
	    		$result[$index]['id'] = $row->id;
	    		$result[$index]['name'] = $row->department;
	    	}
    	}
    	echo json_encode($result);
    }
    
    function close() {
        $this->index();
    }
    
}
