<?php

class Leavetype extends Controller {

	function Leavetype()
	{
		parent::Controller();
		if(!$this->userauth->logined()){
			$infoMsg = array(
				'info' => 'Please login !'
			);
			$this->session->set_flashdata('auth', $this->load->view('msgbox/info', $infoMsg , True));
			redirect("login");
		}else{
			if(!$this->userauth->judge_priv("leavetype")){
				$errorMsg = array(
					'error' => 'You have no privilege into this page !'
				);
				$this->session->set_flashdata('auth', $this->load->view('msgbox/error', $errorMsg , True));
				redirect("msgbox", 'location');
			}
		}
		$this->load->model('St_leave_type_m', 'leave_type_m');
	}
	
	function index()
	{
		$data['leave_types'] = $this->leave_type_m->get_leave_types();
        $data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('leavetype')."'>Leave type</a> > List";
        $this->load->view('navigation', $data);
        $this->load->view('list_leave_types', $data);
	}

	/**
	 * 
	 * @param $id
	 * @return view
	 */
	function add(){
		$data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('leavetype')."'>Leave type</a> > Add New Leave type";
		$this->load->model('St_outcome_m', 'outcome_m');
		$data['working_hours_outcomes'] = $this->outcome_m->get_working_hours_select_dropdown();
		$data['leave_balance_outcomes'] = $this->outcome_m->get_leave_balance_select_dropdown();
		$data['pay_statuses'] = $this->_get_pay_status_dropdown();
		$data['statuses'] = $this->_get_status_dropdown();
        $this->load->view('navigation', $data);
        $this->load->view('add_leave_type', $data);
	}
	
	function do_add(){
		$this->form_validation->set_error_delimiters('<p class="error">', '</p>');
		$this->form_validation->set_rules('leave_type_name', 'Name', 'trim|required');
		$this->form_validation->set_rules('working_hours_outcome_id', 'Outcome(Working hours)', 'required');
		$this->form_validation->set_rules('leave_balance_outcome_id', 'Outcome(Leave Balance)', 'required');
		$this->form_validation->set_rules('pay_status', 'Pay status', 'required');
		$this->form_validation->set_rules('color', 'Color', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		if ($this->form_validation->run() == FALSE) {
            $this->add();
        } else {
			$leave_type_name= $this->input->post('leave_type_name');
			$working_hours_outcome_id 	= $this->input->post('working_hours_outcome_id');
			$leave_balance_outcome_id 	= $this->input->post('leave_balance_outcome_id');
			$pay_status 	= $this->input->post('pay_status');
			$color 	= $this->input->post('color');
			$status 		= $this->input->post('status');
			
			$is_duplicate = $this->leave_type_m->check_unique_name($leave_type_name, 0);
			if($is_duplicate){
				$this->session->set_flashdata('message', $this->load->view('msgbox/error', 
					array(
					'error' => 'Name exists!'
					), True));
				$this->add();
			}
			
			$leave_inifo = array(
				'leave_type' => $leave_type_name,
				'working_hours_outcome_id' => $working_hours_outcome_id,
				'leave_balance_outcome_id' => $leave_balance_outcome_id,
				'pay_status' => $pay_status,
				'color' => $color,
				'status' => $status
			);
			$b_insert = $this->leave_type_m->insert($leave_inifo);
			if($b_insert > 0){
				redirect("leavetype");
			}else{
				$this->session->set_flashdata('message', $this->load->view('msgbox/error', 
					array(
					'error' => 'Add failed!'
					), True)
				);
				$this->add();
			}
		}
	}
	
	/**
	 * edit a leave type
	 * @param $id
	 * @return view
	 */
	function edit($id) {
		$data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('leavetype')."'>Leave type</a> > Edit Leave type";
		$data['leave_type'] = $this->leave_type_m->get($id);
		
		$this->load->model('St_outcome_m', 'outcome_m');
		$data['working_hours_outcomes'] = $this->outcome_m->get_working_hours_select_dropdown();
		$data['leave_balance_outcomes'] = $this->outcome_m->get_leave_balance_select_dropdown();
		$data['pay_statuses'] = $this->_get_pay_status_dropdown();
		$data['statuses'] = $this->_get_status_dropdown();
        $this->load->view('navigation', $data);
        $this->load->view('edit_leave_type', $data);
    }
	
    /**
     * deal update a leave type
     * @return view
     */
	function do_edit() {
		$this->form_validation->set_rules('leave_type_name', 'Name', 'trim|required');
		$this->form_validation->set_rules('working_hours_outcome_id', 'Outcome(Working hours)', 'required');
		$this->form_validation->set_rules('leave_balance_outcome_id', 'Outcome(Leave Balance)', 'required');
		$this->form_validation->set_rules('pay_status', 'Pay status', 'required');
		$this->form_validation->set_rules('color', 'Color', 'trim|required');
		$leave_type_id = $this->input->post('leave_type_id');
		if ($this->form_validation->run() == FALSE) {
            $this->edit($leave_type_id);
        } else {
			$leave_type_name= $this->input->post('leave_type_name');
			$working_hours_outcome_id 	= $this->input->post('working_hours_outcome_id');
			$leave_balance_outcome_id 	= $this->input->post('leave_balance_outcome_id');
			$pay_status 	= $this->input->post('pay_status');
			$color 	= $this->input->post('color');
			$status 		= $this->input->post('status');
			
			$is_duplicate = $this->leave_type_m->check_unique_name($leave_type_name, $leave_type_id);
			if($is_duplicate){
				$this->session->set_flashdata('message', $this->load->view('msgbox/error', 
					array(
					'error' => 'Name exists!'
					), True));
				$this->edit($leave_type_id);
			}
			
			$leave_inifo = array(
				'leave_type' => $leave_type_name,
				'working_hours_outcome_id' => $working_hours_outcome_id,
				'leave_balance_outcome_id' => $leave_balance_outcome_id,
				'pay_status' => $pay_status,
				'color' => $color,
				'status' => $status
			);
			$b_update = $this->leave_type_m->update($leave_type_id, $leave_inifo);
			if($b_update){
				redirect("leavetype");
			}else{
				$this->session->set_flashdata('message', $this->load->view('msgbox/error', 
					array(
					'error' => 'Update failed!'
					), True)
				);
				$this->edit($leave_type_id);
			}
		}
	}
	
	function change_status($id, $status){
		if($status == ACTIVE){
			$this->leave_type_m->set_inactive($id);	
		}else if($status == IN_ACTIVE){
			$this->leave_type_m->set_active($id);	
		}
		redirect('leavetype');
	}
	
	function remove($id){
		$this->leave_type_m->set_inactive($id);
		redirect('leavetype');
	}
	
	/*
	 * get outcome dropdown 
	 * @return array
	 */
	function _get_outcome_dropdown(){
		return array(
			''  => 'Please select',
			'1' => 'Deduct Annual Leave',
			'2' => 'Deduct Personal Leave',
			'3' => 'Non Deducting',	
			'4' => 'Increase work hour'
		);
	}
	
	/**
	 * get pay status dropdown
	 * @return array
	 */
	function _get_pay_status_dropdown(){
		return array(
			''  => 'Please select',
			'2' => 'Unpaid',
			'1' => 'Paid'
		);
	}
	
	function _get_status_dropdown(){
		return array(
			''  => 'Please select',
			'1' => 'Active',
			'0' => 'InActive'
		);
	}
}