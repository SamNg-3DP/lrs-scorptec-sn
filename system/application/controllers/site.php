<?php

class Site extends Controller {

    function Site() {
        parent::Controller();
    	if(!$this->userauth->logined()){
			$infoMsg = array(
				'info' => 'You have no privilege into this page !'
			);
			$this->session->set_flashdata('auth', $this->load->view('msgbox/info', $infoMsg , True));
			redirect("login");
		}else{
			if(!$this->userauth->judge_priv("site")){
				$errorMsg = array(
					'error' => 'You have no privilege into this page !'
				);
				$this->session->set_flashdata('auth', $this->load->view('msgbox/error', $errorMsg , True));
				redirect("msgbox", 'location');
			}
		}
		
		$this->load->model('St_site_m', 'site_m');
        $this->form_validation->set_error_delimiters ( "<div style='font:Tahoma, Geneva, sans-serif; font-size:11px; color:#F00'>", "</div>" );
    }

    function index() {
        $data['sites'] = $this->site_m->get_all();
        $data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('site')."'>Site</a> > View";
        $data['status'] = '';
        $this->load->view('navigation', $data);
        $this->load->view('admin/site', $data);
    }

    function add() {
    	$data['status'] = '';
        $data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('site')."'>Site</a> > Add New";
        $this->load->view('navigation', $data);
        $this->load->view('admin/add_site');
    }

    function do_add() {
        $this->form_validation->set_rules('name', 'Site name', 'trim|required');
		$this->form_validation->set_rules('ip_range', 'IP range', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');

		$data['status'] = '';
        if ($this->form_validation->run() == FALSE) {
            $this->add();
        } else {
            $name = $this->input->post('name');
            
        	//check unique
        	$b_exist = $this->site_m->check_except($name, 0);
        	if($b_exist > 0){
        		$data['status'] = 'The Site Name already exists!';
        		$data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('site')."'>Site</a> > Add New";
        		$this->load->view('navigation', $data);
        		$this->load->view('admin/add_site');
        	}else{
        		$ip_range = $this->input->post('ip_range');
            	$status = $this->input->post('status');
            	$site_data = array(
	                'name' 			=> $name,
	                'ip_range' 		=> $this->input->post('ip_range'),
	                'status' 		=> $this->input->post('status'),
        			'operator_id'	=> $this->session->userdata('user_id')
	            );
            
            	$this->site_m->insert($site_data);
            	$this->index();
        	}
        }
    }
    
    function change_state() {
        $id = $this->uri->segment(3);
        $state = $this->uri->segment(4);
        if($state == 0) {
            $state = 1;
        } else {
            $state = 0;
        }
        
        $this->site_m->change_state($id, $state);
        $this->index();
    }

    function edit($id) {
        $data['site'] = $this->site_m->get($id);
        $data['status'] = '';
        $data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('site')."'>Site</a> > Edit site";
        $this->load->view('navigation', $data);
        $this->load->view('admin/edit_site', $data);
    }
    
	function do_edit() {
		
		$this->form_validation->set_rules('name', 'Site name', 'trim|required');
		$this->form_validation->set_rules('ip_range', 'IP range', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		
		$site_id = $this->input->post('site_id');
		if ($this->form_validation->run() == FALSE) {
            $this->edit($site_id);
        }else{
        	$name = $this->input->post('name');
        	//check unique
        	$b_exist = $this->site_m->check_except($name, $site_id);
        	if($b_exist > 0){
        		$data['status'] = 'The site already exists!';
        		$data['site'] = $this->site_m->get($site_id);
		        $data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('site')."'>Site</a> > Edit site";
		        $this->load->view('navigation', $data);
		        $this->load->view('admin/edit_site', $data);
        	}else{
        		$site_data = array(
	                'name' => $name,
	                'ip_range' 		=> $this->input->post('ip_range'),
	                'status' 		=> $this->input->post('status'),
        			'operator_id'	=> $this->session->userdata('user_id')
	            );
        		$b_update = $this->site_m->update($site_id, $site_data);
        		$this->index();
        	}
        	
        }
	}
	
	/**
	 * remove special id
	 * @return index view
	 */
	function remove($site_id){
		$this->load->model('St_department_m', 'department_m');
		//check whether child department exists
		$b_exist = $this->department_m->count_by_site($site_id);
		if($b_exist > 0){
        	$data['status'] = 'The site has department, please delete the departments!';
        }else{
        	$data['status'] = 'Delete succesfully!';
			$query = $this->site_m->delete($site_id);
		}
		$data['sites'] = $this->site_m->get_all();
        $data['navigation'] = "<a href='". site_url('admin') ."' target='_top'>Admin</a> > <a href='".site_url('site')."'>Site</a> > View";
        $this->load->view('navigation', $data);
        $this->load->view('admin/site', $data);
	}
}