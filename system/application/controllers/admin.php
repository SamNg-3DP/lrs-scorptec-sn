<?php

/**
 * Admin Controller
 * @author Bai Yinbing
 *
 */
class Admin extends Controller {
	
	function Admin()
	{
		parent::Controller();
		if(!$this->userauth->logined()){
			$infoMsg = array(
				'info' => 'Current operation need to login!'
			);
			
			$this->session->set_flashdata('auth', $this->load->view('msgbox/info', $infoMsg , True));
			$this->session->set_flashdata('back_url', current_url());
			redirect("login");
		}
	}
	
	/**
	 * goto admin manage page
	 * @return unknown_type
	 */
	function index()
	{
		$this->load->view('admin');
	}
	
	/**
	 * goto leave request manage page
	 * @return unknown_type
	 */
	function leave(){
		//all default leave url
		$back_url = $this->session->flashdata('back_url');
		if(!empty($back_url)){
			$data['main_url'] = $back_url;
		}else{
			$data['main_url'] = site_url('request');
		}
		
		$this->load->view('leave', $data);
	}
	
	/**
	 * goto jobincident manage page
	 * @return unknown_type
	 */
	function jobincident(){
		//all default leave url
		$back_url = $this->session->flashdata('back_url');
		if(!empty($back_url)){
			$data['main_url'] = $back_url;
		}else{
			$data['main_url'] = site_url('jobincident/incident_list/new');
		}
		
		$this->load->view('jobincident', $data);
	}
	
	/**
	 * load top page
	 * @return 
	 */
	function top(){
		//TODO display manager url by user role from vars
		$this->load->view('admin_top');
	}
	
	/**
	 * left menu
	 * @param $section
	 * @return 
	 */
	function menu($section="leave"){
		//TODO display menu url by user role from vars
		if($section == "admin"){
			$this->load->view('admin_menu');	
		}else if($section == "jobincident"){
			$this->load->model('St_job_incident_m', 'job_incident_m');
			
			$user_level = $this->session->userdata('user_level');
        	$site_id = $this->session->userdata('site_id');
        	$department_id = $this->session->userdata('department');
        	
		    if($user_level > 2){
	        	$site_id = 0;
	        	$department_id = 0;
        	}
			
			$data['new_count'] = $this->job_incident_m->count_approvals_by_new($user_level, $site_id, $department_id);
			$data['progress_count'] = $this->job_incident_m->count_approvals_by_progress($user_level, $site_id, $department_id);
			$data['completed_count'] = $this->job_incident_m->count_approvals_by_completed($user_level, $site_id, $department_id);
			
			$this->load->view('jobincident_menu', $data);
		}else{
			$this->load->view('leave_menu');
		}
	}
	
	function drag(){
		$this->load->view('admin_drag');
	}
}
?>