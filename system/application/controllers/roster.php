<?php

class Roster extends Controller {
    function Roster() {
        parent::Controller();
        $this->load->helper('url');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters("<div style='font:Tahoma, Geneva, sans-serif; font-size:11px; color:#F00'>", "</div>");
    }

    function index() {
        $data['navigation'] = "<a href='#'>Admin</a> > <a href='#'>Roster</a> > View";
        $this->load->view('navigation', $data);
        $this->load->view('roster');
    }
}
