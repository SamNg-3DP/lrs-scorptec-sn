/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function randomPassword()
{
    var seed = new Array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z',
                         'a','b','c','d','e','f','g','h','i','j','k','m','n','p','Q','r','s','t','u','v','w','x','y','z',
                         '2','3','4','5','6','7','8','9'
                         );
    seedlength = seed.length;
    var createPassword = '';
    for (i=0; i<10; i++) {
        j = Math.floor(Math.random() * seedlength);
        createPassword += seed[j];
    }
    return createPassword;
}

function generatePassword(pwd) {
    $("#" + pwd).val(randomPassword());
    $("#" + pwd).focus();
}

function cancelRequest(requestId){
	if(confirm("Do you want to cancel this request?"))
	{
		alert("Cancel succesfully!");
	}
}

function changeUserType(value){
	if(value == '1'){
		$("#add_fulltime").show();
		$("#add_othertime").hide();
		$("#user_type_fulltime").val(value);
	}else {
		$("#add_fulltime").hide();
		$("#add_othertime").show();
		$("#user_type_othertime").val(value);
	}
}
