# Leave Request System #

## Overview ##
The leave request system provides small business an easy way to manage staff leave requests, approvals and manage resources.

* Staff adding new leave requests
* Approving / rejecting leave requests by managers
* Keep records of medical certificate
* Job incident logging system
* On site services logging system
* Integrated with attendance system (finger print)
* Integrated with QuickBooks payroll

## Version ##
### Version 1.0 Release 1.0 [2010-07-06] ###
* Initial release
### Version 1.0 Release 1.1606 [2016-06-01] ###
* Added Piwik analytics tracking code